﻿using UnityEngine;
using UnityEditor;

namespace Paroxe.PdfRenderer.Internal
{
    public class PRParoxeBanner
    {
        private Texture2D m_ParoxeIcon;
        private Texture2D m_TwitterIcon;
        private Texture2D m_RatingIcon;
        private Texture2D m_FacebookIcon;

        private Texture2D m_OpenedIcon;
        private Texture2D m_ClosedIcon;

        private string m_Path;

        private string ParoxePath
        {
            get { return Path("Icons/Paroxe32.png"); }
        }

        private string TwitterPath
        {
            get { return Path("Icons/Twitter32.png"); }
        }

        private string FacebookPath
        {
            get { return Path("Icons/Facebook32.png"); }
        }

        private string RatingPath
        {
            get { return Path("Icons/Rating32.png"); }
        }

        private string OpenedPath
        {
            get { return Path("Icons/Open32.png"); }
        }

        private string ClosedPath
        {
            get { return Path("Icons/Close32.png"); }
        }

        public PRParoxeBanner(string path)
        {
            m_Path = path;

            Intilialize();
        }

        private string Path(string rel)
        {
            return m_Path + "/" + rel;
        }

        private Texture2D GetTexture(string path)
        {
            Texture2D tex = (Texture2D)AssetDatabase.LoadAssetAtPath(path, typeof(Texture2D));
            tex.hideFlags = HideFlags.HideAndDontSave;
            return tex;
        }

        private void Intilialize()
        {
            m_ParoxeIcon = GetTexture(ParoxePath);
            m_TwitterIcon = GetTexture(TwitterPath);
            m_RatingIcon = GetTexture(RatingPath);
            m_FacebookIcon = GetTexture(FacebookPath);

            m_OpenedIcon = GetTexture(OpenedPath);
            m_ClosedIcon = GetTexture(ClosedPath);
        }

        private void Space(float width, float height)
        {
            GUILayoutUtility.GetRect(width, height);
        }

        private void Space()
        {
            float w = 4.0f;
            float h = 32 * 0.75f;
            Space(w, h);
        }

        private bool OnCloseOpenGUI(bool isOpened)
        {
            Texture2D icon = isOpened ? m_ClosedIcon : m_OpenedIcon;

            Rect r = GUILayoutUtility.GetRect(icon.width * 0.3f, icon.height * 0.3f);
            GUI.DrawTexture(r, icon, ScaleMode.ScaleToFit);

            if (GUI.Button(r, "", new GUIStyle()))
            {
                return !isOpened;
            }

            return isOpened;
        }

        private void OnInconGUI(Texture icon, string weblink)
        {
            Rect r = GUILayoutUtility.GetRect(icon.width * 0.75f, icon.height * 0.75f);
            GUI.DrawTexture(r, icon, ScaleMode.ScaleToFit);

            if (GUI.Button(r, "", new GUIStyle()))
            {
                Application.OpenURL(weblink);
            }
        }

        public bool DoOnGUI(bool isOpened)
        {

            EditorGUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            isOpened = OnCloseOpenGUI(isOpened);
            if (isOpened)
            {
                Space();
                OnInconGUI(m_ParoxeIcon, "http://paroxe.com/");
                Space();
                OnInconGUI(m_RatingIcon, "https://www.assetstore.unity3d.com/en/#!/content/32815");
                Space();
                OnInconGUI(m_TwitterIcon, "https://twitter.com/Paroxe_dev");
                Space();
                OnInconGUI(m_FacebookIcon, "https://www.facebook.com/paroxe.multimedia/");
            }
            EditorGUILayout.EndHorizontal();

            return isOpened;
        }
    }
}
