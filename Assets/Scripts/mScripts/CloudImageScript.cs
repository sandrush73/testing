﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine.Video;
using Vuforia;

public class CloudImageScript : MonoBehaviour, IObjectRecoEventHandler
{
    #region VARIABLES
    public ImageTargetBehaviour behaviour;
    public GameObject marker3DPrefab;
    public GameObject videoPlayerObject, audioPlayerObject;
    public float maxValue;
    public Transform audioStarting;
    public GameObject audioVisualizerPrefab;
    public Color color1, color2;
    public GameObject multipleAuthorGameobject;
    public GameObject authorBtnPrefab;
    public Transform multipleTransform;
    [HideInInspector]
    public List<bool> trackingStates;
    [HideInInspector]
    public GameObject scanImageObject;

    CloudRecoBehaviour m_CloudRecoBehaviour;
    ObjectTracker m_ObjectTracker;
    TargetFinder m_TargetFinder;

    private int selectedUserIndex = 0;
    private int totalIndex = 0;
    private List<GameObject> imageTargets;    
    private IEnumerator videoIenum, audioIenum;
    private string currentUrl;
    private GameObject videoPlayerRawImage;
    private GameObject[] _sampleAudioVisualizers;
    private float[] _samples = new float[128];
    private bool isShowingSimulation = false, isPlayingAudio = false, isPlayingVideo = false, isShowingMarkers = true;
    private GameObject mainInstance;
    private PdfSceneScript.PlaneData markerData;
    #endregion

    #region ICLOUDEVENTHANDLER
    public void OnInitError(TargetFinder.InitState initError)
    {
        Debug.LogError("INIT ERROR");
    }

    public void OnInitialized(TargetFinder targetFinder)
    {
        Debug.Log("Initialization success");
        m_ObjectTracker = TrackerManager.Instance.GetTracker<ObjectTracker>();
        m_TargetFinder = targetFinder;
    }

    public void OnInitialized()
    {
        Debug.Log("Cloud Reco initialized successfully.");

        m_ObjectTracker = TrackerManager.Instance.GetTracker<ObjectTracker>();
        m_TargetFinder = m_ObjectTracker.GetTargetFinder<ImageTargetFinder>();
    }
    
    public void OnNewSearchResult(TargetFinder.TargetSearchResult targetSearchResult)
    {
        TargetFinder.CloudRecoSearchResult cloudRecoResult = (TargetFinder.CloudRecoSearchResult)targetSearchResult;
        m_CloudRecoBehaviour.CloudRecoEnabled = false;

        // Clear any existing trackables
        m_TargetFinder.ClearTrackables(false);

        // Enable the new result with the same ImageTargetBehaviour:
        //m_TargetFinder.EnableTracking(cloudRecoResult, m_ImageTargetBehaviour.gameObject);

        // Pass the TargetSearchResult to the Trackable Event Handler for processing
        //m_ImageTargetBehaviour.gameObject.SendMessage("TargetCreated", cloudRecoResult, SendMessageOptions.DontRequireReceiver);

        GameObject newSearchResult = Instantiate(behaviour.gameObject) as GameObject;
        //newSearchResult.GetComponent<ImageTargetScript>().index = totalIndex;
        //newSearchResult.GetComponent<ImageTargetScript>().cloudImageScript = this;
        totalIndex++;
        if (behaviour)
        {
            m_TargetFinder.EnableTracking(cloudRecoResult, newSearchResult);
            newSearchResult.SendMessage("TargetCreated", cloudRecoResult, SendMessageOptions.DontRequireReceiver);
            //ObjectTracker tracker = TrackerManager.Instance.GetTracker<ObjectTracker>();
            //ImageTargetBehaviour imageTargetBehaviour = (ImageTargetBehaviour)tracker.GetTargetFinder<TargetFinder>().EnableTracking(targetSearchResult, newSearchResult);
        }
        string cloudId = targetSearchResult.TargetName;
        Debug.Log("idContent: " + cloudId);
        imageTargets.Add(newSearchResult);
        trackingStates.Add(false);
        newSearchResult.GetComponent<ImageTargetScript>().enabled = true;
        StartCoroutine(GetMarkerData(cloudId, newSearchResult));
    }

    public void OnStateChanged(bool scanning)
    {
        if (scanning)
        {
            //ObjectTracker tracker = TrackerManager.Instance.GetTracker<ObjectTracker>();
            //tracker.GetTargetFinder<TargetFinder>().ClearTrackables(false);
        }
    }

    public void OnUpdateError(TargetFinder.UpdateState updateError)
    {
        Debug.LogError("Update Error");
    }
    #endregion

    #region UNITY
    // Start is called before the first frame update
    void Start()
    {
        Debug.Log(Application.persistentDataPath);
        videoPlayerObject.SetActive(false);
        audioPlayerObject.SetActive(false);
        _sampleAudioVisualizers = new GameObject[128];
        for (int i = 0; i < 128; i++)
        {
            GameObject g = Instantiate(audioVisualizerPrefab, audioStarting) as GameObject;
            g.GetComponent<UnityEngine.UI.Image>().color = Color.Lerp(color1, color2, i / 128.0f);
            _sampleAudioVisualizers[i] = g;
        }
        /*CloudRecoBehaviour cloud = GetComponent<CloudRecoBehaviour>();
        if (cloud != null)
        {
            cloud.RegisterEventHandler(this);
        }*/
        m_CloudRecoBehaviour = GetComponent<CloudRecoBehaviour>();
        if (m_CloudRecoBehaviour)
        {
            m_CloudRecoBehaviour.RegisterEventHandler(this);
        }
        videoPlayerRawImage = videoPlayerObject.GetComponentInChildren<RawImage>().gameObject;
        imageTargets = new List<GameObject>();
        trackingStates = new List<bool>();
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            OnBackPressed();
        }

        if (isPlayingAudio && !audioPlayerObject.GetComponent<AudioSource>().isPlaying)
        {
            OnFinishedPlayingAudio();
        }
        else if (isPlayingVideo && !videoPlayerRawImage.GetComponent<VideoPlayer>().isPlaying)
        {
            OnFinishedPlayingVideo();
        }

        if (audioPlayerObject.GetComponent<AudioSource>().isPlaying)
        {
            audioPlayerObject.GetComponent<AudioSource>().GetSpectrumData(_samples, 0, FFTWindow.Blackman);
            for (int i = 0; i < 128; i++)
            {
                if (_sampleAudioVisualizers[i] != null)
                {
                    _sampleAudioVisualizers[i].transform.localScale = new Vector3(1, _samples[i] * maxValue, 1);
                }
            }
        }
    }
    #endregion

    #region EVENTHANDLERS
    private IEnumerator GetMarkerData(string cloudId, GameObject g)
    {
        //todo string _toSend
        string _toSend = "http://oetdlabs.com/temp1.php?id=" + cloudId;
        UnityWebRequest request = UnityWebRequest.Get(_toSend);
        request.SetRequestHeader("Set-cookie", string.Format("ci_session={0}", PlayerPrefs.GetString(Constants.AUTH_TOKEN)));
        yield return request.SendWebRequest();
        print(request.responseCode);
        if (request.responseCode == 200)
        {
            PdfSceneScript.PlaneData[] data = BasicNavigation.JsonHelper.getJsonArray<PdfSceneScript.PlaneData>(request.downloadHandler.text);
            markerData = data[0];
            SetUpMarkers(g);
        }
        else if (request.responseCode == 401)
        {
            string toSend1 = Constants.BASE_URL + Constants.LOGIN_URL + "?" + Constants.LOGIN_ID + "=" + PlayerPrefs.GetString(Constants.LOGIN_ID) + "&" +
                                 Constants.PASSWORD_HASH + "=" + PlayerPrefs.GetString(Constants.PASSWORD_HASH) + "&" +
                                 Constants.CLIENT_ACCESSKEY + "=" + PlayerPrefs.GetString(Constants.CLIENT_ACCESSKEY);

            UnityWebRequest req = UnityWebRequest.Get(toSend1);
            yield return req.SendWebRequest();
            if (req.responseCode == 200)
            {
                BasicNavigation.LoginModel login = JsonUtility.FromJson<BasicNavigation.LoginModel>(req.downloadHandler.text);
                if (login.Status == Constants.SUCCESS)
                {
                    // Debug.Log("Auth token" + login.AuthToken);
                    PlayerPrefs.SetString(Constants.AUTH_TOKEN, login.AuthToken);
                    UnityWebRequest unityWebRequest = UnityWebRequest.Get(_toSend);
                    unityWebRequest.SetRequestHeader("Set-cookie", string.Format("ci_session={0}", login.AuthToken));
                    //Debug.Log(unityWebRequest.GetRequestHeader("Set-cookie"));
                    yield return unityWebRequest.SendWebRequest();
                    //Debug.Log(request.GetResponseHeader("Set-cookie"));
                    if (unityWebRequest.responseCode == 200)
                    {
                        PdfSceneScript.PlaneData[] data = BasicNavigation.JsonHelper.getJsonArray<PdfSceneScript.PlaneData>(request.downloadHandler.text);
                        markerData = data[0];
                        SetUpMarkers(g);
                    }
                }
            }
        }
    }

    private void SetUpMarkers(GameObject g)
    {
        Debug.Log(markerData.AnchorLists.Length);
        if (markerData.AnchorLists.Length > 1)
        {
            multipleAuthorGameobject.SetActive(true);
            for (int i = 0; i < markerData.AnchorLists.Length; i++)
            {
                GameObject btn = Instantiate(authorBtnPrefab, multipleTransform) as GameObject;
                int index = i;
                btn.GetComponentInChildren<TextMeshProUGUI>().text = markerData.AnchorLists[index].Author;
                btn.GetComponent<Button>().onClick.AddListener(() =>
                {
                    selectedUserIndex = index;
                    multipleAuthorGameobject.SetActive(false);
                    SetMarkersNow(g);
                });
            }
        }
        else
        {
            selectedUserIndex = 0;
            SetMarkersNow(g);
        }
    }

    private void SetMarkersNow(GameObject g)
    {
        PdfSceneScript.Anchors[] allAnchors = markerData.AnchorLists[selectedUserIndex].Anchors;
        for (int j = 0; j < allAnchors.Length; j++)
        {
            int i = j;
            GameObject m = Instantiate(marker3DPrefab, g.transform) as GameObject;
            float xv = float.Parse(allAnchors[i].x);
            float yv = float.Parse(allAnchors[i].y);
            m.transform.localPosition = new Vector3(xv, 0, yv);
            m.GetComponent<MarkerScript>().cloudImageScript = this;
            m.GetComponent<MarkerScript>()._tag = allAnchors[i];
            m.GetComponent<MarkerScript>().enabled = true;
        }
    }

    private IEnumerator PlayVideo(string idContent)
    {
        if (!isPlayingVideo)
        {
            currentUrl = string.Empty;
            StartCoroutine(GetUrlDetails(idContent));
            while (currentUrl == string.Empty) yield return null;
            Camera.main.backgroundColor = Color.black;
            videoPlayerObject.SetActive(true);
            VideoPlayer videoPlayer = videoPlayerRawImage.GetComponent<VideoPlayer>();
            AudioSource audioSource = videoPlayerRawImage.GetComponent<AudioSource>();
            videoPlayer.source = VideoSource.Url;
            videoPlayer.url = currentUrl;
            videoPlayer.audioOutputMode = VideoAudioOutputMode.AudioSource;
            videoPlayer.EnableAudioTrack(0, true);
            videoPlayer.SetTargetAudioSource(0, audioSource);
            videoPlayer.Prepare();
            while (!videoPlayer.isPrepared)
            {
                yield return null;
            }
            videoPlayerRawImage.GetComponent<RawImage>().texture = videoPlayer.texture;
            videoPlayer.GetComponent<RawImage>().SetNativeSize();
            videoPlayerRawImage.transform.localPosition = Vector3.zero;
            videoPlayer.Play();
            audioSource.Play();
            isPlayingVideo = true;
        }
    }

    private void OnFinishedPlayingVideo()
    {
        Camera.main.backgroundColor = new Color(106, 106, 106, 255);
        videoPlayerObject.SetActive(false);
        videoPlayerRawImage.GetComponent<VideoPlayer>().Stop();
        videoPlayerRawImage.GetComponent<AudioSource>().Stop();
        isPlayingVideo = false;
    }

    private void OnFinishedPlayingAudio()
    {
        audioPlayerObject.GetComponent<AudioSource>().Stop();
        audioPlayerObject.SetActive(false);
        isPlayingAudio = false;
    }

    private void OnFinishedShowingSimulation()
    {
        AssetBundle.UnloadAllAssetBundles(true);
        Destroy(mainInstance);
        isShowingSimulation = false;
    }

    private IEnumerator PlayAudio(string idContent)
    {
        if (!isPlayingAudio)
        {
            currentUrl = string.Empty;
            StartCoroutine(GetUrlDetails(idContent));
            while (currentUrl == string.Empty) yield return null;
            audioPlayerObject.SetActive(true);
            AudioType audioType;
            string format = currentUrl.Substring(currentUrl.Length - 3);
            if (format == "mp2" || format == "mp3") audioType = AudioType.MPEG;
            else if (format == "ogg") audioType = AudioType.OGGVORBIS;
            else if (format == "wav") audioType = AudioType.WAV;
            else audioType = AudioType.UNKNOWN;
            using (UnityWebRequest www = UnityWebRequestMultimedia.GetAudioClip(currentUrl, audioType))
            {
                yield return www.SendWebRequest();

                if (www.isHttpError || www.isNetworkError)
                {
                    Debug.Log(www.error);
                }
                else
                {
                    AudioClip myClip = DownloadHandlerAudioClip.GetContent(www);
                    AudioSource audioSource = audioPlayerObject.GetComponent<AudioSource>();
                    audioSource.clip = myClip;
                    audioSource.Play();
                }
            }
            isPlayingAudio = true;
        }
    }

    public IEnumerator PlaySimulation(string idContent)
    {
        if (!isShowingSimulation)
        {
            isShowingSimulation = true;
            currentUrl = string.Empty;
            StartCoroutine(GetUrlDetails(idContent));
            while (currentUrl == string.Empty) yield return null;
            string extraData = currentUrl.Substring(currentUrl.IndexOf('?') + 1);
            string assetName = GetDataFromString("mainassetname", extraData);
            //todo change assetName to idContent below
            string assetBundelUrl = Path.Combine(PlayerPrefs.GetString(Constants.APPLICATION_DIRECTORY_PATH), "Tag" + assetName);
            PlayerPrefs.SetString(Constants.ASSET_BUNDLE_URL, assetBundelUrl);
            Debug.Log(assetBundelUrl);
            if (!File.Exists(assetBundelUrl))
            {
                string assetUrl = currentUrl.Substring(0, currentUrl.IndexOf('?'));
                UnityWebRequest www = UnityWebRequest.Get(assetUrl);
                yield return www.SendWebRequest();
                File.WriteAllBytes(assetBundelUrl, www.downloadHandler.data);
            }
            AssetBundle bundle = AssetBundle.LoadFromFile(assetBundelUrl);
            yield return bundle;
            var mainPrefab = bundle.LoadAsset<GameObject>(assetName);
            mainInstance = Instantiate(mainPrefab) as GameObject;
            string libraryName = GetDataFromString("mainlibraryname", extraData);
            PlayerPrefs.SetString(Constants.LIBRARY_NAME, libraryName);
            TextAsset txt = bundle.LoadAsset(libraryName, typeof(TextAsset)) as TextAsset;
            byte[] allBytes = txt.bytes;
            System.Reflection.Assembly mainAssembly = System.Reflection.Assembly.Load(txt.bytes);
            var type = mainAssembly.GetType("MainLoader");
            mainInstance.AddComponent(type);
            string orientationType = GetDataFromString("oriType", extraData);
            if (orientationType.ToLower() == "portrait") Screen.orientation = ScreenOrientation.Portrait;
            else Screen.orientation = ScreenOrientation.LandscapeLeft;
            bundle.Unload(false);
        }
    }

    private string GetDataFromString(string key, string data)
    {
        string[] values = data.Split('&');
        for (int i = 0; i < values.Length; i++)
        {
            var pairs = values[i].Split('=');
            if (pairs[0] == key) return pairs[1];
        }
        return "";
    }

    private void OnBackPressed()
    {
        if (videoPlayerObject.activeSelf || videoPlayerRawImage.GetComponent<VideoPlayer>().isPlaying)
        {
            StopCoroutine(videoIenum);
            OnFinishedPlayingVideo();
        }
        else if (audioPlayerObject.activeSelf || audioPlayerObject.GetComponent<AudioSource>().isPlaying)
        {
            StopCoroutine(audioIenum);
            OnFinishedPlayingAudio();
        }
        else if (isShowingSimulation)
        {
            OnFinishedShowingSimulation();
        }
        else if (!isShowingSimulation && !isPlayingAudio && !isPlayingVideo)
        {
            //NativeCallsClass.GoBack(string.Empty);

            Application.Quit();
        }
    }

    private IEnumerator GetUrlDetails(string idContent)
    {
        string _toSend = Constants.BASE_URL + Constants.CONTENT_URL + "?" +
                           Constants.ID_CONTENT + "=" + idContent;
        
        UnityWebRequest request = UnityWebRequest.Get(_toSend);
        request.SetRequestHeader("Set-cookie", string.Format("ci_session={0}", PlayerPrefs.GetString(Constants.AUTH_TOKEN)));
        yield return request.SendWebRequest();
        if (request.responseCode == 200)
        {
            BasicNavigation.ContentModel[] content = BasicNavigation.JsonHelper.getJsonArray<BasicNavigation.ContentModel>(request.downloadHandler.text);
            //todo delete
            //currentUrl = content[0].Url;
            currentUrl = idContent;
        }
        else if (request.responseCode == 401)
        {
            string toSend1 = Constants.BASE_URL + Constants.LOGIN_URL + "?" + Constants.LOGIN_ID + "=" + PlayerPrefs.GetString(Constants.LOGIN_ID) + "&" +
                              Constants.PASSWORD_HASH + "=" + PlayerPrefs.GetString(Constants.PASSWORD_HASH) + "&" +
                              Constants.CLIENT_ACCESSKEY + "=" + PlayerPrefs.GetString(Constants.CLIENT_ACCESSKEY);
            UnityWebRequest req = UnityWebRequest.Get(toSend1);
            yield return req.SendWebRequest();
            if (req.responseCode == 200)
            {
                BasicNavigation.LoginModel login = JsonUtility.FromJson<BasicNavigation.LoginModel>(req.downloadHandler.text);
                if (login.Status == Constants.SUCCESS)
                {
                    PlayerPrefs.SetString(Constants.AUTH_TOKEN, login.AuthToken);
                    UnityWebRequest unityWebRequest = UnityWebRequest.Get(_toSend);
                    unityWebRequest.SetRequestHeader("Set-cookie", string.Format("ci_session={0}", login.AuthToken));
                    yield return unityWebRequest.SendWebRequest();
                    if (unityWebRequest.responseCode == 200)
                    {
                        BasicNavigation.ContentModel[] content = BasicNavigation.JsonHelper.getJsonArray<BasicNavigation.ContentModel>(request.downloadHandler.text);
                        //todo delete
                        //currentUrl = content[0].Url;
                        currentUrl = idContent;
                    }
                }
            }
        }
    }

    public void PlayVideoNow(string idContent)
    {
        videoIenum = PlayVideo(idContent);
        StartCoroutine(videoIenum);
    }

    public void PlayAudioNow(string idContent)
    {
        audioIenum = PlayAudio(idContent);
        StartCoroutine(audioIenum);
    }
   
    public void ChangeTracking(int index,bool status)
    {
        trackingStates[index] = status;
        bool finalStatus = false;
        for(int i = 0; i < trackingStates.Count; i++)
        {
            finalStatus = finalStatus || trackingStates[i];
        }
        finalStatus = finalStatus || isShowingSimulation || isPlayingAudio || isPlayingVideo;
        scanImageObject.SetActive(!finalStatus);
    }

    #endregion

}
