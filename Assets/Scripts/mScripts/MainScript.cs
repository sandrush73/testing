﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        if(PlayerPrefs.GetString(Constants.IS_CHALLENGE) == Constants.YES)
        {
            GetComponent<ChallengeScript>().enabled = true;
        }
        else
        {
            GetComponent<BasicNavigation>().enabled = true;
        }
    }

}
