﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ChallengeScript : MonoBehaviour
{
    public GameObject portraitCanas, landscapeCanvas;
    public Button moreBtn_p, heartBtn_p, moreBtn_l, heartBtn_l;
    public Button goBackBtn_p, goBackBtn_l;
    public TextMeshProUGUI contentNameText_p, contentNameText_l;
    public GameObject quitObject_p, quitObject_l;
    public GameObject loadingObject_p, loadingObject_l;
    public Button startBtn_p, startBtn_l;
    public GameObject timerObject_p, timerObject_l;
    public Button cancelQuit_p, cancelQuit_l, okayQuit_p, okayQuit_l;
    public GameObject resultObject_p, resultObject_l;
    public Slider resultSlider_p, resultSlider_l;
    public Button replayBtn_p, replayBtn_l, quitBtn_p, quitBtn_l;
    public bool isPinned;
    public PlaneManager planeManager;
    public GameObject anchorStage;
    public ImageTargetScript imageTarget;

    private BasicNavigation.ContentUsage latestResult;
    private bool isPlaying = false;
    private bool hasLevelLoaded = false;
    private bool goodExit = true;
    private string startTime, endTime;
    private GameObject mainInstance, canvasInstance;
    private IEnumerator getActualContent;
    private IEnumerator countTimer;
    private BasicNavigation.CURRENT_MODE currentOrientation;
    // Start is called before the first frame update
    void Start()
    {
        string orientationType = PlayerPrefs.GetString(Constants.SCREEN_ORIENTATION);

        if (orientationType.ToLower() == "portrait")
        {
            Screen.orientation = ScreenOrientation.Portrait;
            currentOrientation = BasicNavigation.CURRENT_MODE.PORTRAIT;
            portraitCanas.SetActive(true);
            landscapeCanvas.SetActive(false);
            loadingObject_p.SetActive(true);
            timerObject_p.SetActive(false);
            moreBtn_p.gameObject.SetActive(false);
            heartBtn_p.gameObject.SetActive(false);
        }
        else
        {
            Screen.orientation = ScreenOrientation.LandscapeLeft;
            currentOrientation = BasicNavigation.CURRENT_MODE.LANDSCAPE;
            portraitCanas.SetActive(false);
            landscapeCanvas.SetActive(true);
            loadingObject_l.SetActive(true);
            timerObject_l.SetActive(false);
            moreBtn_l.gameObject.SetActive(false);
            heartBtn_l.gameObject.SetActive(false);
        }

        goBackBtn_p.onClick.AddListener(() => QuitShowHide(true));
        goBackBtn_l.onClick.AddListener(() => QuitShowHide(true));
        cancelQuit_p.onClick.AddListener(() => QuitShowHide(false));
        cancelQuit_l.onClick.AddListener(() => QuitShowHide(false));
        okayQuit_p.onClick.AddListener(() => GoBackNow());
        okayQuit_l.onClick.AddListener(() => GoBackNow());
        startBtn_p.onClick.AddListener(() => StartChallenge());
        startBtn_l.onClick.AddListener(() => StartChallenge());
        replayBtn_p.onClick.AddListener(() => StartChallenge());
        replayBtn_l.onClick.AddListener(() => StartChallenge());
        quitBtn_p.onClick.AddListener(() => OnlyQuit());
        quitBtn_l.onClick.AddListener(() => OnlyQuit());
        getActualContent = GetActualContent();
        StartCoroutine(getActualContent);
    }

    private void GoBackNow()
    {
        goodExit = true;
        endTime = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
        BasicNavigation.ContentUsage c = new BasicNavigation.ContentUsage
        {
            idContent = PlayerPrefs.GetString(Constants.ID_CONTENT),
            startTime = startTime,
            endTime = endTime,
            actualDuration = 0,
            lastPosition = 0,
            idLevel = "0",
            type = isPinned ? Constants.TYPE_PIN : Constants.TYPE_SIMULATION
        };
        BasicNavigation.IdContentModel id = new BasicNavigation.IdContentModel
        {
            idContent = c.idContent
        };
        StartCoroutine(NativeCallsClass.PostEventLog(c.startTime, c.endTime, c.type, id.ToString()));
        NativeCallsClass.SaveContentUsage(c, false);
        if (mainInstance != null)
        {
            AssetBundle.UnloadAllAssetBundles(true);
            Destroy(mainInstance);
            Application.Quit();
        }
    }

    private void OnlyQuit()
    {
        goodExit = true;
        if (mainInstance != null)
        {
            AssetBundle.UnloadAllAssetBundles(true);
            Destroy(mainInstance);
        }
        Application.Quit();
    }

    private void QuitShowHide(bool value)
    {
        if (hasLevelLoaded)
        {
            if (currentOrientation == BasicNavigation.CURRENT_MODE.PORTRAIT)
            {
                quitObject_p.SetActive(value);
            }
            else
            {
                quitObject_l.SetActive(value);
            }
        }
        else
        {
            goodExit = true;
            StopCoroutine(getActualContent);
            Application.Quit();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            QuitShowHide(true);
        }   
    }

    IEnumerator GetActualContent()
    {
        if (mainInstance != null)
        {
            AssetBundle.UnloadAllAssetBundles(true);
            Destroy(mainInstance);
        }

        string contentName = PlayerPrefs.GetString(Constants.CONTENT_NAME);
        //Debug.Log(contentName);
        string assetBundleUrl = PlayerPrefs.GetString(Constants.ASSET_BUNDLE_URL);
        //Debug.Log(assetBundleUrl);
        string assetName = PlayerPrefs.GetString(Constants.ASSET_NAME);
        //Debug.Log(assetName);
        string libraryName = PlayerPrefs.GetString(Constants.LIBRARY_NAME);
        //Debug.Log(libraryName);

        contentNameText_p.text = contentName;
        contentNameText_l.text = contentName;

        AssetBundle bundle = AssetBundle.LoadFromFile(assetBundleUrl);
        yield return bundle;
        string[] allBundleAssets = bundle.GetAllAssetNames();

        foreach (string a in allBundleAssets) Debug.Log(a);
        var mainPrefab = bundle.LoadAsset<GameObject>(assetName);
        mainInstance = Instantiate(mainPrefab) as GameObject;
        Renderer[] _childRenderers = mainInstance.GetComponentsInChildren<Renderer>();
        foreach (Renderer r in _childRenderers)
        {
            r.material.shader = Shader.Find(r.material.shader.name);
        }
        //TextAsset txt = bundle.LoadAsset(libraryName, typeof(TextAsset)) as TextAsset;
        //byte[] allBytes = txt.bytes;
        //System.Reflection.Assembly mainAssembly = System.Reflection.Assembly.Load(txt.bytes);
        //var type = mainAssembly.GetType("MainLoader");
        //mainInstance.AddComponent(type);

        if (isPinned)
        {
            if (SceneManager.GetActiveScene().buildIndex == 3)
            {
                mainInstance.transform.parent = anchorStage.transform;
                planeManager.planeAugmentation = mainInstance;
                mainInstance.SetActive(false);
            }
            else
            {
                mainInstance.SetActive(false);
                mainInstance.transform.parent = transform;
                mainInstance.transform.localScale = Vector3.one;
                imageTarget.augmentationObject = mainInstance;
            }
        }
        hasLevelLoaded = true;
        if(currentOrientation == BasicNavigation.CURRENT_MODE.PORTRAIT)
        {
            startBtn_p.gameObject.SetActive(true);
            startBtn_p.gameObject.GetComponent<Animator>().Play("startBtn");
        }
        else
        {
            startBtn_l.gameObject.SetActive(true);
            startBtn_l.gameObject.GetComponent<Animator>().Play("startBtn");
        }
        bundle.Unload(false);
    }

    private void StartChallenge()
    {
        mainInstance.SetActive(true);
        mainInstance.SendMessage("OnLevelStart");
        if(currentOrientation == BasicNavigation.CURRENT_MODE.PORTRAIT)
        {
            loadingObject_p.SetActive(false);
            resultObject_p.SetActive(false);
            //timerObject_p.SetActive(true);
            //timerObject_p.GetComponent<Animator>().Play("timerShow");
        }
        else
        {
            loadingObject_l.SetActive(false);
            resultObject_l.SetActive(false);
            //timerObject_l.SetActive(true);
            //timerObject_l.GetComponent<Animator>().Play("timerShow");
        }
    }

    private IEnumerator CountTimer()
    {
        TextMeshProUGUI timerText;
        if(currentOrientation == BasicNavigation.CURRENT_MODE.PORTRAIT)
        {
            timerText = timerObject_p.GetComponentInChildren<TextMeshProUGUI>();
        }
        else
        {
            timerText = timerObject_l.GetComponentInChildren<TextMeshProUGUI>();
        }
        int totalSeconds = 0;
        while (isPlaying)
        {
            yield return new WaitForSeconds(1);
            totalSeconds++;
            int hours = totalSeconds / 3600;
            int mins = (totalSeconds % 3600) / 60;
            int sec = ((totalSeconds % 3600) % 60);
            string hourString = (hours < 10) ? "0" : "";
            hourString += hours.ToString();
            string minString = (mins < 10) ? "0" : "";
            minString += mins.ToString();
            string secString = (sec < 10) ? "0" : "";
            secString += sec.ToString();
            timerText.text = hourString + ":" + minString + ":" + secString;
        }
    }

    private IEnumerator ShowProgress()
    {
        Slider main = currentOrientation == BasicNavigation.CURRENT_MODE.PORTRAIT ? resultSlider_p : resultSlider_l;
        Debug.Log(latestResult.lastPosition);
        for(int i = 0; i <= latestResult.lastPosition; i++)
        {
            main.value = i / 100.0f;
            yield return null;
        }
    }

    public void NowInstantiateARPin()
    {
        //GetComponent<HelloARController>().NowInstantiate(mainInstance);
        canvasInstance.SetActive(true);
    }

    public void StartChallengeNow()
    {
        isPlaying = true;
        if (currentOrientation == BasicNavigation.CURRENT_MODE.PORTRAIT)
        {
            timerObject_p.SetActive(true);
            timerObject_p.GetComponent<Animator>().Play("timerShow");
        }
        else
        {
            timerObject_l.SetActive(true);
            timerObject_l.GetComponent<Animator>().Play("timerShow");
        }
        countTimer = CountTimer();
        StartCoroutine(countTimer);
        startTime = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
    }

    public void OnLevelSubmit(string arg)
    {
        if (isPlaying)
        {
            isPlaying = false;
            timerObject_p.SetActive(false);
            timerObject_l.SetActive(false);
            ChallengeResultClass challenge = JsonUtility.FromJson<ChallengeResultClass>(arg);
            endTime = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
            BasicNavigation.ContentUsage contentUsage = new BasicNavigation.ContentUsage
            {
                idContent = PlayerPrefs.GetString(Constants.ID_CONTENT),
                startTime = startTime,
                endTime = endTime,
                actualDuration = Int32.Parse(challenge.actualTime),
                lastPosition = Int32.Parse(challenge.percentageComplete),
                idLevel = challenge.levelMode,
                type = isPinned ? Constants.TYPE_PIN : Constants.TYPE_SIMULATION
            };
            NativeCallsClass.SaveContentUsage(contentUsage, false);
            latestResult = contentUsage;
            if (PlayerPrefs.GetString(Constants.NETWORK_CONNECTED) == "T")
            {
                //todo send for result
            }
        }
    }

    public void OnLevelResult()
    {
        mainInstance.SetActive(false);
        string result = (latestResult.lastPosition > 80) ? "HOORAY" : (latestResult.lastPosition > 50 ? "YAYY" : "OOPS");
        string animName = (latestResult.lastPosition > 80) ? "happy" : (latestResult.lastPosition > 50 ? "angry" : "sad");
        if (currentOrientation == BasicNavigation.CURRENT_MODE.PORTRAIT)
        {
            resultObject_p.SetActive(true);
            resultObject_p.transform.Find("Main/GameObject/TitleText").GetComponent<TextMeshProUGUI>().text = PlayerPrefs.GetString(Constants.CONTENT_NAME);
            resultObject_p.transform.Find("Main/GameObject/Slider/Fill Area/Fill/text").GetComponent<TextMeshProUGUI>().text = latestResult.lastPosition + "%";
            resultObject_p.transform.Find("Main/GameObject/TimerObject/TimeTaken").GetComponent<TextMeshProUGUI>().text = latestResult.actualDuration.ToString();        
        }
        else
        {
            resultObject_l.SetActive(true);
            resultObject_l.transform.Find("Main/GameObject/TitleText").GetComponent<TextMeshProUGUI>().text = PlayerPrefs.GetString(Constants.CONTENT_NAME);
            resultObject_l.transform.Find("Main/GameObject/Slider/Fill Area/Fill/text").GetComponent<TextMeshProUGUI>().text = latestResult.lastPosition + "%";
            resultObject_l.transform.Find("Main/GameObject/TimerObject/TimeTaken").GetComponent<TextMeshProUGUI>().text = latestResult.actualDuration.ToString();
        }
        StartCoroutine(ShowProgress());
    }

    private void OnApplicationQuit()
    {
        if (!goodExit)
        {
            if (hasLevelLoaded)
            {
                endTime = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
                BasicNavigation.ContentUsage c = new BasicNavigation.ContentUsage
                {
                    idContent = PlayerPrefs.GetString(Constants.ID_CONTENT),
                    startTime = startTime,
                    endTime = endTime,
                    actualDuration = 0,
                    lastPosition = 0,
                    idLevel = "0",
                    type = isPinned ? Constants.TYPE_PIN : Constants.TYPE_SIMULATION
                };
                NativeCallsClass.SaveContentUsage(c, false);
            }
        }
    }


    private void OnLevelSubmitTest()
    {
        ChallengeResultClass c = new ChallengeResultClass
        {
            levelMode = "2",
            percentageComplete = "85",
            actualTime = "95"
        };
        OnLevelSubmit(JsonUtility.ToJson(c));
        Invoke("OnLevelResult", 1);
    }

    public void TestResultSend()
    {
        StartChallengeNow();
        Invoke("OnLevelSubmitTest", 3);
    }

    [Serializable]
    public class ChallengeResultClass
    {
        public string levelMode;
        public string percentageComplete;
        public string actualTime;
    }
}
