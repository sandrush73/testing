﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class MarkerAccScript : MonoBehaviour
{
    public TextMeshProUGUI text;
    // Start is called before the first frame update
    void Start()
    {
        Screen.orientation = ScreenOrientation.Portrait;
    }

    // Update is called once per frame
    void Update() // runs 60 fps or so
    {
        // find speed based on delta
        float curSpeedx = -45;
        //float curSpeedy = 30;
        Vector3 localRotation = transform.localRotation.eulerAngles;
        // first update the current rotation angles with input from acceleration axis
        //localRotation.y = -1 * Input.acceleration.x * curSpeedy;

        localRotation.x = curSpeedx * Input.acceleration.y + curSpeedx;
    

        text.text = Input.acceleration.x + " " + Input.acceleration.y + " ";
        text.text += localRotation.x + " " + localRotation.y;

        // then rotate this object accordingly to the new angle
        transform.localRotation = Quaternion.Euler(localRotation);

    }
}
