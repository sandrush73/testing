﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Vuforia;

public class ImageTargetScript : MonoBehaviour, ITrackableEventHandler
{
    bool hasInitialized;
    public BasicNavigation basic;
    public GameObject augmentationObject;
    public GameObject scanObject_p,scanObject_l;
    protected TrackableBehaviour mTrackableBehaviour;

    private void Start()
    {
        mTrackableBehaviour = GetComponent<TrackableBehaviour>();
        if (mTrackableBehaviour)
            mTrackableBehaviour.RegisterTrackableEventHandler(this);

        string orientationType = PlayerPrefs.GetString(Constants.SCREEN_ORIENTATION);
        if (orientationType.ToLower() == "portrait")
        {
            scanObject_l.SetActive(false);
        }
        else
        {
            scanObject_p.SetActive(false);
        }
    }

    public void OnTrackableStateChanged(TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
    {
        if (newStatus == TrackableBehaviour.Status.DETECTED || newStatus == TrackableBehaviour.Status.TRACKED || newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            scanObject_p.SetActive(false);
            scanObject_l.SetActive(false);
            if (!hasInitialized)
            {
                hasInitialized = true;
                augmentationObject.SetActive(true);
                augmentationObject.transform.parent = null;
                basic.NowInstantiateARPin();
            }
            //cloudImageScript.ChangeTracking(index, true);
        }
        else
        {
            //scanObject.SetActive(true);
            //cloudImageScript.ChangeTracking(index, false);
        }
    }
}
