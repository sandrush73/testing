﻿using System;

[Serializable]
public class FlixContent {
    public string idContent;
    public string ContentName;
    public string Url;
    public string ThumbnailUrl;
    public string TopicName;
    public string ChapterName;
    public string SubjectId;
    public string SubjectName;

    FlixContent(string idContent,string ContentName,string Url,string ThumbnailUrl,string TopicName,string ChapterName,string SubjectId,string SubjectName)
    {
        this.idContent = idContent;
        this.ContentName = ContentName;
        this.Url = Url;
        this.ThumbnailUrl = ThumbnailUrl;
        this.TopicName = TopicName;
        this.ChapterName = ChapterName;
        this.SubjectId = SubjectId;
        this.SubjectName = SubjectName;
    }
}
