﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarkerScript : MonoBehaviour
{
    public CloudImageScript cloudImageScript;
    public PdfSceneScript.Anchors _tag;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, 100))
            {
                if(hit.collider.gameObject == gameObject)
                {
                    Debug.Log("I am hit yaar");
                    switch (_tag.type)
                    {
                        case "mpg":
                            {
                                cloudImageScript.PlayVideoNow(_tag.idContent);
                                break;
                            }
                        case "audio":
                            {
                                cloudImageScript.PlayAudioNow(_tag.idContent);
                                break;
                            }
                        case "simulation":
                            {
                                StartCoroutine(cloudImageScript.PlaySimulation(_tag.idContent));
                                break;
                            }
                    }
                }
            }
        }

    }
}
