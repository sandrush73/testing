﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class ScanScript : MonoBehaviour, ITrackableEventHandler
{
    public bool hasInitialized = false;
    protected TrackableBehaviour mTrackableBehaviour;
    // Start is called before the first frame update
    void Start()
    {
        mTrackableBehaviour = GetComponentInParent<TrackableBehaviour>();
        if (mTrackableBehaviour)
            mTrackableBehaviour.RegisterTrackableEventHandler(this);
    }

    public void OnTrackableStateChanged(TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
    {
        if (newStatus == TrackableBehaviour.Status.DETECTED || newStatus == TrackableBehaviour.Status.TRACKED || newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            if (!hasInitialized)
            {
                hasInitialized = true;
                GetComponent<BasicNavigation>().NowInstantiateARPin();
            }
            GetComponent<BasicNavigation>().ShowHideScanningObject(true);
        }
        else if (previousStatus == TrackableBehaviour.Status.TRACKED && newStatus == TrackableBehaviour.Status.NO_POSE)
        {
            GetComponent<BasicNavigation>().ShowHideScanningObject(false);
        }
        else
        {
            GetComponent<BasicNavigation>().ShowHideScanningObject(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
