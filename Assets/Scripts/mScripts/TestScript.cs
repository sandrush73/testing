﻿using UnityEngine;
using System;
using TMPro;

public class TestScript : MonoBehaviour
{
    public TMP_InputField inputField;
    public int sceneNumber = 2;
    public string assetBundleName;
    public string libName;
    public string assetName;
    public bool sendChallenge = false;
    public bool portrait = true;
    public int totalXP = 12;
    public int maxEarnings = 0;

    void Start()
    {
        string ori = portrait ? "portrait" : "landscape";
        Debug.Log(ori);
        PlayerPrefs.SetString(Constants.LOGIN_ID, "demojeemainbeta1@3rdflix.com");
        PlayerPrefs.SetString(Constants.PASSWORD_HASH, "ceb6c970658f31504a901b89dcd3e461");
        PlayerPrefs.SetString(Constants.CLIENT_ACCESSKEY, "ceb6c970658f31504a901b89dcd3e461");
        PlayerPrefs.SetString(Constants.AUTH_TOKEN, "da2aa9a0add9deb2df215acdc5b7e75b875860c6");
        PlayerPrefs.SetString(Constants.ID_USER, "72878");
        PlayerPrefs.SetString(Constants.ID_ENTITY, "88");
        PlayerPrefs.SetString(Constants.ID_COURSE, "189");
        PlayerPrefs.SetString(Constants.ID_STUDENT, "71474");
        string aburl = Application.streamingAssetsPath + "/" + assetBundleName;
        string _toSend = "";
        PlayerPrefs.SetString("NetworkConnected", "T");
        if (!sendChallenge)
        {
            _toSend = sceneNumber + "|" + aburl + "|" + assetName + "|" + libName + "|Lorem Ipsum|9481|" + ori + "|524|9820|23763|"+totalXP+"|"+maxEarnings+"|";
            SendMessage("GetValuesFromNative", _toSend);
        }
        else
        {
            _toSend = sceneNumber + "|" + aburl + "|" + assetName + "|" + libName + "|Friction Force Challenge|9481|" + ori + "|";
            SendMessage("GetValuesForChallenge", _toSend);
        }
       
    }

    public void ClickButton(int index)
    {
        string txt = inputField.text;
        if(txt != "")
        {
            assetBundleName = txt;
            libName = txt;
            assetName = txt;
            portrait = false;
            sceneNumber = index;
            //Start1();
        }
    }
}



