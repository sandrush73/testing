﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Text.RegularExpressions;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class BasicNavigation : MonoBehaviour {

    private enum LIKE_STATUS
    {
        LIKE,
        NONE
    }
    private enum BOOKMARK_STATUS
    {
        BOOKMARK,
        NO_BOOKMARK
    }
    private enum PIN_STATUS
    {
        STILL_CAN,
        CAN,
        CANNOT
    }
    public enum CURRENT_MODE
    {
        PORTRAIT,
        LANDSCAPE
    }

    public GameObject LandScapeCanvas, PortraitCanvas;
    [Header("Portrait Objects")]
    public TextMeshProUGUI contentNameText_p;
    public Button homeBtn_p, likeBtn_p, moreBtn_p, commentBtn_p, recommendedBtn_p, closeRecommendationBtn_p, bookmarkBtn_p;
    public Transform relatedSims_p, relatedPins_p, relatedVids_p;
    public GameObject scrollViewRelated_p, scrollViewComments_p, moreScroll_p, toastObject_p;
    public TMP_InputField commentInputField_p;
    public Button commentNowBtn_p, cancelCommentBtn_p;
    [Header("Landscape Objects")]
    public TextMeshProUGUI contentNameText_l;
    public Button homeBtn_l, likeBtn_l, moreBtn_l, commentBtn_l, recommendedBtn_l, closeRecommendationBtn_l, bookmarkBtn_l;
    public Transform relatedSims_l, relatedPins_l, relatedVids_l;
    public GameObject scrollViewRelated_l, scrollViewComments_l, moreScroll_l, toastObject_l;
    public TMP_InputField commentInputField_l;
    public Button commentNowBtn_l, cancelCommentBtn_l;
    [Header("General")]
    public GameObject relatedSimCookieCutter_p, relatedSimCookieCutter_l;
    public Sprite likeLine, likeFill;
    public bool isPinned;

    private RelatedContentIndexModel currentModel;
    private GameObject[] commentObjects;
    private IEnumerator getActualContent;
    private float timeTakenDuringLerp = 0.2f;
    private bool _isLerpingComments, _isLerpingRelated;
    private bool isShowingScrollRelated = false, isShowingScrollComments = false, isShowingScrollMore = false;
    private Vector3 _startPositionRelated_p, _startPositionRelated_l, _startPositionComments_p, _startPositionComments_l;
    private Vector3 _endPositionRelated_p, _endPositionRelated_l, _endPositionComments_p, _endPositionComments_l;
    private float _timeStartedLerping;
    private LIKE_STATUS likeStatus;
    private PIN_STATUS pinStatus;
    private BOOKMARK_STATUS bookmarkStatus;
    private GameObject mainInstance, canvasInstance;
    private string startTime, endTime;
    private int actualTimeInSeconds, startTimeInSeconds, endTimeInSeconds;
    public static CURRENT_MODE currentOrientation = CURRENT_MODE.LANDSCAPE;
    public PlaneManager planeManager;
    public GameObject anchorStage;
    public ImageTargetScript imageTarget;
    private bool goBack = false;
    private bool unloaded = false;


    // Use this for initialization
    void Start ()
    {
        homeBtn_p.onClick.AddListener(() => StartCoroutine(GoBack()));
        homeBtn_l.onClick.AddListener(() => StartCoroutine(GoBack()));
        if (PlayerPrefs.GetString(Constants.NETWORK_CONNECTED) == "T")
        {
            likeBtn_p.onClick.AddListener(() => Like());
            likeBtn_l.onClick.AddListener(() => Like());
            moreBtn_p.onClick.AddListener(() =>
            {
                isShowingScrollMore = !isShowingScrollMore;
                moreScroll_p.SetActive(isShowingScrollMore);
            });
            moreBtn_l.onClick.AddListener(() =>
            {
                isShowingScrollMore = !isShowingScrollMore;
                moreScroll_l.SetActive(isShowingScrollMore);
            });
            recommendedBtn_p.onClick.AddListener(() => ShowHideRelated());
            recommendedBtn_l.onClick.AddListener(() => ShowHideRelated());
            closeRecommendationBtn_p.onClick.AddListener(() => ShowHideRelated());
            closeRecommendationBtn_l.onClick.AddListener(() => ShowHideRelated());
            commentBtn_p.onClick.AddListener(() => ShowHideComments());
            commentBtn_l.onClick.AddListener(() => ShowHideComments());
            bookmarkBtn_p.onClick.AddListener(() => StartCoroutine(Bookmark()));
            bookmarkBtn_l.onClick.AddListener(() => StartCoroutine(Bookmark()));
            cancelCommentBtn_p.onClick.AddListener(() => ShowHideComments());
            cancelCommentBtn_l.onClick.AddListener(() => ShowHideComments());
            commentNowBtn_p.onClick.AddListener(() => StartCoroutine(CommentNow()));
            commentNowBtn_l.onClick.AddListener(() => StartCoroutine(CommentNow()));

            if (!isPinned)
            {
                //StartCoroutine(CheckCompatibility());
            }

            _startPositionRelated_p = scrollViewRelated_p.transform.localPosition;
            _startPositionRelated_l = scrollViewRelated_l.transform.localPosition;
            _endPositionRelated_p = new Vector3(0, 0, 0);
            _endPositionRelated_l = new Vector3(0, 0, 0);
            _startPositionComments_p = scrollViewComments_p.transform.localPosition;
            _startPositionComments_l = scrollViewComments_l.transform.localPosition;
            _endPositionComments_p = new Vector3(0, 0, 0);
            _endPositionComments_l = new Vector3(0, 0, 0);
           
        }
        else
        {
            likeBtn_p.gameObject.SetActive(false);
            likeBtn_l.gameObject.SetActive(false);
            moreBtn_p.gameObject.SetActive(false);
            moreBtn_l.gameObject.SetActive(false);
        }

        getActualContent = GetActualContent(null);
        StartCoroutine(getActualContent);
    }

    /*private IEnumerator PopulateScrollForComments()
    {
        string toSend = Constants.BASE_URL + Constants.USER_POST + "?" +
                   Constants.ID_USER + "=" + PlayerPrefs.GetString(Constants.ID_USER) + "&" +
                   Constants.ID_COURSE + "=" + PlayerPrefs.GetString(Constants.ID_COURSE)+ "&" +
                   Constants.ID_CONTENT + "=" + PlayerPrefs.GetString(Constants.ID_CONTENT) + "&" +
                   Constants.TYPE + "=\"" + Constants.ALL_POSTS + "\"";
        Debug.Log(toSend);
        UnityWebRequest request = UnityWebRequest.Get(toSend);
        request.SetRequestHeader("Set-cookie", string.Format("ci_session={0}", PlayerPrefs.GetString(Constants.AUTH_TOKEN)));
        yield return request.SendWebRequest();
        if (request.responseCode == 200)
        {
            Debug.Log(request.downloadHandler.text);
            PopulateComments(request.downloadHandler.text);
        }
        else if(request.responseCode == 401)
        {
            string toSend1 = Constants.BASE_URL + Constants.LOGIN_URL + "?" + Constants.LOGIN_ID + "=" + PlayerPrefs.GetString(Constants.LOGIN_ID) + "&" +
                              Constants.PASSWORD_HASH + "=" + PlayerPrefs.GetString(Constants.PASSWORD_HASH) + "&" +
                              Constants.CLIENT_ACCESSKEY + "=" + PlayerPrefs.GetString(Constants.CLIENT_ACCESSKEY);
            UnityWebRequest req = UnityWebRequest.Get(toSend1);
            yield return req.SendWebRequest();
            if (req.responseCode == 200)
            {
                LoginModel login = JsonUtility.FromJson<LoginModel>(req.downloadHandler.text);
                if (login.Status == Constants.SUCCESS)
                {
                    PlayerPrefs.SetString(Constants.AUTH_TOKEN, login.AuthToken);
                    UnityWebRequest unityWebRequest = UnityWebRequest.Get(toSend);
                    unityWebRequest.SetRequestHeader("Set-cookie", string.Format("ci_session={0}", login.AuthToken));
                    yield return unityWebRequest.SendWebRequest();
                    if (unityWebRequest.responseCode == 200)
                    {
                        Debug.Log(unityWebRequest.downloadHandler.text);
                        PopulateComments(unityWebRequest.downloadHandler.text);
                    }
                }
            }
        }
    }*/

    private void ShowHideRelated()
    {
        if (isShowingScrollComments && !isShowingScrollRelated)
        {
            _isLerpingComments = true;
        }
        if (!isShowingScrollRelated)
        {
            isShowingScrollMore = !isShowingScrollMore;
            moreScroll_p.SetActive(isShowingScrollMore);
            moreScroll_l.SetActive(isShowingScrollMore);
        }
        _isLerpingRelated = true;
        _timeStartedLerping = Time.time;
    }

    private void ShowHideComments()
    {
        if (isShowingScrollRelated && !isShowingScrollComments)
        {
            _isLerpingRelated = true;
        }
        if (!isShowingScrollComments)
        {
            isShowingScrollMore = !isShowingScrollMore;
            moreScroll_p.SetActive(isShowingScrollMore);
            moreScroll_l.SetActive(isShowingScrollMore);
        }
        _isLerpingComments = true;
        _timeStartedLerping = Time.time;
    }

    void FixedUpdate()
    {
        if (_isLerpingRelated)
        {
            float timeSinceStarted = Time.time - _timeStartedLerping;
            float percentageComplete = timeSinceStarted / timeTakenDuringLerp;

            if (!isShowingScrollRelated)
            {
                scrollViewRelated_p.transform.localPosition = Vector3.Lerp(_startPositionRelated_p, _endPositionRelated_p, percentageComplete);
                scrollViewRelated_l.transform.localPosition = Vector3.Lerp(_startPositionRelated_l, _endPositionRelated_l, percentageComplete);
            }
            else
            {
                scrollViewRelated_p.transform.localPosition = Vector3.Lerp(_endPositionRelated_p, _startPositionRelated_p, percentageComplete);
                scrollViewRelated_l.transform.localPosition = Vector3.Lerp(_endPositionRelated_l, _startPositionRelated_l, percentageComplete);
            }

            if (percentageComplete >= 1.0f)
            {
                _isLerpingRelated = false;
                isShowingScrollRelated = !isShowingScrollRelated;
            }
        }
        if (_isLerpingComments)
        {
            float timeSinceStarted = Time.time - _timeStartedLerping;
            float percentageComplete = timeSinceStarted / timeTakenDuringLerp;

            if (!isShowingScrollComments)
            {
                scrollViewComments_p.transform.localPosition = Vector3.Lerp(_startPositionComments_p, _endPositionComments_p, percentageComplete);
                scrollViewComments_l.transform.localPosition = Vector3.Lerp(_startPositionComments_l, _endPositionComments_l, percentageComplete);
            }
            else
            {
                scrollViewComments_p.transform.localPosition = Vector3.Lerp(_endPositionComments_p, _startPositionComments_p, percentageComplete);
                scrollViewComments_l.transform.localPosition = Vector3.Lerp(_endPositionComments_l, _startPositionComments_l, percentageComplete);
            }

            if (percentageComplete >= 1.0f)
            {
                _isLerpingComments = false;
                isShowingScrollComments = !isShowingScrollComments;
            }
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            StartCoroutine(GoBack());
        }
    }

    void Like()
    {
        string ratingString = "Delete";
        switch (likeStatus)
        {
            case LIKE_STATUS.LIKE:
            {
                likeBtn_p.GetComponent<Image>().sprite = likeLine;
                likeBtn_l.GetComponent<Image>().sprite = likeLine;
                likeStatus = LIKE_STATUS.NONE;  
                ratingString = "0";         
                break;
            }
            case LIKE_STATUS.NONE:
            {
                likeBtn_p.GetComponent<Image>().sprite = likeFill;
                likeBtn_l.GetComponent<Image>().sprite = likeFill;
                likeStatus = LIKE_STATUS.LIKE;
                ratingString = "9";
                break;
            }
        }
        StartCoroutine(PostLikeAndStuff(ratingString));
    }

    public void UnloadActually(float percentageComplete)
    {
        Debug.Log("UNLOAD ACTUALLY CALLED " + percentageComplete.ToString());
        unloaded = true;
        if (mainInstance != null)
        {
            AssetBundle.UnloadAllAssetBundles(true);
            Destroy(mainInstance);
        }
        endTime = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
        actualTimeInSeconds = Mathf.RoundToInt(Time.time - startTimeInSeconds);
        ContentUsage c = new ContentUsage
        {
            idContent = PlayerPrefs.GetString(Constants.ID_CONTENT),
            startTime = startTime,
            endTime = endTime,
            actualDuration = actualTimeInSeconds,
            lastPosition = (int) Mathf.Floor(percentageComplete),
            PerformanceScore = (int) Mathf.Floor(percentageComplete),
            idLevel = "0",
            type = isPinned ? Constants.TYPE_PIN : Constants.TYPE_SIMULATION
        };
        IdContentModel id = new IdContentModel
        {
            idContent = c.idContent
        };
        StartCoroutine(NativeCallsClass.PostEventLog(c.startTime, c.endTime, c.type, id.ToString()));
        NativeCallsClass.SaveContentUsage(c, goBack);
    }

    IEnumerator GoBack()
    {
        goBack = true;
        GameObject.Find("MainObject").SendMessage("GetResult");
        yield return new WaitForSeconds(3);
        if (!unloaded)
        {
            UnloadActually(0);
        }
    }

    IEnumerator Bookmark()
    {
        string isBookmarkedYN = "Y";
        switch (bookmarkStatus)
        {
            case BOOKMARK_STATUS.BOOKMARK:
                {
                    bookmarkStatus = BOOKMARK_STATUS.NO_BOOKMARK;
                    bookmarkBtn_p.GetComponent<TextMeshProUGUI>().text = Constants.BOOKMARK;
                    bookmarkBtn_l.GetComponent<TextMeshProUGUI>().text = Constants.BOOKMARK;
                    isBookmarkedYN = "N";
                    break;
                }
            case BOOKMARK_STATUS.NO_BOOKMARK:
                {
                    bookmarkStatus = BOOKMARK_STATUS.BOOKMARK;
                    bookmarkBtn_p.GetComponent<TextMeshProUGUI>().text = Constants.NO_BOOKMARK;
                    bookmarkBtn_l.GetComponent<TextMeshProUGUI>().text = Constants.NO_BOOKMARK;
                    isBookmarkedYN = "Y";
                    break;
                }
        }
        yield return null;
        string _toSend = Constants.BASE_URL + Constants.CONTENT_BOOKMARK;
        Debug.Log(_toSend);
        BookmarkContentModel bookmarkContent = new BookmarkContentModel
        {
            idContent = PlayerPrefs.GetString(Constants.ID_CONTENT),
            IsBookmarkedYN = isBookmarkedYN
        };
        PostBookmarkModel postBookmarkModel = new PostBookmarkModel
        {
            idStudent = PlayerPrefs.GetString(Constants.ID_STUDENT),
            Contents = new BookmarkContentModel[1] { bookmarkContent }
        };
        Dictionary<string, string> d = new Dictionary<string, string>
        {
            { "Upsert", JsonUtility.ToJson(postBookmarkModel) }
        };
        Debug.Log(d.ToString());

        UnityWebRequest unityWebRequest = UnityWebRequest.Post(_toSend, d);
        unityWebRequest.SetRequestHeader("Set-cookie", string.Format("ci_session={0}", PlayerPrefs.GetString(Constants.AUTH_TOKEN))); yield return unityWebRequest.SendWebRequest();

        Debug.Log("Response Code: " + unityWebRequest.responseCode.ToString());

        if (unityWebRequest.responseCode == 200)
        {
            Debug.Log(bookmarkStatus);

            if (bookmarkStatus == BOOKMARK_STATUS.BOOKMARK)
            {
                toastObject_p.GetComponentInChildren<TextMeshProUGUI>().text = Constants.CONTENT_BOOKMARKED;
                toastObject_l.GetComponentInChildren<TextMeshProUGUI>().text = Constants.CONTENT_BOOKMARKED;
            }
            else
            {
                toastObject_p.GetComponentInChildren<TextMeshProUGUI>().text = Constants.CONTENT_REMOVED_BOOKMARK;
                toastObject_l.GetComponentInChildren<TextMeshProUGUI>().text = Constants.CONTENT_REMOVED_BOOKMARK;
            }
            Debug.Log(currentOrientation);
            if(currentOrientation == CURRENT_MODE.PORTRAIT)
            {
                toastObject_p.SetActive(true);
            }
            else
            {
                toastObject_l.SetActive(true);
            }
            //toastObject_l.SetActive(true);
            isShowingScrollMore = !isShowingScrollMore;
            moreScroll_p.SetActive(isShowingScrollMore);
            moreScroll_l.SetActive(isShowingScrollMore);
            yield return new WaitForSeconds(5);
            toastObject_p.SetActive(false);
            toastObject_l.SetActive(false);
        }
        else if (unityWebRequest.responseCode == 401)
        {
            string toSend1 = Constants.BASE_URL + Constants.LOGIN_URL + "?" + Constants.LOGIN_ID + "=" + PlayerPrefs.GetString(Constants.LOGIN_ID) + "&" +
                              Constants.PASSWORD_HASH + "=" + PlayerPrefs.GetString(Constants.PASSWORD_HASH) + "&" +
                              Constants.CLIENT_ACCESSKEY + "=" + PlayerPrefs.GetString(Constants.CLIENT_ACCESSKEY);
            UnityWebRequest req = UnityWebRequest.Get(toSend1);
            yield return req.SendWebRequest();
            if (req.responseCode == 200)
            {
                LoginModel login = JsonUtility.FromJson<LoginModel>(req.downloadHandler.text);
                if (login.Status == Constants.SUCCESS)
                {
                    PlayerPrefs.SetString(Constants.AUTH_TOKEN, login.AuthToken);
                    UnityWebRequest request = UnityWebRequest.Get(_toSend);
                    request.SetRequestHeader("Set-cookie", string.Format("ci_session={0}", login.AuthToken));
                    yield return request.SendWebRequest();
                    if (request.responseCode == 200)
                    {
                        if (bookmarkStatus == BOOKMARK_STATUS.BOOKMARK)
                        {
                            toastObject_p.GetComponentInChildren<TextMeshProUGUI>().text = Constants.CONTENT_BOOKMARKED;
                            toastObject_l.GetComponentInChildren<TextMeshProUGUI>().text = Constants.CONTENT_BOOKMARKED;
                        }
                        else
                        {
                            toastObject_p.GetComponentInChildren<TextMeshProUGUI>().text = Constants.CONTENT_REMOVED_BOOKMARK;
                            toastObject_l.GetComponentInChildren<TextMeshProUGUI>().text = Constants.CONTENT_REMOVED_BOOKMARK;
                        }
                        if (currentOrientation == CURRENT_MODE.PORTRAIT)
                        {
                            toastObject_p.SetActive(true);
                        }
                        else
                        {
                            toastObject_l.SetActive(true);
                        }
                        isShowingScrollMore = !isShowingScrollMore;
                        moreScroll_p.SetActive(isShowingScrollMore);
                        moreScroll_l.SetActive(isShowingScrollMore);
                        yield return new WaitForSeconds(5);
                        toastObject_p.SetActive(false);
                        toastObject_l.SetActive(false);
                    }
                }
            }
        }
    }

    IEnumerator PostLikeAndStuff(string ratingStr)
    {
        string _toSend = Constants.BASE_URL + Constants.CONTENT_RATING_URL;
        PostLikeModel postLikeModel = new PostLikeModel
        {
            UpdateTime = DateTime.Now.ToString("yyyy-MM-dd HH':'mm':'ss"),
            Rating = ratingStr,
            idStudent = PlayerPrefs.GetString(Constants.ID_STUDENT),
            idContent = PlayerPrefs.GetString(Constants.ID_CONTENT)
        };
        Dictionary<string, string> d = new Dictionary<string, string>
        {
            { "Update", JsonUtility.ToJson(postLikeModel) }
        };
        Debug.Log(JsonUtility.ToJson(postLikeModel));
        UnityWebRequest unityWebRequest = UnityWebRequest.Post(_toSend, d);
        unityWebRequest.SetRequestHeader("Set-cookie", string.Format("ci_session={0}", PlayerPrefs.GetString(Constants.AUTH_TOKEN)));
        yield return unityWebRequest.SendWebRequest();
        if (unityWebRequest.responseCode == 200)
        {
            Debug.Log(unityWebRequest.downloadHandler.text);
        }
        else if (unityWebRequest.responseCode == 401)
        {
            string toSend1 = Constants.BASE_URL + Constants.LOGIN_URL + "?" + Constants.LOGIN_ID + "=" + PlayerPrefs.GetString(Constants.LOGIN_ID) + "&" +
                              Constants.PASSWORD_HASH + "=" + PlayerPrefs.GetString(Constants.PASSWORD_HASH) + "&" +
                              Constants.CLIENT_ACCESSKEY + "=" + PlayerPrefs.GetString(Constants.CLIENT_ACCESSKEY);
            UnityWebRequest req = UnityWebRequest.Get(toSend1);
            yield return req.SendWebRequest();
            if (req.responseCode == 200)
            {
                LoginModel login = JsonUtility.FromJson<LoginModel>(req.downloadHandler.text);
                if (login.Status == Constants.SUCCESS)
                {
                    PlayerPrefs.SetString(Constants.AUTH_TOKEN, login.AuthToken);
                    UnityWebRequest request = UnityWebRequest.Get(_toSend);
                    request.SetRequestHeader("Set-cookie", string.Format("ci_session={0}", login.AuthToken));
                    yield return request.SendWebRequest();
                    if (request.responseCode == 200)
                    {
                        Debug.Log(unityWebRequest.downloadHandler.text);
                    }
                }
            }
        }
    }

    IEnumerator ShowNextScene()
    {
        if (!isPinned)
        {
            if (pinStatus == PIN_STATUS.CAN)
            {
                SceneManager.LoadScene(3);
            }
            else if (pinStatus == PIN_STATUS.CANNOT)
            {
                SceneManager.LoadScene(1);
               // _ShowAndroidToastMessage(Constants.UNSUPPORTED_DEVICE);
            }
            /*else if(pinStatus == PIN_STATUS.STILL_CAN)
            {
                AsyncTask<ApkInstallationStatus> apkInstallationStatus = Session.RequestApkInstallation(false);
                yield return apkInstallationStatus;
                ApkInstallationStatus res = apkInstallationStatus.Result;
                switch (res)
                {
                    case ApkInstallationStatus.Success:
                        pinStatus = PIN_STATUS.CAN;
                        SceneManager.LoadScene(3);
                        break;
                    case ApkInstallationStatus.ErrorUserDeclined:
                        pinStatus = PIN_STATUS.STILL_CAN;
                        break;
                    default:
                        pinStatus = PIN_STATUS.CANNOT;
                        SceneManager.LoadScene(1);
                        //_ShowAndroidToastMessage(Constants.UNSUPPORTED_DEVICE);
                        break;
                }
            }*/
            yield return null;
        }
        else
        {
            SceneManager.LoadScene(2);
        }
    }

    IEnumerator PopulateScrollCategories(string idContent)
    {
        string _toSend = Constants.BASE_URL + Constants.RELATED_SUGGESTIONS_URL + "?" +
                           Constants.ID_COURSE + "=" + PlayerPrefs.GetString(Constants.ID_COURSE) + "&" +
                           Constants.ID_ENTITY + "=" + PlayerPrefs.GetString(Constants.ID_ENTITY) + "&" +
                           Constants.ID_CONTENT + "=" + idContent + "&Count=" + 30;

        Debug.Log(_toSend);
        UnityWebRequest request = UnityWebRequest.Get(_toSend);
        request.SetRequestHeader("Set-cookie", string.Format("ci_session={0}", PlayerPrefs.GetString(Constants.AUTH_TOKEN)));
        yield return request.SendWebRequest();
        Debug.Log("Request Code: " + request.responseCode.ToString());
        //Debug.Log(request.GetResponseHeader("Set-cookie"));
        if(request.responseCode == 200)
        {
            Debug.Log(request.downloadHandler.text);
            PopulateCategories(request.downloadHandler.text);
        }
        else if(request.responseCode == 401)
        {
            string toSend1 = Constants.BASE_URL + Constants.LOGIN_URL + "?" + Constants.LOGIN_ID + "=" + PlayerPrefs.GetString(Constants.LOGIN_ID) + "&" +
                              Constants.PASSWORD_HASH +"=" + PlayerPrefs.GetString(Constants.PASSWORD_HASH) + "&" +
                              Constants.CLIENT_ACCESSKEY + "=" + PlayerPrefs.GetString(Constants.CLIENT_ACCESSKEY);
            //final response_ = await http.get(toSend1);
            //Debug.Log(toSend1);
            UnityWebRequest req = UnityWebRequest.Get(toSend1);
            yield return req.SendWebRequest();
            if(req.responseCode == 200)
            {
               // Debug.Log("Login response: " + req.downloadHandler.text);
                LoginModel login = JsonUtility.FromJson<LoginModel>(req.downloadHandler.text);
                if(login.Status == Constants.SUCCESS)
                {
                   // Debug.Log("Auth token" + login.AuthToken);
                    PlayerPrefs.SetString(Constants.AUTH_TOKEN, login.AuthToken);
                    UnityWebRequest unityWebRequest = UnityWebRequest.Get(_toSend);
                    unityWebRequest.SetRequestHeader("Set-cookie", string.Format("ci_session={0}", login.AuthToken));
                    //Debug.Log(unityWebRequest.GetRequestHeader("Set-cookie"));
                    yield return unityWebRequest.SendWebRequest();
                    //Debug.Log(request.GetResponseHeader("Set-cookie"));
                    if (unityWebRequest.responseCode == 200)
                    {
                        Debug.Log(unityWebRequest.downloadHandler.text);
                        PopulateCategories(unityWebRequest.downloadHandler.text);
                    }
                }
            }
        }

    }

    IEnumerator StartAssetBundleDownload(GameObject overlayObj, GameObject loaderBackground, GameObject loaderForeground, GameObject checkedImage, RelatedContentIndexModel relatedContent, GameObject mainObject)
    {
        //Debug.Log("___________________" + relatedContent);
        mainObject.GetComponent<Button>().onClick.RemoveAllListeners();
        overlayObj.SetActive(true);
        loaderBackground.SetActive(true);
        loaderForeground.transform.localScale = new Vector3(0, 1, 1);
        //Debug.Log(relatedContent.Url);
        UnityWebRequest www = UnityWebRequest.Get(Constants.FILE_BASE_URL + relatedContent.Url);
        AsyncOperation request = www.SendWebRequest();

        while (!request.isDone)
        {
            loaderForeground.transform.localScale = new Vector3(request.progress, 1, 1);
            //Debug.Log(request.progress);
            yield return null;
        }

        overlayObj.SetActive(false);
        loaderBackground.SetActive(false);
        checkedImage.SetActive(true);
        File.WriteAllBytes(Path.Combine(PlayerPrefs.GetString(Constants.APPLICATION_DIRECTORY_PATH), relatedContent.idContent), www.downloadHandler.data);
        if ((isPinned && relatedContent.Type == Constants.TYPE_PIN) || (!isPinned && relatedContent.Type == Constants.TYPE_SIMULATION))
        {
            mainObject.GetComponent<Button>().onClick.AddListener(()=>{
                ShowHideRelated();
                Debug.Log("Related content" + relatedContent.TopicName);
                StopCoroutine(getActualContent);
                getActualContent = GetActualContent(relatedContent);
                StartCoroutine(getActualContent);
            });
        }
        else
            mainObject.GetComponent<Button>().onClick.AddListener(() => GoToNextScene(relatedContent));
    }

    void PopulateCategories(string text)
    {
        //Debug.Log(text);
        RelatedContentIndexModel[] relatedContent = JsonHelper.getJsonArray<RelatedContentIndexModel>(text);
        GameObject[] flixCRelatedContents = new GameObject[relatedContent.Length];
        for (int i = 0; i < relatedContent.Length; i++)
        {
            GameObject g;
            if (relatedContent[i].Type == Constants.TYPE_MPG)
            {
                if (currentOrientation == CURRENT_MODE.PORTRAIT)
                    g = Instantiate(relatedSimCookieCutter_p, relatedVids_p) as GameObject;
                else
                    g = Instantiate(relatedSimCookieCutter_l, relatedVids_l) as GameObject;
            }
            else if (relatedContent[i].Type == Constants.TYPE_PIN)
            {
                if (currentOrientation == CURRENT_MODE.PORTRAIT)
                    g = Instantiate(relatedSimCookieCutter_p, relatedPins_p) as GameObject;
                else
                    g = Instantiate(relatedSimCookieCutter_l, relatedPins_l) as GameObject;
            }
            else
            {
                if (currentOrientation == CURRENT_MODE.PORTRAIT)
                    g = Instantiate(relatedSimCookieCutter_p, relatedSims_p) as GameObject;
                else
                    g = Instantiate(relatedSimCookieCutter_l, relatedSims_l) as GameObject;
            }

            GameObject overlayImgObj = g.transform.Find("Image/Overlay").gameObject;
            GameObject downloadBackground = g.transform.Find("Image/LoaderBackGround").gameObject;
            GameObject downloadForeground = downloadBackground.transform.Find("LoaderForeground").gameObject;
            GameObject checkedImage = g.transform.Find("Image/CheckImage").gameObject;

            RelatedContentIndexModel idContent = relatedContent[i];
            if (relatedContent[i].Type == Constants.TYPE_MPG)
            {
                g.GetComponent<Button>().onClick.AddListener(() => GoToVideoYo(idContent));
                overlayImgObj.SetActive(false);
                downloadBackground.SetActive(false);
                checkedImage.SetActive(false);
            }
            else
            {
                string filePath = PlayerPrefs.GetString(Constants.APPLICATION_DIRECTORY_PATH) + "/" + relatedContent[i].idContent;
                overlayImgObj.SetActive(false);
                downloadBackground.SetActive(false);
                int x = i;
                if (!System.IO.File.Exists(filePath))
                {
                    checkedImage.SetActive(false);
                    g.GetComponent<Button>().onClick.AddListener(() => StartCoroutine(StartAssetBundleDownload(overlayImgObj, downloadBackground, downloadForeground, checkedImage, relatedContent[x], g)));
                }
                else
                {
                    g.GetComponent<Button>().onClick.AddListener(() => ShowHideRelated());
                    checkedImage.SetActive(true);
                    if ((isPinned && relatedContent[x].Type == Constants.TYPE_PIN) || (!isPinned && relatedContent[x].Type == Constants.TYPE_SIMULATION))
                    {
                        g.GetComponent<Button>().onClick.AddListener(() => StartCoroutine(GetActualContent(idContent)));
                    }
                    else
                        g.GetComponent<Button>().onClick.AddListener(() => GoToNextScene(idContent));
                }
            }         
            Image contentImage = g.GetComponentInChildren<Image>();
            TextMeshProUGUI contentText = g.GetComponentInChildren<TextMeshProUGUI>();
            contentText.text = relatedContent[i].ContentName;
            StartCoroutine(GetImage(idContent.ThumbnailImageUrl, contentImage));
        }
    }

    void GoToVideoYo(RelatedContentIndexModel flix)
    {
        PlayerPrefs.SetString("SharedPrefsValue", flix.idContent);
        PlayerPrefs.Save();
        Debug.Log("Check playerprefs: " + PlayerPrefs.GetString("SharedPrefsValue"));
        goBack = true;
        GameObject.Find("MainObject").SendMessage("GetResult");
    }

    void GoToNextScene(RelatedContentIndexModel flix)
    {
        goBack = false;
        GameObject.Find("MainObject").SendMessage("GetResult");
        //AssetBundle.UnloadAllAssetBundles(true);
        //Destroy(mainInstance);
        string contentName = flix.ContentName;
        string assetBundleUrl = Path.Combine(PlayerPrefs.GetString(Constants.APPLICATION_DIRECTORY_PATH), flix.idContent);
        string extraData = flix.Url.Substring(flix.Url.IndexOf('?') + 1);
        string assetName = GetDataFromString("mainassetname", extraData);
        string libraryName = GetDataFromString("mainlibraryname", extraData);
        string orientationType = GetDataFromString("oriType", extraData);
        PlayerPrefs.SetString(Constants.CONTENT_NAME, contentName);
        PlayerPrefs.SetString(Constants.ASSET_BUNDLE_URL, assetBundleUrl);
        PlayerPrefs.SetString(Constants.ASSET_NAME, assetName);
        PlayerPrefs.SetString(Constants.LIBRARY_NAME, libraryName);
        PlayerPrefs.SetString(Constants.SCREEN_ORIENTATION, orientationType);
        //endTime = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
        //actualTimeInSeconds = Mathf.RoundToInt(Time.time - startTimeInSeconds);
        //ContentUsage c = new ContentUsage
        //{
        //    idContent = PlayerPrefs.GetString(Constants.ID_CONTENT),
        //    startTime = startTime,
        //    endTime = endTime,
        //    actualDuration = actualTimeInSeconds,
        //    lastPosition = 0,
        //    idLevel = "0",
        //    type = isPinned ? Constants.TYPE_PIN : Constants.TYPE_SIMULATION
        //};
        //IdContentModel id = new IdContentModel
        //{
        //    idContent = c.idContent
        //};
        //StartCoroutine(NativeCallsClass.PostEventLog(c.startTime, c.endTime, c.type, id.ToString()));
        //NativeCallsClass.SaveContentUsage(c, false);
        if (flix.Type == Constants.TYPE_PIN)
        {
            if (pinStatus == PIN_STATUS.CAN)
                SceneManager.LoadScene(3);
            else
                SceneManager.LoadScene(1);
        }
        else if(flix.Type == Constants.TYPE_SIMULATION)
        {
            SceneManager.LoadScene(2);
        }
    }
    
    string GetDataFromString(string key,string data)
    {
        string[] values = data.Split('&');
        for (int i = 0; i < values.Length; i++)
        {
            var pairs = values[i].Split('=');
            if (pairs[0] == key) return pairs[1];
        }
        return "";
    }

    IEnumerator GetActualContent(RelatedContentIndexModel flixContent)
    {
        //if (mainInstance != null)
        //{
        //    AssetBundle.UnloadAllAssetBundles(true);
        //    Destroy(mainInstance);
        //}

        //if (endTime != null)
        //{
        //    endTime = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
        //    actualTimeInSeconds = Mathf.RoundToInt(Time.time - startTimeInSeconds);
        //    ContentUsage c = new ContentUsage
        //    {
        //        idContent = PlayerPrefs.GetString(Constants.ID_CONTENT),
        //        startTime = startTime,
        //        endTime = endTime,
        //        actualDuration = actualTimeInSeconds,
        //        lastPosition = 0,
        //        idLevel = "0",
        //        type = isPinned ? Constants.TYPE_PIN : Constants.TYPE_SIMULATION
        //    };
        //    IdContentModel id = new IdContentModel
        //    {
        //        idContent = c.idContent
        //    };
        //    StartCoroutine(NativeCallsClass.PostEventLog(c.startTime, c.endTime, c.type, id.ToString()));
        //    NativeCallsClass.SaveContentUsage(c, false);
        //}

        if (endTime != null)
        {
            goBack = false;
            GameObject.Find("MainObject").SendMessage("GetResult");
        }

        string contentName, assetBundleUrl, assetName, libraryName, orientationType;
        if (flixContent != null)
        {
            Debug.Log("Get actual content" + flixContent.idContent);
            contentName = flixContent.ContentName;
            PlayerPrefs.SetString(Constants.CONTENT_NAME, contentName);
            assetBundleUrl = Path.Combine(PlayerPrefs.GetString(Constants.APPLICATION_DIRECTORY_PATH), flixContent.idContent);
            PlayerPrefs.SetString(Constants.ASSET_BUNDLE_URL, assetBundleUrl);
            string extraData = flixContent.Url.Substring(flixContent.Url.IndexOf('?') + 1);
            assetName = GetDataFromString("mainassetname", extraData);
            PlayerPrefs.SetString(Constants.ASSET_NAME, assetName);
            libraryName = GetDataFromString("mainlibraryname", extraData);
            PlayerPrefs.SetString(Constants.LIBRARY_NAME, libraryName);
            orientationType = GetDataFromString("oriType", extraData);
            PlayerPrefs.SetString(Constants.SCREEN_ORIENTATION, orientationType);
            PlayerPrefs.SetString(Constants.ID_SUBJECT, flixContent.idCourseSubject);
            PlayerPrefs.SetString(Constants.ID_CHAPTER, flixContent.idCourseSubjectChapter);
            PlayerPrefs.SetString(Constants.ID_TOPIC, flixContent.idTopic);
            PlayerPrefs.SetString(Constants.ID_CONTENT, flixContent.idContent);
        }
        else
        {
            contentName = PlayerPrefs.GetString(Constants.CONTENT_NAME);
            assetBundleUrl = PlayerPrefs.GetString(Constants.ASSET_BUNDLE_URL);
            assetName = PlayerPrefs.GetString(Constants.ASSET_NAME);
            libraryName = PlayerPrefs.GetString(Constants.LIBRARY_NAME);
            orientationType = PlayerPrefs.GetString(Constants.SCREEN_ORIENTATION);
            Debug.Log(orientationType);
        }


        startTime = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
        endTime = startTime;
        startTimeInSeconds = Mathf.RoundToInt(Time.time);

        Debug.Log(contentName + " " + assetBundleUrl + " " + assetName + " " + libraryName + " " + orientationType);
        PortraitCanvas.SetActive(false);
        LandScapeCanvas.SetActive(true);

        // if (orientationType.ToLower() == "portrait")
        // {
        //     Screen.orientation = ScreenOrientation.Portrait;
        //     currentOrientation = CURRENT_MODE.PORTRAIT;
        //     PortraitCanvas.SetActive(true);
        //     LandScapeCanvas.SetActive(false);
        // }
        // else
        // {
        //     Screen.orientation = ScreenOrientation.LandscapeLeft;
        //     currentOrientation = CURRENT_MODE.LANDSCAPE;
        //     PortraitCanvas.SetActive(false);
        //     LandScapeCanvas.SetActive(true);
        // }
        
        contentNameText_p.text = contentName;
        contentNameText_l.text = contentName;

        AssetBundle bundle = AssetBundle.LoadFromFile(assetBundleUrl);
        yield return bundle;
        var mainPrefab = bundle.LoadAsset<GameObject>(assetName);
        mainInstance = Instantiate(mainPrefab) as GameObject;
        /*Renderer[] _childRenderers = mainInstance.GetComponentsInChildren<Renderer>();
        foreach(Renderer r in _childRenderers)
        {
            r.material.shader = Shader.Find(r.material.shader.name);
        }*/
        //TextAsset txt = bundle.LoadAsset(libraryName, typeof(TextAsset)) as TextAsset;
        //byte[] allBytes = txt.bytes;
        //System.Reflection.Assembly mainAssembly = System.Reflection.Assembly.Load(txt.bytes);
        //var type = mainAssembly.GetType("MainLoader");
        //mainInstance.AddComponent(type);
        
        if (isPinned)
        {
            if (SceneManager.GetActiveScene().buildIndex == 3)
            {
                mainInstance.transform.parent = anchorStage.transform;
                planeManager.planeAugmentation = mainInstance;
                mainInstance.SetActive(false);
                //GetComponent<HelloARController>().enabled = true;
                //if (GetComponent<HelloARController>().hasInitialized) NowInstantiateARPin();
                //mainInstance.SetActive(false);
                //MeshRenderer[] meshRenderers = mainInstance.GetComponentsInChildren<MeshRenderer>();
                //foreach (MeshRenderer m in meshRenderers) m.enabled = false;
                //canvasInstance = mainInstance.GetComponentInChildren<Canvas>().gameObject;
                //Debug.Log(canvasInstance);
                //canvasInstance.SetActive(false);
            }
            else
            {
                mainInstance.SetActive(false);
                mainInstance.transform.parent = transform;
                mainInstance.transform.localScale = Vector3.one;
                imageTarget.augmentationObject = mainInstance;
                imageTarget.basic = this;
                //if (GetComponent<ScanScript>().hasInitialized) NowInstantiateARPin();
            }
            //mainInstance.AddComponent<AROrbitControls>();
            LandScapeCanvas.SetActive(false);
            PortraitCanvas.SetActive(false);
        }
        bundle.Unload(false);
        if (PlayerPrefs.GetString(Constants.NETWORK_CONNECTED) == "T")
        {
            StartCoroutine(PopulateScrollCategories(PlayerPrefs.GetString(Constants.ID_CONTENT)));
            StartCoroutine(GetLikesAndStuff());
        }
    }

    IEnumerator GetLikesAndStuff()
    {
        string idContent = PlayerPrefs.GetString(Constants.ID_CONTENT);
        string idStudent = PlayerPrefs.GetString(Constants.ID_STUDENT);
        string contentSendUrl = Constants.BASE_URL + Constants.CONTENT_URL + "?" +
            Constants.ID_CONTENTS + "=" + idContent + "&" + Constants.ID_STUDENT + "=" + idStudent;
        Debug.Log(contentSendUrl);
        UnityWebRequest req = UnityWebRequest.Get(contentSendUrl);
        yield return req.SendWebRequest();
        Debug.Log("Response code for like and stuff: " + req.responseCode.ToString());
        if (req.responseCode == 200)
        {
            Debug.Log(req.downloadHandler.text);
            ContentModel[] contentModels = JsonHelper.getJsonArray<ContentModel>(req.downloadHandler.text);
            Debug.Log("____________________________");
            Debug.Log(contentModels[0].StudentRating);
            switch (contentModels[0].StudentRating)
            {
                case "0":
                case null:
                    {
                        likeStatus = LIKE_STATUS.NONE;
                        likeBtn_p.GetComponent<Image>().sprite = likeLine;
                        likeBtn_l.GetComponent<Image>().sprite = likeLine;
                        break;
                    }
                case "9":
                    {
                        likeStatus = LIKE_STATUS.LIKE;
                        likeBtn_p.GetComponent<Image>().sprite = likeFill;
                        likeBtn_l.GetComponent<Image>().sprite = likeFill;
                        break;
                    }
            }
            switch (contentModels[0].IsContentBookmarkedYN)
            {
                case "Y":
                    {
                        bookmarkStatus = BOOKMARK_STATUS.BOOKMARK;
                        bookmarkBtn_p.GetComponent<TextMeshProUGUI>().text = "Remove Bookmark";
                        bookmarkBtn_l.GetComponent<TextMeshProUGUI>().text = "Remove Bookmark";
                        break;
                    }
                case "N":
                    {
                        bookmarkStatus = BOOKMARK_STATUS.NO_BOOKMARK;
                        bookmarkBtn_p.GetComponent<TextMeshProUGUI>().text = "Bookmark";
                        bookmarkBtn_l.GetComponent<TextMeshProUGUI>().text = "Bookmark";
                        break;
                    }
            }
        }
        else if (req.responseCode == 401)
        {
            string toSend1 = Constants.BASE_URL + Constants.LOGIN_URL + "?" + Constants.LOGIN_ID + "=" + PlayerPrefs.GetString(Constants.LOGIN_ID) + "&" +
                              Constants.PASSWORD_HASH + "=" + PlayerPrefs.GetString(Constants.PASSWORD_HASH) + "&" +
                              Constants.CLIENT_ACCESSKEY + "=" + PlayerPrefs.GetString(Constants.CLIENT_ACCESSKEY);
            Debug.Log(toSend1);
            UnityWebRequest unityWebRequest = UnityWebRequest.Get(toSend1);
            yield return unityWebRequest.SendWebRequest();
            if (unityWebRequest.responseCode == 200)
            {
                Debug.Log(req.downloadHandler.text);
                LoginModel login = JsonUtility.FromJson<LoginModel>(unityWebRequest.downloadHandler.text);
                if (login.Status == Constants.SUCCESS)
                {
                    PlayerPrefs.SetString(Constants.AUTH_TOKEN, login.AuthToken);
                    UnityWebRequest request = UnityWebRequest.Get(contentSendUrl);
                    request.SetRequestHeader("Set-cookie", string.Format("ci_session={0}", login.AuthToken));
                    yield return request.SendWebRequest();
                    if (request.responseCode == 200)
                    {
                        Debug.Log(request.downloadHandler.text);
                        ContentModel[] contentModels = JsonHelper.getJsonArray<ContentModel>(request.downloadHandler.text);
                        switch (contentModels[0].StudentRating)
                        {
                            case null:
                                {
                                    likeStatus = LIKE_STATUS.NONE;
                                    likeBtn_p.GetComponent<Image>().sprite = likeLine;
                                    likeBtn_l.GetComponent<Image>().sprite = likeLine;
                                    break;
                                }
                            case "9":
                                {
                                    likeStatus = LIKE_STATUS.LIKE;
                                    likeBtn_p.GetComponent<Image>().sprite = likeFill;
                                    likeBtn_l.GetComponent<Image>().sprite = likeFill;
                                    break;
                                }
                        }
                        switch (contentModels[0].IsContentBookmarkedYN)
                        {
                            case "Y":
                                {
                                    bookmarkStatus = BOOKMARK_STATUS.BOOKMARK;
                                    bookmarkBtn_p.GetComponent<TextMeshProUGUI>().text = "Remove Bookmark";
                                    bookmarkBtn_l.GetComponent<TextMeshProUGUI>().text = "Remove Bookmark";
                                    break;
                                }
                            case "N":
                                {
                                    bookmarkStatus = BOOKMARK_STATUS.NO_BOOKMARK;
                                    bookmarkBtn_p.GetComponent<TextMeshProUGUI>().text = "Bookmark";
                                    bookmarkBtn_l.GetComponent<TextMeshProUGUI>().text = "Bookmark";
                                    break;
                                }
                        }
                    }
                }
            }
        }
    }

    public void NowInstantiateARPin()
    {
        mainInstance.SetActive(true);
        Debug.Log("Now Instantiate");
        if (SceneManager.GetActiveScene().buildIndex == 3)
        {
            planeManager.gameObject.SetActive(false);
        }

        if (currentOrientation == CURRENT_MODE.LANDSCAPE)
            LandScapeCanvas.SetActive(true);
        else
            PortraitCanvas.SetActive(true);
    }

    public void ShowHideScanningObject(bool show)
    {
        mainInstance.SetActive(show);
    }

    private IEnumerator CommentNow()
    {
        commentNowBtn_p.onClick.RemoveAllListeners();
        commentNowBtn_l.onClick.RemoveAllListeners();
        string comment;
        if (currentOrientation == CURRENT_MODE.PORTRAIT)
            comment = commentInputField_p.text;
        else
            comment = commentInputField_l.text;
        if (comment.Trim() == string.Empty)
        {
            if(currentOrientation == CURRENT_MODE.PORTRAIT)
            {
                toastObject_p.SetActive(true);
                toastObject_p.GetComponentInChildren<TextMeshProUGUI>().text = Constants.CANNOT_POST_EMPTY;
                yield return new WaitForSeconds(2);
                toastObject_p.SetActive(false);
            }
            else
            {
                toastObject_l.SetActive(true);
                toastObject_l.GetComponentInChildren<TextMeshProUGUI>().text = Constants.CANNOT_POST_EMPTY;
                yield return new WaitForSeconds(2);
                toastObject_l.SetActive(false);
            }
        }
        else
        {
            string _toSend = Constants.BASE_URL + Constants.USER_POST;
            PostCommentModel postComment = new PostCommentModel
            {
                idUser = PlayerPrefs.GetString(Constants.ID_USER),
                idCourse = PlayerPrefs.GetString(Constants.ID_COURSE),
                idContent = PlayerPrefs.GetString(Constants.ID_CONTENT),
                idCourseSubject = PlayerPrefs.GetString(Constants.ID_SUBJECT),
                idCourseSubjectChapter = PlayerPrefs.GetString(Constants.ID_CHAPTER),
                idTopic = PlayerPrefs.GetString(Constants.ID_TOPIC),
                PostSubject = "Comments",
                PostContent = comment,
                IsDeletedYN = "N"
            };
            Dictionary<string, string> d = new Dictionary<string, string>
            {
                { "Upsert", JsonUtility.ToJson(postComment) }
            };
            Debug.Log(d);
            UnityWebRequest unityWebRequest = UnityWebRequest.Post(_toSend, d);
            unityWebRequest.SetRequestHeader("Set-cookie", string.Format("ci_session={0}", PlayerPrefs.GetString(Constants.AUTH_TOKEN)));
            yield return unityWebRequest.SendWebRequest();
            if (unityWebRequest.isNetworkError)
            {
                Debug.LogError(unityWebRequest.error);
            }
            else
            {
                if (unityWebRequest.responseCode == 200)
                {
                    SuccessModel s = JsonUtility.FromJson<SuccessModel>(unityWebRequest.downloadHandler.text);
                    if (s != null && s.Status == Constants.SUCCESS)
                    {
                        commentInputField_p.text = string.Empty;
                        commentInputField_l.text = string.Empty;
                        if (currentOrientation == CURRENT_MODE.PORTRAIT)
                        {
                            toastObject_p.SetActive(true);
                            toastObject_p.GetComponentInChildren<TextMeshProUGUI>().text = Constants.FEEDBACK_SUCCESS;
                            ShowHideComments();
                            yield return new WaitForSeconds(2);
                            toastObject_p.SetActive(false);
                        }
                        else
                        {
                            toastObject_l.SetActive(true);
                            toastObject_l.GetComponentInChildren<TextMeshProUGUI>().text = Constants.FEEDBACK_SUCCESS;
                            ShowHideComments();
                            yield return new WaitForSeconds(2);
                            toastObject_l.SetActive(false);
                        }
                    }
                    else
                    {
                        Debug.Log(unityWebRequest.downloadHandler.text);
                    }
                }
                else if (unityWebRequest.responseCode == 401)
                {
                    string toSend1 = Constants.BASE_URL + Constants.LOGIN_URL + "?" + Constants.LOGIN_ID + "=" + PlayerPrefs.GetString(Constants.LOGIN_ID) + "&" +
                                      Constants.PASSWORD_HASH + "=" + PlayerPrefs.GetString(Constants.PASSWORD_HASH) + "&" +
                                      Constants.CLIENT_ACCESSKEY + "=" + PlayerPrefs.GetString(Constants.CLIENT_ACCESSKEY);
                    UnityWebRequest req = UnityWebRequest.Get(toSend1);
                    yield return req.SendWebRequest();
                    if (req.responseCode == 200)
                    {
                        LoginModel login = JsonUtility.FromJson<LoginModel>(req.downloadHandler.text);
                        if (login.Status == Constants.SUCCESS)
                        {
                            PlayerPrefs.SetString(Constants.AUTH_TOKEN, login.AuthToken);
                            UnityWebRequest request = UnityWebRequest.Get(_toSend);
                            request.SetRequestHeader("Set-cookie", string.Format("ci_session={0}", login.AuthToken));
                            yield return request.SendWebRequest();
                            if (request.responseCode == 200)
                            {
                                SuccessModel s = JsonUtility.FromJson<SuccessModel>(unityWebRequest.downloadHandler.text);
                                if (s != null && s.Status == Constants.SUCCESS)
                                {
                                    commentInputField_p.text = string.Empty;
                                    commentInputField_l.text = string.Empty;
                                    if (currentOrientation == CURRENT_MODE.PORTRAIT)
                                    {
                                        toastObject_p.SetActive(true);
                                        toastObject_p.GetComponentInChildren<TextMeshProUGUI>().text = Constants.FEEDBACK_SUCCESS;
                                        ShowHideComments();
                                        yield return new WaitForSeconds(2);
                                        toastObject_p.SetActive(false);
                                    }
                                    else
                                    {
                                        toastObject_l.SetActive(true);
                                        toastObject_l.GetComponentInChildren<TextMeshProUGUI>().text = Constants.FEEDBACK_SUCCESS;
                                        ShowHideComments();
                                        yield return new WaitForSeconds(2);
                                        toastObject_l.SetActive(false);
                                    }
                                }
                                else
                                {
                                    Debug.Log(unityWebRequest.downloadHandler.text);
                                    if (currentOrientation == CURRENT_MODE.PORTRAIT)
                                    {
                                        toastObject_p.SetActive(true);
                                        toastObject_p.GetComponentInChildren<TextMeshProUGUI>().text = Constants.FEEDBACK_ERROR;
                                    }
                                    else
                                    {
                                        toastObject_l.SetActive(true);
                                        toastObject_l.GetComponentInChildren<TextMeshProUGUI>().text = Constants.FEEDBACK_ERROR;
                                    }
                                    yield return new WaitForSeconds(2);
                                    toastObject_p.SetActive(false);
                                    toastObject_l.SetActive(false);
                                    ShowHideComments();
                                }
                            }
                        }
                    }
                }
                else
                {
                    Debug.Log(unityWebRequest.downloadHandler.text);
                    if (currentOrientation == CURRENT_MODE.PORTRAIT)
                    {
                        toastObject_p.SetActive(true);
                        toastObject_p.GetComponentInChildren<TextMeshProUGUI>().text = Constants.FEEDBACK_ERROR;
                    }
                    else
                    {
                        toastObject_l.SetActive(true);
                        toastObject_l.GetComponentInChildren<TextMeshProUGUI>().text = Constants.FEEDBACK_ERROR;
                    }
                    yield return new WaitForSeconds(2);
                    toastObject_p.SetActive(false);
                    toastObject_l.SetActive(false);
                    ShowHideComments();
                }
            }
        }

        commentNowBtn_p.onClick.AddListener(() => StartCoroutine(CommentNow()));
        commentNowBtn_l.onClick.AddListener(() => StartCoroutine(CommentNow()));
    }

    /*IEnumerator CheckCompatibility()
    {
        AsyncTask<ApkAvailabilityStatus> checkTask = Session.CheckApkAvailability();
        CustomYieldInstruction customYield = checkTask.WaitForCompletion();
        yield return customYield;
        ApkAvailabilityStatus result = checkTask.Result;
        switch (result)
        {
            case ApkAvailabilityStatus.SupportedApkTooOld:
            case ApkAvailabilityStatus.SupportedNotInstalled:
                AsyncTask<ApkInstallationStatus> apkInstallationStatus = Session.RequestApkInstallation(false);
                yield return apkInstallationStatus;
                ApkInstallationStatus res = apkInstallationStatus.Result;
                switch (res)
                {
                    case ApkInstallationStatus.Success:
                        pinStatus = PIN_STATUS.CAN;
                        break;
                    case ApkInstallationStatus.ErrorUserDeclined:
                        pinStatus = PIN_STATUS.STILL_CAN;
                        break;
                    default:
                        pinStatus = PIN_STATUS.CANNOT;
                        break;
                }
                break;
            case ApkAvailabilityStatus.SupportedInstalled:
                pinStatus = PIN_STATUS.CAN;
                break;
            case ApkAvailabilityStatus.UnsupportedDeviceNotCapable:
                {
                    pinStatus = PIN_STATUS.CANNOT;
                    break;
                }
            case ApkAvailabilityStatus.UnknownChecking:
            case ApkAvailabilityStatus.UnknownError:
            case ApkAvailabilityStatus.UnknownTimedOut:
                pinStatus = PIN_STATUS.STILL_CAN;
                break;
        }
        switch (pinStatus)
        {
            case PIN_STATUS.CANNOT:
                {
                    pinContentObject_p.SetActive(false);
                    pinContentObject_l.SetActive(false);
                    break;
                }
            case PIN_STATUS.CAN:
            case PIN_STATUS.STILL_CAN:
                {
                    pinContentObject_p.SetActive(true);
                    pinContentObject_l.SetActive(true);
                    break;
                }
        }
    }*/

    IEnumerator GetImage(string imageUrl,Image contentImage)
    {
        UnityWebRequest www = UnityWebRequestTexture.GetTexture(imageUrl);
        yield return www.SendWebRequest();
        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            Texture2D tex = ((DownloadHandlerTexture)www.downloadHandler).texture;
            contentImage.GetComponent<Image>().sprite = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);
        }
    }

    public void SetResult(int arg)
    {
        ContentUsage c = new ContentUsage
        {
            idContent = PlayerPrefs.GetString(Constants.ID_CONTENT),
            startTime = startTime,
            endTime = endTime,
            actualDuration = actualTimeInSeconds,
            lastPosition = 0,
            idLevel = "0",
            PerformanceScore = arg,
            type = isPinned ? Constants.TYPE_PIN : Constants.TYPE_SIMULATION
        };
        NativeCallsClass.SaveContentUsage(c, false);
    }

    private void _ShowAndroidToastMessage(string message)
    {
        AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject unityActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");

        if (unityActivity != null)
        {
            AndroidJavaClass toastClass = new AndroidJavaClass("android.widget.Toast");
            unityActivity.Call("runOnUiThread", new AndroidJavaRunnable(() =>
            {
                AndroidJavaObject toastObject = toastClass.CallStatic<AndroidJavaObject>("makeText", unityActivity,
                    message, 0);
                toastObject.Call("show");
            }));
        }
    }
    
    [Serializable]
    private class ContentIndexClass
    {
        public List<FlixContentIndexModel> _contentIndex;
    }
   
    public class JsonHelper
    {
        //Usage:
        //YouObject[] objects = JsonHelper.getJsonArray<YouObject> (jsonString);
        public static T[] getJsonArray<T>(string json)
        {
            string newJson = "{ \"array\": " + json + "}";
            Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(newJson);
            return wrapper.array;
        }

        //Usage:
        //string jsonString = JsonHelper.arrayToJson<YouObject>(objects);
        public static string arrayToJson<T>(T[] array)
        {
            Wrapper<T> wrapper = new Wrapper<T>();
            wrapper.array = array;
            return JsonUtility.ToJson(wrapper);
        }

        [System.Serializable]
        private class Wrapper<T>
        {
            public T[] array;
        }
    }

    [Serializable]
    public class ContentUsageModel
    {
        public ContentUsage[] contentUsage;
    }

    [Serializable]
    public class ContentUsage
    {
        public string idContent, startTime, endTime, type, idLevel;
        public int actualDuration, lastPosition;
        public int PerformanceScore;
    }

    [Serializable]
    public class RelatedContentIndexModel
    {
        public string idContent;
        public string ContentName;
        public string idTopic;
        public string TopicName;
        public string idCourseSubjectChapter;
        public string ChapterName;
        public string idCourseSubject;
        public string SubjectName;
        public string Url;
        public string Type;
        public string ViewCount;
        public string AuthorTitle;
        public string AuthorGivenName;
        public string AuthorSurName;
        public string AuthorImageUrl;
        public string AuthorSourceTitle;
        public string AuthorSourceGivenName;
        public string AuthorSourceSurName;
        public string AuthorSourceCreditUrl;
        public string RandomNum;
        public string NetRelevanceScore;
        public string RecommendedScore;
        public string ThumbnailImageUrl;
    }

    [Serializable]
    public class LoginModel
    {
        public string idUser;
        public string idStudent;
        public string idEntity;
        public string isAutoResetPassword;
        public string AuthToken;
        public string disableRewardRedeem;
        public string AllowOfflineAuthenticationYN;
        public string SecretCode;
        public string Status;
        public string Message;
    }

    [Serializable]
    public class ContentModel
    {
        public string StudentRating;
        public string IsContentBookmarkedYN;
        public string idContent;
        public string idEntity;
        public string Type;
        public string Name;
        public string Url;
        public string Status;
        public string UpdateTime;
        public string StudentContentURL;
        public string ThumbnailImageUrl;
        public string Script;
        public string Original_url;
        public string ContentHtml;
        public string ContentRatingLikes;
        public string ContentRatingDislike;
    }

    [Serializable]
    public class CommentModel
    {
        //todo
        public string photoUrl;
        public string idUserPost;
        public string idUser;
        public string DisplayName;
        public string htmlText;
    }

    [Serializable]
    public class PostCommentModel
    {
        public string idUser;
        public string idCourse;
        public string idContent;
        public string idCourseSubject;
        public string idCourseSubjectChapter;
        public string idTopic;
        public string PostSubject;
        public string PostContent;
        public string IsDeletedYN;
    }

    [Serializable]
    public class PostLikeModel
    {
        public string UpdateTime;
        public string idStudent;
        public string idContent;
        public string Rating;
    }

    [Serializable]
    public class PostBookmarkModel
    {
        public string idStudent;
        public BookmarkContentModel[] Contents;
    }

    [Serializable]
    public class BookmarkContentModel
    {
        public string idContent;
        public string IsBookmarkedYN;
    }

    [Serializable]
    public class UpsertModel
    {
        public PostCommentModel Upsert;
    }

    [Serializable]
    public class SuccessModel
    {
        public string Status;
    }

    [Serializable]
    public class IdContentModel
    {
        public string idContent;
    }
}



