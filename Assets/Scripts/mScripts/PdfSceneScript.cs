﻿using Paroxe.PdfRenderer;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine.Video;

public class PdfSceneScript : MonoBehaviour
{
    public Button markerBtn, backBtn;
    public TextMeshProUGUI topicText;
    public GameObject markerPrefab;
    public GameObject rawImagePrefab;
    public GameObject videoPlayerObject, audioPlayerObject;
    public Transform audioStarting;
    public GameObject pdfViewerObject;
    public GameObject audioVisualizerPrefab;
    public Color color1, color2;
    public float maxValue;
    public GameObject multipleAuthorGameobject;
    public GameObject authorBtnPrefab;
    public Transform multipleTransform;
    public Color unselectedColor;
    public GameObject loadingObject;

    private Anchors[] allAnchors;
    private List<GameObject> markers;
    private string currentUrl;
    private int selectedUserIndex = 0;
    private GameObject[] _sampleAudioVisualizers;
    private float[] _samples = new float[128];
    private bool isShowingSimulation = false, isPlayingAudio = false, isPlayingVideo = false, isShowingMarkers = true;
    private int currentIndex = 0;
    private PlaneData pdfData;
    private GameObject mainInstance;
    private Vector2[] sizeDeltas;
    private IEnumerator audioPlayIenum, videoPlayIenum, simPlayIenum;
    // Start is called before the first frame update
    private void Start()
    {
        Screen.orientation = ScreenOrientation.Portrait;
        videoPlayerObject.SetActive(false);
        audioPlayerObject.SetActive(false);
        markerBtn.onClick.AddListener(() => ShowMarkers());
        backBtn.onClick.AddListener(() => OnBackPressed());
        StartCoroutine(GetMainData());
        _sampleAudioVisualizers = new GameObject[128];
        for (int i = 0; i < 128; i++)
        {
            GameObject g = Instantiate(audioVisualizerPrefab, audioStarting) as GameObject;
            g.GetComponent<Image>().color = Color.Lerp(color1, color2, i / 128.0f);
            _sampleAudioVisualizers[i] = g;
        }
    }

    private void ShowMarkers()
    {
        Debug.Log("Show markers");
        isShowingMarkers = !isShowingMarkers;
        Image childImage = markerBtn.GetComponentInChildren<Image>();
        TextMeshProUGUI text = markerBtn.GetComponentInChildren<TextMeshProUGUI>();
        if (isShowingMarkers)
        {
            childImage.color = Color.black;
            text.color = Color.black;
        }
        else
        {
            childImage.color = unselectedColor;
            text.color = unselectedColor;
        }
        for (int i = 0; i < markers.Count; i++)
        {
            markers[i].SetActive(isShowingMarkers);
        }
    }
    
    public void OnZoomChanged()
    {
        if (pdfViewerObject.GetComponent<PDFViewer>().m_IsLoaded && markers != null)
        {
            for (int i = 0; i < markers.Count; i++)
            {
                Vector2 newPageSize = pdfViewerObject.GetComponent<PDFViewer>().m_PageSizes[allAnchors[i].pageNumber - 1];
                Debug.Log(newPageSize);
                Vector2 oldPageSize = sizeDeltas[i];
                Debug.Log(oldPageSize);
                float x = float.Parse(allAnchors[i].x);
                float y = float.Parse(allAnchors[i].y);
                Debug.Log(x);
                float nx = (newPageSize.x * x) / oldPageSize.x;
                float ny = (newPageSize.y * y) / oldPageSize.y;
                Debug.Log(nx);
                markers[i].transform.localPosition = new Vector3(nx, ny, 0);
            }
        }
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            OnBackPressed();
        }

        if(isPlayingAudio && !audioPlayerObject.GetComponent<AudioSource>().isPlaying)
        {
            OnFinishedPlayingAudio();
        }
        else if(isPlayingVideo && !videoPlayerObject.GetComponent<VideoPlayer>().isPlaying)
        {
            OnFinishedPlayingVideo();
        }

        if (audioPlayerObject.GetComponent<AudioSource>().isPlaying)
        {
            audioPlayerObject.GetComponent<AudioSource>().GetSpectrumData(_samples, 0, FFTWindow.Blackman);
            for(int i = 0; i < 128; i++)
            {
                if (_sampleAudioVisualizers[i] != null)
                {
                    _sampleAudioVisualizers[i].transform.localScale = new Vector3(1, _samples[i] * maxValue, 1);
                }
            }
        }
    }

    private IEnumerator GetMainData()
    {
        topicText.text = PlayerPrefs.GetString(Constants.CONTENT_NAME);
        string idContent = PlayerPrefs.GetString(Constants.ID_CONTENT);
        pdfViewerObject.GetComponent<PDFViewer>().enabled = false;
        loadingObject.SetActive(true);
        //TODO
        /*string _toSend = Constants.BASE_URL + Constants.CONTENT_URL + "?" +
                           Constants.ID_CONTENT + "=" + idContent;*/

        string _toSend = "http://oetdlabs.com/temp2.php";
        UnityWebRequest request = UnityWebRequest.Get(_toSend);
        request.SetRequestHeader("Set-cookie", string.Format("ci_session={0}", PlayerPrefs.GetString(Constants.AUTH_TOKEN)));
        yield return request.SendWebRequest();
        print(request.responseCode);
        if (request.responseCode == 200)
        {
            PlaneData[] data = BasicNavigation.JsonHelper.getJsonArray<PlaneData>(request.downloadHandler.text);
            pdfData = data[0];
            pdfViewerObject.GetComponent<PDFViewer>().FileURL = pdfData.Url;
            pdfViewerObject.GetComponent<PDFViewer>().enabled = true;
            SetUpMarkers();
        }
        else if (request.responseCode == 401)
        {
            string toSend1 = Constants.BASE_URL + Constants.LOGIN_URL + "?" + Constants.LOGIN_ID + "=" + PlayerPrefs.GetString(Constants.LOGIN_ID) + "&" +
                                 Constants.PASSWORD_HASH + "=" + PlayerPrefs.GetString(Constants.PASSWORD_HASH) + "&" +
                                 Constants.CLIENT_ACCESSKEY + "=" + PlayerPrefs.GetString(Constants.CLIENT_ACCESSKEY);

            UnityWebRequest req = UnityWebRequest.Get(toSend1);
            yield return req.SendWebRequest();
            if (req.responseCode == 200)
            {
                BasicNavigation.LoginModel login = JsonUtility.FromJson<BasicNavigation.LoginModel>(req.downloadHandler.text);
                if (login.Status == Constants.SUCCESS)
                {
                    // Debug.Log("Auth token" + login.AuthToken);
                    PlayerPrefs.SetString(Constants.AUTH_TOKEN, login.AuthToken);
                    UnityWebRequest unityWebRequest = UnityWebRequest.Get(_toSend);
                    unityWebRequest.SetRequestHeader("Set-cookie", string.Format("ci_session={0}", login.AuthToken));
                    //Debug.Log(unityWebRequest.GetRequestHeader("Set-cookie"));
                    yield return unityWebRequest.SendWebRequest();
                    //Debug.Log(request.GetResponseHeader("Set-cookie"));
                    if (unityWebRequest.responseCode == 200)
                    {
                        Debug.Log(request.downloadHandler.text);
                        PlaneData[] data = BasicNavigation.JsonHelper.getJsonArray<PlaneData>(request.downloadHandler.text);
                        pdfData = data[0];
                        Debug.Log(pdfData.Url);
                        //todo dlete
                        pdfViewerObject.GetComponent<PDFViewer>().FileURL = pdfData.Url;
                        pdfViewerObject.GetComponent<PDFViewer>().enabled = true;
                        SetUpMarkers();
                    }
                }
            }
        }
    }

    private void SetUpMarkers()
    {
        Debug.Log(pdfData.AnchorLists.Length);
        loadingObject.SetActive(false);
        if (pdfData.AnchorLists.Length > 1)
        {
            multipleAuthorGameobject.SetActive(true);
            for(int i = 0; i < pdfData.AnchorLists.Length; i++)
            {
                GameObject g = Instantiate(authorBtnPrefab, multipleTransform) as GameObject;
                int index = i;
                g.GetComponentInChildren<TextMeshProUGUI>().text = pdfData.AnchorLists[index].Author;
                g.GetComponent<Button>().onClick.AddListener(() =>
                {
                    selectedUserIndex = index;
                    multipleAuthorGameobject.SetActive(false);
                    StartCoroutine(SetMarkersNow());
                });
            }
        }
        else
        {
            selectedUserIndex = 0;
            StartCoroutine(SetMarkersNow());
        }
    }

    private IEnumerator SetMarkersNow()
    {
        while (!pdfViewerObject.GetComponent<PDFViewer>().m_IsLoaded) yield return null;
        markers = new List<GameObject>();
        allAnchors = pdfData.AnchorLists[selectedUserIndex].Anchors;
        sizeDeltas = new Vector2[allAnchors.Length];
        for (int j = 0; j < allAnchors.Length; j++)
        {
            int i = j;
            int pNumber = allAnchors[i].pageNumber;
            GameObject parent = pdfViewerObject.GetComponent<PDFViewer>().m_PageTextureHolders[pNumber - 1].m_Page;
            GameObject m = Instantiate(markerPrefab, parent.transform) as GameObject;
            float x = float.Parse(allAnchors[i].x);
            float y = float.Parse(allAnchors[i].y);
            m.transform.localPosition = new Vector3(x, y, 0);
            sizeDeltas[i] = parent.GetComponent<RectTransform>().sizeDelta;
            switch (allAnchors[i].type)
            {
                case "mpg":
                    {
                        m.GetComponent<Button>().onClick.AddListener(() =>
                        {
                            videoPlayIenum = PlayVideo(allAnchors[i].idContent);
                            StartCoroutine(videoPlayIenum);
                        });
                        break;
                    }
                case "audio":
                    {

                        m.GetComponent<Button>().onClick.AddListener(() =>
                        {
                            audioPlayIenum = PlayAudio(allAnchors[i].idContent);
                            StartCoroutine(audioPlayIenum);
                        });
                        break;
                    }
                case "simulation":
                    {
                        m.GetComponent<Button>().onClick.AddListener(() =>
                        {
                            simPlayIenum = PlaySimulation(allAnchors[i].idContent);
                            StartCoroutine(PlaySimulation(allAnchors[i].idContent));
                        });
                        break;
                    }
            }
            markers.Add(m);
        }
    }

    private IEnumerator PlayVideo(string idContent)
    {
        if (!isPlayingVideo)
        {
            currentUrl = string.Empty;
            StartCoroutine(GetUrlDetails(idContent));
            while (currentUrl == string.Empty) yield return null;
            pdfViewerObject.SetActive(false);
            loadingObject.SetActive(true);
            Camera.main.backgroundColor = Color.black;
            videoPlayerObject.SetActive(true);
            VideoPlayer videoPlayer = videoPlayerObject.GetComponent<VideoPlayer>();
            AudioSource audioSource = videoPlayerObject.GetComponent<AudioSource>();
            videoPlayer.source = VideoSource.Url;
            videoPlayer.url = currentUrl;
            Debug.Log("PLAYING VIDEO : " + currentUrl);
            videoPlayer.audioOutputMode = VideoAudioOutputMode.AudioSource;
            videoPlayer.EnableAudioTrack(0, true);
            videoPlayer.SetTargetAudioSource(0, audioSource);
            videoPlayer.Prepare();
            while (!videoPlayer.isPrepared)
            {
                yield return null;
            }
            videoPlayerObject.GetComponent<RawImage>().texture = videoPlayer.texture;
            videoPlayer.GetComponent<RawImage>().SetNativeSize();
            videoPlayerObject.transform.localPosition = Vector3.zero;
            videoPlayer.Play();
            audioSource.Play();
            isPlayingVideo = true;
            loadingObject.SetActive(false);
        }
    }

    private void OnFinishedPlayingVideo()
    {
        Camera.main.backgroundColor = new Color(106, 106, 106, 255);
        videoPlayerObject.SetActive(false);
        videoPlayerObject.GetComponent<VideoPlayer>().Stop();
        videoPlayerObject.GetComponent<AudioSource>().Stop();
        pdfViewerObject.SetActive(true);
        isPlayingVideo = false;
        loadingObject.SetActive(false);
    }

    private void OnFinishedPlayingAudio()
    {
        audioPlayerObject.GetComponent<AudioSource>().Stop();
        audioPlayerObject.SetActive(false);
        pdfViewerObject.SetActive(true);
        isPlayingAudio = false;
        loadingObject.SetActive(false);
    }

    private void OnFinishedShowingSimulation()
    {
        pdfViewerObject.SetActive(true);
        AssetBundle.UnloadAllAssetBundles(true);
        Destroy(mainInstance);
        isShowingSimulation = false;
        loadingObject.SetActive(false);
    }

    private IEnumerator PlayAudio(string idContent)
    {
        if (!isPlayingAudio)
        {
            audioPlayerObject.SetActive(true);
            audioStarting.gameObject.SetActive(false);
            pdfViewerObject.SetActive(false);
            loadingObject.SetActive(true);
            currentUrl = string.Empty;
            StartCoroutine(GetUrlDetails(idContent));
            while (currentUrl == string.Empty) yield return null;
            AudioType audioType;
            string format = currentUrl.Substring(currentUrl.Length - 3);
            if (format == "mp2" || format == "mp3") audioType = AudioType.MPEG;
            else if (format == "ogg") audioType = AudioType.OGGVORBIS;
            else if (format == "wav") audioType = AudioType.WAV;
            else audioType = AudioType.UNKNOWN;
            using (UnityWebRequest www = UnityWebRequestMultimedia.GetAudioClip(currentUrl, audioType))
            {
                yield return www.SendWebRequest();
                loadingObject.SetActive(false);
                if (www.isHttpError || www.isNetworkError)
                {
                    Debug.Log(www.error);
                }
                else
                {
                    audioStarting.gameObject.SetActive(true);
                    AudioClip myClip = DownloadHandlerAudioClip.GetContent(www);
                    AudioSource audioSource = audioPlayerObject.GetComponent<AudioSource>();
                    audioSource.clip = myClip;
                    audioSource.Play();
                    Debug.Log("PLAYING AUDIO: " + currentUrl);
                    isPlayingAudio = true;
                }
            }
        }
    }

    private IEnumerator PlaySimulation(string idContent)
    {
        if (!isShowingSimulation)
        {
            isShowingSimulation = true;
            loadingObject.SetActive(true);
            pdfViewerObject.SetActive(false);
            currentUrl = string.Empty;


            //todo delete
            //StartCoroutine(GetUrlDetails(idContent));
            currentUrl = idContent;
            Debug.Log(currentIndex);
            //YAHA TAK


            while (currentUrl == string.Empty) yield return null;
            string extraData = currentUrl.Substring(currentUrl.IndexOf('?') + 1);
            string assetName = GetDataFromString("mainassetname", extraData);
            //todo change assetName to idContent below
            string assetBundelUrl = Path.Combine(PlayerPrefs.GetString(Constants.APPLICATION_DIRECTORY_PATH), "Tag" + assetName);
            PlayerPrefs.SetString(Constants.ASSET_BUNDLE_URL, assetBundelUrl);
            if (!File.Exists(assetBundelUrl))
            {
                string assetUrl = currentUrl.Substring(0, currentUrl.IndexOf('?'));
                UnityWebRequest www = UnityWebRequest.Get(currentUrl);
                yield return www.SendWebRequest();
                File.WriteAllBytes(assetBundelUrl, www.downloadHandler.data);
            }
            AssetBundle bundle = AssetBundle.LoadFromFile(assetBundelUrl);
            yield return bundle;
            var mainPrefab = bundle.LoadAsset<GameObject>(assetName);
            mainInstance = Instantiate(mainPrefab) as GameObject;
            string libraryName = GetDataFromString("mainlibraryname", extraData);
            PlayerPrefs.SetString(Constants.LIBRARY_NAME, libraryName);
            TextAsset txt = bundle.LoadAsset(libraryName, typeof(TextAsset)) as TextAsset;
            byte[] allBytes = txt.bytes;
            System.Reflection.Assembly mainAssembly = System.Reflection.Assembly.Load(txt.bytes);
            var type = mainAssembly.GetType("MainLoader");
            mainInstance.AddComponent(type);
            string orientationType = GetDataFromString("oriType", extraData);
            if (orientationType.ToLower() == "portrait") Screen.orientation = ScreenOrientation.Portrait;
            else Screen.orientation = ScreenOrientation.LandscapeLeft;
            loadingObject.SetActive(false);
            bundle.Unload(false);
        }
    }

    private string GetDataFromString(string key, string data)
    {
        string[] values = data.Split('&');
        for (int i = 0; i < values.Length; i++)
        {
            var pairs = values[i].Split('=');
            if (pairs[0] == key) return pairs[1];
        }
        return "";
    }

    private void OnBackPressed()
    {
        if (videoPlayerObject.activeSelf || videoPlayerObject.GetComponent<VideoPlayer>().isPlaying)
        {
            StopCoroutine(videoPlayIenum);
            OnFinishedPlayingVideo();
        }
        else if (audioPlayerObject.activeSelf || audioPlayerObject.GetComponent<AudioSource>().isPlaying)
        {
            StopCoroutine(audioPlayIenum);
            OnFinishedPlayingAudio();
        }
        else if (isShowingSimulation)
        {
            OnFinishedShowingSimulation();
        }
        else if (!isShowingSimulation && !isPlayingAudio && !isPlayingVideo)
        {
            //NativeCallsClass.GoBack(String.Empty);
            Application.Quit();
        }
    }

    private IEnumerator GetUrlDetails(string idContent)
    {
        string _toSend = Constants.BASE_URL + Constants.CONTENT_URL + "?" +
                           Constants.ID_CONTENT + "=" + idContent;

        UnityWebRequest request = UnityWebRequest.Get(_toSend);
        request.SetRequestHeader("Set-cookie", string.Format("ci_session={0}", PlayerPrefs.GetString(Constants.AUTH_TOKEN)));
        yield return request.SendWebRequest();
        if (request.responseCode == 200)
        {
            BasicNavigation.ContentModel[] content = BasicNavigation.JsonHelper.getJsonArray<BasicNavigation.ContentModel>(request.downloadHandler.text);
            //currentUrl = content[0].Url;
            //todo delete
            currentUrl = idContent;
        }
        else if (request.responseCode == 401)
        {
            string toSend1 = Constants.BASE_URL + Constants.LOGIN_URL + "?" + Constants.LOGIN_ID + "=" + PlayerPrefs.GetString(Constants.LOGIN_ID) + "&" +
                              Constants.PASSWORD_HASH + "=" + PlayerPrefs.GetString(Constants.PASSWORD_HASH) + "&" +
                              Constants.CLIENT_ACCESSKEY + "=" + PlayerPrefs.GetString(Constants.CLIENT_ACCESSKEY);
            UnityWebRequest req = UnityWebRequest.Get(toSend1);
            yield return req.SendWebRequest();
            if (req.responseCode == 200)
            {
                BasicNavigation.LoginModel login = JsonUtility.FromJson<BasicNavigation.LoginModel>(req.downloadHandler.text);
                if (login.Status == Constants.SUCCESS)
                {
                    PlayerPrefs.SetString(Constants.AUTH_TOKEN, login.AuthToken);
                    UnityWebRequest unityWebRequest = UnityWebRequest.Get(_toSend);
                    unityWebRequest.SetRequestHeader("Set-cookie", string.Format("ci_session={0}", login.AuthToken));
                    yield return unityWebRequest.SendWebRequest();
                    if (unityWebRequest.responseCode == 200)
                    {
                        BasicNavigation.ContentModel[] content = BasicNavigation.JsonHelper.getJsonArray<BasicNavigation.ContentModel>(request.downloadHandler.text);
                        //currentUrl = content[0].Url;
                        //todo delete
                        currentUrl = idContent;
                    }
                }
            }
        }
    }

    [Serializable]
    public class Anchors
    {
        public int pageNumber;
        public string label;
        public string type;
        public string idContent;
        public string x;
        public string y;
        public string z;
    }
    [Serializable]
    public class AnchorList
    {
        public string Author;
        public Anchors[] Anchors;
    }
    [Serializable]
    public class PlaneData
    {
        public string idContent;
        public string Url;
        public AnchorList[] AnchorLists;
    }
}