﻿using TMPro;
using UnityEngine;
using UnityEngine.Events;

public class GyroCameraControl : MonoBehaviour 
{
	private Gyroscope gyro;
	private bool gyroSupported;
	private Quaternion rotationFix = new Quaternion(0f, 0f, 1f, 0f);

    public TextMeshProUGUI text;
	public Transform gyroCamera;
	//public UnityEvent OnGyroIsNotSupported;

	void Start () 
	{
		gyroSupported = SystemInfo.supportsGyroscope;
        text.text = "GYRO SUPPORTED: " + gyroSupported;


		if (gyroSupported)
        {
            gyroCamera.parent.transform.rotation = Quaternion.Euler(90f, 90f, 0f);
            gyro = Input.gyro;
			gyro.enabled = true;
		}
	}

	void Update () 
	{
		if (gyroSupported)
		{
			gyroCamera.localRotation = gyro.attitude * rotationFix;
            text.text = gyroCamera.localRotation.ToString();
		}
	}
}
