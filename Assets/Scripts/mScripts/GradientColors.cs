﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GradientColors : MonoBehaviour {

    public Color startColor;
    public Color endColor;
    void Start()
    {
        var mesh = GetComponent<MeshFilter>().mesh;
        var uv = mesh.uv;
        var colors = new Color[uv.Length];

        // Instead if vertex.y we use uv.x
        for (var i = 0; i < uv.Length; i++)
            colors[i] = Color.Lerp(startColor, endColor, uv[i].x);
        //for (var i = uv.Length / 2; i < uv.Length; i++)
            //colors[i] = Color.Lerp(endColor, startColor, uv[i].x);

        mesh.colors = colors;
    }
}
