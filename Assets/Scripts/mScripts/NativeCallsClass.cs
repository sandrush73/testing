﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class NativeCallsClass : MonoBehaviour
{
    public class PostEventLogClass
    {
        public string UserID;
        public string EventStartTime;
        public string EventEndTime;
        public string EventName;
        public string EventDetails;
        public string EventSourceId;
        public string idEntity;
    }

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    public void GetValuesFromNative(string values)
    {
        Debug.Log(values);
        string[] valuesFromNative = values.Split('|');
        PlayerPrefs.SetString(Constants.ASSET_BUNDLE_URL, valuesFromNative[1]);
        PlayerPrefs.SetString(Constants.APPLICATION_DIRECTORY_PATH, valuesFromNative[1].Substring(0, valuesFromNative[1].LastIndexOf('/')));
        PlayerPrefs.SetString(Constants.ASSET_NAME, valuesFromNative[2]);
        PlayerPrefs.SetString(Constants.LIBRARY_NAME, valuesFromNative[3]);
        PlayerPrefs.SetString(Constants.CONTENT_NAME, valuesFromNative[4]);
        PlayerPrefs.SetString(Constants.ID_CONTENT, valuesFromNative[5]);
        PlayerPrefs.SetString(Constants.SCREEN_ORIENTATION, valuesFromNative[6]);
        PlayerPrefs.SetString(Constants.ID_SUBJECT, valuesFromNative[7]);
        PlayerPrefs.SetString(Constants.ID_CHAPTER, valuesFromNative[8]);
        PlayerPrefs.SetString(Constants.ID_TOPIC, valuesFromNative[9]);
        PlayerPrefs.SetInt("TotalXP", int.Parse(valuesFromNative[10]));
        PlayerPrefs.SetInt("MaxEarnings", int.Parse(valuesFromNative[11]));
        PlayerPrefs.SetString(Constants.IS_CHALLENGE, Constants.NO);
        StartCoroutine(LoadYourAsyncScene(Int32.Parse(valuesFromNative[0])));
    }

    public static IEnumerator PostEventLog(string startTime, string endTime, string eventName, string extraInfo)
    {
        if (PlayerPrefs.GetString(Constants.NETWORK_CONNECTED) == "T")
        {
            PostEventLogClass postEvent = new PostEventLogClass
            {
                UserID = PlayerPrefs.GetString(Constants.ID_USER),
                EventStartTime = startTime,
                EventEndTime = endTime,
                EventName = eventName,
                EventSourceId = PlayerPrefs.GetString(Constants.ID_CONTENT),
                idEntity = PlayerPrefs.GetString(Constants.ID_ENTITY)
            };
            string _toSend = Constants.BASE_URL + Constants.EVENT_LOG;
            Dictionary<string, string> d = new Dictionary<string, string>
            {
                { "Update", JsonUtility.ToJson(postEvent) }
            };
            Debug.Log(JsonUtility.ToJson(postEvent));
            UnityWebRequest unityWebRequest = UnityWebRequest.Post(_toSend, d);
            unityWebRequest.SetRequestHeader("Set-cookie", string.Format("ci_session={0}", PlayerPrefs.GetString(Constants.AUTH_TOKEN)));
            yield return unityWebRequest.SendWebRequest();
            if (unityWebRequest.responseCode == 200)
            {
                Debug.Log(unityWebRequest.downloadHandler.text);
            }
            else if (unityWebRequest.responseCode == 401)
            {
                string toSend1 = Constants.BASE_URL + Constants.LOGIN_URL + "?" + Constants.LOGIN_ID + "=" + PlayerPrefs.GetString(Constants.LOGIN_ID) + "&" +
                                  Constants.PASSWORD_HASH + "=" + PlayerPrefs.GetString(Constants.PASSWORD_HASH) + "&" +
                                  Constants.CLIENT_ACCESSKEY + "=" + PlayerPrefs.GetString(Constants.CLIENT_ACCESSKEY);
                UnityWebRequest req = UnityWebRequest.Get(toSend1);
                yield return req.SendWebRequest();
                if (req.responseCode == 200)
                {
                    BasicNavigation.LoginModel login = JsonUtility.FromJson<BasicNavigation.LoginModel>(req.downloadHandler.text);
                    if (login.Status == Constants.SUCCESS)
                    {
                        PlayerPrefs.SetString(Constants.AUTH_TOKEN, login.AuthToken);
                        UnityWebRequest request = UnityWebRequest.Get(_toSend);
                        request.SetRequestHeader("Set-cookie", string.Format("ci_session={0}", login.AuthToken));
                        yield return request.SendWebRequest();
                        if (request.responseCode == 200)
                        {
                            Debug.Log(unityWebRequest.downloadHandler.text);
                        }
                    }
                }
            }
        }
    }

    public void GetValuesForChallenge(string values)
    {
        Debug.Log(values);
        string[] valuesFromNative = values.Split('|');
        PlayerPrefs.SetString(Constants.ASSET_BUNDLE_URL, valuesFromNative[1]);
        Debug.Log(PlayerPrefs.GetString(Constants.ASSET_BUNDLE_URL));

        PlayerPrefs.SetString(Constants.APPLICATION_DIRECTORY_PATH, valuesFromNative[1].Substring(0, valuesFromNative[1].LastIndexOf('/')));
        PlayerPrefs.SetString(Constants.ASSET_NAME, valuesFromNative[2]);
        Debug.Log(PlayerPrefs.GetString(Constants.ASSET_NAME));

        PlayerPrefs.SetString(Constants.LIBRARY_NAME, valuesFromNative[3]);
        Debug.Log(PlayerPrefs.GetString(Constants.LIBRARY_NAME));

        PlayerPrefs.SetString(Constants.CONTENT_NAME, valuesFromNative[4]);
        Debug.Log(PlayerPrefs.GetString(Constants.CONTENT_NAME));
        
        PlayerPrefs.SetString(Constants.ID_CONTENT, valuesFromNative[5]);
        Debug.Log(PlayerPrefs.GetString(Constants.ID_CONTENT));

        PlayerPrefs.SetString(Constants.SCREEN_ORIENTATION, valuesFromNative[6]);
        Debug.Log(PlayerPrefs.GetString(Constants.SCREEN_ORIENTATION));

        PlayerPrefs.SetString(Constants.IS_CHALLENGE, Constants.YES);
        Debug.Log(PlayerPrefs.GetString(Constants.SCREEN_ORIENTATION));
        PlayerPrefs.Save();
        SceneManager.LoadSceneAsync(Int32.Parse(valuesFromNative[0]));
    }

    public static void SaveContentUsage(BasicNavigation.ContentUsage c, bool quit)
    {
        string destination = Path.Combine(PlayerPrefs.GetString(Constants.APPLICATION_DIRECTORY_PATH), "tempContentUsage.txt");

        Debug.Log(destination);

        if (File.Exists(destination))
        {
            string contentUsage = File.ReadAllText(destination, Encoding.UTF8);
            BasicNavigation.ContentUsageModel cuModel = JsonUtility.FromJson<BasicNavigation.ContentUsageModel>(contentUsage);
            BasicNavigation.ContentUsage[] oldArr = new BasicNavigation.ContentUsage[cuModel.contentUsage.Length + 1];
            cuModel.contentUsage.CopyTo(oldArr, 0);
            oldArr[cuModel.contentUsage.Length] = c;
            cuModel.contentUsage = oldArr;
            File.WriteAllText(destination, JsonUtility.ToJson(cuModel).ToString(), Encoding.UTF8);
            Debug.Log("Content Usage yo!! : " + JsonUtility.ToJson(cuModel));
        }
        else
        {
            BasicNavigation.ContentUsageModel cuModel = new BasicNavigation.ContentUsageModel
            {
                contentUsage = new BasicNavigation.ContentUsage[1]
            };
            cuModel.contentUsage[0] = c;
            File.WriteAllText(destination, JsonUtility.ToJson(cuModel).ToString(), Encoding.UTF8);
            Debug.Log("Content Usage yo!! : " + JsonUtility.ToJson(cuModel));
        }

        if (quit) Application.Quit();
    }

    IEnumerator LoadYourAsyncScene(int sceneIndex)
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneIndex);
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }
} 
