﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class ARCoreCheck : MonoBehaviour {
    public TextMeshProUGUI oopsTxt;
    public Button scanBtn, exploreBtn, pinBtn;
    public TextMeshProUGUI debugTxt;
    public Image backgroundImg, loadingImg;
    //private AsyncTask<ApkAvailabilityStatus> avs;
    private AsyncOperation async = null;
    // Use this for initialization
    void Start () {
        //avs = Session.CheckApkAvailability();
        scanBtn.onClick.AddListener(() =>
        {
            //SceneManager.LoadScene(1);
            PlayerPrefs.SetInt("AR", 1);
            StartCoroutine(LoadScene(1));
        });
        exploreBtn.onClick.AddListener(() =>
        {

            //SceneManager.LoadScene(2);
            StartCoroutine(LoadScene(2));
        });
        //StartCoroutine(CheckCompatibility());
    }

    /*private IEnumerator CheckCompatibility()
    {
        GoogleARCore.AsyncTask<ApkAvailabilityStatus> checkTask = Session.CheckApkAvailability();
        CustomYieldInstruction customYield = checkTask.WaitForCompletion();
        yield return customYield;
        ApkAvailabilityStatus result = checkTask.Result;
        switch (result)
        {
            case ApkAvailabilityStatus.SupportedApkTooOld:
            case ApkAvailabilityStatus.SupportedNotInstalled:
                Session.RequestApkInstallation(false);
                break;
            case ApkAvailabilityStatus.SupportedInstalled:
                oopsTxt.gameObject.SetActive(false);
                pinBtn.onClick.AddListener(() =>
                {
                    //PlayerPrefs.SetInt("AR", 2);
                    StartCoroutine(LoadScene(3));
                });
                break;
            case ApkAvailabilityStatus.UnsupportedDeviceNotCapable:
                oopsTxt.gameObject.SetActive(true);
                break;
            case ApkAvailabilityStatus.UnknownChecking:
            case ApkAvailabilityStatus.UnknownError:
            case ApkAvailabilityStatus.UnknownTimedOut:
                break;
        }
    }*/

    private IEnumerator LoadScene(int x)
    {
        backgroundImg.gameObject.SetActive(true);
        scanBtn.onClick.RemoveAllListeners();
        exploreBtn.onClick.RemoveAllListeners();
        pinBtn.onClick.RemoveAllListeners();
        async = SceneManager.LoadSceneAsync(x);
        while (!async.isDone)
        {
            float y = (8.51f * async.progress * 100f) - 851.0f;
            debugTxt.text = async.progress.ToString();
            loadingImg.transform.localPosition = new Vector3(loadingImg.transform.localPosition.x, y, loadingImg.transform.localPosition.z);
            yield return null;
        }
    }
    private void Update()
    {
        //debugTxt.text = avs.Result.ToString();
        /*if ((avs.Result == ApkAvailabilityStatus.SupportedApkTooOld) || (avs.Result == ApkAvailabilityStatus.SupportedNotInstalled))
        {
            Session.RequestApkInstallation(true);
        }
        else if (avs.Result == ApkAvailabilityStatus.SupportedInstalled)
        {
            oopsTxt.gameObject.SetActive(false);
            pinBtn.onClick.AddListener(() =>
            {
                //PlayerPrefs.SetInt("AR", 2);
                StartCoroutine(LoadScene(3));
            });

        }
        else if (avs.Result == ApkAvailabilityStatus.UnsupportedDeviceNotCapable)
        {
            oopsTxt.gameObject.SetActive(true);
        }*/
    }
}
