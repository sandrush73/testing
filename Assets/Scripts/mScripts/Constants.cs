﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Constants
{
    //urls
    public static string BASE_URL = "http://stage.corsalite.com/v1/webservices/";
    public static string FILE_BASE_URL = "http://stage.corsalite.com/v1/";
    public static string IMAGE_PATH = "http://staging.corsalite.com/adhoc/appbuild/Mat.jpg";
    public static string CONTENT_INDEX_URL = "ContentIndex";
    public static string LOGIN_URL = "AuthToken";
    public static string POPULAR_SUGGESTIONS_URL = "PopularVideos";
    public static string RECOMMENDED_SUGGESTIONS_URL = "RecommendedVideos";
    public static string RELATED_SUGGESTIONS_URL = "Relatedvideos";
    public static string CONTENT_URL = "Content";
    public static string USER_POST = "UserPost";
    public static string CONTENT_RATING_URL = "ContentRating";
    public static string CONTENT_BOOKMARK = "ContentBookmark";
    public static string EVENT_LOG = "EventLog_v1";
    //params
    public static string APPLICATION_DIRECTORY_PATH = "ApplicationDirectoryPath";
    public static string LOGIN_ID = "LoginID";
    public static string PASSWORD_HASH = "PasswordHash";
    public static string CLIENT_ACCESSKEY = "ClientAccesssKey";
    public static string ID_USER = "idUser";
    public static string ID_STUDENT = "idStudent";
    public static string ID_ENTITY = "idEntity";
    public static string AUTH_TOKEN = "AuthToken";
    public static string ID_COURSE = "idCourse";
    public static string COURSE_NAME = "courseName";
    public static string ID_CONTENT = "idContent";
    public static string ID_CONTENTS = "idContents";
    public static string ID_SUBJECT = "idSubject";
    public static string ID_CHAPTER = "idChapter";
    public static string ID_TOPIC = "idTopic";
    public static string ASSET_BUNDLE_URL = "AssetBundleUrl";
    public static string ASSET_NAME = "AssetName";
    public static string LIBRARY_NAME = "LibraryName";
    public static string CONTENT_NAME = "ContentName";
    public static string TOTAL_LIKES = "TotalLikes";
    public static string TOTAL_DISLIKES = "TotalDislikes";
    public static string SCREEN_ORIENTATION = "ScreenOrientation";
    public static string TOTAL_PAGES = "TotalPages";
    public static string NETWORK_CONNECTED = "NetworkConnected";
    public static string TYPE_MPG = "mpg";
    public static string TYPE_SIMULATION = "simulation";
    public static string TYPE_PIN = "AR";
    public static string IS_CHALLENGE = "IsChallenge";

    //tags and names of objs
    public static string TOPIC_TEXT = "TopicText";
    public static string CONTENT_PLACEHOLDER = "ContentPlaceholder";
    public static string CONTENT_IMAGE = "ContentImage";
    public static string CONTENT_TEXT = "ContentText";

    //to show names
    public static string POPULAR = "Popular";
    public static string RELATED = "Related";
    public static string RECOMMENDED = "Recommended";
    public static string UNSUPPORTED_DEVICE = "Unsupported Device";

    //general
    public static string SUCCESS = "SUCCESS";
    public static string TYPE = "type";
    public static string ALL_POSTS = "AllPosts";
    public static string NO_COMMENTS_YET = "No comments yet.";
    public static string CANNOT_POST_EMPTY = "Comment cannot be empty";
    public static string FEEDBACK_SUCCESS = "Feedback successfully submitted";
    public static string FEEDBACK_ERROR = "An error occured. Please try again.";
    public static string BOOKMARK = "Bookmark";
    public static string NO_BOOKMARK = "Remove Bookmark";
    public static string CONTENT_BOOKMARKED = "Successfully Bookmarked";
    public static string CONTENT_REMOVED_BOOKMARK = "Removed from Bookmarked";
    public static string YES = "YES";
    public static string NO = "NO";

}
