﻿using System;

[Serializable]
public class FlixContentModel
{
    public string idContent;
    public string Type;
    public string Status;
    public string ContentName;
    public string AuthorCopyright;
    public string UpdateTime;
    public string NamePrefix;
    public string GivenName;
    public string SurName;
    public string ThumbnailImageUrl;
    public string ContentRatingAvg;
    public string ContentRatingCount;
    public string ContentRatingLikes;
    public string ContentRatingDislike;
    public string IsContentBookmarkedYN;
}