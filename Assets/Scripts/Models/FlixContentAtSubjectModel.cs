﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlixContentAtSubjectModel {

    public string idCourseSubject;
    public string SubjectName;
    public string idCourseSubjectChapter;
    public string ChapterName;
    public string idTopic;
    public string TopicName;
    public string idContent;
    public string ContentName;
    public string AuthorCopyright;
    public string ThumbnailImageUrl;
    public string NamePrefix;
    public string GivenName;
    public string SurName;
    public string Rating;
    public string Url;

}
