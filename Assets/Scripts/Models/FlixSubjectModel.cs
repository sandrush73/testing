﻿using System;
using System.Collections.Generic;
[Serializable]
public class FlixSubjectModel
{
    public string idCourseSubject;
    public string SubjectName;
    public string SubjectStatus;
    public string ScoreRed;
    public string ScoreAmber;
    public string SubjectScoreRed;
    public string SubjectScoreAmber;
    public string ScoreLevelPassing;
    public List<FlixChapterModel> chapters;
}