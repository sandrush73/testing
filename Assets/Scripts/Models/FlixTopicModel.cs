﻿using System;
using System.Collections.Generic;
[Serializable]
public class FlixTopicModel
{
    public string idTopic;
    public string TopicName;
    public string TopicStatus;
    public string TopicSortOrder;
    public string color;
    public string EarnedMarks;
    public string TotalTestedMarks;
    public List<FlixContentModel> contents;
}