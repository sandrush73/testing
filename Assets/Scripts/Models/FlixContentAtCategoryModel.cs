﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlixContentAtCategoryModel {

    public string idContent;
    public string ContentName;
    public string idTopic;
    public string TopicName;
    public string idCourseSubjectChapter;
    public string ChapterName;
    public string idCourseSubject;
    public string SubjectName;
    public string Url;
    public string ViewCount;
    public string AuthorTitle;
    public string AuthorGivenName;
    public string AuthorSurName;
    public string AuthorImageUrl;
    public string AuthorSourceTitle;
    public string AuthorSourceGivenName;
    public string AuthorSourceSurName;
    public string AuthorSourceCreditUrl;
    public string AuthorDisplayName;
    public string AuthorSourceDisplayName;
}
