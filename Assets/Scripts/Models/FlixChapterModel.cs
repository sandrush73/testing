﻿using System;
using System.Collections.Generic;
[Serializable]
public class FlixChapterModel
{
    public string idCourseSubjectChapter;
    public string ChapterName;
    public string ChapterStatus;
    public string ChapterSortOrder;
    public string color;
    public string EarnedMarks;
    public string TotalTestedMarks;
    public List<FlixTopicModel> topics;
}