﻿using System;
using System.Collections.Generic;

[Serializable]
class FlixContentIndexModel
{
    public string idEntity;
    public string idCourse;
    public string CourseName;
    public string CourseStatus;
    public List<FlixSubjectModel> Subjects;
}