﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectOrbit : MonoBehaviour
{
    public Transform target, Target_Zoom, MainObject;
    public Transform cam;
    public Vector3 offset = Vector3.zero, touch1Dir, touch2Dir;
    public float cameraRotSide;
    public float cameraRotUp;
    public float cameraRotSideCur;
    public float cameraRotUpCur;
    public float distance, Distance_X, Distance_Y, X_dist, Y_dist, MulFactr_X, MulFactr_Y;
    public bool reset_pos;
    public float pinchSpeed;
    private Touch touch;
    private float lastDist = 0, dotProduct;
    private float curDist = 0;
    public float initial_dist;
    public float c;
    public float zoomStart = 1f;
    public float zoomMin = 0.5f, Zoom_X_Min, Zoom_Y_Min;
    public float zoomMax = 2f, Zoom_X_Max, Zoom_Y_Max, Pan_X, Pan_Y, Pan_X_Min, Pan_X_Max, Pan_Y_Min, Pan_Y_Max;
    public float cameraRotSideStart;
    public float cameraRotUpStart;
    public float CurentZoom;
    public bool zoom_In;
    public bool zoom_Out;
    public bool orbit_enabled;
    public bool Zoom_bool;
    public bool pan_bool, DisableInteraction;

    public float OrbitTouchMoveSpd, OrbitTouchEndSpd, ZoomPinchSpeed_X_Fac, PanSpeed, Target_Y_Value, ZoomMax_Y_Value, RotUp_Min, RotUp_Max, RotSide_Min, RotSide_Max;

    public void Start()
    {

        target = this.transform;
        cam = Camera.main.transform;

        pinchSpeed = 0.0012f;
        cameraRotSide = cameraRotSideStart;
        cameraRotUp = cameraRotUpStart;
        cameraRotUpCur = transform.localEulerAngles.x;
        cameraRotSideCur = transform.localEulerAngles.y;
        distance = zoomStart;
        reset_pos = false;
        orbit_enabled = true;
        Zoom_bool = true;
        pan_bool = false;
        DisableInteraction = false;
        OrbitTouchMoveSpd = 0.1f;
        OrbitTouchEndSpd = 0.25f;
        ZoomPinchSpeed_X_Fac = 0.33f;
        PanSpeed = 0.00142f;
        //Target_Y_Value = 0.5f;
        //ZoomMax_Y_Value = 0.5f;

        Pan_X_Min = -0.5f;
        Pan_X_Max = 0.5f;
        Pan_Y_Min = -0.3f;
        Pan_Y_Max = 0.5f;
        RotUp_Max = 0f;
        RotUp_Min = 0f;
        RotSide_Min = -180;
        RotSide_Max = 180;


    }

    void Update()
    {


        if (DisableInteraction)
            return;

        if (orbit_enabled)
        {


            CurentZoom = cam.localPosition.z;

            if (Application.platform != RuntimePlatform.Android && Application.platform != RuntimePlatform.IPhonePlayer)
            {

                if (Input.GetMouseButton(0) && !pan_bool)
                {
                    cameraRotSide += Input.GetAxis("Mouse X") * 2.5f;
                    cameraRotUp -= Input.GetAxis("Mouse Y") * 2.5f;

                }

                if ((Input.touchCount == 1))
                {
                    if (Input.GetTouch(0).phase == TouchPhase.Moved)
                    {
                        cameraRotSide += Input.touches[0].deltaPosition.x * OrbitTouchMoveSpd;
                        cameraRotUp -= Input.touches[0].deltaPosition.y * OrbitTouchMoveSpd;
                    }

                    if (Input.GetTouch(0).phase == TouchPhase.Ended)
                    {
                        cameraRotSide += Input.touches[0].deltaPosition.x * OrbitTouchEndSpd;
                        cameraRotUp -= Input.touches[0].deltaPosition.y * OrbitTouchEndSpd;
                    }

                }

            }
            else
            {


                if ((Input.touchCount == 1))
                {
                    if (Input.GetTouch(0).phase == TouchPhase.Moved)
                    {
                        cameraRotSide += Input.touches[0].deltaPosition.x * OrbitTouchMoveSpd;
                        cameraRotUp -= Input.touches[0].deltaPosition.y * OrbitTouchMoveSpd;
                    }

                    if (Input.GetTouch(0).phase == TouchPhase.Ended)
                    {
                        cameraRotSide += Input.touches[0].deltaPosition.x * OrbitTouchEndSpd;
                        cameraRotUp -= Input.touches[0].deltaPosition.y * OrbitTouchEndSpd;
                    }

                }
            }
        }

        if (Zoom_bool)
        {
            if (Application.platform != RuntimePlatform.Android && Application.platform != RuntimePlatform.IPhonePlayer)
            {

                //



                if (Input.touchCount > 1 && (Input.GetTouch(0).phase == TouchPhase.Moved || Input.GetTouch(1).phase == TouchPhase.Moved))
                {
                    touch1Dir = Input.touches[0].deltaPosition.normalized;
                    touch2Dir = Input.touches[1].deltaPosition.normalized;
                    dotProduct = Vector2.Dot(touch1Dir, touch2Dir);
                    //print("zoom.........dot........" + dotProduct + "........xis........." + Input.GetAxis("Mouse X"));
                    if (dotProduct < 0)
                    {
                        var touch1 = Input.GetTouch(0);
                        var touch2 = Input.GetTouch(1);
                        curDist = Vector2.Distance(touch1.position, touch2.position);

                        if (curDist > lastDist)
                        {
                            distance -= Vector2.Distance(touch1.deltaPosition, touch2.deltaPosition) * pinchSpeed;
                            Distance_X += Vector2.Distance(touch1.deltaPosition, touch2.deltaPosition) * MulFactr_X * pinchSpeed * ZoomPinchSpeed_X_Fac;
                            Distance_Y += Vector2.Distance(touch1.deltaPosition, touch2.deltaPosition) * MulFactr_Y * pinchSpeed;
                        }
                        else
                        {
                            distance += Vector2.Distance(touch1.deltaPosition, touch2.deltaPosition) * pinchSpeed;
                            Distance_X -= Vector2.Distance(touch1.deltaPosition, touch2.deltaPosition) * MulFactr_X * pinchSpeed * ZoomPinchSpeed_X_Fac;
                            Distance_Y -= Vector2.Distance(touch1.deltaPosition, touch2.deltaPosition) * MulFactr_Y * pinchSpeed;
                        }

                        lastDist = curDist;
                    }
                }



                distance *= (1 - 1 * Input.GetAxis("Mouse ScrollWheel"));
                Distance_X += 0.4f * Input.GetAxis("Mouse ScrollWheel") * MulFactr_X;
                Distance_Y += 0.5f * Input.GetAxis("Mouse ScrollWheel") * MulFactr_Y;

                //print("x......."+ Input.GetAxis("Mouse ScrollWheel"));
            }
            else
            {
                if (Input.touchCount > 1 && (Input.GetTouch(0).phase == TouchPhase.Moved || Input.GetTouch(1).phase == TouchPhase.Moved))
                {


                    touch1Dir = Input.touches[0].deltaPosition.normalized;
                    touch2Dir = Input.touches[1].deltaPosition.normalized;
                    dotProduct = Vector2.Dot(touch1Dir, touch2Dir);

                    if (dotProduct < 0)
                    {
                        var touch1 = Input.GetTouch(0);
                        var touch2 = Input.GetTouch(1);
                        curDist = Vector2.Distance(touch1.position, touch2.position);

                        if (curDist > lastDist)
                        {
                            distance -= Vector2.Distance(touch1.deltaPosition, touch2.deltaPosition) * pinchSpeed;
                            Distance_X += Vector2.Distance(touch1.deltaPosition, touch2.deltaPosition) * MulFactr_X * pinchSpeed * ZoomPinchSpeed_X_Fac;
                            Distance_Y += Vector2.Distance(touch1.deltaPosition, touch2.deltaPosition) * MulFactr_Y * pinchSpeed;
                        }
                        else
                        {
                            distance += Vector2.Distance(touch1.deltaPosition, touch2.deltaPosition) * pinchSpeed;
                            Distance_X -= Vector2.Distance(touch1.deltaPosition, touch2.deltaPosition) * MulFactr_X * pinchSpeed * ZoomPinchSpeed_X_Fac;
                            Distance_Y -= Vector2.Distance(touch1.deltaPosition, touch2.deltaPosition) * MulFactr_Y * pinchSpeed;
                        }

                        lastDist = curDist;
                    }
                }
            }

            if (zoom_In)
            {
                distance -= 0.01f;
                Distance_X -= 0.01f;
                Distance_Y += 0.01f;
            }
            if (zoom_Out)
            {
                distance += 0.01f;
                Distance_X += 0.01f;
                Distance_Y -= 0.01f;
            }
        }

        if (pan_bool)
        {



            if (Application.platform != RuntimePlatform.Android && Application.platform != RuntimePlatform.IPhonePlayer)
            {



                if (Input.GetMouseButton(0))
                {



                    Pan_X += -Input.GetAxis("Mouse X") * 0.8f * distance / 30;
                    Pan_Y += -Input.GetAxis("Mouse Y") * 0.8f * distance / 30;
                    cam.transform.localPosition = new Vector3(cam.transform.localPosition.x + Pan_X, cam.transform.localPosition.y + Pan_Y, cam.transform.localPosition.z);
                    cam.transform.localPosition = new Vector3(Mathf.Clamp(cam.transform.localPosition.x, Pan_X_Min, Pan_X_Max), Mathf.Clamp(cam.transform.localPosition.y, Pan_Y_Min, Pan_Y_Max), cam.transform.localPosition.z);
                }



                if ((Input.touchCount == 1) && (Input.GetTouch(0).phase == TouchPhase.Moved))
                {
                    if (Input.GetMouseButton(0))
                    {
                        Pan_X += -Input.touches[0].deltaPosition.x * distance * PanSpeed;
                        Pan_Y += -Input.touches[0].deltaPosition.y * distance * PanSpeed;
                        cam.transform.localPosition = new Vector3(cam.transform.localPosition.x + Pan_X, cam.transform.localPosition.y + Pan_Y, cam.transform.localPosition.z);
                        cam.transform.localPosition = new Vector3(Mathf.Clamp(cam.transform.localPosition.x, Pan_X_Min, Pan_X_Max), Mathf.Clamp(cam.transform.localPosition.y, Pan_Y_Min, Pan_Y_Max), cam.transform.localPosition.z);



                    }
                    //print("dot........" + dotProduct + "........xis........." + x);
                }



            }
            else
            {



                if ((Input.touchCount == 1) && (Input.GetTouch(0).phase == TouchPhase.Moved))
                {
                    if (Input.GetMouseButton(0))
                    {
                        Pan_X += -Input.touches[0].deltaPosition.x * distance * PanSpeed;
                        Pan_Y += -Input.touches[0].deltaPosition.y * distance * PanSpeed;
                        cam.transform.localPosition = new Vector3(cam.transform.localPosition.x + Pan_X, cam.transform.localPosition.y + Pan_Y, cam.transform.localPosition.z);
                        cam.transform.localPosition = new Vector3(Mathf.Clamp(cam.transform.localPosition.x, Pan_X_Min, Pan_X_Max), Mathf.Clamp(cam.transform.localPosition.y, Pan_Y_Min, Pan_Y_Max), cam.transform.localPosition.z);
                    }
                }
            }
        }

        if (distance <= zoomMin)
        {
            distance = zoomMin;
            pan_bool = true;
            orbit_enabled = false;
            reset_pos = false;
            Pan_X = 0;
            Pan_Y = 0;
        }
        else if (distance >= zoomMax)
        {
            distance = zoomMax;
        }
        else if (distance > zoomMin && !reset_pos)
        {
            pan_bool = false;
            orbit_enabled = true;
            if (Vector3.Distance(cam.transform.localPosition, new Vector3(0, 0, cam.transform.localPosition.z)) > 0.01f)
            {
                cam.transform.localPosition = Vector3.Lerp(cam.transform.localPosition, new Vector3(0, 0, cam.transform.localPosition.z), 1.5f * Time.deltaTime);
            }
            else
            {
                cam.transform.localPosition = new Vector3(0, 0, cam.transform.localPosition.z);
                reset_pos = true;

            }
            //iTween.MoveTo(cam.gameObject,iTween.Hash("x",0,"y",0,"time",2f,"easetype",iTween.EaseType.easeInOutSine,"islocal",true));
        }


        if (Distance_X <= Zoom_X_Min)
        {
            Distance_X = Zoom_X_Min;
        }
        else if (Distance_X >= Zoom_X_Max)
        {
            Distance_X = Zoom_X_Max;
        }

        if (Distance_Y <= Zoom_Y_Min)
        {
            Distance_Y = Zoom_Y_Min;
        }
        else if (Distance_Y >= Zoom_Y_Max)
        {
            Distance_Y = Zoom_Y_Max;
        }

        if (cam.GetComponent<Camera>().orthographicSize <= zoomMin)
        {
            cam.GetComponent<Camera>().orthographicSize = zoomMin;
        }
        else if (cam.GetComponent<Camera>().orthographicSize >= zoomMax)
        {
            cam.GetComponent<Camera>().orthographicSize = zoomMax;
        }

        if (cameraRotUp < RotUp_Min)
        {
            cameraRotUp = RotUp_Min;
        }
        if (cameraRotUp > RotUp_Max)
        {
            cameraRotUp = RotUp_Max;
        }


        if (cameraRotSide > 180)
            cameraRotSide = -180;
        if (cameraRotSide < -180)
            cameraRotSide = +180;

        if (cameraRotSide < RotSide_Min)
        {
            cameraRotSide = RotSide_Min;
        }
        if (cameraRotSide > RotSide_Max)
        {
            cameraRotSide = RotSide_Max;
        }

        //if (orbit_enabled) {
        cameraRotSideCur = Mathf.LerpAngle(cameraRotSideCur, cameraRotSide, Time.deltaTime * 5);
        cameraRotUpCur = Mathf.Lerp(cameraRotUpCur, cameraRotUp, Time.deltaTime * 5);
        //	}
        Vector3 targetPoint = target.position;

        if (Target_Zoom)
        {
            X_dist = Mathf.Lerp(transform.position.x, Distance_X, Time.deltaTime * 5);
            Y_dist = Mathf.Lerp(transform.position.y, Distance_Y, Time.deltaTime * 5);
            transform.position = new Vector3(X_dist, Y_dist, 0);
        }
        else
        {
            transform.position = Vector3.Lerp(transform.position, new Vector3(0, Target_Y_Value, 0), Time.deltaTime);
        }

        transform.rotation = Quaternion.Euler(cameraRotUp, cameraRotSideCur, 0);


        float dist = Mathf.Lerp(-cam.transform.localPosition.z, distance, Time.deltaTime * 5);
        cam.localPosition = new Vector3(cam.localPosition.x, cam.localPosition.y, -dist);
        c = -dist;
        //}
    }


    public void ChangeTarget(Transform ToTarget)
    {
        Target_Zoom = ToTarget;

        //print("to......."+ToTarget);
        if (ToTarget == null)
        {
            Distance_X = 0f;
            Distance_Y = ZoomMax_Y_Value;
            //target.position = new Vector3(0, Target_Y_Value, 0);
            return;
        }

        Distance_X = ToTarget.position.x;
        Distance_Y = ToTarget.position.y;

        if (ToTarget.position.x >= 0)
        {
            Zoom_X_Min = 0f;
            Zoom_X_Max = ToTarget.position.x;
            MulFactr_X = +1;
        }
        else
        {
            Zoom_X_Min = ToTarget.position.x;
            Zoom_X_Max = 0f;
            MulFactr_X = -1;
        }

        if (ToTarget.position.y >= ZoomMax_Y_Value)
        {
            Zoom_Y_Min = ZoomMax_Y_Value;
            Zoom_Y_Max = ToTarget.position.y;
            MulFactr_Y = +1;
        }
        else
        {
            Zoom_Y_Min = ToTarget.position.y;
            Zoom_Y_Max = ZoomMax_Y_Value;
            MulFactr_Y = -1;
        }
    }
}