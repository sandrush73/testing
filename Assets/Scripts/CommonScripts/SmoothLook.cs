﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmoothLook : MonoBehaviour
{
    [SerializeField]
    public Transform Target;

    void Start()
    {
        Target = Camera.main.transform;
    }

    void Update()
    {
        transform.LookAt(Target);
        transform.localRotation *= Quaternion.Euler(0, 180, 0);
    }

}