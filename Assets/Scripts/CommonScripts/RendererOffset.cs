﻿using UnityEngine;

public class RendererOffset : MonoBehaviour
{
    float scrollSpeed = 0.5f;
    Renderer rend;
    float offset;
    void Start()
    {
        
        rend = GetComponent<Renderer>();
        //Init = rend.materials[0].GetTextureOffset("_MainTex");
        offset = -0.4f;

         rend.materials[0].SetTextureOffset("_MainTex", new Vector2(0, -0.4f));
    }

    void Update()
    {
        offset += Time.deltaTime * scrollSpeed;
        rend.materials[0].SetTextureOffset("_MainTex", new Vector2(0, offset));
    }

   

}