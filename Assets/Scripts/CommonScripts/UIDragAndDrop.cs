﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System.Linq;


public class UIDragAndDrop : MonoBehaviour
{
    public List<GameObject> TargetObj, FinalObj, Arrow;
    public List<Vector3> FinalPos, FinalRot;
    public GameObject ObjTODrag, MainObj;
    public Vector3 screenSpace, offSet, curScreenSpace, curPosition;
    public Vector3 Init_Pos, Final_Pos, Init_Rot, Final_Rot;
    public bool dragging, IsLiquid, IsAR;
    public int CurrObj_Cnt, CurrentStepCount;
    public float MovingTime;
    public orbit CamOrbit;
    public AROrbitControls ArOrbit;

    RaycastHit hit;


    // Start is called before the first frame update
    void Start()
    {
        Invoke("CallStart", 0.02f);
    }

    public void CallStart()
    {
        MainObj = GameObject.Find("MainObject");


        TargetObj = GameObject.FindObjectsOfType(typeof(GameObject)).Select(g => g as GameObject).Where(g => g.name.Contains(NameChange("_UI", "_Target"))).ToList();
        FinalObj = GameObject.FindObjectsOfType(typeof(GameObject)).Select(g => g as GameObject).Where(g => g.name.Contains(NameChange("_UI", "_Final"))).ToList();
        Arrow = GameObject.FindObjectsOfType(typeof(GameObject)).Select(g => g as GameObject).Where(g => g.name.Contains(NameChange("_UI", "_Arrow"))).ToList();

        PlayerPrefs.SetInt("CurrentStepCount", 0);

        TargetObj = TargetObj.OrderBy(x => x.name).ToList();
        FinalObj = FinalObj.OrderBy(x => x.name).ToList();
        Arrow = Arrow.OrderBy(x => x.name).ToList();

        CurrObj_Cnt = 0;

        for (int i = 0; i < TargetObj.Count; i++)
        {
            FinalObj[i].SetActive(false);
            TargetObj[i].SetActive(false);
            TargetObj[i].GetComponent<BoxCollider>().enabled = true;
            iTween.MoveAdd(Arrow[i], iTween.Hash("y", 0.15f, "time", 0.5f, "islocal", true, "looptype", iTween.LoopType.pingPong, "easetype", iTween.EaseType.easeInOutSine));
            Arrow[i].SetActive(false);
            transform.GetChild(1).GetChild(1).GetComponent<Image>().enabled = false;
            FinalPos.Add(FinalObj[i].transform.localPosition);
            FinalRot.Add(FinalObj[i].transform.localEulerAngles);
        }

        Final_Pos = FinalObj[CurrObj_Cnt].transform.localPosition;
        Final_Rot = FinalObj[CurrObj_Cnt].transform.localEulerAngles;

        AddListener(EventTriggerType.PointerDown, MouseDown2, this.gameObject);
        AddListener(EventTriggerType.BeginDrag, MouseDown, this.gameObject);
        AddListener(EventTriggerType.EndDrag, MouseUp, this.gameObject);
        AddListener(EventTriggerType.PointerUp, MouseUp, this.gameObject);

        if (GameObject.Find("Target"))
        {
            CamOrbit = GameObject.Find("Target").GetComponent<orbit>();
            IsAR = false;
        }
        else
        {
            ArOrbit = MainObj.transform.parent.gameObject.GetComponent<AROrbitControls>();
            IsAR = true;
        }
    }

    public void Restart()
    {

        PlayerPrefs.SetInt("CurrentStepCount", 0);

        CurrObj_Cnt = 0;
        transform.GetChild(1).GetChild(1).GetComponent<Image>().enabled = false;
        ObjTODrag = null;
        for (int i = 0; i < TargetObj.Count; i++)
        {
            FinalObj[i].SetActive(false);
            TargetObj[i].SetActive(false);
            TargetObj[i].GetComponent<BoxCollider>().enabled = true;
            iTween.MoveAdd(Arrow[i], iTween.Hash("y", 0.15f, "time", 0.5f, "islocal", true, "looptype", iTween.LoopType.pingPong, "easetype", iTween.EaseType.easeInOutSine));
            Arrow[i].SetActive(false);
            FinalObj[i].transform.localPosition = FinalPos[i];
            FinalObj[i].transform.localEulerAngles = FinalRot[i];
        }

        GetComponent<Image>().raycastTarget = true;
    }


    public string NameChange(string ToRemove, string ToAdd)
    {
        string Name = "";
        Name = transform.name.Replace(ToRemove, ToAdd);


        return Name;

    }
    public void MouseDown2()
    {
        //dragging = true;
        DisableOrbit();
    }
    public void MouseDown()
    {



        DisableOrbit();

        ObjTODrag = Instantiate(FinalObj[CurrObj_Cnt], FinalObj[CurrObj_Cnt].transform.localPosition, FinalObj[CurrObj_Cnt].transform.localRotation) as GameObject;
        print("objtodrag.........." + ObjTODrag);
        ObjTODrag.gameObject.SetActive(true);

        screenSpace = Camera.main.WorldToScreenPoint(ObjTODrag.transform.localPosition);



        curScreenSpace = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenSpace.z);
        curPosition = Camera.main.ScreenToWorldPoint(curScreenSpace);

        Init_Pos = curPosition;

        ObjTODrag.transform.localPosition = curPosition;


        TargetObj[CurrObj_Cnt].SetActive(true);
        Arrow[CurrObj_Cnt].SetActive(true);

        dragging = true;

    }

    void Update()
    {
        if (dragging)
        {
            curScreenSpace = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenSpace.z);
            curPosition = Camera.main.ScreenToWorldPoint(curScreenSpace);
            // curPosition = new Vector3 (curPosition.x, Mathf.Clamp (curPosition.y, intialGroundTouchvalue+GroundTouchValue, 100f), curPosition.z);
            ObjTODrag.transform.localPosition = new Vector3(curPosition.x, curPosition.y, curPosition.z);


        }


    }

    public void MouseUp()
    {
        EnableOrbit();

        if (!ObjTODrag || !dragging)
            return;

        //iTween.RotateTo(ObjTODrag, iTween.Hash("x", Init_Rot.x, "y", Init_Rot.y, "z", Init_Rot.z, "time", 0.5f, "islocal", true, "easetype", iTween.EaseType.linear));
        if (PlayerPrefs.GetInt("CurrentStepCount") == CurrentStepCount)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit, 100000))
            {

                if (hit.collider.name == TargetObj[CurrObj_Cnt].name)
                {
                    Final_Pos = FinalObj[CurrObj_Cnt].transform.localPosition;
                    Final_Rot = FinalObj[CurrObj_Cnt].transform.localEulerAngles;

                    if (!IsLiquid)
                    {
                        iTween.MoveTo(ObjTODrag, iTween.Hash("x", Final_Pos.x, "y", Final_Pos.y, "z", Final_Pos.z, "time", 0.1f, "oncomplete", "UpdateNextStep", "oncompletetarget", this.gameObject, "islocal", true, "easetype", iTween.EaseType.linear));
                        iTween.RotateTo(ObjTODrag, iTween.Hash("x", Final_Rot.x, "y", Final_Rot.y, "z", Final_Rot.z, "time", 0.1f, "islocal", true, "easetype", iTween.EaseType.linear));
                    }
                    else
                    {
                        ObjTODrag.SetActive(false);
                        UpdateNextStep();
                    }

                }
                else
                {
                    MainObj.SendMessage("WrongPlacement");

                    iTween.MoveTo(ObjTODrag, iTween.Hash("x", Init_Pos.x, "y", Init_Pos.y, "z", Init_Pos.z, "time", MovingTime, "oncomplete", "ResetToInitPos", "oncompletetarget", this.gameObject, "islocal", true, "easetype", iTween.EaseType.linear));

                }

            }
            else if (dragging && ObjTODrag)
            {
                MainObj.SendMessage("WrongPlacement");
                iTween.MoveTo(ObjTODrag, iTween.Hash("x", Init_Pos.x, "y", Init_Pos.y, "z", Init_Pos.z, "time", MovingTime, "oncomplete", "ResetToInitPos", "oncompletetarget", this.gameObject, "islocal", true, "easetype", iTween.EaseType.linear));

            }

        }
        else
        {
            MainObj.SendMessage("WrongDragDrop");
            iTween.MoveTo(ObjTODrag, iTween.Hash("x", Init_Pos.x, "y", Init_Pos.y, "z", Init_Pos.z, "time", MovingTime, "oncomplete", "ResetToInitPos", "oncompletetarget", this.gameObject, "islocal", true, "easetype", iTween.EaseType.linear));
        }

        dragging = false;

        //TargetObj[CurrObj_Cnt].SetActive(false);
    }

    public void UpdateNextStep()
    {
        for (int i = 0; i < TargetObj.Count; i++)
        {
            Arrow[i].SetActive(false);
            TargetObj[i].SetActive(false);
        }

        Destroy(ObjTODrag);


        TargetObj[CurrObj_Cnt].GetComponent<BoxCollider>().enabled = false;
        FinalObj[CurrObj_Cnt].SetActive(true);

        //this.gameObject.SetActive(false);
        this.transform.SetAsLastSibling();// SetActive(false);
        transform.GetChild(1).GetChild(1).GetComponent<Image>().enabled = true;




        if (CurrObj_Cnt < FinalObj.Count - 1)
        {
            CurrObj_Cnt++;
        }

        GetComponent<Image>().raycastTarget = false;


        ObjTODrag = null;
        MainObj.SendMessage("PlaySequenceStep");
        print("update to next step.....");
    }

    public void ResetToInitPos()
    {

        Destroy(ObjTODrag);
        for (int i = 0; i < TargetObj.Count; i++)
        {
            Arrow[i].SetActive(false);
            TargetObj[i].SetActive(false);
        }
    }

    public void EnableOrbit()
    {

        if (!IsAR)
        {
            CamOrbit.DisableInteration = false;
            //print("EnableOrbit");
        }
        else
        {
            ArOrbit.DisableInteration = false;
        }
    }

    public void DisableOrbit()
    {

        if (!IsAR)
        {
            CamOrbit.DisableInteration = true;
            //print("DisableOrbit");
        }
        else
        {
            ArOrbit.DisableInteration = true;
        }
    }

    public void AddListener(EventTriggerType eventType, Action MethodToCall, GameObject TriggerObjToAdd)
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall());
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }
}
