﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AROrbitControls : MonoBehaviour
{

    //public GameObject mainObject;
    private float RotSide, RotSideCur, distance, pinchSpeed;
    private float lastDist = 0, ZoomMin, ZoomMax;
    private float curDist = 0;
    private Vector3 offset = Vector3.zero;
    private Touch touch;
    public bool DisableInteration;

    public void Start()
    {
       

        if (SceneManager.GetActiveScene().buildIndex == 3)
        {
            pinchSpeed = 0.0012f;
            //DisableInteration = true;

            //transform.rotation = Quaternion.identity;


            //Invoke("EnableInteraction", 2f);

            ZoomMin = 0.25f;
            ZoomMax = 1.25f;
        }
        else if (SceneManager.GetActiveScene().buildIndex == 1)
        {
            pinchSpeed = 0.0012f * 10;

            ZoomMin = 13f;
            ZoomMax = 25f;

        }
        RotSideCur = transform.rotation.y;
        DisableInteration = false;
        distance = (ZoomMax- ZoomMin) / 2;
    }

    void EnableInteraction()
    {
        RotSideCur = transform.rotation.y;
        DisableInteration = false;
    }
    // Update is called once per frame
    void Update()
    {
        if (DisableInteration)
            return;

        if (Application.platform != RuntimePlatform.Android && Application.platform != RuntimePlatform.IPhonePlayer)
        {
            if (Input.GetMouseButton(0))
            {
                RotSide -= Input.GetAxis("Mouse X") * 5;
            }
        }
        else
        {
            if ((Input.touchCount == 1) && (Input.GetTouch(0).phase == TouchPhase.Moved))
            {
                RotSide -= Input.touches[0].deltaPosition.x;
            }
        }

        if (Application.platform != RuntimePlatform.Android && Application.platform != RuntimePlatform.IPhonePlayer)
        {
            distance *= (1 - 1 * Input.GetAxis("Mouse ScrollWheel"));
        }
        else
        {
            if (Input.touchCount > 1 && (Input.GetTouch(0).phase == TouchPhase.Moved || Input.GetTouch(1).phase == TouchPhase.Moved))
            {
                var touch1 = Input.GetTouch(0);
                var touch2 = Input.GetTouch(1);
                curDist = Vector2.Distance(touch1.position, touch2.position);
                if (curDist > lastDist)
                {
                    distance += Vector2.Distance(touch1.deltaPosition, touch2.deltaPosition) * pinchSpeed;
                }
                else
                {
                    distance -= Vector2.Distance(touch1.deltaPosition, touch2.deltaPosition) * pinchSpeed;
                }
                lastDist = curDist;
            }
        }

        RotSideCur = Mathf.LerpAngle(RotSideCur, RotSide, Time.deltaTime * 2.5f);

        transform.rotation = Quaternion.Euler(0, RotSideCur, 0);

        distance = Mathf.Clamp(distance, ZoomMin, ZoomMax);

        float dist = Mathf.Lerp(transform.localScale.z, distance, Time.deltaTime * 5);

        //print("dist......"+dist);

        transform.localScale = new Vector3(dist, dist, dist);
    }
}