﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasRotate : MonoBehaviour
{

    Quaternion initialRotation;
    private void Start()
    {
        initialRotation = transform.localRotation;
    }
    // Update is called once per frame
    void Update()
    {
        // transform.LookAt(transform.position + Camera.main.transform.rotation * Vector3.forward, Camera.main.transform.rotation * Vector3.up);
        //transform.LookAt(Camera.main.transform);
        transform.rotation = Quaternion.Lerp(transform.rotation, GameObject.FindGameObjectWithTag("MainCamera").transform.rotation, Time.deltaTime * 5); ;
    }
}
