﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;
using System.Linq;
using System;
using System.Text.RegularExpressions;
using UnityEngine.SceneManagement;

public class vid6199_electriccell : MonoBehaviour
{
    public static vid6199_electriccell Instance;
    public GameObject Beaker,Vinegar,CopperPlate,ZincPlate,Lamp, Canvas_BG,Labels,Ground;
    public int CurrentStepCnt,DragDropSteps;


    public TextAsset CSVFile;
    public Material TransparentMaterial;
    public bool IsAR, IsClicked;
    public orbit CamOrbit;
    public AROrbitControls ArOrbit;

    public Button SidePannelOpen;
    public int OptionUnlockedTill, CurrentTopicSelected, TopicToCheck;
    public float Sub_XP_Earned, Total_XP;
    public Dropdown ToDoList_DD;
    public GameObject InstructionPanel_DD, ObjectivePanel, InstructionPanel, InfoPanel, GlassPanel, SidePannel,InventoryScroll;
    public Button Info_Close_Btn, Info_Cntue_Btn, Info_Open_Btn,ShowLabels_Btn,Reset_Btn;
    public List<Dictionary<string, object>> CSV_data;
    public List<String> Topics, Sub_Topics, Sub_Top_ToDoList, Sub_Top_Desc, Sub_Top_XP, LearningGoals;
    public Text XP_ToShow, XP_Effect, Main_XP_ToShow, Learninggoal_Txt, Instruction_Txt, Assumption_Txt, Heading_Txt,NumOfSteps;
    public bool IsTopicUnlocked;
    public Canvas UIcanvas;
    public AssetBundle assetBundle;

    public void Awake()
    {
        Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        CurrentStepCnt = 0;

        

        LoadFromAsssetBundle();

        LoadingData();

       
    }

    public void LoadingData()
    {
        ReadingDataFromCSVFile();

        ReadingXps();

        FindingGameObjects();

        
        AssignClickEventsToObjects();
    }

    
    public void LoadFromAsssetBundle()
    {
        //if (Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.Android)
        //{
        assetBundle = AssetBundle.LoadFromFile(PlayerPrefs.GetString("AssetBundleUrl"));

        CSVFile = assetBundle.LoadAsset(PlayerPrefs.GetString("LibraryName") + "CSV", typeof(TextAsset)) as TextAsset;

        TransparentMaterial = assetBundle.LoadAsset("TransparentMaterial", typeof(Material)) as Material;
        TransparentMaterial.shader = Shader.Find("Standard");





        //}
        //else
        //{

        //string CSVName = transform.parent.gameObject.name;



        //    if (CSVName.Contains("Clone"))
        //    {



        //        CSVName = CSVName.Substring(0, CSVName.Length - 7);
        //    }



        //    print("casvname........." + CSVName);



        //    CSVFile = Resources.Load(CSVName+"CSV") as TextAsset;
        //}







        Camera.main.gameObject.AddComponent<PhysicsRaycaster>();



        Camera.main.backgroundColor = Color.black;



        GameObject TargetForCamera = GameObject.Find("Target");



        if (TargetForCamera != null)
        {
            
            IsAR = false;
            TargetForCamera.AddComponent<orbit>();
            CamOrbit = TargetForCamera.GetComponent<orbit>();

            CamOrbit.zoomMax = 3f;
            CamOrbit.zoomMin = 1.3f;
            CamOrbit.zoomStart = 2f;
            CamOrbit.distance = CamOrbit.zoomStart;
            CamOrbit.transform.position = new Vector3(0, 0.25f, 0);

            CamOrbit.maxRotUp = 30;
            CamOrbit.minRotUp = 1;

            CamOrbit.maxSideRot = 70;
            CamOrbit.minSideRot = 70;
        }
        else
        {
            this.transform.parent.gameObject.AddComponent<AROrbitControls>();
            ArOrbit = this.transform.parent.gameObject.GetComponent<AROrbitControls>();
            IsAR = true;
        }



    }

    public void ReadingDataFromCSVFile()
    {
        Topics = new List<string>();
        Sub_Topics = new List<string>();
        Sub_Top_Desc = new List<string>();
        Sub_Top_ToDoList = new List<string>();
        Sub_Top_XP = new List<string>();
        LearningGoals = new List<string>();

        CSV_data = CSVReader.Read(CSVFile);


        for (var i = 0; i < CSV_data.Count; i++)
        {
            //print("Topics " + CSV_data[i]["Learninggoals"] + ".......... " +"ToDo " + CSV_data[i]["Topic_TodoList"]);


            if (CSV_data[i]["Topic_ToDoList"].ToString() != "" && CSV_data[i]["Topic_ToDoList"].ToString() != null)
            {
                Sub_Top_ToDoList.Add(CSV_data[i]["Topic_ToDoList"].ToString());

            }



            //if (CSV_data[i]["Top_XP"].ToString() != "" && CSV_data[i]["Top_XP"].ToString() != null)
            //{
            //    Sub_Top_XP.Add(CSV_data[i]["Top_XP"].ToString());

            //}

            if (CSV_data[i]["Learninggoals"].ToString() != "" && CSV_data[i]["Learninggoals"].ToString() != null)
            {
                LearningGoals.Add(CSV_data[i]["Learninggoals"].ToString());

            }

        }






    }


    public int totXp, loclXp, PerTopic_XP;

    public void ReadingXps()
    {

        //PlayerPrefs.SetInt("TotalXP", 34);
        //PlayerPrefs.SetInt("MaxEarnings", 39);

        totXp = PlayerPrefs.GetInt("TotalXP");
        loclXp = PlayerPrefs.GetInt("MaxEarnings");

        TopicToCheck = 0;

        if (loclXp != 0)
        {
            OptionUnlockedTill = 0;

        }
        else
        {
            OptionUnlockedTill = 1;

        }


        


        int t = 0;
        int p = 0;

        for (int k = 0; k < Sub_Top_ToDoList.Count; k++)
        {

            p = loclXp / Sub_Top_ToDoList.Count;
            Sub_Top_XP.Add(p.ToString());
            t = t + p;
        }

        if (t != loclXp)
        {
            Sub_Top_XP[Sub_Top_ToDoList.Count - 1] = (p + (loclXp - t)).ToString();
        }

        Total_XP = loclXp;
        

    }



    public float result;

    public void GetResult()
    {
        result = ((float)(TopicToCheck - 0) / (float)(Sub_Top_ToDoList.Count)) * 100;
        Camera.main.SendMessage("UnloadActually", result);
    }

    public void FindingGameObjects()
    {
        Beaker = GameObject.Find("Beaker_Final");
        Vinegar = GameObject.Find("Vinegar_Final");
        CopperPlate = GameObject.Find("CopperPlate_Final");
        ZincPlate = GameObject.Find("ZincPlate_Final");
        Lamp = GameObject.Find("Lamp_Final");

        Lamp.transform.GetChild(1).GetChild(0).gameObject.AddComponent<SmoothLook>();

        Canvas_BG = GameObject.Find("Canvas_BG");
        Labels = GameObject.Find("Labels");
        Labels.SetActive(false);
        for (int i = 0; i < Labels.transform.childCount; i++)
        {
            Labels.transform.GetChild(i).Find("SmoothLook").gameObject.AddComponent<SmoothLook>();
        }

        Ground = GameObject.Find("Ground");

        if (!IsAR)
        {
            Canvas_BG.transform.parent = Camera.main.transform;
            Canvas_BG.GetComponent<Canvas>().worldCamera = Camera.main;
        }

        else
        {
            Canvas_BG.SetActive(false);
            Ground.SetActive(false);
        }


        //Invoke("DeactivateObjects",0.01f);
        UIcanvas = GameObject.Find("Canvas_ElectricCell").GetComponent<Canvas>();
        SidePannel = GameObject.Find("SidePanel");
        //SidePannelOpen = GameObject.Find("PannelOpenButton").GetComponent<Button>();
        InstructionPanel_DD = GameObject.Find("DD_Instruction");
        InstructionPanel = GameObject.Find("InstructionPanel");
        InventoryScroll = GameObject.Find("InventoryScrollView");
        ToDoList_DD = GameObject.Find("ToDoList_Dropdown").GetComponent<Dropdown>();
        XP_ToShow = GameObject.Find("Gained_XP_Value").GetComponent<Text>();
        XP_Effect = GameObject.Find("Effect_Text").GetComponent<Text>();
        Main_XP_ToShow = GameObject.Find("Main_XP_Value").GetComponent<Text>();
        ObjectivePanel = GameObject.Find("ObjectivePanel");
        InstructionPanel.transform.localScale = Vector3.zero;
        InfoPanel = GameObject.Find("InformationPanel");
        Info_Close_Btn = GameObject.Find("InfoCloseBtn").GetComponent<Button>();
        Info_Cntue_Btn = GameObject.Find("InfoContinueBtn").GetComponent<Button>();
        Info_Open_Btn = GameObject.Find("InfoButton").GetComponent<Button>();
        Info_Close_Btn.gameObject.SetActive(false);
        GlassPanel = GameObject.Find("GlassEffect");
        Instruction_Txt = GameObject.Find("IListText").GetComponent<Text>();
        Assumption_Txt = GameObject.Find("AListText").GetComponent<Text>();
        Learninggoal_Txt = GameObject.Find("LGListText").GetComponent<Text>();
        Heading_Txt = GameObject.Find("InfoHeadingText").GetComponent<Text>();
        NumOfSteps = GameObject.Find("Slots_Text").GetComponent<Text>();
        ShowLabels_Btn = GameObject.Find("Labels_Show").GetComponent<Button>();
        Reset_Btn = GameObject.Find("Reset_Exp").GetComponent<Button>();


        if (OptionUnlockedTill == 0)
        {
            /*ToDoList_DD.GetComponent<RectTransform>().anchoredPosition = new Vector3(150f, -150f, 0);
            XP_ToShow.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(150f, -150f, 0);
            SidePannel.GetComponent<RectTransform>().anchoredPosition = new Vector3(-600f, 0f, 0);*/
            ObjectivePanel.transform.localScale = new Vector3(1, 0, 1);
            IsTopicUnlocked = false;
            Reset_Btn.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(-500f, -150f, 0);
        }
        else
        {
            /*ToDoList_DD.GetComponent<RectTransform>().anchoredPosition = new Vector3(-250f, -150f, 0);
            XP_ToShow.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(-250f, -150f, 0);
            SidePannel.GetComponent<RectTransform>().anchoredPosition = new Vector3(-600f, 0f, 0);*/
            ObjectivePanel.transform.localScale = new Vector3(1, 0, 1);
            IsTopicUnlocked = true;
            ShowLabels_Btn.GetComponent<Image>().color = new Color(1, 1, 1, 0.5f);
            Reset_Btn.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(10f, -150f, 0);
        }

        Main_XP_ToShow.text = totXp.ToString();

        XP_ToShow.text = "0/" + Total_XP;

        for (int i = 0; i < Sub_Top_ToDoList.Count; i++)
        {

            ToDoList_DD.options.Add(new Dropdown.OptionData() { text = Sub_Top_ToDoList[i] });


            //List<float> TotalXP = Sub_Top_XP.Select(s => float.Parse(s)).ToList();

            //Total_XP += TotalXP[i];

            //XP_ToShow.text = "0/" + Total_XP;


            ObjectivePanel.transform.Find("ObjectiveText").GetComponent<Text>().text = Sub_Top_ToDoList[TopicToCheck];




        }

        DragDropSteps = UIcanvas.GetComponentsInChildren<UIDragAndDrop>().Length;

        NumOfSteps.text = DragDropSteps.ToString();

        GlassPanel.transform.localScale = Vector3.zero;
        InfoPanel.transform.localScale = Vector3.zero;
        Invoke("OpenInfoPanel", 0.2f);

        Heading_Txt.text = LearningGoals[0];
        Learninggoal_Txt.text = LearningGoals[1];
        //Instruction_Txt.text = LearningGoals[2];
        //Assumption_Txt.text = LearningGoals[3];

        
    }

    public void AssignClickEventsToObjects()
    {


        



        //SidePannelOpen.onClick.AddListener(OpenSidePannel);

        AddListenerToEvents(EventTriggerType.PointerDown, DisableOrbit, InventoryScroll);

        AddListenerToEvents(EventTriggerType.PointerUp, EnableOrbit, InventoryScroll);

        AddListenerToEvents(EventTriggerType.PointerDown, DisableOrbit, InventoryScroll.transform.GetChild(2).gameObject);

        AddListenerToEvents(EventTriggerType.PointerUp, EnableOrbit, InventoryScroll.transform.GetChild(2).gameObject);

        ToDoList_DD.GetComponent<EventTrigger>().triggers.Clear();

        AddListenerToEvents(EventTriggerType.PointerClick, ExploringToDOList_DD, ToDoList_DD.gameObject);
        AddListenerToEvents(EventTriggerType.Select, ToDoList_DDCancel, ToDoList_DD.gameObject);

        Info_Close_Btn.onClick.AddListener(() => CloseInfoPanel(Info_Close_Btn));
        Info_Cntue_Btn.onClick.AddListener(() => CloseInfoPanel(Info_Cntue_Btn));
        Info_Open_Btn.onClick.AddListener(OpenInfoPanel);

        ShowLabels_Btn.onClick.AddListener(ShowLabels);
        Reset_Btn.onClick.AddListener(ResetExperiment);

        /*AddListenerToEvents(EventTriggerType.BeginDrag, OnElementBeginDrag, GameObject.Find("RawImage"));
        AddListenerToEvents(EventTriggerType.EndDrag, OnElementEndDrag, GameObject.Find("RawImage"));
        AddListenerToEvents(EventTriggerType.PointerClick, BackToPT, GameObject.Find("RawImage"));*/

    }


    public void AddListenerToEvents(EventTriggerType eventType, Action MethodToCall, GameObject TriggerObjToAdd)
    {

        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall());
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }

    public void AddListenerToEvents(EventTriggerType eventType, Action<float> MethodToCall, GameObject TriggerObjToAdd, float p)
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall(p));
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }

    public void AddListenerToEvents(EventTriggerType eventType, Action<GameObject> MethodToCall, GameObject TriggerObjToAdd, GameObject p)
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall(p));
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }
    public void OnElementBeginDrag()
    {
        IsClicked = true;
    }

    public void OnElementEndDrag()
    {
        //print("drag ended");
        IsClicked = false;
    }

    public void EnableOrbit()
    {

        if (!IsAR)
        {
            CamOrbit.DisableInteration = false;
            //print("EnableOrbit");
        }
        else
        {
            ArOrbit.DisableInteration = false;
        }
    }

    public void DisableOrbit()
    {

        if (!IsAR)
        {
            CamOrbit.DisableInteration = true;
            //print("DisableOrbit");
        }
        else
        {
            ArOrbit.DisableInteration = true;
        }
    }


    public void OpenInfoPanel()
    {
        UIZoomIn();
        iTween.ScaleTo(InfoPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 0.5f, "easetype", iTween.EaseType.spring));
        iTween.ScaleTo(GlassPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 0.01f, "easetype", iTween.EaseType.spring));

    }

    public void CloseInfoPanel(Button CloseBtn)
    {
        UIZoomOut();

        ObjectivePanel.transform.localScale = new Vector3(1, 0, 1);
        iTween.ScaleTo(ObjectivePanel, iTween.Hash("y", 1, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine));

        iTween.ScaleTo(InfoPanel, iTween.Hash("x", 0, "y", 0, "z", 0, "time", 0.2f, "easetype", iTween.EaseType.linear));
        iTween.ScaleTo(GlassPanel, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.2f, "time", 0.01f, "easetype", iTween.EaseType.linear));
        if (CloseBtn == Info_Cntue_Btn)
        {
            if (!IsAR)
                CamOrbit.DisableInteration = false;
            /*if (ActiveBtnCnt == 0)
            {
                for (int i = 0; i < Elements.Length; i++)
                {

                    Vector3 RandomPos = new Vector3(Elements[i].transform.localPosition.x, Elements[i].transform.localPosition.y, UnityEngine.Random.Range(-5f, 0f));
                    Vector3 CurrPos = Elements[i].transform.localPosition;

                    Elements[i].transform.localPosition = RandomPos;

                    iTween.MoveTo(Elements[i], iTween.Hash("position", CurrPos, "delay", 0.7f, "islocal", true, "time", UnityEngine.Random.Range(0.1f, 0.2f), "speed", 2.5f, "easetype", iTween.EaseType.easeInOutSine));


                }
            }*/
            Info_Close_Btn.gameObject.SetActive(true);
            Info_Cntue_Btn.gameObject.SetActive(false);
        }
        IsClicked = false;
    }

    public void CloseInstructionPanel()
    {
        UIcanvas.GetComponent<GraphicRaycaster>().enabled = true;
        Camera.main.GetComponent<PhysicsRaycaster>().enabled = true;
        //iTween.ScaleTo(GlassPanel, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 1f, "time", 0.01f, "easetype", iTween.EaseType.linear));
        iTween.ScaleTo(InstructionPanel, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.5f, "time", 0.3f, "easetype", iTween.EaseType.spring));
        iTween.ScaleTo(ObjectivePanel, iTween.Hash("y", 1, "time", 0.3f, "delay", 1f, "easetype", iTween.EaseType.easeInOutSine));
    }

    public void ToDoList_DDCancel()
    {
        if (ToDoList_DD.transform.Find("Dropdown List") != null)//&& Obj.transform.localScale.y!=1)
        {
            //print("cancelled........");
            //print("list.....cancelled........" + ToDoList_DD.transform.Find("Dropdown List").gameObject);
            ToDoList_DD.transform.Find("Arrow").transform.eulerAngles = new Vector3(0, 0, 0);
            iTween.ScaleTo(ToDoList_DD.transform.Find("Dropdown List").gameObject, iTween.Hash("y", 0, "time", 0.5f, "easetype", iTween.EaseType.spring));
            iTween.ScaleTo(ObjectivePanel, iTween.Hash("y", 1, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine));
            //if (SidePannelOpen.transform.eulerAngles.z == 180)
            //{
            //    OpenSidePannel();
            //}
        }
    }

    public void ExploringToDOList_DD()
    {
        //print("ShowDD.....");
        //ToDoList_DD.transform.Find("Topic_Objective").GetComponent<Text>().text = "";
        ToDoList_DD.transform.Find("Arrow").transform.eulerAngles = new Vector3(0, 0, 180);
        GameObject DDList = ToDoList_DD.transform.Find("Dropdown List").gameObject;
        DDList.transform.GetComponent<RectTransform>().sizeDelta = new Vector2(0, 250);
        DDList.transform.localScale = new Vector3(1, 0, 1);
        iTween.ScaleTo(DDList, iTween.Hash("y", 1, "time", 0.5f, "easetype", iTween.EaseType.spring));
        //iTween.ScaleTo(ToDoList_DD.transform.Find("Topic_Objective").gameObject, iTween.Hash("y", 0, "time", 0.5f, "easetype", iTween.EaseType.spring));
        if (!IsTopicUnlocked)
            iTween.ScaleTo(ObjectivePanel, iTween.Hash("y", 0, "time", 0.5f, "delay", 0.1f, "easetype", iTween.EaseType.easeInOutSine));

        string TestStr = "";
        int CntToChangePos = 0;
        for (int i = 0; i < ToDoList_DD.options.Count; i++)
        {
            GameObject Child = GameObject.Find("Item " + i + ": " + ToDoList_DD.options[i].text);
            //Child.transform.parent.GetComponent<RectTransform>().sizeDelta = new Vector2(0,250);

            if (TestStr == ToDoList_DD.options[i].text)
            {
                Child.SetActive(false);
                Child.transform.localPosition = Child.transform.localPosition + new Vector3(0, 50 * CntToChangePos, 0);
                CntToChangePos++;
            }
            else
            {
                Child.transform.Find("XP_Text").GetComponent<Text>().text = "+" + Sub_Top_XP[i] + "XP";
                Child.transform.localPosition = Child.transform.localPosition + new Vector3(0, 50 * CntToChangePos, 0);

                if (i < TopicToCheck)
                {
                    Child.transform.Find("Item Checkmark").GetComponent<Image>().enabled = true;
                    //Child.transform.Find("XP_Text").GetComponent<Text>().color = Color.green / 2;
                    //Child.transform.Find("Item Checkmark").GetComponent<Image>().color = Color.green / 2;
                    Child.transform.Find("Item Background").GetComponent<Image>().enabled = false;
                }
                else if (i == TopicToCheck)
                {
                    Child.transform.Find("Item Background").GetComponent<Image>().enabled = true;// color = ReturnColorFromHex("#a01c65");
                                                                                                 //Child.transform.Find("Item Label").GetComponent<Text>().color = Color.white;
                }
            }
            TestStr = ToDoList_DD.options[i].text;
        }



        //iTween.ScaleTo(ToDoList_DD.transform.Find("Topic_Objective").gameObject, iTween.Hash("y", 0, "time", 0.5f, "easetype", iTween.EaseType.spring));
        //if (SidePannelOpen.transform.eulerAngles.z == 0)
        //{
        //    OpenSidePannel();
        //}

    }

    public void OpenSidePannel()
    {
        if (SidePannelOpen.transform.eulerAngles.z == 180)
        {
            //iTween.MoveAdd(SidePannel, iTween.Hash("x", -400f, "time",0.5f,"islocal",true, "easetype", iTween.EaseType.linear));
            //iTween.MoveAdd(SidePannelOpen.gameObject, iTween.Hash("x", -185f, "islocal", true, "time", 0.5f, "easetype", iTween.EaseType.linear));
            //SidePannelOpen.transform.eulerAngles = new Vector3(0,0,180);
            SidePannel.GetComponent<Animator>().Play("PannelForward");
        }
        else
        {
            //iTween.MoveAdd(SidePannel, iTween.Hash("x", 400f, "time", 0.5f, "islocal", true, "easetype", iTween.EaseType.linear));
            //iTween.MoveAdd(SidePannelOpen.gameObject, iTween.Hash("x", -185f, "islocal", true, "time", 0.5f, "easetype", iTween.EaseType.linear));
            //SidePannelOpen.transform.eulerAngles = new Vector3(0, 0, 0);
            SidePannel.GetComponent<Animator>().Play("PannelBackward");
        }
    }

    public void UIReset()
    {
        ObjectivePanel.GetComponent<RectTransform>().anchoredPosition = new Vector3(0f, 5f, 0);

        //ZekkiMassSlider.transform.GetComponent<RectTransform>().anchoredPosition = new Vector3(137.5f, -53f, 0);
        Info_Open_Btn.transform.GetComponent<RectTransform>().anchoredPosition = new Vector3(-37f, -150f, 0);

        
        InventoryScroll.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(-80f, -52f, 0);

        //SidePannel.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(0f, 0f, 0);

        //print("side......" + SidePannel.transform.position+"....local....."+ SidePannel.transform.localPosition);
        if (!IsTopicUnlocked)
        {
            ToDoList_DD.transform.GetComponent<RectTransform>().anchoredPosition = new Vector3(135f, -150f, 0);
            XP_ToShow.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(135f, -150f, 0);
            Reset_Btn.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(-300f, -150f, 0);

        }
        else
        {
            ToDoList_DD.transform.GetComponent<RectTransform>().anchoredPosition = new Vector3(-250f, -150f, 0);
            XP_ToShow.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(-250f, -150f, 0);
            Reset_Btn.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(10f, -150f, 0);

        }

        //Main_XP_ToShow.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(-135f, -60f, 0);

    }

    public void UIZoomOut()
    {
        if (ObjectivePanel.GetComponent<RectTransform>().anchoredPosition.y == 5)
            return;

        UIReset();

        iTween.MoveFrom(Info_Open_Btn.gameObject, iTween.Hash("x", Info_Open_Btn.transform.localPosition.x + 250, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveFrom(InventoryScroll.transform.parent.gameObject, iTween.Hash("x", InventoryScroll.transform.parent.localPosition.x + 1000f, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        //iTween.MoveFrom(ZekkiMassSlider.gameObject, iTween.Hash("x", ZekkiMassSlider.transform.localPosition.x + 250, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveFrom(ObjectivePanel, iTween.Hash("y", ObjectivePanel.transform.localPosition.y - 1000, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveFrom(Reset_Btn.transform.parent.gameObject, iTween.Hash("x", Reset_Btn.transform.parent.localPosition.x - 1000f, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));


        iTween.MoveFrom(ToDoList_DD.gameObject, iTween.Hash("x", ToDoList_DD.transform.localPosition.x - 400, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveFrom(XP_ToShow.transform.parent.gameObject, iTween.Hash("x", XP_ToShow.transform.parent.localPosition.x - 400, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));

        //iTween.MoveFrom(Main_XP_ToShow.transform.parent.gameObject, iTween.Hash("x", Main_XP_ToShow.transform.parent.localPosition.x + 550, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
    }



    public void UIZoomIn()
    {
        if (ObjectivePanel.GetComponent<RectTransform>().anchoredPosition.y != 5)
            return;

        UIReset();
        //print("side......" + SidePannel.transform.position);
        iTween.MoveAdd(ObjectivePanel, iTween.Hash("y", ObjectivePanel.transform.localPosition.y - 1000, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveAdd(Info_Open_Btn.gameObject, iTween.Hash("x", Info_Open_Btn.transform.localPosition.x + 250, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveAdd(InventoryScroll.transform.parent.gameObject, iTween.Hash("x", InventoryScroll.transform.parent.localPosition.x + 1000f, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        
        
        iTween.MoveAdd(Reset_Btn.transform.parent.gameObject, iTween.Hash("x", Reset_Btn.transform.parent.localPosition.x - 1000f, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));

        //iTween.MoveAdd(ZekkiMassSlider.gameObject, iTween.Hash("x", ZekkiMassSlider.transform.localPosition.x + 250, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));

        iTween.MoveAdd(XP_ToShow.transform.parent.gameObject, iTween.Hash("x", XP_ToShow.transform.parent.localPosition.x - 400, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveAdd(ToDoList_DD.gameObject, iTween.Hash("x", ToDoList_DD.transform.localPosition.x - 400, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));

        //iTween.MoveAdd(Main_XP_ToShow.transform.parent.gameObject, iTween.Hash("x", Main_XP_ToShow.transform.parent.localPosition.x + 550, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
    }

    public float ChangeToLocal_X(float Val)
    {
        float X_ToRet = 0;
        //print("wid........"+ UIcanvas.GetComponent<RectTransform>().rect.width);
        X_ToRet = ((UIcanvas.GetComponent<RectTransform>().rect.x) + Val);

        return X_ToRet;
    }

    public float ChangeToLocal_Y(float Val)
    {
        float Y_ToRet = 0;
        //print("wid........"+ UIcanvas.GetComponent<RectTransform>().rect.width);
        Y_ToRet = ((UIcanvas.GetComponent<RectTransform>().rect.y) + Val);

        return Y_ToRet;
    }


    public void Collect_XP(GameObject TargetPos)
    {
        if (IsTopicUnlocked)
            return;

        XP_Effect.transform.position = Camera.main.WorldToScreenPoint(TargetPos.transform.position);

        Camera.main.GetComponent<PhysicsRaycaster>().enabled = false;
        UIcanvas.GetComponent<GraphicRaycaster>().enabled = false;

        XP_Effect.text = "+"+Sub_Top_XP[TopicToCheck] + "XP";

        XP_Effect.transform.localScale = new Vector3(2, 0, 2);

        iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 2, "y", 2, "z", 2, "time", 0.5f, "easetype", iTween.EaseType.easeOutElastic));

        iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", XP_Effect.transform.position.x, "y", XP_Effect.transform.position.y + 150, "z", 0, "delay", 0.3f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

        iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", 150, "y", XP_ToShow.transform.position.y, "z", 0, "delay", 1f, "time", 1f, "easetype", iTween.EaseType.easeInSine, "oncomplete", "Add_To_XP", "oncompletetarget", this.gameObject));

    }

    public void Add_To_XP()
    {

        //print("TopicToCheck......."+ TopicToCheck+"....sub......"+Sub_Topics.Count);
        if (TopicToCheck < (Sub_Top_XP.Count))
        {
            Sub_XP_Earned += float.Parse(Sub_Top_XP[TopicToCheck]);

            XP_Effect.text = "";
            XP_ToShow.text = Sub_XP_Earned + "/" + Total_XP;

            TopicToCheck++;
            if (TopicToCheck <= (Sub_Top_XP.Count - 1))
            {
                ObjectivePanel.transform.Find("ObjectiveText").GetComponent<Text>().text = Sub_Top_ToDoList[TopicToCheck];
                ObjectivePanel.transform.localScale = new Vector3(1, 0, 1);
                iTween.ScaleTo(ObjectivePanel, iTween.Hash("y", 1, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine));
                //ToDoList_DD.transform.Find("Topic_Objective").GetComponent<Text>().text = "Objective: " + Sub_Top_ToDoList[TopicToCheck];

                Camera.main.GetComponent<PhysicsRaycaster>().enabled = true;
                UIcanvas.GetComponent<GraphicRaycaster>().enabled = true;
            }
            else
            {
                XP_Effect.transform.position = (XP_ToShow.transform.position);

                XP_Effect.text = Total_XP + "XP";

                XP_Effect.transform.localScale = new Vector3(1, 0, 1);

                iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 2, "y", 2, "z", 2, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeOutElastic));

                iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", XP_Effect.transform.position.x, "y", XP_Effect.transform.position.y + 50, "z", 0, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

                iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", Main_XP_ToShow.transform.position.x, "y", Main_XP_ToShow.transform.position.y, "z", 0, "delay", 1.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine, "oncomplete", "AddTotalToMainXP", "oncompletetarget", this.gameObject));

                iTween.MoveTo(ToDoList_DD.gameObject, iTween.Hash("x", ChangeToLocal_X(-400), "delay", 3.5f, "time", 0.5f, "islocal", true, "easetype", iTween.EaseType.easeInOutSine));

                iTween.MoveTo(XP_ToShow.transform.parent.gameObject, iTween.Hash("x", ChangeToLocal_X(-400), "delay", 3.5f, "time", 0.5f, "islocal", true, "easetype", iTween.EaseType.easeInOutSine));

                iTween.MoveTo(Reset_Btn.transform.parent.gameObject, iTween.Hash("x", ChangeToLocal_X(10), "delay", 3.5f, "time", 0.5f, "islocal", true, "easetype", iTween.EaseType.easeInOutSine));



                //print("label......" + LabelImage.GetComponent<RectTransform>().anchoredPosition+".......explode...."+ ExplodeSlider.GetComponent<RectTransform>().anchoredPosition);


                //iTween.MoveTo(ValueDisplaySlider.gameObject, iTween.Hash("x", ChangeToLocal_X ( 150), "delay", 0.7f, "time", 0.5f, "islocal", true, "easetype", iTween.EaseType.easeInOutSine));
            }
        }

    }

    public void AddTotalToMainXP()
    {
        if (OptionUnlockedTill < 1)
        {
            OptionUnlockedTill++;

            float MainXp = float.Parse(Main_XP_ToShow.text);
            MainXp += Total_XP;
            Main_XP_ToShow.text = MainXp.ToString();
            //ObjectivePanel.transform.Find("ObjectiveText").GetComponent<Text>().text = "YaY!......Next topic unlocked";
            XP_Effect.text = "";
            IsTopicUnlocked = true;
            iTween.ScaleTo(ObjectivePanel, iTween.Hash("y", 0, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine));
            Camera.main.GetComponent<PhysicsRaycaster>().enabled = true;
            UIcanvas.GetComponent<GraphicRaycaster>().enabled = true;

        }
    }

    

    public void PlaySequenceStep()
    {
        DragDropSteps--;
        if (CurrentStepCnt == 0)
        {
            UpdateToNextStep();
            Collect_XP(Beaker);
        }
        else if (CurrentStepCnt == 1)
        {
            Vinegar.transform.GetChild(1).gameObject.SetActive(false);
            Vinegar.transform.GetChild(0).gameObject.SetActive(true);
            iTween.ScaleTo(Vinegar.transform.GetChild(0).gameObject,iTween.Hash("y",1,"time",1f,"oncomplete", "UpdateToNextStep", "oncompletetarget",this.gameObject,"islocal",true,"easetype",iTween.EaseType.linear));
            Collect_XP(Vinegar);
        }
        else if (CurrentStepCnt == 2)
        {
            iTween.MoveTo(CopperPlate, iTween.Hash("y", 0.23f, "time", 0.5f, "oncomplete", "UpdateToNextStep", "oncompletetarget", this.gameObject, "islocal", true, "easetype", iTween.EaseType.linear));
            Collect_XP(CopperPlate);
        }
        else if (CurrentStepCnt == 3)
        {
            iTween.MoveTo(ZincPlate, iTween.Hash("y", 0.23f, "time", 0.5f, "oncomplete", "UpdateToNextStep", "oncompletetarget", this.gameObject, "islocal", true, "easetype", iTween.EaseType.linear));
            Collect_XP(ZincPlate);
        }
        else if (CurrentStepCnt == 4)
        {
            iTween.MoveTo(Lamp, iTween.Hash("y", 0.494f, "time", 0.5f, "oncomplete", "UpdateToNextStep", "oncompletetarget", this.gameObject, "islocal", true, "easetype", iTween.EaseType.linear));
            Collect_XP(Lamp);
        }
        
        
    }

    public void UpdateToNextStep()
    {
        if (CurrentStepCnt == 4)
            Lamp.transform.GetChild(1).gameObject.SetActive(true);// GetComponentInChildren<Light>().enabled = true;

        CurrentStepCnt++;

        if(CurrentStepCnt>=Sub_Top_XP.Count)
            ShowLabels_Btn.GetComponent<Image>().color = new Color(1, 1,1,1f);

        PlayerPrefs.SetInt("CurrentStepCount", PlayerPrefs.GetInt("CurrentStepCount") + 1);

        if (IsTopicUnlocked)
        {
            if(CurrentStepCnt<Sub_Top_ToDoList.Count)
            ObjectivePanel.transform.Find("ObjectiveText").GetComponent<Text>().text = Sub_Top_ToDoList[CurrentStepCnt];
            ObjectivePanel.transform.localScale = new Vector3(1, 0, 1);
            iTween.ScaleTo(ObjectivePanel, iTween.Hash("y", 1, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine));

        }

        if (CurrentStepCnt <= Sub_Top_ToDoList.Count)
            NumOfSteps.text = DragDropSteps.ToString();

        
    }

    public void WrongDragDrop()
    {
        ShowInstruction_Panel("Wrong Option Selected");
    }

    public void WrongPlacement()
    {
        ShowInstruction_Panel("Wrong Placement");
    }

    public void ShowInstruction_Panel(string TextToDisplay)
    {
        var mouse = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);



        if (mouse.x < Screen.width / 2)
        {
            // WronOptionPanel
            //print("Mouse is on left side of screen.");
            InstructionPanel.GetComponent<RectTransform>().pivot = new Vector2(0, 0);
        }



        if (mouse.x > Screen.width / 2)
        {
            //print("Mouse is on right side of screen.");
            InstructionPanel.GetComponent<RectTransform>().pivot = new Vector2(1, 0);
        }

        InstructionPanel.transform.position = Input.mousePosition;
        UIcanvas.GetComponent<GraphicRaycaster>().enabled = false;
        Camera.main.GetComponent<PhysicsRaycaster>().enabled = false;
        iTween.Stop(InstructionPanel.gameObject);

        InstructionPanel.transform.Find("InstructionText").GetComponent<Text>().text = TextToDisplay;
        //iTween.ScaleTo(GlassPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 0.01f, "easetype", iTween.EaseType.spring));
        iTween.ScaleTo(InstructionPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 0.3f, "easetype", iTween.EaseType.spring, "oncomplete", "CloseInstructionPanel", "oncompletetarget", this.gameObject));
        ObjectivePanel.transform.localScale = new Vector3(1, 0, 1);
    }

    public void ShowLabels()
    {
        if (ShowLabels_Btn.GetComponent<Image>().color.a <= 0.5f)
            return;

        if(!Labels.activeInHierarchy)
        Labels.SetActive(true);
        else
            Labels.SetActive(false);
    }

    public void ResetExperiment()
    {
        Beaker.SetActive(false);
        Vinegar.SetActive(false);
        CopperPlate.SetActive(false);
        ZincPlate.SetActive(false);
        Lamp.SetActive(false);
        Vinegar.transform.GetChild(1).gameObject.SetActive(true);
        Vinegar.transform.GetChild(0).gameObject.SetActive(false);
        Vinegar.transform.GetChild(0).localScale = new Vector3(1,0,1);
        Labels.SetActive(false);
        CurrentStepCnt = 0;
        ShowLabels_Btn.GetComponent<Image>().color = new Color(1,1,1,0.5f);
        Lamp.transform.GetChild(1).gameObject.SetActive(false);

        if (!IsAR)
        {
            CamOrbit.zoomMax = 3f;
            CamOrbit.zoomMin = 1.3f;
            CamOrbit.zoomStart = 2f;
            CamOrbit.distance = CamOrbit.zoomStart;
            CamOrbit.transform.position = new Vector3(0, 0.25f, 0);

        }

        UIDragAndDrop[] UIButtons = GameObject.FindObjectsOfType<UIDragAndDrop>();
        for (int i = 0; i < UIButtons.Length; i++)
        {
            UIButtons[i].Restart();
        }

        ObjectivePanel.transform.Find("ObjectiveText").GetComponent<Text>().text = Sub_Top_ToDoList[CurrentStepCnt];
        ObjectivePanel.transform.localScale = new Vector3(1, 0, 1);
        iTween.ScaleTo(ObjectivePanel, iTween.Hash("y", 1, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine));

        DragDropSteps = UIcanvas.GetComponentsInChildren<UIDragAndDrop>().Length;

        NumOfSteps.text = DragDropSteps.ToString();

    }

    // Update is called once per frame
    void Update()
    {
        
    }


}
