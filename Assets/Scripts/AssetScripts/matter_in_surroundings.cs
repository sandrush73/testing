﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;
using System.Linq;
using System;
using System.Text.RegularExpressions;
using UnityEngine.SceneManagement;

public class matter_in_surroundings : MonoBehaviour
{
    public static matter_in_surroundings Instance;
  
    public GameObject Ice_Cubes, Induction, Induction_1, Box_Pkg_Final, Canvas_BG, Labels, Ground, Effects;
    //public GameObject Beaker, Box, Opener;
    public GameObject Container_Box, Liquified_Water;
    public GameObject TalkBubble_1, TalkBubble_2, TalkBubble_3, TalkBubble_4;
    public GameObject Slot;
    public TextMeshPro Show_Temp, Show_Temp2;
    public Text Celcius_Text, Temp_Text, Temp_P;
    public Button Induction_On, TempDec_Btn, TempInc_Btn, Reset_Button;
    public int CurrentStepCnt,DragDropSteps;
    public GameObject Smoke_Start;
    public GameObject Gas_Vapour;
    private bool IDFunc;
    private bool Wat_Vap;
    //private bool pressed = false;
    public float Temp;
   // public float drop_value;
   // public float BlendValue;
    public float AlphaValue = 0;
    public float Speed = 1;
    public float time = 0;

    public TextAsset CSVFile;
    public Material TransparentMaterial;
    public bool IsAR, IsClicked;
    public orbit CamOrbit;
    public AROrbitControls ArOrbit;

    public Button SidePannelOpen;
    public int OptionUnlockedTill, CurrentTopicSelected, TopicToCheck;
    public float Sub_XP_Earned, Total_XP;
    public Dropdown ToDoList_DD;
    public GameObject InstructionPanel_DD, ObjectivePanel, InstructionPanel, InfoPanel, GlassPanel, SidePannel,InventoryScroll;
    public Button Info_Close_Btn, Info_Cntue_Btn, Info_Open_Btn,ShowLabels_Btn,Reset_Btn;
    public List<Dictionary<string, object>> CSV_data;
    public List<String> Topics, Sub_Topics, Sub_Top_ToDoList, Sub_Top_Desc, Sub_Top_XP, LearningGoals;
    public Text XP_ToShow, XP_Effect, Main_XP_ToShow, Learninggoal_Txt, Instruction_Txt, Assumption_Txt, Heading_Txt,NumOfSteps;
    public bool IsTopicUnlocked;
    public Canvas UIcanvas;
    public AssetBundle assetBundle;
   

    public void Awake()
    {
        Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        CurrentStepCnt = 0;

        LoadFromAsssetBundle();

        LoadingData();

        

        //OpenSidePannel();
    }

    public void LoadingData()
    {
        ReadingDataFromCSVFile();

        //OptionUnlockedTill = 1;

        ReadingXps();

        FindingGameObjects();

        AssignClickEventsToObjects();
    }

    
    public void LoadFromAsssetBundle()
    {

        //..................AssetBundle................

        //assetBundle = AssetBundle.LoadFromFile(PlayerPrefs.GetString("AssetBundleUrl"));

        //CSVFile = assetBundle.LoadAsset(PlayerPrefs.GetString("LibraryName") + "CSV", typeof(TextAsset)) as TextAsset;

        //TransparentMaterial = assetBundle.LoadAsset("TransparentMaterial", typeof(Material)) as Material;
        //TransparentMaterial.shader = Shader.Find("Standard");


        //.......................APK File...........................

        CSVFile = Resources.Load("matter_in_surroundingsCSV") as TextAsset;




        //...............Don't Change............



        Camera.main.gameObject.AddComponent<PhysicsRaycaster>();



        Camera.main.backgroundColor = Color.black;



        GameObject TargetForCamera = GameObject.Find("Target");



        if (TargetForCamera != null)
        {
            
            IsAR = false;
            TargetForCamera.AddComponent<orbit>();
            CamOrbit = TargetForCamera.GetComponent<orbit>();

            CamOrbit.zoomMax = 2.5f;
            CamOrbit.zoomMin = 0.5f;
            CamOrbit.zoomStart = 1f;
            CamOrbit.distance = CamOrbit.zoomStart;
            CamOrbit.transform.position = new Vector3(0, 0.25f, 0);

            //CamOrbit.cameraRotSideStart = 32;
            CamOrbit.cameraRotUpStart = 30;
           

            CamOrbit.maxRotUp = 30;
            CamOrbit.minRotUp = 1;

            CamOrbit.maxSideRot = 60;
            CamOrbit.minSideRot = 60;
        }
        else
        {
            this.transform.parent.gameObject.AddComponent<AROrbitControls>();
            ArOrbit = this.transform.parent.gameObject.GetComponent<AROrbitControls>();
            IsAR = true;
        }



    }

    public void ReadingDataFromCSVFile()
    {
        Topics = new List<string>();
        Sub_Topics = new List<string>();
        Sub_Top_Desc = new List<string>();
        Sub_Top_ToDoList = new List<string>();
        Sub_Top_XP = new List<string>();
        LearningGoals = new List<string>();

        CSV_data = CSVReader.Read(CSVFile);


        for (var i = 0; i < CSV_data.Count; i++)
        {
            //print("Topics " + CSV_data[i]["Learninggoals"] + ".......... " +"ToDo " + CSV_data[i]["Topic_TodoList"]);


            if (CSV_data[i]["Topic_ToDoList"].ToString() != "" && CSV_data[i]["Topic_ToDoList"].ToString() != null)
            {
                Sub_Top_ToDoList.Add(CSV_data[i]["Topic_ToDoList"].ToString());

            }


            //if (CSV_data[i]["Top_XP"].ToString() != "" && CSV_data[i]["Top_XP"].ToString() != null)
            //{
            //    Sub_Top_XP.Add(CSV_data[i]["Top_XP"].ToString());

            //}

            if (CSV_data[i]["Learninggoals"].ToString() != "" && CSV_data[i]["Learninggoals"].ToString() != null)
            {
                LearningGoals.Add(CSV_data[i]["Learninggoals"].ToString());

            }

        }
        
    }

    public int totXp, loclXp, PerTopic_XP;

    public void ReadingXps()
    {
        
        totXp = PlayerPrefs.GetInt("TotalXP");
        loclXp = PlayerPrefs.GetInt("MaxEarnings");

        TopicToCheck = 0;

        if (loclXp != 0)
        {
            OptionUnlockedTill = 0;

        }
        else
        {
            OptionUnlockedTill = 1;

        }
        
        int t = 0;
        int p = 0;

        for (int k = 0; k < Sub_Top_ToDoList.Count; k++)
        {

            p = loclXp / Sub_Top_ToDoList.Count;
            Sub_Top_XP.Add(p.ToString());
            t = t + p;
        }

        if (t != loclXp)
        {
            Sub_Top_XP[Sub_Top_ToDoList.Count - 1] = (p + (loclXp - t)).ToString();
        }

        Total_XP = loclXp;
        
    }
    
    public float result;

    public void GetResult()
    {
        result = ((float)(TopicToCheck - 0) / (float)(Sub_Top_ToDoList.Count)) * 100;
        Camera.main.SendMessage("UnloadActually", result);
    }


    public void FindingGameObjects()
    {
        Ice_Cubes = GameObject.Find("Ice_Cubes_Final");
        Induction = GameObject.Find("Induction_Final");
        Induction_1 = GameObject.Find("Induction_1");
        Box_Pkg_Final = GameObject.Find("Box_Pkg_Final");
        Container_Box = GameObject.Find("Opener");
        Liquified_Water = GameObject.Find("Water");
        TalkBubble_1 = GameObject.Find("TalkBubble_1");
        TalkBubble_1.SetActive(false);
        TalkBubble_2 = GameObject.Find("TalkBubble_2");
        TalkBubble_2.SetActive(false);
        TalkBubble_3 = GameObject.Find("TalkBubble_3");
        TalkBubble_3.SetActive(false);
        TalkBubble_4 = GameObject.Find("TalkBubble_4");
        TalkBubble_4.SetActive(false);
        Show_Temp = GameObject.Find("Show_Temp").GetComponent<TextMeshPro>();
        Show_Temp2 = GameObject.Find("Show_Temp2").GetComponent<TextMeshPro>();
        Celcius_Text = GameObject.Find("Celcius_Text").GetComponent<Text>(); 
        Temp_Text = GameObject.Find("Temp_Text").GetComponent<Text>();
        Slot = GameObject.Find("Slot");

        Smoke_Start = GameObject.Find("smoke_effect");
        Smoke_Start.SetActive(false);

        Gas_Vapour = GameObject.Find("Gas_Vapour");
        Gas_Vapour.SetActive(false);

        Canvas_BG = GameObject.Find("Canvas_BG");
        Labels = GameObject.Find("Labels");
        Labels.SetActive(false);

        for (int i = 0; i < Labels.transform.childCount; i++)
        {
            Labels.transform.GetChild(i).Find("SmoothLook").gameObject.AddComponent<SmoothLook>();
        }

        Ground = GameObject.Find("Ground");

        if (!IsAR)
        {
            Canvas_BG.transform.parent = Camera.main.transform;
            Canvas_BG.GetComponent<Canvas>().worldCamera = Camera.main;
        }

        else
        {
            Canvas_BG.SetActive(false);
            Ground.SetActive(false);
        }


        //Invoke("DeactivateObjects",0.01f);
        UIcanvas = GameObject.Find("Canvas_matter").GetComponent<Canvas>();
        SidePannel = GameObject.Find("SidePanel");
        SidePannelOpen = GameObject.Find("PannelOpenButton").GetComponent<Button>();
        InstructionPanel_DD = GameObject.Find("DD_Instruction");
        InstructionPanel = GameObject.Find("InstructionPanel");
        InventoryScroll = GameObject.Find("InventoryScrollView");
        ToDoList_DD = GameObject.Find("ToDoList_Dropdown").GetComponent<Dropdown>();
        XP_ToShow = GameObject.Find("Gained_XP_Value").GetComponent<Text>();
        XP_Effect = GameObject.Find("Effect_Text").GetComponent<Text>();
        Main_XP_ToShow = GameObject.Find("Main_XP_Value").GetComponent<Text>();
        ObjectivePanel = GameObject.Find("ObjectivePanel");
        InstructionPanel.transform.localScale = Vector3.zero;
        InfoPanel = GameObject.Find("InformationPanel");
        Info_Close_Btn = GameObject.Find("InfoCloseBtn").GetComponent<Button>();
        Info_Cntue_Btn = GameObject.Find("InfoContinueBtn").GetComponent<Button>();
        Info_Open_Btn = GameObject.Find("InfoButton").GetComponent<Button>();
        Info_Close_Btn.gameObject.SetActive(false);
        Induction_On = GameObject.Find("Induction_On").GetComponent<Button>();
        Induction_On.gameObject.SetActive(false);
        TempDec_Btn = GameObject.Find("TempDec_Btn").GetComponent<Button>();
        TempDec_Btn.gameObject.SetActive(true);
        TempInc_Btn = GameObject.Find("TempInc_Btn").GetComponent<Button>();
        TempInc_Btn.gameObject.SetActive(true);
        Reset_Button = GameObject.Find("ResetButton").GetComponent<Button>();
        Reset_Button.gameObject.SetActive(false);
        GlassPanel = GameObject.Find("GlassEffect");
        Instruction_Txt = GameObject.Find("IListText").GetComponent<Text>();
        Assumption_Txt = GameObject.Find("AListText").GetComponent<Text>();
        Learninggoal_Txt = GameObject.Find("LGListText").GetComponent<Text>();
        Heading_Txt = GameObject.Find("InfoHeadingText").GetComponent<Text>();
        NumOfSteps = GameObject.Find("Slots_Text").GetComponent<Text>();
        ShowLabels_Btn = GameObject.Find("Labels_Show").GetComponent<Button>();
        Reset_Btn = GameObject.Find("Reset_Exp").GetComponent<Button>();


        if (OptionUnlockedTill == 0)
        {
            /*ToDoList_DD.GetComponent<RectTransform>().anchoredPosition = new Vector3(150f, -150f, 0);
            XP_ToShow.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(150f, -150f, 0);
            SidePannel.GetComponent<RectTransform>().anchoredPosition = new Vector3(-600f, 0f, 0);*/
            ObjectivePanel.transform.localScale = new Vector3(1, 0, 1);
            IsTopicUnlocked = false;
            Reset_Btn.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(-500f, -150f, 0);
        }
        else
        {
            /*ToDoList_DD.GetComponent<RectTransform>().anchoredPosition = new Vector3(-250f, -150f, 0);
            XP_ToShow.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(-250f, -150f, 0);
            SidePannel.GetComponent<RectTransform>().anchoredPosition = new Vector3(-600f, 0f, 0);*/
            ObjectivePanel.transform.localScale = new Vector3(1, 0, 1);
            XP_ToShow.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(-250f, -150f, 0);
            IsTopicUnlocked = true;
            ToDoList_DD.GetComponent<RectTransform>().anchoredPosition = new Vector3(-250f, -150f, 0);
            Reset_Btn.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(10f, -150f, 0);
        }

        Main_XP_ToShow.text = totXp.ToString();

        XP_ToShow.text = "0/" + Total_XP;



        for (int i = 0; i < Sub_Top_ToDoList.Count; i++)
        {

            ToDoList_DD.options.Add(new Dropdown.OptionData() { text = Sub_Top_ToDoList[i] });


            //List<float> TotalXP = Sub_Top_XP.Select(s => float.Parse(s)).ToList();

            //Total_XP += TotalXP[i];

            //XP_ToShow.text = "0/" + Total_XP;

            if(ObjectivePanel.transform.Find("ObjectiveText").GetComponent<Text>())
                ObjectivePanel.transform.Find("ObjectiveText").GetComponent<Text>().text = Sub_Top_ToDoList[TopicToCheck];
            else if (ObjectivePanel.transform.Find("ObjectiveText").GetComponent<TextMeshPro>())
                ObjectivePanel.transform.Find("ObjectiveText").GetComponent<TextMeshPro>().text = Sub_Top_ToDoList[TopicToCheck];

        }

        DragDropSteps = UIcanvas.GetComponentsInChildren<UIDragAndDrop>().Length;

        NumOfSteps.text = DragDropSteps.ToString();

        GlassPanel.transform.localScale = Vector3.zero;
        InfoPanel.transform.localScale = Vector3.zero;
        Invoke("OpenInfoPanel", 0.2f);
        Temp = -10f;
        //Heading_Txt.text = LearningGoals[0];
        //Learninggoal_Txt.text = LearningGoals[1];
        //Instruction_Txt.text = LearningGoals[2];
        Assumption_Txt.text = LearningGoals[3];

        
    }

    public void AssignClickEventsToObjects()
    {

        SidePannelOpen.onClick.AddListener(OpenSidePannel);

        AddListenerToEvents(EventTriggerType.PointerDown, EnableOrbit, InventoryScroll);

        AddListenerToEvents(EventTriggerType.PointerUp, DisableOrbit, InventoryScroll);

        AddListenerToEvents(EventTriggerType.PointerDown, EnableOrbit, InventoryScroll.transform.GetChild(2).gameObject);

        AddListenerToEvents(EventTriggerType.PointerUp, DisableOrbit, InventoryScroll.transform.GetChild(2).gameObject);

        ToDoList_DD.GetComponent<EventTrigger>().triggers.Clear();

        AddListenerToEvents(EventTriggerType.PointerClick, ExploringToDOList_DD, ToDoList_DD.gameObject);
        AddListenerToEvents(EventTriggerType.Select, ToDoList_DDCancel, ToDoList_DD.gameObject);

        Info_Close_Btn.onClick.AddListener(() => CloseInfoPanel(Info_Close_Btn));
        Info_Cntue_Btn.onClick.AddListener(() => CloseInfoPanel(Info_Cntue_Btn));
        Info_Open_Btn.onClick.AddListener(OpenInfoPanel);

        ShowLabels_Btn.onClick.AddListener(ShowLabels);
        //Reset_Btn.onClick.AddListener(ResetExperiment);
      
        Reset_Button.onClick.AddListener(ResetExperiment);
        //Reset_Button.onClick.AddListener(ButtonPressed);
        Induction_On.onClick.AddListener(Induction_SwitchON);
        TempInc_Btn.GetComponent<Button>().onClick.AddListener(() => IncDecFunction(TempInc_Btn.name));
        //TempDec_Btn.GetComponent<Button>().onClick.AddListener(() => IncDecFunction(TempDec_Btn.name));



        /*AddListenerToEvents(EventTriggerType.BeginDrag, OnElementBeginDrag, GameObject.Find("RawImage"));
        AddListenerToEvents(EventTriggerType.EndDrag, OnElementEndDrag, GameObject.Find("RawImage"));
        AddListenerToEvents(EventTriggerType.PointerClick, BackToPT, GameObject.Find("RawImage"));*/


        //InvokeRepeating("WaterToIce", 0.1f, 0.001f);
    }


    public void AddListenerToEvents(EventTriggerType eventType, Action MethodToCall, GameObject TriggerObjToAdd)
    {

        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall());
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }

    public void AddListenerToEvents(EventTriggerType eventType, Action<float> MethodToCall, GameObject TriggerObjToAdd, float p)
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall(p));
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }

    public void AddListenerToEvents(EventTriggerType eventType, Action<GameObject> MethodToCall, GameObject TriggerObjToAdd, GameObject p)
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall(p));
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }
    public void OnElementBeginDrag()
    {
        IsClicked = true;
    }

    public void OnElementEndDrag()
    {
        //print("drag ended");
        IsClicked = false;
    }

    public void EnableOrbit()
    {

        if (!IsAR)
        {
            CamOrbit.DisableInteration = true;
            //print("EnableOrbit");
        }
    }

    public void DisableOrbit()
    {

        if (!IsAR)
        {
            CamOrbit.DisableInteration = false;
            //print("DisableOrbit");
        }
        else
        {
            ArOrbit.DisableInteration = false;

        }
    }


    public void OpenInfoPanel()
    {
        UIZoomIn();
        iTween.ScaleTo(InfoPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 0.5f, "easetype", iTween.EaseType.spring));
        iTween.ScaleTo(GlassPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 0.01f, "easetype", iTween.EaseType.spring));

    }

    public void CloseInfoPanel(Button CloseBtn)
    {
        UIZoomOut();
        if (!IsTopicUnlocked)
        {
            ObjectivePanel.transform.localScale = new Vector3(1, 0, 1);
            iTween.ScaleTo(ObjectivePanel, iTween.Hash("y", 1, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine));
        }
        iTween.ScaleTo(InfoPanel, iTween.Hash("x", 0, "y", 0, "z", 0, "time", 0.2f, "easetype", iTween.EaseType.linear));
        iTween.ScaleTo(GlassPanel, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.2f, "time", 0.01f, "easetype", iTween.EaseType.linear));
        if (CloseBtn == Info_Cntue_Btn)
        {
            if (!IsAR)
                CamOrbit.DisableInteration = false;

            OpenSidePannel();
            Info_Close_Btn.gameObject.SetActive(true);
            Info_Cntue_Btn.gameObject.SetActive(false);
        }
        IsClicked = false;

        
    }

    public void ShowMessage_Panel(string TextToDisplay)
    {
        InstructionPanel.transform.Find("InstructionText").GetComponent<Text>().text = TextToDisplay;
        iTween.ScaleTo(GlassPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 0.01f, "easetype", iTween.EaseType.spring));
        iTween.ScaleTo(InstructionPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 1f, "easetype", iTween.EaseType.spring));
        ObjectivePanel.transform.localScale = new Vector3(1, 0, 1);
    }

    public void CloseInstructionPanel()
    {
        UIcanvas.GetComponent<GraphicRaycaster>().enabled = true;
        Camera.main.GetComponent<PhysicsRaycaster>().enabled = true;
        //iTween.ScaleTo(GlassPanel, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 1f, "time", 0.01f, "easetype", iTween.EaseType.linear));
        iTween.ScaleTo(InstructionPanel, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.5f, "time", 0.3f, "easetype", iTween.EaseType.spring));
        iTween.ScaleTo(ObjectivePanel, iTween.Hash("y", 1, "time", 0.3f, "delay", 0.5f, "easetype", iTween.EaseType.easeInOutSine));
    }

    public void ToDoList_DDCancel()
    {
        if (ToDoList_DD.transform.Find("Dropdown List") != null)//&& Obj.transform.localScale.y!=1)
        {
            //print("cancelled........");
            //print("list.....cancelled........" + ToDoList_DD.transform.Find("Dropdown List").gameObject);
            ToDoList_DD.transform.Find("Arrow").transform.eulerAngles = new Vector3(0, 0, 0);
            iTween.ScaleTo(ToDoList_DD.transform.Find("Dropdown List").gameObject, iTween.Hash("y", 0, "time", 0.5f, "easetype", iTween.EaseType.spring));
            iTween.ScaleTo(ObjectivePanel, iTween.Hash("y", 1, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine));
            if (SidePannelOpen.transform.eulerAngles.z == 180)
            {
                OpenSidePannel();
            }
        }
    }

    public void ExploringToDOList_DD()
    {
        //print("ShowDD.....");
        //ToDoList_DD.transform.Find("Topic_Objective").GetComponent<Text>().text = "";
        ToDoList_DD.transform.Find("Arrow").transform.eulerAngles = new Vector3(0, 0, 180);
        GameObject DDList = ToDoList_DD.transform.Find("Dropdown List").gameObject;
        DDList.transform.GetComponent<RectTransform>().sizeDelta = new Vector2(0, 250);
        DDList.transform.localScale = new Vector3(1, 0, 1);
        iTween.ScaleTo(DDList, iTween.Hash("y", 1, "time", 0.5f, "easetype", iTween.EaseType.spring));
        //iTween.ScaleTo(ToDoList_DD.transform.Find("Topic_Objective").gameObject, iTween.Hash("y", 0, "time", 0.5f, "easetype", iTween.EaseType.spring));
        if (!IsTopicUnlocked)
            iTween.ScaleTo(ObjectivePanel, iTween.Hash("y", 0, "time", 0.5f, "delay", 0.1f, "easetype", iTween.EaseType.easeInOutSine));

        string TestStr = "";
        int CntToChangePos = 0;
        for (int i = 0; i < ToDoList_DD.options.Count; i++)
        {
            GameObject Child = GameObject.Find("Item " + i + ": " + ToDoList_DD.options[i].text);
            //Child.transform.parent.GetComponent<RectTransform>().sizeDelta = new Vector2(0,250);

            if (TestStr == ToDoList_DD.options[i].text)
            {
                Child.SetActive(false);
                Child.transform.localPosition = Child.transform.localPosition + new Vector3(0, 50 * CntToChangePos, 0);
                CntToChangePos++;
            }
            else
            {
                Child.transform.Find("XP_Text").GetComponent<Text>().text = "+" + Sub_Top_XP[i] + "XP";
                Child.transform.localPosition = Child.transform.localPosition + new Vector3(0, 50 * CntToChangePos, 0);

                if (i < TopicToCheck)
                {
                    Child.transform.Find("Item Checkmark").GetComponent<Image>().enabled = true;
                    //Child.transform.Find("XP_Text").GetComponent<Text>().color = Color.green / 2;
                    //Child.transform.Find("Item Checkmark").GetComponent<Image>().color = Color.green / 2;
                    Child.transform.Find("Item Background").GetComponent<Image>().enabled = false;
                }
                else if (i == TopicToCheck)
                {
                    Child.transform.Find("Item Background").GetComponent<Image>().enabled = true;// color = ReturnColorFromHex("#a01c65");
                                                                                                 //Child.transform.Find("Item Label").GetComponent<Text>().color = Color.white;
                }
            }
            TestStr = ToDoList_DD.options[i].text;
        }
        
        //iTween.ScaleTo(ToDoList_DD.transform.Find("Topic_Objective").gameObject, iTween.Hash("y", 0, "time", 0.5f, "easetype", iTween.EaseType.spring));
        if (SidePannelOpen.transform.eulerAngles.z == 0)
        {
            OpenSidePannel();
        }

    }

    public void OpenSidePannel()
    {
        if (SidePannelOpen.transform.eulerAngles.z == 180)
        {
            //iTween.MoveAdd(SidePannel, iTween.Hash("x", -400f, "time",0.5f,"islocal",true, "easetype", iTween.EaseType.linear));
            //iTween.MoveAdd(SidePannelOpen.gameObject, iTween.Hash("x", -185f, "islocal", true, "time", 0.5f, "easetype", iTween.EaseType.linear));
            //SidePannelOpen.transform.eulerAngles = new Vector3(0,0,180);
            SidePannel.GetComponent<Animator>().Play("PannelForward");
        }
        else
        {
            //iTween.MoveAdd(SidePannel, iTween.Hash("x", 400f, "time", 0.5f, "islocal", true, "easetype", iTween.EaseType.linear));
            //iTween.MoveAdd(SidePannelOpen.gameObject, iTween.Hash("x", -185f, "islocal", true, "time", 0.5f, "easetype", iTween.EaseType.linear));
            //SidePannelOpen.transform.eulerAngles = new Vector3(0, 0, 0);
            SidePannel.GetComponent<Animator>().Play("PannelBackward");
        }
    }


    public float ChangeToLocal_X(float Val)
    {
        float X_ToRet = 0;
        //print("wid........"+ UIcanvas.GetComponent<RectTransform>().rect.width);
        X_ToRet = ((UIcanvas.GetComponent<RectTransform>().rect.x) + Val);

        return X_ToRet;
    }

    public float ChangeToLocal_Y(float Val)
    {
        float Y_ToRet = 0;
        //print("wid........"+ UIcanvas.GetComponent<RectTransform>().rect.width);
        Y_ToRet = ((UIcanvas.GetComponent<RectTransform>().rect.y) + Val);

        return Y_ToRet;
    }


    public void Collect_XP(GameObject TargetPos)
    {
        if (IsTopicUnlocked)
            return;

        XP_Effect.transform.position = Camera.main.WorldToScreenPoint(TargetPos.transform.position);

        Camera.main.GetComponent<PhysicsRaycaster>().enabled = false;
        UIcanvas.GetComponent<GraphicRaycaster>().enabled = false;

        XP_Effect.text = Sub_Top_XP[TopicToCheck] + "XP";

        XP_Effect.transform.localScale = new Vector3(2, 0, 2);

        iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 2, "y", 2, "z", 2, "time", 0.5f, "easetype", iTween.EaseType.easeOutElastic));

        iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", XP_Effect.transform.position.x, "y", XP_Effect.transform.position.y + 150, "z", 0, "delay", 0.3f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

        iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", 150, "y", XP_ToShow.transform.position.y, "z", 0, "delay", 1f, "time", 1f, "easetype", iTween.EaseType.easeInSine, "oncomplete", "Add_To_XP", "oncompletetarget", this.gameObject));

    }

    public void Add_To_XP()
    {

        //print("TopicToCheck......."+ TopicToCheck+"....sub......"+Sub_Topics.Count);
        if (TopicToCheck < (Sub_Top_XP.Count))
        {
            Sub_XP_Earned += float.Parse(Sub_Top_XP[TopicToCheck]);

            XP_Effect.text = "";
            XP_ToShow.text = Sub_XP_Earned + "/" + Total_XP;

            TopicToCheck++;
            if (TopicToCheck <= (Sub_Top_XP.Count - 1))
            {
                ObjectivePanel.transform.Find("ObjectiveText").GetComponent<Text>().text = Sub_Top_ToDoList[TopicToCheck];
                ObjectivePanel.transform.localScale = new Vector3(1, 0, 1);
                iTween.ScaleTo(ObjectivePanel, iTween.Hash("y", 1, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine));
                //ToDoList_DD.transform.Find("Topic_Objective").GetComponent<Text>().text = "Objective: " + Sub_Top_ToDoList[TopicToCheck];

                Camera.main.GetComponent<PhysicsRaycaster>().enabled = true;
                UIcanvas.GetComponent<GraphicRaycaster>().enabled = true;
            }
            else
            {
                XP_Effect.transform.position = (XP_ToShow.transform.position);

                XP_Effect.text = Total_XP + "XP";

                XP_Effect.transform.localScale = new Vector3(1, 0, 1);

                iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 2, "y", 2, "z", 2, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeOutElastic));

                iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", XP_Effect.transform.position.x, "y", XP_Effect.transform.position.y + 50, "z", 0, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

                iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", Main_XP_ToShow.transform.position.x, "y", Main_XP_ToShow.transform.position.y, "z", 0, "delay", 1.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine, "oncomplete", "AddTotalToMainXP", "oncompletetarget", this.gameObject));

                iTween.MoveTo(ToDoList_DD.gameObject, iTween.Hash("x", ChangeToLocal_X(-400), "delay", 3.5f, "time", 0.5f, "islocal", true, "easetype", iTween.EaseType.easeInOutSine));

                iTween.MoveTo(XP_ToShow.transform.parent.gameObject, iTween.Hash("x", ChangeToLocal_X(-400), "delay", 3.5f, "time", 0.5f, "islocal", true, "easetype", iTween.EaseType.easeInOutSine));

                iTween.MoveTo(Reset_Btn.transform.parent.gameObject, iTween.Hash("x", ChangeToLocal_X(10), "delay", 3.5f, "time", 0.5f, "islocal", true, "easetype", iTween.EaseType.easeInOutSine));



                //print("label......" + LabelImage.GetComponent<RectTransform>().anchoredPosition+".......explode...."+ ExplodeSlider.GetComponent<RectTransform>().anchoredPosition);


                //iTween.MoveTo(ValueDisplaySlider.gameObject, iTween.Hash("x", ChangeToLocal_X ( 150), "delay", 0.7f, "time", 0.5f, "islocal", true, "easetype", iTween.EaseType.easeInOutSine));
            }
        }

    }

    public void AddTotalToMainXP()
    {
        if (OptionUnlockedTill < 1)
        {
            OptionUnlockedTill++;

            float MainXp = float.Parse(Main_XP_ToShow.text);
            MainXp += Total_XP;
            Main_XP_ToShow.text = MainXp.ToString();
            //ObjectivePanel.transform.Find("ObjectiveText").GetComponent<Text>().text = "YaY!......Next topic unlocked";
            XP_Effect.text = "";
            IsTopicUnlocked = true;
            iTween.ScaleTo(ObjectivePanel, iTween.Hash("y", 0, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine));
            Camera.main.GetComponent<PhysicsRaycaster>().enabled = true;
            UIcanvas.GetComponent<GraphicRaycaster>().enabled = true;

        }
    }


    //public void ResetOnClick()
    //{


    //    //ResetExperiment();

    //}

    //public void ButtonPressed()
    //{
    //    pressed = true;
    //}

    public void UIReset()
    {
        ObjectivePanel.GetComponent<RectTransform>().anchoredPosition = new Vector3(0f, 5f, 0);

        //ValueDisplaySlider.transform.GetComponent<RectTransform>().anchoredPosition = new Vector3(-20f, 0f, 0);
        Info_Open_Btn.transform.GetComponent<RectTransform>().anchoredPosition = new Vector3(-37f, -150f, 0);
        
        SidePannelOpen.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(135f, 50f, 0);

        InventoryScroll.transform.GetComponent<RectTransform>().anchoredPosition = new Vector3(-80f, 27f, 0);
        Slot.transform.GetComponent<RectTransform>().anchoredPosition = new Vector3(-80f, 55f, 0);



        //print("side......" + SidePannelOpen.transform.position+"....local....."+ SidePannelOpen.transform.localPosition);
        if (!IsTopicUnlocked)
        {
            ToDoList_DD.transform.GetComponent<RectTransform>().anchoredPosition = new Vector3(135f, -150f, 0);
            XP_ToShow.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(135f, -150f, 0);
            Reset_Btn.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(-300f, -150f, 0);


        }
        else
        {
            ToDoList_DD.transform.GetComponent<RectTransform>().anchoredPosition = new Vector3(-250f, -150f, 0);
            XP_ToShow.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(-250f, -150f, 0);
            Reset_Btn.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(10, -150f, 0);


        }



        //Main_XP_ToShow.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(-135f, -60f, 0);



    }



    public void UIZoomOut()
    {
        if (ObjectivePanel.GetComponent<RectTransform>().anchoredPosition.y == 5)
            return;
        
        UIReset();

        iTween.MoveFrom(Info_Open_Btn.gameObject, iTween.Hash("x", Info_Open_Btn.transform.localPosition.x + 250, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveFrom(SidePannelOpen.transform.parent.gameObject, iTween.Hash("x", SidePannelOpen.transform.parent.localPosition.x - 1000f, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        //iTween.MoveFrom(ValueDisplaySlider.gameObject, iTween.Hash("x", ValueDisplaySlider.transform.localPosition.x + 250, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveFrom(ObjectivePanel, iTween.Hash("y", ObjectivePanel.transform.localPosition.y - 1000, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveFrom(InventoryScroll, iTween.Hash("x", InventoryScroll.transform.localPosition.x + 1000f, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveFrom(Slot, iTween.Hash("x", Slot.transform.localPosition.x + 1000f, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));

        iTween.MoveFrom(ToDoList_DD.gameObject, iTween.Hash("x", ToDoList_DD.transform.localPosition.x - 400, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveFrom(XP_ToShow.transform.parent.gameObject, iTween.Hash("x", XP_ToShow.transform.parent.localPosition.x - 400, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveFrom(Reset_Btn.transform.parent.gameObject, iTween.Hash("x", XP_ToShow.transform.parent.localPosition.x - 400, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));



        //iTween.MoveFrom(Main_XP_ToShow.transform.parent.gameObject, iTween.Hash("x", Main_XP_ToShow.transform.parent.localPosition.x + 550, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
    }





    public void UIZoomIn()
    {
        if (ObjectivePanel.GetComponent<RectTransform>().anchoredPosition.y != 5)
            return;



        UIReset();
        //print("side......" + SidePannelOpen.transform.position);
        iTween.MoveAdd(ObjectivePanel, iTween.Hash("y", ObjectivePanel.transform.localPosition.y - 1000, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveAdd(Info_Open_Btn.gameObject, iTween.Hash("x", Info_Open_Btn.transform.localPosition.x + 250, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveAdd(SidePannelOpen.transform.parent.gameObject, iTween.Hash("x", SidePannelOpen.transform.parent.localPosition.x - 1000f, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveAdd(InventoryScroll, iTween.Hash("x", InventoryScroll.transform.localPosition.x + 1000f, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveAdd(Slot, iTween.Hash("x", Slot.transform.localPosition.x + 1000f, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));

        //iTween.MoveAdd(ValueDisplaySlider.gameObject, iTween.Hash("x", ValueDisplaySlider.transform.localPosition.x + 250, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));



        iTween.MoveAdd(XP_ToShow.transform.parent.gameObject, iTween.Hash("x", XP_ToShow.transform.parent.localPosition.x - 400, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveAdd(ToDoList_DD.gameObject, iTween.Hash("x", ToDoList_DD.transform.localPosition.x - 400, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveAdd(Reset_Btn.transform.parent.gameObject, iTween.Hash("x", XP_ToShow.transform.parent.localPosition.x - 400, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));



        //iTween.MoveAdd(Main_XP_ToShow.transform.parent.gameObject, iTween.Hash("x", Main_XP_ToShow.transform.parent.localPosition.x + 550, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
    }

    public void Induction_SwitchON()
    {
       
        Induction_1.GetComponent<Renderer>().material.EnableKeyword("_EMISSION");
        PlaySequenceStep();
        
    }

    //........Increase the water level and melt the ice cubes..........
    public void TempChangeVal()
    {
        //Liquified_Water.gameObject.transform.localScale = new Vector3(0.9f, 0, 0.9f);
        TalkBubble_2.SetActive(false);
        
        if (Temp == 10)
        {
            iTween.ScaleTo(Liquified_Water, iTween.Hash("y", 0.1f, "time", 1f, "easetype", iTween.EaseType.linear));
            iTween.ScaleTo(Ice_Cubes, iTween.Hash("x", Ice_Cubes.transform.localScale.x * 0.8f, "y", Ice_Cubes.transform.localScale.x * 0.8f, "z", Ice_Cubes.transform.localScale.x * 0.8f, "time", 3f, "easetype", iTween.EaseType.linear));
        }

        if (Temp == 20)
        {
            iTween.ScaleTo(Liquified_Water, iTween.Hash("y", 0.2f, "time", 1f, "easetype", iTween.EaseType.linear));
            iTween.ScaleTo(Ice_Cubes, iTween.Hash("x", Ice_Cubes.transform.localScale.x * 0.53f, "y", Ice_Cubes.transform.localScale.x * 0.53f, "z", Ice_Cubes.transform.localScale.x * 0.53f, "time", 3f, "easetype", iTween.EaseType.linear));

        }

        if (Temp == 30)
        {
            TalkBubble_3.SetActive(true);
            iTween.ScaleTo(Liquified_Water, iTween.Hash("y", 0.3f, "time", 1f, "easetype", iTween.EaseType.linear));
            iTween.ScaleTo(Ice_Cubes, iTween.Hash("x", Ice_Cubes.transform.localScale.x * 0.4f, "y", Ice_Cubes.transform.localScale.x * 0.4f, "z", Ice_Cubes.transform.localScale.x * 0.4f, "time", 3f, "easetype", iTween.EaseType.linear));

        }

        if (Temp == 40)
        {
            iTween.ScaleTo(Liquified_Water, iTween.Hash("y", 0.4f, "time", 1f, "easetype", iTween.EaseType.linear));
            iTween.ScaleTo(Ice_Cubes, iTween.Hash("x", Ice_Cubes.transform.localScale.x * 0.27f, "y", Ice_Cubes.transform.localScale.x * 0.27f, "z", Ice_Cubes.transform.localScale.x * 0.27f, "time", 3f, "easetype", iTween.EaseType.linear));

        }

        if (Temp == 50)
        {
            iTween.ScaleTo(Liquified_Water, iTween.Hash("y", 0.5f, "time", 1f, "easetype", iTween.EaseType.linear));
            iTween.ScaleTo(Ice_Cubes, iTween.Hash("x", Ice_Cubes.transform.localScale.x * 0.15f, "y", Ice_Cubes.transform.localScale.x * 0.15f, "z", Ice_Cubes.transform.localScale.x * 0.15f, "time", 3f, "easetype", iTween.EaseType.linear));

        }

        if (Temp == 60)
        {
            iTween.ScaleTo(Liquified_Water, iTween.Hash("y", 0.6f, "time", 1f, "easetype", iTween.EaseType.linear));
            iTween.ScaleTo(Ice_Cubes, iTween.Hash("x", Ice_Cubes.transform.localScale.x * 0f, "y", Ice_Cubes.transform.localScale.x * 0f, "z", Ice_Cubes.transform.localScale.x * 0f, "time", 3f, "easetype", iTween.EaseType.linear));

        }
    }
   
    //...........Increase the temperature.............
    public void IncDecFunction(string nam)
    {
        
        if (!IDFunc)
        {
            if (nam == "TempInc_Btn")
            {
                if (!IsTopicUnlocked)
                {
                    if (CurrentStepCnt < 2)
                    {
                        ShowInstruction_Panel("Wrong Option Selected");
                        return;
                    }
                    else if (CurrentStepCnt == 2)
                    {
                        Collect_XP(Box_Pkg_Final);
                        
                    }
                }
               

                if (Temp < 100f)
                {

                    if (CurrentStepCnt == 2)
                    {
                        Temp = Temp + 10;

                        Invoke("TempChangeVal", 0.1f);

                        Reset_Button.gameObject.SetActive(true);

                        
                    }

                    else
                    {
                        //Temp = -10f;
                        //CancelInvoke("TempChangeVal");
                        ShowInstruction_Panel("Wrong Option Selected");
                    }
                   

                    //............Start the vapours when water increases to full level...........

                    if (Temp > 60)
                    {
                        TalkBubble_3.SetActive(false);
                        
                        Smoke_Start.SetActive(true);
                        Gas_Vapour.SetActive(true);
                    }

                    //if (Temp == 80)
                    //{
                    //    TalkBubble_4.SetActive(true);

                    //}

                    //.............Stop the vapours after scaling down the water level...........
                    if (Temp == 100)
                    {
                        iTween.ScaleTo(Liquified_Water, iTween.Hash("y", 0f, "time", 6f, "delay", 3f, "easetype", iTween.EaseType.linear, "oncomplete", "OffSmoke","oncompletetarget", this.gameObject));
                        TalkBubble_4.SetActive(false);
                        
                    }

                }
          
            }

        }

        Show_Temp.text = Temp.ToString();
        Temp_Text.text = Temp.ToString();
    }

    public void OffSmoke()
    {
        Smoke_Start.SetActive(false);
        TalkBubble_4.SetActive(true);

    }
    

    public void PlaySequenceStep()
    {
       
        if (CurrentStepCnt == 0)
        {
            IDFunc = false;
            Ice_Cubes.transform.GetChild(0).GetChild(0).gameObject.SetActive(true);
            Ice_Cubes.transform.GetChild(0).GetChild(1).gameObject.SetActive(true);
            UpdateToNextStep();
            Collect_XP(Ice_Cubes);
            DragDropSteps--;
            TalkBubble_1.SetActive(true);
            Induction_On.gameObject.SetActive(true);
            iTween.RotateTo(Container_Box, iTween.Hash("z", 0, "time", 0.5f, "easetype", iTween.EaseType.linear));
           
            
        }
        
        else if (CurrentStepCnt == 1)
        {

            IDFunc = false;
            UpdateToNextStep();
            Collect_XP(Induction);
            DragDropSteps--;
            
            TalkBubble_1.SetActive(false);
            TalkBubble_2.SetActive(true);
            Induction_On.gameObject.SetActive(false);
       
        }
        else if (CurrentStepCnt == 2)
        {
            UpdateToNextStep();

            DragDropSteps--;
        }

    }


    public void UpdateToNextStep()
    {
        CurrentStepCnt++;

        if (CurrentStepCnt >= Sub_Top_XP.Count)
        {
            if(ShowLabels_Btn.GetComponent<Image>().color.a<1)
            ShowLabels_Btn.GetComponent<Image>().color = new Color(1, 1, 1, 1f);
        }
        PlayerPrefs.SetInt("CurrentStepCount", PlayerPrefs.GetInt("CurrentStepCount") + 1);

        if (IsTopicUnlocked)
        {
            if (CurrentStepCnt < Sub_Top_ToDoList.Count)
            {
                ObjectivePanel.transform.Find("ObjectiveText").GetComponent<Text>().text = Sub_Top_ToDoList[CurrentStepCnt];
                ObjectivePanel.transform.localScale = new Vector3(1, 0, 1);
                iTween.ScaleTo(ObjectivePanel, iTween.Hash("y", 1, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine));
            }
            else
            {
                ObjectivePanel.transform.localScale = new Vector3(1, 0, 1);
            }
        }

        if (CurrentStepCnt <= Sub_Top_ToDoList.Count)
            NumOfSteps.text = DragDropSteps.ToString();

        
    }

    public void WrongDragDrop()
    {
        ShowInstruction_Panel("Wrong Option Selected");
    }

    public void WrongPlacement()
    {
        ShowInstruction_Panel("Wrong Placement");
    }


    public void ShowInstruction_Panel(string TextToDisplay)
    {
        var mouse = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);



        if (mouse.x < Screen.width / 2)
        {
            // WronOptionPanel
            //print("Mouse is on left side of screen.");
            InstructionPanel.GetComponent<RectTransform>().pivot = new Vector2(0, 0);
        }



        if (mouse.x > Screen.width / 2)
        {
            //print("Mouse is on right side of screen.");
            InstructionPanel.GetComponent<RectTransform>().pivot = new Vector2(1, 0);
        }

        InstructionPanel.transform.position = Input.mousePosition;
        UIcanvas.GetComponent<GraphicRaycaster>().enabled = false;
        Camera.main.GetComponent<PhysicsRaycaster>().enabled = false;
        iTween.Stop(InstructionPanel.gameObject);

        InstructionPanel.transform.Find("InstructionText").GetComponent<Text>().text = TextToDisplay;
        //iTween.ScaleTo(GlassPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 0.01f, "easetype", iTween.EaseType.spring));
        iTween.ScaleTo(InstructionPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 0.3f, "easetype", iTween.EaseType.spring, "oncomplete", "CloseInstructionPanel", "oncompletetarget", this.gameObject));
        ObjectivePanel.transform.localScale = new Vector3(1, 0, 1);
    }


    public void ShowLabels()
    {
        if (ShowLabels_Btn.GetComponent<Image>().color.a <= 0.5f)
            return;

        if (!Labels.activeInHierarchy)
        {
            Labels.SetActive(true);
            
            
        }
        else
            Labels.SetActive(false);
    }

    public void ResetExperiment()
    {
        Ice_Cubes.gameObject.transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
        Ice_Cubes.SetActive(false);
        Ice_Cubes.transform.GetChild(0).GetChild(0).gameObject.SetActive(false);
        Ice_Cubes.transform.GetChild(0).GetChild(1).gameObject.SetActive(false);

        iTween.RotateTo(Container_Box, iTween.Hash("z", 60, "time", 0.1f, "easetype", iTween.EaseType.linear));

        TalkBubble_1.SetActive(false);
        TalkBubble_2.SetActive(false);
        TalkBubble_3.SetActive(false);
        TalkBubble_4.SetActive(false);

        Induction_On.gameObject.SetActive(false);

        Smoke_Start.gameObject.SetActive(false);

        Gas_Vapour.gameObject.SetActive(false);

        Reset_Button.gameObject.SetActive(false);

        Induction_1.GetComponent<Renderer>().material.DisableKeyword("_EMISSION");

        if (Ice_Cubes.GetComponent<iTween>())
            Destroy(Ice_Cubes.GetComponent<iTween>());

        iTween.ScaleTo(Liquified_Water, iTween.Hash("y", 0f, "time", 0f, "easetype", iTween.EaseType.linear));
        

        //IDFunc = false;
        Temp = -10f;
        Show_Temp.text = Temp.ToString();
        Temp_Text.text = Temp.ToString();
        CancelInvoke("TempChangeVal");

        IsTopicUnlocked = true;
        Labels.SetActive(false);
        CurrentStepCnt = 0;
        ShowLabels_Btn.GetComponent<Image>().color =new Color(1,1,1,0.5f);
        


        if (!IsAR)
        {
            CamOrbit.zoomMax = 2.5f;
            CamOrbit.zoomMin = 0.5f;
            CamOrbit.zoomStart = 1.5f;
            CamOrbit.distance = CamOrbit.zoomStart;
            CamOrbit.transform.position = new Vector3(0, 0.25f, 0);
            CamOrbit.cameraRotUpStart = 30f;

        }

        UIDragAndDrop[] UIButtons = GameObject.FindObjectsOfType<UIDragAndDrop>();
        for (int i = 0; i < UIButtons.Length; i++)
        {
            UIButtons[i].Restart();
        }

        ObjectivePanel.transform.Find("ObjectiveText").GetComponent<Text>().text = Sub_Top_ToDoList[CurrentStepCnt];
        ObjectivePanel.transform.localScale = new Vector3(1, 0, 1);
        iTween.ScaleTo(ObjectivePanel, iTween.Hash("y", 1, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine));

        DragDropSteps = UIcanvas.GetComponentsInChildren<UIDragAndDrop>().Length;

        NumOfSteps.text = DragDropSteps.ToString();

    }

    //float walterscle = 0, icsScle = 0, iceAlpha = 1;



    //public void WaterToIce() {
    //    if (Temp > 0)
    //    
    //        if (Liquified_Water.transform.localScale.y < 0.6f)
    //        {
    //            walterscle = walterscle + (Temp * 0.00001f);
    //            icsScle = icsScle - (Temp * 0.00002f);

    //            Liquified_Water.transform.localScale = new Vector3(0.9f, 0 + walterscle, 0.9f);
    //            Ice_Cubes.transform.localScale = new Vector3(1.5f + icsScle, 1.5f + icsScle, 1.5f + icsScle);
    //        }
    //        else {

    //        }
    //        if (iceAlpha < 1)
    //        {
    //            iceAlpha = iceAlpha + 0.01f;
    //        }

    //        Liquified_Water.GetComponent<Renderer>().material.color = new Color(Liquified_Water.GetComponent<Renderer>().material.color.r,
    //                                                                            Liquified_Water.GetComponent<Renderer>().material.color.g,
    //                                                                            Liquified_Water.GetComponent<Renderer>().material.color.b,
    //                                                                            iceAlpha);
    //    }
    //    else if (Temp < 0)
    //    {

    //        if (iceAlpha > 0)
    //        {
    //            iceAlpha = iceAlpha - 0.01f;
    //        }
    //        Liquified_Water.GetComponent<Renderer>().material.color = new Color(Liquified_Water.GetComponent<Renderer>().material.color.r,
    //                                                                            Liquified_Water.GetComponent<Renderer>().material.color.g,
    //                                                                            Liquified_Water.GetComponent<Renderer>().material.color.b,
    //                                                                            iceAlpha);
    //    }
    //    else {

            
           
    //    }

    //}

    // Update is called once per frame
    void Update()
    {
        
    }
}
