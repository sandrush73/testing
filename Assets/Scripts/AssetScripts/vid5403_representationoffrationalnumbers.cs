﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;

public class vid5403_representationoffrationalnumbers : MonoBehaviour, IPointerDownHandler{
    // Start is called before the first frame update

    public Canvas C_BG;

    public orbit CamOrbit;

    public AROrbitControls ArOrbit;

    public bool IsAR;

    public GameObject MainObj, Ground;

    public int OptionUnlockedTill, CurrentTopicSelected, TopicToCheck;

    public float Sub_XP_Earned, Total_XP;

    public GameObject InstructionPanel_DD, ToDoList_Panel;

    public List<Dictionary<string, object>> CSV_data;

    public List<String> TaskList,XpList,InfoText;

    public List<GameObject> AllTaskData;

    public GameObject[] Labels;

    public Text XP_ToShow, XP_Effect, Main_XP_ToShow, localTotalXpValueText, localGainedXpValueText, ObjectiveInfoText, LGListText, IListText, AListText, 
        InfoHeadingText, GF_ResultTxt, E_valueTxt, m_valtxt;

    public GameObject TaskObj, AllTaskListObj;

    public RectTransform rT;

    public int localTotalXpValue, localGaniedXpValue,MainXpValue;

    public GameObject ControlPanel, LocalXP, Main_Xp, ObjectivePanel, InformationPanel, InfoButton, MainSlider, InfoPanelBg, TasklistPanel, WronOptionPanel, Gained_XP_ArrowBut;

    public Button InfoCloseBtn, InfoContinueBtn, Gained_XP_Panel, CpCloseBtn, IncButton1, DecButton1, IncButton2, DecButton2;
       
    public bool sessionStart;

    public TextAsset CSVFile;

    public AssetBundle assetBundle;
       
    public string[] CorrectMsgs = new string[] { "Nice!", "WellDone!", "Excellent!", "Correct!" };

    public Color Highlight_Color, Normal_Color;

    public Text MessageT, DescText;

    //....................................................................................................................................

    public Button submitBtn, Option1, Option2, ConvertBtn;

    public GameObject Point1, Point2, ScrewwithHolder, Screws, RangePanel, Pointsssss, QuestionPanel;

    public int pointCount = 1,RangeVal = 0;
   
    public Vector3[] pointsResult;

    public int QuestionN, QuestionD;

    public Text QuestionText;

    public int ImQuestionN, ImQuestionD;

    public bool imp;

    public int mixedInt, ImN, ImD;

    void Start()
    {        
        LoadFromAsssetBundle();

        MainObj = this.gameObject;

        C_BG = GameObject.Find("Canvas_BG").GetComponent<Canvas>();

        Ground = GameObject.Find("Ground");

        if (!IsAR)
        {
            CamOrbit = Camera.main.GetComponentInParent<orbit>();

            C_BG.transform.parent = Camera.main.transform;
            C_BG.GetComponentInChildren<RawImage>().enabled = true;
            C_BG.renderMode = RenderMode.ScreenSpaceCamera;

            C_BG.worldCamera = Camera.main;

            CamOrbit.target = Camera.main.transform.parent;

            CamOrbit.target.position = new Vector3(0, 0.1f, 0);
            CamOrbit.maxRotUp = 45;
            CamOrbit.minRotUp = 0;
            CamOrbit.zoomMin = 0.5f;
            CamOrbit.zoomMax = 2.5f;
            CamOrbit.zoomStart =1f;

            CamOrbit.minSideRot = 40;
            CamOrbit.maxSideRot = 40;
            CamOrbit.cameraRotUpStart = 30;
            CamOrbit.cameraRotSideStart = 0;
            CamOrbit.Start();
        }
        else
        {
            Ground.SetActive(false);
            C_BG.gameObject.SetActive(false);
            ArOrbit = MainObj.transform.parent.gameObject.GetComponent<AROrbitControls>();
        }
                      
        sessionStart = false;

        //OptionUnlockedTill = 0;
        //TopicToCheck = 1;
        //MainXpValue = 10;       

        ReadingDataFromCSVFile();
        ReadingXps();
        FindingObjects();
        TaskListCreation();
        AssigningClickEvents();
        LabelsAssignment();
        AssignInitialValues();

        //GenerateRandomFraction();        
        //print("Find " + QuestionN+"/"+ QuestionD);
      
    }


    public void LoadFromAsssetBundle()
    {
        //......................................................For Testing ..............................................................

        //CSVFile = Resources.Load("vid5403_representationoffrationalnumbersCSV") as TextAsset;

        //............................................................ For Assetbundle Creation ..........................................

        assetBundle = AssetBundle.LoadFromFile(PlayerPrefs.GetString("AssetBundleUrl"));

        CSVFile = assetBundle.LoadAsset(PlayerPrefs.GetString("LibraryName") + "CSV", typeof(TextAsset)) as TextAsset;        

        //...............................................................................................................................
        Camera.main.gameObject.AddComponent<PhysicsRaycaster>();

        Camera.main.backgroundColor = Color.black;

        GameObject TargetForCamera = GameObject.Find("Target");

        if (TargetForCamera != null)
        {
            TargetForCamera.AddComponent<orbit>();
            CamOrbit = TargetForCamera.GetComponent<orbit>();
            IsAR = false;
        }
        else
        {
            this.transform.parent.gameObject.AddComponent<AROrbitControls>();
            ArOrbit = this.transform.parent.gameObject.GetComponent<AROrbitControls>();
            IsAR = true;
        }

    }

    public void ReadingDataFromCSVFile()
    {

        TaskList = new List<string>();
        XpList = new List<string>();
        InfoText = new List<string>();

        AllTaskData = new List<GameObject>();

        CSV_data = CSVReader.Read(CSVFile);

        for (var i = 0; i < CSV_data.Count; i++)
        {
            if (CSV_data[i]["TaskList"].ToString() != "" && CSV_data[i]["TaskList"].ToString() != null)
            {
                TaskList.Add(CSV_data[i]["TaskList"].ToString());
            }

            //if (CSV_data[i]["XPList"].ToString() != "" && CSV_data[i]["XPList"].ToString() != null)
            //{
            //    XpList.Add(CSV_data[i]["XPList"].ToString());
            //}

            if (CSV_data[i]["InfoText"].ToString() != "" && CSV_data[i]["InfoText"].ToString() != null)
            {
                InfoText.Add(CSV_data[i]["InfoText"].ToString());
            }
        }
    }
    
    public void FindingObjects() {

        InformationPanel = GameObject.Find("InformationPanel");

        InfoCloseBtn = GameObject.Find("InfoCloseBtn").GetComponent<Button>();

        InfoContinueBtn = GameObject.Find("InfoContinueBtn").GetComponent<Button>();

        InfoPanelBg = GameObject.Find("InfoPanelBg");
        
        XP_ToShow = GameObject.Find("localGainedXpValueText").GetComponent<Text>();

        XP_Effect = GameObject.Find("Effect_Text").GetComponent<Text>();

        Main_XP_ToShow = GameObject.Find("Main_XP_Value").GetComponent<Text>();

        localGainedXpValueText = GameObject.Find("localGainedXpValueText").GetComponent<Text>();

        localTotalXpValueText = GameObject.Find("GainedXP_LocalTotalXp").GetComponent<Text>();

        ObjectiveInfoText = GameObject.Find("ObjectiveInfoText").GetComponent<Text>();

        TaskObj = GameObject.Find("TaskObj");

        AllTaskListObj = GameObject.Find("AllTaskList");

        ControlPanel = GameObject.Find("ControlPanel");
        LocalXP = GameObject.Find("LocalXP");
        Main_Xp = GameObject.Find("Main_Xp");
        ObjectivePanel = GameObject.Find("ObjectivePanel");
        InformationPanel = GameObject.Find("InformationPanel");
        InfoButton = GameObject.Find("InfoButton");
        MainSlider = GameObject.Find("MainSlider");
        WronOptionPanel = GameObject.Find("WrongPanel");

        Gained_XP_Panel = GameObject.Find("Gained_XP_Panel").GetComponent<Button>();
        TasklistPanel = GameObject.Find("TasklistPanel");
        CpCloseBtn = GameObject.Find("CpCloseBtn").GetComponent<Button>();

        InfoHeadingText = GameObject.Find("InfoHeadingText").GetComponent<Text>();
        LGListText = GameObject.Find("LGListText").GetComponent<Text>();
        IListText = GameObject.Find("IListText").GetComponent<Text>();
        AListText = GameObject.Find("AListText").GetComponent<Text>();

        Gained_XP_ArrowBut = GameObject.Find("Gained_XP_ArrowBut");

        //............................................................................................................

        IncButton1 = GameObject.Find("IncButton1").GetComponent<Button>();

        DecButton1 = GameObject.Find("DecButton1").GetComponent<Button>();

        m_valtxt = GameObject.Find("m_valtxt").GetComponent<Text>();

        //..................................................................................................

        submitBtn = GameObject.Find("submitBtn").GetComponent<Button>();

        Point1 = GameObject.Find("Point1");

        Point2 = GameObject.Find("Point2");

        ScrewwithHolder = GameObject.Find("ScrewwithHolder");

        Screws = GameObject.Find("Screws");

        Option1 = GameObject.Find("Option1").GetComponent<Button>();

        Option2 = GameObject.Find("Option2").GetComponent<Button>();

        RangePanel = GameObject.Find("RangePanel");

        Pointsssss = GameObject.Find("Pointsssss");

        ConvertBtn = GameObject.Find("ConvertBtn").GetComponent<Button>();

        QuestionPanel = GameObject.Find("QuestionPanel");
        //Invoke("PopUpShow", 3f);
    }

    public int totXp, loclXp;

    public void ReadingXps()
    {
        totXp = PlayerPrefs.GetInt("TotalXP");
        loclXp = PlayerPrefs.GetInt("MaxEarnings");

        if (loclXp != 0)
        {
            OptionUnlockedTill = 0;
        }
        else
        {
            OptionUnlockedTill = 1;
        }

        TopicToCheck = 1;

        MainXpValue = totXp;

        int t = 0;
        int p = 0;

        for (int k = 0; k < TaskList.Count; k++)
        {
            p = loclXp / TaskList.Count;
            XpList.Add(p.ToString());
            t = t + p;
        }

        if (t != loclXp)
        {
            XpList[XpList.Count - 1] = (p + (loclXp - t)).ToString();
        }
    }

    public float result;
    public void GetResult()
    {
        result = ((float)(TopicToCheck - 1) / (float)(TaskList.Count)) * 100;
        Camera.main.SendMessage("UnloadActually", result);
    }
    public void AssigningClickEvents() {
                
        iTween.ScaleTo(InformationPanel.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

        InfoCloseBtn.onClick.AddListener(() => CloseInfoPanel());

        InfoContinueBtn.onClick.AddListener(() => ContinueInfoPanel());

        InfoButton.GetComponent<Button>().onClick.AddListener(() => OpenInfoPanel());

        Gained_XP_Panel.onClick.AddListener(() => OpenTaskList());

        CpCloseBtn.onClick.AddListener(() => OpenOrCloseControlPanel());

        IncButton1.GetComponent<Button>().onClick.AddListener(() => ChangeFration(1));

        DecButton1.GetComponent<Button>().onClick.AddListener(() => ChangeFration(-1));    

        MainSlider.GetComponent<Slider>().onValueChanged.AddListener(delegate { ChangeDistance(MainSlider.GetComponent<Slider>().value, MainSlider.GetComponentInChildren<Text>()); });

        AddListener(EventTriggerType.PointerDown, DisableOrbit, MainSlider.gameObject);

        AddListener(EventTriggerType.PointerUp, EnableOrbit, MainSlider.gameObject);

        submitBtn.onClick.AddListener(() => SubmitFun());

        QuestionText = GameObject.Find("QuestionText").GetComponent<Text>();
        
        Option1.onClick.AddListener(() => ChangeFractionType(true));

        Option2.onClick.AddListener(() => ChangeFractionType(false));

        RangePanel.transform.GetChild(3).GetComponent<Button>().onClick.AddListener(() => ChangeRange(-1));

        RangePanel.transform.GetChild(4).GetComponent<Button>().onClick.AddListener(() => ChangeRange(1));


        AddListener(EventTriggerType.PointerDown, ChangeRangeDup, RangePanel.transform.GetChild(4).gameObject,1,true);
        AddListener(EventTriggerType.PointerUp, ChangeRangeDup, RangePanel.transform.GetChild(4).gameObject, 1,false);

        AddListener(EventTriggerType.PointerDown, ChangeRangeDup, RangePanel.transform.GetChild(3).gameObject, -1, true);
        AddListener(EventTriggerType.PointerUp, ChangeRangeDup, RangePanel.transform.GetChild(3).gameObject, -1, false);



        ConvertBtn.onClick.AddListener(() => ConvertToMixedFraction());

        ConvertBtn.gameObject.SetActive(false);
        //RangePanel.transform.GetChild(0)
    }

    public void AssignInitialValues()
    {
        //InfoHeadingText.text = InfoText[0];
        //LGListText.text = InfoText[1];
        //IListText.text = InfoText[2];
        //AListText.text = InfoText[3];
        // MainXpValue = 10;

        if (OptionUnlockedTill > 0)
        {
            LocalXP.transform.localScale = new Vector3(0, 0, 0);
            ObjectivePanel.transform.localScale = new Vector3(0, 0, 0);
        }

        imp = false;
        InfoCloseBtn.gameObject.transform.localScale = new Vector3(0, 0, 0);
        localGainedXpValueText.text = "0";
        Main_XP_ToShow.text = MainXpValue + "";
        WronOptionPanel.transform.localScale = new Vector3(0, 0, 0);
        ScrewwithHolder.transform.GetChild(1).gameObject.AddComponent<SmoothLook>();
        ScrewwithHolder.transform.GetChild(1).gameObject.SetActive(false);
        ScrewwithHolder.SetActive(false);


        QuestionN = 1;
        QuestionD = 2;


        QuestionText.text = "Plot the number " + QuestionN + "/" + QuestionD;

        for (int i = -15; i < 15; i++) {

            GameObject obj = Instantiate(Pointsssss.transform.GetChild(0).gameObject);
           obj.transform.parent = Pointsssss.transform;

           obj.transform.localPosition = new Vector3(i * 0.6f - 0.6f, 0.028f, 0);

            //GameObject obj2 = Instantiate(Labels[0], new Vector3(0, 0, - 0.037f), Quaternion.identity);
           GameObject obj2 = Instantiate(Labels[0]);
 
           obj2.transform.parent = obj.transform;
           obj2.transform.localPosition = new Vector3(0, 0, 0);
           obj2.transform.localScale = new Vector3(0.2222222f, 0.2222222f, 0.2222222f);         
           obj2.transform.GetChild(0).gameObject.GetComponentInChildren<TextMeshPro>().text = i.ToString();
           //obj2.transform.GetChild(1).gameObject.GetComponentInChildren<TextMeshPro>().text = i.ToString();
        }

        Labels[0].SetActive(false);

        Pointsssss.transform.GetChild(0).gameObject.SetActive(false);
        Pointsssss.transform.localPosition = new Vector3(0.3f, 0, 0);
    }

    public void TaskListCreation() {

        rT = AllTaskListObj.transform.parent.GetComponent<RectTransform>();

        localGaniedXpValue = 0;

        for (int i = 0; i < TaskList.Count; i++)
        {
            GameObject Tl = (GameObject)Instantiate(TaskObj);
            Tl.transform.SetParent(AllTaskListObj.transform);
            Tl.transform.localScale = new Vector3(1, 1, 1);
            Tl.name = i.ToString();

            AllTaskData.Add(Tl);
            AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(false);
            AllTaskData[i].gameObject.transform.GetChild(2).gameObject.SetActive(false);
            AllTaskData[i].gameObject.transform.GetChild(3).GetComponentInChildren<Text>().text = TaskList[i];
            AllTaskData[i].gameObject.transform.GetChild(4).GetComponentInChildren<Text>().text = "+" + XpList[i] + " XP";

            localTotalXpValue += int.Parse(XpList[i]);
        }

        rT.sizeDelta = new Vector2(rT.sizeDelta.x, TaskList.Count * 50);
        localTotalXpValueText.text = "/" + localTotalXpValue.ToString();

        AllTaskData[0].gameObject.transform.GetChild(2).gameObject.SetActive(true);
        ObjectiveInfoText.text = AllTaskData[0].gameObject.transform.GetChild(3).GetComponentInChildren<Text>().text;
    }

    public void LocalXpCalculation()
    {
        for (int i = 0; i < TaskList.Count; i++)
        {
            AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(false);
            AllTaskData[i].gameObject.transform.GetChild(2).gameObject.SetActive(false);
        }

        if (OptionUnlockedTill < 1)
        {
            if ((TopicToCheck) < (TaskList.Count + 1))
            {
                localGaniedXpValue = 0;

                for (int i = 0; i < TopicToCheck - 1; i++)
                {
                    localGaniedXpValue += int.Parse(XpList[i]);
                }

                for (int i = 0; i < TopicToCheck; i++)
                {
                    AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(true);
                }

                localGainedXpValueText.text = localGaniedXpValue.ToString();

                AllTaskData[TopicToCheck - 1].gameObject.transform.GetChild(2).gameObject.SetActive(true);

                ObjectiveInfoText.text = AllTaskData[TopicToCheck - 1].gameObject.transform.GetChild(3).GetComponentInChildren<Text>().text;
            }
            else
            {
                for (int i = 0; i < TaskList.Count; i++)
                {
                    AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(true);
                    AllTaskData[i].gameObject.transform.GetChild(2).gameObject.SetActive(false);
                }

                localGainedXpValueText.text = localTotalXpValue.ToString();

                XP_Effect.text = "+" + localTotalXpValue + " XP";

                XP_Effect.transform.position = (XP_ToShow.transform.position);

                iTween.Stop(XP_Effect.gameObject);

                iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeOutElastic));

                iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", XP_Effect.transform.position.x, "y", XP_Effect.transform.position.y + 50, "z", 0, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

                iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", Main_XP_ToShow.transform.position.x, "y", Main_XP_ToShow.transform.position.y, "z", 0, "delay", 1.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

                iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "delay", 2f, "time", 0.5f, "easetype", iTween.EaseType.linear, "oncomplete", "MainXpCalculation", "oncompletetarget", this.gameObject));
            }
        }
        else
        {
            for (int i = 0; i < TaskList.Count; i++)
            {
                AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(true);
                AllTaskData[i].gameObject.transform.GetChild(2).gameObject.SetActive(false);
            }

            localGainedXpValueText.text = localTotalXpValue.ToString();
        }
    }

    public void CloseAllpanels()
    {
       
        ControlPanel.GetComponent<Animator>().Play("CpanelFullClose");
        LocalXP.GetComponent<Animator>().Play("LocalXpPanelClose");
        Main_Xp.GetComponent<Animator>().Play("MainXpPanelClose");
        ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelClose");
        InfoButton.GetComponent<Animator>().Play("InfoButtonClose");
        MainSlider.GetComponent<Animator>().Play("MainSliderClose");
        
        if(TasklistPanel.gameObject.transform.localScale.y!=0)
            iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));
    }

    public void OpenAllpanels()
    {

        if (OptionUnlockedTill < 1)
        {
            if (TopicToCheck <= (TaskList.Count))
            {
                LocalXP.GetComponent<Animator>().Play("LocalXpPanelOPen");
                ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen");
            }
        }
        ControlPanel.GetComponent<Animator>().Play("CpanelOpen");
        
        Main_Xp.GetComponent<Animator>().Play("MainXpPanelOpen");
       
        InfoButton.GetComponent<Animator>().Play("InfoButtonOpen");
        MainSlider.GetComponent<Animator>().Play("MainSliderOpen");
        iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "time", 0.2f, "easetype", iTween.EaseType.linear));
        iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));
    }

    public void AddXpEffect() {
                
        if (TopicToCheck <= (TaskList.Count))
        {            

            GameObject.Find("Canvas").GetComponent<CanvasGroup>().interactable = false;

            XP_Effect.GetComponent<RectTransform>().anchoredPosition = new Vector2(0,0);

            XP_Effect.text = "+"+int.Parse(XpList[TopicToCheck - 1]) + " XP";

            XP_Effect.transform.localScale = new Vector3(1, 0, 1);

            iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeOutElastic));

            iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", XP_Effect.transform.position.x, "y", XP_Effect.transform.position.y + 50, "z", 0, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

            iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", XP_ToShow.transform.position.x, "y", XP_ToShow.transform.position.y, "z", 0, "delay", 1.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

            iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "delay", 2f, "time", 0.5f, "easetype", iTween.EaseType.linear, "oncomplete", "LocalXpCalculation", "oncompletetarget", this.gameObject));

            TopicToCheck++;

            ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelClose");

            Invoke("EnableRayCast", 3);

           
        }
        
    }

    public void EnableRayCast() {
        //Camera.main.GetComponent<PhysicsRaycaster>().enabled = true;
        GameObject.Find("Canvas").GetComponent<CanvasGroup>().interactable = true;
        if (TopicToCheck <= (TaskList.Count))
        {
            ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen");
        }
    }

    public void MainXpCalculation()
    {
        if (OptionUnlockedTill == 0)
        {
            OptionUnlockedTill = 1;

            MainXpValue += localTotalXpValue;

            Main_XP_ToShow.text = MainXpValue.ToString();

            LocalXP.GetComponent<Animator>().Play("LocalXpPanelClose");

            ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelClose");

            if ((ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelClose")) || (ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelFullClose")))
            {
                ControlPanel.GetComponent<Animator>().Play("CpanelOpen");
            }
        }
    }

    public void CloseInfoPanel()
    {

        AddXpEffect();
        sessionStart = true;
        InfoPanelBg.SetActive(false);
        iTween.ScaleTo(InformationPanel.gameObject, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));
        OpenAllpanels();
    }

    public void ContinueInfoPanel()
    {
        sessionStart = true;
        InfoPanelBg.SetActive(false);
        iTween.ScaleTo(InformationPanel.gameObject, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));
        InfoCloseBtn.transform.localScale = new Vector3(0.27f, 0.27f, 0.27f);
        InfoContinueBtn.transform.localScale = new Vector3(0, 0, 0);
        OpenAllpanels();

       
    }

    public void OpenInfoPanel()
    {
        sessionStart = false;
        iTween.ScaleTo(InformationPanel.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));
        InfoPanelBg.SetActive(true);
        iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "time", 0.2f, "easetype", iTween.EaseType.linear));
        iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));
        CloseAllpanels();
    }

    public void OpenTaskList() {


        if (TasklistPanel.gameObject.transform.localScale.y != 1)
        {
            iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.01f, "time", 0.2f, "easetype", iTween.EaseType.easeInSine));

            ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelClose");

            iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 180, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));

            if ((ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelOpen")))
            {
                ControlPanel.GetComponent<Animator>().Play("CpanelClose");
            }
        }
        else
        {
            iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "delay", 0.01f, "time", 0.2f, "easetype", iTween.EaseType.easeInSine));
            iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));

            ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen");

            ControlPanel.GetComponent<Animator>().Play("CpanelOpen");
        }
    }

    public void OpenOrCloseControlPanel() {

        if ((ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelClose")) || (ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelFullClose")))
        {
            ControlPanel.GetComponent<Animator>().Play("CpanelOpen");
        }
        else
        {
            ControlPanel.GetComponent<Animator>().Play("CpanelClose");
        }
    }

    public void WrongOptionSelected()
    {
        WronOptionPanel.transform.localScale = new Vector3(0, 0, 0);

        iTween.Stop(WronOptionPanel.gameObject);

        iTween.ScaleTo(WronOptionPanel.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.01f, "time", 0.2f, "easetype", iTween.EaseType.easeInSine));

        iTween.ScaleTo(WronOptionPanel.gameObject, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 1f, "time", 0.2f, "easetype", iTween.EaseType.easeInSine));

        // InfoPanelBg.SetActive(true);

        Invoke("CloseBlurBg", 1f);

        GameObject.Find("Canvas").GetComponent<CanvasGroup>().interactable = false;

        ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelClose");


        var mouse = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);

        if (mouse.x < Screen.width / 2)
        {
            // WronOptionPanel
            //print("Mouse is on left side of screen.");
            WronOptionPanel.GetComponent<RectTransform>().pivot = new Vector2(0, 0);
        }

        if (mouse.x > Screen.width / 2)
        {
            //print("Mouse is on right side of screen.");
            WronOptionPanel.GetComponent<RectTransform>().pivot = new Vector2(1, 0);
        }


        WronOptionPanel.transform.position = Input.mousePosition;

    }

    public void CloseBlurBg()
    {
        InfoPanelBg.SetActive(false);
        GameObject.Find("Canvas").GetComponent<CanvasGroup>().interactable = true;

        if (OptionUnlockedTill < 1)
        {
            if (InformationPanel.gameObject.transform.localScale.x == 0f)
            {
                //ObjectivePanel.GetComponent<Animator>().Play("");
                ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen", 0, 0f);
            }
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (InformationPanel.gameObject.transform.localScale.x != 1 && sessionStart)
        {
            if (OptionUnlockedTill < 1)
            {
                if (TopicToCheck <= (TaskList.Count))
                {
                    iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "time", 0.2f, "easetype", iTween.EaseType.linear));
                    iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));
                    ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen");
                    ControlPanel.GetComponent<Animator>().Play("CpanelOpen");
                }
            }  
        }  
    }

    public void LabelsAssignment()
    {
        GameObject Label = GameObject.Find("Labels");

        Labels = new GameObject[Label.transform.childCount];

        for (int j = 0; j < Labels.Length; j++)
        {
            Labels[j] = Label.transform.GetChild(j).gameObject;

            GameObject TempLabels = Labels[j];

            foreach (Transform ChildLabel in TempLabels.transform)
            {
                if (ChildLabel.name.Contains("SmoothLook"))
                {
                    ChildLabel.gameObject.AddComponent<SmoothLook>();
                    //ChildLabel.gameObject.AddComponent<ScaleRelativeToCamera>();
                }
            }
        }

        for (int k = 0; k < Labels.Length; k++)
        {
            string name = Labels[k].name.Remove(Labels[k].name.Length - 6);

            if (GameObject.Find(name + "") != null)
            {
                GameObject dummy = GameObject.Find(name + "").gameObject;

                Labels[k].transform.parent = dummy.transform;
                Labels[k].transform.rotation = Quaternion.identity;
            }
        }
    }

    public void AddListener(EventTriggerType eventType, Action MethodToCall, GameObject TriggerObjToAdd)
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall());
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }

    public void AddListener(EventTriggerType eventType, Action<float> MethodToCall, GameObject TriggerObjToAdd)
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall(TriggerObjToAdd.GetComponent<Slider>().value));
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }

    public void AddListener(EventTriggerType eventType, Action<int,bool> MethodToCall, GameObject TriggerObjToAdd,int t, bool tt)
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall(t,tt));
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }

    public void AddListener(EventTriggerType eventType, Action<GameObject> MethodToCall, GameObject TriggerObjToAdd,GameObject obj)
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall(obj));
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }

    public void EnableOrbit()
    {
        if (!IsAR)
        {
            CamOrbit.DisableInteration = false;
        }
        else
        {
            ArOrbit.DisableInteration = false;
        }

        if (InformationPanel.gameObject.transform.localScale.x != 1)
        {
            if (TopicToCheck <= (TaskList.Count))
            {
                iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "time", 0.2f, "easetype", iTween.EaseType.linear));
                iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));
                ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen");
            }
        }                         
    }
       
    public void DisableOrbit()
    {
        if (!IsAR)
        {
            CamOrbit.DisableInteration = true;
        }
        else
        {
            ArOrbit.DisableInteration = true;
        }
               
    }
    
    public Vector3 MidPoint(GameObject obj1, GameObject obj2)
    {
        Vector3 midp = (obj1.transform.localPosition + obj2.transform.localPosition) / 2;
        return midp;
    }

    public float Scale(GameObject obj1, GameObject obj2)
    {
        float scl = Vector3.Distance(obj1.transform.localPosition, obj2.transform.localPosition);
        return scl;
    } 

    public void PopUpShow() {
        
    }

    public void GenerateRandomFraction()
    {
        if (!imp)
        {
            if (OptionUnlockedTill < 1)
            {
                if (TopicToCheck < 3)
                {

                    iTween.ScaleTo(QuestionText.gameObject.transform.parent.gameObject, iTween.Hash("x", 1f, "y",1f, "z", 1f, "delay", 0.002f, "time", 0.3f, "easetype", iTween.EaseType.easeInSine));
                    //QuestionText.gameObject.transform.parent.transform.localScale = new Vector3(1, 1, 1);
                    QuestionD = UnityEngine.Random.Range(3, 9);
                    QuestionN = UnityEngine.Random.Range(1, QuestionD);
                    QuestionText.text = "Plot the number " + QuestionN + "/" + QuestionD;
                }
                else {
                    iTween.ScaleTo(QuestionText.gameObject.transform.parent.gameObject, iTween.Hash("x", 0f, "y", 0f, "z", 0f, "delay", 0.002f, "time", 0.3f, "easetype", iTween.EaseType.easeInSine));

                    //QuestionText.gameObject.transform.parent.transform.localScale = new Vector3(0, 0, 0);
                }
            }
            else
            {
                iTween.ScaleTo(QuestionText.gameObject.transform.parent.gameObject, iTween.Hash("x", 1f, "y", 1f, "z", 1f, "delay", 0.002f, "time", 0.3f, "easetype", iTween.EaseType.easeInSine));

                QuestionD = UnityEngine.Random.Range(3, 9);
                QuestionN = UnityEngine.Random.Range(1, QuestionD);
                QuestionText.text = "Plot the number " + QuestionN + "/" + QuestionD;
            }
        }
        else
        {
            iTween.ScaleTo(QuestionText.gameObject.transform.parent.gameObject, iTween.Hash("x", 1f, "y", 1f, "z", 1f, "delay", 0.002f, "time", 0.3f, "easetype", iTween.EaseType.easeInSine));

            //QuestionText.gameObject.transform.parent.transform.localScale = new Vector3(1, 1, 1);
            QuestionN = UnityEngine.Random.Range(3, 9);
            QuestionD = UnityEngine.Random.Range(2, QuestionN);
           // print("N " + QuestionN + " D " + QuestionD);

            if (QuestionN % QuestionD != 0)
            {
                QuestionText.text = "Plot the number " + "" + QuestionN + "/" + QuestionD;

            }
            else
            {
                GenerateRandomFraction();               
            }
        }
    }

    public void ChangeFration(int IncrVal)
    {       
        IncrVal += pointCount;

        IncrVal = Mathf.Clamp(IncrVal, 1, 9);

        pointCount = IncrVal;

        m_valtxt.text = pointCount.ToString();

        pointsResult = new Vector3[pointCount];

        generatePoints(Point1.transform.localPosition, Point2.transform.localPosition, pointsResult, pointCount);

        for (int j = 0; j < Screws.transform.childCount; j++)
        {
            Destroy(Screws.transform.GetChild(j).gameObject);
        }

        ScrewwithHolder.SetActive(true);

        for (int i = 1; i < pointCount; i++)
        {
            GameObject obj = Instantiate(ScrewwithHolder);
            obj.name = i.ToString();
            //obj.transform.parent = Screws.transform;

            obj.transform.SetParent(Screws.transform, false);

            obj.transform.localPosition = new Vector3(pointsResult[i].x, ScrewwithHolder.transform.localPosition.y, ScrewwithHolder.transform.localPosition.z);

            obj.transform.localRotation = ScrewwithHolder.transform.localRotation;

             obj.transform.GetChild(1).gameObject.GetComponentInChildren<TextMeshPro>().text = i.ToString()+"/"+ pointCount;
            AddListener(EventTriggerType.PointerDown, ShowFraction, obj.gameObject, obj.gameObject);
        }

        ScrewwithHolder.SetActive(false);
        if (!IsAR)
        {
            if (CamOrbit.distance > 2)
                CamOrbit.distance = 1f;
        }

        if (OptionUnlockedTill < 1) {

            if (TopicToCheck == 1) {
                AddXpEffect();
            }
        }
    }


    public int curretnVal = 1;

    public void ChangeRange(int IncrVal)
    {
        //for (int i = 0; i < Pointsssss.transform.childCount; i++)
        //{
        //    iTween.MoveTo(Pointsssss.transform.GetChild(i).gameObject, iTween.Hash("x", Pointsssss.transform.GetChild(i).gameObject.transform.localPosition.x - IncrVal * 0.6f, "y", 0.028f, "z", 0, "delay", 0f, "time", 1f, "easetype", iTween.EaseType.linear, "islocal", true,"oncomplete", "CreatTrack", "oncompletetarget",this.gameObject));
        //}
        ////iTween.MoveTo(Pointsssss.gameObject, iTween.Hash("x", -IncrVal * 0.6f, "y", 0, "z", 0, "delay", 0f, "time", 0.5f, "easetype", iTween.EaseType.linear, "islocal", true));
        //IncrVal += RangeVal;

        ////IncrVal = Mathf.Clamp(IncrVal, -10, 10);

        //RangeVal = IncrVal;

        //RangePanel.transform.GetChild(0).GetComponent<Text>().text = RangeVal.ToString();
        //RangePanel.transform.GetChild(2).GetComponent<Text>().text = (RangeVal+1).ToString();

        //Labels[0].GetComponentInChildren<TextMeshPro>().text = RangeVal.ToString();
        //Labels[1].GetComponentInChildren<TextMeshPro>().text = (RangeVal + 1).ToString();

        curretnVal = IncrVal;


        pointCount = 1;

        m_valtxt.text = pointCount.ToString();

        for (int j = 0; j < Screws.transform.childCount; j++)
        {
            Destroy(Screws.transform.GetChild(j).gameObject);
        }

        if (!IsAR)
        {
            if (CamOrbit.distance > 2)
                CamOrbit.distance = 1f;
        }

        iTween.Stop(Pointsssss.transform.GetChild(0).gameObject);

        //Invoke("CreatTrack", 0.51f);
    }
   

    public void ChangeRangeDup(int val , bool prsd) {

        if (prsd)
        {

            curretnVal = val;
            prsd = false;
            InvokeRepeating("PressingFun", 0f, 1f);
        }
        else
        {
            //ngeVal -= 1;
            CancelInvoke("PressingFun");
        }
    }

    public void PressingFun() {

        for (int i = 0; i < Pointsssss.transform.childCount; i++)
        {
             iTween.MoveTo(Pointsssss.transform.GetChild(i).gameObject, iTween.Hash("x", Pointsssss.transform.GetChild(i).gameObject.transform.localPosition.x - curretnVal * 0.6f, "y", 0.028f, "z", 0, "delay", 0f, "time", 0.9f, "easetype", iTween.EaseType.linear, "islocal", true, "oncomplete", "CreatTrack", "oncompletetarget", this.gameObject));

             //Pointsssss.transform.GetChild(i).gameObject.transform.localPosition = new Vector3(Mathf.Lerp(Pointsssss.transform.GetChild(i).gameObject.transform.localPosition.x, Pointsssss.transform.GetChild(i).gameObject.transform.localPosition.x + 0.01f, 0.01f), 0.028f, 0);

             // Pointsssss.transform.GetChild(i).gameObject.transform.localPosition = Vector3.Lerp(new Vector3(Pointsssss.transform.GetChild(i).gameObject.transform.localPosition.x, 0.028f, 0), Pointsssss.transform.GetChild(i).gameObject.transform.localPosition.x - 1 * 0.6f, 0.028f, 0), 0.01f));
        }        

        //iTween.MoveTo(Pointsssss.gameObject, iTween.Hash("x", -IncrVal * 0.6f, "y", 0, "z", 0, "delay", 0f, "time", 0.5f, "easetype", iTween.EaseType.linear, "islocal", true));
        //IncrVal += RangeVal;

        RangeVal += curretnVal;

        RangePanel.transform.GetChild(0).GetComponent<Text>().text = RangeVal.ToString();

        RangePanel.transform.GetChild(2).GetComponent<Text>().text = (RangeVal + 1).ToString();

        iTween.Stop(Pointsssss.transform.GetChild(0).gameObject);

        //for (int i = 0; i < Pointsssss.transform.childCount; i++)
        //{
        //    iTween.MoveTo(Pointsssss.transform.GetChild(i).gameObject, iTween.Hash("x", Pointsssss.transform.GetChild(i).gameObject.transform.localPosition.x - 1 * 0.6f, "y", 0.028f, "z", 0, "delay", 0f, "time", 0.5f, "easetype", iTween.EaseType.linear, "islocal", true));
        //}
    }

    public void CreatTrack() {

        Pointsssss.transform.localPosition = new Vector3(0f, 0, 0);

        for (int j = 1; j < Pointsssss.transform.childCount; j++)
        {
            Destroy(Pointsssss.transform.GetChild(j).gameObject);
        }       

        for (int i = -15; i < 15; i++)
        {
            GameObject obj = Instantiate(Pointsssss.transform.GetChild(0).gameObject);

            //obj.transform.parent = Pointsssss.transform;

            obj.transform.SetParent(Pointsssss.transform, false);

            obj.transform.localPosition = new Vector3(i * 0.6f, 0.028f, 0);

            obj.SetActive(true);
            GameObject obj2 = Instantiate(Labels[0]);
            obj2.SetActive(true);
            obj2.transform.parent = obj.transform;
            obj2.transform.localPosition = new Vector3(0, 0, -0.037f);
            obj2.transform.localPosition = new Vector3(0, 0, 0);
            obj2.transform.localScale = new Vector3(0.2222222f, 0.2222222f, 0.2222222f);
            obj2.transform.GetChild(0).gameObject.GetComponentInChildren<TextMeshPro>().text = (RangeVal+i).ToString();
            //obj2.transform.GetChild(1).gameObject.GetComponentInChildren<TextMeshPro>().text = i.ToString();
        }

        Pointsssss.transform.GetChild(0).gameObject.SetActive(false);

        Pointsssss.transform.localPosition = new Vector3(-0.3f, 0, 0);
    }


    public void ChangeDistance(float val,Text txt) {        

        txt.text = val.ToString("F2") + " m";       
    }

    public void ShowFraction(GameObject obj) {

        if (OptionUnlockedTill < 1)
        {
            if (TopicToCheck == 4)
            {
                WrongOptionSelected();
                WronOptionPanel.GetComponentInChildren<Text>().text = "Wrong parameter choosen";
            }
            else {

                OffFractions();

                obj.transform.GetChild(1).gameObject.SetActive(true);

                iTween.ScaleTo(obj.transform.GetChild(1).gameObject, iTween.Hash("x", 0.002f, "y", 0.002f, "z", 0.002f, "delay", 0.002f, "time", 0.3f, "easetype", iTween.EaseType.easeInSine));

                SubmitFun();
            }
        }
        else
        {
            OffFractions();
            
            obj.transform.GetChild(1).gameObject.SetActive(true);

            iTween.ScaleTo(obj.transform.GetChild(1).gameObject, iTween.Hash("x", 0.002f, "y", 0.002f, "z", 0.002f, "delay", 0.002f, "time", 0.3f, "easetype", iTween.EaseType.easeInSine));

            SubmitFun();
        }              
    }
    
    public void OffFractions() {

        for (int j = 0; j < Screws.transform.childCount; j++)
        {
            Screws.transform.GetChild(j).transform.GetChild(1).gameObject.transform.localScale = new Vector3(0f, 0f, 0f);
            Screws.transform.GetChild(j).transform.GetChild(1).gameObject.SetActive(false);
        }
    }
           
    public void SubmitFun() {

        bool ScrewChk = false;

        int i = 0;

        while (i < Screws.transform.childCount)
        {
            if (Screws.transform.GetChild(i).transform.GetChild(1).gameObject.activeSelf)
            {
                ScrewChk = true;
                break;
            }
            i++;
        }

        if (!ScrewChk)
        {
            Invoke("WrongAnswer", 1);
            WronOptionPanel.GetComponentInChildren<Text>().text = "Tap on any fraction";
            WronOptionPanel.GetComponentInChildren<Text>().color = new Color(0.9921f, 0.4431f, 0.08621f, 1);
            //print("Click on Screw");
            return;
        }

        if (!imp)
        {
            if (RangeVal != 0)
            {
                Invoke("WrongAnswer", 1);
                WronOptionPanel.GetComponentInChildren<Text>().text = "Wrong range selected";
                WronOptionPanel.GetComponentInChildren<Text>().color = new Color(0.9921f, 0.4431f, 0.08621f, 1);
                return;
            }            

            if (Screws.transform.childCount > (QuestionN - 1))
            {
                if (QuestionD == pointCount && Screws.transform.GetChild(QuestionN - 1).transform.GetChild(1).gameObject.activeSelf)
                {
                    //print("Correct");
                    for (int j = 0; j < Screws.transform.childCount; j++)
                    {
                        if (!Screws.transform.GetChild(j).transform.GetChild(1).gameObject.activeSelf)
                        {
                            iTween.ScaleTo(Screws.transform.GetChild(j).transform.GetChild(1).gameObject, iTween.Hash("x", 0.0012f, "y", 0.0012f, "z", 0.0012f, "delay", 0.31f, "time", 0.4f, "easetype", iTween.EaseType.easeInSine));

                            //Screws.transform.GetChild(j).transform.GetChild(1).gameObject.transform.localScale = new Vector3(0.0012f, 0.0012f, 0.0012f);
                            Screws.transform.GetChild(j).transform.GetChild(1).gameObject.SetActive(true);
                        }
                    }

                    if (OptionUnlockedTill < 1)
                    {

                        if (TopicToCheck == 2)
                        {
                            AddXpEffect();
                        }
                    }
                    Invoke("FinalCorrectAnswer", 2);


                }
                else
                {
                    Invoke("WrongAnswer", 1);
                    WronOptionPanel.GetComponentInChildren<Text>().text = "Wrong number of fractions selected";
                    WronOptionPanel.GetComponentInChildren<Text>().color = new Color(0.9921f, 0.4431f, 0.08621f, 1);
                    //print("Wrong");
                }
                // Screws.transform.GetChild(QuestionN - 1).transform.GetChild(1).gameObject.SetActive(true);
            }
            else
            {
                Invoke("WrongAnswer", 1);
                WronOptionPanel.GetComponentInChildren<Text>().text = "Wrong number of fractions selected";
                WronOptionPanel.GetComponentInChildren<Text>().color = new Color(0.9921f, 0.4431f, 0.08621f, 1);
                //print("Wrong option");
            }
        }
        else {
         
            if (RangeVal != mixedInt)
            {
                Invoke("WrongAnswer", 1);
                WronOptionPanel.GetComponentInChildren<Text>().text = "Wrong range selected";
                WronOptionPanel.GetComponentInChildren<Text>().color = new Color(0.9921f, 0.4431f, 0.08621f, 1);
                return;
            }

            if (Screws.transform.childCount > (ImN - 1))
            {
                if (pointCount == ImD && Screws.transform.GetChild(ImN - 1).transform.GetChild(1).gameObject.activeSelf)
                {
                    for (int j = 0; j < Screws.transform.childCount; j++)
                    {
                        if (!Screws.transform.GetChild(j).transform.GetChild(1).gameObject.activeSelf)
                        {
                            iTween.ScaleTo(Screws.transform.GetChild(j).transform.GetChild(1).gameObject, iTween.Hash("x", 0.0012f, "y", 0.0012f, "z", 0.0012f, "delay", 0.31f, "time", 0.4f, "easetype", iTween.EaseType.easeInSine));

                            //Screws.transform.GetChild(j).transform.GetChild(1).gameObject.transform.localScale = new Vector3(0.0012f, 0.0012f, 0.0012f);
                            Screws.transform.GetChild(j).transform.GetChild(1).gameObject.SetActive(true);
                        }
                    }


                    Invoke("FinalCorrectAnswer", 2);
                }
                else
                {
                    Invoke("WrongAnswer", 1);
                    WronOptionPanel.GetComponentInChildren<Text>().text = "Wrong number of fractions selected";
                    WronOptionPanel.GetComponentInChildren<Text>().color = new Color(0.9921f, 0.4431f, 0.08621f, 1);
                }                
            }
            else
            {
                Invoke("WrongAnswer", 1);
                WronOptionPanel.GetComponentInChildren<Text>().text = "Wrong number of fractions selected";
                WronOptionPanel.GetComponentInChildren<Text>().color = new Color(0.9921f, 0.4431f, 0.08621f, 1);
            }
        }
    }
    
    public void ConvertToMixedFraction() {

        if (OptionUnlockedTill < 1)
        {
            if (TopicToCheck == 4)
            {
                AddXpEffect();
            }
        }

        QuestionPanel.transform.localScale = new Vector3(0, 0, 0);
        iTween.ScaleTo(QuestionPanel.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.01f, "time", 0.3f, "easetype", iTween.EaseType.easeInSine));
        QuestionText.text = "Plot the number " + "" + QuestionN + "/" + QuestionD + "  = " + Fraction2MixedNumbers("" + QuestionN + "/" + QuestionD + "");
    }

    public void generatePoints(Vector3 from, Vector3 to, Vector3[] result, int chunkAmount)
    {
        //divider must be between 0 and 1
        float divider = 1f / (chunkAmount);
       // float linear = 0f;

        if (chunkAmount == 0)
        {
            Debug.LogError("chunkAmount Distance must be > 0 instead of " + chunkAmount);
            return;
        }

        if (chunkAmount == 1)
        {
            result[0] = Vector3.Lerp(from, to, 0.5f); //Return half/middle point
            return;
        }

        for (int i = 0; i < chunkAmount; i++)
        {
            //if (i == 0){ linear = divider / 2;}else{linear += divider;}
            result[i] = Vector3.Lerp(from, to, divider * i);
        }
    }
            
    public string Fraction2MixedNumbers(string Fraction)
    {
        int numerator;
        int denominator;

        string[] fractionParts = Fraction.Split('/');
        numerator = int.Parse(fractionParts[0]);
        denominator = int.Parse(fractionParts[1]);

        int wholes = numerator / denominator;
        int rest = numerator % denominator;

        string result = string.Empty;

        if (wholes > 0)
            result = wholes.ToString();
        

        if (rest > 0)
            result += " (" + rest + "/" + denominator+")";
        
    
        mixedInt = wholes;
        ImN = rest;
        ImD = denominator;


        return result;
    }

    public void ChangeFractionType(bool prs) {

      

        if (prs)
        {

            if (OptionUnlockedTill < 1)
            {
                //if (TopicToCheck < 3)
                //{
                //    Option1.gameObject.transform.GetChild(0).gameObject.SetActive(true);
                //}
                //if (TopicToCheck >= 3)
                //{
                //    Option2.gameObject.transform.GetChild(0).gameObject.SetActive(true);
                //}
            }
            else
            {
                Option1.gameObject.transform.GetChild(0).gameObject.SetActive(false);
                Option2.gameObject.transform.GetChild(0).gameObject.SetActive(false);

                Option1.gameObject.transform.GetChild(0).gameObject.SetActive(true);
                imp = false;
                ConvertBtn.gameObject.SetActive(false);
                GenerateRandomFraction();
            }
        }
        else
        {
            if (OptionUnlockedTill < 1)
            {

                Option1.gameObject.transform.GetChild(0).gameObject.SetActive(false);
                Option2.gameObject.transform.GetChild(0).gameObject.SetActive(false);

                if (TopicToCheck == 3)
                {
                    AddXpEffect();
                    Option2.gameObject.transform.GetChild(0).gameObject.SetActive(true);
                    imp = true;
                    GenerateRandomFraction();
                    ConvertBtn.gameObject.SetActive(true);
                }
                else
                {
                    Option1.gameObject.transform.GetChild(0).gameObject.SetActive(true);
                    imp = false;
                    WrongOptionSelected();
                    WronOptionPanel.GetComponentInChildren<Text>().text = "Wrong parameter choosen";
                    WronOptionPanel.GetComponentInChildren<Text>().color = new Color(0.9921f, 0.4431f, 0.08621f, 1);
                }
            }
            else
            {

                Option1.gameObject.transform.GetChild(0).gameObject.SetActive(false);
                Option2.gameObject.transform.GetChild(0).gameObject.SetActive(false);

                Option2.gameObject.transform.GetChild(0).gameObject.SetActive(true);
                imp = true;
                GenerateRandomFraction();
                ConvertBtn.gameObject.SetActive(true);
            }
        }
    }

    public void WrongAnswer() {

        WrongOptionSelected();
    }

    public void FinalCorrectAnswer()
    {

        WrongOptionSelected();
        WronOptionPanel.GetComponentInChildren<Text>().text = "Well done!";
        WronOptionPanel.GetComponentInChildren<Text>().color = new Color(0.7568f, 0.8588f, 0.075f, 1);

        Invoke("ResetAll", 1);
    }

    public void ResetAll()
    {
        pointCount = 1;
        m_valtxt.text = pointCount.ToString();

        for (int j = 0; j < Screws.transform.childCount; j++)
        {
            Destroy(Screws.transform.GetChild(j).gameObject);
        }
        GenerateRandomFraction();
    }

}

public class LinesRendererOffset : MonoBehaviour
{    
    Renderer rend;
    public float offset, scrollSpeed = 4f, dir = 1;
    void Start()
    {
        //dir = 1;
        rend = GetComponentInChildren<Renderer>();
        offset = 1f;
        rend.materials[0].SetTextureOffset("_MainTex", new Vector2(0, 1f));
    }

    void Update()
    {
        offset += Time.deltaTime * scrollSpeed;
        rend.materials[0].SetTextureOffset("_MainTex", new Vector2(0, -dir*offset));
        rend.material.SetTextureScale("_MainTex", new Vector2(1, 75*transform.localScale.x));
    }
}
