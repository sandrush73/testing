﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;

public class forceandpressure : MonoBehaviour
{

    public TextAsset CSVFile;
    public GameObject Zekki,Graph_Origin,Graph_Point,Graph_Line,GraphObj,Zekki_LH,Zekki_RH,PressureEffect;
    public Material TransparentMaterial;
    public Text Mass_Val_Txt,Force_Val_Txt,Mult_Val_Txt,Pressure_Val_Txt;
    public float Mass_Value,SliderValCheck, Pressure_Val, To_Y;
    public SkinnedMeshRenderer Trampoline_Blend;
    public Vector3 Zekki_Init_Pos;
    
    public int OptionUnlockedTill, CurrentTopicSelected, TopicToCheck;
    public float Sub_XP_Earned, Total_XP;
    public Dropdown ToDoList_DD;
    public GameObject InstructionPanel_DD, ObjectivePanel, InstructionPanel, InfoPanel, GlassPanel, Canvas_BG, SidePannel,Togg_Bubble;
    public Button Info_Close_Btn, Info_Cntue_Btn, Info_Open_Btn;
    public List<Dictionary<string, object>> CSV_data;
    public List<String> Sub_Top_ToDoList, Sub_Top_Desc, Sub_Top_XP, LearningGoals;
    public Text XP_ToShow, XP_Effect, Main_XP_ToShow, Learninggoal_Txt, Instruction_Txt, Assumption_Txt, Heading_Txt;
    public bool IsTopicUnlocked;
    public GameObject UIcanvas;
    public Button SidePannelOpen, LeaveZekki_But,ShowGraph_But;
    public bool IsClicked,IsAR,IsUIZooming;
    public orbit CamOrbit;
    public Slider ZekkiMassSlider;

    public AssetBundle assetBundle;

    public AROrbitControls ArOrbit;

    


    // Start is called before the first frame update
    void Start()
    {
        //OptionUnlockedTill = 1;

        LoadFromAsssetBundle();

        ReadingDataFromCSVFile();
        ReadingXps();
        FindingGameObjects();

        AssignClickEventsToObjects();

        
    }

    public void FindingGameObjects()
    {
        Zekki = GameObject.Find("Zekki");
        Zekki_LH = GameObject.Find("LeftHand");
        Zekki_RH = GameObject.Find("RightHand");
        PressureEffect = GameObject.Find("Pressure_Effect");

        LeaveZekki_But =GameObject.Find("LeaveZekki").GetComponent<Button>();
        ShowGraph_But = GameObject.Find("ShowGraph").GetComponent<Button>();
        ShowGraph_But.gameObject.SetActive(false);
        Mass_Val_Txt = GameObject.Find("Mass_Value_Text").GetComponent<Text>();
        Force_Val_Txt = GameObject.Find("Force_To_Disp").GetComponent<Text>();
        Mult_Val_Txt = GameObject.Find("Multi_Value").GetComponent<Text>();
        Pressure_Val_Txt = GameObject.Find("Pressure_Val").GetComponent<Text>();
        Graph_Origin = GameObject.Find("graphOrigin");
        Graph_Point = GameObject.Find("Graphpoint");
        Graph_Line = GameObject.Find("GraphLine");
        GraphObj = GameObject.Find("GraphMain");
        GraphObj.SetActive(false);



        Trampoline_Blend = GameObject.Find("Trampoline_Blend").GetComponentInChildren<SkinnedMeshRenderer>();
        Zekki_Init_Pos = Zekki.transform.localPosition;


        Canvas_BG = GameObject.Find("Canvas_BG");

        if (!IsAR)
        {
            Canvas_BG.transform.parent = Camera.main.transform;
            Canvas_BG.transform.GetComponentInChildren<RawImage>().enabled = true;
            Canvas_BG.GetComponent<Canvas>().worldCamera = Camera.main;
        }

        else
        {
            Canvas_BG.SetActive(false);
        }


        SidePannelOpen = GameObject.Find("PannelOpenButton").GetComponent<Button>();
        SidePannelOpen.gameObject.SetActive(false);
        SidePannel = GameObject.Find("SidePanel");

        InstructionPanel_DD = GameObject.Find("DD_Instruction");
        InstructionPanel = GameObject.Find("InstructionPanel");
        ToDoList_DD = GameObject.Find("ToDoList_Dropdown").GetComponent<Dropdown>();
        XP_ToShow = GameObject.Find("Gained_XP_Value").GetComponent<Text>();
        XP_Effect = GameObject.Find("Effect_Text").GetComponent<Text>();
        Main_XP_ToShow = GameObject.Find("Main_XP_Value").GetComponent<Text>();
        ObjectivePanel = GameObject.Find("ObjectivePanel");
        InstructionPanel.transform.localScale = Vector3.zero;
        InfoPanel = GameObject.Find("InformationPanel");
        Info_Close_Btn = GameObject.Find("InfoCloseBtn").GetComponent<Button>();
        Info_Cntue_Btn = GameObject.Find("InfoContinueBtn").GetComponent<Button>();
        Info_Open_Btn = GameObject.Find("InfoButton").GetComponent<Button>();
        Info_Close_Btn.gameObject.SetActive(false);
        GlassPanel = GameObject.Find("GlassEffect");
        ZekkiMassSlider = GameObject.Find("MassSlider").GetComponent<Slider>();
        Learninggoal_Txt = GameObject.Find("LGListText").GetComponent<Text>();
        Instruction_Txt = GameObject.Find("IListText").GetComponent<Text>();
        Assumption_Txt = GameObject.Find("AListText").GetComponent<Text>();
        Heading_Txt = GameObject.Find("InfoHeadingText").GetComponent<Text>();
        UIcanvas = GameObject.Find("Canvas_FandP");
        Togg_Bubble = GameObject.Find("ToggBubble");
        
        if (OptionUnlockedTill == 0)
        {
            /*ToDoList_DD.GetComponent<RectTransform>().anchoredPosition = new Vector3(150f, -150f, 0);
            XP_ToShow.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(150f, -150f, 0);
            SidePannel.GetComponent<RectTransform>().anchoredPosition = new Vector3(-600f, 0f, 0);*/
            ObjectivePanel.transform.localScale = new Vector3(1, 1, 1);
            IsTopicUnlocked = false;
        }
        else
        {
            ToDoList_DD.GetComponent<RectTransform>().anchoredPosition = new Vector3(-250f, -150f, 0);
            XP_ToShow.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(-250f, -150f, 0);
            ObjectivePanel.transform.localScale = new Vector3(1, 0, 1);
            IsTopicUnlocked = true;
        }
        Main_XP_ToShow.text = totXp.ToString();

        XP_ToShow.text = "0/" + Total_XP;



        for (int i = 0; i < Sub_Top_ToDoList.Count; i++)
        {

            ToDoList_DD.options.Add(new Dropdown.OptionData() { text = Sub_Top_ToDoList[i] });


            //List<float> TotalXP = Sub_Top_XP.Select(s => float.Parse(s)).ToList();

            //Total_XP += TotalXP[i];

            //XP_ToShow.text = "0/" + Total_XP;


            ObjectivePanel.transform.Find("ObjectiveText").GetComponent<Text>().text = Sub_Top_ToDoList[TopicToCheck];




        }


        GlassPanel.transform.localScale = Vector3.zero;
        InfoPanel.transform.localScale = Vector3.zero;
        Invoke("OpenInfoPanel", 1f);

        Heading_Txt.text = LearningGoals[0];
        Learninggoal_Txt.text = LearningGoals[1];
        //Instruction_Txt.text = LearningGoals[2];
        //Assumption_Txt.text = LearningGoals[3];

        Mass_Value = 0.75f + (ZekkiMassSlider.value / 10);

        OpenSidePannel();
    }



    public void AssignClickEventsToObjects()
    {
         
        

        SidePannelOpen.onClick.AddListener(OpenSidePannel);
        //TopSidePannelOpen.onClick.AddListener(OpenTopSidePannel);

        AddListenerToEvents(EventTriggerType.PointerUp, EnableOrbit, ZekkiMassSlider.gameObject);
        AddListenerToEvents(EventTriggerType.PointerDown, DisableOrbit, ZekkiMassSlider.gameObject);
        ZekkiMassSlider.onValueChanged.AddListener(delegate { ChangeMassSlider(ZekkiMassSlider.value); });

        AddListenerToEvents(EventTriggerType.PointerUp, SliderConditionCheck, ZekkiMassSlider.gameObject);
        AddListenerToEvents(EventTriggerType.PointerDown, SliderCondMouseDown, ZekkiMassSlider.gameObject);
        //ScrollBarViewPort.gameObject.GetComponent<EventTrigger>().triggers.Add(PointerUp);

        /*for (int i = 0; i < SelectionButtons.Length; i++)
        {
            int j = i;
            SelectionButtons[i].onClick.AddListener(() => PropertyToDisplay(j));
            // print(SelectionButtons[i].name);
            old_color = SelectionButtons[0].transform.GetChild(1).GetComponent<Text>().color;
        }*/
        ToDoList_DD.GetComponent<EventTrigger>().triggers.Clear();

        AddListenerToEvents(EventTriggerType.PointerClick, ExploringToDOList_DD, ToDoList_DD.gameObject);
        AddListenerToEvents(EventTriggerType.Select, ToDoList_DDCancel, ToDoList_DD.gameObject);

        
        Info_Close_Btn.onClick.AddListener(() => CloseInfoPanel(Info_Close_Btn));
        Info_Cntue_Btn.onClick.AddListener(() => CloseInfoPanel(Info_Cntue_Btn));
        Info_Open_Btn.onClick.AddListener(OpenInfoPanel);

        
        LeaveZekki_But.onClick.AddListener(LeaveZekki);
        ShowGraph_But.onClick.AddListener(ShowGraph);


    }


    public void AddListenerToEvents(EventTriggerType eventType, Action MethodToCall, GameObject TriggerObjToAdd)
    {

        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall());
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }

    public void AddListenerToEvents(EventTriggerType eventType, Action<float> MethodToCall, GameObject TriggerObjToAdd, float p)
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall(p));
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }

    public void AddListenerToEvents(EventTriggerType eventType, Action<GameObject> MethodToCall, GameObject TriggerObjToAdd, GameObject p)
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall(p));
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }

    public void ReadingDataFromCSVFile()
    {
        Sub_Top_Desc = new List<string>();
        Sub_Top_ToDoList = new List<string>();
        Sub_Top_XP = new List<string>();
        LearningGoals = new List<string>();

        CSV_data = CSVReader.Read(CSVFile);


        for (var i = 0; i < CSV_data.Count; i++)
        {
            //print("Topics " + CSV_data[i]["Learninggoals"] + ".......... " +"ToDo " + CSV_data[i]["Topic_TodoList"]);




            if (CSV_data[i]["Topic_ToDoList"].ToString() != "" && CSV_data[i]["Topic_ToDoList"].ToString() != null)
            {
                Sub_Top_ToDoList.Add(CSV_data[i]["Topic_ToDoList"].ToString());

            }



            //if (CSV_data[i]["Top_XP"].ToString() != "" && CSV_data[i]["Top_XP"].ToString() != null)
            //{
            //    Sub_Top_XP.Add(CSV_data[i]["Top_XP"].ToString());

            //}

            if (CSV_data[i]["Learninggoals"].ToString() != "" && CSV_data[i]["Learninggoals"].ToString() != null)
            {
                LearningGoals.Add(CSV_data[i]["Learninggoals"].ToString());

            }

        }






    }

    public int totXp, loclXp, PerTopic_XP;

    public void ReadingXps()
    {

        //PlayerPrefs.SetInt("TotalXP", 34);
        //PlayerPrefs.SetInt("MaxEarnings", 39);

        totXp = PlayerPrefs.GetInt("TotalXP");
        loclXp = PlayerPrefs.GetInt("MaxEarnings");

        TopicToCheck = 0;

        if (loclXp != 0)
        {
            OptionUnlockedTill = 0;

        }
        else
        {
            OptionUnlockedTill = 1;

        }





        int t = 0;
        int p = 0;

        for (int k = 0; k < Sub_Top_ToDoList.Count; k++)
        {

            p = loclXp / Sub_Top_ToDoList.Count;
            Sub_Top_XP.Add(p.ToString());
            t = t + p;
        }

        if (t != loclXp)
        {
            Sub_Top_XP[Sub_Top_ToDoList.Count - 1] = (p + (loclXp - t)).ToString();
        }

        Total_XP = loclXp;
        

    }



    public float result;

    public void GetResult()
    {
        result = ((float)(TopicToCheck - 0) / (float)(Sub_Top_ToDoList.Count)) * 100;
        Camera.main.SendMessage("UnloadActually", result);
    }

    public void LoadFromAsssetBundle()
    {
        //if (Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.Android)
        //{
        assetBundle = AssetBundle.LoadFromFile(PlayerPrefs.GetString("AssetBundleUrl"));

        CSVFile = assetBundle.LoadAsset(PlayerPrefs.GetString("LibraryName") + "CSV", typeof(TextAsset)) as TextAsset;

        TransparentMaterial = assetBundle.LoadAsset("TransparentMaterial", typeof(Material)) as Material;
        TransparentMaterial.shader = Shader.Find("Standard");




        //}
        //else
        //{

        //string CSVName = transform.parent.gameObject.name;



        //    if (CSVName.Contains("Clone"))
        //    {



        //        CSVName = CSVName.Substring(0, CSVName.Length - 7);
        //    }



        //    print("casvname........." + CSVName);



        //    CSVFile = Resources.Load(CSVName+"CSV") as TextAsset;
        //}







        Camera.main.gameObject.AddComponent<PhysicsRaycaster>();



        Camera.main.backgroundColor = Color.black;



        GameObject TargetForCamera = GameObject.Find("Target");



        if (TargetForCamera != null)
        {
            TargetForCamera.AddComponent<orbit>();
            CamOrbit = TargetForCamera.GetComponent<orbit>();


            CamOrbit.zoomMax = 3f;
            CamOrbit.zoomMin = 2.1f;
            CamOrbit.zoomStart = 2.6f;
            CamOrbit.distance = CamOrbit.zoomStart;
            CamOrbit.transform.position = new Vector3(0, 0.5f, 0);

            CamOrbit.maxRotUp = 45;
            CamOrbit.minRotUp = 7;

            CamOrbit.maxSideRot = 60;
            CamOrbit.minSideRot = 60;


            IsAR = false;
        }
        else
        {
            this.transform.parent.gameObject.AddComponent<AROrbitControls>();
            ArOrbit = this.transform.parent.gameObject.GetComponent<AROrbitControls>();
            IsAR = true;
        }



    }


    public void OnElementBeginDrag()
    {
        IsClicked = true;
    }

    public void OnElementEndDrag()
    {
        //print("drag ended");
        IsClicked = false;
    }

    public void EnableOrbit()
    {

        if (!IsAR)
        {
            CamOrbit.DisableInteration = false;
            //print("EnableOrbit");
        }
        else
        {
            ArOrbit.DisableInteration = false;
        }
    }

    public void DisableOrbit()
    {

        if (!IsAR)
        {
            CamOrbit.DisableInteration = true;
            //print("DisableOrbit");
        }
        else
        {
            ArOrbit.DisableInteration = true;
        }
    }

    public void OpenInfoPanel()
    {
        UIZoomIn();
        iTween.ScaleTo(InfoPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 0.5f, "easetype", iTween.EaseType.spring));
        iTween.ScaleTo(GlassPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 0.01f, "easetype", iTween.EaseType.spring));

    }

    public void CloseInfoPanel(Button CloseBtn)
    {
        UIZoomOut();
        iTween.ScaleTo(InfoPanel, iTween.Hash("x", 0, "y", 0, "z", 0, "time", 0.2f, "easetype", iTween.EaseType.linear));
        iTween.ScaleTo(GlassPanel, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.2f, "time", 0.01f, "easetype", iTween.EaseType.linear));
        if (CloseBtn == Info_Cntue_Btn)
        {
            if (!IsAR)
                CamOrbit.DisableInteration = false;
            /*if (ActiveBtnCnt == 0)
            {
                for (int i = 0; i < Elements.Length; i++)
                {

                    Vector3 RandomPos = new Vector3(Elements[i].transform.localPosition.x, Elements[i].transform.localPosition.y, UnityEngine.Random.Range(-5f, 0f));
                    Vector3 CurrPos = Elements[i].transform.localPosition;

                    Elements[i].transform.localPosition = RandomPos;

                    iTween.MoveTo(Elements[i], iTween.Hash("position", CurrPos, "delay", 0.7f, "islocal", true, "time", UnityEngine.Random.Range(0.1f, 0.2f), "speed", 2.5f, "easetype", iTween.EaseType.easeInOutSine));


                }
            }*/
            Info_Close_Btn.gameObject.SetActive(true);
            Info_Cntue_Btn.gameObject.SetActive(false);
        }
        IsClicked = false;
    }

    public void CloseInstructionPanel()
    {
        UIcanvas.GetComponent<GraphicRaycaster>().enabled = true;
        Camera.main.GetComponent<PhysicsRaycaster>().enabled = true;
        //iTween.ScaleTo(GlassPanel, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 1f, "time", 0.01f, "easetype", iTween.EaseType.linear));
        iTween.ScaleTo(InstructionPanel, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.5f, "time", 0.3f, "easetype", iTween.EaseType.spring));
        if (!IsTopicUnlocked)
            iTween.ScaleTo(ObjectivePanel, iTween.Hash("y", 1, "time", 0.3f, "delay", 0.5f, "easetype", iTween.EaseType.easeInOutSine));

    }

    public void ToDoList_DDCancel()
    {
        if (ToDoList_DD.transform.Find("Dropdown List") != null)//&& Obj.transform.localScale.y!=1)
        {
            //print("cancelled........");
            //print("list.....cancelled........" + ToDoList_DD.transform.Find("Dropdown List").gameObject);
            ToDoList_DD.transform.Find("Arrow").transform.eulerAngles = new Vector3(0, 0, 0);
            iTween.ScaleTo(ToDoList_DD.transform.Find("Dropdown List").gameObject, iTween.Hash("y", 0, "time", 0.5f, "easetype", iTween.EaseType.spring));
            iTween.ScaleTo(ObjectivePanel, iTween.Hash("y", 1, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine));
            if (SidePannelOpen.transform.eulerAngles.z == 180)
            {
                OpenSidePannel();
            }
        }
    }

    public void ExploringToDOList_DD()
    {
        //print("ShowDD.....");
        //ToDoList_DD.transform.Find("Topic_Objective").GetComponent<Text>().text = "";
        if (SidePannelOpen.transform.eulerAngles.z == 0)
        {
            OpenSidePannel();
        }

        ToDoList_DD.transform.Find("Arrow").transform.eulerAngles = new Vector3(0, 0, 180);
        GameObject DDList = ToDoList_DD.transform.Find("Dropdown List").gameObject;
        DDList.transform.GetComponent<RectTransform>().sizeDelta = new Vector2(0, 250);
        DDList.transform.localScale = new Vector3(1, 0, 1);
        iTween.ScaleTo(DDList, iTween.Hash("y", 1, "time", 0.5f, "easetype", iTween.EaseType.spring));
        //iTween.ScaleTo(ToDoList_DD.transform.Find("Topic_Objective").gameObject, iTween.Hash("y", 0, "time", 0.5f, "easetype", iTween.EaseType.spring));
        if (!IsTopicUnlocked)
            iTween.ScaleTo(ObjectivePanel, iTween.Hash("y", 0, "time", 0.5f, "delay", 0.1f, "easetype", iTween.EaseType.easeInOutSine));

        string TestStr = "";
        int CntToChangePos = 0;
        for (int i = 0; i < ToDoList_DD.options.Count; i++)
        {
            GameObject Child = GameObject.Find("Item " + i + ": " + ToDoList_DD.options[i].text);
            //Child.transform.parent.GetComponent<RectTransform>().sizeDelta = new Vector2(0,250);

            if (TestStr == ToDoList_DD.options[i].text)
            {
                Child.SetActive(false);
                Child.transform.localPosition = Child.transform.localPosition + new Vector3(0, 50 * CntToChangePos, 0);
                CntToChangePos++;
            }
            else
            {
                Child.transform.Find("XP_Text").GetComponent<Text>().text = "+" + Sub_Top_XP[i] + "XP";
                Child.transform.localPosition = Child.transform.localPosition + new Vector3(0, 50 * CntToChangePos, 0);

                if (i < TopicToCheck)
                {
                    Child.transform.Find("Item Checkmark").GetComponent<Image>().enabled = true;
                    //Child.transform.Find("XP_Text").GetComponent<Text>().color = Color.green / 2;
                    //Child.transform.Find("Item Checkmark").GetComponent<Image>().color = Color.green / 2;
                    Child.transform.Find("Item Background").GetComponent<Image>().enabled = false;
                }
                else if (i == TopicToCheck)
                {
                    Child.transform.Find("Item Background").GetComponent<Image>().enabled = true;// color = ReturnColorFromHex("#a01c65");
                                                                                                 //Child.transform.Find("Item Label").GetComponent<Text>().color = Color.white;
                }
            }
            TestStr = ToDoList_DD.options[i].text;
        }
    }


    public void OpenSidePannel()
    {
        if (SidePannelOpen.transform.eulerAngles.z == 180)
        {
            //iTween.MoveAdd(SidePannel, iTween.Hash("x", -400f, "time",0.5f,"islocal",true, "easetype", iTween.EaseType.linear));
            //iTween.MoveAdd(SidePannelOpen.gameObject, iTween.Hash("x", -185f, "islocal", true, "time", 0.5f, "easetype", iTween.EaseType.linear));
            //SidePannelOpen.transform.eulerAngles = new Vector3(0,0,180);
            SidePannel.GetComponent<Animator>().Play("PannelForward");
        }
        else
        {
            //iTween.MoveAdd(SidePannel, iTween.Hash("x", 400f, "time", 0.5f, "islocal", true, "easetype", iTween.EaseType.linear));
            //iTween.MoveAdd(SidePannelOpen.gameObject, iTween.Hash("x", -185f, "islocal", true, "time", 0.5f, "easetype", iTween.EaseType.linear));
            //SidePannelOpen.transform.eulerAngles = new Vector3(0, 0, 0);
            SidePannel.GetComponent<Animator>().Play("PannelBackward");
        }
    }


    public void UIReset()
    {
        ObjectivePanel.GetComponent<RectTransform>().anchoredPosition = new Vector3(0f, 5f, 0);

        //ZekkiMassSlider.transform.GetComponent<RectTransform>().anchoredPosition = new Vector3(137.5f, -53f, 0);
        Info_Open_Btn.transform.GetComponent<RectTransform>().anchoredPosition = new Vector3(-37f, -150f, 0);


        SidePannel.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(0f, 0f, 0);

        //print("side......" + SidePannel.transform.position+"....local....."+ SidePannel.transform.localPosition);
        if (!IsTopicUnlocked)
        {
            ToDoList_DD.transform.GetComponent<RectTransform>().anchoredPosition = new Vector3(135f, -150f, 0);
            XP_ToShow.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(135f, -150f, 0);

        }
        else
        {
            ToDoList_DD.transform.GetComponent<RectTransform>().anchoredPosition = new Vector3(-250f, -150f, 0);
            XP_ToShow.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(-250f, -150f, 0);

        }

        //Main_XP_ToShow.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(-135f, -60f, 0);

    }

    public void UIZoomOut()
    {
        if (ObjectivePanel.GetComponent<RectTransform>().anchoredPosition.y == 5)
            return;

        UIReset();

        iTween.MoveFrom(Info_Open_Btn.gameObject, iTween.Hash("x", Info_Open_Btn.transform.localPosition.x + 250, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveFrom(SidePannel.transform.parent.gameObject, iTween.Hash("x", SidePannel.transform.parent.localPosition.x - 1000f, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        //iTween.MoveFrom(ZekkiMassSlider.gameObject, iTween.Hash("x", ZekkiMassSlider.transform.localPosition.x + 250, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveFrom(ObjectivePanel, iTween.Hash("y", ObjectivePanel.transform.localPosition.y - 1000, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));


        iTween.MoveFrom(ToDoList_DD.gameObject, iTween.Hash("x", ToDoList_DD.transform.localPosition.x - 400, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveFrom(XP_ToShow.transform.parent.gameObject, iTween.Hash("x", XP_ToShow.transform.parent.localPosition.x - 400, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));

        //iTween.MoveFrom(Main_XP_ToShow.transform.parent.gameObject, iTween.Hash("x", Main_XP_ToShow.transform.parent.localPosition.x + 550, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
    }



    public void UIZoomIn()
    {
        if (ObjectivePanel.GetComponent<RectTransform>().anchoredPosition.y != 5)
            return;

        UIReset();
        //print("side......" + SidePannel.transform.position);
        iTween.MoveAdd(ObjectivePanel, iTween.Hash("y", ObjectivePanel.transform.localPosition.y - 1000, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveAdd(Info_Open_Btn.gameObject, iTween.Hash("x", Info_Open_Btn.transform.localPosition.x + 250, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveAdd(SidePannel.transform.parent.gameObject, iTween.Hash("x", SidePannel.transform.parent.localPosition.x - 1000f, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));

        //iTween.MoveAdd(ZekkiMassSlider.gameObject, iTween.Hash("x", ZekkiMassSlider.transform.localPosition.x + 250, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));

        iTween.MoveAdd(XP_ToShow.transform.parent.gameObject, iTween.Hash("x", XP_ToShow.transform.parent.localPosition.x - 400, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveAdd(ToDoList_DD.gameObject, iTween.Hash("x", ToDoList_DD.transform.localPosition.x - 400, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));

        //iTween.MoveAdd(Main_XP_ToShow.transform.parent.gameObject, iTween.Hash("x", Main_XP_ToShow.transform.parent.localPosition.x + 550, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
    }

    public float ChangeToLocal_X(float Val)
    {
        float X_ToRet = 0;
        //print("wid........"+ UIcanvas.GetComponent<RectTransform>().rect.width);
        X_ToRet = ((UIcanvas.GetComponent<RectTransform>().rect.x) + Val);

        return X_ToRet;
    }

    public float ChangeToLocal_Y(float Val)
    {
        float Y_ToRet = 0;
        //print("wid........"+ UIcanvas.GetComponent<RectTransform>().rect.width);
        Y_ToRet = ((UIcanvas.GetComponent<RectTransform>().rect.y) + Val);

        return Y_ToRet;
    }


    public void Collect_XP(GameObject TargetPos)
    {
        XP_Effect.transform.position = Camera.main.WorldToScreenPoint(TargetPos.transform.position);

        Camera.main.GetComponent<PhysicsRaycaster>().enabled = false;
        UIcanvas.GetComponent<GraphicRaycaster>().enabled = false;

        XP_Effect.text = "+"+Sub_Top_XP[TopicToCheck] + "XP";

        XP_Effect.transform.localScale = new Vector3(2, 0, 2);

        iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 2, "y", 2, "z", 2, "time", 0.5f, "easetype", iTween.EaseType.easeOutElastic));

        iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", XP_Effect.transform.position.x, "y", XP_Effect.transform.position.y + 150, "z", 0, "delay", 0.3f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

        iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", 150, "y", XP_ToShow.transform.position.y, "z", 0, "delay", 1f, "time", 1f, "easetype", iTween.EaseType.easeInSine, "oncomplete", "Add_To_XP", "oncompletetarget", this.gameObject));

    }

    public void Add_To_XP()
    {

        //print("TopicToCheck......."+ TopicToCheck+"....sub......"+Sub_Topics.Count);
        if (TopicToCheck < (Sub_Top_ToDoList.Count))
        {
            Sub_XP_Earned += float.Parse(Sub_Top_XP[TopicToCheck]);

            XP_Effect.text = "";
            XP_ToShow.text = Sub_XP_Earned + "/" + Total_XP;

            TopicToCheck++;
            if (TopicToCheck <= (Sub_Top_ToDoList.Count - 1))
            {
                ObjectivePanel.transform.Find("ObjectiveText").GetComponent<Text>().text = Sub_Top_ToDoList[TopicToCheck];
                ObjectivePanel.transform.localScale = new Vector3(1, 0, 1);
                iTween.ScaleTo(ObjectivePanel, iTween.Hash("y", 1, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine));
                Camera.main.GetComponent<PhysicsRaycaster>().enabled = true;
                UIcanvas.GetComponent<GraphicRaycaster>().enabled = true;
                //ToDoList_DD.transform.Find("Topic_Objective").GetComponent<Text>().text = "Objective: " + Sub_Top_ToDoList[TopicToCheck];
            }
            else
            {
                XP_Effect.transform.position = (XP_ToShow.transform.position);

                XP_Effect.text = Total_XP + "XP";

                XP_Effect.transform.localScale = new Vector3(1, 0, 1);

                iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 2, "y", 2, "z", 2, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeOutElastic));

                iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", XP_Effect.transform.position.x, "y", XP_Effect.transform.position.y + 50, "z", 0, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

                iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", Main_XP_ToShow.transform.position.x, "y", Main_XP_ToShow.transform.position.y, "z", 0, "delay", 1.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine, "oncomplete", "AddTotalToMainXP", "oncompletetarget", this.gameObject));

                iTween.MoveTo(ToDoList_DD.gameObject, iTween.Hash("x", ChangeToLocal_X(-400), "delay", 3.5f, "time", 0.5f, "islocal", true, "easetype", iTween.EaseType.easeInOutSine));

                iTween.MoveTo(XP_ToShow.transform.parent.gameObject, iTween.Hash("x", ChangeToLocal_X(-400), "delay", 3.5f, "time", 0.5f, "islocal", true, "easetype", iTween.EaseType.easeInOutSine));
                //print("label......" + LabelImage.GetComponent<RectTransform>().anchoredPosition+".......explode...."+ ExplodeSlider.GetComponent<RectTransform>().anchoredPosition);


                //iTween.MoveTo(ValueDisplaySlider.gameObject, iTween.Hash("x", ChangeToLocal_X ( 150), "delay", 0.7f, "time", 0.5f, "islocal", true, "easetype", iTween.EaseType.easeInOutSine));
            }
        }

    }

    public void AddTotalToMainXP()
    {
        if (OptionUnlockedTill < 1)
        {
            OptionUnlockedTill++;

            float MainXp = float.Parse(Main_XP_ToShow.text);
            MainXp += Total_XP;
            Main_XP_ToShow.text = MainXp.ToString();
            //ObjectivePanel.transform.Find("ObjectiveText").GetComponent<Text>().text = "YaY!......Next topic unlocked";
            XP_Effect.text = "";
            IsTopicUnlocked = true;
            iTween.ScaleTo(ObjectivePanel, iTween.Hash("y", 0, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine));
            //IsInteraction = true;

            Camera.main.GetComponent<PhysicsRaycaster>().enabled = true;
            UIcanvas.GetComponent<GraphicRaycaster>().enabled = true;
        }
    }

    public void SliderCondMouseDown()
    {
        if (!IsTopicUnlocked)
        {
            
                SliderValCheck = ZekkiMassSlider.value;
            
           
        }
    }

    public void SliderConditionCheck()
    {
        if (!IsTopicUnlocked)
        {

            if (TopicToCheck == 0)
            {
                if (CheckFor_XP_Condition(TopicToCheck))
                {

                    return;
                }
            }
            else
            {
                if (CheckFor_XP_Condition(TopicToCheck))
                    ZekkiMassSlider.value = SliderValCheck;
            }
        }
    }



    public void ChangeMassSlider(float SliderVal)
    {
        if (!IsTopicUnlocked)
        {
            if (TopicToCheck != 0)
            {
                return;
            }
        }

        Mass_Val_Txt.text = SliderVal.ToString();
        Force_Val_Txt.text = SliderVal.ToString();
        Mult_Val_Txt.text = SliderVal + " * 5";
        

        Mass_Value = 0.75f +(SliderVal/10);

        Zekki.transform.localPosition = Zekki_Init_Pos;

        if (Zekki.GetComponent<iTween>())
        {
            Destroy(Zekki.GetComponent<iTween>());
            LeaveZekki_But.gameObject.SetActive(true);
            ShowGraph_But.gameObject.SetActive(false);
            GraphObj.SetActive(false);
            ShowGraph_But.GetComponentInChildren<Text>().text = "Show graph";
            Zekki.GetComponentInChildren<Animator>().enabled = true;
        }

        
        Graph_Point.transform.localPosition = Graph_Origin.transform.localPosition + new Vector3(-0.06f*SliderVal,0.06f*SliderVal,0) ;
        Graph_Line.transform.localScale =new Vector3(-Graph_Point.transform.localPosition.x+ Graph_Origin.transform.localPosition.x,1,1);
    }
    

    public void LeaveZekki()
    {
        if (!IsTopicUnlocked)
        {
            if (TopicToCheck != 1)
            {
                if (CheckFor_XP_Condition(TopicToCheck))
                    return;
            }
        }
       
        To_Y = ZekkiMassSlider.value * -0.0055f + 0.205f;

        iTween.MoveTo(Zekki, iTween.Hash("y", To_Y, "time", Zekki.transform.localPosition.y/Mass_Value, "islocal", true, "onupdate", "ChangeBlendValue", "onupdatetarget",
            this.gameObject, "easetype", iTween.EaseType.easeOutSine, "oncomplete", "ChangeItweenPos_Zekki", "oncompletetarget",this.gameObject));

        LeaveZekki_But.gameObject.SetActive(false);
        ShowGraph_But.gameObject.SetActive(true);
        Zekki.GetComponentInChildren<Animator>().enabled = false;

        Pressure_Val = ZekkiMassSlider.value / 5;

        Pressure_Val_Txt.text = (Pressure_Val).ToString();

        if (!IsTopicUnlocked)
        {
            if (TopicToCheck == 1)
                CheckFor_XP_Condition(TopicToCheck);
        }

    }


    public void ChangeItweenPos_Zekki()
    {
        iTween.MoveTo(Zekki, iTween.Hash("y", Mass_Value, "time", 1, "islocal", true, "onupdate", "ChangeBlendValue", "onupdatetarget",
            this.gameObject, "easetype", iTween.EaseType.easeOutSine, "looptype", iTween.LoopType.pingPong));

        //float To_Blend = ZekkiMassSlider.value * 1.6f + 83.34f;

        //iTween.ValueTo(Trampoline_Blend.gameObject, iTween.Hash("from", To_Blend, "to", 75, "delay", 0.9f, "time", 0.1f, "onupdate", "ChangeBlendValue", "onupdatetarget",
        //    this.gameObject, "easetype", iTween.EaseType.easeOutSine, "looptype", iTween.LoopType.pingPong));
    }



    public void ChangeBlendValue()//(float BlendVal)
    {

        if (Zekki.transform.localPosition.y <= 0.3f)
        {

            float To_Blend = (Zekki.transform.localPosition.y) * -250f + 137.5f;

            To_Blend = Mathf.Clamp(To_Blend + 5, 75, 100);

            Trampoline_Blend.SetBlendShapeWeight(0, To_Blend);
            if (!Zekki_LH.GetComponent<iTween>() || (Zekki_LH.GetComponent<iTween>() && Zekki_LH.GetComponent<iTween>().time != 0.2f))
            {
                iTween.RotateTo(Zekki_LH, iTween.Hash("z", 0, "time", 0.2f, "islocal", true, "easetype", iTween.EaseType.easeInOutSine));
                iTween.RotateTo(Zekki_RH, iTween.Hash("z", 0, "time", 0.2f, "islocal", true, "easetype", iTween.EaseType.easeInOutSine));

                float ScaleTo =0.0166f*Pressure_Val+0.0166f;

                iTween.ScaleTo(PressureEffect, iTween.Hash("x", ScaleTo, "z", ScaleTo, "time", 0.5f, "islocal", true, "easetype", iTween.EaseType.easeInOutSine));
                iTween.ScaleTo(PressureEffect, iTween.Hash("x", 0f, "z", 0f,"delay",0.5f, "time", 0.5f, "islocal", true, "easetype", iTween.EaseType.easeInOutSine));
            }
            //print("Blenval........" + To_Blend);
        }
        else if (Zekki.transform.localPosition.y >= (Mass_Value - 0.05f))
        {
            if (!Zekki_LH.GetComponent<iTween>() || (Zekki_LH.GetComponent<iTween>() && Zekki_LH.GetComponent<iTween>().time != 1f))
            {
                iTween.RotateTo(Zekki_LH, iTween.Hash("z", -60, "time",1f, "islocal", true, "easetype", iTween.EaseType.easeInOutSine));
                iTween.RotateTo(Zekki_RH, iTween.Hash("z", 60, "time", 1f, "islocal", true, "easetype", iTween.EaseType.easeInOutSine));
            }

        }

    }

    public void ShowGraph()
    {
        if (!IsTopicUnlocked)
        {
            if (TopicToCheck != 2)
            {
                if (CheckFor_XP_Condition(TopicToCheck))
                    return;
            }
        }

        if (GraphObj.activeInHierarchy)
        {
            GraphObj.SetActive(false);
            ShowGraph_But.GetComponentInChildren<Text>().text = "Show graph";
        }
        else
        {
            GraphObj.SetActive(true);
            ShowGraph_But.GetComponentInChildren<Text>().text = "Hide graph";
        }

        if (!IsTopicUnlocked)
        {
            if (TopicToCheck == 2)
                CheckFor_XP_Condition(TopicToCheck);
        }
    }

    public bool CheckFor_XP_Condition(int Current_Todo)
    {
        bool Condition=true;
        
        if (Current_Todo == 0)
        {
            if (ZekkiMassSlider.value != 1)
            {
                Collect_XP(Zekki);
                Condition = false;
            }
            else
            {
                ShowInstructionPanel("Wrong parameter choosen");
                Condition = true;
            }
        }
        if (Current_Todo == 1)
        {
            if (Zekki.GetComponent<iTween>())
            {
                Collect_XP(Zekki);
                Condition = false;
            }
            else
            {
                ShowInstructionPanel("Wrong parameter choosen");
                Condition = true;
            }
        }
        if (Current_Todo == 2)
        {
            if (GraphObj.activeInHierarchy)
            {
                Collect_XP(Zekki);
                Condition = false;
            }
            else
            {
                ShowInstructionPanel("Wrong parameter choosen");
                Condition = true;
            }
        }


        return Condition;
    }


    public void ShowInstructionPanel(string TextToShow)
    {
        var mouse = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);



        if (mouse.x < Screen.width / 2)
        {
            // WronOptionPanel
            //print("Mouse is on left side of screen.");
            InstructionPanel.GetComponent<RectTransform>().pivot = new Vector2(0, 0);
        }



        if (mouse.x > Screen.width / 2)
        {
            //print("Mouse is on right side of screen.");
            InstructionPanel.GetComponent<RectTransform>().pivot = new Vector2(1, 0);
        }

        InstructionPanel.transform.position = Input.mousePosition;
        UIcanvas.GetComponent<GraphicRaycaster>().enabled = false;
        Camera.main.GetComponent<PhysicsRaycaster>().enabled = false;
        iTween.Stop(InstructionPanel.gameObject);

        InstructionPanel.transform.Find("InstructionText").GetComponent<Text>().text = TextToShow;
        InstructionPanel.transform.GetChild(0).GetComponent<Text>().color = GetColorFromColorCode("#FF7500");
        //iTween.ScaleTo(GlassPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 0.01f, "easetype", iTween.EaseType.spring));
        iTween.ScaleTo(InstructionPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 0.3f, "easetype", iTween.EaseType.spring, "oncomplete", "CloseInstructionPanel", "oncompletetarget", this.gameObject));
        ObjectivePanel.transform.localScale = new Vector3(1, 0, 1);
    }

    public void ShowWelldonePanel(string TextToShow)
    {

        var mouse = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);



        if (mouse.x < Screen.width / 2)
        {
            // WronOptionPanel
            //print("Mouse is on left side of screen.");
            InstructionPanel.GetComponent<RectTransform>().pivot = new Vector2(0, 0);
        }



        if (mouse.x > Screen.width / 2)
        {
            //print("Mouse is on right side of screen.");
            InstructionPanel.GetComponent<RectTransform>().pivot = new Vector2(1, 0);
        }

        InstructionPanel.transform.position = Input.mousePosition;
        UIcanvas.GetComponent<GraphicRaycaster>().enabled = false;
        Camera.main.GetComponent<PhysicsRaycaster>().enabled = false;
        iTween.Stop(InstructionPanel.gameObject);
        //iTween.ScaleTo(GlassPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 0.01f, "easetype", iTween.EaseType.spring));
        InstructionPanel.transform.GetChild(0).GetComponent<Text>().text = TextToShow;// "Wrong parameter choosen";
        InstructionPanel.transform.GetChild(0).GetComponent<Text>().color = GetColorFromColorCode("#AAC018");
        iTween.ScaleTo(InstructionPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 1f, "easetype", iTween.EaseType.spring, "oncomplete", "CloseInstructionPanel", "oncompletetarget", this.gameObject));
        ObjectivePanel.transform.localScale = new Vector3(1, 0, 1);
    }

    public Color GetColorFromColorCode(string ColorCode)
    {
        Color ConvColor;
        bool Converted = ColorUtility.TryParseHtmlString(ColorCode, out ConvColor);
        return ConvColor;
    }
    // Update is called once per frame
    void Update()
    {

        


        //if (Pos != transform.localPosition)
        //{
        //    for (int i = 0; i < Arrows.Count; i++)
        //    {
        //        Vector3 a = (transform.localPosition - new Vector3(3f, 0, 0)) - Arrows[i].transform.localPosition;
        //        Vector3 b = (transform.localPosition + new Vector3(3f, 0, 0)) - Arrows[i].transform.localPosition;

        //        float a1 = Mathf.Atan2(a.y, a.x);
        //        float a2 = Mathf.Atan2(b.y, b.x);
        //        float refl = (Mathf.PI - (a2 - a1)) / 2;

        //        float tan = Mathf.PI - a2 - refl;
        //        float rot = -(tan * Mathf.Rad2Deg);

        //        if (Arrows[i].transform.localPosition.x > (transform.localPosition.x + 3f))
        //        {

        //            Arrows[i].transform.rotation = Quaternion.Euler(0.0f, 0.0f, 180+(a2 * Mathf.Rad2Deg));
        //        }
        //        else if(Arrows[i].transform.localPosition.x < (transform.localPosition.x - 3f))
        //        {
        //            Arrows[i].transform.rotation = Quaternion.Euler(0.0f, 0.0f, (a1 * Mathf.Rad2Deg));
        //        }
        //        else
        //        {
                   
        //                Arrows[i].transform.rotation = Quaternion.Euler(0.0f, 0.0f, rot-refl);
        //            //print("i........"+i+"....tan....."+(tan * Mathf.Rad2Deg));
        //        }
        //        //Arrows[i].transform.LookAt(this.transform,transform.forward);
        //    }
        //    Pos = transform.localPosition;
        //}
    }
}
