﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;

public class vid584_vertibalcolumn : MonoBehaviour
{
    public GameObject MainObject, TargetObj, MidObj, JointsListPanel;
    public GameObject[] Parts, Labels, PartsForTransparency;
    public Vector3[] OriginalPositions, OrgPosForTransparent;
    public Material[] OriginalMaterials;
    public Material TransparentMaterial;
    public Color Highlight_Color, Normal_Color, Transparent_Color;
    //public string[] PartsDescription;
    //public Text TitleText;
    public Image LabelImage, DissolveImage;
    public Slider ExplodeSlider;
    //public Dropdown DropDownBar;
    public List<MeshRenderer> PartToActRend;
    public Dictionary<string, string> DescriTextDoc;
    public float ExplodeValue, transitionTime = 0.01f, Initial_Dist;
    public int CurrJoint;
    public string PartSelectnCheck;
    public bool IsChildTransparency, IsOrbit, IsAR, IsPartHightlight, IsTopicUnlocked, IsUIZooming, IsClicked;
    public Shader StandardShader;
    public Vector3 relativePos, TargetPos, TargetPos_Init;
    public ObjectOrbit CamOrbit;
    public List<Animator> PartsAnimator;

    public int OptionUnlockedTill, CurrentTopicSelected, TopicToCheck;
    public float Sub_XP_Earned, Total_XP;
    public Dropdown Options_DD, ToDoList_DD;
    public GameObject InstructionPanel_DD, ToDoList_Panel, ObjectivePanel, InstructionPanel, GlassPanel;
    public List<Dictionary<string, object>> CSV_data;
    public List<String> Topics, Sub_Topics, Sub_Top_ToDoList, Sub_Top_Desc, Sub_Top_XP;
    public Text XP_ToShow, XP_Effect, Main_XP_ToShow;
    public GameObject UIcanvas;
    public AssetBundle assetBundle;
    public TextAsset CSVFile;
    public AROrbitControls ArOrbit;

    // Start is called before the first frame update
    void Start()
    {

        //OptionUnlockedTill = 5;



        UIcanvas = GameObject.Find("Canvas");

        ExplodeValue = 0.7f;

        LoadFromAsssetBundle();

        LoadingData();
       

        IsPartHightlight = IsAR;
    }

    public void LoadFromAsssetBundle()
    {

        //if (Application.platform == RuntimePlatform.Android||Application.platform == RuntimePlatform.IPhonePlayer)
        //{
        assetBundle = AssetBundle.LoadFromFile(PlayerPrefs.GetString("AssetBundleUrl"));

        CSVFile = assetBundle.LoadAsset(PlayerPrefs.GetString("LibraryName") + "CSV", typeof(TextAsset)) as TextAsset;
        //}
        //else if ()
        //{
        //    CSVFile = Resources.Load("cha_AtwoodMAachineCSV") as TextAsset;
        //}
        //// else
        //{     

       // CSVFile = Resources.Load("vid584_vertibalcolumnCSV") as TextAsset;
        //}







        Camera.main.gameObject.AddComponent<PhysicsRaycaster>();



        Camera.main.backgroundColor = Color.black;



        GameObject TargetForCamera = GameObject.Find("Target");



        if (TargetForCamera != null)
        {
            TargetForCamera.AddComponent<ObjectOrbit>();
            CamOrbit = TargetForCamera.GetComponent<ObjectOrbit>();
            //CamOrbit.Target_Y_Value = 0.5f;
            //CamOrbit.ZoomMax_Y_Value = 0.5f;
            //CamOrbit.transform.position = new Vector3(0, CamOrbit.Target_Y_Value, 0);
            IsAR = false;
            CamOrbit.zoomStart = 1.2f;
            CamOrbit.zoomMin = 0.5f;
            CamOrbit.zoomMax = 2f;
            CamOrbit.Target_Y_Value = 0.3f;
            CamOrbit.ZoomMax_Y_Value = 0.3f;
            CamOrbit.transform.position = new Vector3(0, 0.3f, 0);
        }
        else
        {
            this.transform.parent.gameObject.AddComponent<AROrbitControls>();
            ArOrbit = this.transform.parent.gameObject.GetComponent<AROrbitControls>();
            IsAR = true;
        }



    }



    public void LoadingData()
    {
        IsChildTransparency = false;

        IsOrbit = true;
        IsClicked = true;


        bool NorCol_Converted = ColorUtility.TryParseHtmlString("#FFFFFF", out Normal_Color);
        bool HighCol_Converted = ColorUtility.TryParseHtmlString("#A28C8C", out Highlight_Color);

        ReadingDataFromCSVFile();
        ReadingXps();
        FindingGameObjects();


        AssignClickEventsToObjects();

        //ChangeToOpaque();

        for (int childparts = 0; childparts < Parts.Length; childparts++)
        {
            if (Parts[childparts].GetComponent<Animator>())
            {
                for (int i = 0; i < Parts.Length; i++)
                {
                    if (Parts[childparts].name.Contains(Parts[i].name))
                    {
                        Parts[childparts].transform.parent = Parts[i].GetComponentInChildren<Animator>().transform;
                    }
                }

            }
        }

        StartCoroutine(ActivatePlaymode(0.1f));




        if (Topics.Count <= 1)
            Options_DD.gameObject.SetActive(false);



    }

    public void ReadingDataFromCSVFile()
    {
        Topics = new List<string>();
        Sub_Topics = new List<string>();
        Sub_Top_Desc = new List<string>();
        Sub_Top_ToDoList = new List<string>();
        Sub_Top_XP = new List<string>();

        CSV_data = CSVReader.Read(CSVFile);

        for (var i = 0; i < CSV_data.Count; i++)
        {
            //print("Topics " + CSV_data[i]["Topics"] + ".......... " +"ToDo " + CSV_data[i]["Topic1_TodoList"]);

            if (CSV_data[i]["Topics"].ToString() != "" && CSV_data[i]["Topics"].ToString() != null)
            {
                Topics.Add(CSV_data[i]["Topics"].ToString());

            }

            if (CSV_data[i]["Topic_" + CurrentTopicSelected + "_SubTopics"].ToString() != "" && CSV_data[i]["Topic_" + CurrentTopicSelected + "_SubTopics"].ToString() != null)
            {
                Sub_Topics.Add(CSV_data[i]["Topic_" + CurrentTopicSelected + "_SubTopics"].ToString());

            }

            if (CSV_data[i]["Topic_" + CurrentTopicSelected + "_ToDoList"].ToString() != "" && CSV_data[i]["Topic_" + CurrentTopicSelected + "_ToDoList"].ToString() != null)
            {
                Sub_Top_ToDoList.Add(CSV_data[i]["Topic_" + CurrentTopicSelected + "_ToDoList"].ToString());

            }

            if (CSV_data[i]["Top_" + CurrentTopicSelected + "_Sub_Desc"].ToString() != "" && CSV_data[i]["Top_" + CurrentTopicSelected + "_Sub_Desc"].ToString() != null)
            {
                Sub_Top_Desc.Add(CSV_data[i]["Top_" + CurrentTopicSelected + "_Sub_Desc"].ToString());

            }

            //if (CSV_data[i]["Top_" + CurrentTopicSelected + "_XP"].ToString() != "" && CSV_data[i]["Top_" + CurrentTopicSelected + "_XP"].ToString() != null)
            //{
            //    Sub_Top_XP.Add(CSV_data[i]["Top_" + CurrentTopicSelected + "_XP"].ToString());

            //}

        }






    }

    public int totXp, loclXp, PerTopic_XP;

    public void ReadingXps()
    {

        //****************For local checking only
       // PlayerPrefs.SetInt("TotalXP", 100);//Comment this lines while taking assetbundles
       // PlayerPrefs.SetInt("MaxEarnings", 40);//Comment this lines while taking assetbundles
                                              //****************End of local checking only

        totXp = PlayerPrefs.GetInt("TotalXP");
        loclXp = PlayerPrefs.GetInt("MaxEarnings");

        TopicToCheck = 0;

        if (loclXp != 0)
        {
            OptionUnlockedTill = 0;

        }
        else
        {
            OptionUnlockedTill = 1;

        }





        int t = 0;
        int p = 0;

        for (int k = 0; k < Sub_Top_ToDoList.Count; k++)
        {

            p = loclXp / Sub_Top_ToDoList.Count;
            Sub_Top_XP.Add(p.ToString());
            t = t + p;
        }

        if (t != loclXp)
        {
            Sub_Top_XP[Sub_Top_ToDoList.Count - 1] = (p + (loclXp - t)).ToString();
        }

        Total_XP = loclXp;
       

    }



    public float result;

    public void GetResult()
    {
        result = ((float)(TopicToCheck - 0) / (float)(Sub_Top_ToDoList.Count)) * 100;
        Camera.main.SendMessage("UnloadActually", result);
    }

    public void UIReset()
    {


        ObjectivePanel.GetComponent<RectTransform>().anchoredPosition = new Vector3(0f, 5f, 0);
       // TitleText.transform.GetComponent<RectTransform>().anchoredPosition = new Vector3(340f, -95f, 0);
        LabelImage.transform.GetComponent<RectTransform>().anchoredPosition = new Vector3(35f, 210f, 0);
        DissolveImage.transform.GetComponent<RectTransform>().anchoredPosition = new Vector3(35f, -200f, 0);
        ExplodeSlider.transform.GetComponent<RectTransform>().anchoredPosition = new Vector3(-20f, 0f, 0);
        Options_DD.transform.GetComponent<RectTransform>().anchoredPosition = new Vector3(-135f, -150f, 0);

        if (!IsTopicUnlocked)
        {
            ToDoList_Panel.transform.GetComponent<RectTransform>().anchoredPosition = new Vector3(0f, 0f, 0);
            LabelImage.GetComponent<RectTransform>().anchoredPosition = new Vector3(-100f, 210, 0);
            //ExplodeSlider.GetComponent<RectTransform>().anchoredPosition = new Vector3(100f, 0, 0);
        }
        else
        {
            ToDoList_Panel.transform.GetComponent<RectTransform>().anchoredPosition = new Vector3(-1500f, 0f, 0);
            LabelImage.GetComponent<RectTransform>().anchoredPosition = new Vector3(35f, 210, 0);
            //ExplodeSlider.GetComponent<RectTransform>().anchoredPosition = new Vector3(-20f, 0, 0);
        }

        //Main_XP_ToShow.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(-135f, -110f, 0);

    }

    public void UIZoomOut()
    {
        if (ObjectivePanel.GetComponent<RectTransform>().anchoredPosition.y == 5)
            return;

        UIReset();
        iTween.MoveFrom(ExplodeSlider.gameObject, iTween.Hash("x", ExplodeSlider.transform.localPosition.x + 300, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveFrom(ObjectivePanel, iTween.Hash("y", ObjectivePanel.transform.localPosition.y - 1000, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
      //  iTween.MoveFrom(TitleText.gameObject, iTween.Hash("y", TitleText.transform.localPosition.y + 600, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        if (IsTopicUnlocked)
        {
            iTween.MoveFrom(LabelImage.gameObject, iTween.Hash("x", LabelImage.transform.localPosition.x - 300, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
            iTween.MoveFrom(DissolveImage.gameObject, iTween.Hash("x", DissolveImage.transform.localPosition.x - 300, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));

        }
        else
        {
            iTween.MoveFrom(ToDoList_Panel.gameObject, iTween.Hash("x", ToDoList_DD.transform.localPosition.x - 1500, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        }
        iTween.MoveFrom(Options_DD.gameObject, iTween.Hash("x", Options_DD.transform.localPosition.x + 550, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        //iTween.MoveFrom(Main_XP_ToShow.transform.parent.gameObject, iTween.Hash("x", Main_XP_ToShow.transform.parent.localPosition.x + 550, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
    }



    public void UIZoomIn()
    {
        if (ObjectivePanel.GetComponent<RectTransform>().anchoredPosition.y != 5)
            return;

        UIReset();
        iTween.MoveAdd(ExplodeSlider.gameObject, iTween.Hash("x", ExplodeSlider.transform.localPosition.x + 300, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveAdd(ObjectivePanel, iTween.Hash("y", ObjectivePanel.transform.localPosition.y - 1000, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
       // iTween.MoveAdd(TitleText.gameObject, iTween.Hash("y", TitleText.transform.localPosition.y + 600, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        if (IsTopicUnlocked)
        {
            iTween.MoveAdd(LabelImage.gameObject, iTween.Hash("x", LabelImage.transform.localPosition.x - 300, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
            iTween.MoveAdd(DissolveImage.gameObject, iTween.Hash("x", DissolveImage.transform.localPosition.x - 300, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));

        }
        else
        {
            iTween.MoveAdd(ToDoList_Panel.gameObject, iTween.Hash("x", ToDoList_DD.transform.localPosition.x - 1500, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        }
        iTween.MoveAdd(Options_DD.gameObject, iTween.Hash("x", Options_DD.transform.localPosition.x + 550, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        //iTween.MoveAdd(Main_XP_ToShow.transform.parent.gameObject, iTween.Hash("x", Main_XP_ToShow.transform.parent.localPosition.x + 550, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
    }

    IEnumerator ActivatePlaymode(float WaitTime)
    {

        yield return new WaitForSeconds(WaitTime);

        ChangeToOpaque();

        yield return new WaitForSeconds(1f);

        GameObject PartToAct;

        PartToActRend = new List<MeshRenderer>();

        for (int i = 0; i < PartsForTransparency.Length; i++)

        {
            PartToAct = PartsForTransparency[i];

            if (PartToAct.GetComponent<MeshRenderer>() != null)
            {
                PartToActRend.Add(PartToAct.GetComponent<MeshRenderer>());
            }
            else
                PartToActRend.AddRange(PartToAct.transform.GetComponentsInChildren<MeshRenderer>().ToList());


            for (int j = 0; j < PartToActRend.Count; j++)
            {

                Material Mat = PartToActRend[j].material;
                Mat.shader = Shader.Find("Standard");
                SetupMaterialWithBlendMode(Mat, BlendMode.Transparent);
                //Mat.SetFloat("_Glossiness", 0.1f);
                Mat.SetFloat("_Metallic", 0.05f);
                Mat.SetFloat("_Glossiness", 0.4f);

                Mat.color = new Color(Mat.color.r, Mat.color.g, Mat.color.b, 1);
            }
            //Debug.Log("foreach......."+ PartsForTransparency[i].material);
        }

        yield return new WaitForSeconds(1f);

        InvokeRepeating("ChangeToTransparency", 0.2f, 0.1f);



    }



    public void ChangeToTransparency()
    {
        //print(" trans......"+ PartToActRend[0].material.color.a);
        if (PartToActRend[0].material.color.a < 0.17f)
        {
            CancelInvoke("ChangeToTransparency");
            ExplodeSlider.GetComponentInParent<CanvasGroup>().blocksRaycasts = true;
            IsClicked = false;
            ExplodeSlider.GetComponentInParent<CanvasGroup>().alpha = 1;
        }


        for (int i = 0; i < PartToActRend.Count; i++)

        {

            Color newColor = new Color(PartToActRend[i].material.color.r, PartToActRend[i].material.color.g, PartToActRend[i].material.color.b, 0.05f);


            PartToActRend[i].material.SetColor("_Color", Color.Lerp(PartToActRend[i].material.color, newColor, Time.deltaTime * 7f));




        }


        ExplodeSlider.GetComponentInParent<CanvasGroup>().alpha = ExplodeSlider.GetComponentInParent<CanvasGroup>().alpha + 0.05f;

    }

    public void FindingGameObjects()
    {
        PartsAnimator = new List<Animator>();

        for (int i = 0; i < Topics.Count; i++)
        {
            transform.Find(Topics[i]).gameObject.SetActive(false);
        }


        MainObject = transform.Find(Topics[CurrentTopicSelected]).gameObject;
        MainObject.SetActive(true);

        Parts = new GameObject[MainObject.transform.Find("PartsToAdd").childCount];
        Labels = new GameObject[Parts.Length];
        PartsForTransparency = new GameObject[MainObject.transform.Find("PartsForTransparency").childCount];// MainObject.GetComponentsInChildren<MeshRenderer>().Where(r => r.tag == "PartsForTransparency").ToList(); 

        //OriginalMaterials = new Material[Parts.Length];
        OriginalPositions = new Vector3[Parts.Length];
        OrgPosForTransparent = new Vector3[PartsForTransparency.Length];


        ObjectivePanel = GameObject.Find("ObjectivePanel");
       // TitleText = GameObject.Find("TitleText").GetComponent<Text>();

        LabelImage = GameObject.Find("ActivateLabels").GetComponent<Image>();
        DissolveImage = GameObject.Find("DissolveSelect").GetComponent<Image>();
        ExplodeSlider = GameObject.Find("ExplodeSlider").GetComponent<Slider>();
        //DropDownBar = GameObject.Find("Dropdown").GetComponent<Dropdown>();
        //JointsListPanel = GameObject.Find("JointsBar");



        for (int i = 0; i < Parts.Length; i++)
        {
            Parts[i] = MainObject.transform.Find("PartsToAdd").GetChild(i).gameObject;



            //OriginalMaterials[i] = Parts[i].GetComponent<MeshRenderer>() ? Parts[i].GetComponent<MeshRenderer>().material : Parts[i].transform.GetChild(0).GetComponent<MeshRenderer>().material;
            OriginalPositions[i] = Parts[i].transform.localPosition;
            //print("orgpos..local....." + Parts[i].transform.localPosition + "............" + Parts[i].transform.position);



        }





        for (int j = 0; j < Labels.Length; j++)
        {
            Labels[j] = GameObject.Find(Parts[j].name + "_Label").gameObject;
            Labels[j].transform.parent = Parts[j].transform;
            Labels[j].transform.rotation = Quaternion.identity;

            //Labels[j].GetComponentInChildren<TextMeshPro>().text = Sub_Topics[j];


            GameObject TempLabels = Labels[j].transform.Find("SmoothLook").gameObject;
            if (TempLabels)
                TempLabels.AddComponent<SmoothLook>();

            TempLabels = null;
            TempLabels = Labels[j].transform.Find("SmoothLook/Label_Desc").gameObject;

            if (TempLabels)
                TempLabels.transform.localScale = new Vector3(1, 0, 1);

            Labels[j].SetActive(false);

            /*foreach (Transform ChildLabel in TempLabels.transform)
            {
                if (ChildLabel.name.Contains("SmoothLook"))
                {
                    ChildLabel.gameObject.AddComponent<SmoothLook>();
                }

            }*/





        }




        //OptionUnlockedTill = 4;
        Sub_XP_Earned = 0;

        if (Options_DD == null)
            Options_DD = GameObject.Find("TopicsDropdown").GetComponent<Dropdown>();
        InstructionPanel_DD = GameObject.Find("DD_Instruction");
        InstructionPanel = GameObject.Find("InstructionPanel");
        ToDoList_DD = GameObject.Find("ToDoList_Dropdown").GetComponent<Dropdown>();
        XP_ToShow = GameObject.Find("Gained_XP_Value").GetComponent<Text>();
        XP_Effect = GameObject.Find("Effect_Text").GetComponent<Text>();
        Main_XP_ToShow = GameObject.Find("Main_XP_Value").GetComponent<Text>();
        ToDoList_Panel = GameObject.Find("To_Do_List_Panel");
        InstructionPanel.transform.localScale = Vector3.zero;
        GlassPanel = GameObject.Find("GlassPanel");
        GlassPanel.transform.localScale = Vector3.zero;
        ExplodeSlider.value = 0;
        LabelImage.color = Normal_Color;
        DissolveImage.color = Normal_Color;

        if (CurrentTopicSelected < OptionUnlockedTill)
        {
            ToDoList_Panel.GetComponent<RectTransform>().anchoredPosition = new Vector3(-1500f, 0, 0);
            LabelImage.GetComponent<RectTransform>().anchoredPosition = new Vector3(35f, 210, 0);
            //ExplodeSlider.GetComponent<RectTransform>().anchoredPosition = new Vector3(-20f, 0, 0);
            ObjectivePanel.transform.localScale = new Vector3(1, 0, 1);
            IsTopicUnlocked = true;
        }
        else
        {
            ToDoList_Panel.GetComponent<RectTransform>().anchoredPosition = new Vector3(0, 0, 0);

            LabelImage.GetComponent<RectTransform>().anchoredPosition = new Vector3(-100f, 210, 0);

            //ExplodeSlider.GetComponent<RectTransform>().anchoredPosition = new Vector3(100f, 0, 0);
            ObjectivePanel.transform.localScale = new Vector3(1, 1, 1);
            IsTopicUnlocked = false;
        }

        if (Options_DD.options.Count == 0)
            Options_DD.AddOptions(Topics.ToList());

        Main_XP_ToShow.text = totXp.ToString();
        XP_ToShow.text = "0/" + Total_XP;
        string CheckStr = "";
        for (int i = 0; i < Sub_Top_ToDoList.Count; i++)
        {

            ToDoList_DD.options.Add(new Dropdown.OptionData() { text = Sub_Top_ToDoList[i] });

            Labels[i].transform.Find("SmoothLook/Label_Text").GetComponent<TextMeshPro>().text = Sub_Topics[i];
            Labels[i].transform.Find("SmoothLook/Label_Desc/Label_Desc_Text").GetComponent<TextMeshPro>().text = Sub_Top_Desc[i];


            //List<float> TotalXP = Sub_Top_XP.Select(s => float.Parse(s)).ToList();

            //if (CheckStr != Sub_Top_ToDoList[i])
            //    Total_XP += TotalXP[i];

            XP_ToShow.text = "0/" + Total_XP;

            //ToDoList_DD.transform.Find("Topic_Objective").GetComponent<Text>().text = "Objective: "+Sub_Top_ToDoList[TopicToCheck];

            ObjectivePanel.transform.Find("ObjectiveText").GetComponent<Text>().text = Sub_Top_ToDoList[TopicToCheck];

            Options_DD.transform.Find("Topic_Label").GetComponent<Text>().text = Topics[CurrentTopicSelected];

            CheckStr = Sub_Top_ToDoList[i];
        }

    }

    public float ChangeToLocal_X(float Val)
    {
        float X_ToRet = 0;
        //print("wid........"+ UIcanvas.GetComponent<RectTransform>().rect.width);
        X_ToRet = ((UIcanvas.GetComponent<RectTransform>().rect.x) + Val);

        return X_ToRet;
    }

    public float ChangeToLocal_Y(float Val)
    {
        float Y_ToRet = 0;
        //print("wid........"+ UIcanvas.GetComponent<RectTransform>().rect.width);
        Y_ToRet = ((UIcanvas.GetComponent<RectTransform>().rect.y) + Val);

        return Y_ToRet;
    }

    public void ChangeToOpaque()
    {
        GameObject PartToAct;
        PartToActRend = new List<MeshRenderer>();

        for (int l = 0; l < PartsForTransparency.Length; l++)
        {
            PartsForTransparency[l] = MainObject.transform.Find("PartsForTransparency").GetChild(l).gameObject;
            OrgPosForTransparent[l] = PartsForTransparency[l].transform.localPosition;


            PartToAct = PartsForTransparency[l];

            if (PartToAct.GetComponent<MeshRenderer>() != null)
            {
                PartToActRend.Add(PartToAct.GetComponent<MeshRenderer>());
            }
            else
                PartToActRend.AddRange(PartToAct.transform.GetComponentsInChildren<MeshRenderer>().ToList());


            for (int j = 0; j < PartToActRend.Count; j++)
            {

                Material Mat = PartToActRend[j].material;
                Mat.shader = Shader.Find("Standard");
                SetupMaterialWithBlendMode(Mat, BlendMode.Opaque);
                //Mat.SetFloat("_Glossiness", 0.1f);
                Mat.SetFloat("_Metallic", 0.05f);
                Mat.SetFloat("_Glossiness", 0.4f);
                Mat.color = new Color(Mat.color.r, Mat.color.g, Mat.color.b, 1);
            }
            //Debug.Log("foreach......."+ PartsForTransparency[i].material);

        }
    }


    public void AssignClickEventsToObjects()
    {
        ExplodeSlider.gameObject.GetComponent<EventTrigger>().triggers.RemoveRange(0, ExplodeSlider.gameObject.GetComponent<EventTrigger>().triggers.Count);

        LabelImage.GetComponent<Button>().onClick.RemoveAllListeners();
        LabelImage.GetComponent<Button>().onClick.AddListener(() => ToggleActivateLabels(LabelImage));

        DissolveImage.GetComponent<Button>().onClick.RemoveAllListeners();
        DissolveImage.GetComponent<Button>().onClick.AddListener(() => SelectDissolveMode());

        ExplodeSlider.onValueChanged.RemoveAllListeners();
        ExplodeSlider.onValueChanged.AddListener(delegate { ExplodeObject(ExplodeSlider.value); });




        AddListenerToEvents(EventTriggerType.PointerDown, DisableOrbit, ExplodeSlider.gameObject);
        AddListenerToEvents(EventTriggerType.PointerUp, EnableOrbit, ExplodeSlider.gameObject);


        if (IsChildTransparency)
        {

            for (int i = 0; i < Parts.Length; i++)
            {


                int j = i;



                if (Parts[i].GetComponent<EventTrigger>() != null || Parts[i].GetComponentsInChildren<EventTrigger>() != null)
                {
                    Labels[i].GetComponent<EventTrigger>().triggers.Clear();
                    AddListenerToEvents(EventTriggerType.PointerClick, SelectPartToHighlight, Labels[i], Parts[j]);
                    AddListenerToEvents(EventTriggerType.BeginDrag, OnElementBeginDrag, Labels[i]);
                    AddListenerToEvents(EventTriggerType.EndDrag, OnElementEndDrag, Labels[i]);

                    if (Parts[i].GetComponent<EventTrigger>())
                    {
                        Parts[i].GetComponent<EventTrigger>().triggers.Clear();
                        AddListenerToEvents(EventTriggerType.BeginDrag, OnElementBeginDrag, Parts[i]);
                        AddListenerToEvents(EventTriggerType.EndDrag, OnElementEndDrag, Parts[i]);
                        //Parts[i].gameObject.GetComponent<EventTrigger>().triggers.RemoveRange(0, Parts[i].gameObject.GetComponent<EventTrigger>().triggers.Count);
                        AddListenerToEvents(EventTriggerType.PointerClick, SelectPartToHighlight, Parts[i], Parts[j]);

                    }
                    else
                    {
                        //print("inelse......"+i);
                        foreach (EventTrigger item in Parts[i].gameObject.GetComponentsInChildren<EventTrigger>())
                        {
                            item.triggers.Clear();

                            AddListenerToEvents(EventTriggerType.BeginDrag, OnElementBeginDrag, item.gameObject);
                            AddListenerToEvents(EventTriggerType.EndDrag, OnElementEndDrag, item.gameObject);
                            //Parts[i].gameObject.GetComponent<EventTrigger>().triggers.RemoveRange(0, Parts[i].gameObject.GetComponent<EventTrigger>().triggers.Count);
                            AddListenerToEvents(EventTriggerType.PointerClick, SelectPartForTransparency, item.gameObject, Parts[j]);
                            AddListenerToEvents(EventTriggerType.PointerClick, SelectPartToHighlight, item.gameObject, Parts[j]);
                        }

                    }
                }
            }

        }
        else
        {
            for (int i = 0; i < Parts.Length; i++)
            {


                int j = i;





                if (Parts[i].GetComponent<EventTrigger>() != null || Parts[i].GetComponentsInChildren<EventTrigger>() != null)
                {
                    Labels[i].GetComponent<EventTrigger>().triggers.Clear();
                    AddListenerToEvents(EventTriggerType.PointerClick, SelectPartToHighlight, Labels[i], Parts[j]);
                    AddListenerToEvents(EventTriggerType.BeginDrag, OnElementBeginDrag, Labels[i]);
                    AddListenerToEvents(EventTriggerType.EndDrag, OnElementEndDrag, Labels[i]);

                    if (Parts[i].GetComponent<EventTrigger>())
                    {

                        Parts[i].GetComponent<EventTrigger>().triggers.Clear();
                        AddListenerToEvents(EventTriggerType.BeginDrag, OnElementBeginDrag, Parts[i]);
                        AddListenerToEvents(EventTriggerType.EndDrag, OnElementEndDrag, Parts[i]);
                        //Parts[i].gameObject.GetComponent<EventTrigger>().triggers.RemoveRange(0, Parts[i].gameObject.GetComponent<EventTrigger>().triggers.Count);
                        AddListenerToEvents(EventTriggerType.PointerClick, SelectPartForTransparency, Parts[i], Parts[j]);
                        AddListenerToEvents(EventTriggerType.PointerClick, SelectPartToHighlight, Parts[i], Parts[j]);

                    }
                    else
                    {
                        //print("inelse......"+i);
                        foreach (EventTrigger item in Parts[i].gameObject.GetComponentsInChildren<EventTrigger>())
                        {
                            item.triggers.Clear();
                            //item.triggers.RemoveRange(0, item.triggers.Count);
                            AddListenerToEvents(EventTriggerType.PointerClick, SelectPartForTransparency, item.gameObject, Parts[j]);
                            AddListenerToEvents(EventTriggerType.PointerClick, SelectPartToHighlight, item.gameObject, Parts[j]);
                            AddListenerToEvents(EventTriggerType.BeginDrag, OnElementBeginDrag, item.gameObject);
                            AddListenerToEvents(EventTriggerType.EndDrag, OnElementEndDrag, item.gameObject);

                        }

                    }
                }
            }
        }


        ToDoList_DD.GetComponent<EventTrigger>().triggers.Clear();

        Options_DD.onValueChanged.RemoveAllListeners();
        Options_DD.onValueChanged.AddListener(delegate { SelectTopicFromDropdown(Options_DD.value); });
        AddListenerToEvents(EventTriggerType.PointerClick, ExploringTopicList_DD, Options_DD.gameObject);
        AddListenerToEvents(EventTriggerType.PointerClick, ExploringToDOList_DD, ToDoList_DD.gameObject);
        AddListenerToEvents(EventTriggerType.Select, DDCancel, ToDoList_DD.gameObject, ToDoList_DD.gameObject);
        AddListenerToEvents(EventTriggerType.Select, DDCancel, Options_DD.gameObject, Options_DD.gameObject);
    }

    public void AddListenerToEvents(EventTriggerType eventType, Action MethodToCall, GameObject TriggerObjToAdd)
    {

        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall());
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }

    public void AddListenerToEvents(EventTriggerType eventType, Action<float> MethodToCall, GameObject TriggerObjToAdd, float p)
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall(p));
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }

    public void AddListenerToEvents(EventTriggerType eventType, Action<GameObject> MethodToCall, GameObject TriggerObjToAdd, GameObject p)
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall(p));
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }


    public void OnElementBeginDrag()
    {
        IsClicked = true;
    }



    public void OnElementEndDrag()
    {
        //print("drag ended");
        IsClicked = false;
    }


    public Color ReturnColorFromHex(string ColorCode)
    {
        Color ColorToReturn = Color.white;
        bool NorCol_Converted = ColorUtility.TryParseHtmlString(ColorCode, out ColorToReturn);
        return ColorToReturn;
    }

    public void DDCancel(GameObject DDToCheck)
    {
        DDToCheck.transform.Find("Arrow").transform.eulerAngles = new Vector3(0, 0, 0);
        if (DDToCheck.transform.Find("Dropdown List") != null)//&& Obj.transform.localScale.y!=1)
        {
            //print("cancelled........");
            //print("list.....cancelled........" + ToDoList_DD.transform.Find("Dropdown List").gameObject);
            iTween.ScaleTo(DDToCheck.transform.Find("Dropdown List").gameObject, iTween.Hash("y", 0, "time", 0.1f, "easetype", iTween.EaseType.spring));

        }
        if (!IsTopicUnlocked)
            iTween.ScaleTo(ObjectivePanel, iTween.Hash("y", 1, "time", 0.5f, "delay", 0.1f, "easetype", iTween.EaseType.easeInOutSine));
    }

    public void ExploringToDOList_DD()
    {
        //print("ShowDD.....");
        //ToDoList_DD.transform.Find("Topic_Objective").GetComponent<Text>().text = "";

        ToDoList_DD.transform.Find("Arrow").transform.eulerAngles = new Vector3(0, 0, 180);
        GameObject DDList = ToDoList_DD.transform.Find("Dropdown List").gameObject;
        DDList.transform.GetComponent<RectTransform>().sizeDelta = new Vector2(0, 250);
        DDList.transform.localScale = new Vector3(1, 0, 1);
        iTween.ScaleTo(DDList, iTween.Hash("y", 1, "time", 0.5f, "easetype", iTween.EaseType.spring));
        //iTween.ScaleTo(ToDoList_DD.transform.Find("Topic_Objective").gameObject, iTween.Hash("y", 0, "time", 0.5f, "easetype", iTween.EaseType.spring));
        if (!IsTopicUnlocked)
            iTween.ScaleTo(ObjectivePanel, iTween.Hash("y", 0, "time", 0.5f, "delay", 0.1f, "easetype", iTween.EaseType.easeInOutSine));

        string TestStr = "";
        int CntToChangePos = 0;
        for (int i = 0; i < ToDoList_DD.options.Count; i++)
        {
            GameObject Child = GameObject.Find("Item " + i + ": " + ToDoList_DD.options[i].text);
            //Child.transform.parent.GetComponent<RectTransform>().sizeDelta = new Vector2(0,250);

            if (TestStr == ToDoList_DD.options[i].text)
            {
                Child.SetActive(false);
                Child.transform.localPosition = Child.transform.localPosition + new Vector3(0, 50 * CntToChangePos, 0);
                CntToChangePos++;
            }
            else
            {
                Child.transform.Find("XP_Text").GetComponent<Text>().text = "+" + Sub_Top_XP[i] + "XP";
                Child.transform.localPosition = Child.transform.localPosition + new Vector3(0, 50 * CntToChangePos, 0);

                if (i < TopicToCheck)
                {
                    Child.transform.Find("Item Checkmark").GetComponent<Image>().enabled = true;
                    //Child.transform.Find("XP_Text").GetComponent<Text>().color = Color.green / 2;
                    //Child.transform.Find("Item Checkmark").GetComponent<Image>().color = Color.green / 2;
                    Child.transform.Find("Item Background").GetComponent<Image>().enabled = false;
                }
                else if (i == TopicToCheck)
                {
                    Child.transform.Find("Item Background").GetComponent<Image>().enabled = true;// color = ReturnColorFromHex("#a01c65");
                                                                                                 //Child.transform.Find("Item Label").GetComponent<Text>().color = Color.white;
                }
            }
            TestStr = ToDoList_DD.options[i].text;
        }
    }

    public void ExploringTopicList_DD()
    {
        Options_DD.transform.Find("Arrow").transform.eulerAngles = new Vector3(0, 0, 180);

        GameObject DDList = Options_DD.transform.Find("Dropdown List").gameObject;
        DDList.transform.localScale = new Vector3(1, 0, 1);
        iTween.ScaleTo(DDList, iTween.Hash("y", 1, "time", 0.5f, "easetype", iTween.EaseType.spring));


        for (int i = 0; i < Options_DD.options.Count; i++)
        {
            GameObject Child = GameObject.Find("Item " + i + ": " + Options_DD.options[i].text);

            GameObject Reminder = Child.transform.Find("ItemReminder").gameObject;

            if (i == OptionUnlockedTill && CurrentTopicSelected != i)
            {
                Reminder.GetComponent<Image>().enabled = true;
                if (!Reminder.GetComponent<iTween>())
                    iTween.ScaleTo(Reminder, iTween.Hash("x", 1, "y", 1, "time", 0.5f, "looptype", iTween.LoopType.pingPong, "easetype", iTween.EaseType.easeInOutSine));
                //Child.transform.Find("Item Label").GetComponent<Text>().color = Color.white;
            }
            else
            {
                Reminder.GetComponent<Image>().enabled = false;
                if (Reminder.GetComponent<iTween>())
                    Destroy(Reminder.GetComponent<iTween>());
            }


        }
    }


    public void Collect_XP()
    {


        //print("TopicToCheck......."+ TopicToCheck+"....sub......"+Sub_Topics.Count);
        if (TopicToCheck < (Sub_Topics.Count))
        {
            Sub_XP_Earned += float.Parse(Sub_Top_XP[TopicToCheck]);

            XP_Effect.text = "";
            XP_ToShow.text = Sub_XP_Earned + "/" + Total_XP;
            string CurTopName = Sub_Top_ToDoList[TopicToCheck];



            TopicToCheck++;

            if (TopicToCheck < (Sub_Topics.Count) && CurTopName == Sub_Top_ToDoList[TopicToCheck])
                TopicToCheck++;



            if (TopicToCheck <= (Sub_Topics.Count - 1))
            {
                ObjectivePanel.transform.Find("ObjectiveText").GetComponent<Text>().text = Sub_Top_ToDoList[TopicToCheck];
                Camera.main.GetComponent<PhysicsRaycaster>().enabled = true;
                ObjectivePanel.transform.localScale = new Vector3(1, 0, 1);
                iTween.ScaleTo(ObjectivePanel, iTween.Hash("y", 1, "time", 0.5f, "delay", 0.1f, "easetype", iTween.EaseType.easeInOutSine));
                //ToDoList_DD.transform.Find("Topic_Objective").GetComponent<Text>().text = "Objective: " + Sub_Top_ToDoList[TopicToCheck];
            }
            else
            {
                XP_Effect.transform.position = (XP_ToShow.transform.position);

                XP_Effect.text = "+" + Total_XP + "XP";

                XP_Effect.transform.localScale = new Vector3(1, 0, 1);

                iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 2, "y", 2, "z", 2, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeOutElastic));

                iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", XP_Effect.transform.position.x, "y", XP_Effect.transform.position.y + 125, "z", 0, "delay", 0.5f, "time", 0.7f, "easetype", iTween.EaseType.easeInSine));

                iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", Main_XP_ToShow.transform.position.x, "y", Main_XP_ToShow.transform.position.y, "z", 0, "delay", 1.5f, "time", 1f, "easetype", iTween.EaseType.easeInSine, "oncomplete", "AddTotalToMainXP", "oncompletetarget", this.gameObject));


            }
            if (TopicToCheck == 4)
            {
                Parts[TopicToCheck].GetComponentInChildren<SphereCollider>().enabled = true;
                Parts[TopicToCheck].GetComponentInChildren<Renderer>().material.color = new Color(1, 0.05f, 0.004f, 0f);
            }
        }

    }

    public void AddTotalToMainXP()
    {
        if (OptionUnlockedTill < Topics.Count)
        {
            OptionUnlockedTill++;
            Camera.main.GetComponent<PhysicsRaycaster>().enabled = true;
            float MainXp = float.Parse(Main_XP_ToShow.text);
            MainXp += Total_XP;
            Main_XP_ToShow.text = MainXp.ToString();
            //ObjectivePanel.transform.Find("ObjectiveText").GetComponent<Text>().text = "YaY!......Next topic unlocked";
            XP_Effect.text = "";
            IsTopicUnlocked = true;
            iTween.ScaleTo(ObjectivePanel, iTween.Hash("y", 0, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine));

            iTween.MoveAdd(ToDoList_Panel, iTween.Hash("x", -1500, "delay", 1.5f, "time", 0.5f, "islocal", true, "easetype", iTween.EaseType.easeInOutSine));

            IsUIZooming = true;



            iTween.MoveTo(LabelImage.gameObject, iTween.Hash("x", ChangeToLocal_X(35), "delay", 1.5f, "time", 0.5f, "islocal", true, "easetype", iTween.EaseType.easeInOutSine));

            //iTween.MoveTo(ExplodeSlider.gameObject, iTween.Hash("x", ChangeToLocal_X(-20), "delay", 0.7f, "time", 0.5f, "islocal", true, "easetype", iTween.EaseType.easeInOutSine));

            iTween.ScaleTo(InstructionPanel_DD, iTween.Hash("y", 0, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine));
            if (OptionUnlockedTill < Topics.Count)
            {
                Options_DD.transform.Find("Reminder").GetComponent<Image>().enabled = true;

                if (!Options_DD.transform.Find("Reminder").gameObject.GetComponent<iTween>())
                    iTween.ScaleTo(Options_DD.transform.Find("Reminder").gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 0.5f, "looptype", iTween.LoopType.pingPong, "easetype", iTween.EaseType.easeInOutSine));
            }
        }
    }

    public void SelectTopicFromDropdown(int DD_Value)
    {
        //print("ddval....."+DD_Value);
        if (InstructionPanel_DD.transform.localScale.y > 0)
            InstructionPanel_DD.transform.localScale = new Vector3(1, 0, 1);

        TargetObj = null;
        if (!IsAR)
        {
            CamOrbit.ChangeTarget(null);
            CamOrbit.distance = CamOrbit.zoomStart;
        }

        if (DD_Value <= OptionUnlockedTill)
        {
            CurrentTopicSelected = DD_Value;

            CancelInvoke("ChangeToTransparency");

            iTween[] tweens = UIcanvas.transform.GetComponentsInChildren<iTween>();

            for (int i = 0; i < tweens.Length; i++)
            {
                if (tweens[i].gameObject != InstructionPanel_DD)
                    Destroy(tweens[i]);
            }
            UIReset();

            SelectTopic(DD_Value);

            if (DD_Value == OptionUnlockedTill)
                Options_DD.transform.Find("Reminder").GetComponent<Image>().enabled = false;
        }
        else
        {

            InstructionPanel_DD.GetComponentInChildren<Text>().text = "Complete " + Topics[OptionUnlockedTill] + " to unlock";
            iTween.ScaleAdd(InstructionPanel_DD, iTween.Hash("y", 1, "time", 1f, "islocal", true, "easetype", iTween.EaseType.easeOutSine, "oncomplete", "CloseInstructionPanel_DD", "oncompletetarget", this.gameObject));
            Options_DD.onValueChanged.RemoveAllListeners();
            Options_DD.transform.Find("Arrow").transform.eulerAngles = new Vector3(0, 0, 0);
            Options_DD.ClearOptions();
            Options_DD.AddOptions(Topics.ToList());
            Options_DD.RefreshShownValue();
            Options_DD.onValueChanged.AddListener(delegate { SelectTopicFromDropdown(Options_DD.value); });
            Options_DD.value = OptionUnlockedTill;


        }


    }







    public void SelectTopic(int CurrObj)
    {

        //print("cur......." + CurrObj);

        ExplodeSlider.value = 0;
        for (int j = 0; j < Labels.Length; j++)
        {

            if (Parts[j].GetComponent<Animator>())
            {
                for (int k = 0; k < Parts.Length; k++)
                {
                    if (Parts[j].name.Contains(Parts[k].name))
                    {
                        Parts[j].transform.parent = MainObject.transform.Find("PartsToAdd").transform;

                    }
                }

            }

            Animator[] PartsAnim = Parts[j].GetComponentsInChildren<Animator>();


            for (int Parts = 0; Parts < PartsAnimator.Count; Parts++)
            {
                //PartsAnimator[Parts].enabled = false;
                PartsAnimator[Parts].ResetTrigger("Play");
                PartsAnimator[Parts].SetTrigger("Reset");
            }

            Labels[j].transform.parent = MainObject.transform.Find("Labels");
            Labels[j].SetActive(true);
        }


        PartSelectnCheck = "";

        PartToActRend.Clear();
        PartsAnimator.Clear();
        Topics.Clear();
        Sub_Topics.Clear();
        Sub_Top_ToDoList.Clear();
        Sub_Top_Desc.Clear();
        Sub_Top_XP.Clear();
        ToDoList_DD.ClearOptions();

        TopicToCheck = 0;
        Total_XP = 0;
        Sub_XP_Earned = 0;

        LoadingData();



    }

    // Update is called once per frame

    public void ToggleActivateLabels(Image I_Image)
    {


        for (int Parts = 0; Parts < PartsAnimator.Count; Parts++)
        {
            //PartsAnimator[Parts].enabled = false;
            PartsAnimator[Parts].ResetTrigger("Play");
            PartsAnimator[Parts].SetTrigger("Reset");
        }

        PartSelectnCheck = "";

        if (I_Image.color == Normal_Color)
        {
            for (int i = 0; i < Parts.Length; i++)
            {
                Labels[i].SetActive(true);
                Labels[i].transform.Find("SmoothLook/Label_Desc").localScale = new Vector3(1, 0, 1);
            }
            I_Image.color = Highlight_Color;
            DissolveImage.color = Normal_Color;
            iTween.ScaleTo(ObjectivePanel, iTween.Hash("y", 0, "time", 1, "easetype", iTween.EaseType.easeInOutSine));
            ExplodeValue = 0.7f;
            CancelInvoke("InvokeExplodeOut");
            InvokeRepeating("InvokeExplodeIn", 0.1f, 0.009f);




        }
        else
        {
            for (int i = 0; i < Parts.Length; i++)
            {
                Labels[i].transform.Find("SmoothLook/Label_Desc").localScale = new Vector3(1, 0, 1);
                Labels[i].SetActive(false);
            }


            I_Image.color = Normal_Color;
            iTween.ScaleTo(ObjectivePanel, iTween.Hash("y", 0, "time", 1, "easetype", iTween.EaseType.easeInOutSine));
            CancelInvoke("InvokeExplodeIn");
            InvokeRepeating("InvokeExplodeOut", 0.1f, 0.009f);
            if (!IsAR)
                IsPartHightlight = false;
        }

        TargetObj = null;
        if (!IsAR)
        {
            CamOrbit.ChangeTarget(null);
            CamOrbit.distance = 1.5f; //CamOrbit.zoomStart;
            CamOrbit.cameraRotSide = 0;
        }
        /*if (!IsAR)
        {

            CancelInvoke("ZoomOut");
            Invoke("ZoomOut", 0.2f);
        }*/
    }



    public void InvokeExplodeIn()
    {
        if (ExplodeSlider.value < (ExplodeValue - 0.01f))
            ExplodeSlider.value += 0.01f;
        else if (ExplodeSlider.value > (ExplodeValue + 0.01f))
            ExplodeSlider.value -= 0.01f;
        else
            CancelInvoke("InvokeExplodeIn");

        ExplodeObject(ExplodeSlider.value);
    }


    public void InvokeExplodeOut()
    {
        ExplodeObject(ExplodeSlider.value);

        if (ExplodeSlider.value > 0f)
            ExplodeSlider.value -= 0.02f;
        else
        {
            CancelInvoke("InvokeExplodeOut");
            if (IsPartHightlight && !IsAR)
                IsPartHightlight = false;
        }


    }

    public void SelectPartToHighlight(GameObject PartToAct)
    {
        if (DissolveImage.color == Highlight_Color || IsClicked)
            return;




        ResetLabelImage();
        DissolveImage.color = Normal_Color;

        for (int i = 0; i < Parts.Length; i++)
        {
            Labels[i].transform.Find("SmoothLook/Label_Desc").localScale = new Vector3(1, 0, 1);
            Labels[i].SetActive(false);
            /*if(Parts[i].GetComponent<MeshRenderer>())
            Parts[i].GetComponent<MeshRenderer>().material = OriginalMaterials[i];
            else
                Parts[i].GetComponentInChildren<MeshRenderer>().material = OriginalMaterials[i];*/
        }

        for (int Parts = 0; Parts < PartsAnimator.Count; Parts++)
        {
            //PartsAnimator[Parts].enabled = false;
            PartsAnimator[Parts].ResetTrigger("Play");
            PartsAnimator[Parts].SetTrigger("Reset");
        }

        for (int i = 0; i < Parts.Length; i++)
        {

            if (PartToAct == Parts[i])
            {
                Animator[] PartsAnim = Parts[i].GetComponentsInChildren<Animator>();

                for (int animpar = 0; animpar < PartsAnim.Length; animpar++)
                {
                    if (!PartsAnimator.Contains(PartsAnim[animpar]))
                        PartsAnimator.Add(PartsAnim[animpar]);
                }


                if (PartSelectnCheck != Parts[i].name)
                {
                    if (!IsTopicUnlocked)
                    {
                        //print("topictocheck....."+TopicToCheck);
                        if (Sub_Top_ToDoList[i] == Sub_Top_ToDoList[TopicToCheck])
                        {
                            //Debug.Log("in if.....");
                            Labels[i].SetActive(true);
                            iTween.ScaleTo(Labels[i].transform.Find("SmoothLook/Label_Desc").gameObject, iTween.Hash("y", 1, "delay", 0.2f, "time", 0.5f, "easetype", iTween.EaseType.spring));
                            ExplodeValue = 0.35f;
                            PartSelectnCheck = Parts[i].name;
                            //CancelInvoke("InvokeExplodeOut");
                            //InvokeRepeating("InvokeExplodeIn", 0.1f, 0.015f);

                            IsPartHightlight = true;

                            if (!IsAR)
                            {


                                CamOrbit.distance = 0.7f;
                                TargetObj = Parts[i];
                                CamOrbit.ChangeTarget(TargetObj.transform);

                                relativePos = Parts[i].transform.localPosition - CamOrbit.transform.position;
                                //print("rela......."+relativePos+"......localpos......"+ Parts[i].transform.localPosition);
                                //print(Parts[i].transform.localPosition + ".........Dist......" + Vector3.Distance(Parts[i].transform.position, CamOrbit.target.position));
                                IsUIZooming = true;
                                UIZoomOut();

                                CancelInvoke("ZoomIn");
                                if (ExplodeSlider.value <= 0.1f || ExplodeSlider.value > (ExplodeValue + 0.01f))
                                    Invoke("ZoomIn", 0.2f);
                                else
                                    Invoke("ZoomIn", 0f);


                            }
                            if (TopicToCheck == 4)
                            {
                                Parts[4].GetComponentInChildren<SphereCollider>().enabled = false;
                                Parts[4].GetComponentInChildren<Renderer>().material.color = new Color(1, 0.05f, 0.004f, 0.4f);
                            }

                            XP_Effect.transform.position = Camera.main.WorldToScreenPoint(Parts[i].transform.position);

                            XP_Effect.text = "+" + Sub_Top_XP[TopicToCheck] + "XP";

                            XP_Effect.transform.localScale = new Vector3(2, 0, 2);

                            iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 2, "y", 2, "z", 2, "time", 0.5f, "easetype", iTween.EaseType.easeOutElastic));

                            iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", XP_Effect.transform.position.x, "y", XP_Effect.transform.position.y + 100, "z", 0, "delay", 0.3f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

                            iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", 220, "y", XP_ToShow.transform.position.y, "z", 0, "delay", 1f, "time", 0.7f, "easetype", iTween.EaseType.easeInSine, "oncomplete", "Collect_XP", "oncompletetarget", this.gameObject));

                            Camera.main.GetComponent<PhysicsRaycaster>().enabled = false;
                            //Collect_XP();



                            if (PartsAnim.Length > 0)
                            {
                                PartsAnim[0].ResetTrigger("Reset");
                                PartsAnim[0].SetTrigger("Play");
                            }
                        }
                        else if (TopicToCheck <= (Sub_Topics.Count - 1))
                        {
                            //InstructionPanel.transform.Find("InstructionText").GetComponent<Text>().text = "This is not " + Sub_Topics[TopicToCheck];
                            //iTween.ScaleTo(InstructionPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 1f, "easetype", iTween.EaseType.spring, "oncomplete", "CloseInstructionPanel", "oncompletetarget", this.gameObject, "oncompleteparams", InstructionPanel));
                            //GlassPanel.transform.localScale = Vector3.one;
                            //ObjectivePanel.transform.localScale = new Vector3(1, 0, 1);
                            ShowInstruction_Panel("This is not " + Sub_Topics[TopicToCheck]);


                            if (!IsAR)
                            {
                                CamOrbit.Target_Zoom = null;
                                CamOrbit.distance = CamOrbit.zoomStart + 0.01f;
                            }
                            //ObjectivePanel.transform.Find("ObjectiveText").GetComponent<Text>().text = "This is not " + Sub_Topics[TopicToCheck];
                        }
                    }
                    else
                    {
                        Labels[i].SetActive(true);
                        iTween.ScaleTo(Labels[i].transform.Find("SmoothLook/Label_Desc").gameObject, iTween.Hash("y", 1, "delay", 0.2f, "time", 0.5f, "easetype", iTween.EaseType.spring));
                        ExplodeValue = 0.35f;
                        PartSelectnCheck = Parts[i].name;
                        CancelInvoke("InvokeExplodeOut");
                        InvokeRepeating("InvokeExplodeIn", 0.1f, 0.015f);

                        IsPartHightlight = true;

                        if (!IsAR)
                        {

                            CamOrbit.distance = 0.7f;
                            TargetObj = Parts[i];
                            CamOrbit.ChangeTarget(TargetObj.transform);
                            IsUIZooming = false;
                            UIZoomOut();
                            relativePos = Parts[i].transform.localPosition - CamOrbit.transform.position;
                            print("rela......." + relativePos + "......localpos......" + Parts[i].transform.localPosition);
                            //print(Parts[i].transform.localPosition + ".........Dist......" + Vector3.Distance(Parts[i].transform.position, CamOrbit.target.position));
                            //LerpTime = (Vector3.Distance(Parts[i].transform.position, CamOrbit.target.position)) / 0.05f;

                            CancelInvoke("ZoomIn");
                            if (ExplodeSlider.value <= 0.1f || ExplodeSlider.value > (ExplodeValue + 0.01f))
                                Invoke("ZoomIn", 0.2f);
                            else
                                Invoke("ZoomIn", 0f);


                        }


                        if (PartsAnim.Length > 0)
                        {
                            PartsAnim[0].ResetTrigger("Reset");
                            PartsAnim[0].SetTrigger("Play");
                        }
                    }
                }
                else
                {
                    if (!IsTopicUnlocked)
                    {
                        if (i != TopicToCheck)
                        {

                            PartSelectnCheck = "";
                            //InstructionPanel.transform.Find("InstructionText").GetComponent<Text>().text = "This is not " + Sub_Topics[TopicToCheck];
                            //GlassPanel.transform.localScale = Vector3.one;
                            //iTween.ScaleTo(InstructionPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 1f, "easetype", iTween.EaseType.spring, "oncomplete", "CloseInstructionPanel", "oncompletetarget", this.gameObject, "oncompleteparams", InstructionPanel));
                            //ObjectivePanel.transform.localScale = new Vector3(1, 0, 1);
                            ShowInstruction_Panel("This is not " + Sub_Topics[TopicToCheck]);


                            if (!IsAR)
                            {
                                CamOrbit.Target_Zoom = null;
                                CamOrbit.distance = CamOrbit.zoomStart + 0.01f;
                            }

                        }
                    }
                    else
                    {
                        Labels[i].SetActive(false);
                        //CancelInvoke("InvokeExplodeIn");
                        //InvokeRepeating("InvokeExplodeOut", 0.1f, 0.015f);
                        PartSelectnCheck = "";
                        //Debug.Log("in else.....");
                        TargetObj = null;
                        if (!IsAR)
                        {
                            CamOrbit.distance = CamOrbit.zoomStart;
                            CamOrbit.ChangeTarget(null);
                        }
                        /*if (!IsAR)
                        {
                            CancelInvoke("ZoomOut");
                            Invoke("ZoomOut", 0.2f);
                        }*/

                    }


                }

            }



        }


        //print("Clicked........");
    }

    public void ShowInstruction_Panel(string TextToDisplay)
    {
        var mouse = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);



        if (mouse.x < Screen.width / 2)
        {
            // WronOptionPanel
            //print("Mouse is on left side of screen.");
            InstructionPanel.GetComponent<RectTransform>().pivot = new Vector2(0, 0);
        }



        if (mouse.x > Screen.width / 2)
        {
            //print("Mouse is on right side of screen.");
            InstructionPanel.GetComponent<RectTransform>().pivot = new Vector2(1, 0);
        }

        InstructionPanel.transform.position = Input.mousePosition;
        UIcanvas.GetComponent<GraphicRaycaster>().enabled = false;
        Camera.main.GetComponent<PhysicsRaycaster>().enabled = false;
        iTween.Stop(InstructionPanel.gameObject);

        InstructionPanel.transform.Find("InstructionText").GetComponent<Text>().text = TextToDisplay;
        //iTween.ScaleTo(GlassPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 0.01f, "easetype", iTween.EaseType.spring));
        iTween.ScaleTo(InstructionPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 0.3f, "easetype", iTween.EaseType.spring, "oncomplete", "CloseInstructionPanel", "oncompletetarget", this.gameObject));
        ObjectivePanel.transform.localScale = new Vector3(1, 0, 1);
    }

    public void SelectDissolveMode()
    {
        if (DissolveImage.color != Highlight_Color)
        {

            DissolveImage.color = Highlight_Color;
            LabelImage.color = Normal_Color;
            for (int i = 0; i < Parts.Length; i++)
            {
                Labels[i].SetActive(false);

            }
            iTween.ScaleTo(ObjectivePanel, iTween.Hash("y", 0, "time", 1, "easetype", iTween.EaseType.easeInOutSine));
            for (int Parts = 0; Parts < PartsAnimator.Count; Parts++)
            {
                //PartsAnimator[Parts].enabled = false;
                PartsAnimator[Parts].ResetTrigger("Play");
                PartsAnimator[Parts].SetTrigger("Reset");
            }
        }
        else
        {
            DissolveImage.color = Normal_Color;
        }
    }

    public void ResetLabelImage()
    {
        LabelImage.color = Normal_Color;
        //iTween.ScaleTo(ObjectivePanel, iTween.Hash("y", 0, "time", 1, "easetype", iTween.EaseType.easeInOutSine));
    }

    public void CloseInstructionPanel()
    {
        UIcanvas.GetComponent<GraphicRaycaster>().enabled = true;
        Camera.main.GetComponent<PhysicsRaycaster>().enabled = true;
        //iTween.ScaleTo(GlassPanel, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 1f, "time", 0.01f, "easetype", iTween.EaseType.linear));
        iTween.ScaleTo(InstructionPanel, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.5f, "time", 0.3f, "easetype", iTween.EaseType.spring));
        iTween.ScaleTo(ObjectivePanel, iTween.Hash("y", 1, "time", 0.3f, "delay", 0.5f, "easetype", iTween.EaseType.easeInOutSine));
    }

    public void CloseInstructionPanel_DD()
    {


        iTween.ScaleTo(InstructionPanel_DD, iTween.Hash("x", 1, "y", 0, "z", 1, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.linear));

    }



    public void SelectPartForTransparency(GameObject PartToAct)
    {
        if (DissolveImage.color == Normal_Color || LabelImage.color == Highlight_Color || IsClicked)
            return;

        int CurrPartCnt = 0;
        PartToActRend = new List<MeshRenderer>();

        for (int i = 0; i < Parts.Length; i++)
        {
            if (PartToAct == Parts[i])
            {
                CurrPartCnt = i;
                break;
            }

        }

        //print("PartToAct......." + PartToAct);

        //print("mat...."+PartToAct.GetComponentsInChildren<MeshRenderer>().Where(r => r.tag == "ToChange").ToArray()[0]);



        if (PartToAct.GetComponent<MeshRenderer>() != null)
        {
            PartToActRend.Add(PartToAct.GetComponent<MeshRenderer>());
        }
        else
            PartToActRend = PartToAct.transform.GetComponentsInChildren<MeshRenderer>().Where(r => r.tag == "Finish").ToList();


        for (int i = 0; i < PartToActRend.Count; i++)
        {

            //print("1111111......"+ PartToActRend[i].sharedMaterial+"....111111111...."+ OriginalMaterials[CurrPartCnt]);
            Material Mat = PartToActRend[i].material;

            if (Mat.GetFloat("_Mode") == 0)
            {
                //PartToActRend[i].material = TransparentMaterial;// TransparentMaterials[CurrPartCnt];
                Mat.shader = Shader.Find("Standard");
                SetupMaterialWithBlendMode(Mat, BlendMode.Transparent);
                //Mat.SetFloat("_Metallic", 0.05f);
                Mat.SetFloat("_Glossiness", 0.6f);
                //Mat.color = new Color(Mat.color.r, Mat.color.g, Mat.color.b, Mat.color.a-0.7f);
            }
            else
            {
                //PartToActRend[i].material = OriginalMaterials[CurrPartCnt];
                Mat.shader = Shader.Find("Standard");
                SetupMaterialWithBlendMode(Mat, BlendMode.Opaque);
                //Mat.SetFloat("_Metallic", 0.3f);
                Mat.SetFloat("_Glossiness", 0.25f);
                //Mat.color = new Color(Mat.color.r, Mat.color.g, Mat.color.b, 1);
            }
        }

    }




    public enum BlendMode
    {
        Opaque,
        Cutout,
        Fade,   // Old school alpha-blending mode, fresnel does not affect amount of transparency
        Transparent // Physically plausible transparency mode, implemented as alpha pre-multiply
    }

    public static void SetupMaterialWithBlendMode(Material material, BlendMode blendMode)
    {
        switch (blendMode)
        {
            case BlendMode.Opaque:
                //material.SetOverrideTag("RenderType", "");
                material.SetFloat("_Mode", 0);
                material.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
                material.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.Zero);
                material.SetInt("_ZWrite", 1);
                material.DisableKeyword("_ALPHATEST_ON");
                material.DisableKeyword("_ALPHABLEND_ON");
                material.DisableKeyword("_ALPHAPREMULTIPLY_ON");
                material.renderQueue = -1;
                break;
            case BlendMode.Cutout:
                //material.SetOverrideTag("RenderType", "TransparentCutout");
                material.SetFloat("_Mode", 1);
                material.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
                material.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.Zero);
                material.SetInt("_ZWrite", 1);
                material.EnableKeyword("_ALPHATEST_ON");
                material.DisableKeyword("_ALPHABLEND_ON");
                material.DisableKeyword("_ALPHAPREMULTIPLY_ON");
                material.renderQueue = (int)UnityEngine.Rendering.RenderQueue.AlphaTest;
                break;
            case BlendMode.Fade:
                //material.SetOverrideTag("RenderType", "Transparent");
                material.SetFloat("_Mode", 2);
                material.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
                material.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
                material.SetInt("_ZWrite", 0);
                material.DisableKeyword("_ALPHATEST_ON");
                material.EnableKeyword("_ALPHABLEND_ON");
                material.DisableKeyword("_ALPHAPREMULTIPLY_ON");
                material.renderQueue = (int)UnityEngine.Rendering.RenderQueue.Transparent;
                break;
            case BlendMode.Transparent:
                //material.SetOverrideTag("RenderType", "Transparent");
                material.SetFloat("_Mode", 3);
                material.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
                material.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
                material.SetInt("_ZWrite", 0);
                material.DisableKeyword("_ALPHATEST_ON");
                material.DisableKeyword("_ALPHABLEND_ON");
                material.EnableKeyword("_ALPHAPREMULTIPLY_ON");
                material.renderQueue = (int)UnityEngine.Rendering.RenderQueue.Transparent;
                break;
        }
    }

    Vector3 dist;
    public void ExplodeObject(float value)
    {
        //print("slidervalue........"+value);

        for (int i = 0; i < Parts.Length; i++)
        {
            if (!Parts[i].GetComponent<Animator>())
            {
                dist = Parts[i].transform.localPosition - Vector3.zero;
                Parts[i].transform.localPosition = OriginalPositions[i] + (dist * value * 0.15f);
            }
        }

        for (int j = 0; j < PartsForTransparency.Length; j++)
        {
            dist = PartsForTransparency[j].transform.localPosition - Vector3.zero;
            PartsForTransparency[j].transform.localPosition = OrgPosForTransparent[j] + (dist * value * 0.15f);
        }

        /*if(TargetObj)
        CamOrbit.ChangeTarget(TargetObj.transform);
        //CamOrbit.distance = CamOrbit.zoomStart;
        if (!IsPartHightlight)
        {

            TargetPos = new Vector3(0, MidObj.transform.position.y, 0);
        }*/
    }



    public void EnableOrbit()
    {
        //if (Camera.main.GetComponent<OrbitControls>() != null)
        //{
        //    Camera.main.GetComponent<OrbitControls>().enabled = true;
        //}
        if (!IsAR)
        {
            CamOrbit.DisableInteraction = false;


        }
    }

    public void DisableOrbit()
    {
        //if (Camera.main.GetComponent<OrbitControls>())
        //{
        //    Camera.main.GetComponent<OrbitControls>().enabled = false;
        //}
        if (!IsAR)
        {

            CamOrbit.DisableInteraction = true;


        }
    }

    public void UpdateTargetPosition()
    {
        TargetPos = MidObj.transform.position;
    }

    public void ZoomIn()
    {

        //InvokeRepeating("ChangeTargetPos",0,0.013f);

        TargetPos = TargetObj.transform.position;

        //CamOrbit.target.position =TargetPos;

        Quaternion rotation = Quaternion.LookRotation(relativePos);

        //print("rot........." + rotation);


        //CamOrbit.cameraRotUp = rotation.x*90;
        CamOrbit.cameraRotSide = (rotation.y * Mathf.Rad2Deg);

    }

    void Update()
    {
        //print("cam........."+CamOrbit.target.position+"...........tarpos......"+TargetPos);
        /*if (!IsAR)
        {
            if (CamOrbit.target.position != TargetPos)
            {

                /*if (TargetObj != null)
                {
                    TargetPos = TargetObj.transform.position;
                    //TargetObj = null;
                }

                //print("Invoke......" + LerpTime);
                //CamOrbit.target.position = Vector3.Lerp(CamOrbit.target.position, TargetPos, LerpTime * Time.deltaTime); // Vector3.Lerp(CamOrbit.transform.position,TargetPos,2.5f*Time.deltaTime);
            }
        }*/
        if (!IsAR)
        {
            if (CamOrbit.distance < CamOrbit.zoomStart && !IsUIZooming)
            {
                UIZoomIn();
                IsUIZooming = true;
            }
            else if (CamOrbit.distance > CamOrbit.zoomStart && IsUIZooming)
            {
                UIZoomOut();
                IsUIZooming = false;
            }
        }
        /*if (Input.GetKeyDown(KeyCode.I))
            UIZoomIn();

        if (Input.GetKeyDown(KeyCode.O))
            UIZoomOut();*/
    }


    public void ZoomOut()
    {
        relativePos = new Vector3(0, 0, 0.1f);

        IsPartHightlight = false;

        TargetPos = MidObj.transform.position;



        //InvokeRepeating("ChangeTargetPos", 0, 0.013f);




        Quaternion rotation = Quaternion.LookRotation(relativePos);

        //print("rot........." + rotation);


        //CamOrbit.cameraRotUp = rotation.x*90;
        CamOrbit.cameraRotSide = (rotation.y * Mathf.Rad2Deg);

    }

}

