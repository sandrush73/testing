﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;
using System.Text.RegularExpressions;

public class vid164_gflinearmasscontroller : MonoBehaviour, IPointerDownHandler
{
    // Start is called before the first frame update
    public TextAsset CSVFile;
    public AssetBundle assetBundle;
    public Canvas C_BG;

    public orbit CamOrbit;

    public AROrbitControls ArOrbit;

    public bool IsAR;

    public GameObject MainObj, Ground;

    public int OptionUnlockedTill, CurrentTopicSelected, TopicToCheck;
    public float Sub_XP_Earned, Total_XP;

    public List<Dictionary<string, object>> CSV_data;

    public List<String> TaskList, XpList, InfoText;

    public List<GameObject> AllTaskData;

    public GameObject[] Labels;

    public Text XP_ToShow, XP_Effect, Main_XP_ToShow, localTotalXpValueText, localGainedXpValueText, ObjectiveInfoText, LGListText, IListText, AListText,
        InfoHeadingText, GF_ResultTxt, E_valueTxt, m_valtxt;

    public GameObject TaskObj, AllTaskListObj;
    public RectTransform rT;
    public int localTotalXpValue, localGaniedXpValue, MainXpValue;
    public GameObject ControlPanel, LocalXP, Main_Xp, ObjectivePanel, InformationPanel, InfoButton, MainSlider, InfoPanelBg, TasklistPanel, WronOptionPanel, Gained_XP_ArrowBut;
    public Button InfoCloseBtn, InfoContinueBtn, Gained_XP_Panel, CpCloseBtn, IncButton, DecButton;


    public TextMeshPro Tape_E_value, mu_label_txt;
    public GameObject Ball, MTape, HLine, VLine, HVLine, Dummy, ScannerObject;

    public Slider Slider_distance;

    public Vector3 tempPos;
    public int density;
    public float m, angle, E;
    public float G, r, R, pi, L;
    public bool sessionStart;

    void Start()
    {
        LoadFromAsssetBundle();

        sessionStart = false;

        MainObj = this.gameObject;

        C_BG = GameObject.Find("Canvas_BG").GetComponent<Canvas>();

        Ground = GameObject.Find("Ground");

        if (!IsAR)
        {
            CamOrbit = Camera.main.GetComponentInParent<orbit>();

            C_BG.transform.parent = Camera.main.transform;
            C_BG.GetComponentInChildren<RawImage>().enabled = true;
            C_BG.renderMode = RenderMode.ScreenSpaceCamera;

            C_BG.worldCamera = Camera.main;
            CamOrbit.target = Camera.main.transform.parent;
            CamOrbit.target.position = new Vector3(0, 0.125f, 0);
            CamOrbit.maxRotUp = 45;
            CamOrbit.minRotUp = 1;
            CamOrbit.zoomMin = 0.5f;
            CamOrbit.zoomMax = 1.5f;
            CamOrbit.zoomStart = 0.75f;
            CamOrbit.Start();
        }
        else
        {
            Ground.SetActive(false);

            C_BG.gameObject.SetActive(false);

            ArOrbit = MainObj.transform.parent.gameObject.GetComponent<AROrbitControls>();
        }

        ReadingDataFromCSVFile();
        ReadingXps();
        FindingObjects();
        TaskListCreation();
        AssigningClickEvents();
        LabelsAssignment();
        AssignInitialValues();


        LocalXpCalculation();

    }
    public void LoadFromAsssetBundle()
    {
         assetBundle = AssetBundle.LoadFromFile(PlayerPrefs.GetString("AssetBundleUrl"));

         CSVFile = assetBundle.LoadAsset(PlayerPrefs.GetString("LibraryName") + "CSV", typeof(TextAsset)) as TextAsset;       

        //CSVFile = Resources.Load("vid164_gflinearmasscontrollerCSV") as TextAsset;

        Camera.main.gameObject.AddComponent<PhysicsRaycaster>();

        Camera.main.backgroundColor = Color.black;

        GameObject TargetForCamera = GameObject.Find("Target");

        if (TargetForCamera != null)
        {
            TargetForCamera.AddComponent<orbit>();
            CamOrbit = TargetForCamera.GetComponent<orbit>();
            IsAR = false;
        }
        else
        {
            this.transform.parent.gameObject.AddComponent<AROrbitControls>();
            ArOrbit = this.transform.parent.gameObject.GetComponent<AROrbitControls>();
            IsAR = true;
        }

    }
    public int totXp, loclXp;

    public void ReadingXps()
    {

        totXp = PlayerPrefs.GetInt("TotalXP");
        loclXp = PlayerPrefs.GetInt("MaxEarnings");

        if (loclXp != 0)
        {
            OptionUnlockedTill = 0;

        }
        else
        {
            OptionUnlockedTill = 1;

        }

        TopicToCheck = 1;

        MainXpValue = totXp;

        int t = 0;
        int p = 0;

        for (int k = 0; k < TaskList.Count; k++)
        {

            p = loclXp / TaskList.Count;
            XpList.Add(p.ToString());
            t = t + p;
        }

        if (t != loclXp)
        {
            XpList[XpList.Count - 1] = (p + (loclXp - t)).ToString();
        }
    }

    public float result;

    public void GetResult()
    {
        result = ((float)(TopicToCheck - 1) / (float)(TaskList.Count)) * 100;
        Camera.main.SendMessage("UnloadActually", result);
    }
    public void ReadingDataFromCSVFile()
    {

        TaskList = new List<string>();
        XpList = new List<string>();
        InfoText = new List<string>();

        AllTaskData = new List<GameObject>();

        CSV_data = CSVReader.Read(CSVFile);


        for (var i = 0; i < CSV_data.Count; i++)
        {

            if (CSV_data[i]["TaskList"].ToString() != "" && CSV_data[i]["TaskList"].ToString() != null)
            {
                TaskList.Add(CSV_data[i]["TaskList"].ToString());

            }
            //if (CSV_data[i]["XPList"].ToString() != "" && CSV_data[i]["XPList"].ToString() != null)
            //{
            //    XpList.Add(CSV_data[i]["XPList"].ToString());

            //}

            if (CSV_data[i]["InfoText"].ToString() != "" && CSV_data[i]["InfoText"].ToString() != null)
            {
                InfoText.Add(CSV_data[i]["InfoText"].ToString());

            }
        }
    }

    public void FindingObjects()
    {

        InformationPanel = GameObject.Find("InformationPanel");

        InfoCloseBtn = GameObject.Find("InfoCloseBtn").GetComponent<Button>();

        InfoContinueBtn = GameObject.Find("InfoContinueBtn").GetComponent<Button>();

        InfoPanelBg = GameObject.Find("InfoPanelBg");

        XP_ToShow = GameObject.Find("localGainedXpValueText").GetComponent<Text>();

        XP_Effect = GameObject.Find("Effect_Text").GetComponent<Text>();

        Main_XP_ToShow = GameObject.Find("Main_XP_Value").GetComponent<Text>();

        localGainedXpValueText = GameObject.Find("localGainedXpValueText").GetComponent<Text>();

        localTotalXpValueText = GameObject.Find("GainedXP_LocalTotalXp").GetComponent<Text>();

        ObjectiveInfoText = GameObject.Find("ObjectiveInfoText").GetComponent<Text>();

        TaskObj = GameObject.Find("TaskObj");

        AllTaskListObj = GameObject.Find("AllTaskList");

        ControlPanel = GameObject.Find("ControlPanel");
        LocalXP = GameObject.Find("LocalXP");
        Main_Xp = GameObject.Find("Main_Xp");
        ObjectivePanel = GameObject.Find("ObjectivePanel");
        InformationPanel = GameObject.Find("InformationPanel");
        InfoButton = GameObject.Find("InfoButton");
        MainSlider = GameObject.Find("MainSlider");
        WronOptionPanel = GameObject.Find("WrongPanel");

        Gained_XP_Panel = GameObject.Find("Gained_XP_Panel").GetComponent<Button>();
        TasklistPanel = GameObject.Find("TasklistPanel");
        CpCloseBtn = GameObject.Find("CpCloseBtn").GetComponent<Button>();

        InfoHeadingText = GameObject.Find("InfoHeadingText").GetComponent<Text>();
        LGListText = GameObject.Find("LGListText").GetComponent<Text>();
        IListText = GameObject.Find("IListText").GetComponent<Text>();
        AListText = GameObject.Find("AListText").GetComponent<Text>();

        GF_ResultTxt = GameObject.Find("GF_ResultTxt").GetComponent<Text>();
        E_valueTxt = GameObject.Find("E_valueTxt").GetComponent<Text>();

        m_valtxt = GameObject.Find("m_valtxt").GetComponent<Text>();

        IncButton = GameObject.Find("IncButton").GetComponent<Button>();

        DecButton = GameObject.Find("DecButton").GetComponent<Button>();

        Tape_E_value = GameObject.Find("Tape_E_value").GetComponent<TextMeshPro>();

        Gained_XP_ArrowBut = GameObject.Find("Gained_XP_ArrowBut");

        mu_label_txt = GameObject.Find("mu_label_txt").GetComponent<TextMeshPro>();

        //......................................................

        Slider_distance = GameObject.Find("Slider_distance").GetComponent<Slider>();

        Ball = GameObject.Find("Ball");
        MTape = GameObject.Find("MTape");
        HLine = GameObject.Find("HLine");
        VLine = GameObject.Find("VLine");
        HVLine = GameObject.Find("HVLine");
        Dummy = GameObject.Find("Dummy");

        ScannerObject = GameObject.Find("ScannerObject");
    }


    public void AssigningClickEvents()
    {

        iTween.ScaleTo(InformationPanel.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

        InfoCloseBtn.onClick.AddListener(() => CloseInfoPanel());

        InfoContinueBtn.onClick.AddListener(() => ContinueInfoPanel());

        InfoButton.GetComponent<Button>().onClick.AddListener(() => OpenInfoPanel());

        Gained_XP_Panel.onClick.AddListener(() => OpenTaskList());

        CpCloseBtn.onClick.AddListener(() => OpenOrCloseControlPanel());

        MainSlider.GetComponent<Slider>().onValueChanged.AddListener(delegate { ChangeDistance(MainSlider.GetComponent<Slider>().value, MainSlider.GetComponentInChildren<Text>()); });

        AddListener(EventTriggerType.PointerDown, DisableOrbit, MainSlider.gameObject);

        AddListener(EventTriggerType.PointerUp, EnableOrbit, MainSlider.gameObject);

        //Slider_distance.onValueChanged.AddListener(delegate { ChangeDistance(Slider_distance.value, Slider_distance.GetComponentInChildren<Text>()); });

        //AddListener(EventTriggerType.PointerDown, DisableOrbit, Slider_distance.gameObject);

        //AddListener(EventTriggerType.PointerUp, EnableOrbit, Slider_distance.gameObject);

        AddListener(EventTriggerType.PointerUp, RadiusAddXp, MainSlider.gameObject);

        AddListener(EventTriggerType.PointerUp, DistanceAddXp, Slider_distance.gameObject);

        IncButton.GetComponent<Button>().onClick.AddListener(() => IncDecFunction(IncButton.name));

        DecButton.GetComponent<Button>().onClick.AddListener(() => IncDecFunction(DecButton.name));
    }

    public void AssignInitialValues()
    {
        if (OptionUnlockedTill > 0)
        {
            LocalXP.transform.localScale = new Vector3(0, 0, 0);
            ObjectivePanel.transform.localScale = new Vector3(0, 0, 0);
        }

        density = 1;

        pi = 3.14f;

        R = 1;

        G = 6.67f;

        r = 1f;


        InfoHeadingText.text = InfoText[0];
        LGListText.text = InfoText[1];
        IListText.text = InfoText[2];
        AListText.text = InfoText[3];


        InfoCloseBtn.gameObject.transform.localScale = new Vector3(0, 0, 0);
        localGainedXpValueText.text = "0";
        Main_XP_ToShow.text = MainXpValue + "";
        WronOptionPanel.transform.localScale = new Vector3(0, 0, 0);

        ChangeDistance(Slider_distance.value, Slider_distance.GetComponentInChildren<Text>());
        //ChangeRadius(MainSlider.GetComponent<Slider>().value, MainSlider.GetComponentInChildren<Text>());
        ScannerObject.AddComponent<GfLinearRendererOffset>();
        ScannerObject.GetComponent<GfLinearRendererOffset>().offset = -0.4f;
        ScannerObject.SetActive(false);

        // Labels[1].transform.localPosition = MidPoint(Dummy, Ball);
        Labels[0].transform.localPosition = MidPoint(Ball, MTape);
    }


    public void TaskListCreation()
    {

        rT = AllTaskListObj.transform.parent.GetComponent<RectTransform>();

        localGaniedXpValue = 0;

        for (int i = 0; i < TaskList.Count; i++)
        {

            GameObject Tl = (GameObject)Instantiate(TaskObj);
            Tl.transform.SetParent(AllTaskListObj.transform);
            Tl.transform.localScale = new Vector3(1, 1, 1);
            Tl.name = i.ToString();

            AllTaskData.Add(Tl);
            AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(false);
            AllTaskData[i].gameObject.transform.GetChild(2).gameObject.SetActive(false);
            AllTaskData[i].gameObject.transform.GetChild(3).GetComponentInChildren<Text>().text = TaskList[i];
            AllTaskData[i].gameObject.transform.GetChild(4).GetComponentInChildren<Text>().text = "+" + XpList[i] + " XP";

            localTotalXpValue += int.Parse(XpList[i]);
        }

        rT.sizeDelta = new Vector2(rT.sizeDelta.x, TaskList.Count * 50);
        localTotalXpValueText.text = "/" + localTotalXpValue.ToString();

        AllTaskData[0].gameObject.transform.GetChild(2).gameObject.SetActive(true);
        ObjectiveInfoText.text = AllTaskData[0].gameObject.transform.GetChild(3).GetComponentInChildren<Text>().text;
    }

    public void LocalXpCalculation()
    {


        for (int i = 0; i < TaskList.Count; i++)
        {
            AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(false);
            AllTaskData[i].gameObject.transform.GetChild(2).gameObject.SetActive(false);
        }

        if (OptionUnlockedTill < 1)
        {
            if ((TopicToCheck) < (TaskList.Count + 1))
            {
                localGaniedXpValue = 0;

                for (int i = 0; i < TopicToCheck - 1; i++)
                {
                    localGaniedXpValue += int.Parse(XpList[i]);
                }

                for (int i = 0; i < TopicToCheck; i++)
                {
                    AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(true);
                }

                localGainedXpValueText.text = localGaniedXpValue.ToString();

                AllTaskData[TopicToCheck - 1].gameObject.transform.GetChild(2).gameObject.SetActive(true);

                ObjectiveInfoText.text = AllTaskData[TopicToCheck - 1].gameObject.transform.GetChild(3).GetComponentInChildren<Text>().text;
            }
            else
            {
                for (int i = 0; i < TaskList.Count; i++)
                {
                    AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(true);
                    AllTaskData[i].gameObject.transform.GetChild(2).gameObject.SetActive(false);
                }

                localGainedXpValueText.text = localTotalXpValue.ToString();

                XP_Effect.text = "+" + localTotalXpValue + " XP";

                XP_Effect.transform.position = (XP_ToShow.transform.position);

                iTween.Stop(XP_Effect.gameObject);

                iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeOutElastic));

                iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", XP_Effect.transform.position.x, "y", XP_Effect.transform.position.y + 50, "z", 0, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

                iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", Main_XP_ToShow.transform.position.x, "y", Main_XP_ToShow.transform.position.y, "z", 0, "delay", 1.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

                iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "delay", 2f, "time", 0.5f, "easetype", iTween.EaseType.linear, "oncomplete", "MainXpCalculation", "oncompletetarget", this.gameObject));
            }
        }
        else
        {
            for (int i = 0; i < TaskList.Count; i++)
            {
                AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(true);
                AllTaskData[i].gameObject.transform.GetChild(2).gameObject.SetActive(false);
            }

            localGainedXpValueText.text = localTotalXpValue.ToString();
        }
    }

    public void CloseAllpanels()
    {

        ControlPanel.GetComponent<Animator>().Play("CpanelFullClose");
        LocalXP.GetComponent<Animator>().Play("LocalXpPanelClose");
        Main_Xp.GetComponent<Animator>().Play("MainXpPanelClose");
        ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelClose");
        InfoButton.GetComponent<Animator>().Play("InfoButtonClose");
        MainSlider.GetComponent<Animator>().Play("MainSliderClose");

        if (TasklistPanel.gameObject.transform.localScale.y != 0)
            iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));
    }

    public void OpenAllpanels()
    {
        if (OptionUnlockedTill < 1)
        {
            if (TopicToCheck <= (TaskList.Count))
            {
                LocalXP.GetComponent<Animator>().Play("LocalXpPanelOPen");
                ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen");
            }
        }


        ControlPanel.GetComponent<Animator>().Play("CpanelOpen");

        Main_Xp.GetComponent<Animator>().Play("MainXpPanelOpen");

        InfoButton.GetComponent<Animator>().Play("InfoButtonOpen");
        MainSlider.GetComponent<Animator>().Play("MainSliderOpen");
        iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "time", 0.2f, "easetype", iTween.EaseType.linear));
        iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));
    }

    public void AddXpEffect()
    {


        if (TopicToCheck <= (TaskList.Count))
        {
            // Camera.main.GetComponent<PhysicsRaycaster>().enabled = false;

            GameObject.Find("Canvas").GetComponent<CanvasGroup>().interactable = false;

            XP_Effect.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);

            XP_Effect.text = "+" + int.Parse(XpList[TopicToCheck - 1]) + " XP";

            XP_Effect.transform.localScale = new Vector3(1, 0, 1);

            // iTween.Stop(XP_Effect.gameObject);


            iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeOutElastic));

            iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", XP_Effect.transform.position.x, "y", XP_Effect.transform.position.y + 50, "z", 0, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

            iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", XP_ToShow.transform.position.x, "y", XP_ToShow.transform.position.y, "z", 0, "delay", 1.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

            iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "delay", 2f, "time", 0.5f, "easetype", iTween.EaseType.linear, "oncomplete", "LocalXpCalculation", "oncompletetarget", this.gameObject));

            TopicToCheck++;

            Invoke("EnableRayCast", 3);
        }

    }

    public void EnableRayCast()
    {
        //Camera.main.GetComponent<PhysicsRaycaster>().enabled = true;
        GameObject.Find("Canvas").GetComponent<CanvasGroup>().interactable = true;
    }

    public IEnumerator MainXpCalculation()
    {
        if (OptionUnlockedTill == 0)
        {
            OptionUnlockedTill = 1;

            MainXpValue += localTotalXpValue;

            Main_XP_ToShow.text = MainXpValue.ToString();

            yield return new WaitForSeconds(1f);

            LocalXP.GetComponent<Animator>().Play("LocalXpPanelClose");

            ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelClose");

            if ((ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelClose")) || (ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelFullClose")))
            {
                ControlPanel.GetComponent<Animator>().Play("CpanelOpen");
            }
        }

    }

    public void CloseInfoPanel()
    {
        sessionStart = true;
        InfoPanelBg.SetActive(false);
        iTween.ScaleTo(InformationPanel.gameObject, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));
        OpenAllpanels();
    }

    public void ContinueInfoPanel()
    {
        sessionStart = true;
        InfoPanelBg.SetActive(false);
        iTween.ScaleTo(InformationPanel.gameObject, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));
        InfoCloseBtn.transform.localScale = new Vector3(0.27f, 0.27f, 0.27f);
        InfoContinueBtn.transform.localScale = new Vector3(0, 0, 0);
        OpenAllpanels();
    }

    public void OpenInfoPanel()
    {
        sessionStart = false;
        iTween.ScaleTo(InformationPanel.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));
        InfoPanelBg.SetActive(true);
        iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "time", 0.2f, "easetype", iTween.EaseType.linear));
        iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));
        CloseAllpanels();
    }

    public void OpenTaskList()
    {


        if (TasklistPanel.gameObject.transform.localScale.y != 1)
        {
            iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.01f, "time", 0.2f, "easetype", iTween.EaseType.easeInSine));

            ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelClose");


            //Gained_XP_ArrowBut

            iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 180, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));

            if ((ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelOpen")))
            {
                ControlPanel.GetComponent<Animator>().Play("CpanelClose");
            }
        }
        else
        {
            iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "delay", 0.01f, "time", 0.2f, "easetype", iTween.EaseType.easeInSine));
            iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));

            ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen");

            ControlPanel.GetComponent<Animator>().Play("CpanelOpen");
        }
    }

    public void OpenOrCloseControlPanel()
    {

        if ((ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelClose")) || (ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelFullClose")))
        {
            ControlPanel.GetComponent<Animator>().Play("CpanelOpen");
        }
        else
        {
            ControlPanel.GetComponent<Animator>().Play("CpanelClose");
        }
    }

    public void WrongOptionSelected()
    {

        WronOptionPanel.transform.localScale = new Vector3(0, 0, 0);

        iTween.Stop(WronOptionPanel.gameObject);

        iTween.ScaleTo(WronOptionPanel.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.01f, "time", 0.2f, "easetype", iTween.EaseType.easeInSine));

        iTween.ScaleTo(WronOptionPanel.gameObject, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 1.5f, "time", 0.2f, "easetype", iTween.EaseType.easeInSine));

        // InfoPanelBg.SetActive(true);

        Invoke("CloseBlurBg", 1.5f);

        GameObject.Find("Canvas").GetComponent<CanvasGroup>().interactable = false;

        ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelClose");


        var mouse = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);

        if (mouse.x < Screen.width / 2)
        {
            // WronOptionPanel
            //print("Mouse is on left side of screen.");
            WronOptionPanel.GetComponent<RectTransform>().pivot = new Vector2(0, 0);
        }

        if (mouse.x > Screen.width / 2)
        {
            //print("Mouse is on right side of screen.");
            WronOptionPanel.GetComponent<RectTransform>().pivot = new Vector2(1, 0);
        }


        WronOptionPanel.transform.position = Input.mousePosition;

    }

    public void CloseBlurBg()
    {
        InfoPanelBg.SetActive(false);
        GameObject.Find("Canvas").GetComponent<CanvasGroup>().interactable = true;


        if (OptionUnlockedTill == 0)
        {
            if (TopicToCheck <= (TaskList.Count))
            {
                if (InformationPanel.gameObject.transform.localScale.x == 0f)
                {
                    //ObjectivePanel.GetComponent<Animator>().Play("");
                    ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen", 0, 0f);
                }
            }
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (OptionUnlockedTill == 0)
        {

            if (InformationPanel.gameObject.transform.localScale.x != 1 && sessionStart)
            {
                if (TopicToCheck <= (TaskList.Count))
                {
                    iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "time", 0.2f, "easetype", iTween.EaseType.linear));
                    ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen");
                    iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));

                    ControlPanel.GetComponent<Animator>().Play("CpanelOpen");
                }
            }
        }

    }

    public void LabelsAssignment()
    {
        GameObject Label = GameObject.Find("Labels");

        Labels = new GameObject[Label.transform.childCount];

        for (int j = 0; j < Labels.Length; j++)
        {
            Labels[j] = Label.transform.GetChild(j).gameObject;

            GameObject TempLabels = Labels[j];

            foreach (Transform ChildLabel in TempLabels.transform)
            {
                if (ChildLabel.name.Contains("SmoothLook"))
                {
                    ChildLabel.gameObject.AddComponent<SmoothLook>();
                    // ChildLabel.gameObject.AddComponent<ScaleRelativeToCamera>();

                }
            }
        }

        for (int k = 0; k < Labels.Length; k++)
        {
            string name = Labels[k].name.Remove(Labels[k].name.Length - 6);

            if (GameObject.Find(name + "") != null)
            {
                GameObject dummy = GameObject.Find(name + "").gameObject;

                Labels[k].transform.parent = dummy.transform;
                Labels[k].transform.rotation = Quaternion.identity;
            }


            //TempTextMesh = Labels[k].GetComponentsInChildren<TextMeshPro>();

            //if (TempTextMesh.Length > 1)
            //{
            //    for (int p = 0; p < TempTextMesh.Length; p++)
            //    {
            //        if (DescriTextDoc.ContainsKey(name + "_" + p))
            //            TempTextMesh[p].text = DescriTextDoc[name + "_" + p];
            //    }
            //}
            //else
            //{
            //    if (DescriTextDoc.ContainsKey(name))
            //        Labels[k].GetComponentInChildren<TextMeshPro>().text = DescriTextDoc[name];
            //}

        }


    }

    public void AddListener(EventTriggerType eventType, Action MethodToCall, GameObject TriggerObjToAdd)
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall());
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }

    public void EnableOrbit()
    {
        if (!IsAR)
        {
            CamOrbit.DisableInteration = false;


        }
        else
        {
            ArOrbit.DisableInteration = false;
        }
        if (OptionUnlockedTill < 1)
        {
            if (InformationPanel.gameObject.transform.localScale.x != 1)
            {
                if (TopicToCheck <= (TaskList.Count))
                {
                    iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "time", 0.2f, "easetype", iTween.EaseType.linear));
                    iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));
                    ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen");
                }
            }
        }

    }

    public void RadiusAddXp()
    {
        if (OptionUnlockedTill < 1)
        {
            if ((TopicToCheck <= (TaskList.Count)) && (InformationPanel.transform.localScale.y == 0))
            {
                if (TopicToCheck == 2)
                {
                    AddXpEffect();
                }
                else
                {
                    WrongOptionSelected();
                }
            }
        }
    }

    public void DistanceAddXp()
    {
        if (OptionUnlockedTill < 1)
        {
            if ((TopicToCheck <= (TaskList.Count)) && (InformationPanel.transform.localScale.y == 0))
            {
                if (TopicToCheck == 3)
                {
                    AddXpEffect();
                }
                else
                {
                    WrongOptionSelected();
                }
            }
        }
    }

    public void DisableOrbit()
    {
        if (!IsAR)
        {

            CamOrbit.DisableInteration = true;


        }
        else
        {
            ArOrbit.DisableInteration = true;
        }

    }

    public void AddListener(EventTriggerType eventType, Action<float> MethodToCall, GameObject TriggerObjToAdd)
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall(TriggerObjToAdd.GetComponent<Slider>().value));
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }

    public Vector3 MidPoint(GameObject obj1, GameObject obj2)
    {
        Vector3 midp = (obj1.transform.localPosition + obj2.transform.localPosition) / 2;
        return midp;
    }

    public float Scale(GameObject obj1, GameObject obj2)
    {
        float scl = Vector3.Distance(obj1.transform.localPosition, obj2.transform.localPosition);
        return scl;
    }

    public void ChangeRadius(float val, Text txt)
    {

        //txt.text = val.ToString("F2")+" m";

        //Ball.transform.localScale = new Vector3(2*val, 2*val, 2*val);

        //VLine.transform.localScale = new Vector3(val*2, VLine.transform.localScale.y, VLine.transform.localScale.z);

        //tempPos = new Vector3(0, 0.5f+val*1f, 0);

        //Labels[1].transform.GetChild(0).GetComponentInChildren<TextMeshPro>().text = "a = " + txt.text;

        //Labels[1].transform.localPosition = MidPoint(Dummy, Ball);



    }

    public void ChangeDistance(float val, Text txt)
    {
        txt.text = val.ToString("F2") + " m";

        MTape.transform.localPosition = new Vector3(val, MTape.transform.localPosition.y, MTape.transform.localPosition.z);

        HLine.transform.localPosition = new Vector3(val, HLine.transform.localPosition.y, HLine.transform.localPosition.z);

        HLine.transform.localScale = new Vector3(Scale(HLine, Ball), HLine.transform.localScale.y, HLine.transform.localScale.z);

        //HVLine.transform.localPosition = new Vector3(val, HVLine.transform.localPosition.y, HVLine.transform.localPosition.z);

        Labels[0].transform.GetChild(0).GetComponentInChildren<TextMeshPro>().text = "A = " + txt.text;

        Labels[0].transform.localPosition = MidPoint(Ball, MTape);


        //a_label_txt.text = "r = " + txt.text;
    }
    public void IncDecFunction(string nam)
    {
        ScannerObject.SetActive(false);
        if (nam == "IncButton")
        {
            if (density < 5f)
            {
                density = density + 1;

                ScannerObject.SetActive(true);
                if (OptionUnlockedTill < 1)
                {
                    if (TopicToCheck <= (TaskList.Count))
                    {
                        if (TopicToCheck == 1)
                        {
                            AddXpEffect();
                        }
                        else
                        {
                            WrongOptionSelected();
                        }
                    }
                }

            }
        }
        else if (nam == "DecButton")
        {
            if (density > 1f)
            {
                density = density - 1;
                ScannerObject.SetActive(true);
            }
            if (OptionUnlockedTill < 1)
            {
                if (TopicToCheck <= (TaskList.Count))
                {
                    WrongOptionSelected();
                }
            }
        }

        mu_label_txt.text = density.ToString();
        m_valtxt.text = density.ToString();
        CancelInvoke("StopScanner");
        Invoke("StopScanner", 2f);

    }
    public void StopScanner()
    {
        ScannerObject.SetActive(false);
    }

    public void Update()
    {
        float cnst = 0;

        // Dummy.transform.localPosition = tempPos;

        cnst = G * density * (4 * G * density * 1 / 4 * Mathf.Sqrt(MainSlider.GetComponent<Slider>().value)) - 1;

        E_valueTxt.text = "E = " + cnst.ToString("F2");
        Tape_E_value.text = "" + cnst.ToString("F2");
    }
}
public class GfLinearRendererOffset : MonoBehaviour
{

    float scrollSpeed = 0.5f;
    Renderer rend;
    public float offset = -0.5f;
    void Start()
    {

        rend = GetComponent<Renderer>();
        //Init = rend.materials[0].GetTextureOffset("_MainTex");
        //offset = -0.31f;

        rend.materials[0].SetTextureOffset("_MainTex", new Vector2(0, offset));
    }

    void Update()
    {
        offset += Time.deltaTime * scrollSpeed;
        rend.materials[0].SetTextureOffset("_MainTex", new Vector2(0, offset));
    }



}