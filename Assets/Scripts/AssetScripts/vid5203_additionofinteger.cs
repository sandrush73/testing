﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;
using System.Text.RegularExpressions;

public class vid5203_additionofinteger : MonoBehaviour, IPointerDownHandler
{
    // Start is called before the first frame update
    public static vid5203_additionofinteger Instance;

    public TextAsset CSVFile;
    public Material TransparentMaterial;
    public AssetBundle assetBundle;
    public Canvas C_BG;

    public orbit CamOrbit;

    public AROrbitControls ArOrbit;

    public bool IsAR;

    public GameObject MainObj, Ground;

    public int OptionUnlockedTill, CurrentTopicSelected, TopicToCheck;
    public float Sub_XP_Earned, Total_XP;

    public List<Dictionary<string, object>> CSV_data;

    public List<String> TaskList, XpList, InfoText;

    public List<GameObject> AllTaskData;

    public GameObject[] Labels;

    public Text XP_ToShow, XP_Effect, Main_XP_ToShow, localTotalXpValueText, localGainedXpValueText, ObjectiveInfoText, LGListText, IListText, AListText,
        InfoHeadingText, GF_ResultTxt, E_valueTxt, m_valtxt;

    public GameObject TaskObj, AllTaskListObj;
    public RectTransform rT;
    public int localTotalXpValue, localGaniedXpValue, MainXpValue;
    public GameObject ControlPanel, LocalXP, Main_Xp, ObjectivePanel, InformationPanel, InfoButton, MainSlider, InfoPanelBg, TasklistPanel, WronOptionPanel, Gained_XP_ArrowBut, PopUpPanel;
    public Button InfoCloseBtn, InfoContinueBtn, Gained_XP_Panel, CpCloseBtn, IncButton, DecButton;
    public bool sessionStart, label_onbool;

    /// <summary>
    /// Topic variable
    /// </summary>
    public GameObject Parentcube1, Parentcube2, clone_obj1, clone_obj2, Pos_button, Neg_button, total_label, flylabel, bot_obj, submit_button, buttongroup, but_add_a, but_add_b, but_pick_a, but_pick_b, but_clear, but_dummy,addend_a,addend_b,but_droptoken;
    public int pos_value, neg_value, res_value;
    public List<GameObject> green_obj = new List<GameObject>();
    public List<GameObject> red_obj = new List<GameObject>();
    public bool collision_one;
    public float y_pos, sel_addend_side;
    public int n_value, ran_x, ran_y, ran_res,dummy_res,dummy_x,dummy_y,ran_que,pick_cnt;
    public Rect UIcanvas;
    public GameObject obj1, obj2;
    //public Slider SliderMass1, SliderMass2;
    //public float g, a, t, y1, y2, w1, w2;
    //public static float mass1, mass2, mass3;
    //public GameObject Weight1, Weight2, Weight3, Right_weight, weight1label, weight2label, weight3label, LenghtObj1, LenghtObj2, LenghtObj3, LenghtObj4, Pulley, Pulley2, Shadow1, Shadow2, Arrow, infoPanel,
    //    collider_w2, collider_w3, que_1, que_2, que_3, que1_txt, que2_txt, que3_txt, que2_ans, que3_ans1, que3_ans2, que3_ans3, right_obj,but_mass1, but_mass2, but_mass3,label_accelaration;

    //public Button StartBut, infopanelClose, infopanelOpen, ReadyBut;
    //private Color color1, color2, color3;
    //private Vector3 Weight1Pos, Weight2Pos;

    //public Text Mass1Text, Mass2Text, a_value;
    //public int[] weight_value = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
    //public float[] collider_pos = new float[] { -0.000107f, -0.000198f, -0.000295f, -0.000388f, -0.000481f, -0.000569f, -0.000664f, -0.000755f, -0.000843f, -0.000935f };
    //public float[] weight_d_leftlimit = new float[] { 0.0115f, 0.0207f, 0.0292f, 0.039f, 0.0482f, 0.0574f, 0.0672f, 0.0764f, 0.0856f, 0.0954f };
    //public float[] weight_d_limit = new float[] { -0.517f, -0.527f, -0.537f, -0.547f, -0.554f, -0.565f, -0.573f, -0.584f, -0.592f, -0.602f };
    //public float w0_limit_down_value, w1_limit_down_value, w2_limit_down_value;
    //public float a1, a2, a3, a4;
    //public static bool leftup_move;
    //public string sel_direction;

    void Start()
    {
        collision_one = false;
        Instance = this;
        pos_value = 0;
        neg_value = 0;
        res_value = 0;
        LoadFromAsssetBundle();

        sessionStart = false;
        //OptionUnlockedTill = 0;
        //TopicToCheck = 1;
        //MainXpValue = 10;
        label_onbool = false;
        sel_addend_side = -0.5f;
        MainObj = this.gameObject;

        C_BG = GameObject.Find("Canvas_BG").GetComponent<Canvas>();

        Ground = GameObject.Find("Ground");

        if (!IsAR)
        {
            CamOrbit = Camera.main.GetComponentInParent<orbit>();

            C_BG.transform.parent = Camera.main.transform;
            C_BG.GetComponentInChildren<RawImage>().enabled = true;
            C_BG.renderMode = RenderMode.ScreenSpaceCamera;
            // C_BG.gameObject.SetActive(false);
            C_BG.worldCamera = Camera.main;

            CamOrbit.target = Camera.main.transform.parent;
            CamOrbit.target.position = new Vector3(0, 0.5f, 0);

            CamOrbit.maxRotUp = 45;
            CamOrbit.minRotUp = 1;
            CamOrbit.zoomMin = 1.5f;
            CamOrbit.zoomMax = 4.5f;
            CamOrbit.zoomStart = 3.9f;
            CamOrbit.cameraRotSideStart = 180;
            CamOrbit.cameraRotUpStart = 0;
            CamOrbit.Start();
        }
        else
        {
            Ground.SetActive(false);

            C_BG.gameObject.SetActive(false);

            ArOrbit = MainObj.transform.parent.gameObject.GetComponent<AROrbitControls>();
        }

        ReadingDataFromCSVFile();
        ReadingXps();
        FindingObjects();
        TaskListCreation();
        AssigningClickEvents();
        //  LabelsAssignment();
        AssignInitialValues();


        LocalXpCalculation();


    }

    public void LoadFromAsssetBundle()
    {

        // if (Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.Android)
        // {

        assetBundle = AssetBundle.LoadFromFile(PlayerPrefs.GetString("AssetBundleUrl"));

        CSVFile = assetBundle.LoadAsset(PlayerPrefs.GetString("LibraryName") + "CSV", typeof(TextAsset)) as TextAsset;

        TransparentMaterial = assetBundle.LoadAsset("TransparentMaterial", typeof(Material)) as Material;
        TransparentMaterial.shader = Shader.Find("Standard");
        clone_obj1 = assetBundle.LoadAsset("CubeGreen", typeof(GameObject)) as GameObject;
        clone_obj2 = assetBundle.LoadAsset("CubeRed", typeof(GameObject)) as GameObject;
        // }

        //string CSVName = transform.parent.name;
        //if (CSVName.Contains("Clone"))
        //{

        //    CSVName = CSVName.Substring(0, CSVName.Length - 7);
        //}
        //print("casvname........." + CSVName);
        //CSVFile = Resources.Load(CSVName + "CSV") as TextAsset;



        Camera.main.gameObject.AddComponent<PhysicsRaycaster>();

        Camera.main.backgroundColor = Color.black;

        GameObject TargetForCamera = GameObject.Find("Target");

        if (TargetForCamera != null)
        {
            TargetForCamera.AddComponent<orbit>();
            CamOrbit = TargetForCamera.GetComponent<orbit>();
            IsAR = false;
        }
        else
        {
            this.transform.parent.gameObject.AddComponent<AROrbitControls>();
            ArOrbit = this.transform.parent.gameObject.GetComponent<AROrbitControls>();
            IsAR = true;
        }

    }

    public void ReadingDataFromCSVFile()
    {

        TaskList = new List<string>();
        XpList = new List<string>();
        InfoText = new List<string>();

        AllTaskData = new List<GameObject>();

        CSV_data = CSVReader.Read(CSVFile);


        for (var i = 0; i < CSV_data.Count; i++)
        {

            if (CSV_data[i]["TaskList"].ToString() != "" && CSV_data[i]["TaskList"].ToString() != null)
            {
                TaskList.Add(CSV_data[i]["TaskList"].ToString());

            }
            //if (CSV_data[i]["XPList"].ToString() != "" && CSV_data[i]["XPList"].ToString() != null)
            //{
            //    XpList.Add(CSV_data[i]["XPList"].ToString());

            //}

            if (CSV_data[i]["InfoText"].ToString() != "" && CSV_data[i]["InfoText"].ToString() != null)
            {
                InfoText.Add(CSV_data[i]["InfoText"].ToString());

            }
        }
    }

    public int totXp, loclXp;

    public void ReadingXps()
    {

      ///  PlayerPrefs.SetInt("TotalXP", 0);
      //  PlayerPrefs.SetInt("MaxEarnings", 0);

        totXp = PlayerPrefs.GetInt("TotalXP");
        loclXp = PlayerPrefs.GetInt("MaxEarnings");

        if (loclXp != 0)
        {
            OptionUnlockedTill = 0;

        }
        else
        {
            OptionUnlockedTill = 1;

        }

        TopicToCheck = 1;

        MainXpValue = totXp;

        int t = 0;
        int p = 0;

        for (int k = 0; k < TaskList.Count; k++)
        {

            p = loclXp / TaskList.Count;
            XpList.Add(p.ToString());
            t = t + p;
        }

        if (t != loclXp)
        {
            XpList[XpList.Count - 1] = (p + (loclXp - t)).ToString();
        }
    }

    public float result;

    public void GetResult()
    {
        result = ((float)(TopicToCheck - 1) / (float)(TaskList.Count)) * 100;
        Camera.main.SendMessage("UnloadActually", result);
    }

    public void FindingObjects() {

        InformationPanel = GameObject.Find("InformationPanel");

        InfoCloseBtn = GameObject.Find("InfoCloseBtn").GetComponent<Button>();

        InfoContinueBtn = GameObject.Find("InfoContinueBtn").GetComponent<Button>();

        InfoPanelBg = GameObject.Find("InfoPanelBg");

        XP_ToShow = GameObject.Find("localGainedXpValueText").GetComponent<Text>();

        XP_Effect = GameObject.Find("Effect_Text").GetComponent<Text>();

        Main_XP_ToShow = GameObject.Find("Main_XP_Value").GetComponent<Text>();

        localGainedXpValueText = GameObject.Find("localGainedXpValueText").GetComponent<Text>();

        localTotalXpValueText = GameObject.Find("GainedXP_LocalTotalXp").GetComponent<Text>();

        ObjectiveInfoText = GameObject.Find("ObjectiveInfoText").GetComponent<Text>();

        TaskObj = GameObject.Find("TaskObj");

        AllTaskListObj = GameObject.Find("AllTaskList");

        ControlPanel = GameObject.Find("ControlPanel");
        LocalXP = GameObject.Find("LocalXP");
        Main_Xp = GameObject.Find("Main_Xp");
        ObjectivePanel = GameObject.Find("ObjectivePanel");
        InformationPanel = GameObject.Find("InformationPanel");
        InfoButton = GameObject.Find("InfoButton");
        MainSlider = GameObject.Find("MainSlider");
        WronOptionPanel = GameObject.Find("WrongPanel");

        Gained_XP_Panel = GameObject.Find("Gained_XP_Panel").GetComponent<Button>();
        TasklistPanel = GameObject.Find("TasklistPanel");
        CpCloseBtn = GameObject.Find("CpCloseBtn").GetComponent<Button>();

        InfoHeadingText = GameObject.Find("InfoHeadingText").GetComponent<Text>();
        LGListText = GameObject.Find("LGListText").GetComponent<Text>();
        IListText = GameObject.Find("IListText").GetComponent<Text>();
        AListText = GameObject.Find("AListText").GetComponent<Text>();
        PopUpPanel = GameObject.Find("PopUpPanel");


        Gained_XP_ArrowBut = GameObject.Find("Gained_XP_ArrowBut");

        Parentcube1 = GameObject.Find("PossituveCubes");
        Parentcube2 = GameObject.Find("NegativeCubes");
        Pos_button = GameObject.Find("PositiveButton");
        Neg_button = GameObject.Find("NegativeButton");
        total_label = GameObject.Find("Total");
        flylabel = GameObject.Find("Flyingtext");
        bot_obj = GameObject.Find("Bot");
        submit_button = GameObject.Find("SubmitButton");
        buttongroup = GameObject.Find("ButtonGroup");
        UIcanvas = GameObject.Find("Canvas").GetComponent<RectTransform>().rect;
        Pos_button.transform.GetChild(0).GetComponent<Text>().text = pos_value.ToString();
        Neg_button.transform.GetChild(0).GetComponent<Text>().text = neg_value.ToString();
        but_add_a = GameObject.Find("Addend a");
        but_add_b = GameObject.Find("Addend b");
        but_pick_a = GameObject.Find("pick+1");
        but_pick_b = GameObject.Find("pick-1");
        but_clear = GameObject.Find("ResetButton");
        but_dummy = GameObject.Find("Dontdelete");
        addend_a = GameObject.Find("addend a");
        addend_b = GameObject.Find("addend b");
        but_droptoken = GameObject.Find("DropToken");
    }


    public void AssigningClickEvents() {

        iTween.ScaleTo(InformationPanel.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

        InfoCloseBtn.onClick.AddListener(() => CloseInfoPanel());

        InfoContinueBtn.onClick.AddListener(() => ContinueInfoPanel());

        InfoButton.GetComponent<Button>().onClick.AddListener(() => OpenInfoPanel());

        InfoButton.transform.GetChild(0).GetComponent<Button>().onClick.AddListener(() => Labels_on());
        Gained_XP_Panel.onClick.AddListener(() => OpenTaskList());

        CpCloseBtn.onClick.AddListener(() => OpenOrCloseControlPanel());

        //  MainSlider.GetComponent<Slider>().onValueChanged.AddListener(delegate { ChangeDistance(MainSlider.GetComponent<Slider>().value, MainSlider.GetComponentInChildren<Text>()); });

        AddListener(EventTriggerType.PointerDown, DisableOrbit, MainSlider.gameObject);

        AddListener(EventTriggerType.PointerUp, EnableOrbit, MainSlider.gameObject);

        //Slider_distance.onValueChanged.AddListener(delegate { ChangeDistance(Slider_distance.value, Slider_distance.GetComponentInChildren<Text>()); });

        AddListener(EventTriggerType.PointerDown, DisableOrbit, ControlPanel.transform.GetChild(0).gameObject);

        AddListener(EventTriggerType.PointerUp, EnableOrbit, ControlPanel.transform.GetChild(0).gameObject);

        AddListener(EventTriggerType.PointerUp, RadiusAddXp, MainSlider.gameObject);
        Pos_button.GetComponent<Button>().onClick.AddListener(() => AddCubeAtPossitive());
        Neg_button.GetComponent<Button>().onClick.AddListener(() => AddCubeAtNegative());
        submit_button.GetComponent<Button>().onClick.AddListener(() => Submit());
        but_add_a.GetComponent<Button>().onClick.AddListener(() => SelectionAddend(-0.5f));
        but_add_b.GetComponent<Button>().onClick.AddListener(() => SelectionAddend(0.5f));
        but_pick_a.GetComponent<Button>().onClick.AddListener(() => pick_positive_cube());
        but_pick_b.GetComponent<Button>().onClick.AddListener(() => pick_negative_cube());
        but_clear.GetComponent<Button>().onClick.AddListener(() => ClearTable());
        but_droptoken.GetComponent<Button>().onClick.AddListener(() => BackPick());
        AddListenerToEvents(EventTriggerType.PointerDown, SelectionAddend, addend_a, -0.5f);
        AddListenerToEvents(EventTriggerType.PointerDown, SelectionAddend, addend_b, 0.5f);

        //addend_a.GetComponent<SpriteRenderer>().sprite = but_dummy.GetComponent<Button>().spriteState.pressedSprite;
        //addend_b.GetComponent<SpriteRenderer>().sprite = but_dummy.GetComponent<Button>().spriteState.disabledSprite;
    }









    public void AssignInitialValues()
    {


        //InfoHeadingText.text = InfoText[0];
        //LGListText.text = InfoText[1];
        //IListText.text = InfoText[2];
        //AListText.text = InfoText[3];
        if (OptionUnlockedTill > 0)
        {
            LocalXP.transform.localScale = new Vector3(0, 0, 0);
            //ObjectivePanel.transform.localScale = new Vector3(0, 0, 0);

            ObjectiveInfoText.text = QuestionPick(0);

            ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen");
        }


        InfoCloseBtn.gameObject.transform.localScale = new Vector3(0, 0, 0);
        localGainedXpValueText.text = "0";
        Main_XP_ToShow.text = MainXpValue + "";
        WronOptionPanel.transform.localScale = new Vector3(0, 0, 0);

        // ChangeDistance(Slider_distance.value, Slider_distance.GetComponentInChildren<Text>());
        //ChangeRadius(MainSlider.GetComponent<Slider>().value, MainSlider.GetComponentInChildren<Text>());
        //  ScannerObject.AddComponent<RendererOffset>();
        //  ScannerObject.GetComponent<RendererOffset>().offset = -0.4f;
        //   ScannerObject.SetActive(false);

        // Labels[1].transform.localPosition = MidPoint(Dummy, Ball);
        //   Labels[0].transform.localPosition = MidPoint(Ball, MTape);


    }
    public void AddCubeAtPossitive()
    {
        if (obj1 == null && obj2 == null)
        {
            if (pos_value < 15)
            {
                Pos_button.GetComponent<Button>().interactable = false;
                Neg_button.GetComponent<Button>().interactable = false;
                GameObject obj = Instantiate(clone_obj1, transform.localPosition = new Vector3(0f, 0, 0f), Quaternion.identity);
                //  obj.transform.parent = Parentcube1.transform;
                obj.transform.SetParent(Parentcube1.transform,false);

                   obj.transform.localPosition = new Vector3(sel_addend_side, 3, 0);
                pos_value++;
                green_obj.Add(obj);
                Invoke("DisplayResult", 1.2f);
                
            }
           
            
        }
        else
        {
            BackPick();
        }
        
    }
    public void AddCubeAtNegative()
    {
        if (obj1 == null && obj2 == null)
        {
            if (neg_value > -15)
            {
                Pos_button.GetComponent<Button>().interactable = false;
                Neg_button.GetComponent<Button>().interactable = false;
                GameObject obj = Instantiate(clone_obj2, new Vector3(0f, 0, 0f), Quaternion.identity);
                // obj.transform.parent = Parentcube2.transform;
                obj.transform.SetParent(Parentcube2.transform, false);
                obj.transform.localPosition = new Vector3(sel_addend_side, 3, 0);
                neg_value--;
                red_obj.Add(obj);
                Invoke("DisplayResult", 1.2f);
               
            }
           
            
        }
        else
        {
            BackPick();
        }
    }
    public void SelectionAddend(float num)
    {
        if (obj1 == null && obj2 == null)
        {
            sel_addend_side = num;
        }
        else
        {
            BackPick();
        }

        
    }
    public void pick_positive_cube()
    {
        if (green_obj.Count > 0 && red_obj.Count > 0)
        {
            
            obj1 = green_obj[pos_value - 1];
            //Destroy(obj1.GetComponent<Rigidbody>());
            obj1.GetComponent<Rigidbody>().useGravity = false;
            obj1.GetComponent<Rigidbody>().isKinematic = true;
            iTween.MoveTo(obj1, iTween.Hash("x", obj1.transform.localPosition.x, "y", 2, "z", 0, "delay", 0.2f, "time", .5f, "islocal", true, "easetype", iTween.EaseType.easeInSine));
            if (obj1 != null && obj2 != null)
            {
                //vid5203_additionofinteger.Instance.collision_one = true;
                StartCoroutine(vid5203_additionofinteger.Instance.ManualzeroPair());
            }
            // pick_cnt++;
            but_pick_a.GetComponent<Button>().interactable = false;
        }
        else
        {
            WrongOptionSelected();
            WronOptionPanel.transform.GetChild(0).GetComponent<Text>().text = "No zero pairs available";
            WronOptionPanel.transform.GetChild(0).GetComponent<Text>().color = new Color(0.9921f, 0.4431f, 0.08621f, 1);
        }


    }
    public void pick_negative_cube()
    {
        if (green_obj.Count > 0 && red_obj.Count > 0)
        {
            n_value = Mathf.Abs(neg_value);
            obj2 = red_obj[n_value - 1];
            //Destroy(obj2.GetComponent<Rigidbody>());
            obj2.GetComponent<Rigidbody>().useGravity = false;
            obj2.GetComponent<Rigidbody>().isKinematic = true;
            iTween.MoveTo(obj2, iTween.Hash("x", obj2.transform.localPosition.x, "y", 2, "z", 0, "delay", 0.2f, "time", .5f, "islocal", true, "easetype", iTween.EaseType.easeInSine));
            if (obj1 != null && obj2 != null)
            {
                // vid5203_additionofinteger.Instance.collision_one = true;
                StartCoroutine(vid5203_additionofinteger.Instance.ManualzeroPair());
            }
            // pick_cnt++;
            but_pick_b.GetComponent<Button>().interactable = false;
        }
        else
        {
            WrongOptionSelected();
            WronOptionPanel.transform.GetChild(0).GetComponent<Text>().text = "No zero pairs available";
            WronOptionPanel.transform.GetChild(0).GetComponent<Text>().color = new Color(0.9921f, 0.4431f, 0.08621f, 1);
        }
    }
    public void  BackPick()
    {
        if (obj1 != null)
        {
            obj1.GetComponent<Rigidbody>().useGravity = true;
            obj1.GetComponent<Rigidbody>().isKinematic = false;
            obj1 = null;
            but_pick_a.GetComponent<Button>().interactable = true;
        }
        if (obj2 != null)
        {
            obj2.GetComponent<Rigidbody>().useGravity = true;
            obj2.GetComponent<Rigidbody>().isKinematic = false;
            obj2 = null;
            but_pick_b.GetComponent<Button>().interactable = true;
        }
       
    }
    IEnumerator ManualzeroPair()
    {
        yield return new WaitForSeconds(1f);
        iTween.MoveTo(obj1, iTween.Hash("x", 0, "y", obj1.transform.localPosition.y, "z", 0, "delay", 0.2f, "time", .5f, "islocal", true, "easetype", iTween.EaseType.easeInSine));
        iTween.MoveTo(obj2, iTween.Hash("x", 0, "y", obj2.transform.localPosition.y, "z", 0, "delay", 0.2f, "time", .5f, "islocal", true, "easetype", iTween.EaseType.easeInSine));
        yield return new WaitForSeconds(1f);
        vid5203_additionofinteger.Instance.collision_one = true;
       StartCoroutine(vid5203_additionofinteger.Instance.DeleteCube(vid5203_additionofinteger.Instance.obj1, vid5203_additionofinteger.Instance.obj2));
    }

    public void Update()
    {
        //  if (TopicToCheck == 1)
        //{
        res_value = (pos_value) + (neg_value);
        //  print("P_value = " + pos_value + "N_value = " + neg_value + "Total = " + res_value);
        //if (pos_value > 0 && neg_value < 0 && res_value == 0)
        //{
        //    StartCoroutine(DeleteTwoCube());
        //    pos_value = 0;
        //    neg_value = 0;
        //}
        //  }
        //if (TopicToCheck == 2)
        //{
        //    res_value = (pos_value) + (neg_value);
        //    //  print("P_value = " + pos_value + "N_value = " + neg_value + "Total = " + res_value);
        //    if (pos_value > 0 && neg_value < 0 && res_value == 0)
        //    {
        //        StartCoroutine(DeleteTwoCube());
        //        pos_value = 0;
        //        neg_value = 0;
        //    }
        //}
        //if (TopicToCheck == 3)
        //{
        //    res_value = (pos_value) + (neg_value);
        //    //  print("P_value = " + pos_value + "N_value = " + neg_value + "Total = " + res_value);
        //    if (pos_value > 0 && neg_value < 0 && res_value == 0)
        //    {
        //        StartCoroutine(DeleteTwoCube());
        //        pos_value = 0;
        //        neg_value = 0;
        //    }
        //}
        if (sel_addend_side == -0.5f)
        {
            but_add_a.GetComponent<Image>().sprite = but_add_a.GetComponent<Button>().spriteState.pressedSprite;
            but_add_b.GetComponent<Image>().sprite = but_add_a.GetComponent<Button>().spriteState.disabledSprite;
            addend_a.GetComponent<SpriteRenderer>().sprite =but_dummy.GetComponent<Button>().spriteState.pressedSprite;
            addend_b.GetComponent<SpriteRenderer>().sprite = but_dummy.GetComponent<Button>().spriteState.disabledSprite;

        }
        else if(sel_addend_side == 0.5f)
        {
            but_add_a.GetComponent<Image>().sprite = but_add_b.GetComponent<Button>().spriteState.disabledSprite;
            but_add_b.GetComponent<Image>().sprite = but_add_b.GetComponent<Button>().spriteState.pressedSprite;
            addend_a.GetComponent<SpriteRenderer>().sprite = but_dummy.GetComponent<Button>().spriteState.disabledSprite;
            addend_b.GetComponent<SpriteRenderer>().sprite = but_dummy.GetComponent<Button>().spriteState.pressedSprite;
        }

    }
    public void DisplayResult()
    {
        Pos_button.transform.GetChild(0).GetComponent<Text>().text = pos_value.ToString();
        Neg_button.transform.GetChild(0).GetComponent<Text>().text = neg_value.ToString();
        if (TopicToCheck == 1||ran_que==0)
            total_label.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Total value = " + res_value.ToString();
        //if (TopicToCheck == 2|| ran_que == 1)
        //    total_label.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "X value = " + res_value.ToString();
        //if (TopicToCheck == 3|| ran_que == 2)
        //    total_label.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Y value = " + res_value.ToString();
        //Pos_button.GetComponent<Button>().interactable = true;
       // Neg_button.GetComponent<Button>().interactable = true;
    }
    public IEnumerator DeleteTwoCube()
    {
        // n_value = Mathf.Abs(neg_value);
        yield return new WaitForSeconds(.6f);
        green_obj[pos_value].transform.GetChild(1).gameObject.SetActive(false);
        red_obj[neg_value].transform.GetChild(1).gameObject.SetActive(false);
        yield return new WaitForSeconds(.05f);
        green_obj[pos_value].transform.GetChild(1).gameObject.SetActive(true);
        red_obj[neg_value].transform.GetChild(1).gameObject.SetActive(true);
        yield return new WaitForSeconds(.05f);
        // y_pos = 0.08f;
        for (int i = 0; i < red_obj.Count; i++)
        {
            DestroyObject(red_obj[i]);
        }
        for (int j = 0; j < green_obj.Count; j++)
        {
            DestroyObject(green_obj[j]);
        }
        red_obj.Clear();
        green_obj.Clear();
        collision_one = false;
        total_label.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Total = " + res_value.ToString();
        TextEffect();
        Invoke("DisplayResult", 1.2f);
    }
    public IEnumerator DeleteCube(GameObject obj1, GameObject obj2)
    {
        n_value = Mathf.Abs(neg_value);
       obj1.transform.GetChild(1).gameObject.SetActive(false);
        obj2.transform.GetChild(1).gameObject.SetActive(false);
        yield return new WaitForSeconds(.05f);
        obj1.transform.GetChild(1).gameObject.SetActive(true);
       obj2.transform.GetChild(1).gameObject.SetActive(true);
        yield return new WaitForSeconds(.05f);
        if (collision_one)
        {
           // Debug.Log("calll");
            if (red_obj.Count > 0)
                DestroyObject(obj1);
            if (green_obj.Count > 0)
                DestroyObject(obj2);
            red_obj.RemoveAt(n_value - 1);
           green_obj.RemoveAt(pos_value - 1);
            if (neg_value < 0)
                neg_value++;
            if (pos_value > 0)
                pos_value--;
            Invoke("DisplayResult", 1.2f);
            // total_label.transform.localPosition -= new Vector3(0, 0.08f, 0);
            //  total_label.transform.localPosition -= new Vector3(0, 0.08f, 0);
           // total_label.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Total value = " + res_value.ToString();
            TextEffect();
            vid5203_additionofinteger.Instance.Pos_button.GetComponent<Button>().interactable = true;
            vid5203_additionofinteger.Instance.Neg_button.GetComponent<Button>().interactable = true;
            collision_one = false;
            CubeCollision.cnt = 0;
            but_pick_a.GetComponent<Button>().interactable = true;
            but_pick_b.GetComponent<Button>().interactable = true;
        }

    }
    public void TextEffect()
    {
        flylabel.transform.GetChild(0).GetComponent<TextMeshPro>().text = "Zero pair formed";
        flylabel.transform.position = new Vector3(0, y_pos, 0);

        iTween.Stop(flylabel.gameObject);

        iTween.ScaleTo(flylabel.gameObject, iTween.Hash("x", 0.15, "y", 0.15, "z", 0.15, "delay", 0.1f, "time", 0.5f, "easetype", iTween.EaseType.easeOutElastic));

        iTween.MoveTo(flylabel.gameObject, iTween.Hash("x", flylabel.transform.position.x, "y", flylabel.transform.position.y + 50, "z", 0, "delay", 0.5f, "time", 5f, "easetype", iTween.EaseType.easeInSine));

        // iTween.MoveTo(flylabel.gameObject, iTween.Hash("x", Main_XP_ToShow.transform.position.x, "y", Main_XP_ToShow.transform.position.y, "z", 0, "delay", 1.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

        iTween.ScaleTo(flylabel.gameObject, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 5f, "time", 1f, "easetype", iTween.EaseType.easeOutElastic));
    }

    public void Submit()
    {
        //  if (submit_button.transform.GetChild(0).GetComponent<Text>().text == "Submit")
        if (obj1 == null && obj2 == null)
        {

        if (OptionUnlockedTill < 1) {
                if (TopicToCheck == 1)
                {
                    if (res_value == 7 && red_obj.Count == 0)
                    {
                        submit_button.SetActive(false);
                        AfterRightAnswer();
                        Invoke("AddXpEffect", 2);
                        WrongOptionSelected();
                        WronOptionPanel.transform.GetChild(0).GetComponent<Text>().text = "Well Done!";
                        WronOptionPanel.GetComponentInChildren<Text>().color = new Color(0.7568f, 0.8588f, 0.075f, 1);
                    }
                    else if (res_value == 7 && red_obj.Count != 0)
                    {
                        WrongOptionSelected();
                        WronOptionPanel.transform.GetChild(0).GetComponent<Text>().text = "Clear zero pairs";
                        WronOptionPanel.transform.GetChild(0).GetComponent<Text>().color = new Color(0.9921f, 0.4431f, 0.08621f, 1);
                    }
                    else
                    {
                        WrongOptionSelected();
                        WronOptionPanel.transform.GetChild(0).GetComponent<Text>().text = "Wrong value submited";
                        WronOptionPanel.transform.GetChild(0).GetComponent<Text>().color = new Color(0.9921f, 0.4431f, 0.08621f, 1);
                        Debug.Log("Ooops");
                        // AfterWrongAnswer();
                    }
                }
                else if (TopicToCheck == 2)
                {
                    if (res_value == -3 && green_obj.Count == 0)
                    {
                        submit_button.SetActive(false);
                        AfterRightAnswer();

                        Invoke("AddXpEffect", 2);
                        WrongOptionSelected();
                        WronOptionPanel.transform.GetChild(0).GetComponent<Text>().text = "Well Done!";
                        WronOptionPanel.GetComponentInChildren<Text>().color = new Color(0.7568f, 0.8588f, 0.075f, 1);
                    }
                    else if (res_value == -3 && green_obj.Count != 0)
                    {
                        WrongOptionSelected();
                        WronOptionPanel.transform.GetChild(0).GetComponent<Text>().text = "Clear zero pairs";
                        WronOptionPanel.transform.GetChild(0).GetComponent<Text>().color = new Color(0.9921f, 0.4431f, 0.08621f, 1);
                    }
                    else
                    {
                        WrongOptionSelected();
                        WronOptionPanel.transform.GetChild(0).GetComponent<Text>().text = "Wrong value submited";
                        WronOptionPanel.transform.GetChild(0).GetComponent<Text>().color = new Color(0.9921f, 0.4431f, 0.08621f, 1);
                        Debug.Log("Ooops");
                        // AfterWrongAnswer();
                    }
                }
                else if (TopicToCheck == 3)
                {
                    if (res_value == 2 && red_obj.Count == 0)
                    {
                        submit_button.SetActive(false);
                        AfterRightAnswer();

                        Invoke("AddXpEffect", 2);
                        WrongOptionSelected();
                        WronOptionPanel.transform.GetChild(0).GetComponent<Text>().text = "Well Done!";
                        WronOptionPanel.GetComponentInChildren<Text>().color = new Color(0.7568f, 0.8588f, 0.075f, 1);
                    }
                    else if (res_value == 2 && red_obj.Count != 0)
                    {
                        WrongOptionSelected();
                        WronOptionPanel.transform.GetChild(0).GetComponent<Text>().text = "Clear zero pairs";
                        WronOptionPanel.transform.GetChild(0).GetComponent<Text>().color = new Color(0.9921f, 0.4431f, 0.08621f, 1);
                    }
                    else
                    {
                        WrongOptionSelected();
                        WronOptionPanel.transform.GetChild(0).GetComponent<Text>().text = "Wrong value submited";
                        WronOptionPanel.transform.GetChild(0).GetComponent<Text>().color = new Color(0.9921f, 0.4431f, 0.08621f, 1);
                        Debug.Log("Ooops");
                        // AfterWrongAnswer();
                    }
                }
        }
        else
        {
            if (dummy_res > 0)
            {
                if (res_value == dummy_res && red_obj.Count == 0)
                {
                    submit_button.SetActive(false);
                    AfterRightAnswer();

                    WrongOptionSelected();
                    WronOptionPanel.transform.GetChild(0).GetComponent<Text>().text = "Well Done!";
                    WronOptionPanel.GetComponentInChildren<Text>().color = new Color(0.7568f, 0.8588f, 0.075f, 1);
                }
                else if (res_value == dummy_res && red_obj.Count != 0)
                {
                    WrongOptionSelected();
                    WronOptionPanel.transform.GetChild(0).GetComponent<Text>().text = "Clear zero pairs";
                    WronOptionPanel.transform.GetChild(0).GetComponent<Text>().color = new Color(0.9921f, 0.4431f, 0.08621f, 1);
                }
                else
                {
                    WrongOptionSelected();
                    WronOptionPanel.transform.GetChild(0).GetComponent<Text>().text = "Wrong value submited";
                    WronOptionPanel.transform.GetChild(0).GetComponent<Text>().color = new Color(0.9921f, 0.4431f, 0.08621f, 1);
                    Debug.Log("Ooops");
                    // AfterWrongAnswer();
                }
            }
            else if (dummy_res < 0)
            {
                if (res_value == dummy_res && green_obj.Count == 0)
                {
                    submit_button.SetActive(false);
                    AfterRightAnswer();
                    //if (OptionUnlockedTill < 1)
                    //    Invoke("AddXpEffect", 2);
                    WrongOptionSelected();
                    WronOptionPanel.transform.GetChild(0).GetComponent<Text>().text = "Well Done!";
                    WronOptionPanel.GetComponentInChildren<Text>().color = new Color(0.7568f, 0.8588f, 0.075f, 1);
                }
                else if (res_value == dummy_res && green_obj.Count != 0)
                {
                    WrongOptionSelected();
                    WronOptionPanel.transform.GetChild(0).GetComponent<Text>().text = "Clear zero pairs";
                    WronOptionPanel.transform.GetChild(0).GetComponent<Text>().color = new Color(0.9921f, 0.4431f, 0.08621f, 1);
                }
                else
                {
                    WrongOptionSelected();
                    WronOptionPanel.transform.GetChild(0).GetComponent<Text>().text = "Wrong value submited";
                    WronOptionPanel.transform.GetChild(0).GetComponent<Text>().color = new Color(0.9921f, 0.4431f, 0.08621f, 1);
                    Debug.Log("Ooops");
                    // AfterWrongAnswer();
                }
            }
            else if (dummy_res == 0)
            {
                if (res_value == dummy_res && green_obj.Count == 0 && red_obj.Count == 0)
                {
                    submit_button.SetActive(false);
                    AfterRightAnswer();
                    //if (OptionUnlockedTill < 1)
                    //    Invoke("AddXpEffect", 2);
                    WrongOptionSelected();
                    WronOptionPanel.transform.GetChild(0).GetComponent<Text>().text = "Well Done!";
                    WronOptionPanel.GetComponentInChildren<Text>().color = new Color(0.7568f, 0.8588f, 0.075f, 1);
                }
                else if (res_value == dummy_res && (green_obj.Count != 0 || red_obj.Count != 0))
                {
                    WrongOptionSelected();
                    WronOptionPanel.transform.GetChild(0).GetComponent<Text>().text = "Clear zero pairs";
                    WronOptionPanel.transform.GetChild(0).GetComponent<Text>().color = new Color(0.9921f, 0.4431f, 0.08621f, 1);
                }
                else
                {
                    WrongOptionSelected();
                    WronOptionPanel.transform.GetChild(0).GetComponent<Text>().text = "Wrong value submited";
                    WronOptionPanel.transform.GetChild(0).GetComponent<Text>().color = new Color(0.9921f, 0.4431f, 0.08621f, 1);
                    Debug.Log("Ooops");
                    // AfterWrongAnswer();
                }
            }
            // if (ran_que == 0)
            // {

            // }
            //else if (ran_que == 1)
            //{
            //    if (res_value == dummy_x)
            //    {
            //        submit_button.SetActive(false);
            //        AfterRightAnswer();
            //        Invoke("AddXpEffect", 2);
            //        WrongOptionSelected();
            //        WronOptionPanel.transform.GetChild(0).GetComponent<Text>().text = "Well Done!";
            //        WronOptionPanel.GetComponentInChildren<Text>().color = new Color(0.7568f, 0.8588f, 0.075f, 1);
            //    }
            //    else
            //    {
            //        WrongOptionSelected();
            //        WronOptionPanel.transform.GetChild(0).GetComponent<Text>().text = "Wrong value submited";
            //        WronOptionPanel.transform.GetChild(0).GetComponent<Text>().color = new Color(0.9921f, 0.4431f, 0.08621f, 1);
            //        Debug.Log("Ooops");
            //        // AfterWrongAnswer();
            //    }
            //}
            //else if (ran_que == 2)
            //{
            //    if (res_value == dummy_y)
            //    {
            //        submit_button.SetActive(false);
            //        AfterRightAnswer();
            //        Invoke("AddXpEffect", 2);
            //        WrongOptionSelected();
            //        WronOptionPanel.transform.GetChild(0).GetComponent<Text>().text = "Well Done!";
            //        WronOptionPanel.GetComponentInChildren<Text>().color = new Color(0.7568f, 0.8588f, 0.075f, 1);
            //    }
            //    else
            //    {
            //        WrongOptionSelected();
            //        WronOptionPanel.transform.GetChild(0).GetComponent<Text>().text = "Wrong value submited";
            //        WronOptionPanel.transform.GetChild(0).GetComponent<Text>().color = new Color(0.9921f, 0.4431f, 0.08621f, 1);
            //        Debug.Log("Ooops");
            //        // AfterWrongAnswer();
            //    }
            //}
        }

            iTween.MoveTo(buttongroup, iTween.Hash("x", 0, "y", ChangeToLocal_Y(-200), "z", 0, "delay", 0.01f, "time", 0.5f, "islocal", true, "easetype", iTween.EaseType.easeInSine));
            Pos_button.GetComponent<Button>().interactable = false;
            Neg_button.GetComponent<Button>().interactable = false;
        }
        else
        {
            BackPick();
        }
        



    }


public float ChangeToLocal_X(float Val)
    {
        float X_ToRet = 0;
        //print("wid........"+ UIcanvas.GetComponent<RectTransform>().rect.width);
        X_ToRet = ((UIcanvas.x) + Val);


        return X_ToRet;
    }


    public float ChangeToLocal_Y(float Val)
    {
        float Y_ToRet = 0;
        //print("wid........"+ UIcanvas.GetComponent<RectTransform>().rect.width);
        Y_ToRet = ((UIcanvas.y) + Val);


        return Y_ToRet;
    }






    public void AfterRightAnswer()
    {
        for (int i = 0; i < red_obj.Count; i++)
        {
            if (red_obj[i].GetComponent<Rigidbody>())
                Destroy(red_obj[i].GetComponent<Rigidbody>());
        }
        for (int j = 0; j < green_obj.Count; j++)
        {
            if (green_obj[j].GetComponent<Rigidbody>())
                Destroy(green_obj[j].GetComponent<Rigidbody>());
        }

        iTween.RotateTo(bot_obj, iTween.Hash("x", 0, "y", -90, "z", 0, "delay", 0.01f, "time", 0.02f, "islocal", true, "easetype", iTween.EaseType.easeInSine));
        iTween.MoveTo(bot_obj, iTween.Hash("x", 9.2, "y", 0.6, "z", 0, "delay", 0.2f, "time", 5.5f, "islocal", true, "easetype", iTween.EaseType.easeInSine));
        iTween.MoveTo(Parentcube1, iTween.Hash("x", 10, "y", -1, "z", 0, "delay", 0.65f, "time", 5f, "islocal", true, "easetype", iTween.EaseType.easeInSine));
        iTween.MoveTo(Parentcube2, iTween.Hash("x", 10, "y", -1, "z", 0, "delay", 0.65f, "time", 5f, "islocal", true, "easetype", iTween.EaseType.easeInSine));
        if (OptionUnlockedTill == 1)
        {
            Invoke("ResetToNext", 5.8f);
           // Invoke("DisplayResult", 1f);
        }
            
    }
    public void ResetToNext()
    {
        for (int i = 0; i < red_obj.Count; i++)
        {
            DestroyObject(red_obj[i]);
        }
        for (int j = 0; j < green_obj.Count; j++)
        {
            DestroyObject(green_obj[j]);
        }
        red_obj.Clear();
        green_obj.Clear();
        pos_value = 0;
        neg_value = 0;
        res_value = 0;
        n_value = 0;

        bot_obj.transform.localPosition = new Vector3(-10, 0.6f, 0);
        bot_obj.transform.localEulerAngles = new Vector3(0, 0, 0);
        Parentcube1.transform.localPosition = new Vector3(0, -1, 0);
        Parentcube2.transform.localPosition = new Vector3(0, -1, 0);
        iTween.MoveTo(bot_obj, iTween.Hash("x", 0, "y", 0.6f, "z", 0, "delay", 0.5f, "time", 0.5f, "islocal", true, "easetype", iTween.EaseType.easeInSine));
        Pos_button.transform.GetChild(0).GetComponent<Text>().text = pos_value.ToString();
        Neg_button.transform.GetChild(0).GetComponent<Text>().text = neg_value.ToString();
        total_label.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Total value = " + res_value.ToString();
        submit_button.SetActive(true);
        if (OptionUnlockedTill == 1)
        {
           // ran_que = UnityEngine.Random.Range(0, 2);
            ObjectiveInfoText.text = QuestionPick(0);

            ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen");
           // iTween.MoveTo(buttongroup, iTween.Hash("x", 0, "y", ChangeToLocal_Y(0), "z", 0, "delay", 0.01f, "time", .5f, "islocal", true, "easetype", iTween.EaseType.easeInSine));
        }
        Invoke("DisplayResult", 1f);
        iTween.MoveTo(buttongroup, iTween.Hash("x", 0, "y", ChangeToLocal_Y(0), "z", 0, "delay", 0.01f, "time", .5f, "islocal", true, "easetype", iTween.EaseType.easeInSine));
        vid5203_additionofinteger.Instance.Pos_button.GetComponent<Button>().interactable = true;
        vid5203_additionofinteger.Instance.Neg_button.GetComponent<Button>().interactable = true;
    }
    public void ClearTable()
    {
        for (int i = 0; i < red_obj.Count; i++)
        {
            DestroyObject(red_obj[i]);
        }
        for (int j = 0; j < green_obj.Count; j++)
        {
            DestroyObject(green_obj[j]);
        }
        red_obj.Clear();
        green_obj.Clear();
        pos_value = 0;
        neg_value = 0;
        res_value = 0;
        n_value = 0;

        bot_obj.transform.localPosition = new Vector3(-10, 0.6f, 0);
        bot_obj.transform.localEulerAngles = new Vector3(0, 0, 0);
        Parentcube1.transform.localPosition = new Vector3(0, -1, 0);
        Parentcube2.transform.localPosition = new Vector3(0, -1, 0);
        iTween.MoveTo(bot_obj, iTween.Hash("x", 0, "y", 0.6f, "z", 0, "delay", 0.5f, "time", 0.5f, "islocal", true, "easetype", iTween.EaseType.easeInSine));
        Pos_button.transform.GetChild(0).GetComponent<Text>().text = pos_value.ToString();
        Neg_button.transform.GetChild(0).GetComponent<Text>().text = neg_value.ToString();
        total_label.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Total value = " + res_value.ToString();
        submit_button.SetActive(true);
        if (OptionUnlockedTill == 1)
        {
            // ran_que = UnityEngine.Random.Range(0, 2);
           // ObjectiveInfoText.text = QuestionPick(0);

            ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen");
            // iTween.MoveTo(buttongroup, iTween.Hash("x", 0, "y", ChangeToLocal_Y(0), "z", 0, "delay", 0.01f, "time", .5f, "islocal", true, "easetype", iTween.EaseType.easeInSine));
        }
        Invoke("DisplayResult", 1f);
        iTween.MoveTo(buttongroup, iTween.Hash("x", 0, "y", ChangeToLocal_Y(0), "z", 0, "delay", 0.01f, "time", .5f, "islocal", true, "easetype", iTween.EaseType.easeInSine));
        vid5203_additionofinteger.Instance.Pos_button.GetComponent<Button>().interactable = true;
        vid5203_additionofinteger.Instance.Neg_button.GetComponent<Button>().interactable = true;
    }


   

    public String QuestionPick(int i)
    {
        string Ques = "";
        switch (i)
        {
            case 0:
                ran_x = UnityEngine.Random.Range(-5, 5);
                ran_y = UnityEngine.Random.Range(-5, 5);
                dummy_res = ran_x + ran_y;
                Ques = "Find total value in (" + ran_x + ") + (" + ran_y + ") = ? ";

                break;
            case 1:
                ran_res = UnityEngine.Random.Range(-5, 5);
                ran_y = UnityEngine.Random.Range(-5, 5);
                dummy_x = ran_res - ran_y;
                Ques = "Find the value of \"X\" in  (X) + (" + ran_y + ") = " + ran_res;

                break;
            case 2:
                ran_res = UnityEngine.Random.Range(-5, 5);
                ran_x = UnityEngine.Random.Range(-5, 5);
                dummy_y = ran_res - ran_x;
                Ques = "Find the value of \"Y\" in ("+ ran_x +") + (Y) = " + ran_res;

                break;
                               
        }

        return Ques;
    }



    public void AfterWrongAnswer()
    {
        for (int i = 0; i < red_obj.Count; i++)
        {
            if(red_obj[i].GetComponent<Rigidbody>())
            Destroy(red_obj[i].GetComponent<Rigidbody>());
        }
        for (int j = 0; j < green_obj.Count; j++)
        {
            if (green_obj[j].GetComponent<Rigidbody>())
                Destroy(green_obj[j].GetComponent<Rigidbody>());
        }

        Invoke("MovePos",0.1f);
        //  iTween.RotateTo(bot_obj, iTween.Hash("x", 0, "y", -90, "z", 0, "delay", 0.01f, "time", 0.02f, "islocal", true, "easetype", iTween.EaseType.easeInSine));
        // iTween.MoveTo(bot_obj, iTween.Hash("x", 9.2, "y", 0.6, "z", 0, "delay", 0.2f, "time", 5.5f, "islocal", true, "easetype", iTween.EaseType.easeInSine));
        // iTween.MoveTo(Parentcube1, iTween.Hash("x", 10, "y", -1, "z", 0, "delay", 0.65f, "time", 5f, "islocal", true, "easetype", iTween.EaseType.easeInSine));
        //  iTween.MoveTo(Parentcube2, iTween.Hash("x", 10, "y", -1, "z", 0, "delay", 0.65f, "time", 5f, "islocal", true, "easetype", iTween.EaseType.easeInSine));


    }

    public void MovePos()
    {
        print("cnt......");
        if (red_obj.Count != 0)
        {
            for (int i = 0; i < red_obj.Count; i++)
            {
                iTween.MoveTo(red_obj[i].gameObject, iTween.Hash("x", 0, "y", 2, "z", 0, "delay", 0.02f, "time", .1f, "islocal", true, "easetype", iTween.EaseType.easeInSine));
                iTween.MoveTo(red_obj[i].gameObject, iTween.Hash("x", 0, "y", 2, "z", -3, "delay", 0.15f*i, "time", .5f, "islocal", true, "easetype", iTween.EaseType.easeInSine));
            }
        }
        if (green_obj.Count != 0)
        {
            for (int i = 0; i < green_obj.Count; i++)
            {
                iTween.MoveTo(green_obj[i].gameObject, iTween.Hash("x", 0, "y", 2, "z", 0, "delay", 0.02f, "time", .1f, "islocal", true, "easetype", iTween.EaseType.easeInSine));
                iTween.MoveTo(green_obj[i].gameObject, iTween.Hash("x", 0, "y", 2, "z", -3, "delay", 0.15f*i, "time", .5f, "islocal", true, "easetype", iTween.EaseType.easeInSine));
            }
        }
    }

    public void TaskListCreation() {

        rT = AllTaskListObj.transform.parent.GetComponent<RectTransform>();

        localGaniedXpValue = 0;

        for (int i = 0; i < TaskList.Count; i++)
        {

            GameObject Tl = (GameObject)Instantiate(TaskObj);
            Tl.transform.SetParent(AllTaskListObj.transform);
            Tl.transform.localScale = new Vector3(1, 1, 1);
            Tl.name = i.ToString();

            AllTaskData.Add(Tl);
            AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(false);
            AllTaskData[i].gameObject.transform.GetChild(2).gameObject.SetActive(false);
            AllTaskData[i].gameObject.transform.GetChild(3).GetComponentInChildren<Text>().text = TaskList[i];
            AllTaskData[i].gameObject.transform.GetChild(4).GetComponentInChildren<Text>().text = "+" + XpList[i] + " XP";

            localTotalXpValue += int.Parse(XpList[i]);
        }

        rT.sizeDelta = new Vector2(rT.sizeDelta.x, TaskList.Count * 50);
        localTotalXpValueText.text = "/" + localTotalXpValue.ToString();

        AllTaskData[0].gameObject.transform.GetChild(2).gameObject.SetActive(true);
        ObjectiveInfoText.text = AllTaskData[0].gameObject.transform.GetChild(3).GetComponentInChildren<Text>().text;
    }

    public void LocalXpCalculation() {

       
            for (int i = 0; i < TaskList.Count; i++)
            {
                AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(false);
                AllTaskData[i].gameObject.transform.GetChild(2).gameObject.SetActive(false);
            }

        if (OptionUnlockedTill < 1)
        {
            if ((TopicToCheck) < (TaskList.Count + 1))
            {
                localGaniedXpValue = 0;

                for (int i = 0; i < TopicToCheck - 1; i++)
                {
                    localGaniedXpValue += int.Parse(XpList[i]);
                }

                for (int i = 0; i < TopicToCheck; i++)
                {
                    AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(true);
                }

                localGainedXpValueText.text = localGaniedXpValue.ToString();

                AllTaskData[TopicToCheck - 1].gameObject.transform.GetChild(2).gameObject.SetActive(true);

                ObjectiveInfoText.text = AllTaskData[TopicToCheck - 1].gameObject.transform.GetChild(3).GetComponentInChildren<Text>().text;
            }
            else
            {
                for (int i = 0; i < TaskList.Count; i++)
                {
                    AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(true);
                    AllTaskData[i].gameObject.transform.GetChild(2).gameObject.SetActive(false);
                }

                localGainedXpValueText.text = localTotalXpValue.ToString();

                XP_Effect.text = "+" + localTotalXpValue + " XP";

                XP_Effect.transform.position = (XP_ToShow.transform.position);

                iTween.Stop(XP_Effect.gameObject);

                iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeOutElastic));

                iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", XP_Effect.transform.position.x, "y", XP_Effect.transform.position.y + 50, "z", 0, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

                iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", Main_XP_ToShow.transform.position.x, "y", Main_XP_ToShow.transform.position.y, "z", 0, "delay", 1.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

                iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "delay", 2f, "time", 0.5f, "easetype", iTween.EaseType.linear, "oncomplete", "MainXpCalculation", "oncompletetarget", this.gameObject));
            }
        }
        else
        {
            for (int i = 0; i < TaskList.Count; i++)
            {
                AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(true);
                AllTaskData[i].gameObject.transform.GetChild(2).gameObject.SetActive(false);
            }

            localGainedXpValueText.text = localTotalXpValue.ToString();   
        }
    }

    public void CloseAllpanels()
    {

        ControlPanel.GetComponent<Animator>().Play("CpanelFullClose");
        LocalXP.GetComponent<Animator>().Play("LocalXpPanelClose");
        Main_Xp.GetComponent<Animator>().Play("MainXpPanelClose");
       // ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelClose");
        InfoButton.GetComponent<Animator>().Play("InfoButtonClose");
        MainSlider.GetComponent<Animator>().Play("MainSliderClose");
        
        if(TasklistPanel.gameObject.transform.localScale.y!=0)
            iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));
    }

    public void OpenAllpanels()
    {
        if (OptionUnlockedTill < 1)
        {
            if (TopicToCheck <= (TaskList.Count))
            {
                LocalXP.GetComponent<Animator>().Play("LocalXpPanelOPen");
              
            }
        }

        ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen");
        ControlPanel.GetComponent<Animator>().Play("CpanelOpen");

        Main_Xp.GetComponent<Animator>().Play("MainXpPanelOpen");

        InfoButton.GetComponent<Animator>().Play("InfoButtonOpen");
        MainSlider.GetComponent<Animator>().Play("MainSliderOpen");
        iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "time", 0.2f, "easetype", iTween.EaseType.linear));
        iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));


    }


    public void AddXpEffect() {

        
        if (TopicToCheck <= (TaskList.Count))
        {
            // Camera.main.GetComponent<PhysicsRaycaster>().enabled = false;
            ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelClose");
            GameObject.Find("Canvas").GetComponent<CanvasGroup>().interactable = false;

            XP_Effect.GetComponent<RectTransform>().anchoredPosition = new Vector2(0,0);

            XP_Effect.text = "+"+int.Parse(XpList[TopicToCheck - 1]) + " XP";

            XP_Effect.transform.localScale = new Vector3(1, 0, 1);

           // iTween.Stop(XP_Effect.gameObject);

            iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeOutElastic));

            iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", XP_Effect.transform.position.x, "y", XP_Effect.transform.position.y + 50, "z", 0, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

            iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", XP_ToShow.transform.position.x, "y", XP_ToShow.transform.position.y, "z", 0, "delay", 1.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

            iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "delay", 2f, "time", 0.5f, "easetype", iTween.EaseType.linear, "oncomplete", "LocalXpCalculation", "oncompletetarget", this.gameObject));

            TopicToCheck++;

            Invoke("EnableRayCast", 3);
        }
        
    }

    public void EnableRayCast() {
        //Camera.main.GetComponent<PhysicsRaycaster>().enabled = true;
        GameObject.Find("Canvas").GetComponent<CanvasGroup>().interactable = true;
       // if (TopicToCheck <= (TaskList.Count))
        {
            ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen");
            Invoke("ResetToNext", 3.3f);
        }
       
    }

    public IEnumerator MainXpCalculation()
    {
        if (OptionUnlockedTill == 0)
        {
            OptionUnlockedTill = 1;

            MainXpValue += localTotalXpValue;

            Main_XP_ToShow.text = MainXpValue.ToString();

            yield return new WaitForSeconds(1f);

            LocalXP.GetComponent<Animator>().Play("LocalXpPanelClose");

            ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelClose");

            if ((ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelClose")) || (ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelFullClose")))
            {
                ControlPanel.GetComponent<Animator>().Play("CpanelOpen");
            }
        }

    }

    public void CloseInfoPanel()
    {
        sessionStart = true;
        InfoPanelBg.SetActive(false);
        iTween.ScaleTo(InformationPanel.gameObject, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));
        OpenAllpanels();
        ControlPanel.GetComponent<Animator>().Play("CpanelOpen");
    }

    public void ContinueInfoPanel()
    {
        iTween.MoveTo(bot_obj, iTween.Hash("x", 0, "y", 0.6f, "z", 0, "delay", 0.5f, "time", 0.5f,"islocal",true, "easetype", iTween.EaseType.easeInSine));
        sessionStart = true;
        InfoPanelBg.SetActive(false);
        iTween.ScaleTo(InformationPanel.gameObject, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));
        InfoCloseBtn.transform.localScale = new Vector3(0.27f, 0.27f, 0.27f);
        InfoContinueBtn.transform.localScale = new Vector3(0, 0, 0);
        OpenAllpanels();
    }

    public void OpenInfoPanel()
    {
        sessionStart = false;
        iTween.ScaleTo(InformationPanel.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));
        InfoPanelBg.SetActive(true);
        iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "time", 0.2f, "easetype", iTween.EaseType.linear));
        iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));
        CloseAllpanels();
    }

    public void OpenTaskList() {


        if (TasklistPanel.gameObject.transform.localScale.y != 1)
        {
            iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.01f, "time", 0.2f, "easetype", iTween.EaseType.easeInSine));
            
            ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelClose");


            //Gained_XP_ArrowBut

            iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 180, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));

            if ((ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelOpen")))
            {
                ControlPanel.GetComponent<Animator>().Play("CpanelClose");
            }
        }
        else {
            iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "delay", 0.01f, "time", 0.2f, "easetype", iTween.EaseType.easeInSine));
            iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));

            ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen");

            ControlPanel.GetComponent<Animator>().Play("CpanelOpen");
        }
    }

    public void OpenOrCloseControlPanel()
    {

        if ((ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelClose")) || (ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelFullClose")))
        {
              ControlPanel.GetComponent<Animator>().Play("CpanelOpen");
        }
        else
        {
            ControlPanel.GetComponent<Animator>().Play("CpanelClose");
        }
    }

    public void WrongOptionSelected()
    {
        WronOptionPanel.transform.localScale = new Vector3(0, 0, 0);

        iTween.Stop(WronOptionPanel.gameObject);

        iTween.ScaleTo(WronOptionPanel.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.01f, "time", 0.2f, "easetype", iTween.EaseType.easeInSine));

        iTween.ScaleTo(WronOptionPanel.gameObject, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 1f, "time", 0.2f, "easetype", iTween.EaseType.easeInSine));

        // InfoPanelBg.SetActive(true);

        Invoke("CloseBlurBg", 1f);

        GameObject.Find("Canvas").GetComponent<CanvasGroup>().interactable = false;

        ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelClose");


        var mouse = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);

        if (mouse.x < Screen.width / 2)
        {
            // WronOptionPanel
            //print("Mouse is on left side of screen.");
            WronOptionPanel.GetComponent<RectTransform>().pivot = new Vector2(0, 0);
        }

        if (mouse.x > Screen.width / 2)
        {
            //print("Mouse is on right side of screen.");
            WronOptionPanel.GetComponent<RectTransform>().pivot = new Vector2(1, 0);
        }


        WronOptionPanel.transform.position = Input.mousePosition;

    }
    public void CloseBlurBg()
    {
        InfoPanelBg.SetActive(false);
        GameObject.Find("Canvas").GetComponent<CanvasGroup>().interactable = true;

        if (OptionUnlockedTill < 1)
        {
            if (InformationPanel.gameObject.transform.localScale.x == 0f)
            {
                ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen", 0, 0f);
            }
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (InformationPanel.gameObject.transform.localScale.x != 1 && sessionStart)
        {
            if (OptionUnlockedTill < 1)
            {
                if (TopicToCheck <= (TaskList.Count))
                {
                    iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "time", 0.2f, "easetype", iTween.EaseType.linear));
                    iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));
                    ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen");
                    ControlPanel.GetComponent<Animator>().Play("CpanelOpen");
                }
            }
            
        }
    }


    public void LabelsAssignment()
    {
        GameObject Label = GameObject.Find("Labels");

        Labels = new GameObject[Label.transform.childCount];

        for (int j = 0; j < Labels.Length; j++)
        {
            Labels[j] = Label.transform.GetChild(j).gameObject;

            GameObject TempLabels = Labels[j];

            foreach (Transform ChildLabel in TempLabels.transform)
            {
                if (ChildLabel.name.Contains("SmoothLook"))
                {
                    ChildLabel.gameObject.AddComponent<SmoothLook>();
                   // ChildLabel.gameObject.AddComponent<ScaleRelativeToCamera>();

                }
            }
        }
                     
        for (int k = 0; k < Labels.Length; k++)
        {
            string name = Labels[k].name.Remove(Labels[k].name.Length - 6);

            if (GameObject.Find(name + "") != null)
            {
                GameObject dummy = GameObject.Find(name + "").gameObject;

                Labels[k].transform.parent = dummy.transform;
                Labels[k].transform.rotation = Quaternion.identity;
                Labels[k].SetActive(false);
            }


            //TempTextMesh = Labels[k].GetComponentsInChildren<TextMeshPro>();

            //if (TempTextMesh.Length > 1)
            //{
            //    for (int p = 0; p < TempTextMesh.Length; p++)
            //    {
            //        if (DescriTextDoc.ContainsKey(name + "_" + p))
            //            TempTextMesh[p].text = DescriTextDoc[name + "_" + p];
            //    }
            //}
            //else
            //{
            //    if (DescriTextDoc.ContainsKey(name))
            //        Labels[k].GetComponentInChildren<TextMeshPro>().text = DescriTextDoc[name];
            //}
           
        }

      
    }
    //public void AddListener(EventTriggerType eventType, Action MethodToCall2(float v), GameObject TriggerObjToAdd)
    //{
    //    EventTrigger.Entry entry = new EventTrigger.Entry();
    //    entry.eventID = eventType;
    //    entry.callback.AddListener((data) => MethodToCall2(float v ));
    //    TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    //}
    public void AddListenerToEvents(EventTriggerType eventType, Action<float> MethodToCall, GameObject TriggerObjToAdd, float p)
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall(p));
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }

    public void AddListener(EventTriggerType eventType, Action MethodToCall, GameObject TriggerObjToAdd)
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall());
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }

    public void EnableOrbit()
    {
        if (!IsAR)
        {
            CamOrbit.DisableInteration = false;


        }
        else
        {
            ArOrbit.DisableInteration = false;
        }
        if (OptionUnlockedTill < 1)
        {

            if (InformationPanel.gameObject.transform.localScale.x != 1)
            {

                if (TopicToCheck <= (TaskList.Count))
                {
                    iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "time", 0.2f, "easetype", iTween.EaseType.linear));
                    iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));
                    ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen");
                }
            }
        }

    }

    public void RadiusAddXp() {
        if ((TopicToCheck <= (TaskList.Count)) && (InformationPanel.transform.localScale.y == 0))
        {
            if (TopicToCheck == 2)
            {
                AddXpEffect();
            }
            else
            {
                WrongOptionSelected();
            }
        }
    }

    public void DistanceAddXp()
    {
        if ((TopicToCheck <= (TaskList.Count)) && (InformationPanel.transform.localScale.y == 0))
        {
            if (TopicToCheck == 3)
            {
                AddXpEffect();
            }
            else
            {
                WrongOptionSelected();
            }
        }
    }

    public void DisableOrbit()
    {
        if (!IsAR)
        {
           CamOrbit.DisableInteration = true;
        }
        else
        {
            ArOrbit.DisableInteration = true;
        }

    }

    public void AddListener(EventTriggerType eventType, Action<float> MethodToCall, GameObject TriggerObjToAdd)
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall(TriggerObjToAdd.GetComponent<Slider>().value));
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }

    public Vector3 MidPoint(GameObject obj1, GameObject obj2)
    {
        Vector3 midp = (obj1.transform.localPosition + obj2.transform.localPosition) / 2;
        return midp;
    }

    public float Scale(GameObject obj1, GameObject obj2)
    {
        float scl = Vector3.Distance(obj1.transform.localPosition, obj2.transform.localPosition);
        return scl;
    }





    public void StartCamera(float val)
    {
        iTween.ValueTo(gameObject, iTween.Hash("from", CamOrbit.cameraRotSide, "to", val, "delay", 0.1f, "islocal", true, "time", 2f, "easetype", iTween.EaseType.easeInSine, "onupdate", "TweenedSomeValue"));
        //iTween.ValueTo(gameObject, iTween.Hash("from", orbit.distance, "to", 0.5f, "delay", 0.1f, "islocal", true, "time", 2f, "easetype", iTween.EaseType.easeInSine, "onupdate", "TweenedValue"));
        // orbit.distance = 0.5f;
    }
    public void TweenedSomeValue(float val)
    {
        CamOrbit.cameraRotSide = val;
    }

    public void AddXpCall()
    {
        AddXpEffect();
    }
  
    public void Labels_on()
    {
        label_onbool = !label_onbool;
        if (!label_onbool)
        {
            for (int i = 0; i < Labels.Length; i++)
            {
                Labels[i].SetActive(true);
            }
        }
        else
        {
            for (int i = 0; i < Labels.Length; i++)
            {
                Labels[i].SetActive(false);
            }
        }
        
    }
    public void ResetButton()
    {
       
    }
}
//public class MaindragAndDrop : MonoBehaviour
//{
//    public int id;
//    private bool dragging = false;
//    private float distance, intialGroundTouchvalue;
//    private Vector3 init_pos, initialRotation;
//    RaycastHit hit;
//    private Vector3 temp;
//    private Vector3 screenSpace;
//    private Vector3 offSet;
//    private Camera cam;
//    private GameObject Target_collder, Target_collder2, TagetPos, TagetPos2;
//    private bool CorrectPlace;
//    public orbit CamOrbit;
//    public bool IsAR;
//    public Vector3 relativePos, TargetPos, TargetPos_Init, Magnet_oldPos, Magnet_oldRot;
//    private float Initial_Dist;
//    public chargetomassratioelectron chargetomassratioelectron;
//    private int placePos;
//    public AROrbitControls ArOrbit;
//    public void Start()
//    {
//        placePos = 0;
//        CorrectPlace = false;
//        cam = Camera.main.GetComponent<Camera>();
//        IsAR = Camera.main.GetComponentInParent<orbit>() ? false : true;
//        chargetomassratioelectron = GameObject.Find("MainObject").GetComponent<chargetomassratioelectron>();

//        if (!IsAR)
//        {
//            CamOrbit = Camera.main.GetComponentInParent<orbit>();


//            TargetPos_Init = CamOrbit.target.transform.position;
//            //TargetPos = MidObj.transform.position;

//            Initial_Dist = orbit.distance;
//        }
//        else
//        {
//            //CamOrbit = chargetomassratioelectron.Instance.MainObj.transform.parent.gameObject.AddComponent<AROrbitControls>();
//            //  CamOrbit = chargetomassratioelectron.Instance.MainObj.transform.parent.gameObject.AddComponent<AROrbitControls>();
//            ArOrbit = chargetomassratioelectron.Instance.MainObj.transform.parent.gameObject.GetComponent<AROrbitControls>();
//            //ArOrbit = MainObj.transform.parent.gameObject.GetComponent<AROrbitControls>();
//        }

//        Target_collder = GameObject.Find(gameObject.name + "_Collider");
//        Target_collder2 = GameObject.Find(gameObject.name + "_Collider2");
//        TagetPos = GameObject.Find("MagnetPos");
//        TagetPos2 = GameObject.Find("MagnetStartPos");
//       // Target_collder.AddComponent<ColorLerp>();
//       // Target_collder2.AddComponent<ColorLerp>();
//        //dummyObject = GameObject.Find(gameObject.name + "_Dummy");

//        //initialRotation = transform.eulerAngles;

//        if (Target_collder)
//        {
//            Target_collder.SetActive(false);
//            Target_collder2.SetActive(false);
//        }

//        // dummyObject.SetActive(false);






//        EventTrigger.Entry PointerDownentry = new EventTrigger.Entry();
//        PointerDownentry.eventID = EventTriggerType.PointerDown;
//        PointerDownentry.callback.AddListener((data) => { MouseDown(); });

//        gameObject.GetComponent<EventTrigger>().triggers.Add(PointerDownentry);

//        EventTrigger.Entry PointerUpentry = new EventTrigger.Entry();
//        PointerUpentry.eventID = EventTriggerType.PointerUp;
//        PointerUpentry.callback.AddListener((data) => { MouseUp(); });

//        gameObject.GetComponent<EventTrigger>().triggers.Add(PointerUpentry);



//    }

//    void MouseDown()
//    {



//        if (!CorrectPlace)
//        {
//            //orbit.orbit_enabled = false;

//            init_pos = transform.position;

//            initialRotation = transform.eulerAngles;

//            if (placePos == 0)
//            {
//                Target_collder.SetActive(true);
//                Target_collder2.SetActive(false);
//            }
//            else
//            {
//                Target_collder.SetActive(false);
//                Target_collder2.SetActive(true);
//            }

//            this.GetComponent<Collider>().enabled = false;
//            //dummyObject.SetActive(true);
//            chargetomassratioelectron.arrow.transform.localPosition = chargetomassratioelectron.arrow_pos2.transform.localPosition;
//            DisableOrbit();

//            screenSpace = cam.WorldToScreenPoint(transform.position);
//            offSet = transform.position - cam.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenSpace.z));

//            Vector3 curScreenSpace = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenSpace.z);
//            Vector3 curPosition = cam.ScreenToWorldPoint(curScreenSpace) + offSet;

//            intialGroundTouchvalue = curPosition.y;
//            dragging = true;



//        }
//    }

//    public void DisableOrbit()
//    {
//        if (!IsAR)
//        {

//            CamOrbit.DisableInteration = true;


//        }
//        else
//        {
//            ArOrbit.DisableInteration = true;
//        }

//    }
//    void MouseUp()
//    {
//        //if (controllerLevel1.SeqCount == id) {
//        if (!CorrectPlace)
//        {

//            Ray ray = cam.ScreenPointToRay(Input.mousePosition);

//            if (Physics.Raycast(ray, out hit, 100))
//            {

//                if (hit.collider.name == Target_collder.name)
//                {

//                    // print(hit.collider.name);
//                    gameObject.transform.localPosition = TagetPos.transform.localPosition;
//                    gameObject.transform.localEulerAngles = TagetPos.transform.localEulerAngles;
//                    chargetomassratioelectron.magnetic_bool = true;
//                   // placePos = 1;
//                    chargetomassratioelectron.electricarrows.SetActive(false);
//                    chargetomassratioelectron.magneticarrows.SetActive(true);
//                    chargetomassratioelectron.arrow.SetActive(false);
//                    if (chargetomassratioelectron.OptionUnlockedTill < 1)
//                    {
//                        if (chargetomassratioelectron.TopicToCheck == 4)
//                        {
//                            chargetomassratioelectron.AddXpEffect();
//                        }
//                    }
//                }
//                else if (hit.collider.name == Target_collder2.name)
//                {
//                    gameObject.transform.localPosition = TagetPos2.transform.localPosition;
//                    gameObject.transform.localEulerAngles = TagetPos2.transform.localEulerAngles;
//                    chargetomassratioelectron.magnetic_bool = false;
//                    chargetomassratioelectron.electricarrows.SetActive(false);
//                    chargetomassratioelectron.magneticarrows.SetActive(false);
//                    placePos = 0;
//                }
//                else
//                {
//                    // InvokeRepeating("PlacemnetAdjustment", 0.1f, 0.01f);
//                    transform.position = init_pos;
//                    transform.eulerAngles = initialRotation;
//                    this.GetComponent<Collider>().enabled = true;
//                }

//            }
//            else
//            {
//                // InvokeRepeating("PlacemnetAdjustment", 0.1f, 0.01f);
//                transform.position = init_pos;
//                transform.eulerAngles = initialRotation;
//                this.GetComponent<Collider>().enabled = true;

//            }

//            // InvokeRepeating("PlacemnetAdjustment", 0.1f, 0.01f);
//            this.GetComponent<Collider>().enabled = true;
//            Target_collder.SetActive(false);
//            Target_collder2.SetActive(false);
//            //dummyObject.SetActive(false);
//            // orbit.orbit_enabled = true;
//            EnableOrbit();
//            dragging = false;
//        }
//    }

//    public void EnableOrbit()
//    {
//        if (!IsAR)
//        {
//            CamOrbit.DisableInteration = false;


//        }
//        else
//        {
//            ArOrbit.DisableInteration = false;
//        }

//        //if (InformationPanel.gameObject.transform.localScale.x != 1)
//        //{

//        //    if (TopicToCheck <= (TaskList.Count))
//        //    {
//        //        iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "time", 0.2f, "easetype", iTween.EaseType.linear));
//        //        iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));
//        //        ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen");
//        //    }
//        //}

//    }
//    //bool CheckCondition()
//    //{

//    //    bool b = false;
//    //    int i = 0;

//    //    while (i < chargetomassratioelectron.Instance.DragObjects.Length - 1)
//    //    {
//    //        //if (chargetomassratioelectron.Instance.DragObjects[i].transform.localPosition == chargetomassratioelectron.Instance.DummyObjects[i].transform.localPosition)

//    //        if (chargetomassratioelectron.Instance.DragObjects[i].transform.localPosition == chargetomassratioelectron.Instance.DummyObjects[i].transform.localPosition)
//    //        {
//    //            b = true;
//    //        }
//    //        else { b = false; }
//    //        i++;
//    //    }
//    //    return b;
//    //}


//    void PlacemnetAdjustment()
//    {
//        if (CorrectPlace)
//        {
//            if (transform.position != Target_collder.transform.position)
//            {
//                transform.position = Vector3.Lerp(transform.position, Target_collder.transform.position, 0.1f);
//                transform.eulerAngles = Vector3.Lerp(transform.eulerAngles, Target_collder.transform.eulerAngles, 0.1f);
//            }
//            else
//            {
//                CancelInvoke("PlacemnetAdjustment");
//            }
//        }
//        else
//        {
//            if (transform.position != init_pos)
//            {
//                transform.position = Vector3.Lerp(transform.position, init_pos, 0.1f);
//                transform.eulerAngles = Vector3.Lerp(transform.eulerAngles, initialRotation, 0.1f);
//            }
//            else
//            {
//                CancelInvoke("PlacemnetAdjustment");

//            }
//        }
//    }


//    void Update()
//    {

//        if (dragging)
//        {
//            Vector3 curScreenSpace = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenSpace.z);
//            Vector3 curPosition = cam.ScreenToWorldPoint(curScreenSpace) + offSet;
//            //curPosition = new Vector3 (curPosition.x, Mathf.Clamp (curPosition.y, intialGroundTouchvalue+GroundTouchValue, 100f), curPosition.z);
//            transform.position = curPosition;
//        }
//    }

//    //public void EnableOrbit()
//    //{
//    //    if (!IsAR)
//    //    {
//    //        CamOrbit.orbit_enabled = true;
//    //    }
//    //    else
//    //    {
//    //        ARorbit.orbit_enabled = true;
//    //    }
//    //}

//    //public void DisableOrbit()
//    //{

//    //    if (!IsAR)
//    //    {
//    //        CamOrbit.orbit_enabled = false;
//    //    }
//    //    else
//    //    {
//    //        ARorbit.orbit_enabled = false;
//    //    }
//    //}
//}
