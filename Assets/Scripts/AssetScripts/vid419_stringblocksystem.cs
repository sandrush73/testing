﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;

public class vid419_stringblocksystem : MonoBehaviour, IPointerDownHandler
{
    // Start is called before the first frame update

    public Canvas C_BG;

    public orbit CamOrbit;

    public AROrbitControls ArOrbit;

    public bool IsAR;

    public GameObject MainObj, Ground;

    public int OptionUnlockedTill, CurrentTopicSelected, TopicToCheck;

    public float Sub_XP_Earned, Total_XP;

    public Dropdown Options_DD, ToDoList_DD;

    public GameObject InstructionPanel_DD, ToDoList_Panel;

    public List<Dictionary<string, object>> CSV_data;

    public List<String> TaskList,XpList,InfoText;

    public List<GameObject> AllTaskData;

    public GameObject[] Labels;

    public Text XP_ToShow, XP_Effect, Main_XP_ToShow, localTotalXpValueText, localGainedXpValueText, ObjectiveInfoText, LGListText, IListText, AListText, 
        InfoHeadingText, GF_ResultTxt, E_valueTxt, m_valtxt;

    public GameObject TaskObj, AllTaskListObj;

    public RectTransform rT;

    public int localTotalXpValue, localGaniedXpValue,MainXpValue;

    public GameObject ControlPanel, LocalXP, Main_Xp, ObjectivePanel, InformationPanel, InfoButton, MainSlider, InfoPanelBg, TasklistPanel, WronOptionPanel, Gained_XP_ArrowBut;

    public Button InfoCloseBtn, InfoContinueBtn, Gained_XP_Panel, CpCloseBtn, IncButton1, DecButton1, IncButton2, DecButton2, DecButton3, IncButton3;
       
    public bool sessionStart;

    public TextAsset CSVFile;

    public AssetBundle assetBundle;
       
    public string[] CorrectMsgs = new string[] { "Nice!", "WellDone!", "Excellent!", "Correct!" };

    public Color Highlight_Color, Normal_Color;

    public Text MessageT, DescText;

    //......................

    public Button StartBut;

    public GameObject mainBox1,mainBox2,Box1,Box2, Arrows1, Arrows2, SetUp, groundPlane, weight1_label, weight2_label,T_label, Acc_label, Zekie, ZekkiLeftHand;

    public Text M1_value_txt, M2_value_txt, a_valuetxt, T_valuetxt, F_valuetxt;

    public SkinnedMeshRenderer AngleBlend, arrow_blend_Force, arrow_blend_angle;

    public float acclartn, tensn, totalM, CosTheta, F,t, TempSliderVal;

    public TextMeshPro Mass1_Tex, Mass2_Tex, ForceDisply_Text, AngelDispl_Text;

    public GameObject groundPlaneAR;
    void Start()
    {

        LoadFromAsssetBundle();


        MainObj = this.gameObject;

        C_BG = GameObject.Find("Canvas_BG").GetComponent<Canvas>();

        Ground = GameObject.Find("Ground");

        groundPlaneAR = GameObject.Find("groundPlaneAR");

        if (!IsAR)
        {
            CamOrbit = Camera.main.GetComponentInParent<orbit>();

            C_BG.transform.parent = Camera.main.transform;
            C_BG.GetComponentInChildren<RawImage>().enabled = true;
            C_BG.renderMode = RenderMode.ScreenSpaceCamera;

            C_BG.worldCamera = Camera.main;

            CamOrbit.target = Camera.main.transform.parent;

            CamOrbit.target.position = new Vector3(0, 0.1f, 0);
            CamOrbit.maxRotUp = 10;
            CamOrbit.minRotUp = 1;
            CamOrbit.zoomMin = 1;
            CamOrbit.zoomMax = 2;
            CamOrbit.zoomStart = 1.5f;

            CamOrbit.Start();

            groundPlaneAR.SetActive(false);
        }
        else
        {
            Ground.SetActive(false);
            groundPlaneAR.SetActive(true);
            C_BG.gameObject.SetActive(false);

            ArOrbit = MainObj.transform.parent.gameObject.GetComponent<AROrbitControls>();
        }




        sessionStart = false;

        //OptionUnlockedTill = 0;
        //TopicToCheck = 1;
        //MainXpValue = 10;
        ReadingDataFromCSVFile();
        ReadingXps();

        FindingObjects();
        TaskListCreation();
        AssigningClickEvents();
        LabelsAssignment();
        AssignInitialValues();
    }
    public void LoadFromAsssetBundle()
    {

        //......................................................For Testing ..............................................................

       // CSVFile = Resources.Load("vid419_stringblocksystemCSV") as TextAsset;

        //............................................................ For Assetbundle Creation ..........................................

        assetBundle = AssetBundle.LoadFromFile(PlayerPrefs.GetString("AssetBundleUrl"));

        CSVFile = assetBundle.LoadAsset(PlayerPrefs.GetString("LibraryName") + "CSV", typeof(TextAsset)) as TextAsset;
        

        //...............................................................................................................................
        Camera.main.gameObject.AddComponent<PhysicsRaycaster>();

        Camera.main.backgroundColor = Color.black;

        GameObject TargetForCamera = GameObject.Find("Target");

        if (TargetForCamera != null)
        {
            TargetForCamera.AddComponent<orbit>();
            CamOrbit = TargetForCamera.GetComponent<orbit>();
            IsAR = false;
        }
        else
        {
            this.transform.parent.gameObject.AddComponent<AROrbitControls>();
            ArOrbit = this.transform.parent.gameObject.GetComponent<AROrbitControls>();
            IsAR = true;
        }

    }
    public void ReadingDataFromCSVFile()
    {

        TaskList = new List<string>();
        XpList = new List<string>();
        InfoText = new List<string>();

        AllTaskData = new List<GameObject>();

        CSV_data = CSVReader.Read(CSVFile);

        for (var i = 0; i < CSV_data.Count; i++)
        {
            if (CSV_data[i]["TaskList"].ToString() != "" && CSV_data[i]["TaskList"].ToString() != null)
            {
                TaskList.Add(CSV_data[i]["TaskList"].ToString());
            }

            //if (CSV_data[i]["XPList"].ToString() != "" && CSV_data[i]["XPList"].ToString() != null)
            //{
            //    XpList.Add(CSV_data[i]["XPList"].ToString());
            //}

            if (CSV_data[i]["InfoText"].ToString() != "" && CSV_data[i]["InfoText"].ToString() != null)
            {
                InfoText.Add(CSV_data[i]["InfoText"].ToString());
            }
        }
    }

    public int totXp, loclXp;

    public void ReadingXps()
    {

        totXp = PlayerPrefs.GetInt("TotalXP");
        loclXp = PlayerPrefs.GetInt("MaxEarnings");

        if (loclXp != 0)
        {
            OptionUnlockedTill = 0;

        }
        else
        {
            OptionUnlockedTill = 1;

        }

        TopicToCheck = 1;

        MainXpValue = totXp;

        int t = 0;
        int p = 0;

        for (int k = 0; k < TaskList.Count; k++)
        {
            p = loclXp / TaskList.Count;
            XpList.Add(p.ToString());
            t = t + p;
        }

        if (t != loclXp)
        {
            XpList[XpList.Count - 1] = (p + (loclXp - t)).ToString();
        }
    }
    public float result;

    public void GetResult()
    {
        result = ((float)(TopicToCheck - 1) / (float)(TaskList.Count)) * 100;
        Camera.main.SendMessage("UnloadActually", result);
    }
    public void FindingObjects() {

        InformationPanel = GameObject.Find("InformationPanel");

        InfoCloseBtn = GameObject.Find("InfoCloseBtn").GetComponent<Button>();

        InfoContinueBtn = GameObject.Find("InfoContinueBtn").GetComponent<Button>();

        InfoPanelBg = GameObject.Find("InfoPanelBg");
        
        XP_ToShow = GameObject.Find("localGainedXpValueText").GetComponent<Text>();

        XP_Effect = GameObject.Find("Effect_Text").GetComponent<Text>();

        Main_XP_ToShow = GameObject.Find("Main_XP_Value").GetComponent<Text>();

        localGainedXpValueText = GameObject.Find("localGainedXpValueText").GetComponent<Text>();

        localTotalXpValueText = GameObject.Find("GainedXP_LocalTotalXp").GetComponent<Text>();

        ObjectiveInfoText = GameObject.Find("ObjectiveInfoText").GetComponent<Text>();

        TaskObj = GameObject.Find("TaskObj");

        AllTaskListObj = GameObject.Find("AllTaskList");

        ControlPanel = GameObject.Find("ControlPanel");
        LocalXP = GameObject.Find("LocalXP");
        Main_Xp = GameObject.Find("Main_Xp");
        ObjectivePanel = GameObject.Find("ObjectivePanel");
        InformationPanel = GameObject.Find("InformationPanel");
        InfoButton = GameObject.Find("InfoButton");
        MainSlider = GameObject.Find("MainSlider");
        WronOptionPanel = GameObject.Find("WrongPanel");

        Gained_XP_Panel = GameObject.Find("Gained_XP_Panel").GetComponent<Button>();
        TasklistPanel = GameObject.Find("TasklistPanel");
        CpCloseBtn = GameObject.Find("CpCloseBtn").GetComponent<Button>();

        InfoHeadingText = GameObject.Find("InfoHeadingText").GetComponent<Text>();
        LGListText = GameObject.Find("LGListText").GetComponent<Text>();
        IListText = GameObject.Find("IListText").GetComponent<Text>();
        AListText = GameObject.Find("AListText").GetComponent<Text>();

        Gained_XP_ArrowBut = GameObject.Find("Gained_XP_ArrowBut");

        //......................................................

        IncButton1 = GameObject.Find("IncButton1").GetComponent<Button>();

        DecButton1 = GameObject.Find("DecButton1").GetComponent<Button>();

        IncButton2 = GameObject.Find("IncButton2").GetComponent<Button>();

        DecButton2 = GameObject.Find("DecButton2").GetComponent<Button>();

        IncButton3 = GameObject.Find("IncButton3").GetComponent<Button>();

        DecButton3 = GameObject.Find("DecButton3").GetComponent<Button>();

        //................................................................................

        SetUp = GameObject.Find("SetUp");

        mainBox1 = GameObject.Find("MainBox1");
        mainBox2 = GameObject.Find("MainBox2");

        Box1 = GameObject.Find("Box1");
        Box2 = GameObject.Find("Box2");

        M1_value_txt = GameObject.Find("M1_value_txt").GetComponent<Text>();

        M2_value_txt = GameObject.Find("M2_value_txt").GetComponent<Text>();

        F_valuetxt = GameObject.Find("F_valuetxt").GetComponent<Text>();

        Arrows1 = GameObject.Find("Arrows1");

        AngleBlend = GameObject.Find("AngleBlend1").GetComponent<SkinnedMeshRenderer>();

        arrow_blend_Force = GameObject.Find("arrow_blend_Force").GetComponent<SkinnedMeshRenderer>();

        arrow_blend_angle = GameObject.Find("arrow_blend_angle").GetComponent<SkinnedMeshRenderer>();

        StartBut = GameObject.Find("Start").GetComponent<Button>();

        //SliderMass1 = GameObject.Find("SliderMass1").GetComponent<Slider>();
        //SliderMass2 = GameObject.Find("SliderMass2").GetComponent<Slider>();

        //Mass1Text = GameObject.Find("m1_value").GetComponent<Text>();
        //Mass2Text = GameObject.Find("m2_value").GetComponent<Text>();

        groundPlane = GameObject.Find("groundPlane");

        AddListener(EventTriggerType.PointerDown, DisableOrbit, MainSlider.gameObject);

        AddListener(EventTriggerType.PointerUp, EnableOrbit, MainSlider.gameObject);

        MainSlider.GetComponent<Slider>().onValueChanged.AddListener(delegate { AngleSlider(MainSlider.GetComponent<Slider>().value, MainSlider.GetComponentInChildren<Text>()); });

        a_valuetxt = GameObject.Find("a_valuetxt").GetComponent<Text>();

        T_valuetxt = GameObject.Find("T_valuetxt").GetComponent<Text>();

        weight1_label = GameObject.Find("weight1_label");

        weight2_label = GameObject.Find("weight2_label");

        Acc_label = GameObject.Find("Acc_label");

        T_label = GameObject.Find("T_label");

        Mass1_Tex = GameObject.Find("Mass1_Tex").GetComponent<TextMeshPro>();

        Mass2_Tex = GameObject.Find("Mass2_Tex").GetComponent<TextMeshPro>();

        ForceDisply_Text = GameObject.Find("ForceDisply_Text").GetComponent<TextMeshPro>();

        AngelDispl_Text = GameObject.Find("AngelDispl_Text").GetComponent<TextMeshPro>();

        Zekie = GameObject.Find("Zekie");

        ZekkiLeftHand = GameObject.Find("ZekkiLeftHand");

    }

    public void AssigningClickEvents() {
                
        iTween.ScaleTo(InformationPanel.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

        InfoCloseBtn.onClick.AddListener(() => CloseInfoPanel());

        InfoContinueBtn.onClick.AddListener(() => ContinueInfoPanel());

        InfoButton.GetComponent<Button>().onClick.AddListener(() => OpenInfoPanel());

        Gained_XP_Panel.onClick.AddListener(() => OpenTaskList());

        CpCloseBtn.onClick.AddListener(() => OpenOrCloseControlPanel());
        
        IncButton1.GetComponent<Button>().onClick.AddListener(() => ChangeBallMass(0.2f, M1_value_txt,Box1,Arrows1, weight1_label, Mass1_Tex));
        DecButton1.GetComponent<Button>().onClick.AddListener(() => ChangeBallMass(-0.2f, M1_value_txt,Box1, Arrows1, weight1_label, Mass1_Tex));

        IncButton2.GetComponent<Button>().onClick.AddListener(() => ChangeBallMass(0.2f, M2_value_txt, Box2, Arrows2, weight2_label, Mass2_Tex));
        DecButton2.GetComponent<Button>().onClick.AddListener(() => ChangeBallMass(-0.2f, M2_value_txt, Box2, Arrows2, weight2_label, Mass2_Tex));
        
        IncButton3.GetComponent<Button>().onClick.AddListener(() => ChangeForce(1f, F_valuetxt));
        DecButton3.GetComponent<Button>().onClick.AddListener(() => ChangeForce(-1f, F_valuetxt));

        StartBut.onClick.AddListener(() => StartFuntion());

    }

    public void AssignInitialValues()
    {
        //InfoHeadingText.text = InfoText[0];
        //LGListText.text = InfoText[1];
        //IListText.text = InfoText[2];
        //AListText.text = InfoText[3];
        if (OptionUnlockedTill > 0)
        {
            LocalXP.transform.localScale = new Vector3(0, 0, 0);
            ObjectivePanel.transform.localScale = new Vector3(0, 0, 0);
        }

        t = 0;        
        
        acclartn = 0;
        tensn = 0;
        CosTheta = 0;
        F = 1;
        totalM = 2;

        InfoCloseBtn.gameObject.transform.localScale = new Vector3(0, 0, 0);
        localGainedXpValueText.text = "0";
        Main_XP_ToShow.text = MainXpValue + "";
        WronOptionPanel.transform.localScale = new Vector3(0, 0, 0);
        
        M1_value_txt.text = "1.0";
        M2_value_txt.text = "1.0";

        Acc_label.GetComponentInChildren<TextMeshPro>().text = "a = "+acclartn.ToString("F1");
    }


    public void TaskListCreation() {

        rT = AllTaskListObj.transform.parent.GetComponent<RectTransform>();

        localGaniedXpValue = 0;

        for (int i = 0; i < TaskList.Count; i++)
        {

            GameObject Tl = (GameObject)Instantiate(TaskObj);
            Tl.transform.SetParent(AllTaskListObj.transform);
            Tl.transform.localScale = new Vector3(1, 1, 1);
            Tl.name = i.ToString();

            AllTaskData.Add(Tl);
            AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(false);
            AllTaskData[i].gameObject.transform.GetChild(2).gameObject.SetActive(false);
            AllTaskData[i].gameObject.transform.GetChild(3).GetComponentInChildren<Text>().text = TaskList[i];
            AllTaskData[i].gameObject.transform.GetChild(4).GetComponentInChildren<Text>().text = "+" + XpList[i] + " XP";

            localTotalXpValue += int.Parse(XpList[i]);
        }

        rT.sizeDelta = new Vector2(rT.sizeDelta.x, TaskList.Count * 50);
        localTotalXpValueText.text = "/" + localTotalXpValue.ToString();

        AllTaskData[0].gameObject.transform.GetChild(2).gameObject.SetActive(true);
        ObjectiveInfoText.text = AllTaskData[0].gameObject.transform.GetChild(3).GetComponentInChildren<Text>().text;
    }

    public void LocalXpCalculation()
    {
        for (int i = 0; i < TaskList.Count; i++)
        {
            AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(false);
            AllTaskData[i].gameObject.transform.GetChild(2).gameObject.SetActive(false);
        }

        if (OptionUnlockedTill < 1)
        {
            if ((TopicToCheck) < (TaskList.Count + 1))
            {
                localGaniedXpValue = 0;

                for (int i = 0; i < TopicToCheck - 1; i++)
                {
                    localGaniedXpValue += int.Parse(XpList[i]);
                }

                for (int i = 0; i < TopicToCheck; i++)
                {
                    AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(true);
                }

                localGainedXpValueText.text = localGaniedXpValue.ToString();

                AllTaskData[TopicToCheck - 1].gameObject.transform.GetChild(2).gameObject.SetActive(true);

                ObjectiveInfoText.text = AllTaskData[TopicToCheck - 1].gameObject.transform.GetChild(3).GetComponentInChildren<Text>().text;
            }
            else
            {
                for (int i = 0; i < TaskList.Count; i++)
                {
                    AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(true);
                    AllTaskData[i].gameObject.transform.GetChild(2).gameObject.SetActive(false);
                }

                localGainedXpValueText.text = localTotalXpValue.ToString();

                XP_Effect.text = "+" + localTotalXpValue + " XP";

                XP_Effect.transform.position = (XP_ToShow.transform.position);

                iTween.Stop(XP_Effect.gameObject);

                iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeOutElastic));

                iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", XP_Effect.transform.position.x, "y", XP_Effect.transform.position.y + 50, "z", 0, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

                iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", Main_XP_ToShow.transform.position.x, "y", Main_XP_ToShow.transform.position.y, "z", 0, "delay", 1.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

                iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "delay", 2f, "time", 0.5f, "easetype", iTween.EaseType.linear, "oncomplete", "MainXpCalculation", "oncompletetarget", this.gameObject));
            }
        }
        else
        {
            for (int i = 0; i < TaskList.Count; i++)
            {
                AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(true);
                AllTaskData[i].gameObject.transform.GetChild(2).gameObject.SetActive(false);
            }

            localGainedXpValueText.text = localTotalXpValue.ToString();
        }
    }

    public void CloseAllpanels()
    {

        ControlPanel.GetComponent<Animator>().Play("CpanelFullClose");
        LocalXP.GetComponent<Animator>().Play("LocalXpPanelClose");
        Main_Xp.GetComponent<Animator>().Play("MainXpPanelClose");
        ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelClose");
        InfoButton.GetComponent<Animator>().Play("InfoButtonClose");
        MainSlider.GetComponent<Animator>().Play("MainSliderClose");
        
        if(TasklistPanel.gameObject.transform.localScale.y!=0)
            iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));
    }

    public void OpenAllpanels()
    {
        if (OptionUnlockedTill < 1)
        {
            if (TopicToCheck <= (TaskList.Count))
            {
                LocalXP.GetComponent<Animator>().Play("LocalXpPanelOPen");
                ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen");
            }
        }
        ControlPanel.GetComponent<Animator>().Play("CpanelOpen");
        
        Main_Xp.GetComponent<Animator>().Play("MainXpPanelOpen");
       
        InfoButton.GetComponent<Animator>().Play("InfoButtonOpen");
        MainSlider.GetComponent<Animator>().Play("MainSliderOpen");
        iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "time", 0.2f, "easetype", iTween.EaseType.linear));
        iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));
    }

    public void AddXpEffect() {
                
        if (TopicToCheck <= (TaskList.Count))
        {            

            GameObject.Find("Canvas").GetComponent<CanvasGroup>().interactable = false;

            XP_Effect.GetComponent<RectTransform>().anchoredPosition = new Vector2(0,0);

            XP_Effect.text = "+"+int.Parse(XpList[TopicToCheck - 1]) + " XP";

            XP_Effect.transform.localScale = new Vector3(1, 0, 1);

            iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeOutElastic));

            iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", XP_Effect.transform.position.x, "y", XP_Effect.transform.position.y + 50, "z", 0, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

            iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", XP_ToShow.transform.position.x, "y", XP_ToShow.transform.position.y, "z", 0, "delay", 1.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

            iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "delay", 2f, "time", 0.5f, "easetype", iTween.EaseType.linear, "oncomplete", "LocalXpCalculation", "oncompletetarget", this.gameObject));

            TopicToCheck++;
            ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelClose");
            Invoke("EnableRayCast", 3);
        }
        
    }

    public void EnableRayCast() {
        //Camera.main.GetComponent<PhysicsRaycaster>().enabled = true;
        GameObject.Find("Canvas").GetComponent<CanvasGroup>().interactable = true;
        if (TopicToCheck <= (TaskList.Count))
        {

            ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen");
        }
    }

    public void MainXpCalculation()
    {
        if (OptionUnlockedTill == 0)
        {
            OptionUnlockedTill = 1;

            MainXpValue += localTotalXpValue;

            Main_XP_ToShow.text = MainXpValue.ToString();

            LocalXP.GetComponent<Animator>().Play("LocalXpPanelClose");

            ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelClose");

            if ((ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelClose")) || (ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelFullClose")))
            {
                ControlPanel.GetComponent<Animator>().Play("CpanelOpen");
            }
        }
    }

    public void CloseInfoPanel()
    {
        sessionStart = true;
        InfoPanelBg.SetActive(false);
        iTween.ScaleTo(InformationPanel.gameObject, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));
        OpenAllpanels();
    }

    public void ContinueInfoPanel()
    {
        sessionStart = true;
        InfoPanelBg.SetActive(false);
        iTween.ScaleTo(InformationPanel.gameObject, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));
        InfoCloseBtn.transform.localScale = new Vector3(0.27f, 0.27f, 0.27f);
        InfoContinueBtn.transform.localScale = new Vector3(0, 0, 0);
        OpenAllpanels();
    }

    public void OpenInfoPanel()
    {
        sessionStart = false;
        iTween.ScaleTo(InformationPanel.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));
        InfoPanelBg.SetActive(true);
        iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "time", 0.2f, "easetype", iTween.EaseType.linear));
        iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));
        CloseAllpanels();
    }

    public void OpenTaskList() {


        if (TasklistPanel.gameObject.transform.localScale.y != 1)
        {
            iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.01f, "time", 0.2f, "easetype", iTween.EaseType.easeInSine));

            ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelClose");

            iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 180, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));

            if ((ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelOpen")))
            {
                ControlPanel.GetComponent<Animator>().Play("CpanelClose");
            }
        }
        else
        {
            iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "delay", 0.01f, "time", 0.2f, "easetype", iTween.EaseType.easeInSine));
            iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));

            ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen");

            ControlPanel.GetComponent<Animator>().Play("CpanelOpen");
        }
    }

    public void OpenOrCloseControlPanel() {

        if ((ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelClose")) || (ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelFullClose")))
        {
            ControlPanel.GetComponent<Animator>().Play("CpanelOpen");
        }
        else
        {
            ControlPanel.GetComponent<Animator>().Play("CpanelClose");
        }
    }

    public void WrongOptionSelected()
    {
        WronOptionPanel.transform.localScale = new Vector3(0, 0, 0);

        iTween.Stop(WronOptionPanel.gameObject);

        iTween.ScaleTo(WronOptionPanel.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.01f, "time", 0.2f, "easetype", iTween.EaseType.easeInSine));

        iTween.ScaleTo(WronOptionPanel.gameObject, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 1f, "time", 0.2f, "easetype", iTween.EaseType.easeInSine));

        // InfoPanelBg.SetActive(true);

        Invoke("CloseBlurBg", 1f);

        GameObject.Find("Canvas").GetComponent<CanvasGroup>().interactable = false;

        ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelClose");


        var mouse = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);

        if (mouse.x < Screen.width / 2)
        {
            // WronOptionPanel
            //print("Mouse is on left side of screen.");
            WronOptionPanel.GetComponent<RectTransform>().pivot = new Vector2(0, 0);
        }

        if (mouse.x > Screen.width / 2)
        {
            //print("Mouse is on right side of screen.");
            WronOptionPanel.GetComponent<RectTransform>().pivot = new Vector2(1, 0);
        }


        WronOptionPanel.transform.position = Input.mousePosition;
    }

    public void CloseBlurBg()
    {
        InfoPanelBg.SetActive(false);
        GameObject.Find("Canvas").GetComponent<CanvasGroup>().interactable = true;

        if (OptionUnlockedTill < 1)
        {
            if (InformationPanel.gameObject.transform.localScale.x == 0f)
            {
                //ObjectivePanel.GetComponent<Animator>().Play("");
                ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen", 0, 0f);
            }
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (OptionUnlockedTill < 1)
        {
            if (InformationPanel.gameObject.transform.localScale.x != 1 && sessionStart)
            {            
                if (TopicToCheck <= (TaskList.Count))
                {
                    iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "time", 0.2f, "easetype", iTween.EaseType.linear));
                    iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));
                    ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen");
                    ControlPanel.GetComponent<Animator>().Play("CpanelOpen");
                }
            }  
        }  
    }

    public void LabelsAssignment()
    {
        GameObject Label = GameObject.Find("Labels");

        Labels = new GameObject[Label.transform.childCount];

        for (int j = 0; j < Labels.Length; j++)
        {
            Labels[j] = Label.transform.GetChild(j).gameObject;

            GameObject TempLabels = Labels[j];

            foreach (Transform ChildLabel in TempLabels.transform)
            {
                if (ChildLabel.name.Contains("SmoothLook"))
                {
                    ChildLabel.gameObject.AddComponent<SmoothLook>();
                    //ChildLabel.gameObject.AddComponent<ScaleRelativeToCamera>();

                }
            }
        }

        for (int k = 0; k < Labels.Length; k++)
        {
            string name = Labels[k].name.Remove(Labels[k].name.Length - 6);

            if (GameObject.Find(name + "") != null)
            {
                GameObject dummy = GameObject.Find(name + "").gameObject;

                Labels[k].transform.parent = dummy.transform;
                Labels[k].transform.rotation = Quaternion.identity;
            }

        }


    }

    public void AddListener(EventTriggerType eventType, Action MethodToCall, GameObject TriggerObjToAdd)
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall());
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }

    public void AddListener(EventTriggerType eventType, Action<float> MethodToCall, GameObject TriggerObjToAdd)
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall(TriggerObjToAdd.GetComponent<Slider>().value));
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }

    public void EnableOrbit()
    {
        if (!IsAR)
        {
            CamOrbit.DisableInteration = false;
        }
        else
        {
            ArOrbit.DisableInteration = false;
        }

        if (OptionUnlockedTill < 1)
        {
            if (InformationPanel.gameObject.transform.localScale.x != 1)
            {
                if (TopicToCheck <= (TaskList.Count))
                {
                    iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "time", 0.2f, "easetype", iTween.EaseType.linear));
                    iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));
                    ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen");
                }
            }
        }

        if (OptionUnlockedTill < 1)
        {
            if (StartBut.GetComponentInChildren<Text>().text == "Start")
            {
                if (TopicToCheck == 4)
                {
                    if (MainSlider.GetComponent<Slider>().value != 20)
                    {

                    }
                    else
                    {
                        MainSlider.GetComponent<Slider>().value = TempSliderVal;
                        WrongOptionSelected();
                        WronOptionPanel.GetComponentInChildren<Text>().text = "Change angle with slider";
                    }
                }
                else
                {
                    MainSlider.GetComponent<Slider>().value = TempSliderVal;
                    // MainSlider.GetComponent<Slider>().value = 20;
                    WrongOptionSelected();
                    WronOptionPanel.GetComponentInChildren<Text>().text = "Wrong parameter chosen";
                }
            }
            else
            {
                MainSlider.GetComponent<Slider>().value = TempSliderVal;
                WrongOptionSelected();
                WronOptionPanel.GetComponentInChildren<Text>().text = "Tap on Reset";
            }
        }
        else
        {
            if (StartBut.GetComponentInChildren<Text>().text == "Start")
            {
       
            }
            else
            {
                MainSlider.GetComponent<Slider>().value = TempSliderVal;
                WrongOptionSelected();
                WronOptionPanel.GetComponentInChildren<Text>().text = "Tap on Reset";
            }
        }
    }
       
    public void DisableOrbit()
    {
        if (!IsAR)
        {
            CamOrbit.DisableInteration = true;
        }
        else
        {
            ArOrbit.DisableInteration = true;
        }

        

    }
    
    public Vector3 MidPoint(GameObject obj1, GameObject obj2)
    {
        Vector3 midp = (obj1.transform.localPosition + obj2.transform.localPosition) / 2;
        return midp;
    }

    public float Scale(GameObject obj1, GameObject obj2)
    {
        float scl = Vector3.Distance(obj1.transform.localPosition, obj2.transform.localPosition);
        return scl;
    }   

    public void PopUpShow() {
       // DescPopUp.SetActive(true);
    }
           

    public void ChangeBallMass(float val,Text t, GameObject obj,GameObject Arrows,GameObject Label,TextMeshPro txt)
    {
        if (StartBut.GetComponentInChildren<Text>().text == "Start")
        {
            val += obj.transform.localScale.x;

            val = Mathf.Clamp(val, 1, 2);

            t.text = val.ToString("F1");

            obj.transform.localScale = val * Vector3.one;

            if (Arrows != null)
            {
                Arrows.transform.localPosition = new Vector3(val * 0.13f, Arrows.transform.localPosition.y, Arrows.transform.localPosition.z);
            }

            Label.transform.localPosition = new Vector3(Label.transform.localPosition.x, val * 0.15f, Label.transform.localPosition.z);


            txt.text = val.ToString("F1");


            if (OptionUnlockedTill < 1)
            {
                if (TopicToCheck == 1)
                {
                    if (t.name == "M1_value_txt")
                    {

                    }
                    else
                    {
                        if (M1_value_txt.text == "1.0")
                        {
                            WrongOptionSelected();
                            WronOptionPanel.GetComponentInChildren<Text>().text = "Change M₁";
                        }
                        else
                        {
                            WrongOptionSelected();
                            WronOptionPanel.GetComponentInChildren<Text>().text = "Wrong parameter chosen";
                        }
                    }

                }
                else if (TopicToCheck == 2)
                {
                    if (t.name == "M2_value_txt")
                    {

                    }
                    else
                    {
                        if (M2_value_txt.text == "1.0")
                        {
                            WrongOptionSelected();
                            WronOptionPanel.GetComponentInChildren<Text>().text = "Change M₂";
                        }
                        else
                        {
                            WrongOptionSelected();
                            WronOptionPanel.GetComponentInChildren<Text>().text = "Wrong parameter chosen";
                        }
                    }

                }
                else 
                {
                    WrongOptionSelected();
                    WronOptionPanel.GetComponentInChildren<Text>().text = "Wrong parameter chosen";
                }
               
            }
            
                
        }
        else {
            WrongOptionSelected();
            WronOptionPanel.GetComponentInChildren<Text>().text = "Tap on Reset";
        }

    }

    public void ChangeForce(float val, Text t) {

        if (StartBut.GetComponentInChildren<Text>().text == "Start")
        {
            val += float.Parse(t.text);

            val = Mathf.Clamp(val, 1, 5);

            t.text = val.ToString("F1");

            F = float.Parse(t.text);

            if (OptionUnlockedTill < 1)
            {
                if (TopicToCheck == 3)
                {
                    if (t.name == "F_valuetxt")
                    {

                    }
                    else
                    {
                        if (F_valuetxt.text == "1.0")
                        {
                            WrongOptionSelected();
                            WronOptionPanel.GetComponentInChildren<Text>().text = "Change Force";
                        }
                        else
                        {
                            WrongOptionSelected();
                            WronOptionPanel.GetComponentInChildren<Text>().text = "Wrong parameter chosen";
                        }
                    }
                }
                else
                {
                    WrongOptionSelected();
                    WronOptionPanel.GetComponentInChildren<Text>().text = "Wrong parameter chosen";
                }
            }
        }
        else
        {
            WrongOptionSelected();
            WronOptionPanel.GetComponentInChildren<Text>().text = "Tap on Reset";
        }
    }

    public void AngleSlider(float val, Text t)
    {
        if (StartBut.GetComponentInChildren<Text>().text == "Start")
        {
            t.text = val.ToString() + " °";

            Arrows1.transform.GetChild(2).gameObject.transform.localEulerAngles = new Vector3(-val, Arrows1.transform.GetChild(2).gameObject.transform.localEulerAngles.y, Arrows1.transform.GetChild(2).gameObject.transform.localEulerAngles.z);

            AngleBlend.SetBlendShapeWeight(0, 100-val*0.277f);

            CosTheta = Mathf.Cos(val*Mathf.Deg2Rad) ;

            TempSliderVal = MainSlider.GetComponent<Slider>().value;
            AngelDispl_Text.text = (MainSlider.GetComponent<Slider>().value).ToString()+ " °"; 
        }
        else
        {
            //if (OptionUnlockedTill < 1)
            //{
            //    if (TopicToCheck != 4)
            //    {
                    //MainSlider.GetComponent<Slider>().value = TempSliderVal;
                    //val = TempSliderVal;
                    //t.text = val.ToString() + " °";
                    //Arrows1.transform.GetChild(2).gameObject.transform.localEulerAngles = new Vector3(-val, Arrows1.transform.GetChild(2).gameObject.transform.localEulerAngles.y, Arrows1.transform.GetChild(2).gameObject.transform.localEulerAngles.z);
                    //AngleBlend.SetBlendShapeWeight(0, 100 - val * 0.277f);
                    //CosTheta = Mathf.Cos(val * Mathf.Deg2Rad);
                    //WrongOptionSelected();
                    //WronOptionPanel.GetComponentInChildren<Text>().text = "Tap on Reset";
            //    }
            //    else {
            //    }
            //}
            //else {
            //    MainSlider.GetComponent<Slider>().value = TempSliderVal;
            //    val = TempSliderVal;
            //    t.text = val.ToString() + " °";
            //    Arrows1.transform.GetChild(2).gameObject.transform.localEulerAngles = new Vector3(-val, Arrows1.transform.GetChild(2).gameObject.transform.localEulerAngles.y, Arrows1.transform.GetChild(2).gameObject.transform.localEulerAngles.z);
            //    AngleBlend.SetBlendShapeWeight(0, 100 - val * 0.277f);
            //    CosTheta = Mathf.Cos(val * Mathf.Deg2Rad);
            //    WrongOptionSelected();
            //    WronOptionPanel.GetComponentInChildren<Text>().text = "Tap on Reset";
            //}
        }
    }

    float tempPosX;
    Vector2 uv;

    public void StartFuntion() {

        if (OptionUnlockedTill < 1)
        {
            if (StartBut.GetComponentInChildren<Text>().text == "Start")
            {
                if (TopicToCheck == 1)
                {
                    if (M1_value_txt.text != "1.0")
                    {
                        AddXpEffect();
                        StartAction();
                    }
                    else {
                        WrongOptionSelected();
                        WronOptionPanel.GetComponentInChildren<Text>().text = "Change M₁";
                    }
                }
                else if (TopicToCheck == 2)
                {
                    if (M2_value_txt.text != "1.0")
                    {
                        AddXpEffect();
                        StartAction();
                    }
                    else
                    {
                        WrongOptionSelected();
                        WronOptionPanel.GetComponentInChildren<Text>().text = "Change M₂";
                    }
                }
                else if (TopicToCheck == 3)
                {
                    if (F_valuetxt.text != "1.0")
                    {
                        AddXpEffect();
                        StartAction();
                    }
                    else
                    {
                        WrongOptionSelected();
                        WronOptionPanel.GetComponentInChildren<Text>().text = "Change Force";
                    }
                }
                else if (TopicToCheck == 4)
                {
                    if (MainSlider.GetComponent<Slider>().value != 20)
                    {
                        AddXpEffect();
                        StartAction();
                    }
                    else
                    {
                        WrongOptionSelected();
                        WronOptionPanel.GetComponentInChildren<Text>().text = "Change Force";
                    }
                }
            }
            else
            {
                ResetAll();
            }
        }
        else {
            if (StartBut.GetComponentInChildren<Text>().text == "Start")
            {
                StartAction();
            }
            else
            {
                ResetAll();
            }
        }
    }

    public void StartAction() {

        CosTheta = Mathf.Cos(MainSlider.GetComponent<Slider>().value * Mathf.Deg2Rad);

        totalM = float.Parse(M1_value_txt.text) + float.Parse(M2_value_txt.text);

        acclartn = (F * CosTheta) / (totalM);

        tensn = (float.Parse(M2_value_txt.text) * CosTheta) / (totalM);

        a_valuetxt.text = acclartn.ToString("F2");

        T_valuetxt.text = tensn.ToString("F2");

        Acc_label.GetComponentInChildren<TextMeshPro>().text = "a = " + a_valuetxt.text;

        T_label.GetComponentInChildren<TextMeshPro>().text = "T = " + T_valuetxt.text;

        iTween.MoveTo(Zekie, iTween.Hash("x", 0.25f, "y", 0.41f, "z", Zekie.transform.position.z, "islocal", true, "delay", 0.01f, "time", 0.85f, "easetype", iTween.EaseType.easeInSine));
        iTween.RotateTo(Zekie, iTween.Hash("x", 0, "y", -35f, "z", 0, "islocal", true, "delay", 0.85f, "time", 0.45f, "easetype", iTween.EaseType.easeInSine));
        iTween.RotateTo(ZekkiLeftHand, iTween.Hash("x", 0, "y", 35f, "z", -35f, "islocal", true, "delay", 1.25f, "time", 0.25f, "easetype", iTween.EaseType.easeInSine));

        InvokeRepeating("MoveSystem", 1.5f, 0.01f);

        StartBut.GetComponentInChildren<Text>().text = "Reset";
        if (!IsAR)
        {
            uv = groundPlane.GetComponent<Renderer>().materials[0].GetTextureOffset("_MainTex");
        }
        else
        {
            uv = groundPlaneAR.GetComponent<Renderer>().materials[0].GetTextureOffset("_MainTex");
            
        }

        tempPosX = 0;

        mainBox1.transform.GetChild(1).gameObject.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(0, tensn * 50);

        mainBox2.transform.GetChild(1).gameObject.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(0, tensn * 50);

        arrow_blend_Force.SetBlendShapeWeight(0, F * 10+50);

        arrow_blend_angle.SetBlendShapeWeight(0, F *10+50);

        ForceDisply_Text.text = "F = " + F.ToString("F1");

        AngelDispl_Text.text = (MainSlider.GetComponent<Slider>().value).ToString() + " °";
    }

    public void ResetAll() {

        StartBut.GetComponentInChildren<Text>().text = "Start";
        tempPosX = 0;
        CancelInvoke("MoveSystem");
        Box1.transform.localScale = new Vector3(1, 1, 1);
        Box2.transform.localScale = new Vector3(1, 1, 1);
        M1_value_txt.text = "1.0";
        M2_value_txt.text = "1.0";

        Mass1_Tex.text = "1.0s";
        Mass2_Tex.text = "1.0s";
        MainSlider.GetComponent<Slider>().value = 20;

        iTween.MoveTo(Zekie.gameObject, iTween.Hash("x", -0.5f, "y", 0.6f, "z", Zekie.transform.position.z, "islocal", true, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));
        iTween.RotateTo(Zekie.gameObject, iTween.Hash("x", 0, "y", 0f, "z", 0, "islocal", true, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));
        iTween.RotateTo(ZekkiLeftHand, iTween.Hash("x", 0, "y", 0f, "z", 0f, "islocal", true, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

        groundPlane.GetComponent<Renderer>().materials[0].SetTextureOffset("_MainTex", new Vector2(0, 0));

        Arrows1.transform.localPosition = new Vector3(0.13f, Arrows1.transform.localPosition.y, Arrows1.transform.localPosition.z);

        weight1_label.transform.localPosition = new Vector3(weight1_label.transform.localPosition.x, 0.15f, weight1_label.transform.localPosition.z);

        weight2_label.transform.localPosition = new Vector3(weight2_label.transform.localPosition.x, 0.15f, weight2_label.transform.localPosition.z);

        mainBox1.transform.GetChild(1).gameObject.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(0, 0 * 50);

        mainBox2.transform.GetChild(1).gameObject.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(0, 0 * 50);

        arrow_blend_Force.SetBlendShapeWeight(0, 60);

        arrow_blend_angle.SetBlendShapeWeight(0, 60);

        a_valuetxt.text = "0";

        T_valuetxt.text = "0";

        Acc_label.GetComponentInChildren<TextMeshPro>().text = "a = " + a_valuetxt.text;

        F_valuetxt.text = "1.0";

        F = float.Parse(F_valuetxt.text);

        T_label.GetComponentInChildren<TextMeshPro>().text = "T = " + T_valuetxt.text;

        ForceDisply_Text.text = "F = " + F.ToString("F1");
        AngelDispl_Text.text = (MainSlider.GetComponent<Slider>().value).ToString() + " °";

    }

    public void MoveSystem() {

        t = t + 0.01f;

        tempPosX += acclartn*0.01f;
        if (!IsAR)
        {
            groundPlane.GetComponent<Renderer>().materials[0].SetTextureOffset("_MainTex", new Vector2(0, tempPosX));
        }
        else
        {
            groundPlaneAR.GetComponent<Renderer>().materials[0].SetTextureOffset("_MainTex", new Vector2(0, tempPosX));
        }

    }

}
