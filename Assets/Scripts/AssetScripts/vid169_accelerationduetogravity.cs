﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;

public class vid169_accelerationduetogravity : MonoBehaviour, IPointerDownHandler
{
    // Start is called before the first frame update

    public Canvas C_BG;

    public orbit CamOrbit;

    public AROrbitControls ArOrbit;

    public bool IsAR;

    public GameObject MainObj, Ground;

    public int OptionUnlockedTill, CurrentTopicSelected, TopicToCheck;

    public float Sub_XP_Earned, Total_XP;

    public Dropdown Options_DD, ToDoList_DD;

    public GameObject InstructionPanel_DD, ToDoList_Panel;

    public List<Dictionary<string, object>> CSV_data;

    public List<String> TaskList,XpList,InfoText;

    public List<GameObject> AllTaskData;

    public GameObject[] Labels;

    public Text XP_ToShow, XP_Effect, Main_XP_ToShow, localTotalXpValueText, localGainedXpValueText, ObjectiveInfoText, LGListText, IListText, AListText, 
        InfoHeadingText, GF_ResultTxt, E_valueTxt, m_valtxt;

    public GameObject TaskObj, AllTaskListObj;

    public RectTransform rT;

    public int localTotalXpValue, localGaniedXpValue,MainXpValue;

    public GameObject ControlPanel, LocalXP, Main_Xp, ObjectivePanel, InformationPanel, InfoButton, MainSlider, InfoPanelBg, TasklistPanel, WronOptionPanel, Gained_XP_ArrowBut;

    public Button InfoCloseBtn, InfoContinueBtn, Gained_XP_Panel, CpCloseBtn, IncButton, DecButton;

    public bool sessionStart;

    
       
    public int Mass;

    public TextMeshPro mu_label_txt;


    public GameObject BallHanger, Ball_1, Ball_2, Wire_1, Wire_2, ToRotate,LeftHand,RightHand, FakeShadow;
    public Slider HangerSlider, Slider_OnOff;
    public GameObject Ball_Incr, Ball_Decr;
    public Button CraneRelease, ResetBtn;
    public Toggle IsVaccum_Toggle;
    public bool IsVaccum, CanResetBall;

    public List<Rigidbody> Ball_Rigidbodies;
    public string Description_IsVaccum, Description_NotVaccum;
    public Text DescriptionText;
    public Vector3[] BallPos, BallRot, BallScale;

    public TextAsset CSVFile;
    public AssetBundle assetBundle;

    void Start()
    {

        LoadFromAsssetBundle();

        sessionStart = false;

        //OptionUnlockedTill = 0;
        //TopicToCheck = 1;
        //MainXpValue = 10;

        IsVaccum = true;
        CanResetBall = true;

        IsAR = Camera.main.GetComponentInParent<orbit>() ? false : true;

        MainObj = this.gameObject;

        C_BG = GameObject.Find("Canvas_BG").GetComponent<Canvas>();

        Ground = GameObject.Find("Ground");

        if (!IsAR)
        {
            CamOrbit = Camera.main.GetComponentInParent<orbit>();

            C_BG.transform.parent = Camera.main.transform;
            C_BG.GetComponentInChildren<RawImage>().enabled = true;
            C_BG.renderMode = RenderMode.ScreenSpaceCamera;
            C_BG.worldCamera = Camera.main;

            CamOrbit.target = Camera.main.transform.parent;

            CamOrbit.target.position = new Vector3(0, 0.75f, 0);
            CamOrbit.maxRotUp = 45;
            CamOrbit.minRotUp = 1;
            CamOrbit.zoomMin = 0.5f;
            CamOrbit.zoomMax = 4;
            CamOrbit.zoomStart = 2f;
            CamOrbit.cameraRotUpStart = 0;

            CamOrbit.Start();
        }
        else
        {
            Ground.SetActive(false);

            C_BG.gameObject.SetActive(false);

            ArOrbit = MainObj.transform.parent.gameObject.GetComponent<AROrbitControls>();
        }

        ReadingDataFromCSVFile();
        ReadingXps();
        FindingObjects();
        TaskListCreation();
        AssigningClickEvents();
        LabelsAssignment();
        AssignInitialValues();
        LocalXpCalculation();
    }

    public void LoadFromAsssetBundle()
    {
        
        assetBundle = AssetBundle.LoadFromFile(PlayerPrefs.GetString("AssetBundleUrl"));

        CSVFile = assetBundle.LoadAsset(PlayerPrefs.GetString("LibraryName") + "CSV", typeof(TextAsset)) as TextAsset;
       
        //CSVFile = Resources.Load("vid169_accelerationduetogravityCSV") as TextAsset;
       

        Camera.main.gameObject.AddComponent<PhysicsRaycaster>();

        Camera.main.backgroundColor = Color.black;

        GameObject TargetForCamera = GameObject.Find("Target");

        if (TargetForCamera != null)
        {
            TargetForCamera.AddComponent<orbit>();
            CamOrbit = TargetForCamera.GetComponent<orbit>();
            IsAR = false;
        }
        else
        {
            this.transform.parent.gameObject.AddComponent<AROrbitControls>();
            ArOrbit = this.transform.parent.gameObject.GetComponent<AROrbitControls>();
            IsAR = true;
        }

    }

    public int totXp, loclXp;

    public void ReadingXps()
    {
        totXp = PlayerPrefs.GetInt("TotalXP");
        loclXp = PlayerPrefs.GetInt("MaxEarnings");

        if (loclXp != 0)
        {
            OptionUnlockedTill = 0;
        }
        else
        {
            OptionUnlockedTill = 1;
        }

        TopicToCheck = 1;

        MainXpValue = totXp;

        int t = 0;
        int p = 0;

        for (int k = 0; k < TaskList.Count; k++)
        {
            p = loclXp / TaskList.Count;
            XpList.Add(p.ToString());
            t = t + p;
        }

        if (t != loclXp)
        {
            XpList[XpList.Count - 1] = (p + (loclXp - t)).ToString();
        }
    }

    public float result;
    public void GetResult()
    {
        result = ((float)(TopicToCheck - 1) / (float)(TaskList.Count)) * 100;
        Camera.main.SendMessage("UnloadActually", result);
    }
    public void ReadingDataFromCSVFile()
    {             
      
        TaskList = new List<string>();
        XpList = new List<string>();
        InfoText = new List<string>();

        AllTaskData = new List<GameObject>();

        CSV_data = CSVReader.Read(CSVFile);
                  
        for (var i = 0; i < CSV_data.Count; i++)
        {

            if (CSV_data[i]["TaskList"].ToString() != "" && CSV_data[i]["TaskList"].ToString() != null)
            {
                TaskList.Add(CSV_data[i]["TaskList"].ToString());          
            }
            //if (CSV_data[i]["XPList"].ToString() != "" && CSV_data[i]["XPList"].ToString() != null)
            //{
            //    XpList.Add(CSV_data[i]["XPList"].ToString());
            //}
        }
    }
    
    public void FindingObjects() {

        InformationPanel = GameObject.Find("InformationPanel");

        InfoCloseBtn = GameObject.Find("InfoCloseBtn").GetComponent<Button>();

        InfoContinueBtn = GameObject.Find("InfoContinueBtn").GetComponent<Button>();

        InfoPanelBg = GameObject.Find("InfoPanelBg");
        
        XP_ToShow = GameObject.Find("localGainedXpValueText").GetComponent<Text>();

        XP_Effect = GameObject.Find("Effect_Text").GetComponent<Text>();

        Main_XP_ToShow = GameObject.Find("Main_XP_Value").GetComponent<Text>();

        localGainedXpValueText = GameObject.Find("localGainedXpValueText").GetComponent<Text>();

        localTotalXpValueText = GameObject.Find("GainedXP_LocalTotalXp").GetComponent<Text>();

        ObjectiveInfoText = GameObject.Find("ObjectiveInfoText").GetComponent<Text>();

        TaskObj = GameObject.Find("TaskObj");

        AllTaskListObj = GameObject.Find("AllTaskList");

        ControlPanel = GameObject.Find("ControlPanel");
        LocalXP = GameObject.Find("LocalXP");
        Main_Xp = GameObject.Find("Main_Xp");
        ObjectivePanel = GameObject.Find("ObjectivePanel");
        InformationPanel = GameObject.Find("InformationPanel");
        InfoButton = GameObject.Find("InfoButton");
        MainSlider = GameObject.Find("MainSlider");
        WronOptionPanel = GameObject.Find("WrongPanel");

        Gained_XP_Panel = GameObject.Find("Gained_XP_Panel").GetComponent<Button>();
        TasklistPanel = GameObject.Find("TasklistPanel");
        CpCloseBtn = GameObject.Find("CpCloseBtn").GetComponent<Button>();

        InfoHeadingText = GameObject.Find("InfoHeadingText").GetComponent<Text>();
        LGListText = GameObject.Find("LGListText").GetComponent<Text>();
        IListText = GameObject.Find("IListText").GetComponent<Text>();
        AListText = GameObject.Find("AListText").GetComponent<Text>();

       
        //IncButton = GameObject.Find("IncButton").GetComponent<Button>();

        //DecButton = GameObject.Find("DecButton").GetComponent<Button>();
       
        Gained_XP_ArrowBut = GameObject.Find("Gained_XP_ArrowBut");

        m_valtxt = GameObject.Find("m_valtxt").GetComponent<Text>();

        //......................................................


       // mu_label_txt = GameObject.Find("mu_label_txt").GetComponent<TextMeshPro>();


        //...........................................................................


        BallHanger = GameObject.Find("HangingBar");
        HangerSlider = GameObject.Find("MainSlider").GetComponent<Slider>();
        //GravitySlider = GameObject.Find("GravitySlider").GetComponent<Slider>();
        Ball_1 = GameObject.Find("Ball_1");
        Ball_2 = GameObject.Find("Ball_2");
        Wire_1 = GameObject.Find("Wire_1");
        Wire_2 = GameObject.Find("Wire_2");

        Ball_Incr = GameObject.Find("Mass_Increment");
        Ball_Decr = GameObject.Find("Mass_Decrement");

        CraneRelease = GameObject.Find("CraneReleaseButton").GetComponent<Button>();
        ResetBtn = GameObject.Find("ResetButton").GetComponent<Button>();
        //IsVaccum_Toggle = GameObject.Find("IsVaccum_Toggle").GetComponent<Toggle>();
        ToRotate = GameObject.Find("ToRotate");
        DescriptionText = GameObject.Find("Desc_Text").GetComponent<Text>();
        Ball_Rigidbodies = new List<Rigidbody>();

        Slider_OnOff = GameObject.Find("Slider_OnOff").GetComponent<Slider>();

        if (Ball_1.GetComponent<Rigidbody>())
        {
            Ball_Rigidbodies.Add(Ball_1.GetComponent<Rigidbody>());
        }
        else
        {
            Ball_Rigidbodies.AddRange(Ball_1.transform.GetComponentsInChildren<Rigidbody>().ToList());
            //Ball_1.transform.GetComponentsInChildren<MeshRenderer>().material.renderQueue = 3001;

            for (int i = 0; i < 3; i++) {
                Ball_Rigidbodies[i].GetComponent<Renderer>().material.renderQueue = 3002;
            }
        }

        


        if (Ball_2.GetComponent<Rigidbody>())
        {
            Ball_Rigidbodies.Add(Ball_2.GetComponent<Rigidbody>());
        }
        //else
        //    Ball_Rigidbodies.AddRange(Ball_2.transform.GetComponentsInChildren<Rigidbody>().ToList());

        BallPos = new Vector3[Ball_Rigidbodies.Count];
        BallRot = new Vector3[Ball_Rigidbodies.Count];
        BallScale = new Vector3[Ball_Rigidbodies.Count];
        
        FakeShadow = GameObject.Find("FakeShadow");

        LeftHand = GameObject.Find("LeftHand");
        RightHand = GameObject.Find("RightHand");
       

    }

     
    public void AssigningClickEvents() {
                
        iTween.ScaleTo(InformationPanel.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

        InfoCloseBtn.onClick.AddListener(() => CloseInfoPanel());

        InfoContinueBtn.onClick.AddListener(() => ContinueInfoPanel());

        InfoButton.GetComponent<Button>().onClick.AddListener(() => OpenInfoPanel());

        Gained_XP_Panel.onClick.AddListener(() => OpenTaskList());

        CpCloseBtn.onClick.AddListener(() => OpenOrCloseControlPanel());

       // MainSlider.GetComponent<Slider>().onValueChanged.AddListener(delegate { ChangeRadius(MainSlider.GetComponent<Slider>().value, MainSlider.GetComponentInChildren<Text>()); });

        AddListener(EventTriggerType.PointerDown, DisableOrbit, MainSlider.gameObject);

        AddListener(EventTriggerType.PointerUp, EnableOrbit, MainSlider.gameObject);
        
        //IncButton.GetComponent<Button>().onClick.AddListener(() => IncDecFunction(IncButton.name));

        //DecButton.GetComponent<Button>().onClick.AddListener(() => IncDecFunction(DecButton.name));

        //.......................................................................................................................................

        HangerSlider.onValueChanged.AddListener(delegate { ChangeHangerPosition(HangerSlider.value); });

        //Slider_OnOff.onValueChanged.AddListener(delegate { ChangeOnOff(Slider_OnOff.value, Slider_OnOff.GetComponentInChildren<Text>()); });

        AddListener(EventTriggerType.PointerDown, DisableOrbit, Slider_OnOff.gameObject);

        AddListener(EventTriggerType.PointerUp, EnableOrbit, Slider_OnOff.gameObject);

        AddListener(EventTriggerType.PointerUp, ChangeON, Slider_OnOff.gameObject);


        Ball_Incr.GetComponent<Button>().onClick.AddListener(() => ChangeBallMass(0.25f));
        Ball_Decr.GetComponent<Button>().onClick.AddListener(() => ChangeBallMass(-0.25f));

        CraneRelease.onClick.AddListener(() => ClickToRelease());
        ResetBtn.onClick.AddListener(() => ResetBallPositions());

        //IsVaccum_Toggle.onValueChanged.AddListener(delegate { ToggleVaccum(IsVaccum_Toggle.isOn); });

        EventTrigger.Entry PointerDownSlider = new EventTrigger.Entry();
        PointerDownSlider.eventID = EventTriggerType.PointerDown;
        PointerDownSlider.callback.AddListener((data) => { DisableOrbit(); });

        HangerSlider.gameObject.GetComponent<EventTrigger>().triggers.Add(PointerDownSlider);
       

        EventTrigger.Entry PointerUpSlider = new EventTrigger.Entry();
        PointerUpSlider.eventID = EventTriggerType.PointerUp;
        PointerUpSlider.callback.AddListener((data) => { EnableOrbit(); });

        HangerSlider.gameObject.GetComponent<EventTrigger>().triggers.Add(PointerUpSlider);




        //.........................................

        ResetBtn.gameObject.SetActive(false);
        CraneRelease.gameObject.SetActive(true);


    }

    public void AssignInitialValues()
    {
        if (OptionUnlockedTill > 0)
        {
            LocalXP.transform.localScale = new Vector3(0, 0, 0);
            ObjectivePanel.transform.localScale = new Vector3(0, 0, 0);
        }

        Mass = 0;


        //InfoHeadingText.text = InfoText[0];
        //LGListText.text = InfoText[1];
        //IListText.text = InfoText[2];
        //AListText.text = InfoText[3];

        InfoCloseBtn.gameObject.transform.localScale = new Vector3(0, 0, 0);
        localGainedXpValueText.text = "0";
        Main_XP_ToShow.text = MainXpValue + "";
        WronOptionPanel.transform.localScale = new Vector3(0, 0, 0);

        ChangeHangerPosition(HangerSlider.value);
    }


    public void TaskListCreation() {

        rT = AllTaskListObj.transform.parent.GetComponent<RectTransform>();

        localGaniedXpValue = 0;

        for (int i = 0; i < TaskList.Count; i++)
        {

            GameObject Tl = (GameObject)Instantiate(TaskObj);
            Tl.transform.SetParent(AllTaskListObj.transform);
            Tl.transform.localScale = new Vector3(1, 1, 1);
            Tl.name = i.ToString();

            AllTaskData.Add(Tl);
            AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(false);
            AllTaskData[i].gameObject.transform.GetChild(2).gameObject.SetActive(false);
            AllTaskData[i].gameObject.transform.GetChild(3).GetComponentInChildren<Text>().text = TaskList[i];
            AllTaskData[i].gameObject.transform.GetChild(4).GetComponentInChildren<Text>().text = "+" + XpList[i] + " XP";

            localTotalXpValue += int.Parse(XpList[i]);
        }

        rT.sizeDelta = new Vector2(rT.sizeDelta.x, TaskList.Count * 50);
        localTotalXpValueText.text = "/" + localTotalXpValue.ToString();

        AllTaskData[0].gameObject.transform.GetChild(2).gameObject.SetActive(true);
        ObjectiveInfoText.text = AllTaskData[0].gameObject.transform.GetChild(3).GetComponentInChildren<Text>().text;
    }

    public void LocalXpCalculation()
    {


        for (int i = 0; i < TaskList.Count; i++)
        {
            AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(false);
            AllTaskData[i].gameObject.transform.GetChild(2).gameObject.SetActive(false);
        }

        if (OptionUnlockedTill < 1)
        {
            if ((TopicToCheck) < (TaskList.Count + 1))
            {
                localGaniedXpValue = 0;

                for (int i = 0; i < TopicToCheck - 1; i++)
                {
                    localGaniedXpValue += int.Parse(XpList[i]);
                }

                for (int i = 0; i < TopicToCheck; i++)
                {
                    AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(true);
                }

                localGainedXpValueText.text = localGaniedXpValue.ToString();

                AllTaskData[TopicToCheck - 1].gameObject.transform.GetChild(2).gameObject.SetActive(true);

                ObjectiveInfoText.text = AllTaskData[TopicToCheck - 1].gameObject.transform.GetChild(3).GetComponentInChildren<Text>().text;
            }
            else
            {
                for (int i = 0; i < TaskList.Count; i++)
                {
                    AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(true);
                    AllTaskData[i].gameObject.transform.GetChild(2).gameObject.SetActive(false);
                }

                localGainedXpValueText.text = localTotalXpValue.ToString();

                XP_Effect.text = "+" + localTotalXpValue + " XP";

                XP_Effect.transform.position = (XP_ToShow.transform.position);

                iTween.Stop(XP_Effect.gameObject);

                iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeOutElastic));

                iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", XP_Effect.transform.position.x, "y", XP_Effect.transform.position.y + 50, "z", 0, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

                iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", Main_XP_ToShow.transform.position.x, "y", Main_XP_ToShow.transform.position.y, "z", 0, "delay", 1.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

                iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "delay", 2f, "time", 0.5f, "easetype", iTween.EaseType.linear, "oncomplete", "MainXpCalculation", "oncompletetarget", this.gameObject));
            }
        }
        else
        {
            for (int i = 0; i < TaskList.Count; i++)
            {
                AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(true);
                AllTaskData[i].gameObject.transform.GetChild(2).gameObject.SetActive(false);
            }

            localGainedXpValueText.text = localTotalXpValue.ToString();
        }
    }

    public void CloseAllpanels()
    {

        ControlPanel.GetComponent<Animator>().Play("CpanelFullClose");
        LocalXP.GetComponent<Animator>().Play("LocalXpPanelClose");
        Main_Xp.GetComponent<Animator>().Play("MainXpPanelClose");
        ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelClose");
        InfoButton.GetComponent<Animator>().Play("InfoButtonClose");
        MainSlider.GetComponent<Animator>().Play("MainSliderClose");

        if (TasklistPanel.gameObject.transform.localScale.y != 0)
            iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));
    }

    public void OpenAllpanels()
    {
        if (OptionUnlockedTill < 1)
        {
            if (TopicToCheck <= (TaskList.Count))
            {
                LocalXP.GetComponent<Animator>().Play("LocalXpPanelOPen");
                ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen");
            }
        }
       

        ControlPanel.GetComponent<Animator>().Play("CpanelOpen");

        Main_Xp.GetComponent<Animator>().Play("MainXpPanelOpen");

        InfoButton.GetComponent<Animator>().Play("InfoButtonOpen");
        MainSlider.GetComponent<Animator>().Play("MainSliderOpen");
        iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "time", 0.2f, "easetype", iTween.EaseType.linear));
        iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));
    }

    public void AddXpEffect()
    {


        if (TopicToCheck <= (TaskList.Count))
        {
            // Camera.main.GetComponent<PhysicsRaycaster>().enabled = false;

            GameObject.Find("Canvas").GetComponent<CanvasGroup>().interactable = false;

            XP_Effect.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);

            XP_Effect.text = "+" + int.Parse(XpList[TopicToCheck - 1]) + " XP";

            XP_Effect.transform.localScale = new Vector3(1, 0, 1);

            // iTween.Stop(XP_Effect.gameObject);


            iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeOutElastic));

            iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", XP_Effect.transform.position.x, "y", XP_Effect.transform.position.y + 50, "z", 0, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

            iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", XP_ToShow.transform.position.x, "y", XP_ToShow.transform.position.y, "z", 0, "delay", 1.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

            iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "delay", 2f, "time", 0.5f, "easetype", iTween.EaseType.linear, "oncomplete", "LocalXpCalculation", "oncompletetarget", this.gameObject));

            TopicToCheck++;

            Invoke("EnableRayCast", 3);
        }

    }

    public void EnableRayCast()
    {
        //Camera.main.GetComponent<PhysicsRaycaster>().enabled = true;
        GameObject.Find("Canvas").GetComponent<CanvasGroup>().interactable = true;
    }

    public IEnumerator MainXpCalculation()
    {
        if (OptionUnlockedTill == 0)
        {
            OptionUnlockedTill = 1;

            MainXpValue += localTotalXpValue;

            Main_XP_ToShow.text = MainXpValue.ToString();

            yield return new WaitForSeconds(1f);

            LocalXP.GetComponent<Animator>().Play("LocalXpPanelClose");

            ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelClose");

            if ((ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelClose")) || (ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelFullClose")))
            {
                ControlPanel.GetComponent<Animator>().Play("CpanelOpen");
            }
        }

    }

    public void CloseInfoPanel()
    {
        sessionStart = true;
        InfoPanelBg.SetActive(false);
        iTween.ScaleTo(InformationPanel.gameObject, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));
        OpenAllpanels();
    }

    public void ContinueInfoPanel()
    {
        sessionStart = true;
        InfoPanelBg.SetActive(false);
        iTween.ScaleTo(InformationPanel.gameObject, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));
        InfoCloseBtn.transform.localScale = new Vector3(0.27f, 0.27f, 0.27f);
        InfoContinueBtn.transform.localScale = new Vector3(0, 0, 0);
        OpenAllpanels();
    }

    public void OpenInfoPanel()
    {
        sessionStart = false;
        iTween.ScaleTo(InformationPanel.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));
        InfoPanelBg.SetActive(true);
        iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "time", 0.2f, "easetype", iTween.EaseType.linear));
        iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));
        CloseAllpanels();
    }

    public void OpenTaskList()
    {


        if (TasklistPanel.gameObject.transform.localScale.y != 1)
        {
            iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.01f, "time", 0.2f, "easetype", iTween.EaseType.easeInSine));

            ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelClose");


            //Gained_XP_ArrowBut

            iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 180, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));

            if ((ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelOpen")))
            {
                ControlPanel.GetComponent<Animator>().Play("CpanelClose");
            }
        }
        else
        {
            iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "delay", 0.01f, "time", 0.2f, "easetype", iTween.EaseType.easeInSine));
            iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));

            ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen");

            ControlPanel.GetComponent<Animator>().Play("CpanelOpen");
        }
    }

    public void OpenOrCloseControlPanel()
    {

        if ((ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelClose")) || (ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelFullClose")))
        {
            ControlPanel.GetComponent<Animator>().Play("CpanelOpen");
        }
        else
        {
            ControlPanel.GetComponent<Animator>().Play("CpanelClose");
        }
    }

    public void WrongOptionSelected()
    {
        WronOptionPanel.transform.localScale = new Vector3(0, 0, 0);

        iTween.Stop(WronOptionPanel.gameObject);

        iTween.ScaleTo(WronOptionPanel.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.01f, "time", 0.2f, "easetype", iTween.EaseType.easeInSine));

        iTween.ScaleTo(WronOptionPanel.gameObject, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 1f, "time", 0.2f, "easetype", iTween.EaseType.easeInSine));

        // InfoPanelBg.SetActive(true);

        Invoke("CloseBlurBg", 1f);

        GameObject.Find("Canvas").GetComponent<CanvasGroup>().interactable = false;

        ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelClose");


        var mouse = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);

        if (mouse.x < Screen.width / 2)
        {
            // WronOptionPanel
            //print("Mouse is on left side of screen.");
            WronOptionPanel.GetComponent<RectTransform>().pivot = new Vector2(0, 0);
        }

        if (mouse.x > Screen.width / 2)
        {
            //print("Mouse is on right side of screen.");
            WronOptionPanel.GetComponent<RectTransform>().pivot = new Vector2(1, 0);
        }


        WronOptionPanel.transform.position = Input.mousePosition;

    }

    public void CloseBlurBg()
    {
        InfoPanelBg.SetActive(false);
        GameObject.Find("Canvas").GetComponent<CanvasGroup>().interactable = true;


        if (OptionUnlockedTill == 0)
        {
            if (TopicToCheck <= (TaskList.Count))
            {
                if (InformationPanel.gameObject.transform.localScale.x == 0f)
                {
                    //ObjectivePanel.GetComponent<Animator>().Play("");
                    ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen", 0, 0f);
                }
            }
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (InformationPanel.gameObject.transform.localScale.x != 1 && sessionStart)
        {
            if (TopicToCheck <= (TaskList.Count))
            {
                iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "time", 0.2f, "easetype", iTween.EaseType.linear));
                ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen");
                iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));
                ControlPanel.GetComponent<Animator>().Play("CpanelOpen");
            }
        }

    }

    public void LabelsAssignment()
    {
        GameObject Label = GameObject.Find("Labels");

        Labels = new GameObject[Label.transform.childCount];

        for (int j = 0; j < Labels.Length; j++)
        {
            Labels[j] = Label.transform.GetChild(j).gameObject;

            GameObject TempLabels = Labels[j];

            foreach (Transform ChildLabel in TempLabels.transform)
            {
                if (ChildLabel.name.Contains("SmoothLook"))
                {
                    ChildLabel.gameObject.AddComponent<SmoothLook>();
                   // ChildLabel.gameObject.AddComponent<ScaleRelativeToCamera>();

                }
            }
        }
                     
        for (int k = 0; k < Labels.Length; k++)
        {
            string name = Labels[k].name.Remove(Labels[k].name.Length - 6);

            if (GameObject.Find(name + "") != null)
            {
                GameObject dummy = GameObject.Find(name + "").gameObject;

                Labels[k].transform.parent = dummy.transform;
                Labels[k].transform.rotation = Quaternion.identity;
                Labels[k].SetActive(false);
            }


            //TempTextMesh = Labels[k].GetComponentsInChildren<TextMeshPro>();

            //if (TempTextMesh.Length > 1)
            //{
            //    for (int p = 0; p < TempTextMesh.Length; p++)
            //    {
            //        if (DescriTextDoc.ContainsKey(name + "_" + p))
            //            TempTextMesh[p].text = DescriTextDoc[name + "_" + p];
            //    }
            //}
            //else
            //{
            //    if (DescriTextDoc.ContainsKey(name))
            //        Labels[k].GetComponentInChildren<TextMeshPro>().text = DescriTextDoc[name];
            //}

        }

      
    }

    public void AddListener(EventTriggerType eventType, Action MethodToCall, GameObject TriggerObjToAdd)
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall());
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }


    public void EnableOrbit()
    {
        if (!IsAR)
        {
            CamOrbit.DisableInteration = false;
        }
        else
        {
            ArOrbit.DisableInteration = false;
        }

        if (OptionUnlockedTill < 1)
        {
            if (InformationPanel.gameObject.transform.localScale.x != 1)
            {
                if (TopicToCheck <= (TaskList.Count))
                {
                    iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "time", 0.2f, "easetype", iTween.EaseType.linear));
                    iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));
                    ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen");
                }
            }
        }

    }

    public void DisableOrbit()
    {
        if (!IsAR)
        {
            CamOrbit.DisableInteration = true;
        }
        else
        {
            ArOrbit.DisableInteration = true;
        }

        //if (CanResetBall)
        //{

        //}
        //else
        //{
        //    WrongOptionSelected();
        //    WronOptionPanel.GetComponentInChildren<Text>().text = "Pickup the objects";
        //}


    }

    public void AddListener(EventTriggerType eventType, Action<float> MethodToCall, GameObject TriggerObjToAdd)
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall(TriggerObjToAdd.GetComponent<Slider>().value));
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }

    public Vector3 MidPoint(GameObject obj1, GameObject obj2)
    {
        Vector3 midp = (obj1.transform.localPosition + obj2.transform.localPosition) / 2;
        return midp;
    }

    public float Scale(GameObject obj1, GameObject obj2)
    {
        float scl = Vector3.Distance(obj1.transform.localPosition, obj2.transform.localPosition);
        return scl;
    }

    public void ChangeON() {


        if (CanResetBall)
        {

        }
        else
        {
            WrongOptionSelected();
            WronOptionPanel.GetComponentInChildren<Text>().text = "Pickup the objects";
        }


        //Slider_OnOff.value = 1;

        if (Slider_OnOff.value == 1)
        {
            Slider_OnOff.GetComponentInChildren<Text>().text = "Off";
            IsVaccum = true;
            Slider_OnOff.value = 0;
            //IsVaccum_Toggle.enabled = false;
        }
        else
        {
            Slider_OnOff.value = 1;
            Slider_OnOff.GetComponentInChildren<Text>().text = "On";
            IsVaccum = false;
            // IsVaccum_Toggle.enabled = true;
        }
    }



    public void ChangeOnOff(float val, Text txt)
    {
        //if (CanResetBall)
        //{
            
        //}
        //else
        //{
        //    WrongOptionSelected();
        //    WronOptionPanel.GetComponentInChildren<Text>().text = "Pickup the objects";
        //}

        //if (val == 0)
        //{
        //    txt.text = "Off";
        //    IsVaccum = true;
        //    //IsVaccum_Toggle.enabled = false;
        //}
        //else
        //{
        //    txt.text = "On";
        //    IsVaccum = false;
        //    // IsVaccum_Toggle.enabled = true;
        //}
    }
        
    public void IncDecFunction(string nam)
    {
        
        if (nam == "IncButton")
        {
            if (Mass < 5f)
            {
                Mass = Mass + 1;
               
                //if (TopicToCheck <= (TaskList.Count))
                //{
                //    if (TopicToCheck == 1)
                //    {
                //        AddXpEffect();
                //    }
                //    else
                //    {
                //        WrongOptionSelected();
                //    }
                //}

            }
        }
        else if (nam == "DecButton")
        {
            if (Mass > 1f)
            {
                Mass = Mass - 1;
                
            }
        }
                
        m_valtxt.text = Mass.ToString() + " kg";
        
        // CancelInvoke("StopScanner");
        //Invoke("StopScanner", 2);
    }

    public void StopScanner() {
        //ScannerObject.SetActive(false);
    }

    public void Update()
    {
        float dd = Scale(BallHanger, FakeShadow);
        FakeShadow.transform.localScale = new Vector3(dd * 0.5f + 1f, 0.001f, dd * 0.5f + 1f);
    }


    //.................
    public void ChangeHangerPosition(float SliderVal)
    {
        float Pos_y = SliderVal * 1.1111f - 0.01888f;


        BallHanger.transform.localPosition = new Vector3(BallHanger.transform.localPosition.x, Pos_y, BallHanger.transform.localPosition.z);

        float Scale_y = SliderVal * -1.2f + 2.29f;
        Wire_1.transform.localScale = new Vector3(Wire_1.transform.localScale.x, Scale_y, Wire_1.transform.localScale.z);
        Wire_2.transform.localScale = new Vector3(Wire_2.transform.localScale.x, Scale_y, Wire_2.transform.localScale.z);

        for (int i = 0; i < ToRotate.transform.childCount; i++)
        {
            ToRotate.transform.GetChild(i).localRotation = Quaternion.Euler(0, 90, ToRotate.transform.GetChild(i).localScale.z * SliderVal * -360);
        }

        HangerSlider.GetComponentInChildren<Text>().text = SliderVal.ToString("F1");

        if (CanResetBall)
        {
           

        }
        else {
            WrongOptionSelected();
            WronOptionPanel.GetComponentInChildren<Text>().text = "Pickup the objects";
        }

       // CamOrbit.distance = SliderVal * 3;
    }

    public void ChangeGravity(float SliderVal)
    {
        Time.timeScale = SliderVal;
    }

    public void ChangeBallMass(float IncrVal)
    {
        if (CanResetBall)
        {
            IncrVal += Ball_2.transform.localScale.x;
            IncrVal = Mathf.Clamp(IncrVal, 1, 2);
            Ball_2.transform.localScale = IncrVal * Vector3.one;
            m_valtxt.text = IncrVal.ToString() + " kg";

        }
        else
        {
            WrongOptionSelected();
            WronOptionPanel.GetComponentInChildren<Text>().text = "Pickup the objects";
        }

       

        //float LocalScale = Ball_2.transform.localScale.x * -0.001f + 0.011f;

        //Ball_Incr.transform.localScale = LocalScale * Vector3.one;
        //Ball_Decr.transform.localScale = LocalScale * Vector3.one;
    }


    public void ClickToRelease()
    {
        if (OptionUnlockedTill == 0)
        {
            if (TopicToCheck == 1 )
            {
                if (IsVaccum == true )
                {
                    Release();
                }
                else
                {
                    WrongOptionSelected();
                    WronOptionPanel.GetComponentInChildren<Text>().text = "Wrong parameter chosen";
                }
            }

            else if (TopicToCheck == 2 )
            {
                if (IsVaccum == false )
                {
                    Release();  
                }
                else
                {
                    WrongOptionSelected();
                    WronOptionPanel.GetComponentInChildren<Text>().text = "Wrong parameter chosen";
                }
            }
            else if (TopicToCheck == 3)
            {
                if (IsVaccum == true && (Ball_2.transform.localScale.x >= 1.25f))
                {
                    Release();   
                }
                else
                {
                    WrongOptionSelected();
                    WronOptionPanel.GetComponentInChildren<Text>().text = "Wrong parameter chosen";
                }
            }
            else if (TopicToCheck == 4)
            {
                if (IsVaccum == false && (Ball_2.transform.localScale.x >= 1.25f))
                {
                    Release();                   
                }
                else
                {
                    WrongOptionSelected();
                    WronOptionPanel.GetComponentInChildren<Text>().text = "Wrong parameter chosen";
                }
            }
            else
            {
                Release();
            }
        }
        else
        {
            Release();

           
        }


        if (IsVaccum == true)
        {
            Labels[0].transform.GetChild(0).gameObject.GetComponentInChildren<TextMeshPro>().text = "All the objects accelerate equally and hit the ground at the same time.";
        }
        else
        {
            Labels[0].transform.GetChild(0).gameObject.GetComponentInChildren<TextMeshPro>().text = "All the objects do not accelerate equally and hit the ground at different point of time due to air resistance.";
        }


    }
    //public void Release() {

    //    Invoke("Release1", 0.5f);
    //   // CamOrbit.distance = 3.5f;
    //}


    public void Release() {

        if (!Ball_Decr.GetComponent<Button>().interactable)
            return;

        // IsVaccum_Toggle.enabled = false;
        CanResetBall = false;

        //HangerSlider.enabled = false;
        //Slider_OnOff.enabled = false;
        //Ball_Decr.GetComponent<Button>().interactable = false;
        //Ball_Incr.GetComponent<Button>().interactable = false;

        Ball_1.transform.parent = BallHanger.transform.parent;
        Ball_2.transform.parent = BallHanger.transform.parent;

        for (int i = 0; i < Ball_Rigidbodies.Count; i++)
        {
            BallPos[i] = Ball_Rigidbodies[i].transform.localPosition;
            BallRot[i] = Ball_Rigidbodies[i].transform.localEulerAngles;
            BallScale[i] = Ball_Rigidbodies[i].transform.localScale;
        }

        if (IsVaccum)
        {
            //Ball_1.GetComponent<Rigidbody>().drag= 5;
            //Ball_2.GetComponent<Rigidbody>().mass= Ball_2.transform.localScale.x*20;
            //Ball_2.GetComponent<Rigidbody>().AddForce(new Vector3(0,-1f,0),ForceMode.Impulse); 
            for (int i = 0; i < Ball_Rigidbodies.Count; i++)
            {
                Ball_Rigidbodies[i].drag = 0;
            }
            DescriptionText.text = Description_IsVaccum;
        }
        else
        {
            DescriptionText.text = Description_NotVaccum;
            for (int i = 0; i < Ball_Rigidbodies.Count; i++)
            {
                if (Ball_Rigidbodies[i].gameObject != Ball_2)
                    Ball_Rigidbodies[i].drag = 5;
            }

        }

        for (int i = 0; i < Ball_Rigidbodies.Count; i++)
        {
            Ball_Rigidbodies[i].isKinematic = false;
            Ball_Rigidbodies[i].interpolation = RigidbodyInterpolation.Interpolate;

        }

        iTween.RotateTo(LeftHand.gameObject, iTween.Hash("x", 0, "y", 0, "z", 0, "islocal", true, "delay", .01f, "time", 0.2f, "easetype", iTween.EaseType.linear));

        iTween.RotateTo(RightHand.gameObject, iTween.Hash("x", 0, "y", 0, "z", 0, "islocal", true, "delay", .01f, "time", 0.2f, "easetype", iTween.EaseType.linear));

        ResetBtn.gameObject.SetActive(false);
        CraneRelease.gameObject.SetActive(false);
        //ResetBtn.GetComponent<Image>().enabled = true;
        Invoke("ResetBallBool", 2f);

        //CraneRelease.GetComponent<Image>().sprite.name = "Hook2";
        if (OptionUnlockedTill < 1)
        {
            AddXpEffect();
        }

    }

    public void ResetBallBool()
    {
        //print("reset......");
        // ResetBtn.GetComponent<Image>().enabled = true;
        //ResetBtn.gameObject.SetActive(false);
        //CraneRelease.gameObject.SetActive(true);
        // IsVaccum_Toggle.enabled = true;
        Labels[0].SetActive(true);
        GameObject obj = Labels[0].transform.GetChild(0).gameObject;
        obj.transform.localScale = new Vector3(1, 0, 1);
        iTween.ScaleTo(obj.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "islocal", true, "delay", .01f, "time", 0.5f, "easetype", iTween.EaseType.linear));

        ResetBtn.gameObject.SetActive(true);
        CraneRelease.gameObject.SetActive(false);
    }

    public void ResetBallPositions()
    {
        //ResetBtn.GetComponent<Image>().enabled = false;

        //for (int i = 0; i < Ball_Rigidbodies.Count; i++)
        //{
        //    Ball_Rigidbodies[i].isKinematic = true;
        //    Ball_Rigidbodies[i].interpolation = RigidbodyInterpolation.None;
        //    Ball_Rigidbodies[i].transform.localPosition = BallPos[i];
        //    Ball_Rigidbodies[i].transform.localEulerAngles = BallRot[i];
        //    Ball_Rigidbodies[i].transform.localScale = BallScale[i];
        //}

        //Ball_1.transform.parent = BallHanger.transform;
        //Ball_2.transform.parent = BallHanger.transform;

        //Ball_Decr.GetComponent<Button>().interactable = true;
        //Ball_Incr.GetComponent<Button>().interactable = true;

        //HangerSlider.enabled = true;

        //HangerSlider.value = 0.1f;

        //ResetBtn.gameObject.SetActive(false);
        //CraneRelease.gameObject.SetActive(true);


        //........................................................................

        Labels[0].SetActive(false);
        ResetBtn.gameObject.SetActive(false);
        CraneRelease.gameObject.SetActive(false);

        iTween.MoveTo(BallHanger.gameObject, iTween.Hash("x", BallHanger.transform.localPosition.x, "y", -0.9f, "z", BallHanger.transform.localPosition.z, "islocal", true, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.linear, "oncomplete", "CollectinObjects", "oncompletetarget", this.gameObject));


        iTween.RotateTo(LeftHand.gameObject, iTween.Hash("x", 0, "y", 0, "z", -75, "islocal", true, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.linear));

        iTween.RotateTo(RightHand.gameObject, iTween.Hash("x", 0, "y", 0, "z", 75, "islocal", true, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.linear));


    }

    public void CollectinObjects() {

        for (int i = 0; i < Ball_Rigidbodies.Count; i++)
        {
            Ball_Rigidbodies[i].isKinematic = true;
            Ball_Rigidbodies[i].interpolation = RigidbodyInterpolation.None;
            Ball_Rigidbodies[i].transform.localPosition = BallPos[i];
            
            Ball_Rigidbodies[i].transform.localEulerAngles = BallRot[i];
            Ball_Rigidbodies[i].transform.localScale = BallScale[i];
            //if(i< Ball_Rigidbodies.Count-1)
                //iTween.MoveTo(Ball_Rigidbodies[i].gameObject, iTween.Hash("x", BallPos[i].x, "y", BallPos[i].y, "z", BallPos[i].z, "islocal", true, "delay", 0.01f, "time", 0.2f, "easetype", iTween.EaseType.linear));
        }

        //
        Ball_1.transform.parent = BallHanger.transform;
        Ball_2.transform.parent = BallHanger.transform;

        Ball_1.transform.localPosition = new Vector3(-0.3f, -0.1f, 0);

        Ball_2.transform.localPosition = new Vector3(0.3f, -0.1f, 0);

        // iTween.MoveTo(Ball_1.gameObject, iTween.Hash("x", -0.3f, "y", -0.1f, "z", 0, "islocal", true, "delay", 0.51f, "time", 0.2f, "easetype", iTween.EaseType.linear, "oncomplete", "ResetBodies", "oncompletetarget", this.gameObject));
        //iTween.MoveTo(Ball_2.gameObject, iTween.Hash("x", 0.3f, "y", -0.1f, "z", 0, "islocal", true, "delay", 0.5f, "time", 0.2f, "easetype", iTween.EaseType.linear));


        iTween.MoveTo(BallHanger.gameObject, iTween.Hash("x", BallHanger.transform.localPosition.x, "y", 0.09223f, "z", BallHanger.transform.localPosition.z, "islocal", true, "delay", .1f, "time", 0.5f, "easetype", iTween.EaseType.linear,"oncomplete", "ResetBodies", "oncompletetarget", this.gameObject)); ;

    }


    public void ResetBodies() {

        CanResetBall = true;

        //Ball_Decr.GetComponent<Button>().interactable = true;
        //Ball_Incr.GetComponent<Button>().interactable = true;
        //HangerSlider.enabled = true;
        //Slider_OnOff.enabled = true;

        HangerSlider.value = 0.1f;

        ResetBtn.gameObject.SetActive(false);
        CraneRelease.gameObject.SetActive(true);

       // CamOrbit.distance = 1.5f;
    }

    //public void ToggleVaccum(bool Vaccum)
    //{
    //    IsVaccum = Vaccum;
    //    IsVaccum_Toggle.GetComponent<Animator>().enabled = !Vaccum;
    //    IsVaccum_Toggle.transform.GetChild(0).gameObject.SetActive(!Vaccum);
    //    //print("isvaccum........"+IsVaccum);
    //}

}
