﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Zekki_Magnet : MonoBehaviour
{
    public GameObject BarMagnet, BarMagnet1,ToggBubble;

    // Start is called before the first frame update
    void Start()
    {
        BarMagnet = GameObject.Find("bar_magnet_needle_v01");
        BarMagnet1 = GameObject.Find("bar_magnet_needle_v01 (1)");
        ToggBubble = GameObject.Find("ToggBubble");
        BarMagnet1.SetActive(false);


    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.name.Contains("IntensityRadius"))
        {
            //print("collider....");
            float X_Val=0;
            if (GetComponent<iTween>())
                Destroy(GetComponent<iTween>());

            if (transform.localPosition.y < (BarMagnet.transform.localPosition.y - 0.15f))
                iTween.RotateTo(this.gameObject, iTween.Hash("z", 150, "time", 0.5f, "islocal", true, "easetype", iTween.EaseType.easeInExpo));
            else if (transform.localPosition.y > (BarMagnet.transform.localPosition.y + 0.15f))
                iTween.RotateTo(this.gameObject, iTween.Hash("z", 30, "time", 0.5f, "islocal", true, "easetype", iTween.EaseType.easeInExpo));
            else
                iTween.RotateTo(this.gameObject, iTween.Hash("z", 90, "time", 0.5f, "islocal", true, "easetype", iTween.EaseType.easeInExpo));

            if (BarMagnet1.activeInHierarchy)
            {
                X_Val = 0.22f;
                
            }
            else
            {
                X_Val =0.27f;
                
            }

            
            
            
            
                ToggBubble.GetComponentInChildren<TextMeshPro>().text = "Magnets attract iron when exposed to magnetic field.";
                ToggBubble.transform.localPosition = BarMagnet.transform.localPosition - new Vector3(0.6f, 0.1f, 0.15f);
            
            iTween.ScaleTo(ToggBubble, iTween.Hash("y", 0.2f, "delay", 0.7f, "time", 0.5f, "islocal", true, "easetype", iTween.EaseType.spring));

            iTween.MoveTo(this.gameObject,iTween.Hash("x",BarMagnet.transform.localPosition.x- X_Val, "y", BarMagnet.transform.localPosition.y,"time",0.5f,"islocal",true,"easetype",iTween.EaseType.easeInExpo, "oncomplete", "MakeParent", "oncompletetarget", this.gameObject));

            iTween.RotateAdd(transform.GetChild(0).GetChild(1).GetChild(3).gameObject, iTween.Hash("z", -30, "time", 0.05f, "looptype", iTween.LoopType.pingPong, "easetype", iTween.EaseType.linear));
            iTween.RotateAdd(transform.GetChild(0).GetChild(1).GetChild(4).gameObject, iTween.Hash("z", 30, "time", 0.05f, "looptype", iTween.LoopType.pingPong, "easetype", iTween.EaseType.linear));

            //transform.parent.GetComponent<MagnetMovement>().IsInteraction = true;
            

        }

        if (other.name.Contains("OutOfRange"))
        {
            ToggBubble.GetComponentInChildren<TextMeshPro>().text = "Magnets doesn't attract iron when not exposed to magnetic field.";
            ToggBubble.transform.localPosition = BarMagnet.transform.localPosition - new Vector3(0.25f, -0.05f, 0.15f);
            iTween.ScaleTo(ToggBubble, iTween.Hash("y", 0.2f, "delay", 0f, "time", 0.5f, "islocal", true, "easetype", iTween.EaseType.spring));
        }

    }


    public void MakeParent()
    {
        this.transform.parent = BarMagnet.transform;
    }
}
