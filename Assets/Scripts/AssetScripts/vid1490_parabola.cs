﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;
using System.Linq;
using System;
using System.Text.RegularExpressions;
using UnityEngine.SceneManagement;

public class vid1490_parabola : MonoBehaviour
{
    public AssetBundle assetBundle;
    public Material TransparentMaterial;
    public TextAsset CSVFile;
    private Color old_color;
    public GameObject F_PointObj, Zekki_Middle, Zekki_Up, Zekki_Down,Ball,CommonPoint_R1_R2,R1_StartPoint,MidPoint,R1_TMP,R2_TMP;
    public LineRenderer ParabolaLine_Plus, ParabolaLine_Minus, ParaDotted_Plus, ParaDotted_Minus, R1_Dist_Line, R2_Dist_Line;
    public List<Vector3> LinePos_Plus, LinePos_Minus;
    public Slider F_Point_Slider,Ball_Move_Slider;
    public Button ThrowBall;
    int NextPos = 0;
    public bool IsBallMoving;
    public float InitPos = -0.1f, F_Point_Val;
    public Text r1_Val_Text, r2_Val_Text;

    public orbit CamOrbit;
    public AROrbitControls ArOrbit;
   

    public bool IsAR, IsUIZooming;
    public int OptionUnlockedTill, CurrentTopicSelected, TopicToCheck;
    public float Sub_XP_Earned, Total_XP;
    public Dropdown ToDoList_DD;
    public Slider ValueDisplaySlider;
    public GameObject InstructionPanel_DD, ObjectivePanel, InstructionPanel, InfoPanel, GlassPanel, Canvas_BG, SidePannel;
    public Button Info_Close_Btn, Info_Cntue_Btn, Info_Open_Btn, SidePannelOpen, Reset_Btn;
    public List<Dictionary<string, object>> CSV_data;
    public List<String> Topics, Sub_Topics, Sub_Top_ToDoList, Sub_Top_Desc, Sub_Top_XP, LearningGoals;
    public Text XP_ToShow, XP_Effect, Main_XP_ToShow, Learninggoal_Txt, Instruction_Txt, Assumption_Txt, Heading_Txt;
    public bool IsTopicUnlocked;
    public Canvas UIcanvas;

    // Start is called before the first frame update
    void Start()
    {
        //OptionUnlockedTill = 1;

        
        UIcanvas = GameObject.Find("Canvas_Integration").GetComponent<Canvas>();

        LoadFromAsssetBundle();



        ReadingDataFromCSVFile();

        ReadingXps();

        FindGameObjects();

        AssignEventsToObjects();

        DrawParabolaLines(2f);

        

    }

    public void LoadFromAsssetBundle()
    {
        //if (Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.Android)
        //{
        assetBundle = AssetBundle.LoadFromFile(PlayerPrefs.GetString("AssetBundleUrl"));

        CSVFile = assetBundle.LoadAsset(PlayerPrefs.GetString("LibraryName") + "CSV", typeof(TextAsset)) as TextAsset;

        TransparentMaterial = assetBundle.LoadAsset("TransparentMaterial", typeof(Material)) as Material;
        TransparentMaterial.shader = Shader.Find("Standard");





        //}
        //else
        //{

        //string CSVName = transform.parent.gameObject.name;



        //if (CSVName.Contains("Clone"))
        //{



        //    CSVName = CSVName.Substring(0, CSVName.Length - 7);
        //}



        //print("casvname........." + CSVName);



        //CSVFile = Resources.Load(CSVName + "CSV") as TextAsset;
        //}







        Camera.main.gameObject.AddComponent<PhysicsRaycaster>();



        Camera.main.backgroundColor = Color.black;



        GameObject TargetForCamera = GameObject.Find("Target");



        if (TargetForCamera != null)
        {


               TargetForCamera.AddComponent<orbit>();
                CamOrbit = TargetForCamera.GetComponent<orbit>();
                CamOrbit.zoomMax = 0.6f;
                CamOrbit.zoomMin = 0.4f;
                CamOrbit.zoomStart = 0.5f;
                CamOrbit.distance = CamOrbit.zoomStart;
                CamOrbit.transform.position = new Vector3(0, 0f, 0);

            CamOrbit.minSideRot = 60;
            CamOrbit.maxSideRot = 60;

            CamOrbit.maxRotUp = 60;
            CamOrbit.minRotUp = -25;
            CamOrbit.cameraRotUpStart =45;
            

            IsAR = false;
        }
        else
        {
            this.transform.parent.gameObject.AddComponent<AROrbitControls>();
            ArOrbit = this.transform.parent.gameObject.GetComponent<AROrbitControls>();
            IsAR = true;
        }

        

    }

    public void ZoomCamera()
    {

        Camera.main.gameObject.AddComponent<PhysicsRaycaster>();
        UIcanvas.GetComponent<GraphicRaycaster>().enabled = true;
        if (!IsAR)
        {
           CamOrbit.zoomMax = 3f;
            CamOrbit.zoomMin = 0.5f;
            CamOrbit.zoomStart = 1.5f;
            CamOrbit.distance = CamOrbit.zoomStart;
        }
    }

    public void ReadingDataFromCSVFile()
    {
        Topics = new List<string>();
        Sub_Topics = new List<string>();
        Sub_Top_Desc = new List<string>();
        Sub_Top_ToDoList = new List<string>();
        Sub_Top_XP = new List<string>();
        LearningGoals = new List<string>();

        CSV_data = CSVReader.Read(CSVFile);


        for (var i = 0; i < CSV_data.Count; i++)
        {
            //print("Topics " + CSV_data[i]["Learninggoals"] + ".......... " +"ToDo " + CSV_data[i]["Topic_TodoList"]);



            if (CSV_data[i]["Topic_ToDoList"].ToString() != "" && CSV_data[i]["Topic_ToDoList"].ToString() != null)
            {
                Sub_Top_ToDoList.Add(CSV_data[i]["Topic_ToDoList"].ToString());

            }



            //if (CSV_data[i]["Top_XP"].ToString() != "" && CSV_data[i]["Top_XP"].ToString() != null)
            //{
            //    Sub_Top_XP.Add(CSV_data[i]["Top_XP"].ToString());

            //}

            if (CSV_data[i]["Learninggoals"].ToString() != "" && CSV_data[i]["Learninggoals"].ToString() != null)
            {
                LearningGoals.Add(CSV_data[i]["Learninggoals"].ToString());

            }

        }






    }


    public int totXp, loclXp, PerTopic_XP;

    public void ReadingXps()
    {

        //PlayerPrefs.SetInt("TotalXP", 34);
        //PlayerPrefs.SetInt("MaxEarnings", 39);

        totXp = PlayerPrefs.GetInt("TotalXP");
        loclXp = PlayerPrefs.GetInt("MaxEarnings");

        TopicToCheck = 0;

        if (loclXp != 0)
        {
            OptionUnlockedTill = 0;

        }
        else
        {
            OptionUnlockedTill = 1;

        }

       


        int t = 0;
        int p = 0;

        for (int k = 0; k < Sub_Top_ToDoList.Count; k++)
        {

            p = loclXp / Sub_Top_ToDoList.Count;
            Sub_Top_XP.Add(p.ToString());
            t = t + p;
        }

        if (t != loclXp)
        {
            Sub_Top_XP[Sub_Top_ToDoList.Count - 1] = (p + (loclXp - t)).ToString();
        }

        Total_XP = loclXp;
        

    }



    public float result;

    public void GetResult()
    {
        result = ((float)(TopicToCheck - 0) / (float)(Sub_Top_ToDoList.Count)) * 100;
        Camera.main.SendMessage("UnloadActually", result);
    }

    public void FindGameObjects()
    {
        F_PointObj = GameObject.Find("F_Point");
        Zekki_Middle = GameObject.Find("Zekki_Middle");
        Ball = GameObject.Find("Golf_Ball");
        Ball.SetActive(false);
        Zekki_Up = GameObject.Find("Zekki_Up"); //Instantiate(Zekki_Middle,Zekki_Middle.transform.parent);
        Zekki_Down = GameObject.Find("Zekki_Down"); //Instantiate(Zekki_Middle, Zekki_Middle.transform.parent);
        ParabolaLine_Plus = GameObject.Find("ParabolaLine_Plus").GetComponent<LineRenderer>();
        ParabolaLine_Minus =GameObject.Find("ParabolaLine_Minus").GetComponent<LineRenderer>();
        ParaDotted_Plus = GameObject.Find("ParabolaDotted_Plus").GetComponent<LineRenderer>();
        ParaDotted_Minus = GameObject.Find("ParabolaDotted_Minus").GetComponent<LineRenderer>();
        F_Point_Slider = GameObject.Find("F_Point_Slider").GetComponent<Slider>();
        Ball_Move_Slider = GameObject.Find("Ball_Move_Slider").GetComponent<Slider>();
        ThrowBall = GameObject.Find("ThrowBall").GetComponent<Button>();
        R1_Dist_Line = GameObject.Find("R1_distance_line").GetComponent<LineRenderer>();
        R2_Dist_Line = GameObject.Find("R2_distance_line").GetComponent<LineRenderer>();
        CommonPoint_R1_R2 = GameObject.Find("CommonPoint_R1_R2");
        R1_StartPoint = GameObject.Find("R1_StartPoint");
        MidPoint = GameObject.Find("MidPoint");
        R1_TMP = GameObject.Find("R1_TMP");
        R2_TMP = GameObject.Find("R2_TMP");
        r1_Val_Text= GameObject.Find("r1_Val_Text").GetComponent<Text>();
        r2_Val_Text= GameObject.Find("r2_Val_Text").GetComponent<Text>();

        Canvas_BG = GameObject.Find("Canvas_BG");

        if (!IsAR)
        {
            Canvas_BG.transform.parent = Camera.main.transform;
            Canvas_BG.transform.GetComponentInChildren<RawImage>().enabled = true;
            Canvas_BG.GetComponent<Canvas>().worldCamera = Camera.main;
        }

        else
        {
            Canvas_BG.SetActive(false);
        }
        //GameObject content = GameObject.Find("Content");


        SidePannel = GameObject.Find("SidePanel");
        SidePannelOpen = GameObject.Find("PannelOpenButton").GetComponent<Button>();
        SidePannelOpen.gameObject.SetActive(false);
        //ValueDisplaySlider = GameObject.Find("ValueDisplaySlider").GetComponent<Slider>();
        InstructionPanel_DD = GameObject.Find("DD_Instruction");
        InstructionPanel = GameObject.Find("InstructionPanel");
        ToDoList_DD = GameObject.Find("ToDoList_Dropdown").GetComponent<Dropdown>();
        XP_ToShow = GameObject.Find("Gained_XP_Value").GetComponent<Text>();
        XP_Effect = GameObject.Find("Effect_Text").GetComponent<Text>();
        Main_XP_ToShow = GameObject.Find("Main_XP_Value").GetComponent<Text>();
        ObjectivePanel = GameObject.Find("ObjectivePanel");
        InstructionPanel.transform.localScale = Vector3.zero;
        InfoPanel = GameObject.Find("InformationPanel");
        Info_Close_Btn = GameObject.Find("InfoCloseBtn").GetComponent<Button>();
        Info_Cntue_Btn = GameObject.Find("InfoContinueBtn").GetComponent<Button>();
        Info_Open_Btn = GameObject.Find("InfoButton").GetComponent<Button>();
        Info_Close_Btn.gameObject.SetActive(false);
        GlassPanel = GameObject.Find("GlassEffect");
        Instruction_Txt = GameObject.Find("IListText").GetComponent<Text>();
        Assumption_Txt = GameObject.Find("AListText").GetComponent<Text>();
        Learninggoal_Txt = GameObject.Find("LGListText").GetComponent<Text>();
        Heading_Txt = GameObject.Find("InfoHeadingText").GetComponent<Text>();
        Reset_Btn = GameObject.Find("Reset_Exp").GetComponent<Button>();


        
        if (OptionUnlockedTill == 0)
        {
            /*ToDoList_DD.GetComponent<RectTransform>().anchoredPosition = new Vector3(150f, -150f, 0);
            XP_ToShow.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(150f, -150f, 0);
            SidePannel.GetComponent<RectTransform>().anchoredPosition = new Vector3(-600f, 0f, 0);*/
            ObjectivePanel.transform.localScale = new Vector3(1, 1, 1);
            IsTopicUnlocked = false;
        }
        else
        {
            ToDoList_DD.GetComponent<RectTransform>().anchoredPosition = new Vector3(-250f, -150f, 0);
            XP_ToShow.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(-250f, -150f, 0);
            ObjectivePanel.transform.localScale = new Vector3(1, 0, 1);
            IsTopicUnlocked = true;
        }
        Main_XP_ToShow.text = totXp.ToString();

        XP_ToShow.text = "0/" + Total_XP;



        for (int i = 0; i < Sub_Top_ToDoList.Count; i++)
        {

            ToDoList_DD.options.Add(new Dropdown.OptionData() { text = Sub_Top_ToDoList[i] });


            //List<float> TotalXP = Sub_Top_XP.Select(s => float.Parse(s)).ToList();

            //Total_XP += TotalXP[i];

            //XP_ToShow.text = "0/" + Total_XP;


            ObjectivePanel.transform.Find("ObjectiveText").GetComponent<Text>().text = Sub_Top_ToDoList[TopicToCheck];




        }


        GlassPanel.transform.localScale = Vector3.zero;
        InfoPanel.transform.localScale = Vector3.zero;
        OpenSidePannel();
        Invoke("OpenInfoPanel", 0.1f);

        Heading_Txt.text = LearningGoals[0];
        Learninggoal_Txt.text = LearningGoals[1];
        Instruction_Txt.text = LearningGoals[2];
        Assumption_Txt.text = LearningGoals[3];

       
        
    }


    public void AssignEventsToObjects()
    {





        SidePannelOpen.onClick.AddListener(OpenSidePannel);
        //TopSidePannelOpen.onClick.AddListener(OpenTopSidePannel);
        


        EventTrigger.Entry PointerDown = new EventTrigger.Entry();
        PointerDown.eventID = EventTriggerType.EndDrag;
        PointerDown.callback.AddListener((data) => { DisableOrbit(); });

        //ScrollBarViewPort.gameObject.GetComponent<EventTrigger>().triggers.Add(PointerDown);

        EventTrigger.Entry PointerUp = new EventTrigger.Entry();
        PointerUp.eventID = EventTriggerType.BeginDrag;
        PointerUp.callback.AddListener((data) => { EnableOrbit(); });

        //ScrollBarViewPort.gameObject.GetComponent<EventTrigger>().triggers.Add(PointerUp);

        /*for (int i = 0; i < SelectionButtons.Length; i++)
        {
            int j = i;
            SelectionButtons[i].onClick.AddListener(() => PropertyToDisplay(j));
            // print(SelectionButtons[i].name);
            old_color = SelectionButtons[0].transform.GetChild(1).GetComponent<Text>().color;
        }*/
        ToDoList_DD.GetComponent<EventTrigger>().triggers.Clear();

        AddListenerToEvents(EventTriggerType.PointerClick, ExploringToDOList_DD, ToDoList_DD.gameObject);
        AddListenerToEvents(EventTriggerType.Select, ToDoList_DDCancel, ToDoList_DD.gameObject);
        Info_Close_Btn.onClick.AddListener(() => CloseInfoPanel(Info_Close_Btn));
        Info_Cntue_Btn.onClick.AddListener(() => CloseInfoPanel(Info_Cntue_Btn));
        Info_Open_Btn.onClick.AddListener(OpenInfoPanel);

        AddListenerToEvents(EventTriggerType.PointerDown, DisableOrbit, F_Point_Slider.gameObject);
        AddListenerToEvents(EventTriggerType.PointerUp, EnableOrbit, F_Point_Slider.gameObject);
        F_Point_Slider.onValueChanged.AddListener(delegate { DrawParabolaLines(F_Point_Slider.value); });
        AddListenerToEvents(EventTriggerType.PointerUp, SliderCondCheck, F_Point_Slider.gameObject);

        AddListenerToEvents(EventTriggerType.PointerDown, DisableOrbit, Ball_Move_Slider.gameObject);
        AddListenerToEvents(EventTriggerType.PointerUp, EnableOrbit, Ball_Move_Slider.gameObject);
        Ball_Move_Slider.onValueChanged.AddListener(delegate { ChangeBallWithSlider(Ball_Move_Slider.value); });
        AddListenerToEvents(EventTriggerType.PointerUp, SliderCondCheck, Ball_Move_Slider.gameObject);

        ThrowBall.onClick.AddListener(BallMove);

        /*AddListenerToEvents(EventTriggerType.BeginDrag, OnElementBeginDrag, GameObject.Find("RawImage"));
        AddListenerToEvents(EventTriggerType.EndDrag, OnElementEndDrag, GameObject.Find("RawImage"));
        AddListenerToEvents(EventTriggerType.PointerClick, BackToPT, GameObject.Find("RawImage"));*/

    }

    public void DrawParabolaLines(float a_Val)
    {

        if (!IsTopicUnlocked )
        {
            if (TopicToCheck == 1)
            {
                //if (CheckFor_XP_Condition(TopicToCheck))
                    return;
            }
            //else
            //{
            //    CheckFor_XP_Condition(TopicToCheck);
            //}
        }

        LinePos_Plus = new List<Vector3>(0);
        LinePos_Minus = new List<Vector3>(0);

       
        F_PointObj.transform.localPosition = new Vector3(InitPos + (a_Val*0.0125f), F_PointObj.transform.localPosition.y, F_PointObj.transform.localPosition.z);
        MidPoint.transform.localPosition = new Vector3((InitPos + F_PointObj.transform.localPosition.x) / 2, 0, 0);
        ParabolaLine_Plus.transform.parent.localPosition =new Vector3(MidPoint.transform.localPosition.x, 0,0);
        

        float GlobalPos_X = 0;
        for (int i = 0; i < 100; i++)
        {
            GlobalPos_X = (ParabolaLine_Plus.transform.position.x + 0.004f * i);
            if (GlobalPos_X > (InitPos+0.25f))
                break;

            Vector3 Pos =new Vector3(ParabolaLine_Plus.transform.localPosition.x + 0.004f *i,0, ParabolaLine_Plus.transform.position.z+Mathf.Sqrt(4* a_Val/150 * 0.004f * i));
            //print("pos......"+Pos.x);
            

            LinePos_Plus.Add(Pos);

            Vector3 Pos2 = new Vector3(ParabolaLine_Minus.transform.localPosition.x+0.004f * i, 0, ParabolaLine_Minus.transform.position.z - Mathf.Sqrt(4 * a_Val/150 * 0.004f * i));
            LinePos_Minus.Add(Pos2);
        }
        ParabolaLine_Plus.positionCount = 0;
        ParaDotted_Plus.positionCount = 0;
        //ParabolaLine_Plus.positionCount = LinePos_Plus.Count;
        //ParabolaLine_Plus.SetPositions(LinePos_Plus.ToArray());

        ParabolaLine_Minus.positionCount = 0;
        ParaDotted_Minus.positionCount = 0;
        //ParabolaLine_Minus.positionCount = LinePos_Minus.Count;
        //ParabolaLine_Minus.SetPositions(LinePos_Minus.ToArray());

        Vector3 Global_X_Up = LinePos_Plus[LinePos_Plus.Count - 1];
        Vector3 Global_X_Down = LinePos_Minus[LinePos_Minus.Count - 1];

        Zekki_Middle.transform.localPosition =new Vector3(ParabolaLine_Plus.transform.parent.localPosition.x-0.02f,Zekki_Middle.transform.localPosition.y, Zekki_Middle.transform.localPosition.z);
        Zekki_Up.transform.localPosition = new Vector3(Global_X_Up.x+ ParabolaLine_Plus.transform.parent.localPosition.x, Zekki_Up.transform.localPosition.y, Global_X_Up.z - 0.025f);
        Zekki_Down.transform.localPosition = new Vector3(Global_X_Down.x+ ParabolaLine_Plus.transform.parent.localPosition.x, Zekki_Down.transform.localPosition.y, Global_X_Down.z-0.025f);
        Ball.transform.localPosition = new Vector3(Global_X_Up.x + ParabolaLine_Plus.transform.parent.localPosition.x, Ball.transform.localPosition.y, Global_X_Up.z);


        Ball_Move_Slider.minValue = -LinePos_Plus.Count;
        Ball_Move_Slider.maxValue = LinePos_Minus.Count;
        Ball_Move_Slider.value = -LinePos_Minus.Count ;

        if (R1_TMP.activeInHierarchy)
        {
            R1_StartPoint.SetActive(false);//transform.localScale = Vector3.zero;// new Vector3(InitPos,0,0);
            CommonPoint_R1_R2.SetActive(false);//.transform.localScale = Vector3.zero;//new Vector3(InitPos, 0, 0);
            R1_TMP.SetActive(false);//.transform.localScale = Vector3.zero;
            R2_TMP.SetActive(false);//.transform.localScale = Vector3.zero;

            R1_Dist_Line.SetPosition(0, Vector3.zero);
            R1_Dist_Line.SetPosition(1, Vector3.zero);

            R2_Dist_Line.SetPosition(0, Vector3.zero);
            R2_Dist_Line.SetPosition(1, Vector3.zero);

            Ball.SetActive(false);
        }

        F_Point_Val = a_Val;
    }

    public void BallMove()
    {
        //iTween.ValueTo(ParabolaLine_Plus.gameObject,iTween.Hash("from", LinePos_Plus.Count-1,"to",0,"time",1,"easetype",iTween.EaseType.linear
        //    , "onupdate", "ChangeBallPos", "onupdatetarget",this.gameObject));

        if (!IsTopicUnlocked)
        {
            if (TopicToCheck == 0 || TopicToCheck == 1)
            {
                if (CheckFor_XP_Condition(TopicToCheck))
                    return;
            }
            
        }

        Ball.SetActive(true);
        NextPos = LinePos_Plus.Count ;
        ParabolaLine_Plus.positionCount = 0;
        ParabolaLine_Minus.positionCount = 0;
        iTween.RotateTo(Zekki_Up.transform.GetChild(1).gameObject, iTween.Hash("x",25,"time",0.25f,"looptype",iTween.LoopType.pingPong,"easetype",iTween.EaseType.easeInOutSine
           , "islocal", true, "oncomplete", "DestroyZekkiTween", "oncompletetarget",this.gameObject));
        IsBallMoving = true;
        if (IsInvoking("ChangeBallPos"))
            CancelInvoke("ChangeBallPos");

        InvokeRepeating("ChangeBallPos",0.5f,0.025f);
    }

    public void DestroyZekkiTween()
    {
        //print("Called");
        if(Zekki_Up.transform.GetChild(1).GetComponent<iTween>() && Zekki_Up.transform.GetChild(1).localEulerAngles.x < 0.01f)
        Destroy(Zekki_Up.transform.GetChild(1).GetComponent<iTween>());

        if (Zekki_Middle.transform.GetChild(1).GetComponent<iTween>() && Zekki_Middle.transform.GetChild(1).localEulerAngles.y >= 88.9f)
            Destroy(Zekki_Middle.transform.GetChild(1).GetComponent<iTween>());
    }

    public void SliderCondCheck()
    {

        if (!IsTopicUnlocked)
        {
            if (TopicToCheck == 0)
            {
                if (CheckFor_XP_Condition(TopicToCheck))
                {
                    DrawParabolaLines(2f);
                }
                
            }
            if (TopicToCheck == 1)
            {
                if (CheckFor_XP_Condition(TopicToCheck))
                {
                    //DrawParabolaLines(F_Point_Val);
                    Ball_Move_Slider.value = Ball_Move_Slider.minValue;
                }

            }

        }

    }

    public void ChangeBallWithSlider(float SliderVal)
    {
        //print("changenallmove");
        if (!IsTopicUnlocked)
        {
            if (TopicToCheck == 0 )
            {
                //if (CheckFor_XP_Condition(TopicToCheck))
                    return;
            }
            //else
            //{
            //    CheckFor_XP_Condition(TopicToCheck);
            //}
        }

        int SliVal = 0;
        int ParabaolaPlusCnt = 0, ParabaolaMinusCnt = 0;


        if (SliderVal < 0)
        {
            SliVal = -(int)(SliderVal);
            ParabaolaPlusCnt = LinePos_Plus.Count - SliVal;
            ParabaolaMinusCnt = 0;

        }
        else if (SliderVal >= 0)
        {
           
            SliVal = (int)SliderVal;
            ParabaolaPlusCnt = LinePos_Plus.Count;
            ParabaolaMinusCnt = SliVal;

            
            
        }


        ParaDotted_Plus.positionCount = ParabaolaPlusCnt;
        for (int i = 0; i < ParaDotted_Plus.positionCount; i++)
        {
            ParaDotted_Plus.SetPosition(i, LinePos_Plus[LinePos_Plus.Count - i - 1]);
            CommonPoint_R1_R2.transform.localPosition = LinePos_Plus[LinePos_Plus.Count - i - 1] + new Vector3(ParabolaLine_Plus.transform.parent.localPosition.x, 0, ParabolaLine_Plus.transform.parent.localPosition.z);
        }

        ParaDotted_Minus.positionCount = ParabaolaMinusCnt;
        for (int i = 0; i < ParaDotted_Minus.positionCount; i++)
        {
            ParaDotted_Minus.SetPosition(i, LinePos_Minus[i]);
            CommonPoint_R1_R2.transform.localPosition = LinePos_Minus[i] + new Vector3(ParabolaLine_Minus.transform.parent.localPosition.x, 0, ParabolaLine_Minus.transform.parent.localPosition.z);
        }
        Change_R1_R2_Lines();
        //print("slider....." + SliderVal + "......sli....." + SliVal);
    }

    public void ChangeBallPos()
    {
        //print("nextpos......."+NextPos);

        if (ParabolaLine_Plus.positionCount != LinePos_Plus.Count && NextPos > 0)
        {
            
            if (NextPos==15)// && !Zekki_Middle.transform.GetChild(1).GetComponent<iTween>())
            {
                //print("NextPos......." + NextPos + "....pos......." + Vector3.Distance(Zekki_Middle.transform.localPosition, Ball.transform.localPosition));

                iTween.RotateTo(Zekki_Middle.transform.GetChild(1).gameObject, iTween.Hash("y", 25, "time", 0.2f, "looptype", iTween.LoopType.pingPong, "easetype", iTween.EaseType.easeInOutSine
            ,"islocal",true, "oncomplete", "DestroyZekkiTween", "oncompletetarget", this.gameObject));
            }
            NextPos--;
            ParabolaLine_Plus.positionCount = LinePos_Plus.Count - NextPos;
            Ball.transform.localPosition = LinePos_Plus[NextPos] + new Vector3(ParabolaLine_Plus.transform.parent.localPosition.x, Ball.transform.localPosition.y, +ParabolaLine_Plus.transform.parent.localPosition.z);
            ParabolaLine_Plus.SetPosition(LinePos_Plus.Count-NextPos-1, LinePos_Plus[NextPos]);
        }
        else if (ParabolaLine_Minus.positionCount != LinePos_Minus.Count && NextPos < LinePos_Minus.Count - 1)
        {
            NextPos++;
            ParabolaLine_Minus.positionCount = NextPos+1;
            Ball.transform.localPosition = LinePos_Minus[NextPos] + new Vector3(ParabolaLine_Plus.transform.parent.localPosition.x, Ball.transform.localPosition.y, +ParabolaLine_Plus.transform.parent.localPosition.z);
            ParabolaLine_Minus.SetPosition(NextPos, LinePos_Minus[NextPos]);
        }
        else
        {
            CancelInvoke("ChangeBallPos");
            IsBallMoving = false;
        }

        Change_R1_R2_Lines();
        CommonPoint_R1_R2.transform.localPosition = Ball.transform.localPosition;

        

    }

    public void Change_R1_R2_Lines()
    {
        if (!R1_TMP.activeInHierarchy)
        {
            R1_StartPoint.SetActive(true);//.transform.localScale = new Vector3(0.005F,0.005F,0.005F);
            CommonPoint_R1_R2.SetActive(true);//.transform.localScale = new Vector3(0.005F, 0.005F, 0.005F);
            R1_TMP.SetActive(true);
            R2_TMP.SetActive(true);
        }
        float Distance = 0;
        if (!IsBallMoving)
        {
            R1_StartPoint.transform.localPosition = new Vector3(InitPos + R1_Dist_Line.transform.localPosition.x, 0, CommonPoint_R1_R2.transform.localPosition.z);
            R1_Dist_Line.SetPosition(0, new Vector3(R1_StartPoint.transform.localPosition.x, 0.001f, CommonPoint_R1_R2.transform.localPosition.z));
            R1_Dist_Line.SetPosition(1, CommonPoint_R1_R2.transform.localPosition);

            R2_Dist_Line.SetPosition(0, F_PointObj.transform.localPosition);
            R2_Dist_Line.SetPosition(1, CommonPoint_R1_R2.transform.localPosition);

            R1_TMP.transform.localPosition = GetMidPoint(R1_StartPoint, CommonPoint_R1_R2);
            R2_TMP.transform.localPosition = GetMidPoint(F_PointObj, CommonPoint_R1_R2) + new Vector3(GetMidPoint(F_PointObj, Ball).x / 4, 0, 0);

            Distance = Vector3.Distance(R1_StartPoint.transform.localPosition, CommonPoint_R1_R2.transform.localPosition) * 100;
            r1_Val_Text.text = Distance.ToString("F2");
            r2_Val_Text.text = Distance.ToString("F2");
        }
        else
        {
            R1_StartPoint.transform.localPosition = new Vector3(InitPos + R1_Dist_Line.transform.localPosition.x, 0, Ball.transform.localPosition.z);
            R1_Dist_Line.SetPosition(0, new Vector3(R1_StartPoint.transform.localPosition.x, 0.001f, Ball.transform.localPosition.z));
            R1_Dist_Line.SetPosition(1, Ball.transform.localPosition);

            R2_Dist_Line.SetPosition(0, F_PointObj.transform.localPosition);
            R2_Dist_Line.SetPosition(1, Ball.transform.localPosition);

            R1_TMP.transform.localPosition = GetMidPoint(R1_StartPoint, Ball);
            R2_TMP.transform.localPosition = GetMidPoint(F_PointObj, Ball)+new Vector3(GetMidPoint(F_PointObj, Ball).x / 4, 0,0);

            Distance = Vector3.Distance(R1_StartPoint.transform.localPosition, Ball.transform.localPosition) * 100;
            r1_Val_Text.text = Distance.ToString("F2");
            r2_Val_Text.text = Distance.ToString("F2");
        }
    }

    public Vector3 GetMidPoint(GameObject obj1, GameObject obj2)
    {
        Vector3 midp = (obj1.transform.localPosition + obj2.transform.localPosition) / 2;
        return midp;
    }


    

    
    

    

    public bool CheckFor_XP_Condition(int Current_Todo)
    {
        bool Condition = true;
        if (Current_Todo == 0)
        {
            if (F_Point_Slider.value>2)
            {
                Condition = false;
                Collect_XP(Zekki_Middle);
            }
            else
            {
                Ball_Move_Slider.value = Ball_Move_Slider.minValue;
                ShowInstructionPanel("Wrong parameter chosen");
                Condition = true;
            }
        }
        if (Current_Todo == 1)
        {
            if (Ball_Move_Slider.value > -LinePos_Plus.Count)
            {
                Condition = false;
                Collect_XP(Zekki_Middle);
            }
            else
            {
                F_Point_Slider.value = F_Point_Val;
                ShowInstructionPanel("Wrong parameter chosen");
                Condition = true;
            }
        }

        return Condition;
    }


    public void ShowInstructionPanel(string TextToDisplay)
    {
        var mouse = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);



        if (mouse.x < Screen.width / 2)
        {
            // WronOptionPanel
            //print("Mouse is on left side of screen.");
            InstructionPanel.GetComponent<RectTransform>().pivot = new Vector2(0, 0);
        }



        if (mouse.x > Screen.width / 2)
        {
            //print("Mouse is on right side of screen.");
            InstructionPanel.GetComponent<RectTransform>().pivot = new Vector2(1, 0);
        }

        InstructionPanel.transform.position = Input.mousePosition;
        UIcanvas.GetComponent<GraphicRaycaster>().enabled = false;
        Camera.main.GetComponent<PhysicsRaycaster>().enabled = false;
        //iTween.Stop(InstructionPanel.gameObject);

        InstructionPanel.transform.Find("InstructionText").GetComponent<Text>().text = TextToDisplay;
        InstructionPanel.transform.GetChild(0).GetComponent<Text>().color = GetColorFromColorCode("#FF7500");
        //iTween.ScaleTo(GlassPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 0.01f, "easetype", iTween.EaseType.spring));
        iTween.ScaleTo(InstructionPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 0.3f, "easetype", iTween.EaseType.spring, "oncomplete", "CloseInstructionPanel", "oncompletetarget", this.gameObject));
        ObjectivePanel.transform.localScale = new Vector3(1, 0, 1);
    }

    public void ShowWelldonePanel(string TextToShow)
    {
        var mouse = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);



        if (mouse.x < Screen.width / 2)
        {
            // WronOptionPanel
            //print("Mouse is on left side of screen.");
            InstructionPanel.GetComponent<RectTransform>().pivot = new Vector2(0, 0);
        }



        if (mouse.x > Screen.width / 2)
        {
            //print("Mouse is on right side of screen.");
            InstructionPanel.GetComponent<RectTransform>().pivot = new Vector2(1, 0);
        }

        InstructionPanel.transform.position = Input.mousePosition;
        UIcanvas.GetComponent<GraphicRaycaster>().enabled = false;
        Camera.main.GetComponent<PhysicsRaycaster>().enabled = false;
        //iTween.Stop(InstructionPanel.gameObject);

        //iTween.ScaleTo(GlassPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 0.01f, "easetype", iTween.EaseType.spring));
        InstructionPanel.transform.GetChild(0).GetComponent<Text>().text = TextToShow;// "Wrong parameter choosen";
        InstructionPanel.transform.GetChild(0).GetComponent<Text>().color = GetColorFromColorCode("#AAC018");
        iTween.ScaleTo(InstructionPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 1f, "easetype", iTween.EaseType.spring, "oncomplete", "CloseInstructionPanel", "oncompletetarget", this.gameObject));
        ObjectivePanel.transform.localScale = new Vector3(1, 0, 1);
    }


    public void AddListenerToEvents(EventTriggerType eventType, Action MethodToCall, GameObject TriggerObjToAdd)
    {

        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall());
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }

    public void AddListenerToEvents(EventTriggerType eventType, Action<float> MethodToCall, GameObject TriggerObjToAdd, float p)
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall(p));
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }

    public void AddListenerToEvents(EventTriggerType eventType, Action<GameObject> MethodToCall, GameObject TriggerObjToAdd, GameObject p)
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall(p));
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }

    public void EnableOrbit()
    {

        if (!IsAR)
        {
            CamOrbit.DisableInteration = false;
            //print("EnableOrbit");
        }
        else
        {
            ArOrbit.DisableInteration = false;
        }
    }

    public void DisableOrbit()
    {

        if (!IsAR)
        {
            CamOrbit.DisableInteration = true;
            //print("DisableOrbit");
        }
        else
        {
            ArOrbit.DisableInteration = true;
        }
    }

    public void OpenInfoPanel()
    {
        UIZoomIn();
        iTween.ScaleTo(InfoPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 0.5f, "easetype", iTween.EaseType.spring));
        iTween.ScaleTo(GlassPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 0.01f, "easetype", iTween.EaseType.spring));

    }

    public void CloseInfoPanel(Button CloseBtn)
    {
        UIZoomOut();
        iTween.ScaleTo(InfoPanel, iTween.Hash("x", 0, "y", 0, "z", 0, "time", 0.2f, "easetype", iTween.EaseType.linear));
        iTween.ScaleTo(GlassPanel, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.2f, "time", 0.01f, "easetype", iTween.EaseType.linear));
        if (CloseBtn == Info_Cntue_Btn)
        {
            if (!IsAR)
                CamOrbit.DisableInteration = false;
            /*if (ActiveBtnCnt == 0)
            {
                for (int i = 0; i < Elements.Length; i++)
                {

                    Vector3 RandomPos = new Vector3(Elements[i].transform.localPosition.x, Elements[i].transform.localPosition.y, UnityEngine.Random.Range(-5f, 0f));
                    Vector3 CurrPos = Elements[i].transform.localPosition;

                    Elements[i].transform.localPosition = RandomPos;

                    iTween.MoveTo(Elements[i], iTween.Hash("position", CurrPos, "delay", 0.7f, "islocal", true, "time", UnityEngine.Random.Range(0.1f, 0.2f), "speed", 2.5f, "easetype", iTween.EaseType.easeInOutSine));


                }
            }*/
            Info_Close_Btn.gameObject.SetActive(true);
            Info_Cntue_Btn.gameObject.SetActive(false);
            
            
        }

    }
    
    public void CloseInstructionPanel()
    {
        UIcanvas.GetComponent<GraphicRaycaster>().enabled = true;
        Camera.main.GetComponent<PhysicsRaycaster>().enabled = true;
        //iTween.ScaleTo(GlassPanel, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 1f, "time", 0.01f, "easetype", iTween.EaseType.linear));
        iTween.ScaleTo(InstructionPanel, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.5f, "time", 0.3f, "easetype", iTween.EaseType.spring));
        if (!IsTopicUnlocked)
            iTween.ScaleTo(ObjectivePanel, iTween.Hash("y", 1, "time", 0.3f, "delay", 0.5f, "easetype", iTween.EaseType.easeInOutSine));
    }

    public void ToDoList_DDCancel()
    {
        if (ToDoList_DD.transform.Find("Dropdown List") != null)//&& Obj.transform.localScale.y!=1)
        {
            //print("cancelled........");
            //print("list.....cancelled........" + ToDoList_DD.transform.Find("Dropdown List").gameObject);
            ToDoList_DD.transform.Find("Arrow").transform.eulerAngles = new Vector3(0, 0, 0);
            iTween.ScaleTo(ToDoList_DD.transform.Find("Dropdown List").gameObject, iTween.Hash("y", 0, "time", 0.5f, "easetype", iTween.EaseType.spring));
            iTween.ScaleTo(ObjectivePanel, iTween.Hash("y", 1, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine));
            if (SidePannelOpen.transform.eulerAngles.z == 180 )
            {
                OpenSidePannel();
            }
        }
    }

    public void ExploringToDOList_DD()
    {
        //print("ShowDD.....");
        //ToDoList_DD.transform.Find("Topic_Objective").GetComponent<Text>().text = "";
        if (SidePannelOpen.transform.eulerAngles.z == 0)
        {
            OpenSidePannel();
        }

        ToDoList_DD.transform.Find("Arrow").transform.eulerAngles = new Vector3(0, 0, 180);
        GameObject DDList = ToDoList_DD.transform.Find("Dropdown List").gameObject;
        DDList.transform.GetComponent<RectTransform>().sizeDelta = new Vector2(0, 250);
        DDList.transform.localScale = new Vector3(1, 0, 1);
        iTween.ScaleTo(DDList, iTween.Hash("y", 1, "time", 0.5f, "easetype", iTween.EaseType.spring));
        //iTween.ScaleTo(ToDoList_DD.transform.Find("Topic_Objective").gameObject, iTween.Hash("y", 0, "time", 0.5f, "easetype", iTween.EaseType.spring));
        if (!IsTopicUnlocked)
            iTween.ScaleTo(ObjectivePanel, iTween.Hash("y", 0, "time", 0.5f, "delay", 0.1f, "easetype", iTween.EaseType.easeInOutSine));

        string TestStr = "";
        int CntToChangePos = 0;
        for (int i = 0; i < ToDoList_DD.options.Count; i++)
        {
            GameObject Child = GameObject.Find("Item " + i + ": " + ToDoList_DD.options[i].text);
            //Child.transform.parent.GetComponent<RectTransform>().sizeDelta = new Vector2(0,250);

            if (TestStr == ToDoList_DD.options[i].text)
            {
                Child.SetActive(false);
                Child.transform.localPosition = Child.transform.localPosition + new Vector3(0, 50 * CntToChangePos, 0);
                CntToChangePos++;
            }
            else
            {
                Child.transform.Find("XP_Text").GetComponent<Text>().text = "+" + Sub_Top_XP[i] + "XP";
                Child.transform.localPosition = Child.transform.localPosition + new Vector3(0, 50 * CntToChangePos, 0);

                if (i < TopicToCheck)
                {
                    Child.transform.Find("Item Checkmark").GetComponent<Image>().enabled = true;
                    //Child.transform.Find("XP_Text").GetComponent<Text>().color = Color.green / 2;
                    //Child.transform.Find("Item Checkmark").GetComponent<Image>().color = Color.green / 2;
                    Child.transform.Find("Item Background").GetComponent<Image>().enabled = false;
                }
                else if (i == TopicToCheck)
                {
                    Child.transform.Find("Item Background").GetComponent<Image>().enabled = true;// color = ReturnColorFromHex("#a01c65");
                                                                                                 //Child.transform.Find("Item Label").GetComponent<Text>().color = Color.white;
                }
            }
            TestStr = ToDoList_DD.options[i].text;
        }
    }


    public Color GetColorFromColorCode(string ColorCode)
    {
        Color ConvColor;
        bool Converted = ColorUtility.TryParseHtmlString(ColorCode, out ConvColor);
        return ConvColor;
    }



    public void ZoomCamera(float Value)
    {
        CamOrbit.distance = Value;
    }







    public void OpenSidePannel()
    {
        if (SidePannelOpen.transform.eulerAngles.z == 180)
        {
            //iTween.MoveAdd(SidePannel, iTween.Hash("x", -400f, "time",0.5f,"islocal",true, "easetype", iTween.EaseType.linear));
            //iTween.MoveAdd(SidePannelOpen.gameObject, iTween.Hash("x", -185f, "islocal", true, "time", 0.5f, "easetype", iTween.EaseType.linear));
            //SidePannelOpen.transform.eulerAngles = new Vector3(0,0,180);
            SidePannel.GetComponent<Animator>().Play("PannelForward");
        }
        else
        {
            //iTween.MoveAdd(SidePannel, iTween.Hash("x", 400f, "time", 0.5f, "islocal", true, "easetype", iTween.EaseType.linear));
            //iTween.MoveAdd(SidePannelOpen.gameObject, iTween.Hash("x", -185f, "islocal", true, "time", 0.5f, "easetype", iTween.EaseType.linear));
            //SidePannelOpen.transform.eulerAngles = new Vector3(0, 0, 0);
            SidePannel.GetComponent<Animator>().Play("PannelBackward");
        }
    }
    /*public void OpenTopSidePannel()
    {
        //print("Clicked......"+ PannelOpen.transform.eulerAngles.y);
        if (TopSidePannelOpen.gameObject.transform.localEulerAngles.z == 0)
        {
            TopSidePannel.GetComponent<Animator>().Play("TopButtonOpen");
            BtnselName.gameObject.SetActive(false);
            // TopSidePannelOpen.gameObject.transform.localEulerAngles = new Vector3(0, 0, 180);
        }
        else
        {
            TopSidePannel.GetComponent<Animator>().Play("TopButtonClose");
            // TopSidePannelOpen.gameObject.transform.localEulerAngles = new Vector3(0, 0, 0);
        }
    }*/

    // Update is called once per frame
    void FixedUpdate()
    {
        
        if (ParabolaLine_Plus.widthMultiplier != transform.parent.localScale.x)
        {
            ParabolaLine_Plus.widthMultiplier = transform.parent.localScale.x;
            ParabolaLine_Minus.widthMultiplier = transform.parent.localScale.x;
            ParaDotted_Plus.widthMultiplier = transform.parent.localScale.x;
            ParaDotted_Minus.widthMultiplier = transform.parent.localScale.x;
            R1_Dist_Line.widthMultiplier = transform.parent.localScale.x;
            R2_Dist_Line.widthMultiplier = transform.parent.localScale.x;
            F_PointObj.transform.GetComponentInChildren<LineRenderer>().widthMultiplier = transform.parent.localScale.x;


        }
        //if (!IsAR)
        //{

        //    if (CamOrbit.distance < CamOrbit.zoomStart && !IsUIZooming)
        //    {
        //        UIZoomIn();
        //        IsUIZooming = true;
        //    }
        //    else if (CamOrbit.distance > CamOrbit.zoomStart && IsUIZooming)
        //    {
        //        UIZoomOut();
        //        IsUIZooming = false;
        //    }
        //}

        /*if (CamOrbit.Zoom_bool && ActiveBtnCnt<8)
        {
            CamOrbit.zoomMin = CamOrbit.cameraRotUp * -0.01125f + 1.15f;
        }*/
    }



    public void UIReset()
    {
        ObjectivePanel.GetComponent<RectTransform>().anchoredPosition = new Vector3(0f, 5f, 0);

        //ValueDisplaySlider.transform.GetComponent<RectTransform>().anchoredPosition = new Vector3(-20f, 0f, 0);
        Info_Open_Btn.transform.GetComponent<RectTransform>().anchoredPosition = new Vector3(-37f, -150f, 0);


        SidePannel.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(0f, 0f, 0);

        //print("side......" + SidePannel.transform.position+"....local....."+ SidePannel.transform.localPosition);
        if (!IsTopicUnlocked)
        {
            ToDoList_DD.transform.GetComponent<RectTransform>().anchoredPosition = new Vector3(135f, -150f, 0);
            XP_ToShow.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(135f, -150f, 0);

        }
        else
        {
            ToDoList_DD.transform.GetComponent<RectTransform>().anchoredPosition = new Vector3(-250f, -150f, 0);
            XP_ToShow.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(-250f, -150f, 0);

        }

        //Main_XP_ToShow.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(-135f, -60f, 0);

    }

    public void UIZoomOut()
    {
        if (ObjectivePanel.GetComponent<RectTransform>().anchoredPosition.y == 5)
            return;

        UIReset();

        iTween.MoveFrom(Info_Open_Btn.gameObject, iTween.Hash("x", Info_Open_Btn.transform.localPosition.x + 250, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveFrom(SidePannel.transform.parent.gameObject, iTween.Hash("x", SidePannel.transform.parent.localPosition.x - 1000f, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        //iTween.MoveFrom(ValueDisplaySlider.gameObject, iTween.Hash("x", ValueDisplaySlider.transform.localPosition.x + 250, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveFrom(ObjectivePanel, iTween.Hash("y", ObjectivePanel.transform.localPosition.y - 1000, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));


        iTween.MoveFrom(ToDoList_DD.gameObject, iTween.Hash("x", ToDoList_DD.transform.localPosition.x - 400, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveFrom(XP_ToShow.transform.parent.gameObject, iTween.Hash("x", XP_ToShow.transform.parent.localPosition.x - 400, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));

        //iTween.MoveFrom(Main_XP_ToShow.transform.parent.gameObject, iTween.Hash("x", Main_XP_ToShow.transform.parent.localPosition.x + 550, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
    }



    public void UIZoomIn()
    {
        if (ObjectivePanel.GetComponent<RectTransform>().anchoredPosition.y != 5)
            return;

        UIReset();
        //print("side......" + SidePannel.transform.position);
        iTween.MoveAdd(ObjectivePanel, iTween.Hash("y", ObjectivePanel.transform.localPosition.y - 1000, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveAdd(Info_Open_Btn.gameObject, iTween.Hash("x", Info_Open_Btn.transform.localPosition.x + 250, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveAdd(SidePannel.transform.parent.gameObject, iTween.Hash("x", SidePannel.transform.parent.localPosition.x - 1000f, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));

        //iTween.MoveAdd(ValueDisplaySlider.gameObject, iTween.Hash("x", ValueDisplaySlider.transform.localPosition.x + 250, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));

        iTween.MoveAdd(XP_ToShow.transform.parent.gameObject, iTween.Hash("x", XP_ToShow.transform.parent.localPosition.x - 400, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveAdd(ToDoList_DD.gameObject, iTween.Hash("x", ToDoList_DD.transform.localPosition.x - 400, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));

        //iTween.MoveAdd(Main_XP_ToShow.transform.parent.gameObject, iTween.Hash("x", Main_XP_ToShow.transform.parent.localPosition.x + 550, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
    }

    public float ChangeToLocal_X(float Val)
    {
        float X_ToRet = 0;
        //print("wid........"+ UIcanvas.GetComponent<RectTransform>().rect.width);
        X_ToRet = ((UIcanvas.GetComponent<RectTransform>().rect.x) + Val);

        return X_ToRet;
    }

    public float ChangeToLocal_Y(float Val)
    {
        float Y_ToRet = 0;
        //print("wid........"+ UIcanvas.GetComponent<RectTransform>().rect.width);
        Y_ToRet = ((UIcanvas.GetComponent<RectTransform>().rect.y) + Val);

        return Y_ToRet;
    }


    public void Collect_XP(GameObject TargetPos)
    {

        UIcanvas.GetComponent<GraphicRaycaster>().enabled = false;
        Camera.main.GetComponent<PhysicsRaycaster>().enabled = false;


        XP_Effect.transform.position = Camera.main.WorldToScreenPoint(TargetPos.transform.position);

        XP_Effect.text = "+"+Sub_Top_XP[TopicToCheck] + "XP";

        XP_Effect.transform.localScale = new Vector3(2, 0, 2);

        iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 2, "y", 2, "z", 2, "time", 0.5f, "easetype", iTween.EaseType.easeOutElastic));

        iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", XP_Effect.transform.position.x, "y", XP_Effect.transform.position.y + 150, "z", 0, "delay", 0.3f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

        iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", 150, "y", XP_ToShow.transform.position.y, "z", 0, "delay", 1f, "time", 1f, "easetype", iTween.EaseType.easeInSine, "oncomplete", "Add_To_XP", "oncompletetarget", this.gameObject));

    }

    public void Add_To_XP()
    {

        //print("TopicToCheck......."+ TopicToCheck+"....sub......"+Sub_Topics.Count);
        if (TopicToCheck < (Sub_Top_ToDoList.Count))
        {
            Sub_XP_Earned += float.Parse(Sub_Top_XP[TopicToCheck]);

            XP_Effect.text = "";
            XP_ToShow.text = Sub_XP_Earned + "/" + Total_XP;

            TopicToCheck++;
            if (TopicToCheck <= (Sub_Top_ToDoList.Count - 1))
            {
                ObjectivePanel.transform.Find("ObjectiveText").GetComponent<Text>().text = Sub_Top_ToDoList[TopicToCheck];
                ObjectivePanel.transform.localScale = new Vector3(1, 0, 1);
                iTween.ScaleTo(ObjectivePanel, iTween.Hash("y", 1, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine));
                UIcanvas.GetComponent<GraphicRaycaster>().enabled = true;
                Camera.main.GetComponent<PhysicsRaycaster>().enabled = true;
                //ToDoList_DD.transform.Find("Topic_Objective").GetComponent<Text>().text = "Objective: " + Sub_Top_ToDoList[TopicToCheck];
            }
            else
            {
                XP_Effect.transform.position = (XP_ToShow.transform.position);

                XP_Effect.text = Total_XP + "XP";

                XP_Effect.transform.localScale = new Vector3(1, 0, 1);

                iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 2, "y", 2, "z", 2, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeOutElastic));

                iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", XP_Effect.transform.position.x, "y", XP_Effect.transform.position.y + 50, "z", 0, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

                iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", Main_XP_ToShow.transform.position.x, "y", Main_XP_ToShow.transform.position.y, "z", 0, "delay", 1.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine, "oncomplete", "AddTotalToMainXP", "oncompletetarget", this.gameObject));

                iTween.MoveTo(ToDoList_DD.gameObject, iTween.Hash("x", ChangeToLocal_X(-400), "delay", 3.5f, "time", 0.5f, "islocal", true, "easetype", iTween.EaseType.easeInOutSine));

                iTween.MoveTo(XP_ToShow.transform.parent.gameObject, iTween.Hash("x", ChangeToLocal_X(-400), "delay", 3.5f, "time", 0.5f, "islocal", true, "easetype", iTween.EaseType.easeInOutSine));
                //print("label......" + LabelImage.GetComponent<RectTransform>().anchoredPosition+".......explode...."+ ExplodeSlider.GetComponent<RectTransform>().anchoredPosition);


                //iTween.MoveTo(ValueDisplaySlider.gameObject, iTween.Hash("x", ChangeToLocal_X ( 150), "delay", 0.7f, "time", 0.5f, "islocal", true, "easetype", iTween.EaseType.easeInOutSine));
            }
        }

    }

    public void AddTotalToMainXP()
    {
        if (OptionUnlockedTill < 1)
        {
            OptionUnlockedTill++;
            UIcanvas.GetComponent<GraphicRaycaster>().enabled = true;
            Camera.main.GetComponent<PhysicsRaycaster>().enabled = true;
            float MainXp = float.Parse(Main_XP_ToShow.text);
            MainXp += Total_XP;
            Main_XP_ToShow.text = MainXp.ToString();
            //ObjectivePanel.transform.Find("ObjectiveText").GetComponent<Text>().text = "YaY!......Next topic unlocked";
            XP_Effect.text = "";
            IsTopicUnlocked = true;
            iTween.ScaleTo(ObjectivePanel, iTween.Hash("y", 0, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine));



        }
    }
}
