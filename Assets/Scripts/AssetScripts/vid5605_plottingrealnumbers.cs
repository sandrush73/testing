﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;

public class vid5605_plottingrealnumbers : MonoBehaviour, IPointerDownHandler
{
    // Start is called before the first frame update

    public Canvas C_BG;

    public orbit CamOrbit;

    public AROrbitControls ArOrbit;

    public bool IsAR;

    public GameObject MainObj, Ground;

    public int OptionUnlockedTill, CurrentTopicSelected, TopicToCheck;

    public float Sub_XP_Earned, Total_XP;

    public Dropdown Options_DD, ToDoList_DD;

    public GameObject InstructionPanel_DD, ToDoList_Panel;

    public List<Dictionary<string, object>> CSV_data;

    public List<String> TaskList, XpList, InfoText;

    public List<GameObject> AllTaskData;

    public GameObject[] Labels;

    public Text XP_ToShow, XP_Effect, Main_XP_ToShow, localTotalXpValueText, localGainedXpValueText, ObjectiveInfoText, LGListText, IListText, AListText,
        InfoHeadingText, GF_ResultTxt, E_valueTxt, m_valtxt;

    public GameObject TaskObj, AllTaskListObj;

    public RectTransform rT;

    public int localTotalXpValue, localGaniedXpValue, MainXpValue;

    public GameObject ControlPanel, LocalXP, Main_Xp, ObjectivePanel, InformationPanel, InfoButton, MainSlider, InfoPanelBg, TasklistPanel, WronOptionPanel, Gained_XP_ArrowBut;

    public Button InfoCloseBtn, InfoContinueBtn, Gained_XP_Panel, CpCloseBtn, IncButton1, DecButton1, IncButton2, DecButton2;

    public bool sessionStart;

    public TextAsset CSVFile;

    public AssetBundle assetBundle;

    public string[] CorrectMsgs = new string[] { "Nice!", "WellDone!", "Excellent!", "Correct!" };

    public Color Highlight_Color, Normal_Color;

    public Text MessageT, DescText;

    //....................................................................................................................................

    public Image LabelImage;

    public bool Lab;

    public Button PickProtractorBtn, PickCompassBtn, DrawarcBtn, SubmitBtn, ResetBtn;

    public Slider Slider_Angle;

    public GameObject OriginPoint, CompassObject, proractorObj, plane_geo_01281, QuestionPanel;

    public GameObject Arcs, Lines, SquareRoots;

    public int indexingNum,StepNum;

    public float[] protractorPlace;

    private LineRenderer lineRenderer;

    public float[] MainprotractorAngles2 = new float[] {-76.5f,-93f, -110f,-130f,-155f};
    public float[] InnerprotractorAnglesY = new float[] { -35f, -30f, -26.5f, -24f, -22.5f };
    public float[] InnerprotractorAnglesZ = new float[] { -41f, -49f, -56.5f, -66f, -77.5f };

    public List<String> QList;


    void Start()
    {
        LoadFromAsssetBundle();

        MainObj = this.gameObject;

        C_BG = GameObject.Find("Canvas_BG").GetComponent<Canvas>();

        Ground = GameObject.Find("Ground");

        if (!IsAR)
        {
            CamOrbit = Camera.main.GetComponentInParent<orbit>();

            C_BG.transform.parent = Camera.main.transform;
            C_BG.GetComponentInChildren<RawImage>().enabled = true;
            C_BG.renderMode = RenderMode.ScreenSpaceCamera;

            C_BG.worldCamera = Camera.main;

            CamOrbit.target = Camera.main.transform.parent;

            CamOrbit.target.position = new Vector3(0, 0.01f, 0);
            CamOrbit.maxRotUp = 45;
            CamOrbit.minRotUp = 1;
            CamOrbit.zoomMin = 0.65f;
            CamOrbit.zoomMax = 2;
            CamOrbit.zoomStart = 1f;
            CamOrbit.cameraRotUpStart = 30f;

            CamOrbit.Start();
        }
        else
        {
            Ground.SetActive(false);
            C_BG.gameObject.SetActive(false);
            ArOrbit = MainObj.transform.parent.gameObject.GetComponent<AROrbitControls>();
        }

        sessionStart = false;

        //OptionUnlockedTill = 0;
        //TopicToCheck = 1;
        //MainXpValue = 10;

        ReadingDataFromCSVFile();
        ReadingXps();
        FindingObjects();
        TaskListCreation();
        AssigningClickEvents();
        LabelsAssignment();
        AssignInitialValues();

    }

    public void LoadFromAsssetBundle()
    {
        //......................................................For Testing ..............................................................

        //CSVFile = Resources.Load("vid5605_plottingrealnumbersCSV") as TextAsset;

        //............................................................ For Assetbundle Creation ..........................................

         assetBundle = AssetBundle.LoadFromFile(PlayerPrefs.GetString("AssetBundleUrl"));

        CSVFile = assetBundle.LoadAsset(PlayerPrefs.GetString("LibraryName") + "CSV", typeof(TextAsset)) as TextAsset;        

        //.................................................................................................................................
        Camera.main.gameObject.AddComponent<PhysicsRaycaster>();

        Camera.main.backgroundColor = Color.black;

        GameObject TargetForCamera = GameObject.Find("Target");

        if (TargetForCamera != null)
        {
            TargetForCamera.AddComponent<orbit>();
            CamOrbit = TargetForCamera.GetComponent<orbit>();
            IsAR = false;
        }
        else
        {
            this.transform.parent.gameObject.AddComponent<AROrbitControls>();
            ArOrbit = this.transform.parent.gameObject.GetComponent<AROrbitControls>();
            IsAR = true;
        }
    }

    public void ReadingDataFromCSVFile()
    {
        TaskList = new List<string>();
        XpList = new List<string>();
        InfoText = new List<string>();

        AllTaskData = new List<GameObject>();

        CSV_data = CSVReader.Read(CSVFile);

        for (var i = 0; i < CSV_data.Count; i++)
        {
            if (CSV_data[i]["TaskList"].ToString() != "" && CSV_data[i]["TaskList"].ToString() != null)
            {
                TaskList.Add(CSV_data[i]["TaskList"].ToString());
            }

            //if (CSV_data[i]["XPList"].ToString() != "" && CSV_data[i]["XPList"].ToString() != null)
            //{
            //    XpList.Add(CSV_data[i]["XPList"].ToString());
            //}

            if (CSV_data[i]["InfoText"].ToString() != "" && CSV_data[i]["InfoText"].ToString() != null)
            {
                InfoText.Add(CSV_data[i]["InfoText"].ToString());
            }
        }

        QList.Add("Plot √2");
        QList.Add("Plot √3");
        QList.Add("Plot √4");
        QList.Add("Plot √5");
        QList.Add("Plot √6");

    }

    public int totXp, loclXp;

    public void ReadingXps()
    {

        totXp = PlayerPrefs.GetInt("TotalXP");
        loclXp = PlayerPrefs.GetInt("MaxEarnings");

        if (loclXp != 0)
        {
            OptionUnlockedTill = 0;

        }
        else
        {
            OptionUnlockedTill = 1;

        }

        TopicToCheck = 1;

        MainXpValue = totXp;

        int t = 0;
        int p = 0;

        for (int k = 0; k < TaskList.Count; k++)
        {

            p = loclXp / TaskList.Count;
            XpList.Add(p.ToString());
            t = t + p;
        }

        if (t != loclXp)
        {
            XpList[XpList.Count - 1] = (p + (loclXp - t)).ToString();
        }



    }
    public float result;

    public void GetResult()
    {
        result = ((float)(TopicToCheck - 1) / (float)(TaskList.Count)) * 100;
        Camera.main.SendMessage("UnloadActually", result);
    }
    public void FindingObjects()
    {

        InformationPanel = GameObject.Find("InformationPanel");

        InfoCloseBtn = GameObject.Find("InfoCloseBtn").GetComponent<Button>();

        InfoContinueBtn = GameObject.Find("InfoContinueBtn").GetComponent<Button>();

        InfoPanelBg = GameObject.Find("InfoPanelBg");

        XP_ToShow = GameObject.Find("localGainedXpValueText").GetComponent<Text>();

        XP_Effect = GameObject.Find("Effect_Text").GetComponent<Text>();

        Main_XP_ToShow = GameObject.Find("Main_XP_Value").GetComponent<Text>();

        localGainedXpValueText = GameObject.Find("localGainedXpValueText").GetComponent<Text>();

        localTotalXpValueText = GameObject.Find("GainedXP_LocalTotalXp").GetComponent<Text>();

        ObjectiveInfoText = GameObject.Find("ObjectiveInfoText").GetComponent<Text>();

        TaskObj = GameObject.Find("TaskObj");

        AllTaskListObj = GameObject.Find("AllTaskList");

        ControlPanel = GameObject.Find("ControlPanel");
        LocalXP = GameObject.Find("LocalXP");
        Main_Xp = GameObject.Find("Main_Xp");
        ObjectivePanel = GameObject.Find("ObjectivePanel");
        InformationPanel = GameObject.Find("InformationPanel");
        InfoButton = GameObject.Find("InfoButton");
        MainSlider = GameObject.Find("MainSlider");
        WronOptionPanel = GameObject.Find("WrongPanel");

        Gained_XP_Panel = GameObject.Find("Gained_XP_Panel").GetComponent<Button>();
        TasklistPanel = GameObject.Find("TasklistPanel");
        CpCloseBtn = GameObject.Find("CpCloseBtn").GetComponent<Button>();

        InfoHeadingText = GameObject.Find("InfoHeadingText").GetComponent<Text>();
        LGListText = GameObject.Find("LGListText").GetComponent<Text>();
        IListText = GameObject.Find("IListText").GetComponent<Text>();
        AListText = GameObject.Find("AListText").GetComponent<Text>();

        Gained_XP_ArrowBut = GameObject.Find("Gained_XP_ArrowBut");
        
        InfoPanelBg.transform.localScale = new Vector3(1, 1, 1);

        //............................................................................................................

        //IncButton1 = GameObject.Find("IncButton1").GetComponent<Button>();

        //DecButton1 = GameObject.Find("DecButton1").GetComponent<Button>();
        QuestionPanel = GameObject.Find("QuestionPanel");

        Slider_Angle = GameObject.Find("Slider_Angle").GetComponent<Slider>();

        LabelImage = GameObject.Find("ActivateLabels").GetComponent<Image>();

        //m_valtxt = GameObject.Find("m_valtxt").GetComponent<Text>();

        PickProtractorBtn = GameObject.Find("PickProtractorBtn").GetComponent<Button>();

        PickCompassBtn = GameObject.Find("PickCompassBtn").GetComponent<Button>();

        DrawarcBtn = GameObject.Find("DrawarcBtn").GetComponent<Button>();

        SubmitBtn = GameObject.Find("SubmitBtn").GetComponent<Button>();

        ResetBtn = GameObject.Find("ResetBtn").GetComponent<Button>();

        CompassObject = GameObject.Find("CompassObject");

        proractorObj = GameObject.Find("proractorObj");

        plane_geo_01281 = GameObject.Find("plane_geo_01281");

        // plane_geo_01281.AddComponent<DrawRadar>();

        SquareRoots = GameObject.Find("SquareRoots");

        CompassObject.SetActive(false);

        proractorObj.SetActive(false);

        Arcs = GameObject.Find("Arcs");

        Lines = GameObject.Find("LinesOf");      

    }

    public void AssigningClickEvents()
    {
        iTween.ScaleTo(InformationPanel.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

        InfoCloseBtn.onClick.AddListener(() => CloseInfoPanel());

        InfoContinueBtn.onClick.AddListener(() => ContinueInfoPanel());

        InfoButton.GetComponent<Button>().onClick.AddListener(() => OpenInfoPanel());

        Gained_XP_Panel.onClick.AddListener(() => OpenTaskList());

        CpCloseBtn.onClick.AddListener(() => OpenOrCloseControlPanel());

        //IncButton1.GetComponent<Button>().onClick.AddListener(() => ChangeBallMass(1));
        //DecButton1.GetComponent<Button>().onClick.AddListener(() => ChangeBallMass(-1));

        //MainSlider.GetComponent<Slider>().onValueChanged.AddListener(delegate { ChangeAngle(MainSlider.GetComponent<Slider>().value, MainSlider.GetComponentInChildren<Text>()); });

        AddListener(EventTriggerType.PointerDown, DisableOrbit, MainSlider.gameObject);
        AddListener(EventTriggerType.PointerUp, EnableOrbit, MainSlider.gameObject);

        LabelImage.GetComponent<Button>().onClick.AddListener(() => ToggleActivateLabels());

        PickProtractorBtn.GetComponent<Button>().onClick.AddListener(() => ShowProtractor());

        Slider_Angle.GetComponent<Slider>().onValueChanged.AddListener(delegate { ChangeAngle(Slider_Angle.GetComponent<Slider>().value, Slider_Angle.GetComponentInChildren<Text>()); });

        AddListener(EventTriggerType.PointerDown, DisableOrbit, Slider_Angle.gameObject);
        AddListener(EventTriggerType.PointerUp, EnableOrbit, Slider_Angle.gameObject);

        PickCompassBtn.GetComponent<Button>().onClick.AddListener(() => ShowCompass());

        DrawarcBtn.GetComponent<Button>().onClick.AddListener(() => DrawingArc());

        SubmitBtn.GetComponent<Button>().onClick.AddListener(() => SubmitFun());

        ResetBtn.GetComponent<Button>().onClick.AddListener(() => ResetFun());
    }

    public void AssignInitialValues()
    {
        if (OptionUnlockedTill > 0)
        {
            LocalXP.transform.localScale = new Vector3(0, 0, 0);
            ObjectivePanel.transform.localScale = new Vector3(0, 0, 0);
        }


        InfoCloseBtn.gameObject.transform.localScale = new Vector3(0, 0, 0);
        localGainedXpValueText.text = "0";
        Main_XP_ToShow.text = MainXpValue + "";
        WronOptionPanel.transform.localScale = new Vector3(0, 0, 0);


        StepNum = 1;

        indexingNum = 1;

        QuestionPanel.GetComponentInChildren<Text>().text =  QList[indexingNum];
        
        for (int i = 1; i < Lines.transform.childCount; i++)
        {
            Lines.transform.GetChild(i).gameObject.SetActive(false);
        }

        for (int i = 1; i < SquareRoots.transform.childCount; i++)
        {
           
            SquareRoots.transform.GetChild(i).gameObject.SetActive(false);
            // Arcs.transform.GetChild(i).gameObject.SetActive(true);
            GameObject obj = SquareRoots.transform.GetChild(i).gameObject;

            obj.SetActive(false);

            obj.transform.GetChild(0).gameObject.SetActive(false);
            obj.transform.GetChild(1).gameObject.SetActive(false);
            obj.transform.GetChild(2).gameObject.SetActive(false);
            obj.transform.GetChild(3).gameObject.SetActive(false);
            obj.transform.GetChild(0).gameObject.GetComponent<LineRenderer>().enabled = false;
            obj.transform.GetChild(5).gameObject.GetComponent<LineRenderer>().enabled = false;
            obj.transform.GetChild(5).gameObject.SetActive(false);
        }

        plane_geo_01281.transform.GetChild(1).gameObject.GetComponent<TrailRenderer>().enabled = false;

        for (int i= 1; i < indexingNum; i++){
            Lines.transform.GetChild(i).gameObject.SetActive(true);
            SquareRoots.transform.GetChild(i).gameObject.SetActive(true);
           // Arcs.transform.GetChild(i).gameObject.SetActive(true);
            GameObject obj = SquareRoots.transform.GetChild(i).gameObject;

            obj.SetActive(true);

            obj.transform.GetChild(0).gameObject.SetActive(true);
            obj.transform.GetChild(1).gameObject.SetActive(true);
            obj.transform.GetChild(2).gameObject.SetActive(true);
            obj.transform.GetChild(3).gameObject.SetActive(true);
            obj.transform.GetChild(5).gameObject.GetComponent<LineRenderer>().enabled = true;
            obj.transform.GetChild(5).gameObject.SetActive(true);
            lineRenderer = obj.transform.GetChild(5).gameObject.GetComponent<LineRenderer>();
            obj.transform.GetChild(5).gameObject.transform.localPosition = obj.transform.GetChild(0).gameObject.transform.localPosition;
            DrawQuadraticBezierCurve(obj.transform.GetChild(5).gameObject.transform.localPosition, obj.transform.GetChild(4).gameObject.transform.localPosition, obj.transform.GetChild(3).gameObject.transform.localPosition);
          
            obj.transform.GetChild(5).gameObject.transform.localPosition = new Vector3(0, 0, 0);
        }

        protractorPlace = new float[4];


        for (int i = 0; i < 4; i++) {
            GameObject obj = SquareRoots.transform.GetChild(i).gameObject;
            protractorPlace[i] = obj.transform.GetChild(3).gameObject.transform.localPosition.x;
        }

        proractorObj.transform.localPosition = new Vector3(protractorPlace[indexingNum-1], proractorObj.transform.localPosition.y, proractorObj.transform.localPosition.z);

        lineRenderer = SquareRoots.transform.GetChild(0).transform.GetChild(6).gameObject.GetComponent<LineRenderer>();

        DrawQuadraticBezierCurve(SquareRoots.transform.GetChild(0).transform.GetChild(6).gameObject.transform.localPosition, SquareRoots.transform.GetChild(0).transform.GetChild(4).gameObject.transform.localPosition, SquareRoots.transform.GetChild(0).transform.GetChild(3).gameObject.transform.localPosition);
        SquareRoots.transform.GetChild(0).transform.GetChild(6).gameObject.transform.localPosition = new Vector3(0, 0, 0);
    }

    public void TaskListCreation()
    {

        rT = AllTaskListObj.transform.parent.GetComponent<RectTransform>();

        localGaniedXpValue = 0;

        for (int i = 0; i < TaskList.Count; i++)
        {
            GameObject Tl = (GameObject)Instantiate(TaskObj);
            Tl.transform.SetParent(AllTaskListObj.transform);
            Tl.transform.localScale = new Vector3(1, 1, 1);
            Tl.name = i.ToString();

            AllTaskData.Add(Tl);
            AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(false);
            AllTaskData[i].gameObject.transform.GetChild(2).gameObject.SetActive(false);
            AllTaskData[i].gameObject.transform.GetChild(3).GetComponentInChildren<Text>().text = TaskList[i];
            AllTaskData[i].gameObject.transform.GetChild(4).GetComponentInChildren<Text>().text = "+" + XpList[i] + " XP";

            localTotalXpValue += int.Parse(XpList[i]);
        }

        rT.sizeDelta = new Vector2(rT.sizeDelta.x, TaskList.Count * 50);
        localTotalXpValueText.text = "/" + localTotalXpValue.ToString();

        AllTaskData[0].gameObject.transform.GetChild(2).gameObject.SetActive(true);
        ObjectiveInfoText.text = AllTaskData[0].gameObject.transform.GetChild(3).GetComponentInChildren<Text>().text;
    }

    public void LocalXpCalculation()
    {
        for (int i = 0; i < TaskList.Count; i++)
        {
            AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(false);
            AllTaskData[i].gameObject.transform.GetChild(2).gameObject.SetActive(false);
        }

        if (OptionUnlockedTill < 1)
        {
            if ((TopicToCheck) < (TaskList.Count + 1))
            {
                localGaniedXpValue = 0;

                for (int i = 0; i < TopicToCheck - 1; i++)
                {
                    localGaniedXpValue += int.Parse(XpList[i]);
                }

                for (int i = 0; i < TopicToCheck; i++)
                {
                    AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(true);
                }

                localGainedXpValueText.text = localGaniedXpValue.ToString();

                AllTaskData[TopicToCheck - 1].gameObject.transform.GetChild(2).gameObject.SetActive(true);

                ObjectiveInfoText.text = AllTaskData[TopicToCheck - 1].gameObject.transform.GetChild(3).GetComponentInChildren<Text>().text;
            }
            else
            {
                for (int i = 0; i < TaskList.Count; i++)
                {
                    AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(true);
                    AllTaskData[i].gameObject.transform.GetChild(2).gameObject.SetActive(false);
                }

                localGainedXpValueText.text = localTotalXpValue.ToString();

                XP_Effect.text = "+" + localTotalXpValue + " XP";

                XP_Effect.transform.position = (XP_ToShow.transform.position);

                iTween.Stop(XP_Effect.gameObject);

                iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeOutElastic));

                iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", XP_Effect.transform.position.x, "y", XP_Effect.transform.position.y + 50, "z", 0, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

                iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", Main_XP_ToShow.transform.position.x, "y", Main_XP_ToShow.transform.position.y, "z", 0, "delay", 1.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

                iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "delay", 2f, "time", 0.5f, "easetype", iTween.EaseType.linear, "oncomplete", "MainXpCalculation", "oncompletetarget", this.gameObject));
            }
        }
        else
        {
            for (int i = 0; i < TaskList.Count; i++)
            {
                AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(true);
                AllTaskData[i].gameObject.transform.GetChild(2).gameObject.SetActive(false);
            }

            localGainedXpValueText.text = localTotalXpValue.ToString();
        }
    }

    public void CloseAllpanels()
    {

        ControlPanel.GetComponent<Animator>().Play("CpanelFullClose");
        LocalXP.GetComponent<Animator>().Play("LocalXpPanelClose");
        Main_Xp.GetComponent<Animator>().Play("MainXpPanelClose");
        ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelClose");
        InfoButton.GetComponent<Animator>().Play("InfoButtonClose");
        MainSlider.GetComponent<Animator>().Play("MainSliderClose");
    
        if (TasklistPanel.gameObject.transform.localScale.y != 0)
            iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

        iTween.ScaleTo(QuestionPanel.gameObject, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));
    }

    public void OpenAllpanels()
    {
        if (OptionUnlockedTill < 1)
        {
            if (TopicToCheck <= (TaskList.Count))
            {
                LocalXP.GetComponent<Animator>().Play("LocalXpPanelOPen");
                ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen");
            }
        }

        ControlPanel.GetComponent<Animator>().Play("CpanelOpen");

        Main_Xp.GetComponent<Animator>().Play("MainXpPanelOpen");

        InfoButton.GetComponent<Animator>().Play("InfoButtonOpen");
        MainSlider.GetComponent<Animator>().Play("MainSliderOpen");
        iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "time", 0.2f, "easetype", iTween.EaseType.linear));
        iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));


        QuestionPanel.transform.localScale = new Vector3(0, 0, 0);
        iTween.ScaleTo(QuestionPanel.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));
    }

    public void AddXpEffect()
    {

        if (TopicToCheck <= (TaskList.Count))
        {

            GameObject.Find("Canvas").GetComponent<CanvasGroup>().interactable = false;

            XP_Effect.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);

            XP_Effect.text = "+" + int.Parse(XpList[TopicToCheck - 1]) + " XP";

            XP_Effect.transform.localScale = new Vector3(1, 0, 1);

            iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeOutElastic));

            iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", XP_Effect.transform.position.x, "y", XP_Effect.transform.position.y + 50, "z", 0, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

            iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", XP_ToShow.transform.position.x, "y", XP_ToShow.transform.position.y, "z", 0, "delay", 1.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

            iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "delay", 2f, "time", 0.5f, "easetype", iTween.EaseType.linear, "oncomplete", "LocalXpCalculation", "oncompletetarget", this.gameObject));

            TopicToCheck++;

            ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelClose");

            Invoke("EnableRayCast", 3);


        }

    }

    public void EnableRayCast()
    {
        //Camera.main.GetComponent<PhysicsRaycaster>().enabled = true;
        GameObject.Find("Canvas").GetComponent<CanvasGroup>().interactable = true;
        if (TopicToCheck <= (TaskList.Count))
        {
            ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen");
        }
    }

    public void MainXpCalculation()
    {
        if (OptionUnlockedTill == 0)
        {
            OptionUnlockedTill = 1;

            MainXpValue += localTotalXpValue;

            Main_XP_ToShow.text = MainXpValue.ToString();

            LocalXP.GetComponent<Animator>().Play("LocalXpPanelClose");

            ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelClose");

            if ((ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelClose")) || (ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelFullClose")))
            {
                ControlPanel.GetComponent<Animator>().Play("CpanelOpen");
            }
        }
    }

    public void CloseInfoPanel()
    {
        sessionStart = true;
        InfoPanelBg.SetActive(false);
        iTween.ScaleTo(InformationPanel.gameObject, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));
        OpenAllpanels();
    }

    public void ContinueInfoPanel()
    {
        sessionStart = true;
        InfoPanelBg.SetActive(false);
        iTween.ScaleTo(InformationPanel.gameObject, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));
        InfoCloseBtn.transform.localScale = new Vector3(0.27f, 0.27f, 0.27f);
        InfoContinueBtn.transform.localScale = new Vector3(0, 0, 0);
        OpenAllpanels();
    }

    public void OpenInfoPanel()
    {
        sessionStart = false;
        iTween.ScaleTo(InformationPanel.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));
        InfoPanelBg.SetActive(true);
        iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "time", 0.2f, "easetype", iTween.EaseType.linear));
        iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));
        CloseAllpanels();
    }

    public void OpenTaskList()
    {
        if (TasklistPanel.gameObject.transform.localScale.y != 1)
        {
            iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.01f, "time", 0.2f, "easetype", iTween.EaseType.easeInSine));

            ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelClose");

            iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 180, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));

            if ((ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelOpen")))
            {
                ControlPanel.GetComponent<Animator>().Play("CpanelClose");
            }
        }
        else
        {
            iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "delay", 0.01f, "time", 0.2f, "easetype", iTween.EaseType.easeInSine));
            iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));

            ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen");

            ControlPanel.GetComponent<Animator>().Play("CpanelOpen");
        }
    }

    public void OpenOrCloseControlPanel()
    {

        if ((ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelClose")) || (ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelFullClose")))
        {
            ControlPanel.GetComponent<Animator>().Play("CpanelOpen");
        }
        else
        {
            ControlPanel.GetComponent<Animator>().Play("CpanelClose");
        }
    }

    public void WrongOptionSelected()
    {
        WronOptionPanel.transform.localScale = new Vector3(0, 0, 0);

        iTween.Stop(WronOptionPanel.gameObject);

        iTween.ScaleTo(WronOptionPanel.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.01f, "time", 0.2f, "easetype", iTween.EaseType.easeInSine));

        iTween.ScaleTo(WronOptionPanel.gameObject, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 1f, "time", 0.2f, "easetype", iTween.EaseType.easeInSine));

        // InfoPanelBg.SetActive(true);

        Invoke("CloseBlurBg", 1f);

        GameObject.Find("Canvas").GetComponent<CanvasGroup>().interactable = false;

        ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelClose");


        var mouse = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);

        if (mouse.x < Screen.width / 2)
        {
            // WronOptionPanel
            //print("Mouse is on left side of screen.");
            WronOptionPanel.GetComponent<RectTransform>().pivot = new Vector2(0, 0);
        }

        if (mouse.x > Screen.width / 2)
        {
            //print("Mouse is on right side of screen.");
            WronOptionPanel.GetComponent<RectTransform>().pivot = new Vector2(1, 0);
        }


        WronOptionPanel.transform.position = Input.mousePosition;
    }

    public void CloseBlurBg()
    {
        InfoPanelBg.SetActive(false);
        GameObject.Find("Canvas").GetComponent<CanvasGroup>().interactable = true;

        if (OptionUnlockedTill < 1)
        {
            if (InformationPanel.gameObject.transform.localScale.x == 0f)
            {
                //ObjectivePanel.GetComponent<Animator>().Play("");
                ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen", 0, 0f);
            }
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (InformationPanel.gameObject.transform.localScale.x != 1 && sessionStart)
        {
            if (OptionUnlockedTill < 1)
            {
                if (TopicToCheck <= (TaskList.Count))
                {
                    iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "time", 0.2f, "easetype", iTween.EaseType.linear));
                    iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));
                    ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen");
                    ControlPanel.GetComponent<Animator>().Play("CpanelOpen");
                }
            }
        }
    }

    public void LabelsAssignment()
    {
        GameObject Label = GameObject.Find("Labels");

        Labels = new GameObject[Label.transform.childCount];

        for (int j = 0; j < Labels.Length; j++)
        {
            Labels[j] = Label.transform.GetChild(j).gameObject;

            GameObject TempLabels = Labels[j];

            foreach (Transform ChildLabel in TempLabels.transform)
            {
                if (ChildLabel.name.Contains("SmoothLook"))
                {
                    ChildLabel.gameObject.AddComponent<SmoothLook>();
                    //ChildLabel.gameObject.AddComponent<ScaleRelativeToCamera>();
                }
            }
        }

        for (int k = 0; k < Labels.Length; k++)
        {
            string name = Labels[k].name.Remove(Labels[k].name.Length - 6);
           // Labels[k].SetActive(false);
            if (GameObject.Find(name + "") != null)
            {
                GameObject dummy = GameObject.Find(name + "").gameObject;

                Labels[k].transform.parent = dummy.transform;
                Labels[k].transform.rotation = Quaternion.identity;
            }
        }
    }

    public void ToggleActivateLabels()
    {
        if (!Lab)
        {
            Lab = true;
            for (int i = 0; i < Labels.Length; i++)
            {
                Labels[i].SetActive(false);
            }          
        }
        else
        {
            Lab = false;
            for (int i = 0; i < Labels.Length; i++)
            {
                Labels[i].SetActive(true);
                
            }
     
        }
    }

    public void AddListener(EventTriggerType eventType, Action MethodToCall, GameObject TriggerObjToAdd)
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall());
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }

    public void AddListener(EventTriggerType eventType, Action<float> MethodToCall, GameObject TriggerObjToAdd)
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall(TriggerObjToAdd.GetComponent<Slider>().value));
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }

    public void EnableOrbit()
    {
        if (!IsAR)
        {
            CamOrbit.DisableInteration = false;
        }
        else
        {
            ArOrbit.DisableInteration = false;
        }
        if (OptionUnlockedTill < 1)
        {
            if (InformationPanel.gameObject.transform.localScale.x != 1)
            {
                if (TopicToCheck <= (TaskList.Count))
                {
                    iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "time", 0.2f, "easetype", iTween.EaseType.linear));
                    iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));
                    ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen");
                }
            }
        }
        if (StepNum == 3)
        {

        }
        else {
            Slider_Angle.value = 0;
            WrongOptionSelected();
            WronOptionPanel.GetComponentInChildren<Text>().text = "Wrong parameter chosen";
            WronOptionPanel.GetComponentInChildren<Text>().color = new Color(0.9921f, 0.4431f, 0.08621f, 1);
        }
    }
    
    public void DisableOrbit()
    {
        if (!IsAR)
        {
            CamOrbit.DisableInteration = true;
        }
        else
        {
            ArOrbit.DisableInteration = true;
        }

    }

    public Vector3 MidPoint(GameObject obj1, GameObject obj2)
    {
        Vector3 midp = (obj1.transform.localPosition + obj2.transform.localPosition) / 2;
        return midp;
    }

    public float Scale(GameObject obj1, GameObject obj2)
    {
        float scl = Vector3.Distance(obj1.transform.localPosition, obj2.transform.localPosition);
        return scl;
    }

    public void ChangeBallMass(int IncrVal)
    {
        IncrVal += 1;
        IncrVal = Mathf.Clamp(IncrVal, 1, 3);
    }

    public void ChangeAngle(float val, Text txt)
    {
        txt.text = (val).ToString("F1") + " "; 
        plane_geo_01281.transform.localEulerAngles = new Vector3(0, -val, 0);
    }

    public void ShowProtractor() {
        if (StepNum == 1)
        {
            if (!proractorObj.activeSelf)
            {
                if (OptionUnlockedTill < 1)
                {
                    if (TopicToCheck == 1)
                    {
                        AddXpEffect();
                        PShow();
                    }
                    else {
                        WrongOptionSelected();
                        WronOptionPanel.GetComponentInChildren<Text>().text = "Wrong parameter chosen";
                        WronOptionPanel.GetComponentInChildren<Text>().color = new Color(0.9921f, 0.4431f, 0.08621f, 1);
                    }
                }
                else {
                    PShow();
                }
            }
            else
            {
                proractorObj.SetActive(false);
            }
        }
        else
        {
            WrongOptionSelected();
            WronOptionPanel.GetComponentInChildren<Text>().text = "Wrong parameter chosen";
            WronOptionPanel.GetComponentInChildren<Text>().color = new Color(0.9921f, 0.4431f, 0.08621f, 1);
        }
    }


    public void PShow() {

        ResetObjs();
        StepNum++;
        proractorObj.SetActive(true);
        iTween.ScaleTo(proractorObj, iTween.Hash("x", 2.6f, "y", 2.6f, "z", 2.6f, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine, "oncomplete", "UpperDisplayPoint", "oncompletetarget", this.gameObject));

    }

    public void ShowCompass()
    {
        if (StepNum == 2)
        {
            if (!CompassObject.activeSelf)
            {
                if (OptionUnlockedTill < 1)
                {
                    if (TopicToCheck == 2)
                    {
                        AddXpEffect();
                        CShow();
                    }
                    else
                    {
                        WrongOptionSelected();
                        WronOptionPanel.GetComponentInChildren<Text>().text = "Wrong parameter chosen";
                        WronOptionPanel.GetComponentInChildren<Text>().color = new Color(0.9921f, 0.4431f, 0.08621f, 1);

                    }
                }
                else {
                    CShow();
                }
            }
            else
            {
                CompassObject.SetActive(false);
            }
        }
        else {
            WrongOptionSelected();
            WronOptionPanel.GetComponentInChildren<Text>().text = "Wrong parameter chosen";
            WronOptionPanel.GetComponentInChildren<Text>().color = new Color(0.9921f, 0.4431f, 0.08621f, 1);
        }
    }

    public void CShow() {

        ResetObjs();
        CompassObject.transform.localEulerAngles = new Vector3(0, InnerprotractorAnglesY[indexingNum - 1], InnerprotractorAnglesZ[indexingNum - 1]);
        StepNum++;
        CompassObject.SetActive(true);
    }

    public void ResetObjs() {
        CompassObject.SetActive(false);
        proractorObj.SetActive(false);
    }

    public void DrawingArc() {

        if (StepNum == 3)
        {
            if (OptionUnlockedTill < 1)
            {
                if (TopicToCheck == 3)
                {                   

                    if ((-Slider_Angle.value) >= MainprotractorAngles2[indexingNum - 1] - 1f && (-Slider_Angle.value) <= MainprotractorAngles2[indexingNum - 1] + 1f)
                    {
                        plane_geo_01281.transform.GetChild(1).gameObject.GetComponent<TrailRenderer>().enabled = true;
                        iTween.RotateTo(CompassObject, iTween.Hash("x", 0, "y", 1, "z", CompassObject.transform.localEulerAngles.z, "delay", 0.01f, "time", 0.5f, "easetype","islocal",false, iTween.EaseType.easeInSine, "oncomplete", "AfterAarc", "oncompletetarget", this.gameObject));

                        WrongOptionSelected();
                        WronOptionPanel.GetComponentInChildren<Text>().text = "Well Done!";
                        WronOptionPanel.GetComponentInChildren<Text>().color = new Color(0.7568f, 0.8588f, 0.075f, 1);
                    }
                    else
                    {
                        WrongOptionSelected();
                        WronOptionPanel.GetComponentInChildren<Text>().text = "Wrong angle selected";
                        WronOptionPanel.GetComponentInChildren<Text>().color = new Color(0.9921f, 0.4431f, 0.08621f, 1);
                    }
                }
                else
                {
                    WrongOptionSelected();
                    WronOptionPanel.GetComponentInChildren<Text>().text = "Wrong parameter chosen";
                    WronOptionPanel.GetComponentInChildren<Text>().color = new Color(0.9921f, 0.4431f, 0.08621f, 1);
                }
            }
            else {
                if ((-Slider_Angle.value) >= MainprotractorAngles2[indexingNum - 1] - 1f && (-Slider_Angle.value) <= MainprotractorAngles2[indexingNum - 1] + 1f)
                {
                    plane_geo_01281.transform.GetChild(1).gameObject.GetComponent<TrailRenderer>().enabled = true;
                    iTween.RotateTo(CompassObject, iTween.Hash("x", 0, "y", 1, "z", CompassObject.transform.localEulerAngles.z, "delay", 0.01f, "time", 0.5f, "easetype","islocal",false, iTween.EaseType.easeInSine, "oncomplete", "AfterAarc", "oncompletetarget", this.gameObject));
                }
                else
                {
                    WrongOptionSelected();
                    WronOptionPanel.GetComponentInChildren<Text>().text = "Wrong angle selected";
                    WronOptionPanel.GetComponentInChildren<Text>().color = new Color(0.9921f, 0.4431f, 0.08621f, 1);
                }
            }
        }
        else
        {
            WrongOptionSelected();
            WronOptionPanel.GetComponentInChildren<Text>().text = "Wrong  parameter chosen";
            WronOptionPanel.GetComponentInChildren<Text>().color = new Color(0.9921f, 0.4431f, 0.08621f, 1);
        }
    }

    public void AfterAarc()
    {

        CompassObject.transform.localEulerAngles= new Vector3(0, 0, CompassObject.transform.localEulerAngles.z);
        plane_geo_01281.transform.GetChild(1).gameObject.GetComponent<TrailRenderer>().enabled = false;
        GameObject obj = SquareRoots.transform.GetChild(indexingNum).gameObject;
        obj.transform.GetChild(5).gameObject.GetComponent<LineRenderer>().enabled = true;
        lineRenderer = obj.transform.GetChild(5).gameObject.GetComponent<LineRenderer>();

        //obj.transform.GetChild(5).gameObject.transform.localPosition = obj.transform.GetChild(0).gameObject.transform.localPosition;
        DrawQuadraticBezierCurve(obj.transform.GetChild(5).gameObject.transform.localPosition, obj.transform.GetChild(4).gameObject.transform.localPosition, obj.transform.GetChild(3).gameObject.transform.localPosition);
        
        obj.transform.GetChild(3).gameObject.SetActive(true);

        obj.transform.GetChild(5).gameObject.SetActive(true);

        Lines.transform.GetChild(indexingNum).gameObject.SetActive(true);

        obj.transform.GetChild(5).gameObject.transform.localPosition = new Vector3(0, 0, 0);
        Invoke("FinalFun",2);
        if (OptionUnlockedTill < 1)
        {
            AddXpEffect();
        }
    }

    public void FinalFun() {
        if (indexingNum < 4)
        {
            indexingNum++;
        }
        else
        {
            indexingNum = 1;
        }
        QuestionPanel.GetComponentInChildren<Text>().text = QList[indexingNum];
        
        
       
        ResetFun();
    }
    public void SubmitFun() {

        //if (Slider_Angle.value <= MainprotractorAngles2[indexingNum-1]-1f && Slider_Angle.value >= MainprotractorAngles2[indexingNum-1] + 1f)
        //{
        //    GameObject obj = SquareRoots.transform.GetChild(indexingNum).gameObject;
        //    obj.transform.GetChild(3).gameObject.SetActive(true);
        //    Lines.transform.GetChild(indexingNum).gameObject.SetActive(true);
        //}
        //else {
        //    WrongOptionSelected();
        //    WronOptionPanel.GetComponentInChildren<Text>().text = "Wrong angle selected";
        //}
      
        //Arcs.transform.GetChild(indexingNum).gameObject.SetActive(true);
        //}
    }

    public void UpperDisplayPoint()
    {
        
        GameObject obj = SquareRoots.transform.GetChild(indexingNum).gameObject;

        obj.SetActive(true);

        obj.transform.GetChild(0).gameObject.SetActive(true);
        obj.transform.GetChild(1).gameObject.SetActive(true);
        obj.transform.GetChild(2).gameObject.SetActive(true);

        float p = obj.transform.GetChild(1).gameObject.transform.localScale.z;
        float p2 = obj.transform.GetChild(2).gameObject.transform.localScale.z;

        obj.transform.GetChild(1).gameObject.transform.localScale = new Vector3(0, 0, 0);
        obj.transform.GetChild(2).gameObject.transform.localScale = new Vector3(0, 0, 0);

        iTween.ScaleTo(obj.transform.GetChild(1).gameObject, iTween.Hash("x", 1, "y", 1, "z", p, "delay", 0.01f, "time", 1f, "easetype", iTween.EaseType.easeInSine));
        iTween.ScaleTo(obj.transform.GetChild(2).gameObject, iTween.Hash("x", 1, "y", 1, "z", p2, "delay", 0.01f, "time", 1f, "easetype", iTween.EaseType.easeInSine));

     }

    void DrawQuadraticBezierCurve(Vector3 point0, Vector3 point1, Vector3 point2)
    {            
        lineRenderer.positionCount = 20;
        float t = 0f;
        Vector3 B = new Vector3(0, 0, 0);

        for (int i = 0; i < lineRenderer.positionCount; i++)
        {
            B = (1 - t) * (1 - t) * point0 + 2 * (1 - t) * t * point1 + t * t * point2;
            lineRenderer.SetPosition(i, B);
            t += (1 / (float)lineRenderer.positionCount);
        }
    }


    public void ResetFun() {

        ResetObjs();
        QuestionPanel.transform.localScale = new Vector3(0, 0, 0);
        iTween.ScaleTo(QuestionPanel.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));
        //QuestionPanel.GetComponentInChildren<Text>().text = QList[indexingNum];

        //indexingNum = UnityEngine.Random.Range(1,4);
        //if (indexingNum < 4)
        //{
        //    indexingNum++;
        //}
        //else {
        //    indexingNum = 1;
        //}


        StepNum = 1;

        plane_geo_01281.transform.GetChild(1).gameObject.GetComponent<TrailRenderer>().enabled = false;

        Slider_Angle.value = 0;

        for (int i = 1; i < Lines.transform.childCount; i++)
        {
            Lines.transform.GetChild(i).gameObject.SetActive(false);
        }

        for (int j = 1; j < SquareRoots.transform.childCount; j++)
        {

            SquareRoots.transform.GetChild(j).gameObject.SetActive(false);
            // Arcs.transform.GetChild(i).gameObject.SetActive(true);
            GameObject obj = SquareRoots.transform.GetChild(j).gameObject;

            obj.SetActive(false);

            obj.transform.GetChild(0).gameObject.SetActive(false);
            obj.transform.GetChild(1).gameObject.SetActive(false);
            obj.transform.GetChild(2).gameObject.SetActive(false);
            obj.transform.GetChild(3).gameObject.SetActive(false);
            obj.transform.GetChild(5).gameObject.transform.localPosition = obj.transform.GetChild(0).gameObject.transform.localPosition;
            obj.transform.GetChild(0).gameObject.GetComponent<LineRenderer>().enabled = false;
            obj.transform.GetChild(5).gameObject.GetComponent<LineRenderer>().enabled = false;
            obj.transform.GetChild(5).gameObject.SetActive(false);
        }

        for (int k = 1; k < indexingNum; k++)
        {
            Lines.transform.GetChild(k).gameObject.SetActive(true);
            SquareRoots.transform.GetChild(k).gameObject.SetActive(true);
            // Arcs.transform.GetChild(i).gameObject.SetActive(true);
            GameObject obj = SquareRoots.transform.GetChild(k).gameObject;

            obj.SetActive(true);

            obj.transform.GetChild(0).gameObject.SetActive(true);
            obj.transform.GetChild(1).gameObject.SetActive(true);
            obj.transform.GetChild(2).gameObject.SetActive(true);
            obj.transform.GetChild(3).gameObject.SetActive(true);
            obj.transform.GetChild(5).gameObject.GetComponent<LineRenderer>().enabled = true;

            obj.transform.GetChild(5).gameObject.SetActive(true);
            lineRenderer = obj.transform.GetChild(5).gameObject.GetComponent<LineRenderer>();
            //obj.transform.GetChild(5).gameObject.transform.localPosition = new Vector3(0, 0, 0.1f);
           
            DrawQuadraticBezierCurve(obj.transform.GetChild(5).gameObject.transform.localPosition, obj.transform.GetChild(4).gameObject.transform.localPosition, obj.transform.GetChild(3).gameObject.transform.localPosition);
            obj.transform.GetChild(5).gameObject.transform.localPosition = new Vector3(0, 0, 0);
        }
        proractorObj.transform.localPosition = new Vector3(protractorPlace[indexingNum - 1], proractorObj.transform.localPosition.y, proractorObj.transform.localPosition.z);
    }
}

public class vidOhmsLawLinesRendererOffset : MonoBehaviour
{
    Renderer rend;
    public float offset, scrollSpeed = 4f, dir = 1;
    void Start()
    {
        //dir = 1;
        rend = GetComponentInChildren<Renderer>();
        offset = 1f;
        rend.materials[0].SetTextureOffset("_MainTex", new Vector2(0, 1f));
    }

    void Update()
    {
        offset += Time.deltaTime * scrollSpeed;
        rend.materials[0].SetTextureOffset("_MainTex", new Vector2(0, -dir * offset));
        //rend.material.SetTextureScale("_MainTex", new Vector2(1, 500 * transform.localScale.x));
    }
}

public class DrawRadar : MonoBehaviour
{
    public float ThetaScale = 0.01f;
    public float radius = 0.1f;
    private int Size;
    private LineRenderer LineDrawer;
    private float Theta = 0f;

    void Start()
    {
        LineDrawer = GetComponent<LineRenderer>();
    }

    void Update()
    {
        Theta = 0f;

        Size = (int)((1f / ThetaScale) + 1f);

        LineDrawer.SetVertexCount(Size);

        for (int i = 0; i < Size; i++)
        {
            Theta += (2.0f * Mathf.PI * ThetaScale);
            float x = radius * Mathf.Cos(Theta);
            float y = radius * Mathf.Sin(Theta);
            LineDrawer.SetPosition(i, new Vector3(x, 0, y));
        }
    }
}

