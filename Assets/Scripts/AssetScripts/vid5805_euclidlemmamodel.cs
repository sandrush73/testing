﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;
using System.Linq;
using System;
using System.Text.RegularExpressions;
using UnityEngine.SceneManagement;

public class vid5805_euclidlemmamodel : MonoBehaviour
{
    public AssetBundle assetBundle;
    public Material TransparentMaterial;
    public TextAsset CSVFile;
    public AROrbitControls ArOrbit;
    public float a_Value, b_Value, NumOfZekToCreate, RemZekToCreate,Quotient_val,Reminder_val,Dividend_change, Divisor_change, RowToCreate;
    public GameObject Zekki,Start_Pos,End_Pos,Line_X,Line_Y, WidthLine_X, WidthLine_Y, AlgorithmObj,HCFObj,Algorithm_UI, HCF_UI,phy_table;
    public List<GameObject> Zekkis,GridLines_X,GridLines_Y,DivisionSteps;
    public Button Condition_Yes_Btn, Condition_No_Btn,Verify_Btn, Cond_Yes_Btn_HCF, Cond_No_Btn_HCF, Verify_Btn_HCF,a_incr_Btn_HCF,
        a_decr_Btn_HCF, b_incr_Btn_HCF, b_decr_Btn_HCF,Validate_Btn_HCF,Calculate_Btn_HCF;
    public Text Condition_Text, Zekki_Count_Text,a_val_text,b_val_text, q_val_text, r_val_text,FinalCond_text,First_Equ_text;

    public Text Cond_Text_HCF,  a_val_text_HCF, b_val_text_HCF,GivenIntegers_Txt_HCF, Updateda_val_text_HCF, Updatedb_val_text_HCF;
    public TextMeshPro FinalTxt_HCF;

    public float a_Value_HCF, b_Value_HCF, Quotient_Val_HCF, Reminder_Val_HCF, Divisor_Val_HCF, Dividend_Val_HCF;
    public int DiviStepCount;

    public orbit CamOrbit;
    
    private Color old_color;

    public Text a_Val_Text, b_Val_Text;
    public Button Equation1, Equation2,Reset_Algo,Reset_HCF;


    public bool IsAR, IsUIZooming,InEquation1,Verify_Click;
    public int OptionUnlockedTill, CurrentTopicSelected, TopicToCheck;
    public float Sub_XP_Earned, Total_XP;
    public Dropdown ToDoList_DD;
    public Slider ValueDisplaySlider;
    public GameObject InstructionPanel_DD, ObjectivePanel, InstructionPanel, InfoPanel, GlassPanel, Canvas_BG, SidePannel;
    public Button Info_Close_Btn, Info_Cntue_Btn, Info_Open_Btn, SidePannelOpen, Reset_Btn;
    public List<Dictionary<string, object>> CSV_data;
    public List<String> Topics, Sub_Topics, Sub_Top_ToDoList, Sub_Top_Desc, Sub_Top_XP, LearningGoals;
    public Text XP_ToShow, XP_Effect, Main_XP_ToShow, Learninggoal_Txt, Instruction_Txt, Assumption_Txt, Heading_Txt;
    public bool IsTopicUnlocked;
    public Canvas UIcanvas;

    // Start is called before the first frame update
    void Start()
    {
        //OptionUnlockedTill = 1;

        UIcanvas = GameObject.Find("Canvas_Euclid").GetComponent<Canvas>();

        LoadFromAsssetBundle();



        ReadingDataFromCSVFile();

        ReadingXps();

        FindGameObjects();

        AssignEventsToObjects();

        LoadEquation(true);

        

    }

    public void LoadFromAsssetBundle()
    {
        //if (Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.Android)
        //{
        assetBundle = AssetBundle.LoadFromFile(PlayerPrefs.GetString("AssetBundleUrl"));

        CSVFile = assetBundle.LoadAsset(PlayerPrefs.GetString("LibraryName") + "CSV", typeof(TextAsset)) as TextAsset;

        TransparentMaterial = assetBundle.LoadAsset("TransparentMaterial", typeof(Material)) as Material;
        TransparentMaterial.shader = Shader.Find("Standard");

        


        //}
        //else
        //{

        //string CSVName = transform.parent.gameObject.name;



        //if (CSVName.Contains("Clone"))
        //{



        //    CSVName = CSVName.Substring(0, CSVName.Length - 7);
        //}



        //print("casvname........." + CSVName);



        //CSVFile = Resources.Load(CSVName + "CSV") as TextAsset;
        //}







        Camera.main.gameObject.AddComponent<PhysicsRaycaster>();



        Camera.main.backgroundColor = Color.black;



        GameObject TargetForCamera = GameObject.Find("Target");



        if (TargetForCamera != null)
        {


            TargetForCamera.AddComponent<orbit>();
            CamOrbit = TargetForCamera.GetComponent<orbit>();
            

            IsAR = false;
        }
        else
        {
            this.transform.parent.gameObject.AddComponent<AROrbitControls>();
            ArOrbit = this.transform.parent.gameObject.GetComponent<AROrbitControls>();
            IsAR = true;
        }



    }

    public void ZoomCamera()
    {

        Camera.main.gameObject.AddComponent<PhysicsRaycaster>();
        UIcanvas.GetComponent<GraphicRaycaster>().enabled = true;
        if (!IsAR)
        {
            CamOrbit.zoomMax = 3f;
            CamOrbit.zoomMin = 0.5f;
            CamOrbit.zoomStart = 1.5f;
            CamOrbit.distance = CamOrbit.zoomStart;
        }
    }

    public void ReadingDataFromCSVFile()
    {
        Topics = new List<string>();
        Sub_Topics = new List<string>();
        Sub_Top_Desc = new List<string>();
        Sub_Top_ToDoList = new List<string>();
        Sub_Top_XP = new List<string>();
        LearningGoals = new List<string>();

        CSV_data = CSVReader.Read(CSVFile);


        for (var i = 0; i < CSV_data.Count; i++)
        {
            //print("Topics " + CSV_data[i]["Learninggoals"] + ".......... " +"ToDo " + CSV_data[i]["Topic_TodoList"]);



            if (CSV_data[i]["Topic_ToDoList"].ToString() != "" && CSV_data[i]["Topic_ToDoList"].ToString() != null)
            {
                Sub_Top_ToDoList.Add(CSV_data[i]["Topic_ToDoList"].ToString());

            }



            //if (CSV_data[i]["Top_XP"].ToString() != "" && CSV_data[i]["Top_XP"].ToString() != null)
            //{
            //    Sub_Top_XP.Add(CSV_data[i]["Top_XP"].ToString());

            //}

            if (CSV_data[i]["Learninggoals"].ToString() != "" && CSV_data[i]["Learninggoals"].ToString() != null)
            {
                LearningGoals.Add(CSV_data[i]["Learninggoals"].ToString());

            }

        }






    }


    public int totXp, loclXp, PerTopic_XP;

    public void ReadingXps()
    {

        //PlayerPrefs.SetInt("TotalXP", 34);
        //PlayerPrefs.SetInt("MaxEarnings", 39);

        totXp = PlayerPrefs.GetInt("TotalXP");
        loclXp = PlayerPrefs.GetInt("MaxEarnings");

        TopicToCheck = 0;

        if (loclXp != 0)
        {
            OptionUnlockedTill = 0;

        }
        else
        {
            OptionUnlockedTill = 1;

        }


       


        int t = 0;
        int p = 0;

        for (int k = 0; k < Sub_Top_ToDoList.Count; k++)
        {

            p = loclXp / Sub_Top_ToDoList.Count;
            Sub_Top_XP.Add(p.ToString());
            t = t + p;
        }

        if (t != loclXp)
        {
            Sub_Top_XP[Sub_Top_ToDoList.Count - 1] = (p + (loclXp - t)).ToString();
        }

        Total_XP = loclXp;
        

    }



    public float result;

    public void GetResult()
    {
        result = ((float)(TopicToCheck - 0) / (float)(Sub_Top_ToDoList.Count)) * 100;
        Camera.main.SendMessage("UnloadActually", result);
    }

    public void FindGameObjects()
    {
        SidePannel = GameObject.Find("SidePanel");
        SidePannelOpen = GameObject.Find("PannelOpenButton").GetComponent<Button>();
        SidePannelOpen.gameObject.SetActive(false);

        Zekki = GameObject.Find("Zekki");
        Zekki.SetActive(false);
        phy_table = GameObject.Find("phy_table");
        if (IsAR)
            phy_table.SetActive(false);

        WidthLine_X = GameObject.Find("X_Width");
        WidthLine_Y = GameObject.Find("Y_Width");
        Start_Pos = GameObject.Find("StartPoint");
        End_Pos = GameObject.Find("EndPoint"); 

        a_Val_Text = GameObject.Find("a_Value").GetComponent<Text>();
        b_Val_Text = GameObject.Find("b_Value").GetComponent<Text>();
        Condition_Text = GameObject.Find("ConditionText").GetComponent<Text>();
        Zekki_Count_Text = GameObject.Find("Zekki_Count_Text").GetComponent<Text>();
        Condition_Yes_Btn = GameObject.Find("Yes_Btn").GetComponent<Button>();
        Condition_No_Btn = GameObject.Find("No_Btn").GetComponent<Button>();
        Verify_Btn = GameObject.Find("Verify_Btn").GetComponent<Button>();

        a_val_text = GameObject.Find("a_Val_Txt").GetComponent<Text>();
        b_val_text = GameObject.Find("b_Val_Txt").GetComponent<Text>();
        q_val_text = GameObject.Find("q_Val_Txt").GetComponent<Text>();
        r_val_text = GameObject.Find("r_Val_Txt").GetComponent<Text>();

        Algorithm_UI = SidePannel.transform.Find("BG/Algorithm").gameObject;
        HCF_UI = SidePannel.transform.Find("BG/HCF").gameObject;


        Cond_Yes_Btn_HCF = GameObject.Find("Yes_Btn_HCF").GetComponent<Button>();
        Cond_No_Btn_HCF = GameObject.Find("No_Btn_HCF").GetComponent<Button>();
        //Verify_Btn_HCF = GameObject.Find("Verify_Btn_HCF").GetComponent<Button>();
        Cond_Text_HCF = GameObject.Find("ConditionText_HCF").GetComponent<Text>();

        a_incr_Btn_HCF = GameObject.Find("a_increment").GetComponent<Button>();
        a_decr_Btn_HCF = GameObject.Find("a_decrement").GetComponent<Button>();
        b_incr_Btn_HCF = GameObject.Find("b_increment").GetComponent<Button>();
        b_decr_Btn_HCF = GameObject.Find("b_decrement").GetComponent<Button>();

        Validate_Btn_HCF = GameObject.Find("Validate_Values").GetComponent<Button>();
        Calculate_Btn_HCF = GameObject.Find("Calculate_Values").GetComponent<Button>();

        a_val_text_HCF = GameObject.Find("a_Value_HCF").GetComponent<Text>();
        b_val_text_HCF = GameObject.Find("b_Value_HCF").GetComponent<Text>();
        Updateda_val_text_HCF = GameObject.Find("Updateda_Value_HCF").GetComponent<Text>();
        Updatedb_val_text_HCF = GameObject.Find("Updatedb_Value_HCF").GetComponent<Text>();
        First_Equ_text = GameObject.Find("First_EDA_Condition").GetComponent<Text>();
        FinalTxt_HCF = GameObject.Find("Final_ToDisplay_HCF").GetComponent<TextMeshPro>();

        GivenIntegers_Txt_HCF = GameObject.Find("GivenNumbers_Txt").GetComponent<Text>();

        FinalCond_text = GameObject.Find("Final_Cond_Txt").GetComponent<Text>();
        Line_X =GameObject.Find("Line_X");
        Line_Y = GameObject.Find("Line_Y");
        AlgorithmObj = GameObject.Find("EuclidAlgorithm");
        HCFObj = GameObject.Find("HCFMethod");

        Equation1 = GameObject.Find("Equation1_Button").GetComponent<Button>();
        Equation2 = GameObject.Find("Equation2_Button").GetComponent<Button>();
        Reset_Algo = GameObject.Find("Reset_Btn_Algo").GetComponent<Button>();
        Reset_HCF = GameObject.Find("Reset_Btn_HCF").GetComponent<Button>();

        DivisionSteps = GameObject.FindObjectsOfType(typeof(GameObject)).Select(g => g as GameObject).Where(g => g.name.Contains("DivisionStep_")).ToList();
        DivisionSteps = DivisionSteps.OrderBy(x => x.name).ToList();

        Canvas_BG = GameObject.Find("Canvas_BG");

        if (!IsAR)
        {
            Canvas_BG.transform.parent = Camera.main.transform;
            Canvas_BG.transform.GetComponentInChildren<RawImage>().enabled = true;
            Canvas_BG.GetComponent<Canvas>().worldCamera = Camera.main;
        }

        else
        {
            Canvas_BG.SetActive(false);
        }
        //GameObject content = GameObject.Find("Content");


        
        //ValueDisplaySlider = GameObject.Find("ValueDisplaySlider").GetComponent<Slider>();
        InstructionPanel_DD = GameObject.Find("DD_Instruction");
        InstructionPanel = GameObject.Find("InstructionPanel");
        ToDoList_DD = GameObject.Find("ToDoList_Dropdown").GetComponent<Dropdown>();
        XP_ToShow = GameObject.Find("Gained_XP_Value").GetComponent<Text>();
        XP_Effect = GameObject.Find("Effect_Text").GetComponent<Text>();
        Main_XP_ToShow = GameObject.Find("Main_XP_Value").GetComponent<Text>();
        ObjectivePanel = GameObject.Find("ObjectivePanel");
        InstructionPanel.transform.localScale = Vector3.zero;
        InfoPanel = GameObject.Find("InformationPanel");
        Info_Close_Btn = GameObject.Find("InfoCloseBtn").GetComponent<Button>();
        Info_Cntue_Btn = GameObject.Find("InfoContinueBtn").GetComponent<Button>();
        Info_Open_Btn = GameObject.Find("InfoButton").GetComponent<Button>();
        Info_Close_Btn.gameObject.SetActive(false);
        GlassPanel = GameObject.Find("GlassEffect");
        Instruction_Txt = GameObject.Find("IListText").GetComponent<Text>();
        Assumption_Txt = GameObject.Find("AListText").GetComponent<Text>();
        Learninggoal_Txt = GameObject.Find("LGListText").GetComponent<Text>();
        Heading_Txt = GameObject.Find("InfoHeadingText").GetComponent<Text>();
        Reset_Btn = GameObject.Find("Reset_Exp").GetComponent<Button>();



        if (OptionUnlockedTill == 0)
        {
            /*ToDoList_DD.GetComponent<RectTransform>().anchoredPosition = new Vector3(150f, -150f, 0);
            XP_ToShow.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(150f, -150f, 0);
            SidePannel.GetComponent<RectTransform>().anchoredPosition = new Vector3(-600f, 0f, 0);*/
            ObjectivePanel.transform.localScale = new Vector3(1, 1, 1);
            IsTopicUnlocked = false;
        }
        else
        {
            ToDoList_DD.GetComponent<RectTransform>().anchoredPosition = new Vector3(-250f, -150f, 0);
            XP_ToShow.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(-250f, -150f, 0);
            ObjectivePanel.transform.localScale = new Vector3(1, 0, 1);
            IsTopicUnlocked = true;
        }

        Main_XP_ToShow.text = totXp.ToString();

        XP_ToShow.text = "0/" + Total_XP;


        for (int i = 0; i < Sub_Top_ToDoList.Count; i++)
        {

            ToDoList_DD.options.Add(new Dropdown.OptionData() { text = Sub_Top_ToDoList[i] });


            //List<float> TotalXP = Sub_Top_XP.Select(s => float.Parse(s)).ToList();

            //Total_XP += TotalXP[i];

            //XP_ToShow.text = "0/" + Total_XP;


            ObjectivePanel.transform.Find("ObjectiveText").GetComponent<Text>().text = Sub_Top_ToDoList[TopicToCheck];




        }


        GlassPanel.transform.localScale = Vector3.zero;
        InfoPanel.transform.localScale = Vector3.zero;
        Invoke("OpenInfoPanel", 0.1f);

        Heading_Txt.text = LearningGoals[0];
        Learninggoal_Txt.text = LearningGoals[1];
        //Instruction_Txt.text = LearningGoals[2];
        //Assumption_Txt.text = LearningGoals[3];

        OpenSidePannel();
        

        
    }


    public void AssignEventsToObjects()
    {





        SidePannelOpen.onClick.AddListener(OpenSidePannel);
        //TopSidePannelOpen.onClick.AddListener(OpenTopSidePannel);



        EventTrigger.Entry PointerDown = new EventTrigger.Entry();
        PointerDown.eventID = EventTriggerType.EndDrag;
        PointerDown.callback.AddListener((data) => { DisableOrbit(); });

        //ScrollBarViewPort.gameObject.GetComponent<EventTrigger>().triggers.Add(PointerDown);

        EventTrigger.Entry PointerUp = new EventTrigger.Entry();
        PointerUp.eventID = EventTriggerType.BeginDrag;
        PointerUp.callback.AddListener((data) => { EnableOrbit(); });

        //ScrollBarViewPort.gameObject.GetComponent<EventTrigger>().triggers.Add(PointerUp);

        /*for (int i = 0; i < SelectionButtons.Length; i++)
        {
            int j = i;
            SelectionButtons[i].onClick.AddListener(() => PropertyToDisplay(j));
            // print(SelectionButtons[i].name);
            old_color = SelectionButtons[0].transform.GetChild(1).GetComponent<Text>().color;
        }*/
        ToDoList_DD.GetComponent<EventTrigger>().triggers.Clear();

        AddListenerToEvents(EventTriggerType.PointerClick, ExploringToDOList_DD, ToDoList_DD.gameObject);
        AddListenerToEvents(EventTriggerType.Select, ToDoList_DDCancel, ToDoList_DD.gameObject);
        Info_Close_Btn.onClick.AddListener(() => CloseInfoPanel(Info_Close_Btn));
        Info_Cntue_Btn.onClick.AddListener(() => CloseInfoPanel(Info_Cntue_Btn));
        Info_Open_Btn.onClick.AddListener(OpenInfoPanel);
        Condition_Yes_Btn.onClick.AddListener(() => CheckForCondition(Condition_Yes_Btn));
        Condition_No_Btn.onClick.AddListener(() => CheckForCondition(Condition_No_Btn));

        Equation1.onClick.AddListener(() => LoadEquation(true));
        Equation2.onClick.AddListener(() => LoadEquation(false));

        Cond_Yes_Btn_HCF.onClick.AddListener(() => CheckForConditionHCF(Cond_Yes_Btn_HCF));
        Cond_No_Btn_HCF.onClick.AddListener(() => CheckForConditionHCF(Cond_No_Btn_HCF));
        Verify_Btn.onClick.AddListener(Verify_Btn_Click);
        a_incr_Btn_HCF.onClick.AddListener(()=> Change_a_b_Values(a_incr_Btn_HCF));
        a_decr_Btn_HCF.onClick.AddListener(() => Change_a_b_Values(a_decr_Btn_HCF));
        b_incr_Btn_HCF.onClick.AddListener(() => Change_a_b_Values(b_incr_Btn_HCF));
        b_decr_Btn_HCF.onClick.AddListener(() => Change_a_b_Values(b_decr_Btn_HCF));

        Validate_Btn_HCF.onClick.AddListener(Validate_a_b_values);
        Calculate_Btn_HCF.onClick.AddListener(()=> CheckForConditionHCF(Cond_No_Btn_HCF));

        Reset_Algo.onClick.AddListener(()=>LoadEquation(true));
        Reset_HCF.onClick.AddListener(() => LoadEquation(false));
        /*AddListenerToEvents(EventTriggerType.BeginDrag, OnElementBeginDrag, GameObject.Find("RawImage"));
        AddListenerToEvents(EventTriggerType.EndDrag, OnElementEndDrag, GameObject.Find("RawImage"));
        AddListenerToEvents(EventTriggerType.PointerClick, BackToPT, GameObject.Find("RawImage"));*/

    }

    public void LoadEquation(bool loadEquation1)
    {
        if (!IsTopicUnlocked && !HCFObj.activeInHierarchy)
        {
            if (TopicToCheck == 3 && !loadEquation1)
            {
                Collect_XP(HCFObj);
            }
            else
            {
                ShowInstructionPanel("Wrong option selected");
                return;
            }
        }

        for (int i = 0; i < GridLines_X.Count; i++)
        {
            Destroy(GridLines_X[i]);
        }
        for (int i = 0; i < GridLines_Y.Count; i++)
        {
            Destroy(GridLines_Y[i]);
        }
        for (int i = 0; i < Zekkis.Count; i++)
        {
            Destroy(Zekkis[i]);
        }
        GridLines_X.Clear();
        GridLines_Y.Clear();
        Zekkis.Clear();
        Verify_Click = false;
        Reset_Algo.gameObject.SetActive(false);
        Reset_HCF.gameObject.SetActive(false);


        if (loadEquation1)
        {
           
            AlgorithmObj.SetActive(true);
            HCFObj.SetActive(false);
            Algorithm_UI.SetActive(true);
            HCF_UI.SetActive(false);
            Line_X.SetActive(true);
            Line_Y.SetActive(true);
            RowToCreate = 0;
            a_val_text.text = "";
            b_val_text.text = "";
            q_val_text.text = "";
            r_val_text.text = "";
            FinalCond_text.text = "";
            FinalTxt_HCF.text = "";
            Equation2.GetComponent<Image>().sprite = Equation2.spriteState.disabledSprite;
            Equation1.GetComponent<Image>().sprite = Equation1.spriteState.pressedSprite;

            CreateZekki();

            if (!IsAR)
            {
                CamOrbit.zoomMax = 1.5f;
                CamOrbit.zoomMin = 0.75f;
                CamOrbit.zoomStart = 1f;
                CamOrbit.distance = CamOrbit.zoomStart;
                CamOrbit.transform.position = new Vector3(0, 0, 0);

                CamOrbit.minSideRot = 45;
                CamOrbit.maxSideRot = 45;

                CamOrbit.maxRotUp = 60;
                CamOrbit.minRotUp = 0;

                CamOrbit.cameraRotUpStart = 32;
                CamOrbit.cameraRotUp = CamOrbit.cameraRotUpStart;
            }
        }
        else
        {

            
            AlgorithmObj.SetActive(false);
            HCFObj.SetActive(true);
            Algorithm_UI.SetActive(false);
            HCF_UI.SetActive(true);
            Zekki_Count_Text.text = "";
            Calculate_Btn_HCF.gameObject.SetActive(false);
            Cond_Yes_Btn_HCF.transform.parent.gameObject.SetActive(false);
            DiviStepCount = 0;
            Validate_Btn_HCF.gameObject.SetActive(true);
            Calculate_Btn_HCF.gameObject.SetActive(false);
            First_Equ_text.text = "";
            FinalTxt_HCF.text = "";
            Updateda_val_text_HCF.transform.parent.parent.gameObject.SetActive(false);
            Validate_Btn_HCF.transform.parent.gameObject.SetActive(true);
            Equation1.GetComponent<Image>().sprite = Equation1.spriteState.disabledSprite;
            Equation2.GetComponent<Image>().sprite = Equation2.spriteState.pressedSprite;

            a_incr_Btn_HCF.interactable = true;
            a_decr_Btn_HCF.interactable = true;
            b_incr_Btn_HCF.interactable = true;
            b_decr_Btn_HCF.interactable = true;

            a_val_text_HCF.text = "0";
            b_val_text_HCF.text = "0";

            for (int i = 0; i < DivisionSteps.Count; i++)
            {
                DivisionSteps[i].SetActive(false);
            }

            DivisionSteps[DiviStepCount].SetActive(true);
            DivisionSteps[DiviStepCount].transform.Find("Divisor_TMP").GetComponent<TextMeshPro>().text = "";
            DivisionSteps[DiviStepCount].transform.Find("Dividend_TMP").GetComponent<TextMeshPro>().text = "";
            DivisionSteps[DiviStepCount].transform.Find("Multiplied_TMP").GetComponent<TextMeshPro>().text = "";
            DivisionSteps[DiviStepCount].transform.Find("Quotient_TMP").GetComponent<TextMeshPro>().text = "";
            DivisionSteps[DiviStepCount].transform.Find("Reminder_TMP").GetComponent<TextMeshPro>().text = "";

            LoadValuesHCF();

            if (!IsAR)
            {
                CamOrbit.zoomMax = 1.5f;
                CamOrbit.zoomMin = 0.75f;
                CamOrbit.zoomStart = 1f;
                CamOrbit.distance = CamOrbit.zoomStart;
                CamOrbit.transform.position = new Vector3(0, 0.25f, 0);

                CamOrbit.minSideRot = 45;
                CamOrbit.maxSideRot = 45;

                CamOrbit.maxRotUp = 60;
                CamOrbit.minRotUp = 0;

                CamOrbit.cameraRotUpStart = 0;
                CamOrbit.cameraRotUp = CamOrbit.cameraRotUpStart;
            }
        }
        InEquation1 = loadEquation1;
    }


    public void CreateZekki()
    {
        if (!IsTopicUnlocked)
        {
            b_Value = 9;// UnityEngine.Random.Range(3, 10);
            a_Value = 13;// UnityEngine.Random.Range(10, 31);
        }
        else
        {
            b_Value = UnityEngine.Random.Range(3, 10);
            a_Value = UnityEngine.Random.Range(10, 31);
        }

        a_Val_Text.text = a_Value.ToString();
        b_Val_Text.text = b_Value.ToString();

        Dividend_change = a_Value;


        Get_Reminder_Quotient();

        NumOfZekToCreate = a_Value;
        RemZekToCreate = NumOfZekToCreate;

        Condition_Text.text = "Is " + RemZekToCreate + " >= " + b_Value;


        Zekki_Count_Text.text = "Bots left(a): " + RemZekToCreate.ToString();


        float X_Pos = 0, Z_Pos = 0, NumOfRow = 0;

        
        if (Reminder_val == 0)
        {
            X_Pos = WidthLine_X.transform.localScale.x / Quotient_val;
            NumOfRow = Quotient_val - 1;
        }
        else
        {
            X_Pos = WidthLine_X.transform.localScale.x / (Quotient_val + 1);
            NumOfRow = Quotient_val;


        }

        Z_Pos = WidthLine_Y.transform.localScale.z / b_Value;


        for (int i = 0; i < b_Value; i++)
        {
            Vector3 Pos = new Vector3(0, 0, Start_Pos.transform.localPosition.z) + new Vector3(0, 0, -Z_Pos * i + Z_Pos / 2);
            print("pos....." + Pos);
            GameObject LineX = Instantiate(Line_X, Pos, Quaternion.identity, Line_X.transform.parent);
            print("LineX....." + LineX.transform.localPosition);
            LineX.transform.localPosition = Pos;
            LineX.transform.localEulerAngles = Vector3.zero;
            GridLines_X.Add(LineX);
        }

        for (int i = 0; i <= NumOfRow; i++)
        {
            Vector3 Pos = new Vector3(Start_Pos.transform.localPosition.x, 0, 0) + new Vector3(X_Pos * i + X_Pos / 2, 0, 0);

            GameObject LineY = Instantiate(Line_Y, Pos, Quaternion.identity, Line_Y.transform.parent);
            LineY.transform.localPosition = Pos;
            LineY.transform.localEulerAngles = Vector3.zero;
            GridLines_Y.Add(LineY);
        }

        /*float X = 0, Z = 0;

        for (int i = 0; i < NumOfZekToCreate; i++)
        {

            if (i % Quotient_val == 0)
            {
                X = 0;
                Z_Pos += Z_Pos;
                Z = 0;
            }

            GameObject Zekk = Instantiate(Zekki, Vector3.zero, Quaternion.identity, this.transform);
            Zekk.transform.localPosition = Start_Pos.transform.localPosition + new Vector3(X * X_Pos, 0, Z_Pos * Z);

            X++;
            Z++;
        }*/
        Line_X.SetActive(false);
        Line_Y.SetActive(false);
    }

    public void CheckForCondition(Button YesOrNo)
    {
        float X_Pos = 0, Z_Pos = 0;
        bool IsCondition = false;

        if (Reminder_val == 0)
        {
            X_Pos = WidthLine_X.transform.localScale.x / Quotient_val;

        }
        else
        {
            X_Pos = WidthLine_X.transform.localScale.x / (Quotient_val + 1);

        }

        Z_Pos = WidthLine_Y.transform.localScale.z / b_Value;

        if (RemZekToCreate >= b_Value && YesOrNo == Condition_Yes_Btn)
            IsCondition = true;
        else if (RemZekToCreate < b_Value && YesOrNo == Condition_No_Btn)
            IsCondition = true;



        if (IsCondition)
        {

            RowToCreate++;
            for (int i = (int)RowToCreate - 1; i < RowToCreate; i++)
            {
                for (int j = 0; j < b_Value; j++)
                {
                    if (Zekkis.Count < NumOfZekToCreate)
                    {
                        Vector3 Pos = Start_Pos.transform.localPosition + new Vector3(i * X_Pos, WidthLine_X.transform.localPosition.y, -Z_Pos * j);
                        Vector3 StartPos = new Vector3(0, 2, 0);// Camera.main.WorldToViewportPoint(Zekki_Count_Text.transform.parent.localPosition);
                        //print("Start......."+StartPos);
                        GameObject Zekk = Instantiate(Zekki, StartPos, Quaternion.identity, this.transform);
                        //new Vector3(0.47f,0.052f,-0.5f)
                        iTween.MoveTo(Zekk, iTween.Hash("x", Pos.x, "y", Pos.y, "z", Pos.z, "delay", j * 0.1f, "time", 0.5f, "islocal", true, "easetype", iTween.EaseType.easeInOutSine));
                        Zekk.SetActive(true);

                        RemZekToCreate--;

                        Zekkis.Add(Zekk);
                    }

                }


            }

            Condition_Text.text = "Is " + RemZekToCreate + " >= " + b_Value;
            Zekki_Count_Text.text = RemZekToCreate.ToString();
        }
        else
        {
            ShowInstructionPanel("Wrong option selected");
        }

        if (IsCondition)
        {
            a_val_text.text = (RemZekToCreate + b_Value).ToString();
            b_val_text.text = b_Value.ToString();
            q_val_text.text = "1";
            r_val_text.text = RemZekToCreate.ToString();
            FinalCond_text.text = "0 <= " + RemZekToCreate + " < " + b_Value;
            Zekki_Count_Text.text = "Bots left(a): " + RemZekToCreate.ToString();
        }

        if(!IsTopicUnlocked)
        CheckFor_XP_Condition(TopicToCheck);
    }

    public void Get_Reminder_Quotient()
    {
        while ((Dividend_change % b_Value) != 0)
        {
            //print("Dividend_change..."+ Dividend_change+"...........val...."+(Dividend_change % b_Value));
            Dividend_change--;

        }

        Reminder_val = (a_Value - Dividend_change);
        Quotient_val = (a_Value - Reminder_val) / b_Value;
    }



    public void LoadValuesHCF()
    {
        a_Value_HCF = UnityEngine.Random.Range(95, 1000);// 995;
        b_Value_HCF = UnityEngine.Random.Range(2, 96); //22;
        Reminder_Val_HCF = 5f;

        GivenIntegers_Txt_HCF.text = "Given integers "+a_Value_HCF+", "+b_Value_HCF ;
        //a_val_text_HCF.text = a_Value_HCF.ToString();
        //b_val_text_HCF.text = b_Value_HCF.ToString();

        Cond_Text_HCF.text = "Is Remainder is ' 0 '";

        Dividend_Val_HCF = a_Value_HCF;
        Divisor_Val_HCF = b_Value_HCF;

        Dividend_change = a_Value_HCF;
        Divisor_change = b_Value_HCF;

        

    }


    public void Change_a_b_Values(Button aorb)
    {
        if (aorb == a_incr_Btn_HCF || aorb == a_decr_Btn_HCF)
        {
            if (a_val_text_HCF.text == a_Value_HCF.ToString())
            {
                a_val_text_HCF.text = b_Value_HCF.ToString();
            }
            else
            {
                a_val_text_HCF.text = a_Value_HCF.ToString();
            }
        }
        else
        {
            if (b_val_text_HCF.text == a_Value_HCF.ToString())
            {
                b_val_text_HCF.text = b_Value_HCF.ToString();
            }
            else
            {
                b_val_text_HCF.text = a_Value_HCF.ToString();
            }
        }
    }

    public void Validate_a_b_values()
    {
        if (a_val_text_HCF.text == a_Value_HCF.ToString() && b_val_text_HCF.text == b_Value_HCF.ToString())
        {
            Validate_Btn_HCF.gameObject.SetActive(false);
            Calculate_Btn_HCF.gameObject.SetActive(true);

            a_incr_Btn_HCF.interactable = false;
            a_decr_Btn_HCF.interactable = false;
            b_incr_Btn_HCF.interactable = false;
            b_decr_Btn_HCF.interactable = false;

            for (int i = 0; i < DivisionSteps[DiviStepCount].transform.childCount; i++)
            {
                DivisionSteps[DiviStepCount].transform.GetChild(i).localScale = Vector3.zero;
                iTween.ScaleTo(DivisionSteps[DiviStepCount].transform.GetChild(i).gameObject, iTween.Hash("x", 1f, "y", 1f, "z", 1f, "time", 1f, "easetype", iTween.EaseType.easeOutBounce));

            }

            DivisionSteps[DiviStepCount].transform.Find("Divisor_TMP").GetComponent<TextMeshPro>().text = Divisor_Val_HCF.ToString();
            DivisionSteps[DiviStepCount].transform.Find("Dividend_TMP").GetComponent<TextMeshPro>().text = Dividend_Val_HCF.ToString();
            DivisionSteps[DiviStepCount].transform.Find("Multiplied_TMP").GetComponent<TextMeshPro>().text = "";
            DivisionSteps[DiviStepCount].transform.Find("Quotient_TMP").GetComponent<TextMeshPro>().text = "";
            DivisionSteps[DiviStepCount].transform.Find("Reminder_TMP").GetComponent<TextMeshPro>().text = "";
        }
        else
        {
            ShowInstructionPanel("Wrong values chosen");
        }
    }

    public void CheckForConditionHCF(Button YesOrNo)
    {

       

        Calculate_Btn_HCF.gameObject.SetActive(false);

        
        YesOrNo.transform.parent.gameObject.SetActive(true);

        if (YesOrNo == Cond_Yes_Btn_HCF)
        {
            if (Reminder_Val_HCF == 0)
            {
                FinalTxt_HCF.text = "HCF of given integers " + a_Value_HCF + "," + b_Value_HCF + " is " + Dividend_Val_HCF;
                ShowWelldonePanel("Welldone!");
                Reset_HCF.gameObject.SetActive(true);
            }
            else
            {
                ShowInstructionPanel("Wrong option selected");
            }
        }
        else
        {
            
            
                if (Reminder_Val_HCF != 0)
                {
                    UpdateReminderQuo_HCF();
                }
                else
                {
                    ShowInstructionPanel("Wrong option selected");
                }
            

        }


    }


    public void UpdateReminderQuo_HCF()
    {
        if (Reminder_Val_HCF == 0)
            return;

        while ((Dividend_change % Divisor_change) != 0)
        {
            //print("Dividend_change..."+ Dividend_change+"...........val...."+(Dividend_change % b_Value));
            Dividend_change--;

        }

        Reminder_Val_HCF = (Dividend_Val_HCF - Dividend_change);
        Quotient_Val_HCF = (Dividend_Val_HCF - Reminder_Val_HCF) / Divisor_change;

        if (DiviStepCount == 1)
        {
            Updateda_val_text_HCF.transform.parent.parent.gameObject.SetActive(true);
            Validate_Btn_HCF.transform.parent.gameObject.SetActive(false);
        }
            First_Equ_text.text = Dividend_Val_HCF + " = "+ Divisor_change + " * "+Quotient_Val_HCF+" + "+Reminder_Val_HCF;

        Updateda_val_text_HCF.text = Dividend_Val_HCF.ToString();
        Updatedb_val_text_HCF.text = Divisor_change.ToString();


        DivisionSteps[DiviStepCount].SetActive(true);
            
            for (int i = 0; i < DivisionSteps[DiviStepCount].transform.childCount; i++)
            {
                DivisionSteps[DiviStepCount].transform.GetChild(i).localScale = Vector3.zero;
                iTween.ScaleTo(DivisionSteps[DiviStepCount].transform.GetChild(i).gameObject, iTween.Hash("x", 1f, "y", 1f, "z", 1f, "time", 1f, "easetype", iTween.EaseType.easeOutBounce));

            }

            DivisionSteps[DiviStepCount].transform.Find("Dividend_TMP").GetComponent<TextMeshPro>().text = Dividend_Val_HCF.ToString();
            DivisionSteps[DiviStepCount].transform.Find("Multiplied_TMP").GetComponent<TextMeshPro>().text = Dividend_change.ToString();
            DivisionSteps[DiviStepCount].transform.Find("Quotient_TMP").GetComponent<TextMeshPro>().text = Quotient_Val_HCF.ToString();
            DivisionSteps[DiviStepCount].transform.Find("Reminder_TMP").GetComponent<TextMeshPro>().text = Reminder_Val_HCF.ToString();

            
            
            
            Dividend_Val_HCF = Divisor_change;
            Divisor_change = Reminder_Val_HCF;
            Dividend_change = Dividend_Val_HCF;

            DiviStepCount++;
        
    }

    public bool CheckFor_XP_Condition(int Current_Todo)
    {
        bool Condition = true;
        if (Current_Todo == 0)
        {
            if (RemZekToCreate == 4)
            {
                Collect_XP(this.gameObject);
                Condition = false;
            }
            else
            {
                ShowInstructionPanel("Wrong parameter chosen");
                Condition = true;
            }
        }
        else if (Current_Todo == 1)
        {
            if (RemZekToCreate == 0)
            {
                Collect_XP(this.gameObject);
                Condition = false;
            }
            else
            {
                ShowInstructionPanel("Wrong parameter chosen");
                Condition = true;
            }

        }
        else if (Current_Todo == 2)
        {
            if (Verify_Click)
            {
                Collect_XP(this.gameObject);
                Condition = false;
            }
            else
            {
                ShowInstructionPanel("Wrong parameter chosen");
                Condition = true;
            }

        }
        else
        {
            ShowInstructionPanel("Wrong parameter chosen");
            Condition = true;
        }
        
        

        return Condition;
    }


    public void Verify_Btn_Click()
    {
        if (RemZekToCreate == 0)
        {
            if (0 <= RemZekToCreate && RemZekToCreate < b_Value)
            {
                ShowWelldonePanel("Welldone!");
                Verify_Click = true;
                Reset_Algo.gameObject.SetActive(true);
                if (!IsTopicUnlocked)
                    CheckFor_XP_Condition(TopicToCheck);
                
            }
            else
            {
                ShowInstructionPanel("Condition not satisfied");
            }
        }
        else
        {
            ShowInstructionPanel(" Still ( "+RemZekToCreate+" ) bots remaining! ");
        }
    }

    public void ShowInstructionPanel(string TextToDisplay)
    {
        var mouse = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);



        if (mouse.x < Screen.width / 2)
        {
            // WronOptionPanel
            //print("Mouse is on left side of screen.");
            InstructionPanel.GetComponent<RectTransform>().pivot = new Vector2(0, 0);
        }



        if (mouse.x > Screen.width / 2)
        {
            //print("Mouse is on right side of screen.");
            InstructionPanel.GetComponent<RectTransform>().pivot = new Vector2(1, 0);
        }

        InstructionPanel.transform.position = Input.mousePosition;
        UIcanvas.GetComponent<GraphicRaycaster>().enabled = false;
        Camera.main.GetComponent<PhysicsRaycaster>().enabled = false;
        iTween.Stop(InstructionPanel.gameObject);

        InstructionPanel.transform.Find("InstructionText").GetComponent<Text>().text = TextToDisplay;
        InstructionPanel.transform.GetChild(0).GetComponent<Text>().color = GetColorFromColorCode("#FF7500");
        //iTween.ScaleTo(GlassPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 0.01f, "easetype", iTween.EaseType.spring));
        iTween.ScaleTo(InstructionPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 0.3f, "easetype", iTween.EaseType.spring, "oncomplete", "CloseInstructionPanel", "oncompletetarget", this.gameObject));
        ObjectivePanel.transform.localScale = new Vector3(1, 0, 1);
    }

    public void ShowWelldonePanel(string TextToShow)
    {
        var mouse = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);



        if (mouse.x < Screen.width / 2)
        {
            // WronOptionPanel
            //print("Mouse is on left side of screen.");
            InstructionPanel.GetComponent<RectTransform>().pivot = new Vector2(0, 0);
        }



        if (mouse.x > Screen.width / 2)
        {
            //print("Mouse is on right side of screen.");
            InstructionPanel.GetComponent<RectTransform>().pivot = new Vector2(1, 0);
        }

        InstructionPanel.transform.position = Input.mousePosition;
        UIcanvas.GetComponent<GraphicRaycaster>().enabled = false;
        Camera.main.GetComponent<PhysicsRaycaster>().enabled = false;
        iTween.Stop(InstructionPanel.gameObject);

        //iTween.ScaleTo(GlassPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 0.01f, "easetype", iTween.EaseType.spring));
        InstructionPanel.transform.GetChild(0).GetComponent<Text>().text = TextToShow;// "Wrong parameter choosen";
        InstructionPanel.transform.GetChild(0).GetComponent<Text>().color = GetColorFromColorCode("#AAC018");
        iTween.ScaleTo(InstructionPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 1f, "easetype", iTween.EaseType.spring, "oncomplete", "CloseInstructionPanel", "oncompletetarget", this.gameObject));
        ObjectivePanel.transform.localScale = new Vector3(1, 0, 1);
    }


    public void AddListenerToEvents(EventTriggerType eventType, Action MethodToCall, GameObject TriggerObjToAdd)
    {

        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall());
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }

    public void AddListenerToEvents(EventTriggerType eventType, Action<float> MethodToCall, GameObject TriggerObjToAdd, float p)
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall(p));
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }

    public void AddListenerToEvents(EventTriggerType eventType, Action<GameObject> MethodToCall, GameObject TriggerObjToAdd, GameObject p)
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall(p));
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }

    public void EnableOrbit()
    {

        if (!IsAR)
        {
            CamOrbit.DisableInteration = false;
            //print("EnableOrbit");
        }
        else
        {
            ArOrbit.DisableInteration = false;
        }
    }

    public void DisableOrbit()
    {

        if (!IsAR)
        {
            CamOrbit.DisableInteration = true;
            //print("DisableOrbit");
        }
        else
        {
            ArOrbit.DisableInteration = true;
        }
    }

    public void OpenInfoPanel()
    {
        UIZoomIn();
        iTween.ScaleTo(InfoPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 0.5f, "easetype", iTween.EaseType.spring));
        iTween.ScaleTo(GlassPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 0.01f, "easetype", iTween.EaseType.spring));

    }

    public void CloseInfoPanel(Button CloseBtn)
    {
        UIZoomOut();
        iTween.ScaleTo(InfoPanel, iTween.Hash("x", 0, "y", 0, "z", 0, "time", 0.2f, "easetype", iTween.EaseType.linear));
        iTween.ScaleTo(GlassPanel, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.2f, "time", 0.01f, "easetype", iTween.EaseType.linear));
        if (CloseBtn == Info_Cntue_Btn)
        {
            if (!IsAR)
                CamOrbit.DisableInteration = false;
            /*if (ActiveBtnCnt == 0)
            {
                for (int i = 0; i < Elements.Length; i++)
                {

                    Vector3 RandomPos = new Vector3(Elements[i].transform.localPosition.x, Elements[i].transform.localPosition.y, UnityEngine.Random.Range(-5f, 0f));
                    Vector3 CurrPos = Elements[i].transform.localPosition;

                    Elements[i].transform.localPosition = RandomPos;

                    iTween.MoveTo(Elements[i], iTween.Hash("position", CurrPos, "delay", 0.7f, "islocal", true, "time", UnityEngine.Random.Range(0.1f, 0.2f), "speed", 2.5f, "easetype", iTween.EaseType.easeInOutSine));


                }
            }*/
            Info_Close_Btn.gameObject.SetActive(true);
            Info_Cntue_Btn.gameObject.SetActive(false);
            
        }

    }

    public void CloseInstructionPanel()
    {
        UIcanvas.GetComponent<GraphicRaycaster>().enabled = true;
        Camera.main.GetComponent<PhysicsRaycaster>().enabled = true;
        //iTween.ScaleTo(GlassPanel, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 1f, "time", 0.01f, "easetype", iTween.EaseType.linear));
        iTween.ScaleTo(InstructionPanel, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.5f, "time", 0.3f, "easetype", iTween.EaseType.spring));
        if(!IsTopicUnlocked)
        iTween.ScaleTo(ObjectivePanel, iTween.Hash("y", 1, "time", 0.3f, "delay", 0.5f, "easetype", iTween.EaseType.easeInOutSine));
    }

    public void ToDoList_DDCancel()
    {
        if (ToDoList_DD.transform.Find("Dropdown List") != null)//&& Obj.transform.localScale.y!=1)
        {
            //print("cancelled........");
            //print("list.....cancelled........" + ToDoList_DD.transform.Find("Dropdown List").gameObject);
            ToDoList_DD.transform.Find("Arrow").transform.eulerAngles = new Vector3(0, 0, 0);
            iTween.ScaleTo(ToDoList_DD.transform.Find("Dropdown List").gameObject, iTween.Hash("y", 0, "time", 0.5f, "easetype", iTween.EaseType.spring));
            iTween.ScaleTo(ObjectivePanel, iTween.Hash("y", 1, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine));
            if (SidePannelOpen.transform.eulerAngles.z == 180)
            {
                OpenSidePannel();
            }
        }
    }

    public void ExploringToDOList_DD()
    {
        //print("ShowDD.....");
        //ToDoList_DD.transform.Find("Topic_Objective").GetComponent<Text>().text = "";
        if (SidePannelOpen.transform.eulerAngles.z == 0)
        {
            OpenSidePannel();
        }

        ToDoList_DD.transform.Find("Arrow").transform.eulerAngles = new Vector3(0, 0, 180);
        GameObject DDList = ToDoList_DD.transform.Find("Dropdown List").gameObject;
        DDList.transform.GetComponent<RectTransform>().sizeDelta = new Vector2(0, 250);
        DDList.transform.localScale = new Vector3(1, 0, 1);
        iTween.ScaleTo(DDList, iTween.Hash("y", 1, "time", 0.5f, "easetype", iTween.EaseType.spring));
        //iTween.ScaleTo(ToDoList_DD.transform.Find("Topic_Objective").gameObject, iTween.Hash("y", 0, "time", 0.5f, "easetype", iTween.EaseType.spring));
        if (!IsTopicUnlocked)
            iTween.ScaleTo(ObjectivePanel, iTween.Hash("y", 0, "time", 0.5f, "delay", 0.1f, "easetype", iTween.EaseType.easeInOutSine));

        string TestStr = "";
        int CntToChangePos = 0;
        for (int i = 0; i < ToDoList_DD.options.Count; i++)
        {
            GameObject Child = GameObject.Find("Item " + i + ": " + ToDoList_DD.options[i].text);
            //Child.transform.parent.GetComponent<RectTransform>().sizeDelta = new Vector2(0,250);

            if (TestStr == ToDoList_DD.options[i].text)
            {
                Child.SetActive(false);
                Child.transform.localPosition = Child.transform.localPosition + new Vector3(0, 50 * CntToChangePos, 0);
                CntToChangePos++;
            }
            else
            {
                Child.transform.Find("XP_Text").GetComponent<Text>().text = "+" + Sub_Top_XP[i] + "XP";
                Child.transform.localPosition = Child.transform.localPosition + new Vector3(0, 50 * CntToChangePos, 0);

                if (i < TopicToCheck)
                {
                    Child.transform.Find("Item Checkmark").GetComponent<Image>().enabled = true;
                    //Child.transform.Find("XP_Text").GetComponent<Text>().color = Color.green / 2;
                    //Child.transform.Find("Item Checkmark").GetComponent<Image>().color = Color.green / 2;
                    Child.transform.Find("Item Background").GetComponent<Image>().enabled = false;
                }
                else if (i == TopicToCheck)
                {
                    Child.transform.Find("Item Background").GetComponent<Image>().enabled = true;// color = ReturnColorFromHex("#a01c65");
                                                                                                 //Child.transform.Find("Item Label").GetComponent<Text>().color = Color.white;
                }
            }
            TestStr = ToDoList_DD.options[i].text;
        }
    }


    public Color GetColorFromColorCode(string ColorCode)
    {
        Color ConvColor;
        bool Converted = ColorUtility.TryParseHtmlString(ColorCode, out ConvColor);
        return ConvColor;
    }



    public void ZoomCamera(float Value)
    {
        CamOrbit.distance = Value;
    }







    public void OpenSidePannel()
    {
        if (SidePannelOpen.transform.eulerAngles.z == 180)
        {
            //iTween.MoveAdd(SidePannel, iTween.Hash("x", -400f, "time",0.5f,"islocal",true, "easetype", iTween.EaseType.linear));
            //iTween.MoveAdd(SidePannelOpen.gameObject, iTween.Hash("x", -185f, "islocal", true, "time", 0.5f, "easetype", iTween.EaseType.linear));
            //SidePannelOpen.transform.eulerAngles = new Vector3(0,0,180);
            SidePannel.GetComponent<Animator>().Play("PannelForward");
        }
        else
        {
            //iTween.MoveAdd(SidePannel, iTween.Hash("x", 400f, "time", 0.5f, "islocal", true, "easetype", iTween.EaseType.linear));
            //iTween.MoveAdd(SidePannelOpen.gameObject, iTween.Hash("x", -185f, "islocal", true, "time", 0.5f, "easetype", iTween.EaseType.linear));
            //SidePannelOpen.transform.eulerAngles = new Vector3(0, 0, 0);
            SidePannel.GetComponent<Animator>().Play("PannelBackward");
        }
    }
    /*public void OpenTopSidePannel()
    {
        //print("Clicked......"+ PannelOpen.transform.eulerAngles.y);
        if (TopSidePannelOpen.gameObject.transform.localEulerAngles.z == 0)
        {
            TopSidePannel.GetComponent<Animator>().Play("TopButtonOpen");
            BtnselName.gameObject.SetActive(false);
            // TopSidePannelOpen.gameObject.transform.localEulerAngles = new Vector3(0, 0, 180);
        }
        else
        {
            TopSidePannel.GetComponent<Animator>().Play("TopButtonClose");
            // TopSidePannelOpen.gameObject.transform.localEulerAngles = new Vector3(0, 0, 0);
        }
    }*/

    // Update is called once per frame
    void FixedUpdate()
    {
        if(GridLines_X.Count>0)
        if (GridLines_X[0].GetComponent<LineRenderer>().widthMultiplier != transform.parent.localScale.x)
        {
            for (int i = 0; i < GridLines_X.Count; i++)
            {
                GridLines_X[i].GetComponent<LineRenderer>().widthMultiplier = transform.parent.localScale.x;
            }

            for (int i = 0; i < GridLines_Y.Count; i++)
            {
                GridLines_Y[i].GetComponent<LineRenderer>().widthMultiplier = transform.parent.localScale.x;
            }
        }

        //if (!IsAR)
        //{

        //    if (CamOrbit.distance < CamOrbit.zoomStart && !IsUIZooming)
        //    {
        //        UIZoomIn();
        //        IsUIZooming = true;
        //    }
        //    else if (CamOrbit.distance > CamOrbit.zoomStart && IsUIZooming)
        //    {
        //        UIZoomOut();
        //        IsUIZooming = false;
        //    }
        //}

        /*if (CamOrbit.Zoom_bool && ActiveBtnCnt<8)
        {
            CamOrbit.zoomMin = CamOrbit.cameraRotUp * -0.01125f + 1.15f;
        }*/
    }



    public void UIReset()
    {
        ObjectivePanel.GetComponent<RectTransform>().anchoredPosition = new Vector3(0f, 5f, 0);

        //ValueDisplaySlider.transform.GetComponent<RectTransform>().anchoredPosition = new Vector3(-20f, 0f, 0);
        Info_Open_Btn.transform.GetComponent<RectTransform>().anchoredPosition = new Vector3(-37f, -150f, 0);


        SidePannel.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(0f, 0f, 0);

        //print("side......" + SidePannel.transform.position+"....local....."+ SidePannel.transform.localPosition);
        if (!IsTopicUnlocked)
        {
            ToDoList_DD.transform.GetComponent<RectTransform>().anchoredPosition = new Vector3(135f, -150f, 0);
            XP_ToShow.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(135f, -150f, 0);

        }
        else
        {
            ToDoList_DD.transform.GetComponent<RectTransform>().anchoredPosition = new Vector3(-250f, -150f, 0);
            XP_ToShow.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(-250f, -150f, 0);

        }

        //Main_XP_ToShow.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(-135f, -60f, 0);

    }

    public void UIZoomOut()
    {
        if (ObjectivePanel.GetComponent<RectTransform>().anchoredPosition.y == 5)
            return;

        UIReset();

        iTween.MoveFrom(Info_Open_Btn.gameObject, iTween.Hash("x", Info_Open_Btn.transform.localPosition.x + 250, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveFrom(SidePannel.transform.parent.gameObject, iTween.Hash("x", SidePannel.transform.parent.localPosition.x - 1000f, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        //iTween.MoveFrom(ValueDisplaySlider.gameObject, iTween.Hash("x", ValueDisplaySlider.transform.localPosition.x + 250, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveFrom(ObjectivePanel, iTween.Hash("y", ObjectivePanel.transform.localPosition.y - 1000, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));


        iTween.MoveFrom(ToDoList_DD.gameObject, iTween.Hash("x", ToDoList_DD.transform.localPosition.x - 400, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveFrom(XP_ToShow.transform.parent.gameObject, iTween.Hash("x", XP_ToShow.transform.parent.localPosition.x - 400, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));

        //iTween.MoveFrom(Main_XP_ToShow.transform.parent.gameObject, iTween.Hash("x", Main_XP_ToShow.transform.parent.localPosition.x + 550, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
    }



    public void UIZoomIn()
    {
        if (ObjectivePanel.GetComponent<RectTransform>().anchoredPosition.y != 5)
            return;

        UIReset();
        //print("side......" + SidePannel.transform.position);
        iTween.MoveAdd(ObjectivePanel, iTween.Hash("y", ObjectivePanel.transform.localPosition.y - 1000, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveAdd(Info_Open_Btn.gameObject, iTween.Hash("x", Info_Open_Btn.transform.localPosition.x + 250, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveAdd(SidePannel.transform.parent.gameObject, iTween.Hash("x", SidePannel.transform.parent.localPosition.x - 1000f, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));

        //iTween.MoveAdd(ValueDisplaySlider.gameObject, iTween.Hash("x", ValueDisplaySlider.transform.localPosition.x + 250, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));

        iTween.MoveAdd(XP_ToShow.transform.parent.gameObject, iTween.Hash("x", XP_ToShow.transform.parent.localPosition.x - 400, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveAdd(ToDoList_DD.gameObject, iTween.Hash("x", ToDoList_DD.transform.localPosition.x - 400, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));

        //iTween.MoveAdd(Main_XP_ToShow.transform.parent.gameObject, iTween.Hash("x", Main_XP_ToShow.transform.parent.localPosition.x + 550, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
    }

    public float ChangeToLocal_X(float Val)
    {
        float X_ToRet = 0;
        //print("wid........"+ UIcanvas.GetComponent<RectTransform>().rect.width);
        X_ToRet = ((UIcanvas.GetComponent<RectTransform>().rect.x) + Val);

        return X_ToRet;
    }

    public float ChangeToLocal_Y(float Val)
    {
        float Y_ToRet = 0;
        //print("wid........"+ UIcanvas.GetComponent<RectTransform>().rect.width);
        Y_ToRet = ((UIcanvas.GetComponent<RectTransform>().rect.y) + Val);

        return Y_ToRet;
    }


    public void Collect_XP(GameObject TargetPos)
    {

        UIcanvas.GetComponent<GraphicRaycaster>().enabled = false;
        Camera.main.GetComponent<PhysicsRaycaster>().enabled = false;


        XP_Effect.transform.position = Camera.main.WorldToScreenPoint(TargetPos.transform.position);

        XP_Effect.text = "+"+Sub_Top_XP[TopicToCheck] + "XP";

        XP_Effect.transform.localScale = new Vector3(2, 0, 2);

        iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 2, "y", 2, "z", 2, "time", 0.5f, "easetype", iTween.EaseType.easeOutElastic));

        iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", XP_Effect.transform.position.x, "y", XP_Effect.transform.position.y + 150, "z", 0, "delay", 0.3f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

        iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", 150, "y", XP_ToShow.transform.position.y, "z", 0, "delay", 1f, "time", 1f, "easetype", iTween.EaseType.easeInSine, "oncomplete", "Add_To_XP", "oncompletetarget", this.gameObject));

    }

    public void Add_To_XP()
    {

        //print("TopicToCheck......."+ TopicToCheck+"....sub......"+Sub_Topics.Count);
        if (TopicToCheck < (Sub_Top_ToDoList.Count))
        {
            Sub_XP_Earned += float.Parse(Sub_Top_XP[TopicToCheck]);

            XP_Effect.text = "";
            XP_ToShow.text = Sub_XP_Earned + "/" + Total_XP;

            TopicToCheck++;
            if (TopicToCheck <= (Sub_Top_ToDoList.Count - 1))
            {
                ObjectivePanel.transform.Find("ObjectiveText").GetComponent<Text>().text = Sub_Top_ToDoList[TopicToCheck];
                ObjectivePanel.transform.localScale = new Vector3(1, 0, 1);
                iTween.ScaleTo(ObjectivePanel, iTween.Hash("y", 1, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine));
                UIcanvas.GetComponent<GraphicRaycaster>().enabled = true;
                Camera.main.GetComponent<PhysicsRaycaster>().enabled = true;
                //ToDoList_DD.transform.Find("Topic_Objective").GetComponent<Text>().text = "Objective: " + Sub_Top_ToDoList[TopicToCheck];
            }
            else
            {
                XP_Effect.transform.position = (XP_ToShow.transform.position);

                XP_Effect.text = Total_XP + "XP";

                XP_Effect.transform.localScale = new Vector3(1, 0, 1);

                iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 2, "y", 2, "z", 2, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeOutElastic));

                iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", XP_Effect.transform.position.x, "y", XP_Effect.transform.position.y + 50, "z", 0, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

                iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", Main_XP_ToShow.transform.position.x, "y", Main_XP_ToShow.transform.position.y, "z", 0, "delay", 1.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine, "oncomplete", "AddTotalToMainXP", "oncompletetarget", this.gameObject));

                iTween.MoveTo(ToDoList_DD.gameObject, iTween.Hash("x", ChangeToLocal_X(-400), "delay", 3.5f, "time", 0.5f, "islocal", true, "easetype", iTween.EaseType.easeInOutSine));

                iTween.MoveTo(XP_ToShow.transform.parent.gameObject, iTween.Hash("x", ChangeToLocal_X(-400), "delay", 3.5f, "time", 0.5f, "islocal", true, "easetype", iTween.EaseType.easeInOutSine));
                //print("label......" + LabelImage.GetComponent<RectTransform>().anchoredPosition+".......explode...."+ ExplodeSlider.GetComponent<RectTransform>().anchoredPosition);


                //iTween.MoveTo(ValueDisplaySlider.gameObject, iTween.Hash("x", ChangeToLocal_X ( 150), "delay", 0.7f, "time", 0.5f, "islocal", true, "easetype", iTween.EaseType.easeInOutSine));
            }
        }

    }

    public void AddTotalToMainXP()
    {
        if (OptionUnlockedTill < 1)
        {
            OptionUnlockedTill++;
            UIcanvas.GetComponent<GraphicRaycaster>().enabled = true;
            Camera.main.GetComponent<PhysicsRaycaster>().enabled = true;
            float MainXp = float.Parse(Main_XP_ToShow.text);
            MainXp += Total_XP;
            Main_XP_ToShow.text = MainXp.ToString();
            //ObjectivePanel.transform.Find("ObjectiveText").GetComponent<Text>().text = "YaY!......Next topic unlocked";
            XP_Effect.text = "";
            IsTopicUnlocked = true;
            iTween.ScaleTo(ObjectivePanel, iTween.Hash("y", 0, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine));



        }
    }
}
