﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;

public class vid420_atwoodmachine : MonoBehaviour, IPointerDownHandler
{
    // Start is called before the first frame update

    public Canvas C_BG;

    public orbit CamOrbit;

    public AROrbitControls ArOrbit;

    public bool IsAR;

    public GameObject MainObj, Ground;

    public int OptionUnlockedTill, CurrentTopicSelected, TopicToCheck;

    public float Sub_XP_Earned, Total_XP;

    public Dropdown Options_DD, ToDoList_DD;

    public GameObject InstructionPanel_DD, ToDoList_Panel;

    public List<Dictionary<string, object>> CSV_data;

    public List<String> TaskList,XpList,InfoText;

    public List<GameObject> AllTaskData;

    public GameObject[] Labels;

    public Text XP_ToShow, XP_Effect, Main_XP_ToShow, localTotalXpValueText, localGainedXpValueText, ObjectiveInfoText, LGListText, IListText, AListText, 
        InfoHeadingText, GF_ResultTxt, E_valueTxt, m_valtxt;

    public GameObject TaskObj, AllTaskListObj;

    public RectTransform rT;

    public int localTotalXpValue, localGaniedXpValue,MainXpValue;

    public GameObject ControlPanel, LocalXP, Main_Xp, ObjectivePanel, InformationPanel, InfoButton, MainSlider, InfoPanelBg, TasklistPanel, WronOptionPanel, Gained_XP_ArrowBut;

    public Button InfoCloseBtn, InfoContinueBtn, Gained_XP_Panel, CpCloseBtn, IncButton, DecButton;
       
    public bool sessionStart;

    public TextAsset CSVFile;

    public AssetBundle assetBundle;
       
    public string[] CorrectMsgs = new string[] { "Nice!", "WellDone!", "Excellent!", "Correct!" };

    public Color Highlight_Color, Normal_Color;

    public Text MessageT, DescText;

    //......................

    public Slider SliderMass1, SliderMass2;

    public float mass1, mass2, g, a, t, y1, y2, w1, w2;

    public GameObject Weight1, Weight2, weight1_label, LenghtObj1, LenghtObj2, Pulley, Shadow1, Shadow2, Arrow, infoPanel,DescPopUp;

    private Color color1, color2, color3;

    private Vector3 Weight1Pos, Weight2Pos;

    public Text Mass1Text, Mass2Text, a_value;

    public TextMeshPro DescriptionTex, Mass1_Tex, Mass2_Tex, Acc_Text;


    public Button StartBut;
    

    void Start()
    {

        LoadFromAsssetBundle();
                     

        MainObj = this.gameObject;

        C_BG = GameObject.Find("Canvas_BG").GetComponent<Canvas>();

        Ground = GameObject.Find("Ground");

        if (!IsAR)
        {
            CamOrbit = Camera.main.GetComponentInParent<orbit>();

            C_BG.transform.parent = Camera.main.transform;
            C_BG.GetComponentInChildren<RawImage>().enabled = true;
            C_BG.renderMode = RenderMode.ScreenSpaceCamera;

            C_BG.worldCamera = Camera.main;

            CamOrbit.target = Camera.main.transform.parent;

            CamOrbit.target.position = new Vector3(0, 0.4f, 0);
            CamOrbit.maxRotUp = 10;
            CamOrbit.minRotUp = 1;
            CamOrbit.zoomMin = 0.5f;
            CamOrbit.zoomMax = 2;
            CamOrbit.zoomStart = 1f;
        }
        else
        {
            Ground.SetActive(false);

            C_BG.gameObject.SetActive(false);

            ArOrbit = MainObj.transform.parent.gameObject.GetComponent<AROrbitControls>();
        }

       

        sessionStart = false;

        //OptionUnlockedTill = 0;
        //TopicToCheck = 1;
        //MainXpValue = 10;

        ReadingDataFromCSVFile();

        ReadingXps();
        FindingObjects();
        TaskListCreation();
        AssigningClickEvents();
        LabelsAssignment();
        AssignInitialValues();
    }
    public void LoadFromAsssetBundle()
    {
        assetBundle = AssetBundle.LoadFromFile(PlayerPrefs.GetString("AssetBundleUrl"));

        CSVFile = assetBundle.LoadAsset(PlayerPrefs.GetString("LibraryName") + "CSV", typeof(TextAsset)) as TextAsset;
        
        //CSVFile = Resources.Load("vid420_atwoodmachineCSV") as TextAsset;        

        Camera.main.gameObject.AddComponent<PhysicsRaycaster>();

        Camera.main.backgroundColor = Color.black;

        GameObject TargetForCamera = GameObject.Find("Target");

        if (TargetForCamera != null)
        {
            TargetForCamera.AddComponent<orbit>();
            CamOrbit = TargetForCamera.GetComponent<orbit>();
            IsAR = false;
        }
        else
        {
            this.transform.parent.gameObject.AddComponent<AROrbitControls>();
            ArOrbit = this.transform.parent.gameObject.GetComponent<AROrbitControls>();
            IsAR = true;
        }

    }
    public void ReadingDataFromCSVFile()
    {

        TaskList = new List<string>();
        XpList = new List<string>();
        InfoText = new List<string>();

        AllTaskData = new List<GameObject>();

       // CSV_data = CSVReader.Read(CSVFile);


        //TaskList.Add("");
        TaskList.Add("Change the mass of block M₁ and start the simulation");
        TaskList.Add("Change the mass of block M₂ and start the simulation");

        //for (var i = 0; i < CSV_data.Count; i++)
        //{

            //if (CSV_data[i]["TaskList"].ToString() != "" && CSV_data[i]["TaskList"].ToString() != null)
            //{
            //    TaskList.Add(CSV_data[i]["TaskList"].ToString());
            //}

            //if (CSV_data[i]["XPList"].ToString() != "" && CSV_data[i]["XPList"].ToString() != null)
            //{
            //    XpList.Add(CSV_data[i]["XPList"].ToString());
            //}

            //if (CSV_data[i]["InfoText"].ToString() != "" && CSV_data[i]["InfoText"].ToString() != null)
            //{
            //    InfoText.Add(CSV_data[i]["InfoText"].ToString());
            //}

        //}
    }
    public int totXp, loclXp;

    public void ReadingXps()
    {

        totXp = PlayerPrefs.GetInt("TotalXP");
        loclXp = PlayerPrefs.GetInt("MaxEarnings");

        if (loclXp != 0)
        {
            OptionUnlockedTill = 0;

        }
        else
        {
            OptionUnlockedTill = 1;

        }

        TopicToCheck = 1;

        MainXpValue = totXp;

        int t = 0;
        int p = 0;

        for (int k = 0; k < TaskList.Count; k++)
        {
            p = loclXp / TaskList.Count;
            XpList.Add(p.ToString());
            t = t + p;
        }

        if (t != loclXp)
        {
            XpList[XpList.Count - 1] = (p + (loclXp - t)).ToString();
        }
    }
    public float result;

    public void GetResult()
    {
        result = ((float)(TopicToCheck - 1) / (float)(TaskList.Count)) * 100;
        Camera.main.SendMessage("UnloadActually", result);
    }
    public void FindingObjects() {

        InformationPanel = GameObject.Find("InformationPanel");

        InfoCloseBtn = GameObject.Find("InfoCloseBtn").GetComponent<Button>();

        InfoContinueBtn = GameObject.Find("InfoContinueBtn").GetComponent<Button>();

        InfoPanelBg = GameObject.Find("InfoPanelBg");
        
        XP_ToShow = GameObject.Find("localGainedXpValueText").GetComponent<Text>();

        XP_Effect = GameObject.Find("Effect_Text").GetComponent<Text>();

        Main_XP_ToShow = GameObject.Find("Main_XP_Value").GetComponent<Text>();

        localGainedXpValueText = GameObject.Find("localGainedXpValueText").GetComponent<Text>();

        localTotalXpValueText = GameObject.Find("GainedXP_LocalTotalXp").GetComponent<Text>();

        ObjectiveInfoText = GameObject.Find("ObjectiveInfoText").GetComponent<Text>();

        TaskObj = GameObject.Find("TaskObj");

        AllTaskListObj = GameObject.Find("AllTaskList");

        ControlPanel = GameObject.Find("ControlPanel");
        LocalXP = GameObject.Find("LocalXP");
        Main_Xp = GameObject.Find("Main_Xp");
        ObjectivePanel = GameObject.Find("ObjectivePanel");
        InformationPanel = GameObject.Find("InformationPanel");
        InfoButton = GameObject.Find("InfoButton");
        MainSlider = GameObject.Find("MainSlider");
        WronOptionPanel = GameObject.Find("WrongPanel");

        Gained_XP_Panel = GameObject.Find("Gained_XP_Panel").GetComponent<Button>();
        TasklistPanel = GameObject.Find("TasklistPanel");
        CpCloseBtn = GameObject.Find("CpCloseBtn").GetComponent<Button>();

        InfoHeadingText = GameObject.Find("InfoHeadingText").GetComponent<Text>();
        LGListText = GameObject.Find("LGListText").GetComponent<Text>();
        IListText = GameObject.Find("IListText").GetComponent<Text>();
        AListText = GameObject.Find("AListText").GetComponent<Text>();

        Gained_XP_ArrowBut = GameObject.Find("Gained_XP_ArrowBut");
        //......................................................

        StartBut = GameObject.Find("Start").GetComponent<Button>();

        SliderMass1 = GameObject.Find("SliderMass1").GetComponent<Slider>();
        SliderMass2 = GameObject.Find("SliderMass2").GetComponent<Slider>();

        Mass1Text = GameObject.Find("m1_value").GetComponent<Text>();
        Mass2Text = GameObject.Find("m2_value").GetComponent<Text>();

        AddListener(EventTriggerType.PointerDown, DisableOrbit, SliderMass1.gameObject);
        AddListener(EventTriggerType.PointerUp, EnableOrbit, SliderMass1.gameObject);

    

        AddListener(EventTriggerType.PointerDown, DisableOrbit, SliderMass2.gameObject);
        AddListener(EventTriggerType.PointerUp, EnableOrbit, SliderMass2.gameObject);

        AddListener(EventTriggerType.PointerUp, SliderMass2Change, SliderMass2.gameObject);

        AddListener(EventTriggerType.PointerUp, SliderMass1Change, SliderMass1.gameObject);

        Weight1 = GameObject.Find("weight1");
        Weight2 = GameObject.Find("weight2");

        weight1_label = GameObject.Find("weight1_label");

        Shadow1 = GameObject.Find("Shadows1");
        Shadow2 = GameObject.Find("Shadows2");

        LenghtObj1 = GameObject.Find("rope_V1");
        LenghtObj2 = GameObject.Find("rope_V2");

        Pulley = GameObject.Find("Pulley");

        Arrow = GameObject.Find("arrow");

        a_value = GameObject.Find("a_value").GetComponent<Text>();

        DescText = GameObject.Find("DescriptionText").GetComponent<Text>();

        DescPopUp = GameObject.Find("DescPopUp");

        DescriptionTex = GameObject.Find("DescriptionTex").GetComponent<TextMeshPro>();

        Mass1_Tex = GameObject.Find("Mass1_Tex").GetComponent<TextMeshPro>();
        Mass2_Tex = GameObject.Find("Mass2_Tex").GetComponent<TextMeshPro>();
        Acc_Text = GameObject.Find("Acc_Text").GetComponent<TextMeshPro>();

        color3 = Shadow2.GetComponent<Renderer>().material.color;

        Mass1Text.text = "5 gm";
        Mass2Text.text = "5 gm";

        Weight1Pos = Weight1.transform.localPosition;
        Weight2Pos = Weight2.transform.localPosition;


        SliderMass1.onValueChanged.AddListener(delegate { ChangeMass(SliderMass1.value, Weight1, Mass1Text, Mass1_Tex); });
        SliderMass2.onValueChanged.AddListener(delegate { ChangeMass(SliderMass2.value, Weight2, Mass2Text, Mass2_Tex); });
    }

     
    public void AssigningClickEvents() {
                
        iTween.ScaleTo(InformationPanel.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

        InfoCloseBtn.onClick.AddListener(() => CloseInfoPanel());

        InfoContinueBtn.onClick.AddListener(() => ContinueInfoPanel());

        InfoButton.GetComponent<Button>().onClick.AddListener(() => OpenInfoPanel());

        Gained_XP_Panel.onClick.AddListener(() => OpenTaskList());

        CpCloseBtn.onClick.AddListener(() => OpenOrCloseControlPanel());

        //MainSlider.GetComponent<Slider>().onValueChanged.AddListener(delegate { ChangeRadius(MainSlider.GetComponent<Slider>().value, MainSlider.GetComponentInChildren<Text>()); });

        //AddListener(EventTriggerType.PointerDown, DisableOrbit, MainSlider.gameObject);

        //AddListener(EventTriggerType.PointerUp, EnableOrbit, MainSlider.gameObject);
        
        StartBut.GetComponent<Button>().onClick.AddListener(() => StartAction());

    }

    public void AssignInitialValues()
    {
        //InfoHeadingText.text = InfoText[0];
        //LGListText.text = InfoText[1];
        //IListText.text = InfoText[2];
        //AListText.text = InfoText[3];

        if (OptionUnlockedTill > 0)
        {
            LocalXP.transform.localScale = new Vector3(0, 0, 0);
            ObjectivePanel.transform.localScale = new Vector3(0, 0, 0);
        }

        InfoCloseBtn.gameObject.transform.localScale = new Vector3(0, 0, 0);
        localGainedXpValueText.text = "0";
        Main_XP_ToShow.text = MainXpValue + "";
        WronOptionPanel.transform.localScale = new Vector3(0, 0, 0);

        g = 9.81f;

        DescPopUp.SetActive(false);
    }


    public void TaskListCreation() {

        rT = AllTaskListObj.transform.parent.GetComponent<RectTransform>();

        localGaniedXpValue = 0;

        for (int i = 0; i < TaskList.Count; i++)
        {

            GameObject Tl = (GameObject)Instantiate(TaskObj);
            Tl.transform.SetParent(AllTaskListObj.transform);
            Tl.transform.localScale = new Vector3(1, 1, 1);
            Tl.name = i.ToString();

            AllTaskData.Add(Tl);
            AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(false);
            AllTaskData[i].gameObject.transform.GetChild(2).gameObject.SetActive(false);
            AllTaskData[i].gameObject.transform.GetChild(3).GetComponentInChildren<Text>().text = TaskList[i];
            AllTaskData[i].gameObject.transform.GetChild(4).GetComponentInChildren<Text>().text = "+" + XpList[i] + " XP";

            localTotalXpValue += int.Parse(XpList[i]);
        }

        rT.sizeDelta = new Vector2(rT.sizeDelta.x, TaskList.Count * 50);
        localTotalXpValueText.text = "/" + localTotalXpValue.ToString();

        AllTaskData[0].gameObject.transform.GetChild(2).gameObject.SetActive(true);
        ObjectiveInfoText.text = AllTaskData[0].gameObject.transform.GetChild(3).GetComponentInChildren<Text>().text;
    }

    public void LocalXpCalculation()
    {


        for (int i = 0; i < TaskList.Count; i++)
        {
            AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(false);
            AllTaskData[i].gameObject.transform.GetChild(2).gameObject.SetActive(false);
        }

        if (OptionUnlockedTill < 1)
        {
            if ((TopicToCheck) < (TaskList.Count + 1))
            {
                localGaniedXpValue = 0;

                for (int i = 0; i < TopicToCheck - 1; i++)
                {
                    localGaniedXpValue += int.Parse(XpList[i]);
                }

                for (int i = 0; i < TopicToCheck; i++)
                {
                    AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(true);
                }

                localGainedXpValueText.text = localGaniedXpValue.ToString();

                AllTaskData[TopicToCheck - 1].gameObject.transform.GetChild(2).gameObject.SetActive(true);

                ObjectiveInfoText.text = AllTaskData[TopicToCheck - 1].gameObject.transform.GetChild(3).GetComponentInChildren<Text>().text;
            }
            else
            {
                for (int i = 0; i < TaskList.Count; i++)
                {
                    AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(true);
                    AllTaskData[i].gameObject.transform.GetChild(2).gameObject.SetActive(false);
                }

                localGainedXpValueText.text = localTotalXpValue.ToString();

                XP_Effect.text = "+" + localTotalXpValue + " XP";

                XP_Effect.transform.position = (XP_ToShow.transform.position);

                iTween.Stop(XP_Effect.gameObject);

                iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeOutElastic));

                iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", XP_Effect.transform.position.x, "y", XP_Effect.transform.position.y + 50, "z", 0, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

                iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", Main_XP_ToShow.transform.position.x, "y", Main_XP_ToShow.transform.position.y, "z", 0, "delay", 1.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

                iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "delay", 2f, "time", 0.5f, "easetype", iTween.EaseType.linear, "oncomplete", "MainXpCalculation", "oncompletetarget", this.gameObject));
            }
        }
        else
        {
            for (int i = 0; i < TaskList.Count; i++)
            {
                AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(true);
                AllTaskData[i].gameObject.transform.GetChild(2).gameObject.SetActive(false);
            }

            localGainedXpValueText.text = localTotalXpValue.ToString();
        }
    }

    public void CloseAllpanels()
    {

        ControlPanel.GetComponent<Animator>().Play("CpanelFullClose");
        LocalXP.GetComponent<Animator>().Play("LocalXpPanelClose");
        Main_Xp.GetComponent<Animator>().Play("MainXpPanelClose");
        ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelClose");
        InfoButton.GetComponent<Animator>().Play("InfoButtonClose");
        MainSlider.GetComponent<Animator>().Play("MainSliderClose");
        
        if(TasklistPanel.gameObject.transform.localScale.y!=0)
            iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));
    }

    public void OpenAllpanels()
    {
        if (OptionUnlockedTill < 1)
        {
            if (TopicToCheck <= (TaskList.Count))
            {
                LocalXP.GetComponent<Animator>().Play("LocalXpPanelOPen");
                ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen");
            }
        }
        ControlPanel.GetComponent<Animator>().Play("CpanelOpen");
        
        Main_Xp.GetComponent<Animator>().Play("MainXpPanelOpen");
       
        InfoButton.GetComponent<Animator>().Play("InfoButtonOpen");
        MainSlider.GetComponent<Animator>().Play("MainSliderOpen");
        iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "time", 0.2f, "easetype", iTween.EaseType.linear));
        iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));
    }

    public void AddXpEffect() {

        
        if (TopicToCheck <= (TaskList.Count))
        {
            // Camera.main.GetComponent<PhysicsRaycaster>().enabled = false;

            GameObject.Find("Canvas").GetComponent<CanvasGroup>().interactable = false;

           XP_Effect.GetComponent<RectTransform>().anchoredPosition = new Vector2(0,0);

            XP_Effect.text = "+"+int.Parse(XpList[TopicToCheck - 1]) + " XP";

            XP_Effect.transform.localScale = new Vector3(1, 0, 1);

           // iTween.Stop(XP_Effect.gameObject);


            iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeOutElastic));

            iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", XP_Effect.transform.position.x, "y", XP_Effect.transform.position.y + 50, "z", 0, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

            iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", XP_ToShow.transform.position.x, "y", XP_ToShow.transform.position.y, "z", 0, "delay", 1.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

            iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "delay", 2f, "time", 0.5f, "easetype", iTween.EaseType.linear, "oncomplete", "LocalXpCalculation", "oncompletetarget", this.gameObject));

            TopicToCheck++;
             Invoke("EnableRayCast", 3);
        }
        
    }

    public void EnableRayCast() {
        //Camera.main.GetComponent<PhysicsRaycaster>().enabled = true;
        GameObject.Find("Canvas").GetComponent<CanvasGroup>().interactable = true;
    }

    public void MainXpCalculation()
    {
        if (OptionUnlockedTill == 0)
        {
            OptionUnlockedTill = 1;
            MainXpValue += localTotalXpValue;

            Main_XP_ToShow.text = MainXpValue.ToString();

            LocalXP.GetComponent<Animator>().Play("LocalXpPanelClose");

            ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelClose");

            if ((ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelClose")) || (ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelFullClose")))
            {
                ControlPanel.GetComponent<Animator>().Play("CpanelOpen");
            }
        }
    }

    public void CloseInfoPanel()
    {
        sessionStart = true;
        InfoPanelBg.SetActive(false);
        iTween.ScaleTo(InformationPanel.gameObject, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));
        OpenAllpanels();
    }

    public void ContinueInfoPanel()
    {
        sessionStart = true;
        InfoPanelBg.SetActive(false);
        iTween.ScaleTo(InformationPanel.gameObject, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));
        InfoCloseBtn.transform.localScale = new Vector3(0.27f, 0.27f, 0.27f);
        InfoContinueBtn.transform.localScale = new Vector3(0, 0, 0);
        OpenAllpanels();
    }

    public void OpenInfoPanel()
    {
        sessionStart = false;
        iTween.ScaleTo(InformationPanel.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));
        InfoPanelBg.SetActive(true);
        iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "time", 0.2f, "easetype", iTween.EaseType.linear));
        iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));
        CloseAllpanels();
    }

    public void OpenTaskList() {


        if (TasklistPanel.gameObject.transform.localScale.y != 1)
        {
            iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.01f, "time", 0.2f, "easetype", iTween.EaseType.easeInSine));

            ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelClose");

            iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 180, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));

            if ((ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelOpen")))
            {
                ControlPanel.GetComponent<Animator>().Play("CpanelClose");
            }
        }
        else
        {
            iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "delay", 0.01f, "time", 0.2f, "easetype", iTween.EaseType.easeInSine));
            iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));

            ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen");

            ControlPanel.GetComponent<Animator>().Play("CpanelOpen");
        }
    }

    public void OpenOrCloseControlPanel() {

        if ((ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelClose")) || (ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelFullClose")))
        {
            ControlPanel.GetComponent<Animator>().Play("CpanelOpen");
        }
        else
        {
            ControlPanel.GetComponent<Animator>().Play("CpanelClose");
        }
    }

    public void WrongOptionSelected()
    {
        WronOptionPanel.transform.localScale = new Vector3(0, 0, 0);

        iTween.Stop(WronOptionPanel.gameObject);

        iTween.ScaleTo(WronOptionPanel.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.01f, "time", 0.2f, "easetype", iTween.EaseType.easeInSine));

        iTween.ScaleTo(WronOptionPanel.gameObject, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 1f, "time", 0.2f, "easetype", iTween.EaseType.easeInSine));

        // InfoPanelBg.SetActive(true);

        Invoke("CloseBlurBg", 1f);

        GameObject.Find("Canvas").GetComponent<CanvasGroup>().interactable = false;

        ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelClose");


        var mouse = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);

        if (mouse.x < Screen.width / 2)
        {
            // WronOptionPanel
            //print("Mouse is on left side of screen.");
            WronOptionPanel.GetComponent<RectTransform>().pivot = new Vector2(0, 0);
        }

        if (mouse.x > Screen.width / 2)
        {
            //print("Mouse is on right side of screen.");
            WronOptionPanel.GetComponent<RectTransform>().pivot = new Vector2(1, 0);
        }


        WronOptionPanel.transform.position = Input.mousePosition;
    }

    public void CloseBlurBg()
    {
        InfoPanelBg.SetActive(false);
        GameObject.Find("Canvas").GetComponent<CanvasGroup>().interactable = true;

        if (OptionUnlockedTill < 1)
        {
            if (InformationPanel.gameObject.transform.localScale.x == 0f)
            {
                ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen", 0, 0f);
            }
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (OptionUnlockedTill < 1)
        {
            if (InformationPanel.gameObject.transform.localScale.x != 1 && sessionStart)
            {
                if (TopicToCheck <= (TaskList.Count))
                {
                    iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "time", 0.2f, "easetype", iTween.EaseType.linear));
                    iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));
                    ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen");
                    ControlPanel.GetComponent<Animator>().Play("CpanelOpen");
                }
            }
        }       
    }

    public void LabelsAssignment()
    {
        GameObject Label = GameObject.Find("Labels");

        Labels = new GameObject[Label.transform.childCount];

        for (int j = 0; j < Labels.Length; j++)
        {
            Labels[j] = Label.transform.GetChild(j).gameObject;

            GameObject TempLabels = Labels[j];

            foreach (Transform ChildLabel in TempLabels.transform)
            {
                if (ChildLabel.name.Contains("SmoothLook"))
                {
                    ChildLabel.gameObject.AddComponent<SmoothLook>();
                    //ChildLabel.gameObject.AddComponent<ScaleRelativeToCamera>();

                }
            }
        }

        for (int k = 0; k < Labels.Length; k++)
        {
            string name = Labels[k].name.Remove(Labels[k].name.Length - 6);

            if (GameObject.Find(name + "") != null)
            {
                GameObject dummy = GameObject.Find(name + "").gameObject;

                Labels[k].transform.parent = dummy.transform;
                Labels[k].transform.rotation = Quaternion.identity;
            }

        }


    }

    public void AddListener(EventTriggerType eventType, Action MethodToCall, GameObject TriggerObjToAdd)
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall());
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }

    public void AddListener(EventTriggerType eventType, Action<float> MethodToCall, GameObject TriggerObjToAdd)
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall(TriggerObjToAdd.GetComponent<Slider>().value));
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }

    public void EnableOrbit()
    {
        if (!IsAR)
        {
            CamOrbit.DisableInteration = false;
        }
        else
        {
            ArOrbit.DisableInteration = false;
        }


        if (OptionUnlockedTill < 1)
        {
            if (InformationPanel.gameObject.transform.localScale.x != 1)
            {

                if (TopicToCheck <= (TaskList.Count))
                {
                    iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "time", 0.2f, "easetype", iTween.EaseType.linear));
                    iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));
                    ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen");
                }
            }
        }


        if (StartBut.GetComponentInChildren<Text>().text != "Start")
        {
            WrongOptionSelected();
            WronOptionPanel.GetComponentInChildren<Text>().text = "Tap on Reset";
        }
    }


    public void SliderMass1Change() {

        if (OptionUnlockedTill < 1)
        {
            if (TopicToCheck == 1)
            {
               // AddXpEffect();
            }
            else 
            {
                WrongOptionSelected();

                if (StartBut.GetComponentInChildren<Text>().text == "Start")
                {
                    if (SliderMass2.value == 50)
                    {
                        WronOptionPanel.GetComponentInChildren<Text>().text = "Change Mass M₂";
                    }
                    else
                    {
                        WronOptionPanel.GetComponentInChildren<Text>().text = "Tap on Start";
                    }
                }
                else {
                    WronOptionPanel.GetComponentInChildren<Text>().text = "Tap on Reset";
                }
            }
        }
    }

    public void SliderMass2Change()
    {

        if (OptionUnlockedTill < 1)
        {
            if (TopicToCheck == 2)
            {
                // AddXpEffect();
            }
            else
            {
                WrongOptionSelected();

                if (StartBut.GetComponentInChildren<Text>().text == "Start")
                {
                    if (SliderMass1.value == 50)
                    {
                        WronOptionPanel.GetComponentInChildren<Text>().text = "Change Mass M₁";

                    }
                    else
                    {
                        WronOptionPanel.GetComponentInChildren<Text>().text = "Tap on Start";
                    }
                }
                else {
                    WronOptionPanel.GetComponentInChildren<Text>().text = "Tap on Reset";
                }
            }
        }
    }
    
    public void DisableOrbit()
    {
        if (!IsAR)
        {
            CamOrbit.DisableInteration = true;
        }
        else
        {
            ArOrbit.DisableInteration = true;
        }
    }
    
    public Vector3 MidPoint(GameObject obj1, GameObject obj2)
    {
        Vector3 midp = (obj1.transform.localPosition + obj2.transform.localPosition) / 2;
        return midp;
    }

    public float Scale(GameObject obj1, GameObject obj2)
    {
        float scl = Vector3.Distance(obj1.transform.localPosition, obj2.transform.localPosition);
        return scl;
    }   

    public void RandomizePosition()
    {
        float y = UnityEngine.Random.Range(0.6f, 0.4f);

        Weight2.transform.localPosition = new Vector3(Weight2.transform.localPosition.x, y, Weight2.transform.localPosition.z);

        float dis2 = Vector3.Distance(Weight2.transform.localPosition, LenghtObj2.transform.localPosition);

        LenghtObj2.transform.localScale = new Vector3(100, dis2 * 100f - 5f, 100);
    }

   
    public void StartAction()
    {
        if (StartBut.GetComponentInChildren<Text>().text == "Start")
        {
            if (OptionUnlockedTill < 1)
            {
                if (TopicToCheck == 1)
                {
                    if (SliderMass1.value != 50)
                    {
                        ResultAction();
                        AddXpEffect();
                    }
                    else
                    {
                        WrongOptionSelected();
                        WronOptionPanel.GetComponentInChildren<Text>().text = "Change Mass M₁";
                    }
                }
                else if (TopicToCheck == 2)
                {
                    if (SliderMass2.value != 50)
                    {
                        ResultAction();
                        AddXpEffect();
                    }
                    else
                    {
                        WrongOptionSelected();
                        WronOptionPanel.GetComponentInChildren<Text>().text = "Change Mass M₂";
                    }

                }

                else
                {
                    WrongOptionSelected();
                }
            }
            else
            {
                ResultAction();
            }
        }
  
        else
        {
            StartBut.GetComponentInChildren<Text>().text = "Start";
            ResetFun();
        }

    }

    public void ResultAction() {
      
            FindAcceleration();
            StartBut.GetComponentInChildren<Text>().text = "Reset";

            if (w1 != w2)
            {
                SliderMass1.interactable = false;
                SliderMass2.interactable = false;

                Arrow.SetActive(true);
            // DescText.gameObject.SetActive(true);
                Invoke("PopUpShow", 4);
                InvokeRepeating("Timer", 0.05f, 0.01f);

            }
            else
            {
                Invoke("PopUpShow", 0.5f);
                DescriptionTex.text = "M<sub>1</sub> is equal to M<sub>2</sub>, So the system is in equilibrium.";
            }
       
    }

    public void PopUpShow() {
        DescPopUp.SetActive(true);
    }

    public void ChangeMass(float mass, GameObject weight, Text t,TextMeshPro dupT)
    {
        weight.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(0, 100 - mass);

        t.text = ((100 - weight.GetComponent<SkinnedMeshRenderer>().GetBlendShapeWeight(0)) / 10).ToString() + " gm"; ;

        //t.color = Color.cyan;
        t.resizeTextForBestFit = true;

        if (dupT.gameObject.name == "Mass1_Tex")
        {
            dupT.text = "M<sub>1</sub> = " + ((100 - weight.GetComponent<SkinnedMeshRenderer>().GetBlendShapeWeight(0)) / 10).ToString();
        }
        else {
            dupT.text = "M<sub>2</sub> = " + ((100 - weight.GetComponent<SkinnedMeshRenderer>().GetBlendShapeWeight(0)) / 10).ToString();
        }

    }

    public void FindAcceleration()
    {
        w1 = 100 - Weight1.GetComponent<SkinnedMeshRenderer>().GetBlendShapeWeight(0);
        w2 = 100 - Weight2.GetComponent<SkinnedMeshRenderer>().GetBlendShapeWeight(0);

        a = Mathf.Abs(g * ((w1 - w2) / (w1 + w2)));

        a_value.text = (g * ((w1 - w2) / (w1 + w2))).ToString("F2");

        Acc_Text.text = "a = "+a_value.text;
    }

    public void Timer()
    {
        t = t + 0.001f;

        y1 = 0.5f * a * t * t;

        if (w1 > w2)
        {
            if (Weight1.transform.localPosition.y > (-0.00095f * (Weight1.GetComponent<SkinnedMeshRenderer>().GetBlendShapeWeight(0)) + 0.1f))
            {
                Weight1.transform.localPosition = new Vector3(Weight1.transform.localPosition.x, Weight1.transform.localPosition.y - y1 * 0.01f, Weight1.transform.localPosition.z);
                Weight2.transform.localPosition = new Vector3(Weight2.transform.localPosition.x, Weight2.transform.localPosition.y + y1 * 0.01f, Weight2.transform.localPosition.z);

                Pulley.transform.localEulerAngles = new Vector3(0, 0, Pulley.transform.localEulerAngles.z - t);
            }

            //DescPopUp.SetActive(true);
            DescriptionTex.text = "M<sub>1</sub> is greater than M<sub>2</sub>, M<sub>1</sub> goes down.";
            //DescText.text = "M₁ is greater than M₂, M₁ goes down.";

            Arrow.transform.eulerAngles = new Vector3(0, 0, 180);
        }
        else if (w1 < w2)
        {

            if (Weight2.transform.localPosition.y > (-0.00095f * (Weight2.GetComponent<SkinnedMeshRenderer>().GetBlendShapeWeight(0)) + 0.1f))
            {
                Weight2.transform.localPosition = new Vector3(Weight2.transform.localPosition.x, Weight2.transform.localPosition.y - y1 * 0.01f, Weight2.transform.localPosition.z);
                Weight1.transform.localPosition = new Vector3(Weight1.transform.localPosition.x, Weight1.transform.localPosition.y + y1 * 0.01f, Weight1.transform.localPosition.z);

                Pulley.transform.localEulerAngles = new Vector3(0, 0, Pulley.transform.localEulerAngles.z + t);
            }
            DescriptionTex.text = "M<sub>2</sub> is greater than M<sub>1</sub>, M<sub>2</sub> goes down.";
           // DescText.text = "M₂ is greater than M₁, M₂ goes down.";

            Arrow.transform.eulerAngles = new Vector3(0, 0, 0);
        }
        else if (w1 == w2)
        {
            DescriptionTex.text = "M<sub>1</sub> is equal to M<sub>2</sub>, So the system is iin equilibrium.";
           // DescText.text = "M₁ is equal to M₂, So the system is iin equilibrium.";

        }


        color1 = Shadow1.GetComponent<Renderer>().material.color;
        color1.a = (-0.6f * Weight1.transform.localPosition.y + 0.8f);
        Shadow1.GetComponent<Renderer>().material.color = color1;

        color2 = Shadow2.GetComponent<Renderer>().material.color;
        color2.a = (-0.6f * Weight2.transform.localPosition.y + 0.8f);
        Shadow2.GetComponent<Renderer>().material.color = color2;

        float dis1 = Vector3.Distance(Weight1.transform.localPosition, LenghtObj1.transform.localPosition);
        float dis2 = Vector3.Distance(Weight2.transform.localPosition, LenghtObj2.transform.localPosition);

        LenghtObj1.transform.localScale = new Vector3(100, (dis1 * 100f - 5f), 100);
        LenghtObj2.transform.localScale = new Vector3(100, dis2 * 100f - 5f, 100);

    }

    public void ResetFun()
    {
        CancelInvoke("Timer");

        t = 0;

        Arrow.SetActive(false);

        StartBut.GetComponentInChildren<Text>().text = "Start";

        Shadow2.GetComponent<Renderer>().material.color = color3;
        Shadow1.GetComponent<Renderer>().material.color = color3;

        Weight1.transform.localPosition = Weight1Pos;

        RandomizePosition();

        SliderMass1.value = 50f;
        SliderMass2.value = 50f;

        SliderMass1.interactable = true;
        SliderMass2.interactable = true;
        Weight1.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(0, 100 - SliderMass1.value);
        Weight2.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(0, 100 - SliderMass1.value);

        float dis1 = Vector3.Distance(Weight1.transform.localPosition, LenghtObj1.transform.localPosition);
        float dis2 = Vector3.Distance(Weight2.transform.localPosition, LenghtObj2.transform.localPosition);

        LenghtObj1.transform.localScale = new Vector3(100, (dis1 * 100f - 5f), 100);
        LenghtObj2.transform.localScale = new Vector3(100, dis2 * 100f - 5f, 100);

        a_value.text = "0";

        Acc_Text.text = "0";

        Mass1Text.resizeTextForBestFit = false;
        Mass2Text.resizeTextForBestFit = false;

        Mass1Text.text = ((100 - Weight1.GetComponent<SkinnedMeshRenderer>().GetBlendShapeWeight(0)) / 10).ToString() + " gm";
        Mass2Text.text = ((100 - Weight2.GetComponent<SkinnedMeshRenderer>().GetBlendShapeWeight(0)) / 10).ToString() + " gm";
//
        //Mass1_Tex = 

        //DescText.gameObject.SetActive(false);

        DescPopUp.SetActive(false);
    }

   
}
