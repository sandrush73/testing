﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class practically_ar : MonoBehaviour
{
    public GameObject Prefab, PartSound, ShadowParent,Ground;
    public MeshRenderer ToCheck;
    public bool IsTracked, IsAR;
    public orbit CamOrbit;
    public AROrbitControls ArOrbit;

    // Start is called before the first frame update
    void Start()
    {
        Prefab = GameObject.Find("Man_JAN_03_V2").GetComponentInChildren<Animator>().gameObject;
        PartSound = GameObject.Find("PartSound").GetComponentInChildren<Animator>().gameObject;
        ToCheck = GameObject.Find("ShadowPlane").GetComponent<MeshRenderer>();
        ShadowParent = GameObject.Find("MAN:MAN:Man_Root_M");
        Ground = GameObject.Find("Ground");
        IsTracked = false;

        GameObject TargetForCamera = GameObject.Find("Target");

        if (TargetForCamera != null)
        {
            transform.localScale = Vector3.one * 5f;
            Camera.main.backgroundColor = ReturnColorFromHex("#E2242C");
            TargetForCamera.AddComponent<orbit>();
            CamOrbit = TargetForCamera.GetComponent<orbit>();
            

            CamOrbit.target = Camera.main.transform.parent;
            CamOrbit.target.position = new Vector3(0, 10f, 0);
            CamOrbit.maxRotUp = 15;
            CamOrbit.minRotUp = 0.1f;

            CamOrbit.zoomMin = 25;
            CamOrbit.zoomMax = 45;
            CamOrbit.zoomStart = 35;

            CamOrbit.cameraRotSideStart = 0;
            CamOrbit.cameraRotUpStart = 0;
            CamOrbit.Start();
            IsAR = false;
            BecameVisible();
        }
        else
        {
            Ground.SetActive(false);
            transform.localScale = Vector3.one * 5f;
            //transform.localScale = Vector3.one * 0.25f;
            IsAR = true;
        }

       
        
    }

    public Color ReturnColorFromHex(string ColorCode)
    {
        Color ColorToReturn = Color.white;
        bool NorCol_Converted = ColorUtility.TryParseHtmlString(ColorCode, out ColorToReturn);
        return ColorToReturn;
    }

    // Update is called once per frame
    void Update()
    {
        //ToCheck.transform.localEulerAngles =new Vector3(79,28,119);
        if (IsAR)
        {
            if (ToCheck.enabled && !IsTracked)
            {
                print(".....tracked." + ToCheck.enabled);
                BecameVisible();
                IsTracked = true;
            }
            else if (!ToCheck.enabled && IsTracked)
            {
                print(".....not." + ToCheck.enabled);
                BecameInvisible();
                IsTracked = false;


            }
        }

        ToCheck.transform.position = new Vector3(ShadowParent.transform.position.x, ToCheck.transform.position.y, ShadowParent.transform.position.z);


    }

   
    public void BecameVisible()
    {
        Prefab.GetComponent<Animator>().Play("Start");
        PartSound.GetComponent<Animator>().Play("Start");
        Prefab.GetComponent<Animator>().enabled = true;
        PartSound.GetComponent<Animator>().enabled = true;
        
    }

    public void BecameInvisible()
    {
        Prefab.GetComponent<Animator>().Play("Stop");
        //Prefab.GetComponent<Animator>().enabled = false;

        PartSound.GetComponent<Animator>().Play("Stop");
        //PartSound.GetComponent<Animator>().enabled = false;
    }
}
