﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;

public class vid76_rutherfordexperiment : MonoBehaviour, IPointerDownHandler
{
    // Start is called before the first frame update

    public Canvas C_BG;

    public orbit CamOrbit;

    public AROrbitControls ArOrbit;

    public bool IsAR;

    public GameObject MainObj, Ground;

    public int OptionUnlockedTill, CurrentTopicSelected, TopicToCheck;

    public float Sub_XP_Earned, Total_XP;

     public GameObject InstructionPanel_DD, ToDoList_Panel;

    public List<Dictionary<string, object>> CSV_data;

    public List<String> TaskList,XpList,InfoText;

    public List<GameObject> AllTaskData;

    public GameObject[] Labels;

    public Text XP_ToShow, XP_Effect, Main_XP_ToShow, localTotalXpValueText, localGainedXpValueText, ObjectiveInfoText, LGListText, IListText, AListText, 
        InfoHeadingText, GF_ResultTxt, E_valueTxt, m_valtxt;

    public GameObject TaskObj, AllTaskListObj;

    public RectTransform rT;

    public int localTotalXpValue, localGaniedXpValue,MainXpValue;

    public GameObject ControlPanel, LocalXP, Main_Xp, ObjectivePanel, InformationPanel, InfoButton, MainSlider, InfoPanelBg, TasklistPanel, WronOptionPanel, Gained_XP_ArrowBut;

    public Button InfoCloseBtn, InfoContinueBtn, Gained_XP_Panel, CpCloseBtn, IncButton, DecButton;
       
    public bool sessionStart;

    public TextAsset CSVFile;

    public AssetBundle assetBundle;
       
    public string[] CorrectMsgs = new string[] { "Nice!", "WellDone!", "Excellent!", "Correct!" };

    public Color Highlight_Color, Normal_Color, Transparent_Color;

    public Text MessageT, DescText;

    //......................

    public GameObject[] DragObjects, DummyObjects,dummyColliders;

    public GameObject DoorOfBox, Table, ObservationObject, ObsBtns,Alpha, ObservationBtnClose, RutherReset;

    public static vid76_rutherfordexperiment Instance;

    public Dictionary<string, string> DescriTextDoc;

    public string[] PartsDescription;
    // public Text DescriptionText;
    // public string[] PartsDescription;
    public int instrunctionCount,stepCount;

    public Image LabelImage, ObservationBtn, DummyOne, DummyTwo;

    public Vector3[] initPos;

    
    public bool Lab, obs;

    public GameObject Effects;


    void Start()
    {
        LoadFromAsssetBundle();                     

        MainObj = this.gameObject;

        C_BG = GameObject.Find("Canvas_BG").GetComponent<Canvas>();

        Ground = GameObject.Find("Ground");

        if (!IsAR)
        {
            CamOrbit = Camera.main.GetComponentInParent<orbit>();

            C_BG.transform.parent = Camera.main.transform;
            C_BG.GetComponentInChildren<RawImage>().enabled = true;
            C_BG.renderMode = RenderMode.ScreenSpaceCamera;

            C_BG.worldCamera = Camera.main;

            CamOrbit.target = Camera.main.transform.parent;

            CamOrbit.target.position = new Vector3(0, 0.1f, 0);

            CamOrbit.maxRotUp = 45;
            CamOrbit.minRotUp = 1;

            CamOrbit.maxRotUp = 45;
            CamOrbit.minRotUp = 1;
            //CamOrbit.maxSideRot = 45;
            //CamOrbit.minSideRot = 180;

            CamOrbit.zoomMin = 0.5f;
            CamOrbit.zoomMax = 3;
            CamOrbit.zoomStart = 1f;
            CamOrbit.pinchSpeed = 0.5f;
            CamOrbit.cameraRotUpStart = 15;
            CamOrbit.Start();
        }
        else
        {
            Ground.SetActive(false);

            C_BG.gameObject.SetActive(false);

            ArOrbit = MainObj.transform.parent.gameObject.GetComponent<AROrbitControls>();
        }

        sessionStart = false;

        //OptionUnlockedTill = 0;
        //TopicToCheck = 1;
        //MainXpValue = 10;


        

        ReadingDataFromCSVFile();
        ReadingXps();
        FindingObjects();
        LabelsAssignment();
        TaskListCreation();
        AssigningClickEvents();
        
        AssignInitialValues();

        Instance = this;
        instrunctionCount = 0;
        PartsDescription = new string[5];

        //if (OptionUnlockedTill < 1)
        //{
        //    stepCount = TopicToCheck;
        //}
        //else {
        //    stepCount = 0;
        //}
        stepCount = TopicToCheck;

    }
    public void LoadFromAsssetBundle()
    {
        //.............................................. For Asset Bundle ...........................................................................................

        assetBundle = AssetBundle.LoadFromFile(PlayerPrefs.GetString("AssetBundleUrl"));

        CSVFile = assetBundle.LoadAsset(PlayerPrefs.GetString("LibraryName") + "CSV", typeof(TextAsset)) as TextAsset;
        ////......................................................................................................................TextAsset descriptionTextAsset = assetBundle.LoadAsset("Description") as TextAsset;
        //Debug.Log(descriptionTextAsset.text);
        TextAsset descriptionTextAsset = assetBundle.LoadAsset("Description", typeof(TextAsset)) as TextAsset;

        Dictionary<string, string> descDictionary = new Dictionary<string, string>();
        string[] fLines = Regex.Split(descriptionTextAsset.text, "\n");
        //Debug.Log(fLines.Length);
        for (int i = 0; i < fLines.Length; i++)
        {
            //Debug.Log("flines......."+fLines[i]);
            string[] split = fLines[i].Split(':');
            // Debug.Log("splits......"+split[1]);
            descDictionary.Add(split[0], split[1]);
        }
        DescriTextDoc = descDictionary;

        //.............................................. For Testing in editor or APK .................................................................................

        //CSVFile = Resources.Load("vid76_rutherfordexperimentCSV") as TextAsset;

        //TextAsset descriptionTextAsset = Resources.Load<TextAsset>("Description") as TextAsset;
        ////Debug.Log(descriptionTextAsset.text);
        //Dictionary<string, string> descDictionary = new Dictionary<string, string>();
        //string[] fLines = Regex.Split(descriptionTextAsset.text, "\n");
        ////Debug.Log(fLines.Length);
        //for (int i = 0; i < fLines.Length; i++)
        //{
        //    //Debug.Log("flines......."+fLines[i]);
        //    string[] split = fLines[i].Split(':');
        //    // Debug.Log("splits......"+split[1]);
        //    descDictionary.Add(split[0], split[1]);
        //}
        //DescriTextDoc = descDictionary;


        Camera.main.gameObject.AddComponent<PhysicsRaycaster>();

        Camera.main.backgroundColor = Color.black;

        GameObject TargetForCamera = GameObject.Find("Target");

        if (TargetForCamera != null)
        {
            TargetForCamera.AddComponent<orbit>();
            CamOrbit = TargetForCamera.GetComponent<orbit>();
            IsAR = false;
            
        }
        else
        {
            this.transform.parent.gameObject.AddComponent<AROrbitControls>();
            ArOrbit = this.transform.parent.gameObject.GetComponent<AROrbitControls>();
            IsAR = true;
        }
    }

    public void ReadingDataFromCSVFile()
    {
        TaskList = new List<string>();
        XpList = new List<string>();
        InfoText = new List<string>();

        AllTaskData = new List<GameObject>();

        CSV_data = CSVReader.Read(CSVFile);
        
        for (var i = 0; i < CSV_data.Count; i++)
        {
            if (CSV_data[i]["TaskList"].ToString() != "" && CSV_data[i]["TaskList"].ToString() != null)
            {
                TaskList.Add(CSV_data[i]["TaskList"].ToString());
            }

            //if (CSV_data[i]["XPList"].ToString() != "" && CSV_data[i]["XPList"].ToString() != null)
            //{
            //    XpList.Add(CSV_data[i]["XPList"].ToString());
            //}

            if (CSV_data[i]["InfoText"].ToString() != "" && CSV_data[i]["InfoText"].ToString() != null)
            {
                InfoText.Add(CSV_data[i]["InfoText"].ToString());
            }
        }
    }
    public int totXp, loclXp;

    public void ReadingXps()
    {

        totXp = PlayerPrefs.GetInt("TotalXP");
        loclXp = PlayerPrefs.GetInt("MaxEarnings");

        if (loclXp != 0)
        {
            OptionUnlockedTill = 0;

        }
        else
        {
            OptionUnlockedTill = 1;

        }

        TopicToCheck = 1;

        MainXpValue = totXp;

        int t = 0;
        int p = 0;

        for (int k = 0; k < TaskList.Count; k++)
        {
            p = loclXp / TaskList.Count;
            XpList.Add(p.ToString());
            t = t + p;
        }

        if (t != loclXp)
        {
            XpList[XpList.Count - 1] = (p + (loclXp - t)).ToString();
        }
    }
    public float result;

    public void GetResult()
    {
        result = ((float)(TopicToCheck - 1) / (float)(TaskList.Count)) * 100;
        Camera.main.SendMessage("UnloadActually", result);
    }
    public void FindingObjects() {

        InformationPanel = GameObject.Find("InformationPanel");

        InfoCloseBtn = GameObject.Find("InfoCloseBtn").GetComponent<Button>();

        InfoContinueBtn = GameObject.Find("InfoContinueBtn").GetComponent<Button>();

        InfoPanelBg = GameObject.Find("InfoPanelBg");
        
        XP_ToShow = GameObject.Find("localGainedXpValueText").GetComponent<Text>();

        XP_Effect = GameObject.Find("Effect_Text").GetComponent<Text>();

        Main_XP_ToShow = GameObject.Find("Main_XP_Value").GetComponent<Text>();

        localGainedXpValueText = GameObject.Find("localGainedXpValueText").GetComponent<Text>();

        localTotalXpValueText = GameObject.Find("GainedXP_LocalTotalXp").GetComponent<Text>();

        ObjectiveInfoText = GameObject.Find("ObjectiveInfoText").GetComponent<Text>();

        TaskObj = GameObject.Find("TaskObj");

        AllTaskListObj = GameObject.Find("AllTaskList");

        ControlPanel = GameObject.Find("ControlPanel");
        LocalXP = GameObject.Find("LocalXP");
        Main_Xp = GameObject.Find("Main_Xp");
        ObjectivePanel = GameObject.Find("ObjectivePanel");
        InformationPanel = GameObject.Find("InformationPanel");
        InfoButton = GameObject.Find("InfoButton");
        MainSlider = GameObject.Find("MainSlider");
        WronOptionPanel = GameObject.Find("WrongPanel");

        Gained_XP_Panel = GameObject.Find("Gained_XP_Panel").GetComponent<Button>();
        TasklistPanel = GameObject.Find("TasklistPanel");
        CpCloseBtn = GameObject.Find("CpCloseBtn").GetComponent<Button>();

        InfoHeadingText = GameObject.Find("InfoHeadingText").GetComponent<Text>();
        LGListText = GameObject.Find("LGListText").GetComponent<Text>();
        IListText = GameObject.Find("IListText").GetComponent<Text>();
        AListText = GameObject.Find("AListText").GetComponent<Text>();

        Gained_XP_ArrowBut = GameObject.Find("Gained_XP_ArrowBut");
        //......................................................

        Effects = GameObject.Find("Effects");

        MainObj = GameObject.Find("Activity");

        ObservationObject = GameObject.Find("Observation");

        ObsBtns = GameObject.Find("PanelObservations");
        
        GameObject Target = GameObject.Find("Target");
        //DescriptionText = GameObject.Find("DescriptionText").GetComponent<Text>();

        LabelImage = GameObject.Find("ActivateLabels").GetComponent<Image>();

        ObservationBtn = GameObject.Find("Observationsbtn").GetComponent<Image>();

        Alpha = GameObject.Find("Alpha Array");        

        DoorOfBox = GameObject.Find("DoorOfBox");

        DragObjects = new GameObject[GameObject.Find("DragableObjects").transform.childCount];

        DummyObjects = new GameObject[DragObjects.Length];

        dummyColliders = new GameObject[DragObjects.Length];

        initPos = new Vector3[DragObjects.Length];


        Table = GameObject.Find("phy_table");



        // Invoke("ChangeInstrunction", 2f);

        bool NorCol_Converted = ColorUtility.TryParseHtmlString("#FFFFFF", out Normal_Color);

        bool HighCol_Converted = ColorUtility.TryParseHtmlString("#A28C8C", out Highlight_Color);

        // DummyOne = GameObject.Find("DummyOne").GetComponent<Image>();

        // DummyTwo = GameObject.Find("DummyTwo").GetComponent<Image>();
        DoorOfBox.transform.GetChild(0).gameObject.SetActive(false);

        ObservationBtnClose = GameObject.Find("ObservationBtnClose");

        RutherReset = GameObject.Find("RutherReset");

    }
     
    public void AssigningClickEvents() {
                
        iTween.ScaleTo(InformationPanel.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

        InfoCloseBtn.onClick.AddListener(() => CloseInfoPanel());

        InfoContinueBtn.onClick.AddListener(() => ContinueInfoPanel());

        InfoButton.GetComponent<Button>().onClick.AddListener(() => OpenInfoPanel());

        Gained_XP_Panel.onClick.AddListener(() => OpenTaskList());

        CpCloseBtn.onClick.AddListener(() => OpenOrCloseControlPanel());

        //MainSlider.GetComponent<Slider>().onValueChanged.AddListener(delegate { ChangeRadius(MainSlider.GetComponent<Slider>().value, MainSlider.GetComponentInChildren<Text>()); });

        //AddListener(EventTriggerType.PointerDown, DisableOrbit, MainSlider.gameObject);

        //AddListener(EventTriggerType.PointerUp, EnableOrbit, MainSlider.gameObject);

        LabelImage.GetComponent<Button>().onClick.AddListener(() => ToggleActivateLabels());
        ObservationBtn.GetComponent<Button>().onClick.AddListener(() => ChangeMode(false));
        ObservationBtnClose.GetComponent<Button>().onClick.AddListener(() => ChangeMode(true));


        Alpha.AddComponent<rutherfordsecondcontroller>();
        DoorOfBox.transform.GetChild(0).gameObject.AddComponent<RutherColorLerp>();

        for (int i = 0; i < DragObjects.Length; i++)
        {
            DragObjects[i] = GameObject.Find("DragableObjects").transform.GetChild(i).gameObject;
            DragObjects[i].AddComponent<RutherMaindragAndDrop>();

            initPos[i] = DragObjects[i].transform.localPosition;

            DragObjects[i].GetComponent<RutherMaindragAndDrop>().num = i;

            DummyObjects[i] = GameObject.Find(DragObjects[i].name + "_Dummy");
            dummyColliders[i] =GameObject.Find(DragObjects[i].name + "_Collider");
            // DummyObjects[i].AddComponent<ColorLerp>();
            DummyObjects[i].GetComponentInChildren<Transform>().gameObject.AddComponent<RutherColorLerp>();
        }
        ObservationBtn.transform.GetChild(0).gameObject.SetActive(false);

        ObservationBtnClose.gameObject.SetActive(false);

        RutherReset.GetComponent<Button>().onClick.AddListener(() => ResetAll());

        if (OptionUnlockedTill < 1)
        {
            RutherReset.transform.localScale = new Vector3(0, 0, 0);
        }
        else {
            RutherReset.transform.localScale = new Vector3(1, 1, 1);
        }
    }

    public void AssignInitialValues()
    {
        //InfoHeadingText.text = InfoText[0];
        //LGListText.text = InfoText[1];
        //IListText.text = InfoText[2];
        //AListText.text = InfoText[3];

         if (OptionUnlockedTill > 0)
        {
            LocalXP.transform.localScale = new Vector3(0, 0, 0);
            ObjectivePanel.transform.localScale = new Vector3(0, 0, 0);
        }

      
        InfoCloseBtn.gameObject.transform.localScale = new Vector3(0, 0, 0);
        localGainedXpValueText.text = "0";
        Main_XP_ToShow.text = MainXpValue + "";
        WronOptionPanel.transform.localScale = new Vector3(0, 0, 0);


        DoorOfBox.AddComponent<RutherCloseBox>();

        // DescriptionText.text = DescriTextDoc["instr" + instrunctionCount.ToString()];
        MainObj.SetActive(true);
        //  DescriptionText.gameObject.SetActive(true);
        ObservationObject.SetActive(false);
        ObsBtns.SetActive(false);

        Lab = false;
        obs = false;

    }
    
    public void TaskListCreation() {

        rT = AllTaskListObj.transform.parent.GetComponent<RectTransform>();

        localGaniedXpValue = 0;

        for (int i = 0; i < TaskList.Count; i++)
        {
            GameObject Tl = (GameObject)Instantiate(TaskObj);
            Tl.transform.SetParent(AllTaskListObj.transform);
            Tl.transform.localScale = new Vector3(1, 1, 1);
            Tl.name = i.ToString();

            AllTaskData.Add(Tl);
            AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(false);
            AllTaskData[i].gameObject.transform.GetChild(2).gameObject.SetActive(false);
            AllTaskData[i].gameObject.transform.GetChild(3).GetComponentInChildren<Text>().text = TaskList[i];
            AllTaskData[i].gameObject.transform.GetChild(4).GetComponentInChildren<Text>().text = "+" + XpList[i] + " XP";

            localTotalXpValue += int.Parse(XpList[i]);
        }

        rT.sizeDelta = new Vector2(rT.sizeDelta.x, TaskList.Count * 50);
        localTotalXpValueText.text = "/" + localTotalXpValue.ToString();

        AllTaskData[0].gameObject.transform.GetChild(2).gameObject.SetActive(true);
        ObjectiveInfoText.text = AllTaskData[0].gameObject.transform.GetChild(3).GetComponentInChildren<Text>().text;
    }

    public void LocalXpCalculation()
    {
        for (int i = 0; i < TaskList.Count; i++)
        {
            AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(false);
            AllTaskData[i].gameObject.transform.GetChild(2).gameObject.SetActive(false);
        }

        if (OptionUnlockedTill < 1)
        {
            if ((TopicToCheck) < (TaskList.Count + 1))
            {
                localGaniedXpValue = 0;

                for (int i = 0; i < TopicToCheck - 1; i++)
                {
                    localGaniedXpValue += int.Parse(XpList[i]);
                }

                for (int i = 0; i < TopicToCheck; i++)
                {
                    AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(true);
                }

                localGainedXpValueText.text = localGaniedXpValue.ToString();

                AllTaskData[TopicToCheck - 1].gameObject.transform.GetChild(2).gameObject.SetActive(true);

                ObjectiveInfoText.text = AllTaskData[TopicToCheck - 1].gameObject.transform.GetChild(3).GetComponentInChildren<Text>().text;
            }
            else
            {
                for (int i = 0; i < TaskList.Count; i++)
                {
                    AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(true);
                    AllTaskData[i].gameObject.transform.GetChild(2).gameObject.SetActive(false);
                }

                localGainedXpValueText.text = localTotalXpValue.ToString();

                XP_Effect.text = "+" + localTotalXpValue + " XP";

                XP_Effect.transform.position = (XP_ToShow.transform.position);

                iTween.Stop(XP_Effect.gameObject);

                iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeOutElastic));

                iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", XP_Effect.transform.position.x, "y", XP_Effect.transform.position.y + 50, "z", 0, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

                iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", Main_XP_ToShow.transform.position.x, "y", Main_XP_ToShow.transform.position.y, "z", 0, "delay", 1.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

                iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "delay", 2f, "time", 0.5f, "easetype", iTween.EaseType.linear, "oncomplete", "MainXpCalculation", "oncompletetarget", this.gameObject));
            }
        }
        else
        {
            for (int i = 0; i < TaskList.Count; i++)
            {
                AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(true);
                AllTaskData[i].gameObject.transform.GetChild(2).gameObject.SetActive(false);
            }

            localGainedXpValueText.text = localTotalXpValue.ToString();
        }
    }

    public void CloseAllpanels()
    {

        ControlPanel.GetComponent<Animator>().Play("CpanelFullClose");
        LocalXP.GetComponent<Animator>().Play("LocalXpPanelClose");
        Main_Xp.GetComponent<Animator>().Play("MainXpPanelClose");
        ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelClose");
        InfoButton.GetComponent<Animator>().Play("InfoButtonClose");
        MainSlider.GetComponent<Animator>().Play("MainSliderClose");
        
        if(TasklistPanel.gameObject.transform.localScale.y!=0)
            iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));
    }

    public void OpenAllpanels()
    {
        if (OptionUnlockedTill< 1){
            if (TopicToCheck <= (TaskList.Count))
            {
                LocalXP.GetComponent<Animator>().Play("LocalXpPanelOPen");
                ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen");
            }
        }
        ControlPanel.GetComponent<Animator>().Play("CpanelOpen");
        
        Main_Xp.GetComponent<Animator>().Play("MainXpPanelOpen");
       
        InfoButton.GetComponent<Animator>().Play("InfoButtonOpen");
        MainSlider.GetComponent<Animator>().Play("MainSliderOpen");
        iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "time", 0.2f, "easetype", iTween.EaseType.linear));
        iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));
    }

    public void AddXpEffect() {

        
        if (TopicToCheck <= (TaskList.Count))
        {
            // Camera.main.GetComponent<PhysicsRaycaster>().enabled = false;

            GameObject.Find("Canvas").GetComponent<CanvasGroup>().interactable = false;

            XP_Effect.GetComponent<RectTransform>().anchoredPosition = new Vector2(0,0);

            XP_Effect.text = "+"+int.Parse(XpList[TopicToCheck - 1]) + " XP";

            XP_Effect.transform.localScale = new Vector3(1, 0, 1);

            iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeOutElastic));

            iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", XP_Effect.transform.position.x, "y", XP_Effect.transform.position.y + 50, "z", 0, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

            iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", XP_ToShow.transform.position.x, "y", XP_ToShow.transform.position.y, "z", 0, "delay", 1.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

            iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "delay", 2f, "time", 0.5f, "easetype", iTween.EaseType.linear, "oncomplete", "LocalXpCalculation", "oncompletetarget", this.gameObject));

            TopicToCheck++;
            Invoke("EnableRayCast", 3);
        }
        
    }

    public void EnableRayCast() {
        //Camera.main.GetComponent<PhysicsRaycaster>().enabled = true;
        GameObject.Find("Canvas").GetComponent<CanvasGroup>().interactable = true;
    }

    public void MainXpCalculation()
    {
        if (OptionUnlockedTill == 0)
        {
            OptionUnlockedTill = 1;
            MainXpValue += localTotalXpValue;

            Main_XP_ToShow.text = MainXpValue.ToString();

            LocalXP.GetComponent<Animator>().Play("LocalXpPanelClose");

            ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelClose");

            if ((ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelClose")) || (ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelFullClose")))
            {
                ControlPanel.GetComponent<Animator>().Play("CpanelOpen");
            }
        }
    }

    public void CloseInfoPanel()
    {
        sessionStart = true;
        InfoPanelBg.SetActive(false);
        iTween.ScaleTo(InformationPanel.gameObject, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));
        OpenAllpanels();
    }

    public void ContinueInfoPanel()
    {
        sessionStart = true;
        InfoPanelBg.SetActive(false);
        iTween.ScaleTo(InformationPanel.gameObject, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));
        InfoCloseBtn.transform.localScale = new Vector3(0.27f, 0.27f, 0.27f);
        InfoContinueBtn.transform.localScale = new Vector3(0, 0, 0);
        OpenAllpanels();
    }

    public void OpenInfoPanel()
    {
        sessionStart = false;
        iTween.ScaleTo(InformationPanel.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));
        InfoPanelBg.SetActive(true);
        iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "time", 0.2f, "easetype", iTween.EaseType.linear));
        iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));
        CloseAllpanels();
    }

    public void OpenTaskList() {


        if (TasklistPanel.gameObject.transform.localScale.y != 1)
        {
            iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.01f, "time", 0.2f, "easetype", iTween.EaseType.easeInSine));

            ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelClose");

            iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 180, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));

            if ((ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelOpen")))
            {
                ControlPanel.GetComponent<Animator>().Play("CpanelClose");
            }
        }
        else
        {
            iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "delay", 0.01f, "time", 0.2f, "easetype", iTween.EaseType.easeInSine));
            iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));

            ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen");

            ControlPanel.GetComponent<Animator>().Play("CpanelOpen");
        }
    }

    public void OpenOrCloseControlPanel() {

        if ((ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelClose")) || (ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelFullClose")))
        {
            ControlPanel.GetComponent<Animator>().Play("CpanelOpen");
        }
        else
        {
            ControlPanel.GetComponent<Animator>().Play("CpanelClose");
        }
    }

    public void WrongOptionSelected()
    {
        WronOptionPanel.transform.localScale = new Vector3(0, 0, 0);

        iTween.Stop(WronOptionPanel.gameObject);

        iTween.ScaleTo(WronOptionPanel.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.01f, "time", 0.2f, "easetype", iTween.EaseType.easeInSine));

        iTween.ScaleTo(WronOptionPanel.gameObject, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 1f, "time", 0.2f, "easetype", iTween.EaseType.easeInSine));

        // InfoPanelBg.SetActive(true);

        Invoke("CloseBlurBg", 1f);

        GameObject.Find("Canvas").GetComponent<CanvasGroup>().interactable = false;

        ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelClose");


        var mouse = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);

        if (mouse.x < Screen.width / 2)
        {
            // WronOptionPanel
            //print("Mouse is on left side of screen.");
            WronOptionPanel.GetComponent<RectTransform>().pivot = new Vector2(0, 0);
        }

        if (mouse.x > Screen.width / 2)
        {
            //print("Mouse is on right side of screen.");
            WronOptionPanel.GetComponent<RectTransform>().pivot = new Vector2(1, 0);
        }


        WronOptionPanel.transform.position = Input.mousePosition;
    }

    public void CloseBlurBg()
    {
        InfoPanelBg.SetActive(false);
        GameObject.Find("Canvas").GetComponent<CanvasGroup>().interactable = true;

        if (OptionUnlockedTill < 1)
        {
            if (InformationPanel.gameObject.transform.localScale.x == 0f)
            {
                //ObjectivePanel.GetComponent<Animator>().Play("");
                ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen", 0, 0f);
            }
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (OptionUnlockedTill < 1)
        {
            if (InformationPanel.gameObject.transform.localScale.x != 1 && sessionStart)
            {
                if (TopicToCheck <= (TaskList.Count))
                {
                    iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "time", 0.2f, "easetype", iTween.EaseType.linear));
                    iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));
                    ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen");
                    ControlPanel.GetComponent<Animator>().Play("CpanelOpen");
                }
            }
        }
    }

    public void LabelsAssignment()
    {
        GameObject Label = GameObject.Find("Labels");

        Labels = new GameObject[Label.transform.childCount];

        for (int j = 0; j < Labels.Length; j++)
        {
            Labels[j] = Label.transform.GetChild(j).gameObject;

            GameObject TempLabels = Labels[j];

            foreach (Transform ChildLabel in TempLabels.transform)
            {
                if (ChildLabel.name.Contains("SmoothLook"))
                {
                    ChildLabel.gameObject.AddComponent<SmoothLook>();
                    //ChildLabel.gameObject.AddComponent<ScaleRelativeToCamera>();
                }
            }
        }

        for (int j = 0; j < Labels.Length; j++)
        {
            string name = Labels[j].name.Remove(Labels[j].name.Length - 6);
            GameObject obj = GameObject.Find(name).gameObject;
            Labels[j].transform.parent = obj.transform;
            Labels[j].transform.rotation = Quaternion.identity;
            Labels[j].SetActive(false);

            //string name2 = Labels[j].name.Remove(Labels[j].name.Length - 6);

            TextMeshPro[] TempTextMesh = Labels[j].GetComponentsInChildren<TextMeshPro>();

            if (TempTextMesh.Length > 1)
            {
                for (int k = 0; k < TempTextMesh.Length; k++)
                {
                    if (DescriTextDoc.ContainsKey(name + "_" + k))
                        TempTextMesh[k].text = DescriTextDoc[name + "_" + k];
                }
            }
            else
            {
                if (DescriTextDoc.ContainsKey(name))
                    Labels[j].GetComponentInChildren<TextMeshPro>().text = DescriTextDoc[name];
            }
        }
    }

    public void AddListener(EventTriggerType eventType, Action MethodToCall, GameObject TriggerObjToAdd)
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall());
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }

    public void AddListener(EventTriggerType eventType, Action<float> MethodToCall, GameObject TriggerObjToAdd)
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall(TriggerObjToAdd.GetComponent<Slider>().value));
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }

    public void EnableOrbit()
    {
        if (!IsAR)
        {
            CamOrbit.DisableInteration = false;
        }
        else
        {
            ArOrbit.DisableInteration = false;
        }

        if (InformationPanel.gameObject.transform.localScale.x != 1)
        {
            if (TopicToCheck <= (TaskList.Count))
            {
                iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "time", 0.2f, "easetype", iTween.EaseType.linear));
                iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));
                ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen");
            }
        }

        //print("HI");
        //if (OptionUnlockedTill < 1)
        //{
        //    if (TopicToCheck == 1 && SliderMass1.value != 50)
        //    {
        //        AddXpEffect();
        //    }
        //    else if (TopicToCheck == 2 && SliderMass2.value != 50)
        //    {
        //        WrongOptionSelected();
        //    }
        //}

    }

    public void DisableOrbit()
    {
        if (!IsAR)
        {
            CamOrbit.DisableInteration = true;
        }
        else
        {
            ArOrbit.DisableInteration = true;
        }
    }
    
    public Vector3 MidPoint(GameObject obj1, GameObject obj2)
    {
        Vector3 midp = (obj1.transform.localPosition + obj2.transform.localPosition) / 2;
        return midp;
    }

    public float Scale(GameObject obj1, GameObject obj2)
    {
        float scl = Vector3.Distance(obj1.transform.localPosition, obj2.transform.localPosition);
        return scl;
    }

    public void ToggleActivateLabels()
    {
        if (!Lab)
        {
            Lab = true;
            for (int i = 0; i < Labels.Length; i++)
            {
                Labels[i].SetActive(true);
            }
            //LabelImage.GetComponent<Image>() = DummyTwo.GetComponent < Image>();
            //I_Image.color = Highlight_Color;
           // LabelImage = DummyTwo;
        }
        else
        {
            Lab = false;
            for (int i = 0; i < Labels.Length; i++)
            {
                Labels[i].SetActive(false);
            }
            // I_Image.color = Normal_Color;

           // LabelImage = LabelImage;
        }
    }

    public void ChangeMode(bool obs1)
    {

        if (!obs1)
        {
            if (OptionUnlockedTill < 1)
            {
                if (TopicToCheck == 6)
                {
                    //image.color = Highlight_Color;
                    MainObj.SetActive(false);
                    ObservationObject.SetActive(true);
                    ObsBtns.SetActive(true);
                    //DescriptionText.gameObject.SetActive(false);

                    AddXpEffect();
                    ObservationBtn.transform.GetChild(0).gameObject.SetActive(false);
                    ObservationBtn.gameObject.SetActive(false);
                    ObservationBtnClose.SetActive(true);
                    iTween.ScaleTo(RutherReset, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));
                    if (!IsAR)
                    {
                        Table.SetActive(false);
                        CamOrbit.Start();
                    }
                }
                else
                {
                    WrongOptionSelected();
                    WronOptionPanel.GetComponentInChildren<Text>().text = "Wrong parameter chosen";
                }

            }
            else if (Effects.activeSelf)
            {
                if (!IsAR)
                {
                    Table.SetActive(false);
                    CamOrbit.Start();
                }
                ObservationBtn.transform.GetChild(0).gameObject.SetActive(false);
                ObservationBtn.gameObject.SetActive(false);
                ObservationBtnClose.SetActive(true);
                //image.color = Highlight_Color;
                MainObj.SetActive(false);
                ObservationObject.SetActive(true);
                ObsBtns.SetActive(true);
                //DescriptionText.gameObject.SetActive(false);
                
            }
            else {
                WrongOptionSelected();
                WronOptionPanel.GetComponentInChildren<Text>().text = "Please complete the setup first";

            }
        }
        else
        {
            if (!IsAR)
            {
                CamOrbit.Start();
                Table.SetActive(true);
            }
            ObservationBtn.gameObject.SetActive(true);
            ObservationBtnClose.SetActive(false);
            //image.color = Normal_Color;
            MainObj.SetActive(true);
            ObservationObject.SetActive(false);
            ObsBtns.SetActive(false);
           // DescriptionText.gameObject.SetActive(true);
            
        }
    }

    public void ChangeInstrunction()
    {
        //AddXpEffectAddXpEffect();
        if (OptionUnlockedTill < 1)
        {
            Invoke("AddXpEffect", 0.1f);
        }
        else
        {
            stepCount++;
            instrunctionCount++;
        
        }

        if (instrunctionCount == 4)
        {
            DoorOfBox.transform.GetChild(0).gameObject.SetActive(true);
        }

        if (OptionUnlockedTill < 1)
        {

            if (instrunctionCount == 5)
            {
                ObservationBtn.transform.GetChild(0).gameObject.SetActive(true);

                iTween.ScaleTo(ObservationBtn.gameObject.transform.GetChild(0).gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.01f, "time", 0.5f, "looptype", iTween.LoopType.pingPong, "easetype", iTween.EaseType.easeInSine));
            }
        }
        // DescriptionText.text = DescriTextDoc["instr" + instrunctionCount.ToString()];

    }
    public void ResetActivate() {

       
       

    }

    public void ResetAll()
    {
        //ChangeMode(false);
        if (!IsAR) {
            CamOrbit.Start();
            Table.SetActive(true);
        }
        
        instrunctionCount = 0;
        vid76_rutherfordexperiment.Instance.stepCount = 1;

        // ChangeInstrunction();
        //if (OptionUnlockedTill < 1) {
        //    TopicToCheck = 0;
        //}

        sessionStart = true;
        //OptionUnlockedTill = 0;

        ObservationBtn.gameObject.SetActive(true);
        ObservationBtnClose.SetActive(false);
        //image.color = Normal_Color;
        MainObj.SetActive(true);
        ObservationObject.SetActive(false);
        ObsBtns.SetActive(false);
        // DescriptionText.gameObject.SetActive(true);
        
        DoorOfBox.transform.GetChild(0).gameObject.SetActive(false);

        for (int i = 0; i < DragObjects.Length; i++)
        {           
            DragObjects[i].transform.localPosition = initPos[i];
            DummyObjects[i].SetActive(true);
            dummyColliders[i].SetActive(true);

            DragObjects[i].SetActive(false);
            DragObjects[i].SetActive(true);
            DragObjects[i].GetComponent<RutherMaindragAndDrop>().Start();

            
        }

        DoorOfBox.transform.localEulerAngles = new Vector3(0,0,0);
        RutherCloseBox.Instance.Start();
        LocalXpCalculation();

        //ChangeMode(false);
    }
}
public class RutherMaindragAndDrop : MonoBehaviour
{
    public int id;
    private bool dragging = false;
    private float distance, intialGroundTouchvalue,RanSpeed;
    private Vector3 init_pos, initialRotation;
    RaycastHit hit;
    private Vector3 temp;
    private Vector3 screenSpace;
    private Vector3 offSet;
    private Camera cam;
    private GameObject Target_collder, dummyObject;
    private bool CorrectPlace;
    public orbit CamOrbit;
    public AROrbitControls ARorbit;
    public bool IsAR;
    public Vector3 relativePos, TargetPos, TargetPos_Init;
  
    public int num;

    public void Start()
    {
        CorrectPlace = false;
        cam = Camera.main.GetComponent<Camera>();
       // IsAR = Camera.main.GetComponentInParent<orbit>() ? false : true;
        dragging = false;

        //if (!IsAR)
        //{
        //    CamOrbit = Camera.main.GetComponentInParent<orbit>();

        //   // TargetPos_Init = CamOrbit.target.transform.position;
        //    //TargetPos = MidObj.transform.position;
         

        //}
        //else
        //{
        //    //CamOrbit = ObjectController.Instance.MainObj.transform.parent.gameObject.AddComponent<AROrbitControls>();
        //    //  CamOrbit = ObjectController.Instance.MainObj.transform.parent.gameObject.AddComponent<AROrbitControls>();
        //    ARorbit = vid76_rutherfordexperiment.Instance.MainObj.transform.parent.gameObject.GetComponent<AROrbitControls>();
        //}

        Target_collder = GameObject.Find(gameObject.name + "_Collider");
        dummyObject = GameObject.Find(gameObject.name + "_Dummy");

        //initialRotation = transform.eulerAngles;

        Target_collder.SetActive(false);
        dummyObject.SetActive(false);

        gameObject.GetComponent<EventTrigger>().triggers.Clear();


        EventTrigger.Entry PointerDownentry = new EventTrigger.Entry();
        PointerDownentry.eventID = EventTriggerType.PointerDown;
        PointerDownentry.callback.AddListener((data) => { MouseDown(); });        

        gameObject.GetComponent<EventTrigger>().triggers.Add(PointerDownentry);

        EventTrigger.Entry PointerUpentry = new EventTrigger.Entry();
        PointerUpentry.eventID = EventTriggerType.PointerUp;
        PointerUpentry.callback.AddListener((data) => { MouseUp(); });

        gameObject.GetComponent<EventTrigger>().triggers.Add(PointerUpentry);


        init_pos = transform.position;

        RanSpeed = UnityEngine.Random.RandomRange(0.7f, 1f);

        CancelInvoke("EffecRot");
        InvokeRepeating("EffecRot", 0.1f, 0.01f);
    }

    void MouseDown()
    {
        if (!CorrectPlace)
        {
            //orbit.orbit_enabled = false;
            CancelInvoke("EffecRot");
            init_pos = transform.position;
            initialRotation = transform.eulerAngles;

            Target_collder.SetActive(true);
            this.GetComponent<Collider>().enabled = false;
            dummyObject.SetActive(true);

            DisableOrbit();

            screenSpace = cam.WorldToScreenPoint(transform.position);
            offSet = transform.position - cam.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenSpace.z));

            Vector3 curScreenSpace = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenSpace.z);
            Vector3 curPosition = cam.ScreenToWorldPoint(curScreenSpace) + offSet;

            intialGroundTouchvalue = curPosition.y;
            dragging = true;
        }
    }

    void MouseUp()
    {

        if (vid76_rutherfordexperiment.Instance.stepCount == (num + 1))
        {
            if (!CorrectPlace)
            {

                Ray ray = cam.ScreenPointToRay(Input.mousePosition);

                if (Physics.Raycast(ray, out hit, 100))
                {

                    if (hit.collider.name == Target_collder.name)
                    {
                        CancelInvoke("EffecRot");
                        CorrectPlace = true;
                        this.GetComponent<Collider>().enabled = false;
                        vid76_rutherfordexperiment.Instance.ChangeInstrunction(); 
                    }
                    else
                    {
                        CancelInvoke("EffecRot");
                        vid76_rutherfordexperiment.Instance.WrongOptionSelected();
                        vid76_rutherfordexperiment.Instance.WronOptionPanel.GetComponentInChildren<Text>().text = "Wrong place chosen";
                        this.GetComponent<Collider>().enabled = true;
                    }

                }
                else
                {
                    this.GetComponent<Collider>().enabled = true;
                  

                }
                //CancelInvoke("EffecRot");
                // InvokeRepeating("EffecRot", 0.1f, 0.01f);
                //ObjectController.Instance.WrongOptionSelected();
                //ObjectController.Instance.WronOptionPanel.GetComponentInChildren<Text>().text = "Wrong object chosen";
                InvokeRepeating("PlacemnetAdjustment", 0.01f, 0.01f);
                this.GetComponent<Collider>().enabled = true;
                Target_collder.SetActive(false);
                dummyObject.SetActive(false);
                // orbit.orbit_enabled = true;
                EnableOrbit();
                dragging = false;
            }
        }
        else {
            EnableOrbit();
            this.GetComponent<Collider>().enabled = true;
            Target_collder.SetActive(false);
            dummyObject.SetActive(false);
            dragging = false;
            InvokeRepeating("PlacemnetAdjustment", 0.01f, 0.01f);
            vid76_rutherfordexperiment.Instance.WrongOptionSelected();
            vid76_rutherfordexperiment.Instance.WronOptionPanel.GetComponentInChildren<Text>().text = "Wrong object chosen";
            //ObjectController.Instance.WronOptionPanel.GetComponentInChildren<Text>().text = "Instant Karma";
        }

    }

    bool CheckCondition()
    {

        bool b = false;
        int i = 0;

        while (i < vid76_rutherfordexperiment.Instance.DragObjects.Length - 1)
        {
            //if (ObjectController.Instance.DragObjects[i].transform.localPosition == ObjectController.Instance.DummyObjects[i].transform.localPosition)

            if (vid76_rutherfordexperiment.Instance.DragObjects[i].transform.localPosition == vid76_rutherfordexperiment.Instance.DummyObjects[i].transform.localPosition)
            {
                b = true;
            }
            else { b = false; }
            i++;
        }
        return b;
    }


    void PlacemnetAdjustment()
    {
            if (CorrectPlace)
            {
                if (transform.position != Target_collder.transform.position)
                {
                    transform.position = Vector3.Lerp(transform.position, Target_collder.transform.position, 0.1f);
                    transform.eulerAngles = Vector3.Lerp(transform.eulerAngles, Target_collder.transform.eulerAngles, 0.1f);
                }
                else
                {
                    CancelInvoke("PlacemnetAdjustment");
                }
            }
            else
            {
                if (transform.position != init_pos)
                {

                    if (vid76_rutherfordexperiment.Instance.IsAR)
                    {
                        transform.position = init_pos;
                        transform.eulerAngles = initialRotation;
                    }
                    else
                    {
                        transform.position = Vector3.Lerp(transform.position, init_pos, 0.1f);
                        transform.eulerAngles = Vector3.Lerp(transform.eulerAngles, initialRotation, 0.1f);
                    }
                }
                else
                {
                    CancelInvoke("EffecRot");
                    InvokeRepeating("EffecRot", 0.01f, 0.01f);
                    CancelInvoke("PlacemnetAdjustment");
                }
            }
    }
    
    void Update()
    {
        if (dragging)
        {
            Vector3 curScreenSpace = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenSpace.z);
            Vector3 curPosition = cam.ScreenToWorldPoint(curScreenSpace) + offSet;
            //curPosition = new Vector3 (curPosition.x, Mathf.Clamp (curPosition.y, intialGroundTouchvalue+GroundTouchValue, 100f), curPosition.z);
            transform.position = curPosition;
        }
    }

    public void EffecRot()
    {
        //if (!vid76_rutherfordexperiment.Instance.IsAR)
        //{
            //vid76_rutherfordexperiment.Instance.MainObj.transform.parent.localScale.x*
             //transform.localPosition = new Vector3(transform.localPosition.x, Mathf.PingPong(Time.time, RanSpeed) * 0.05f , transform.localPosition.z);
             //transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, transform.localEulerAngles.y + 0.5f, transform.localEulerAngles.z);
        //}
    }

    public void EnableOrbit()
    {
        vid76_rutherfordexperiment.Instance.EnableOrbit();
    }

    public void DisableOrbit()
    {
        vid76_rutherfordexperiment.Instance.DisableOrbit();
    }
}

public class RutherColorLerp : MonoBehaviour
{
    Color lerpeColor = Color.red;
    Vector3 intPos;
    
    private void Start()
    {
        intPos = transform.localPosition;
    }
    void Update()
    {
        //lerpeColor = Color.Lerp(new Color(1, 0, 0, 0.5f), new Color(0, 1, 0, 0.5f), Mathf.PingPong(Time.time, 0.75f));
        //this.GetComponent<Renderer>().material.color = lerpeColor;

        transform.localPosition = new Vector3(transform.localPosition.x, Mathf.PingPong(Time.time, 1f) *0.2f + intPos.y, transform.localPosition.z);
    }
}

public class RutherCloseBox : MonoBehaviour
{
    private Vector3 InitRot;
    private float Rot;
    private bool clkCheck;

    // public CloseBox 
    public static RutherCloseBox Instance;

    public void Awake()
    {
        Instance = this;

    }
    public void Start()
    {
        clkCheck = false;
        InitRot = gameObject.transform.localEulerAngles;
        vid76_rutherfordexperiment.Instance.Effects.SetActive(false);

        EventTrigger.Entry PointerDownentry = new EventTrigger.Entry();
        PointerDownentry.eventID = EventTriggerType.PointerDown;
        PointerDownentry.callback.AddListener((data) => { MouseDown(); });

        gameObject.GetComponent<EventTrigger>().triggers.Add(PointerDownentry);       
    }



    void MouseDown()
    {
        if (vid76_rutherfordexperiment.Instance.stepCount == 5)
        {
            clkCheck = true;
            vid76_rutherfordexperiment.Instance.ChangeInstrunction();
            Rot = InitRot.y;
            InvokeRepeating("Close", 0.01f, 0.01f);
            vid76_rutherfordexperiment.Instance.Effects.SetActive(true);
            this.transform.GetChild(0).gameObject.SetActive(false);
        }
    }
    void Close()
    {
        if (clkCheck)
        {
            Rot = Mathf.Lerp(Rot, 90f, Time.deltaTime * 1f);
            gameObject.transform.localEulerAngles = new Vector3(InitRot.x, InitRot.y, Rot);
        }

        if (Rot > 89.9f)
        {
            CancelInvoke("Close");
        }
    }

}

//......................................second Scene....................................
public class rutherfordsecondcontroller : MonoBehaviour
{
    // Start is called before the first frame update

    public GameObject[] particleObjects;
    public Animator[] Anims;
    public Image[] ObservationButtons;

    public string[] names = new string[] { "A", "B", "C", "All" };

    void Start()
    {
        GameObject Alpha = GameObject.Find("Alpha Array");
        
        particleObjects = new GameObject[Alpha.transform.childCount];

        Anims = new Animator[Alpha.transform.childCount];

        for (int j = 0; j < particleObjects.Length; j++)
        {
            particleObjects[j] = Alpha.transform.GetChild(j).gameObject;
            Anims[j] = particleObjects[j].GetComponent<Animator>();
        }

        GameObject btnsList = GameObject.Find("PanelObservations");
        ObservationButtons = new Image[btnsList.transform.childCount];

        for (int j = 0; j < ObservationButtons.Length; j++)
        {
            int k = j;
            ObservationButtons[k] = btnsList.transform.GetChild(k).gameObject.GetComponent<Image>();
            ObservationButtons[k].GetComponent<Button>().onClick.AddListener(() => toggle(names[k]));
        }

    }

    public void ResetBtn()
    {

        for (int j = 0; j < Anims.Length; j++)
        {
            int i = j;
            Anims[i].Play("Particle_Play", 0, 0.001f);
        }
    }

    // Update is called once per frame
    public void toggle(string name)
    {

        if (name == "A")
        {

            //particleObjects[4].SetActive(true);

            Anims[4].Play("Particle_Play", 0, 0.001f);

        }
        else if (name == "B")
        {


            Anims[0].Play("Particle_Play", 0, 0.001f);
            Anims[1].Play("Particle_Play", 0, 0.001f);

        }
        else if (name == "C")
        {
            Anims[2].Play("Particle_Play", 0, 0.001f);
            Anims[3].Play("Particle_Play", 0, 0.001f);


        }
        else if (name == "All")
        {
            ResetBtn();

        }

    }

}
