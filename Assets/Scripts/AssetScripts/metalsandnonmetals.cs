﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;
using System.Linq;
using System;
using System.Text.RegularExpressions;
using UnityEngine.SceneManagement;

public class metalsandnonmetals : MonoBehaviour
{
    public static metalsandnonmetals Instance;
    public GameObject Canvas_BG, Labels, Ground, Ironbar_Final, Aluminiumbar_Final, Copperbar_Final, Woodenbar_Final, Paperroll_Final, Glassbar_Final, metal;


    public Material MatToChange;
    public GameObject[] Label;
    public GameObject[] MainObjTopics;
    public Button[] GrpButtons;
    public GameObject dup_btn1, dup_btn2, dup_btn3;
    public GameObject Bulb3, Lighteffect, metal_and_non_metal_set;
    public GameObject Wires1, Wires2, Wires4, Wires8;




    public int CurrentStepCnt, DragDropSteps;
    //public SkinnedMeshRenderer Water_Render, Etonol_Render1,Etonol_Render;
    //public Button BurnerStart_Btn;
    //public Slider WaterSlider, EtanolSlider, MainSlider;
    //public Text Waterlevel_Text, Etanollevel_Text, TempLevel_Text;
    //public float TempLevel,time;
    //public string unit;
    //public byte decimals = 2;

    public TextAsset CSVFile;
    public Material TransparentMaterial;
    public bool IsAR, IsClicked;
    public orbit CamOrbit;
    public AROrbitControls ArOrbit;

    public Button SidePannelOpen;
    public int OptionUnlockedTill, CurrentTopicSelected, TopicToCheck, GrpUnlockedTill;
    public float Sub_XP_Earned, Total_XP;
    public Dropdown ToDoList_DD;
    public GameObject InstructionPanel_DD, ObjectivePanel, InstructionPanel, InfoPanel, GlassPanel, SidePannel, InventoryScroll;
    public Button Info_Close_Btn, Info_Cntue_Btn, Info_Open_Btn, ShowLabels_Btn, Reset_Btn;
    public List<Dictionary<string, object>> CSV_data;
    public List<String> Topics, Sub_Topics, Sub_Top_ToDoList, Sub_Top_Desc, Sub_Top_XP, LearningGoals;
    public Text XP_ToShow, XP_Effect, Main_XP_ToShow, Learninggoal_Txt, Instruction_Txt, Assumption_Txt, Heading_Txt, NumOfSteps;
    public bool IsTopicUnlocked, IsGrp_B_btn;
    public Canvas UIcanvas;
    public AssetBundle assetBundle;
    public vid_metalsUIDragAndDrop[] UIButtons;
    public Color bluepaper, redpaper;
    public GameObject Wires_Anim;

    public void Awake()
    {
        Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {


        CurrentStepCnt = 0;

        GrpUnlockedTill = 0;


        LoadFromAsssetBundle();

        LoadingData();
        ObjectivePanel.transform.localScale = new Vector3(1, 1, 1);


        //time = 0;
        //TempLevel = 0;
        //BurnerFlame.SetActive(false);
        //Effects.SetActive(false);
        //MainSlider.gameObject.SetActive(false);
        //w_Label.SetActive(false);
        //e_Label.SetActive(false);
        //alphVal = 0;
        //speed = 1;

        //WaterSlider.value = 0;
        //EtanolSlider.value = 0;
        //Condenser.transform.GetChild(0).GetComponent<MeshCollider>().enabled = false;
        //Rubbercork.transform.GetChild(0).GetComponent<MeshCollider>().enabled = false;


        //OpenSidePannel();


    }

    public void LoadingData()
    {
        ReadingDataFromCSVFile();
        OptionUnlockedTill = 1;
        ReadingXps();
        FindingGameObjects();


        AssignClickEventsToObjects();
    }


    public void LoadFromAsssetBundle()
    {
        //if (Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.Android)
        //{
        assetBundle = AssetBundle.LoadFromFile(PlayerPrefs.GetString("AssetBundleUrl"));

        CSVFile = assetBundle.LoadAsset(PlayerPrefs.GetString("LibraryName") + "CSV", typeof(TextAsset)) as TextAsset;

        //TransparentMaterial = assetBundle.LoadAsset("TransparentMaterial", typeof(Material)) as Material;
        //TransparentMaterial.shader = Shader.Find("Standard");





        //}
        //else
        //{

        string CSVName = transform.parent.gameObject.name;



        if (CSVName.Contains("Clone"))
        {



            CSVName = CSVName.Substring(0, CSVName.Length - 7);
        }



        print("casvname........." + CSVName);



       // CSVFile = Resources.Load(CSVName + "CSV") as TextAsset;
        //}







        Camera.main.gameObject.AddComponent<PhysicsRaycaster>();



        Camera.main.backgroundColor = Color.black;



        GameObject TargetForCamera = GameObject.Find("Target");



        if (TargetForCamera != null)
        {

            IsAR = false;
            TargetForCamera.AddComponent<orbit>();
            CamOrbit = TargetForCamera.GetComponent<orbit>();

            CamOrbit.zoomMax = 2f;
            CamOrbit.zoomMin = 1f;
            CamOrbit.zoomStart = 0.5f;
            CamOrbit.distance = CamOrbit.zoomStart;
            CamOrbit.transform.position = new Vector3(0, 0.12f, 0);
            CamOrbit.cameraRotUpStart = 20;
            CamOrbit.maxRotUp = 30;
            CamOrbit.minRotUp = 10;

            CamOrbit.maxSideRot = 360;
            CamOrbit.minSideRot = 360;
        }
        else
        {
            this.transform.parent.gameObject.AddComponent<AROrbitControls>();
            ArOrbit = this.transform.parent.gameObject.GetComponent<AROrbitControls>();
            IsAR = true;
        }



    }

    public void ReadingDataFromCSVFile()
    {
        Topics = new List<string>();
        Sub_Topics = new List<string>();
        Sub_Top_Desc = new List<string>();
        Sub_Top_ToDoList = new List<string>();
        Sub_Top_XP = new List<string>();
        LearningGoals = new List<string>();

        CSV_data = CSVReader.Read(CSVFile);


        for (var i = 0; i < CSV_data.Count; i++)
        {
            //print("Topics " + CSV_data[i]["Learninggoals"] + ".......... " +"ToDo " + CSV_data[i]["Topic_TodoList"]);


            if (CSV_data[i]["Topic_ToDoList"].ToString() != "" && CSV_data[i]["Topic_ToDoList"].ToString() != null)
            {
                Sub_Top_ToDoList.Add(CSV_data[i]["Topic_ToDoList"].ToString());

            }



            //if (CSV_data[i]["Top_XP"].ToString() != "" && CSV_data[i]["Top_XP"].ToString() != null)
            //{
            //    Sub_Top_XP.Add(CSV_data[i]["Top_XP"].ToString());

            //}

            if (CSV_data[i]["Learninggoals"].ToString() != "" && CSV_data[i]["Learninggoals"].ToString() != null)
            {
                LearningGoals.Add(CSV_data[i]["Learninggoals"].ToString());

            }

        }

    }
    public int totXp, loclXp, PerTopic_XP;

    public void ReadingXps()
    {

        //****************For local checking only
       // PlayerPrefs.SetInt("TotalXP", 100);//Comment this lines while taking assetbundles
        //PlayerPrefs.SetInt("MaxEarnings", 40);//Comment this lines while taking assetbundles
        //                                      //****************End of local checking only

        totXp = PlayerPrefs.GetInt("TotalXP");
        loclXp = PlayerPrefs.GetInt("MaxEarnings");

        TopicToCheck = 0;

        if (loclXp != 0)
        {
            OptionUnlockedTill = 0;

        }
        else
        {
            OptionUnlockedTill = 1;

        }





        int t = 0;
        int p = 0;

        for (int k = 0; k < Sub_Top_ToDoList.Count; k++)
        {

            p = loclXp / Sub_Top_ToDoList.Count;
            Sub_Top_XP.Add(p.ToString());
            t = t + p;
        }

        if (t != loclXp)
        {
            //  Sub_Top_XP[Sub_Top_ToDoList.Count - 1] = (p + (loclXp - t)).ToString();
        }

        Total_XP = loclXp;


    }



    public float result;

    public void GetResult()
    {
        result = ((float)(TopicToCheck - 0) / (float)(Sub_Top_ToDoList.Count)) * 100;
        Camera.main.SendMessage("UnloadActually", result);
    }

    public void FindingGameObjects()
    {
        // beaker_with_lemon_juice = GameObject.Find("beaker_with_lemon_juice");
        // BlueLitmus_Final = GameObject.Find("Blue_Final");
        // RedLitmus_Final = GameObject.Find("Red_Final");

        // Ironbar_Final = GameObject.Find("Ironbar_Final");
        // Aluminiumbar_Final = GameObject.Find("Aluminiumbar_Final");
        // Copperbar_Final = GameObject.Find(" Copperbar_Final");
        //Woodenbar_Final = GameObject.Find("Woodenabar_Final");
        // Paperroll_Final = GameObject.Find(" Paperroll_Final");
        // Glassbar_Final = GameObject.Find("Glassbar_Final");
        Bulb3 = GameObject.Find("Bulb3");
        Bulb3.GetComponent<Animator>().Play("blubof");
        Wires1 = GameObject.Find("Wires1");
        Wires1.GetComponent<Animator>().Play("wire1of");
        Wires2 = GameObject.Find("Wires2");
        Wires2.GetComponent<Animator>().Play("wire2of");
        Wires4 = GameObject.Find("Wires4");
        Wires4.GetComponent<Animator>().Play("wire4of");
        Wires8 = GameObject.Find("Wires8");
        Wires8.GetComponent<Animator>().Play("wire8of");
        Lighteffect = GameObject.Find("Lighteffect");
        Lighteffect.SetActive(false);
        metal = GameObject.Find("metal");
        metal_and_non_metal_set = GameObject.Find("metal_and_non_metal_set");
        dup_btn1 = GameObject.Find("dup_btn1");
        dup_btn2 = GameObject.Find("dup_btn2");
        dup_btn3 = GameObject.Find("dup_btn3");

        dup_btn1.SetActive(false);
        dup_btn2.SetActive(false);
        dup_btn3.SetActive(false);

        GrpButtons = GameObject.FindObjectsOfType(typeof(Button)).Select(g => g as Button).Where(g => g.name.Contains("Group_Btn_")).ToArray();
        GrpButtons = GrpButtons.OrderBy(x => x.name).ToArray();

        MainObjTopics = GameObject.FindObjectsOfType(typeof(GameObject)).Select(g => g as GameObject).Where(g => g.name.Contains("MainTopic_")).ToArray();
        MainObjTopics = MainObjTopics.OrderBy(x => x.name).ToArray();

        Label = GameObject.FindObjectsOfType(typeof(GameObject)).Select(g => g as GameObject).Where(g => g.name.Contains(" ")).ToArray();
        Label = Label.OrderBy(x => x.name).ToArray();

        Wires_Anim = GameObject.Find("Wires_Anim");

        Wires_Anim.SetActive(false);

        UIButtons = GameObject.Find("InventoryScrollView").GetComponentsInChildren<vid_metalsUIDragAndDrop>();
        Canvas_BG = GameObject.Find("Canvas_BG");
        Labels = GameObject.Find("Labels");
        // Label = new GameObject[Labels.transform.childCount];

        // Labels.SetActive(false);
        for (int i = 0; i < Labels.transform.childCount; i++)
        {
            // Label[i] = Labels.transform.GetChild(i).gameObject;

            Labels.transform.GetChild(i).Find("SmoothLook").gameObject.AddComponent<SmoothLook>();
        }


        for (int k = 0; k < Wires_Anim.transform.childCount; k++)
        {
            Wires_Anim.transform.GetChild(k).gameObject.AddComponent<metals1>();
        }

        Ground = GameObject.Find("Ground");

        if (!IsAR)
        {
            Canvas_BG.transform.parent = Camera.main.transform;
            Canvas_BG.GetComponent<Canvas>().worldCamera = Camera.main;
        }

        else
        {
            Canvas_BG.SetActive(false);
            Ground.SetActive(false);
        }
        UIcanvas = GameObject.Find("Canvas_fractionaldistillation").GetComponent<Canvas>();
        SidePannel = GameObject.Find("SidePanel");
        SidePannelOpen = GameObject.Find("PannelOpenButton").GetComponent<Button>();
        InstructionPanel_DD = GameObject.Find("DD_Instruction");
        InstructionPanel = GameObject.Find("InstructionPanel");
        InventoryScroll = GameObject.Find("InventoryScrollView");
        ToDoList_DD = GameObject.Find("ToDoList_Dropdown").GetComponent<Dropdown>();
        XP_ToShow = GameObject.Find("Gained_XP_Value").GetComponent<Text>();
        XP_Effect = GameObject.Find("Effect_Text").GetComponent<Text>();
        Main_XP_ToShow = GameObject.Find("Main_XP_Value").GetComponent<Text>();
        ObjectivePanel = GameObject.Find("ObjectivePanel");
        InstructionPanel.transform.localScale = Vector3.zero;
        InfoPanel = GameObject.Find("InformationPanel");

        Info_Close_Btn = GameObject.Find("InfoCloseBtn").GetComponent<Button>();
        Info_Cntue_Btn = GameObject.Find("InfoContinueBtn").GetComponent<Button>();
        Info_Open_Btn = GameObject.Find("InfoButton").GetComponent<Button>();
        Info_Close_Btn.gameObject.SetActive(false);
        GlassPanel = GameObject.Find("GlassEffect");
        Instruction_Txt = GameObject.Find("IListText").GetComponent<Text>();
        Assumption_Txt = GameObject.Find("AListText").GetComponent<Text>();
        Learninggoal_Txt = GameObject.Find("LGListText").GetComponent<Text>();
        Heading_Txt = GameObject.Find("InfoHeadingText").GetComponent<Text>();
        NumOfSteps = GameObject.Find("Slots_Text").GetComponent<Text>();
        ShowLabels_Btn = GameObject.Find("Labels_Show").GetComponent<Button>();
        Reset_Btn = GameObject.Find("Reset_Exp").GetComponent<Button>();


        if (OptionUnlockedTill == 0)
        {
            /*ToDoList_DD.GetComponent<RectTransform>().anchoredPosition = new Vector3(150f, -150f, 0);
            XP_ToShow.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(150f, -150f, 0);
            SidePannel.GetComponent<RectTransform>().anchoredPosition = new Vector3(-600f, 0f, 0);*/
            ObjectivePanel.transform.localScale = new Vector3(1, 0, 1);
            IsTopicUnlocked = false;
            Reset_Btn.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(-500f, -150f, 0);
        }
        else
        {
            /*ToDoList_DD.GetComponent<RectTransform>().anchoredPosition = new Vector3(-250f, -150f, 0);
            XP_ToShow.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(-250f, -150f, 0);
            SidePannel.GetComponent<RectTransform>().anchoredPosition = new Vector3(-600f, 0f, 0);*/
            ObjectivePanel.transform.localScale = new Vector3(1, 0, 1);
            XP_ToShow.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(-250f, -150f, 0);
            IsTopicUnlocked = true;
            ToDoList_DD.GetComponent<RectTransform>().anchoredPosition = new Vector3(-250f, -150f, 0);
            Reset_Btn.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(10f, -150f, 0);
        }


        Main_XP_ToShow.text = totXp.ToString();

        XP_ToShow.text = "0/" + Total_XP;

        for (int i = 0; i < Sub_Top_ToDoList.Count; i++)
        {

            ToDoList_DD.options.Add(new Dropdown.OptionData() { text = Sub_Top_ToDoList[i] });


            //List<float> TotalXP = Sub_Top_XP.Select(s => float.Parse(s)).ToList();

            //Total_XP += TotalXP[i];

            //XP_ToShow.text = "0/" + Total_XP;


            ObjectivePanel.transform.Find("ObjectiveText").GetComponent<Text>().text = Sub_Top_ToDoList[TopicToCheck];

        }

        DragDropSteps = UIcanvas.GetComponentsInChildren<vid_metalsUIDragAndDrop>().Length;

        NumOfSteps.text = DragDropSteps.ToString();

        GlassPanel.transform.localScale = Vector3.zero;
        InfoPanel.transform.localScale = Vector3.zero;
        Invoke("OpenInfoPanel", 0.2f);

        Heading_Txt.text = LearningGoals[0];
        Learninggoal_Txt.text = LearningGoals[1];
        // bluepaper = BlueLitmus_Final.transform.GetChild(1).GetComponent<MeshRenderer>().material.color;
        //redpaper = RedLitmus_Final.transform.GetChild(1).GetComponent<MeshRenderer>().material.color;

        //Instruction_Txt.text = LearningGoals[2];
        //Assumption_Txt.text = LearningGoals[3];

        Ironbar_Final.transform.GetChild(1).gameObject.SetActive(false);
        Aluminiumbar_Final.transform.GetChild(1).gameObject.SetActive(false);
        Copperbar_Final.transform.GetChild(1).gameObject.SetActive(false);
        Woodenbar_Final.transform.GetChild(1).gameObject.SetActive(false);
        Paperroll_Final.transform.GetChild(1).gameObject.SetActive(false);
        Glassbar_Final.transform.GetChild(1).gameObject.SetActive(false);

        //................................... Adding smoothlook for popUps....................................
        Ironbar_Final.transform.GetChild(1).gameObject.AddComponent<SmoothLook>();
        Aluminiumbar_Final.transform.GetChild(1).gameObject.AddComponent<SmoothLook>();
        Woodenbar_Final.transform.GetChild(1).gameObject.AddComponent<SmoothLook>();
        Copperbar_Final.transform.GetChild(1).gameObject.AddComponent<SmoothLook>();
        Paperroll_Final.transform.GetChild(1).gameObject.AddComponent<SmoothLook>();
        Glassbar_Final.transform.GetChild(1).gameObject.AddComponent<SmoothLook>();


    }
    public void LoadCurrentTopic()
    {
        for (int i = 0; i < Label.Length; i++)

        {

            Label[i].SetActive(false);
        }
        for (int i = 0; i < MainObjTopics.Length; i++)

        {

            MainObjTopics[i].SetActive(false);

        }
        MainObjTopics[GrpUnlockedTill].SetActive(true);

        for (int i = 0; i < UIButtons.Length; i++)
        {
            UIButtons[i].Restart();
            UIButtons[i].transform.SetSiblingIndex(i);

        }
        //BlueLitmus_Final.transform.GetChild(1).GetComponent<MeshRenderer>().material.color = bluepaper;
        // RedLitmus_Final.transform.GetChild(1).GetComponent<MeshRenderer>().material.color = redpaper;
        // BlueLitmus_Final.transform.GetChild(1).GetComponent<MeshRenderer>().material.color.a = 0;
        // RedLitmus_Final.transform.GetChild(1).GetComponent<MeshRenderer>().material.color.a = 0;
    }

    public void AssignClickEventsToObjects()
    {

        for (int i = 0; i < GrpButtons.Length; i++)
        {
            int j = i;
            //GrpButtons[i].onClick.AddListener(() => Selectgrp(j));
        }



        SidePannelOpen.onClick.AddListener(OpenSidePannel);

        AddListenerToEvents(EventTriggerType.PointerDown, EnableOrbit, InventoryScroll);

        AddListenerToEvents(EventTriggerType.PointerUp, DisableOrbit, InventoryScroll);

        AddListenerToEvents(EventTriggerType.PointerDown, EnableOrbit, InventoryScroll.transform.GetChild(3).gameObject);

        AddListenerToEvents(EventTriggerType.PointerUp, DisableOrbit, InventoryScroll.transform.GetChild(3).gameObject);

        ToDoList_DD.GetComponent<EventTrigger>().triggers.Clear();

        AddListenerToEvents(EventTriggerType.PointerClick, ExploringToDOList_DD, ToDoList_DD.gameObject);
        AddListenerToEvents(EventTriggerType.Select, ToDoList_DDCancel, ToDoList_DD.gameObject);

        Info_Close_Btn.onClick.AddListener(() => CloseInfoPanel(Info_Close_Btn));
        Info_Cntue_Btn.onClick.AddListener(() => CloseInfoPanel(Info_Cntue_Btn));
        Info_Open_Btn.onClick.AddListener(OpenInfoPanel);

        ShowLabels_Btn.onClick.AddListener(ShowLabels);
        Reset_Btn.onClick.AddListener(ResetExperiment);

    }
    public void AssignInitialValues()
    {


        for (int i = 0; i < Wires_Anim.transform.childCount; i++)
        {

            Wires_Anim.transform.GetChild(i).gameObject.AddComponent<metals1>().scrollSpeed = 2;

        }

        Wires_Anim.SetActive(false);

    }


    public void AddListenerToEvents(EventTriggerType eventType, Action MethodToCall, GameObject TriggerObjToAdd)
    {

        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall());
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }

    public void AddListenerToEvents(EventTriggerType eventType, Action<float> MethodToCall, GameObject TriggerObjToAdd, float p)
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall(p));
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }

    public void AddListenerToEvents(EventTriggerType eventType, Action<GameObject> MethodToCall, GameObject TriggerObjToAdd, GameObject p)
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall(p));
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }
    public void OnElementBeginDrag()
    {
        IsClicked = true;
    }

    public void OnElementEndDrag()
    {
        //print("drag ended");
        IsClicked = false;
    }

    public void EnableOrbit()
    {

        if (!IsAR)
        {
            CamOrbit.DisableInteration = false;
            //print("EnableOrbit");
        }
        else
        {
            ArOrbit.DisableInteration = false;

        }
    }

    public void DisableOrbit()
    {

        if (!IsAR)
        {
            CamOrbit.DisableInteration = true;
            //print("DisableOrbit");
        }
        else
        {
            ArOrbit.DisableInteration = true;

        }
    }


    public void OpenInfoPanel()
    {
        UIZoomIn();
        iTween.ScaleTo(InfoPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 0.5f, "easetype", iTween.EaseType.spring));
        iTween.ScaleTo(GlassPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 0.01f, "easetype", iTween.EaseType.spring));

    }

    public void CloseInfoPanel(Button CloseBtn)
    {
        UIZoomOut();
        //if (!IsTopicUnlocked)
        //{
        ObjectivePanel.transform.localScale = new Vector3(1, 0, 1);
        iTween.ScaleTo(ObjectivePanel, iTween.Hash("y", 1, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine));
        // }
        iTween.ScaleTo(InfoPanel, iTween.Hash("x", 0, "y", 0, "z", 0, "time", 0.2f, "easetype", iTween.EaseType.linear));
        iTween.ScaleTo(GlassPanel, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.2f, "time", 0.01f, "easetype", iTween.EaseType.linear));
        if (CloseBtn == Info_Cntue_Btn)
        {
            if (!IsAR)
                CamOrbit.DisableInteration = false;

            OpenSidePannel();
            Info_Close_Btn.gameObject.SetActive(true);
            Info_Cntue_Btn.gameObject.SetActive(false);
        }
        IsClicked = false;
    }

    public void ShowMessage_Panel(string TextToDisplay)
    {
        InstructionPanel.transform.Find("InstructionText").GetComponent<Text>().text = TextToDisplay;
        iTween.ScaleTo(GlassPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 0.01f, "easetype", iTween.EaseType.spring));
        iTween.ScaleTo(InstructionPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 1f, "easetype", iTween.EaseType.spring));
        ObjectivePanel.transform.localScale = new Vector3(1, 0, 1);
    }


    public void ShowInstruction_Panel(string TextToDisplay)
    {
        var mouse = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);



        if (mouse.x < Screen.width / 2)
        {
            // WronOptionPanel
            //print("Mouse is on left side of screen.");
            InstructionPanel.GetComponent<RectTransform>().pivot = new Vector2(0, 0);
        }



        if (mouse.x > Screen.width / 2)
        {
            //print("Mouse is on right side of screen.");
            InstructionPanel.GetComponent<RectTransform>().pivot = new Vector2(1, 0);
        }

        InstructionPanel.transform.position = Input.mousePosition;
        UIcanvas.GetComponent<GraphicRaycaster>().enabled = false;
        Camera.main.GetComponent<PhysicsRaycaster>().enabled = false;
        iTween.Stop(InstructionPanel.gameObject);

        InstructionPanel.transform.Find("InstructionText").GetComponent<Text>().text = TextToDisplay;
        //iTween.ScaleTo(GlassPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 0.01f, "easetype", iTween.EaseType.spring));
        iTween.ScaleTo(InstructionPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 0.3f, "easetype", iTween.EaseType.spring, "oncomplete", "CloseInstructionPanel", "oncompletetarget", this.gameObject));
        ObjectivePanel.transform.localScale = new Vector3(1, 0, 1);
    }

    public void CloseInstructionPanel()
    {
        UIcanvas.GetComponent<GraphicRaycaster>().enabled = true;
        Camera.main.GetComponent<PhysicsRaycaster>().enabled = true;
        //iTween.ScaleTo(GlassPanel, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 1f, "time", 0.01f, "easetype", iTween.EaseType.linear));
        iTween.ScaleTo(InstructionPanel, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.5f, "time", 0.3f, "easetype", iTween.EaseType.spring));
        iTween.ScaleTo(ObjectivePanel, iTween.Hash("y", 1, "time", 0.3f, "delay", 0.5f, "easetype", iTween.EaseType.easeInOutSine));
    }

    public void ToDoList_DDCancel()
    {
        if (ToDoList_DD.transform.Find("Dropdown List") != null)//&& Obj.transform.localScale.y!=1)
        {
            //print("cancelled........");
            //print("list.....cancelled........" + ToDoList_DD.transform.Find("Dropdown List").gameObject);
            ToDoList_DD.transform.Find("Arrow").transform.eulerAngles = new Vector3(0, 0, 0);
            iTween.ScaleTo(ToDoList_DD.transform.Find("Dropdown List").gameObject, iTween.Hash("y", 0, "time", 0.5f, "easetype", iTween.EaseType.spring));
            iTween.ScaleTo(ObjectivePanel, iTween.Hash("y", 1, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine));
            if (SidePannelOpen.transform.eulerAngles.z == 180)
            {
                OpenSidePannel();
            }
        }
    }

    public void ExploringToDOList_DD()
    {
        //print("ShowDD.....");
        //ToDoList_DD.transform.Find("Topic_Objective").GetComponent<Text>().text = "";

        ToDoList_DD.transform.Find("Arrow").transform.eulerAngles = new Vector3(0, 0, 180);
        GameObject DDList = ToDoList_DD.transform.Find("Dropdown List").gameObject;
        DDList.transform.GetComponent<RectTransform>().sizeDelta = new Vector2(0, 250);
        DDList.transform.localScale = new Vector3(1, 0, 1);
        iTween.ScaleTo(DDList, iTween.Hash("y", 1, "time", 0.5f, "easetype", iTween.EaseType.spring));
        //iTween.ScaleTo(ToDoList_DD.transform.Find("Topic_Objective").gameObject, iTween.Hash("y", 0, "time", 0.5f, "easetype", iTween.EaseType.spring));
        if (!IsTopicUnlocked)
            iTween.ScaleTo(ObjectivePanel, iTween.Hash("y", 0, "time", 0.5f, "delay", 0.1f, "easetype", iTween.EaseType.easeInOutSine));

        string TestStr = "";
        int CntToChangePos = 0;
        for (int i = 0; i < ToDoList_DD.options.Count; i++)
        {
            GameObject Child = GameObject.Find("Item " + i + ": " + ToDoList_DD.options[i].text);
            //Child.transform.parent.GetComponent<RectTransform>().sizeDelta = new Vector2(0,250);

            if (TestStr == ToDoList_DD.options[i].text)
            {
                Child.SetActive(false);
                Child.transform.localPosition = Child.transform.localPosition + new Vector3(0, 50 * CntToChangePos, 0);
                CntToChangePos++;
            }
            else
            {
                Child.transform.Find("XP_Text").GetComponent<Text>().text = "+" + Sub_Top_XP[i] + "XP";
                Child.transform.localPosition = Child.transform.localPosition + new Vector3(0, 50 * CntToChangePos, 0);

                if (i < TopicToCheck)
                {
                    Child.transform.Find("Item Checkmark").GetComponent<Image>().enabled = true;
                    //Child.transform.Find("XP_Text").GetComponent<Text>().color = Color.green / 2;
                    //Child.transform.Find("Item Checkmark").GetComponent<Image>().color = Color.green / 2;
                    Child.transform.Find("Item Background").GetComponent<Image>().enabled = false;
                }
                else if (i == TopicToCheck)
                {
                    Child.transform.Find("Item Background").GetComponent<Image>().enabled = true;// color = ReturnColorFromHex("#a01c65");
                                                                                                 //Child.transform.Find("Item Label").GetComponent<Text>().color = Color.white;
                }
            }
            TestStr = ToDoList_DD.options[i].text;
        }
        //if(SidePannelOpen.transform.localEulerAngles.z==0)
        //    {
        //        OpenSidePannel();

        // }
    }


    //iTween.ScaleTo(ToDoList_DD.transform.Find("Topic_Objective").gameObject, iTween.Hash("y", 0, "time", 0.5f, "easetype", iTween.EaseType.spring));
    //if (SidePannelOpen.transform.eulerAngles.z == 0)
    //{
    //    OpenSidePannel();
    //}



    public void OpenSidePannel()
    {
        if (SidePannelOpen.transform.eulerAngles.z == 180)
        {
            //iTween.MoveAdd(SidePannel, iTween.Hash("x", -400f, "time",0.5f,"islocal",true, "easetype", iTween.EaseType.linear));
            //iTween.MoveAdd(SidePannelOpen.gameObject, iTween.Hash("x", -185f, "islocal", true, "time", 0.5f, "easetype", iTween.EaseType.linear));
            //SidePannelOpen.transform.eulerAngles = new Vector3(0,0,180);
            SidePannel.GetComponent<Animator>().Play("PannelForward");
        }
        else
        {
            //iTween.MoveAdd(SidePannel, iTween.Hash("x", 400f, "time", 0.5f, "islocal", true, "easetype", iTween.EaseType.linear));
            //iTween.MoveAdd(SidePannelOpen.gameObject, iTween.Hash("x", -185f, "islocal", true, "time", 0.5f, "easetype", iTween.EaseType.linear));
            //SidePannelOpen.transform.eulerAngles = new Vector3(0, 0, 0);
            SidePannel.GetComponent<Animator>().Play("PannelBackward");
        }
    }


    public float ChangeToLocal_X(float Val)
    {
        float X_ToRet = 0;
        //print("wid........"+ UIcanvas.GetComponent<RectTransform>().rect.width);
        X_ToRet = ((UIcanvas.GetComponent<RectTransform>().rect.x) + Val);

        return X_ToRet;
    }

    public float ChangeToLocal_Y(float Val)
    {
        float Y_ToRet = 0;
        //print("wid........"+ UIcanvas.GetComponent<RectTransform>().rect.width);
        Y_ToRet = ((UIcanvas.GetComponent<RectTransform>().rect.y) + Val);

        return Y_ToRet;
    }


    public void Collect_XP(GameObject TargetPos)
    {
        if (IsTopicUnlocked)
            return;

        XP_Effect.transform.position = Camera.main.WorldToScreenPoint(TargetPos.transform.position);

        Camera.main.GetComponent<PhysicsRaycaster>().enabled = false;
        UIcanvas.GetComponent<GraphicRaycaster>().enabled = false;

        XP_Effect.text = "+" + Sub_Top_XP[TopicToCheck] + "XP";

        XP_Effect.transform.localScale = new Vector3(2, 0, 2);

        iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 2, "y", 2, "z", 2, "time", 0.5f, "easetype", iTween.EaseType.easeOutElastic));

        iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", XP_Effect.transform.position.x, "y", XP_Effect.transform.position.y + 150, "z", 0, "delay", 0.3f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

        iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", 150, "y", XP_ToShow.transform.position.y, "z", 0, "delay", 1f, "time", 1f, "easetype", iTween.EaseType.easeInSine, "oncomplete", "Add_To_XP", "oncompletetarget", this.gameObject));

    }

    public void Add_To_XP()
    {

        //print("TopicToCheck......."+ TopicToCheck+"....sub......"+Sub_Topics.Count);
        if (TopicToCheck < (Sub_Top_XP.Count))
        {
            Sub_XP_Earned += float.Parse(Sub_Top_XP[TopicToCheck]);

            XP_Effect.text = "";
            XP_ToShow.text = Sub_XP_Earned + "/" + Total_XP;

            TopicToCheck++;
            if (TopicToCheck <= (Sub_Top_XP.Count - 1))
            {
                ObjectivePanel.transform.Find("ObjectiveText").GetComponent<Text>().text = Sub_Top_ToDoList[TopicToCheck];
                ObjectivePanel.transform.localScale = new Vector3(1, 0, 1);
                iTween.ScaleTo(ObjectivePanel, iTween.Hash("y", 1, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine));
                //ToDoList_DD.transform.Find("Topic_Objective").GetComponent<Text>().text = "Objective: " + Sub_Top_ToDoList[TopicToCheck];

                Camera.main.GetComponent<PhysicsRaycaster>().enabled = true;
                UIcanvas.GetComponent<GraphicRaycaster>().enabled = true;
            }
            else
            {
                XP_Effect.transform.position = (XP_ToShow.transform.position);

                XP_Effect.text = Total_XP + "XP";

                XP_Effect.transform.localScale = new Vector3(1, 0, 1);

                iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 2, "y", 2, "z", 2, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeOutElastic));

                iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", XP_Effect.transform.position.x, "y", XP_Effect.transform.position.y + 50, "z", 0, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

                iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", Main_XP_ToShow.transform.position.x, "y", Main_XP_ToShow.transform.position.y, "z", 0, "delay", 1.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine, "oncomplete", "AddTotalToMainXP", "oncompletetarget", this.gameObject));

                iTween.MoveTo(ToDoList_DD.gameObject, iTween.Hash("x", ChangeToLocal_X(-400), "delay", 3.5f, "time", 0.5f, "islocal", true, "easetype", iTween.EaseType.easeInOutSine));

                iTween.MoveTo(XP_ToShow.transform.parent.gameObject, iTween.Hash("x", ChangeToLocal_X(-400), "delay", 3.5f, "time", 0.5f, "islocal", true, "easetype", iTween.EaseType.easeInOutSine));

                iTween.MoveTo(Reset_Btn.transform.parent.gameObject, iTween.Hash("x", ChangeToLocal_X(10), "delay", 5f, "time", 0.5f, "islocal", true, "easetype", iTween.EaseType.easeInOutSine));



                //print("label......" + LabelImage.GetComponent<RectTransform>().anchoredPosition+".......explode...."+ ExplodeSlider.GetComponent<RectTransform>().anchoredPosition);


                //iTween.MoveTo(ValueDisplaySlider.gameObject, iTween.Hash("x", ChangeToLocal_X ( 150), "delay", 0.7f, "time", 0.5f, "islocal", true, "easetype", iTween.EaseType.easeInOutSine));
            }
        }

    }

    public void AddTotalToMainXP()
    {
        if (OptionUnlockedTill < 1)
        {
            OptionUnlockedTill++;

            float MainXp = float.Parse(Main_XP_ToShow.text);
            MainXp += Total_XP;
            Main_XP_ToShow.text = MainXp.ToString();
            //ObjectivePanel.transform.Find("ObjectiveText").GetComponent<Text>().text = "YaY!......Next topic unlocked";
            XP_Effect.text = "";
            IsTopicUnlocked = true;
            iTween.ScaleTo(ObjectivePanel, iTween.Hash("y", 0, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine));
            Camera.main.GetComponent<PhysicsRaycaster>().enabled = true;
            UIcanvas.GetComponent<GraphicRaycaster>().enabled = true;

            if (OptionUnlockedTill == 1)
            {
                iTween.ScaleTo(ObjectivePanel, iTween.Hash("y", 1, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine));
                ObjectivePanel.transform.Find("ObjectiveText").GetComponent<Text>().text = "Drag any element in the circuit";
            }

        }


    }
    public void UIReset()
    {
        ObjectivePanel.GetComponent<RectTransform>().anchoredPosition = new Vector3(0f, 5f, 0);



        //ValueDisplaySlider.transform.GetComponent<RectTransform>().anchoredPosition = new Vector3(-20f, 0f, 0);
        Info_Open_Btn.transform.GetComponent<RectTransform>().anchoredPosition = new Vector3(-37f, -150f, 0);




        SidePannelOpen.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(135f, -31f, 0);
        InventoryScroll.transform.GetComponent<RectTransform>().anchoredPosition = new Vector3(-79f, -79f, 0);


        //print("side......" + SidePannelOpen.transform.position+"....local....."+ SidePannelOpen.transform.localPosition);
        if (!IsTopicUnlocked)
        {
            ToDoList_DD.transform.GetComponent<RectTransform>().anchoredPosition = new Vector3(135f, -150f, 0);
            XP_ToShow.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(135f, -150f, 0);
            Reset_Btn.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(-300f, -150f, 0);


        }
        else
        {
            ToDoList_DD.transform.GetComponent<RectTransform>().anchoredPosition = new Vector3(-250f, -150f, 0);
            XP_ToShow.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(-250f, -150f, 0);
            Reset_Btn.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(10, -150f, 0);


        }



        //Main_XP_ToShow.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(-135f, -60f, 0);



    }



    public void UIZoomOut()
    {
        if (ObjectivePanel.GetComponent<RectTransform>().anchoredPosition.y == 5)
            return;



        UIReset();



        iTween.MoveFrom(Info_Open_Btn.gameObject, iTween.Hash("x", Info_Open_Btn.transform.localPosition.x + 250, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveFrom(SidePannelOpen.transform.parent.gameObject, iTween.Hash("x", SidePannelOpen.transform.parent.localPosition.x - 1000f, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        //iTween.MoveFrom(ValueDisplaySlider.gameObject, iTween.Hash("x", ValueDisplaySlider.transform.localPosition.x + 250, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveFrom(ObjectivePanel, iTween.Hash("y", ObjectivePanel.transform.localPosition.y - 1000, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveFrom(InventoryScroll, iTween.Hash("x", InventoryScroll.transform.localPosition.x + 1000f, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));



        iTween.MoveFrom(ToDoList_DD.gameObject, iTween.Hash("x", ToDoList_DD.transform.localPosition.x - 400, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveFrom(XP_ToShow.transform.parent.gameObject, iTween.Hash("x", XP_ToShow.transform.parent.localPosition.x - 400, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveFrom(Reset_Btn.transform.parent.gameObject, iTween.Hash("x", XP_ToShow.transform.parent.localPosition.x - 400, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));



        //iTween.MoveFrom(Main_XP_ToShow.transform.parent.gameObject, iTween.Hash("x", Main_XP_ToShow.transform.parent.localPosition.x + 550, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
    }





    public void UIZoomIn()
    {
        if (ObjectivePanel.GetComponent<RectTransform>().anchoredPosition.y != 5)
            return;



        UIReset();
        //print("side......" + SidePannelOpen.transform.position);
        iTween.MoveAdd(ObjectivePanel, iTween.Hash("y", ObjectivePanel.transform.localPosition.y - 1000, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveAdd(Info_Open_Btn.gameObject, iTween.Hash("x", Info_Open_Btn.transform.localPosition.x + 250, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveAdd(SidePannelOpen.transform.parent.gameObject, iTween.Hash("x", SidePannelOpen.transform.parent.localPosition.x - 1000f, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveAdd(InventoryScroll, iTween.Hash("x", InventoryScroll.transform.localPosition.x + 1000f, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));



        //iTween.MoveAdd(ValueDisplaySlider.gameObject, iTween.Hash("x", ValueDisplaySlider.transform.localPosition.x + 250, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));



        iTween.MoveAdd(XP_ToShow.transform.parent.gameObject, iTween.Hash("x", XP_ToShow.transform.parent.localPosition.x - 400, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveAdd(ToDoList_DD.gameObject, iTween.Hash("x", ToDoList_DD.transform.localPosition.x - 400, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveAdd(Reset_Btn.transform.parent.gameObject, iTween.Hash("x", XP_ToShow.transform.parent.localPosition.x - 400, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));



        //iTween.MoveAdd(Main_XP_ToShow.transform.parent.gameObject, iTween.Hash("x", Main_XP_ToShow.transform.parent.localPosition.x + 550, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
    }
    public void PlaySequenceStep()
    {
        for (int i = 0; i < UIButtons.Length; i++)
        {
            if (UIButtons[i].CurrObj_Cnt == 0)
            {

            }
            else
            {
                UIButtons[i].Restart();
            }

        }

        if (Ironbar_Final.activeInHierarchy || Aluminiumbar_Final.activeInHierarchy || Copperbar_Final.activeInHierarchy)

        {
            DragDropSteps--;
            UpdateToNextStep();
            Collect_XP(metal_and_non_metal_set);
            Wires_Anim.SetActive(true);
            Wires1.GetComponent<Animator>().Play("wire1on");
            Wires2.GetComponent<Animator>().Play("wire2on");
            Wires4.GetComponent<Animator>().Play("wire4on");
            Wires8.GetComponent<Animator>().Play("wire8on");
            Bulb3.GetComponent<Animator>().Play("blubon");
            Bulb3.GetComponent<Renderer>().material.color = new Color(0, 0, 1, 1);
            //BulbShining.GetComponent<Renderer>().material.color = new Color(0, 0, 1, 1);
            Lighteffect.SetActive(true);

            if (Ironbar_Final.activeInHierarchy)
            {

                Ironbar_Final.transform.GetChild(1).gameObject.SetActive(true);
                Ironbar_Final.transform.GetChild(1).gameObject.transform.localScale = new Vector3(1, 0, 1);

                iTween.ScaleTo(Ironbar_Final.transform.GetChild(1).gameObject, iTween.Hash("y", 1, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine));
            }
            else if (Aluminiumbar_Final.activeInHierarchy)
            {
                Aluminiumbar_Final.transform.GetChild(1).gameObject.SetActive(true);

                Aluminiumbar_Final.transform.GetChild(1).gameObject.transform.localScale = new Vector3(1, 0, 1);

                iTween.ScaleTo(Aluminiumbar_Final.transform.GetChild(1).gameObject, iTween.Hash("y", 1, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine));
            }
            else if (Copperbar_Final.activeInHierarchy)
            {
                Copperbar_Final.transform.GetChild(1).gameObject.SetActive(true);
                Copperbar_Final.transform.GetChild(1).gameObject.transform.localScale = new Vector3(1, 0, 1);

                iTween.ScaleTo(Copperbar_Final.transform.GetChild(1).gameObject, iTween.Hash("y", 1, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine));
            }

        }
        else if (Woodenbar_Final.activeInHierarchy || Paperroll_Final.activeInHierarchy || Glassbar_Final.activeInHierarchy)
        {

            Wires_Anim.SetActive(false);
            Wires1.GetComponent<Animator>().Play("wire1of");
            Wires2.GetComponent<Animator>().Play("wire2of");
            Wires4.GetComponent<Animator>().Play("wire4of");
            Wires8.GetComponent<Animator>().Play("wire8of");
            Bulb3.GetComponent<Animator>().Play("blubof");
            Lighteffect.SetActive(false);
            DragDropSteps--;
            UpdateToNextStep();
            Collect_XP(metal_and_non_metal_set);
            if (Woodenbar_Final.activeInHierarchy)
            {

                Woodenbar_Final.transform.GetChild(1).gameObject.SetActive(true);
                Woodenbar_Final.transform.GetChild(1).gameObject.transform.localScale = new Vector3(1, 0, 1);

                iTween.ScaleTo(Woodenbar_Final.transform.GetChild(1).gameObject, iTween.Hash("y", 1, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine));
            }
            else if (Paperroll_Final.activeInHierarchy)
            {
                Paperroll_Final.transform.GetChild(1).gameObject.SetActive(true);
                Paperroll_Final.transform.GetChild(1).gameObject.transform.localScale = new Vector3(1, 0, 1);

                iTween.ScaleTo(Paperroll_Final.transform.GetChild(1).gameObject, iTween.Hash("y", 1, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine));
            }
            else if (Glassbar_Final.activeInHierarchy)
            {
                Glassbar_Final.transform.GetChild(1).gameObject.SetActive(true);

                Glassbar_Final.transform.GetChild(1).gameObject.transform.localScale = new Vector3(1, 0, 1);

                iTween.ScaleTo(Glassbar_Final.transform.GetChild(1).gameObject, iTween.Hash("y", 1, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine));
            }

        }



        //if (OptionUnlockedTill < 1)
        //{

        //}






    }




    public void UpdateToNextStep()
    {


        //  CurrentStepCnt++;
        //  PlayerPrefs.SetInt("CurrentStepCount", PlayerPrefs.GetInt("CurrentStepCount") + 1);






    }

    public void WrongDragDrop()
    {
        ShowInstruction_Panel("Wrong Option Selected");
    }
    public void WrongPlacement()
    {
        ShowInstruction_Panel("Wrong Placement");
    }



    public void ShowLabels()
    {
        if (ShowLabels_Btn.GetComponent<Image>().color.a <= 0.5f)
            return;

        if (!Label[0].activeInHierarchy)
        {
            //Labels.SetActive(true);
            for (int i = 0; i < Labels.transform.childCount; i++)
            {
                Label[i].SetActive(true);

            }
        }
        else
        {
            //Labels.SetActive(false);
            for (int i = 0; i < Labels.transform.childCount; i++)
            {
                Label[i].SetActive(false);
            }

        }
    }


    public void ResetExperiment()
    {
        CurrentStepCnt = 0;
        //Selectgrp(-1);
        IsTopicUnlocked = true;
        GrpUnlockedTill = 0;
        dup_btn1.SetActive(false);
        dup_btn2.SetActive(false);
        dup_btn3.SetActive(false);
        Labels.SetActive(false);

        ShowLabels_Btn.GetComponent<Image>().color = new Color(1, 1, 1, 0.5f);


        if (!IsAR)
        {
            CamOrbit.zoomMax = 2f;
            CamOrbit.zoomMin = 1f;
            CamOrbit.zoomStart = 0.5f;
            CamOrbit.distance = CamOrbit.zoomStart;
            CamOrbit.transform.position = new Vector3(0, 0.12f, 0);

        }




        for (int i = 0; i < UIButtons.Length; i++)
        {
            UIButtons[i].Restart();
            //this.transform.SetAsLastSibling();
            //SetActive(false);

        }

        ObjectivePanel.transform.Find("ObjectiveText").GetComponent<Text>().text = Sub_Top_ToDoList[CurrentStepCnt];
        ObjectivePanel.transform.localScale = new Vector3(1, 0, 1);
        iTween.ScaleTo(ObjectivePanel, iTween.Hash("y", 1, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine));

        DragDropSteps = UIcanvas.GetComponentsInChildren<vid_metalsUIDragAndDrop>().Length;

        NumOfSteps.text = DragDropSteps.ToString();

    }
    public class metals1 : MonoBehaviour
    {
        Renderer rend;
        public float offset, scrollSpeed = 4f, dir = 1;
        void Start()
        {
            //dir = 1;
            rend = GetComponentInChildren<Renderer>();
            offset = 1f;
            rend.materials[0].SetTextureOffset("_MainTex", new Vector2(0, 1f));
        }

        // Update is called once per frame
        void Update()
        {
            offset += Time.deltaTime * scrollSpeed;
            rend.materials[0].SetTextureOffset("_MainTex", new Vector2(0, -dir * offset));

        }
    }

}
