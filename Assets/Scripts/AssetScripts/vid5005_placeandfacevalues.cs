﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;

public class vid5005_placeandfacevalues : MonoBehaviour, IPointerDownHandler
{
    // Start is called before the first frame update

    public Canvas C_BG;

    public orbit CamOrbit;

    public AROrbitControls ArOrbit;

    public bool IsAR;

    public GameObject MainObj, Ground;

    public int OptionUnlockedTill, CurrentTopicSelected, TopicToCheck;

    public float Sub_XP_Earned, Total_XP;

    public Dropdown Options_DD, ToDoList_DD;

    public GameObject InstructionPanel_DD, ToDoList_Panel;

    public List<Dictionary<string, object>> CSV_data;

    public List<String> TaskList, XpList, InfoText;

    public List<GameObject> AllTaskData;

    public GameObject[] Labels;

    public Text XP_ToShow, XP_Effect, Main_XP_ToShow, localTotalXpValueText, localGainedXpValueText, ObjectiveInfoText, LGListText, IListText, AListText,
        InfoHeadingText, GF_ResultTxt, E_valueTxt, m_valtxt;

    public GameObject TaskObj, AllTaskListObj;

    public RectTransform rT;

    public int localTotalXpValue, localGaniedXpValue, MainXpValue;

    public GameObject ControlPanel, LocalXP, Main_Xp, ObjectivePanel, InformationPanel, InfoButton, MainSlider, InfoPanelBg, TasklistPanel, WronOptionPanel, Gained_XP_ArrowBut;

    public Button InfoCloseBtn, InfoContinueBtn, Gained_XP_Panel, CpCloseBtn, IncButton1, DecButton1, IncButton2, DecButton2, DecButton3, IncButton3;

    public bool sessionStart;

    public TextAsset CSVFile;

    public AssetBundle assetBundle;

    public string[] CorrectMsgs = new string[] { "Nice!", "WellDone!", "Excellent!", "Correct!" };

    public Color Highlight_Color, Normal_Color;

    public Text MessageT, DescText;

    //......................

    public GameObject[] NumberObjects, QuestionObjects, FaceValues, nObjects, PoleColliders, Poles;

    public Vector3[] NumPos, QuePos, FacePos;

    public int StoredValue, te;

    public GameObject TempObj, SubmitButton, PopUpMsg;

    public int[] mulFac = new int[]{1,10,100,1000,10000,100000,1000000};

    public TextMeshPro[] DisplayFactors;

    public int RandomNumQues, RNQLength,findPlaceVal,mulFa, findingNum;

    public GameObject[] IncButtns,DecButtns,textValues,Buttons;

    public float[] initRingPos;

    public static vid5005_placeandfacevalues Instance;
    
    public List<int> placeValues,FindNumbers;

    public int[] FirstFiveRandomNumbers, FirstFivRandomDigitPlace, FirstFivRandomDigit;

    void Start()
    {
        Instance = this;

        LoadFromAsssetBundle();                     

        MainObj = this.gameObject;

        C_BG = GameObject.Find("Canvas_BG").GetComponent<Canvas>();

        Ground = GameObject.Find("Ground");

        if (!IsAR)
        {
            CamOrbit = Camera.main.GetComponentInParent<orbit>();

            C_BG.transform.parent = Camera.main.transform;
            C_BG.GetComponentInChildren<RawImage>().enabled = true;
            C_BG.renderMode = RenderMode.ScreenSpaceCamera;
            C_BG.worldCamera = Camera.main;
            CamOrbit.target = Camera.main.transform.parent;

            CamOrbit.target.position = new Vector3(0, 0.2f, 0);
            CamOrbit.maxRotUp = 1;
            CamOrbit.minRotUp = 1;

            CamOrbit.maxSideRot = 45;
            CamOrbit.minSideRot = 45;

            CamOrbit.minRotUp = 1;
            CamOrbit.zoomMin = 0.53f;
            CamOrbit.zoomMax = 1.5f;
            CamOrbit.zoomStart = 0.6f;
            CamOrbit.Start();
        }
        else
        {
            Ground.SetActive(false);
            C_BG.gameObject.SetActive(false);
            ArOrbit = MainObj.transform.parent.gameObject.GetComponent<AROrbitControls>();
        }

       
        sessionStart = false;
        //OptionUnlockedTill = 0;
        //TopicToCheck = 1;
        //MainXpValue = 10;

        PopUpMsg = GameObject.Find("PopUpMsg");

        GeneratingFiveRandomNumbersForTasks();
        //ChangeNumber();

        ReadingDataFromCSVFile();
        ReadingXps();
        FindingObjects();
       
        TaskListCreation();
        AssigningClickEvents();
        LabelsAssignment();
        AssignInitialValues();

    }


    public void GeneratingFiveRandomNumbersForTasks() {

        FirstFiveRandomNumbers = new int[2];
        FirstFivRandomDigitPlace = new int[2];
        FirstFivRandomDigit = new int[2];

        for (int i = 0; i < FirstFiveRandomNumbers.Length; i++) {

            FirstFiveRandomNumbers[i] = UnityEngine.Random.Range(1, 999999);

            FirstFivRandomDigitPlace[i] = UnityEngine.Random.Range(1, FirstFiveRandomNumbers[i].ToString().Length);
            
            findPlaceVal = FirstFivRandomDigitPlace[i];                 

            mulFa = 1;

            for (int P = 1; P < 8; P++)
            {
                if (P < findPlaceVal)
                {
                    mulFa = mulFa * 10;
                }
            }

            findingNum = GetPlace(FirstFiveRandomNumbers[i], mulFa);
            
           // print(findingNum);

            FirstFivRandomDigit[i] = findingNum;

        }

        //.............................................Random digit selection in selected number........................................      


        ChangeNumber(FirstFiveRandomNumbers[0], FirstFivRandomDigit[0]);
    }

    public void LoadFromAsssetBundle()
    {

        //......................................................For Testing ..............................................................

        //CSVFile = Resources.Load("vid5005_placeandfacevaluesCSV") as TextAsset;

        //............................................................ For Assetbundle Creation ..........................................

        assetBundle = AssetBundle.LoadFromFile(PlayerPrefs.GetString("AssetBundleUrl"));

        CSVFile = assetBundle.LoadAsset(PlayerPrefs.GetString("LibraryName") + "CSV", typeof(TextAsset)) as TextAsset;       

        //...............................................................................................................................

        Camera.main.gameObject.AddComponent<PhysicsRaycaster>();

        Camera.main.backgroundColor = Color.black;

        GameObject TargetForCamera = GameObject.Find("Target");

        if (TargetForCamera != null)
        {
            TargetForCamera.AddComponent<orbit>();
            CamOrbit = TargetForCamera.GetComponent<orbit>();
            IsAR = false;
        }
        else
        {
            this.transform.parent.gameObject.AddComponent<AROrbitControls>();
            ArOrbit = this.transform.parent.gameObject.GetComponent<AROrbitControls>();
            IsAR = true;
        }
    }

    public void ReadingDataFromCSVFile()
    {

        TaskList = new List<string>();
        XpList = new List<string>();
        InfoText = new List<string>();

        AllTaskData = new List<GameObject>();

        CSV_data = CSVReader.Read(CSVFile);

        for (var i = 0; i < CSV_data.Count; i++)
        {
            if (CSV_data[i]["TaskList"].ToString() != "" && CSV_data[i]["TaskList"].ToString() != null)
            {
                TaskList.Add(CSV_data[i]["TaskList"].ToString()+" "+ FirstFivRandomDigit[i].ToString()+"(s) in "+ FirstFiveRandomNumbers[i].ToString());
            }

            //if (CSV_data[i]["XPList"].ToString() != "" && CSV_data[i]["XPList"].ToString() != null)
            //{
            //    XpList.Add(CSV_data[i]["XPList"].ToString());
            //}

            if (CSV_data[i]["InfoText"].ToString() != "" && CSV_data[i]["InfoText"].ToString() != null)
            {
                InfoText.Add(CSV_data[i]["InfoText"].ToString());
            }
        }
    }

    public int totXp, loclXp;

    public void ReadingXps()
    {
        totXp = PlayerPrefs.GetInt("TotalXP");
        loclXp = PlayerPrefs.GetInt("MaxEarnings");

        if (loclXp != 0)
        {
            OptionUnlockedTill = 0;
        }
        else
        {
            OptionUnlockedTill = 1;
        }

        TopicToCheck = 1;

        MainXpValue = totXp;

        int t = 0;
        int p = 0;

        for (int k = 0; k < TaskList.Count; k++)
        {
            p = loclXp / TaskList.Count;
            XpList.Add(p.ToString());
            t = t + p;
        }

        if (t != loclXp)
        {
            XpList[XpList.Count - 1] = (p + (loclXp - t)).ToString();
        }
    }

    public float result;
    public void GetResult()
    {
        result = ((float)(TopicToCheck - 1) / (float)(TaskList.Count)) * 100;
        Camera.main.SendMessage("UnloadActually", result);
    }

    public void FindingObjects() {

        InformationPanel = GameObject.Find("InformationPanel");

        InfoCloseBtn = GameObject.Find("InfoCloseBtn").GetComponent<Button>();

        InfoContinueBtn = GameObject.Find("InfoContinueBtn").GetComponent<Button>();

        InfoPanelBg = GameObject.Find("InfoPanelBg");
        
        XP_ToShow = GameObject.Find("localGainedXpValueText").GetComponent<Text>();

        XP_Effect = GameObject.Find("Effect_Text").GetComponent<Text>();

        Main_XP_ToShow = GameObject.Find("Main_XP_Value").GetComponent<Text>();

        localGainedXpValueText = GameObject.Find("localGainedXpValueText").GetComponent<Text>();

        localTotalXpValueText = GameObject.Find("GainedXP_LocalTotalXp").GetComponent<Text>();

        ObjectiveInfoText = GameObject.Find("ObjectiveInfoText").GetComponent<Text>();

        TaskObj = GameObject.Find("TaskObj");

        AllTaskListObj = GameObject.Find("AllTaskList");

        ControlPanel = GameObject.Find("ControlPanel");
        LocalXP = GameObject.Find("LocalXP");
        Main_Xp = GameObject.Find("Main_Xp");
        ObjectivePanel = GameObject.Find("ObjectivePanel");
        InformationPanel = GameObject.Find("InformationPanel");
        InfoButton = GameObject.Find("InfoButton");
        MainSlider = GameObject.Find("MainSlider");
        WronOptionPanel = GameObject.Find("WrongPanel");

        Gained_XP_Panel = GameObject.Find("Gained_XP_Panel").GetComponent<Button>();
        TasklistPanel = GameObject.Find("TasklistPanel");
        CpCloseBtn = GameObject.Find("CpCloseBtn").GetComponent<Button>();

        InfoHeadingText = GameObject.Find("InfoHeadingText").GetComponent<Text>();
        LGListText = GameObject.Find("LGListText").GetComponent<Text>();
        IListText = GameObject.Find("IListText").GetComponent<Text>();
        AListText = GameObject.Find("AListText").GetComponent<Text>();

        Gained_XP_ArrowBut = GameObject.Find("Gained_XP_ArrowBut");

        //......................................................

        GameObject OneRng = GameObject.Find("OnesRings");

        initRingPos = new float[OneRng.transform.childCount];

        for (int p = 0; p < initRingPos.Length; p++)
        {
            initRingPos[p] = OneRng.transform.GetChild(p).transform.localPosition.y;
        }

        GameObject NumObj = GameObject.Find("NumberObjects");

        NumberObjects = new GameObject[NumObj.transform.childCount];
        //NumPos = new Vector3[NumObj.transform.childCount];

        for (int j = 0; j < NumberObjects.Length; j++)
        {
            int I = j;
            NumberObjects[j] = NumObj.transform.GetChild(j).gameObject;
            //NumberObjects[j].AddComponent<Vid5005_PingPongPos>();
            //NumberObjects[j].GetComponent<Vid5005_PingPongPos>().enabled =false;
            //AddListener(EventTriggerType.PointerDown, ActivateLerp, NumberObjects[j].gameObject, NumberObjects[j].gameObject);
            NumberObjects[j].GetComponent<Button>().onClick.AddListener(() => ActivateLerp(NumberObjects[I]));

            NumberObjects[j].AddComponent<Maths2DPlaceMaindragAndDrop>();

            NumberObjects[j].GetComponent<Maths2DPlaceMaindragAndDrop>().num = j;

            //" Maths2DPlaceMaindragAndDrop"
            // NumPos[j] = NumberObjects[j].transform.localPosition;
        }
        
        GameObject QueObj = GameObject.Find("QuestionObjects");

        QuestionObjects = new GameObject[QueObj.transform.childCount];
        QuePos = new Vector3[QueObj.transform.childCount];

        for (int j = 0; j < QuestionObjects.Length; j++)
        {
            QuestionObjects[j] = QueObj.transform.GetChild(j).gameObject;
            QuestionObjects[j].AddComponent<Vid5005_PingPongScale>();
            QuestionObjects[j].GetComponent<Vid5005_PingPongScale>().enabled = false;

            QuePos[j] = QuestionObjects[j].GetComponentInChildren<TextMeshPro>().gameObject.transform.localScale;
            QuestionObjects[j].transform.GetChild(2).gameObject.SetActive(false);
            //QuestionObjects[j].GetComponentInChildren<Renderer>().gameObject.SetActive(false);

            AddListener(EventTriggerType.PointerDown, PlaceValue, QuestionObjects[j].gameObject, QuestionObjects[j],j);
        }

        GameObject nObj = GameObject.Find("nObjects");

        nObjects = new GameObject[nObj.transform.childCount];

        for (int j = 0; j < nObjects.Length; j++)
        {
            nObjects[j] = nObj.transform.GetChild(j).gameObject;
            //AddListener(EventTriggerType.PointerDown, RemoveRing, nObjects[j].gameObject, nObjects[j].gameObject);
            // nObjects[j].SetActive(false);

            for (int l = 0; l < nObjects[j].transform.childCount; l++)
            {
                nObjects[j].transform.GetChild(l).gameObject.transform.localPosition = new Vector3(0, 0, 0);
                nObjects[j].transform.GetChild(l).gameObject.SetActive(false);
                //AddListener(EventTriggerType.PointerDown, RemoveRing, nObjects[j].transform.GetChild(l).gameObject, nObjects[j].gameObject);
            }
        }

        GameObject FaceObj = GameObject.Find("FaceValues");

        FaceValues = new GameObject[FaceObj.transform.childCount];
        FacePos = new Vector3[FaceObj.transform.childCount];

        for (int j = 0; j < FaceValues.Length; j++)
        {
            FaceValues[j] = FaceObj.transform.GetChild(j).gameObject;
            //QuestionObjects[j].AddComponent<Vid5005_PingPongScale>();
            //QuestionObjects[j].GetComponent<Vid5005_PingPongScale>().enabled = false;
            //QuePos[j] = QuestionObjects[j].GetComponentInChildren<TextMeshPro>().gameObject.transform.localScale;
            //AddListener(EventTriggerType.PointerDown, CreateFaceValues, FaceValues[j].gameObject, j);
            FaceValues[j].AddComponent<MathsPlaceMaindragAndDrop>();
            FaceValues[j].GetComponent<MathsPlaceMaindragAndDrop>().num = j;
        }

        GameObject PolCol = GameObject.Find("PoleColliders");
        PoleColliders = new GameObject[PolCol.transform.childCount];
        GameObject pol = GameObject.Find("Poles");
        Poles = new GameObject[PolCol.transform.childCount];

        for (int j = 0; j < PoleColliders.Length; j++)
        {
            Poles[j] = pol.transform.GetChild(j).gameObject;
            PoleColliders[j] = PolCol.transform.GetChild(j).gameObject;           
        }

        GameObject Dfctors = GameObject.Find("DisplayFactors");
        GameObject btns = GameObject.Find("Buttons");
        DisplayFactors = new TextMeshPro[Dfctors.transform.childCount];
        Buttons = new GameObject[btns.transform.childCount];

        for (int j = 0; j < DisplayFactors.Length; j++)
        {
            DisplayFactors[j] = Dfctors.transform.GetChild(j).gameObject.GetComponentInChildren<TextMeshPro>();
            Buttons[j] = btns.transform.GetChild(j).gameObject;
            Buttons[j].SetActive(false);          
        }
     
        IncButtns = new GameObject[btns.transform.childCount];
        DecButtns = new GameObject[btns.transform.childCount];
        textValues = new GameObject[btns.transform.childCount];

        for (int j = 0; j < IncButtns.Length; j++)
        {
            int I = j;
            
            DecButtns[j] = btns.transform.GetChild(j).gameObject.transform.GetChild(0).gameObject;
            textValues[j] = btns.transform.GetChild(j).gameObject.transform.GetChild(1).gameObject;
            IncButtns[j] = btns.transform.GetChild(j).gameObject.transform.GetChild(2).gameObject;
          
            AddListener(EventTriggerType.PointerDown, ChangeValue, IncButtns[j].gameObject, 1, textValues[j].GetComponent<TextMeshPro>(), j);
            AddListener(EventTriggerType.PointerDown, ChangeValue, DecButtns[j].gameObject, -1, textValues[j].GetComponent<TextMeshPro>(), j);

            for (int l = 0; l < nObjects[j].transform.childCount; l++)
            {
                AddListener(EventTriggerType.PointerDown, ChangeValue, nObjects[j].transform.GetChild(l).gameObject, -1, textValues[j].GetComponent<TextMeshPro>(), j);
            }
        }

        SubmitButton = GameObject.Find("SubmitButton");

      
    }

    public void AssigningClickEvents() {
                
        iTween.ScaleTo(InformationPanel.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

        InfoCloseBtn.onClick.AddListener(() => CloseInfoPanel());

        InfoContinueBtn.onClick.AddListener(() => ContinueInfoPanel());

        InfoButton.GetComponent<Button>().onClick.AddListener(() => OpenInfoPanel());

        Gained_XP_Panel.onClick.AddListener(() => OpenTaskList());

        CpCloseBtn.onClick.AddListener(() => OpenOrCloseControlPanel());

        //IncButton1.GetComponent<Button>().onClick.AddListener(() => ChangeBallMass(0.2f, M1_value_txt,Box1,Arrows1, weight1_label, Mass1_Tex));
        //DecButton1.GetComponent<Button>().onClick.AddListener(() => ChangeBallMass(-0.2f, M1_value_txt,Box1, Arrows1, weight1_label, Mass1_Tex));

        AddListener(EventTriggerType.PointerDown, SubmitValue, SubmitButton, GetPlace(RandomNumQues, mulFa));
    }

    public void AssignInitialValues()
    {
        //InfoHeadingText.text = InfoText[0];
        //LGListText.text = InfoText[1];
        //IListText.text = InfoText[2];
        //AListText.text = InfoText[3];
        if (OptionUnlockedTill > 0)
        {
            LocalXP.transform.localScale = new Vector3(0, 0, 0);
            //ObjectivePanel.transform.localScale = new Vector3(0, 0, 0);
        }

        InfoCloseBtn.gameObject.transform.localScale = new Vector3(0, 0, 0);
        localGainedXpValueText.text = "0";
        Main_XP_ToShow.text = MainXpValue + "";
        WronOptionPanel.transform.localScale = new Vector3(0, 0, 0);

       // PopUpMsg.SetActive(false);
    }


    public void ChangeNumber(int Num, int didgit) {

        StoredValue = -1;

        

        PopUpMsg.SetActive(true);

        PopUpMsg.GetComponentInChildren<TextMeshPro>().text = "Place the digit into abacus";

        //.............................................Random number Selection.........................................................

        placeValues.Clear();
        FindNumbers.Clear();
        RandomNumQues = Num;    
        findingNum = didgit;

        //...............................How many times repeated.........................................................................

        int m = 1, c = 0;

        for (int l = 0; l < (Num.ToString()).Length; l++)
        {
            int rn = GetPlace(Num, m);

            if (rn == findingNum)
            {
                c++;

                placeValues.Add(l);
            }
            else {
                placeValues.Add(-1);
            }
            m = m * 10;
        }

        // print("How many times repeated" + c);

        //...............................where it is repeated............................................................... 

        for (int k = 0; k < placeValues.Count; k++)
        {
          //  print("where it is repeated" + placeValues[k]);
            int mulFa1 = 1;

            if (placeValues[k] != 0)
            {
                for (int P = 0; P < 8; P++)
                {
                    if (P < placeValues[k])
                    {
                        mulFa1 = mulFa1 * 10;
                    }
                }
            }
            FindNumbers.Add(GetPlace(RandomNumQues, mulFa));           
        }
    }
    
    public int GetPlace(int value, int place)
    {
        return ((value % (place * 10)) - (value % place)) / place;
    }

    //public int GetPlace(int value, int place)
    //{
    //    return (value % (place * 10)) - (value % place);
    //}

    public void TaskListCreation() {

        rT = AllTaskListObj.transform.parent.GetComponent<RectTransform>();

        localGaniedXpValue = 0;

        for (int i = 0; i < TaskList.Count; i++)
        {

            GameObject Tl = (GameObject)Instantiate(TaskObj);
            Tl.transform.SetParent(AllTaskListObj.transform);
            Tl.transform.localScale = new Vector3(1, 1, 1);
            Tl.name = i.ToString();

            AllTaskData.Add(Tl);
            AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(false);
            AllTaskData[i].gameObject.transform.GetChild(2).gameObject.SetActive(false);
            AllTaskData[i].gameObject.transform.GetChild(3).GetComponentInChildren<Text>().text = TaskList[i];
            AllTaskData[i].gameObject.transform.GetChild(4).GetComponentInChildren<Text>().text = "+" + XpList[i] + " XP";

            localTotalXpValue += int.Parse(XpList[i]);
        }

        rT.sizeDelta = new Vector2(rT.sizeDelta.x, TaskList.Count * 50);
        localTotalXpValueText.text = "/" + localTotalXpValue.ToString();

        AllTaskData[0].gameObject.transform.GetChild(2).gameObject.SetActive(true);
        ObjectiveInfoText.text = AllTaskData[0].gameObject.transform.GetChild(3).GetComponentInChildren<Text>().text;
    }

    public void LocalXpCalculation()
    {
        for (int i = 0; i < TaskList.Count; i++)
        {
            AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(false);
            AllTaskData[i].gameObject.transform.GetChild(2).gameObject.SetActive(false);
        }

        if (OptionUnlockedTill < 1)
        {
            if ((TopicToCheck) < (TaskList.Count + 1))
            {
                localGaniedXpValue = 0;

                for (int i = 0; i < TopicToCheck - 1; i++)
                {
                    localGaniedXpValue += int.Parse(XpList[i]);
                }

                for (int i = 0; i < TopicToCheck; i++)
                {
                    AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(true);
                }

                localGainedXpValueText.text = localGaniedXpValue.ToString();

                AllTaskData[TopicToCheck - 1].gameObject.transform.GetChild(2).gameObject.SetActive(true);

                ObjectiveInfoText.text = AllTaskData[TopicToCheck - 1].gameObject.transform.GetChild(3).GetComponentInChildren<Text>().text;
            }
            else
            {
                for (int i = 0; i < TaskList.Count; i++)
                {
                    AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(true);
                    AllTaskData[i].gameObject.transform.GetChild(2).gameObject.SetActive(false);
                }

                localGainedXpValueText.text = localTotalXpValue.ToString();

                XP_Effect.text = "+" + localTotalXpValue + " XP";

                XP_Effect.transform.position = (XP_ToShow.transform.position);

                iTween.Stop(XP_Effect.gameObject);

                iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeOutElastic));

                iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", XP_Effect.transform.position.x, "y", XP_Effect.transform.position.y + 50, "z", 0, "delay", 0.5f, "time", 0.2f, "easetype", iTween.EaseType.easeInSine));

                iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", Main_XP_ToShow.transform.position.x, "y", Main_XP_ToShow.transform.position.y, "z", 0, "delay", 0.7f, "time", 0.2f, "easetype", iTween.EaseType.easeInSine));

                iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "delay", 0.9f, "time", 0.1f, "easetype", iTween.EaseType.linear, "oncomplete", "MainXpCalculation", "oncompletetarget", this.gameObject));
            }
        }
        else
        {
            for (int i = 0; i < TaskList.Count; i++)
            {
                AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(true);
                AllTaskData[i].gameObject.transform.GetChild(2).gameObject.SetActive(false);
            }

            localGainedXpValueText.text = localTotalXpValue.ToString();
        }
    }

    public void CloseAllpanels()
    {

        ControlPanel.GetComponent<Animator>().Play("CpanelFullClose");
        LocalXP.GetComponent<Animator>().Play("LocalXpPanelClose");
        Main_Xp.GetComponent<Animator>().Play("MainXpPanelClose");
        ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelClose");
        InfoButton.GetComponent<Animator>().Play("InfoButtonClose");
        MainSlider.GetComponent<Animator>().Play("MainSliderClose");
        
        if(TasklistPanel.gameObject.transform.localScale.y!=0)
            iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));
    }

    public void OpenAllpanels()
    {
        if (OptionUnlockedTill < 1)
        {
            if (TopicToCheck <= (TaskList.Count))
            {
                LocalXP.GetComponent<Animator>().Play("LocalXpPanelOPen");

            }
        }
        ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen");
        ControlPanel.GetComponent<Animator>().Play("CpanelOpen");
        
        Main_Xp.GetComponent<Animator>().Play("MainXpPanelOpen");
       
        InfoButton.GetComponent<Animator>().Play("InfoButtonOpen");
        MainSlider.GetComponent<Animator>().Play("MainSliderOpen");
        iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "time", 0.2f, "easetype", iTween.EaseType.linear));
        iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));
    }

    public void AddXpEffect() {
                
        if (TopicToCheck <= (TaskList.Count))
        {            

            GameObject.Find("Canvas").GetComponent<CanvasGroup>().interactable = false;

            XP_Effect.GetComponent<RectTransform>().anchoredPosition = new Vector2(0,0);

            XP_Effect.text = "+"+int.Parse(XpList[TopicToCheck - 1]) + " XP";

            XP_Effect.transform.localScale = new Vector3(1, 0, 1);

            iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeOutElastic));

            iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", XP_Effect.transform.position.x, "y", XP_Effect.transform.position.y + 50, "z", 0, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

            iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", XP_ToShow.transform.position.x, "y", XP_ToShow.transform.position.y, "z", 0, "delay", 1.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

            iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "delay", 2f, "time", 0.5f, "easetype", iTween.EaseType.linear, "oncomplete", "LocalXpCalculation", "oncompletetarget", this.gameObject));

            TopicToCheck++;

            Invoke("EnableRayCast", 2);
        }
        
    }

    public void EnableRayCast() {
        //Camera.main.GetComponent<PhysicsRaycaster>().enabled = true;
        GameObject.Find("Canvas").GetComponent<CanvasGroup>().interactable = true;
    }

    public void MainXpCalculation()
    {
        if (OptionUnlockedTill == 0)
        {
            OptionUnlockedTill = 1;

            MainXpValue += localTotalXpValue;

            Main_XP_ToShow.text = MainXpValue.ToString();

            LocalXP.GetComponent<Animator>().Play("LocalXpPanelClose");

            //ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelClose");

            if ((ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelClose")) || (ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelFullClose")))
            {
                ControlPanel.GetComponent<Animator>().Play("CpanelOpen");
            }
        }
    }

    public void CloseInfoPanel()
    {
        sessionStart = true;
        InfoPanelBg.SetActive(false);
        iTween.ScaleTo(InformationPanel.gameObject, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));
        OpenAllpanels();
    }

    public void ContinueInfoPanel()
    {
        sessionStart = true;
        InfoPanelBg.SetActive(false);
        iTween.ScaleTo(InformationPanel.gameObject, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));
        InfoCloseBtn.transform.localScale = new Vector3(0.27f, 0.27f, 0.27f);
        InfoContinueBtn.transform.localScale = new Vector3(0, 0, 0);
        OpenAllpanels();
    }

    public void OpenInfoPanel()
    {
        sessionStart = false;
        iTween.ScaleTo(InformationPanel.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));
        InfoPanelBg.SetActive(true);
        iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "time", 0.2f, "easetype", iTween.EaseType.linear));
        iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));
        CloseAllpanels();
    }

    public void OpenTaskList() {

        if (TasklistPanel.gameObject.transform.localScale.y != 1)
        {
            iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.01f, "time", 0.2f, "easetype", iTween.EaseType.easeInSine));

            ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelClose");

            iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 180, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));

            if ((ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelOpen")))
            {
                ControlPanel.GetComponent<Animator>().Play("CpanelClose");
            }
        }
        else
        {
            iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "delay", 0.01f, "time", 0.2f, "easetype", iTween.EaseType.easeInSine));
            iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));

            ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen");

            ControlPanel.GetComponent<Animator>().Play("CpanelOpen");
        }
    }

    public void OpenOrCloseControlPanel() {

        if ((ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelClose")) || (ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelFullClose")))
        {
            ControlPanel.GetComponent<Animator>().Play("CpanelOpen");
        }
        else
        {
            ControlPanel.GetComponent<Animator>().Play("CpanelClose");
        }
    }

    public void WrongOptionSelected()
    {
        WronOptionPanel.transform.localScale = new Vector3(0, 0, 0);

        iTween.Stop(WronOptionPanel.gameObject);

        iTween.ScaleTo(WronOptionPanel.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.01f, "time", 0.2f, "easetype", iTween.EaseType.easeInSine));

        iTween.ScaleTo(WronOptionPanel.gameObject, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 1f, "time", 0.2f, "easetype", iTween.EaseType.easeInSine));

        // InfoPanelBg.SetActive(true);

        Invoke("CloseBlurBg", 1f);

        GameObject.Find("Canvas").GetComponent<CanvasGroup>().interactable = false;

        ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelClose");


        var mouse = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);

        if (mouse.x < Screen.width / 2)
        {
            // WronOptionPanel
            //print("Mouse is on left side of screen.");
            WronOptionPanel.GetComponent<RectTransform>().pivot = new Vector2(0, 0);
        }

        if (mouse.x > Screen.width / 2)
        {
            //print("Mouse is on right side of screen.");
            WronOptionPanel.GetComponent<RectTransform>().pivot = new Vector2(1, 0);
        }


        WronOptionPanel.transform.position = Input.mousePosition;
    }

    public void CloseBlurBg()
    {
        InfoPanelBg.SetActive(false);
        GameObject.Find("Canvas").GetComponent<CanvasGroup>().interactable = true;

        //if (OptionUnlockedTill < 1)
        //{
            if (InformationPanel.gameObject.transform.localScale.x == 0f)
            {
                //ObjectivePanel.GetComponent<Animator>().Play("");
                ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen", 0, 0f);
            }
        //}
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (InformationPanel.gameObject.transform.localScale.x != 1 && sessionStart)
        {
            if (OptionUnlockedTill < 1)
            {
                if (TopicToCheck <= (TaskList.Count))
                {
                    iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "time", 0.2f, "easetype", iTween.EaseType.linear));
                    iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));
                    
                    ControlPanel.GetComponent<Animator>().Play("CpanelOpen");
                }
            }
            ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen");
        }  
    }

    public void LabelsAssignment()
    {
        GameObject Label = GameObject.Find("Labels");

        Labels = new GameObject[Label.transform.childCount];

        for (int j = 0; j < Labels.Length; j++)
        {
            Labels[j] = Label.transform.GetChild(j).gameObject;

            GameObject TempLabels = Labels[j];

            foreach (Transform ChildLabel in TempLabels.transform)
            {
                if (ChildLabel.name.Contains("SmoothLook"))
                {
                    ChildLabel.gameObject.AddComponent<SmoothLook>();
                    //ChildLabel.gameObject.AddComponent<ScaleRelativeToCamera>();

                }
            }
        }

        for (int k = 0; k < Labels.Length; k++)
        {
            string name = Labels[k].name.Remove(Labels[k].name.Length - 6);

            if (GameObject.Find(name + "") != null)
            {
                GameObject dummy = GameObject.Find(name + "").gameObject;

                Labels[k].transform.parent = dummy.transform;
                Labels[k].transform.rotation = Quaternion.identity;
            }

        }


    }

    public void AddListener(EventTriggerType eventType, Action MethodToCall, GameObject TriggerObjToAdd)
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall());
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }

    public void AddListener(EventTriggerType eventType, Action<float> MethodToCall, GameObject TriggerObjToAdd)
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall(TriggerObjToAdd.GetComponent<Slider>().value));
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }

    public void AddListener(EventTriggerType eventType, Action<GameObject,int> MethodToCall, GameObject TriggerObjToAdd,GameObject obj,int t)
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall(obj,t));
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }

    public void AddListener(EventTriggerType eventType, Action<TextMeshPro> MethodToCall, GameObject TriggerObjToAdd, TextMeshPro t)
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall(t));
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }
       
    public void AddListener(EventTriggerType eventType, Action<int> MethodToCall, GameObject TriggerObjToAdd, int t)
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall(t));
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }
 
    public void AddListener(EventTriggerType eventType, Action<float,TextMeshPro> MethodToCall, GameObject TriggerObjToAdd, float t,TextMeshPro txt)
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall(t,txt));
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }
    
    public void AddListener(EventTriggerType eventType, Action<int, TextMeshPro,int> MethodToCall, GameObject TriggerObjToAdd, int t, TextMeshPro txt, int tt)
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall(t, txt,tt));
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }

    public void AddListener(EventTriggerType eventType, Action<GameObject,int,int> MethodToCall, GameObject TriggerObjToAdd, GameObject t,int RefNum,int RefTe)
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall(t, RefNum, RefTe));
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }

    public void EnableOrbit()
    {
        if (!IsAR)
        {
            CamOrbit.DisableInteration = false;
        }
        else
        {
            ArOrbit.DisableInteration = false;
        }

        if (OptionUnlockedTill < 1)
        {
            if (InformationPanel.gameObject.transform.localScale.x != 1)
            {
                if (TopicToCheck <= (TaskList.Count))
                {
                    iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "time", 0.2f, "easetype", iTween.EaseType.linear));
                    iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));
                    ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen");
                }
            }
        }

      
 
    }
       
    public void DisableOrbit()
    {
        if (!IsAR)
        {
            CamOrbit.DisableInteration = true;
        }
        else
        {
            ArOrbit.DisableInteration = true;
        }
    }
    
    public Vector3 MidPoint(GameObject obj1, GameObject obj2)
    {
        Vector3 midp = (obj1.transform.localPosition + obj2.transform.localPosition) / 2;
        return midp;
    }

    public float Scale(GameObject obj1, GameObject obj2)
    {
        float scl = Vector3.Distance(obj1.transform.localPosition, obj2.transform.localPosition);
        return scl;
    }   

    public void PopUpShow() {
       // DescPopUp.SetActive(true);
    }         
   
    public void AngleSlider(float val, Text t)
    {       
        t.text = val.ToString() + " °";    
    }

    public void ResetActs()
    {
        StoredValue = -1;
        te = -1;

        for (int j = 0; j < NumberObjects.Length; j++)
        {
            NumberObjects[j].GetComponentInChildren<Text>().fontSize = 22;
        }

        for (int j = 0; j < QuestionObjects.Length; j++)
        {
            QuestionObjects[j].GetComponent<Vid5005_PingPongScale>().enabled = false;
            if (QuestionObjects[j].GetComponentInChildren<TextMeshPro>().text == "?")
            {
                QuestionObjects[j].GetComponentInChildren<TextMeshPro>().gameObject.transform.localScale = QuePos[j];
                QuestionObjects[j].transform.GetChild(2).gameObject.SetActive(false);
            }
            else {
                QuestionObjects[j].transform.GetChild(2).gameObject.SetActive(true);
                QuestionObjects[j].GetComponentInChildren<TextMeshPro>().gameObject.transform.localScale = new Vector3(0.4f, 0.4f, 0.4f);
            }
        }
    }

    public void ActivateLerp(GameObject obj) {
        
        for (int j = 0; j < NumberObjects.Length; j++)
        {
            NumberObjects[j].GetComponentInChildren<Text>().fontSize = 22;
        }

        StoredValue = int.Parse(obj.GetComponentInChildren<Text>().text);

        obj.GetComponentInChildren<Text>().fontSize = 40;
       
        for (int j = 0; j < QuestionObjects.Length; j++)
        {
            //if(QuestionObjects[j].GetComponentInChildren<TextMeshPro>().text == "?") 
            QuestionObjects[j].GetComponent<Vid5005_PingPongScale>().enabled = true;            
        }
    }

    public void PlaceValue(GameObject obj,int num)
    {
        //obj.GetComponent<Vid5005_PingPong>().enabled = true;

        print("HIhhh"); 
        if (StoredValue != -1)
        {
            Buttons[num].SetActive(true);
            obj.GetComponentInChildren<TextMeshPro>().text = StoredValue.ToString();
            obj.transform.GetChild(0).gameObject.SetActive(true);
            QuestionObjects[num].transform.GetChild(2).gameObject.SetActive(true);
            ResetActs();
            PopUpMsg.SetActive(true);

            PopUpMsg.GetComponentInChildren<TextMeshPro>().text = "Place the required beads into abacus";
        }
        else {
            Buttons[num].SetActive(false);
            obj.GetComponentInChildren<TextMeshPro>().text = "?";
            obj.transform.GetChild(0).gameObject.SetActive(true);

            for (int k = 0; k < nObjects[num].transform.childCount; k++)
            {
                GameObject o = nObjects[num].transform.GetChild(k).gameObject;
                o.transform.localPosition = new Vector3(0, 0, 0);
                o.SetActive(false);
            }
            DisplayFactors[num].text = "";

            //PopUpMsg.GetComponentInChildren<Text>().text = "Place the required beads into abacus";

            //PopUpMsg.GetComponentInChildren<Text>().text = "Place the digit into abacus";
            textValues[num].gameObject.GetComponent<TextMeshPro>().text = "0";
            QuestionObjects[num].transform.GetChild(2).gameObject.SetActive(false);
            ResetActs();
            PopUpMsg.GetComponentInChildren<TextMeshPro>().text = "Place the digit into abacus";

            for (int j = 0; j < QuestionObjects.Length; j++) {
                if(QuestionObjects[j].GetComponentInChildren<TextMeshPro>().text != "?") 
                {
                    PopUpMsg.GetComponentInChildren<TextMeshPro>().text = "Place the required beads into abacus";
                }
            }        
        }
    }

    public void CreateFaceValues(int obj) {
        te = obj;
    }

    public void ChangeValue(int IncrVal, TextMeshPro txt, int num)
    {
        if (QuestionObjects[num].GetComponentInChildren<TextMeshPro>().text != "?")
        {
            if (IncrVal > 0)
            {
                IncrVal += int.Parse(txt.text);
                IncrVal = Mathf.Clamp(IncrVal, 0, 9);
                txt.text = IncrVal.ToString();
                GameObject o = nObjects[num].transform.GetChild(IncrVal - 1).gameObject;
                o.SetActive(true);
                iTween.MoveTo(o, iTween.Hash("x", 0, "y", initRingPos[IncrVal - 1], "z", 0, "delay", 0.01f, "time", 0.5f, "islocal", true, "easetype", iTween.EaseType.easeInOutSine));

            }
            else
            {

                IncrVal += int.Parse(txt.text);
                IncrVal = Mathf.Clamp(IncrVal, 0, 9);
                txt.text = IncrVal.ToString();
                GameObject o = nObjects[num].transform.GetChild(IncrVal).gameObject;
                iTween.MoveTo(o, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.5f, "islocal", true, "easetype", iTween.EaseType.linear, "oncomplete", "ObjectFalse", "oncompleteparams", o.gameObject, "oncompletetarget", this.gameObject));
            }

            DisplayFactors[num].text = (mulFac[num] * IncrVal).ToString();
        }
        else {
            WrongOptionSelected();
            WronOptionPanel.GetComponentInChildren<Text>().text = "Digit value can not be empty";
            WronOptionPanel.GetComponentInChildren<Text>().color = new Color(0.9921f, 0.4431f, 0.08621f, 1);

        }
    }

    public void ObjectFalse(GameObject obj)
    {
        obj.SetActive(false);
    }

    bool chk = true;

    public void SubmitValue(int num) {

        chk = true;

        for (int l = 0; l< placeValues.Count; l++) {

            if (placeValues[l] != -1)
            {
                if (QuestionObjects[placeValues[l]].GetComponentInChildren<TextMeshPro>().text == findingNum.ToString()
                    && textValues[placeValues[l]].GetComponent<TextMeshPro>().text == findingNum.ToString())
                {                   
                   // WrongOptionSelected();
                   // WronOptionPanel.GetComponent<Text>().text = "Well done!";
                }
                else
                {
                    WrongOptionSelected();
                    WronOptionPanel.GetComponentInChildren<Text>().text = "Wrong value submitted";

                    WronOptionPanel.GetComponentInChildren<Text>().color = new Color(0.9921f,0.4431f,0.08621f,1);

                    // WronOptionPanel.GetComponentInChildren<Text>().color = FD7116

                    chk = false;
                    break;
                }
            }
            else {
                if (QuestionObjects[l].GetComponentInChildren<TextMeshPro>().text == "?")
                {
                    //WrongOptionSelected();
                   // WronOptionPanel.GetComponent<Text>().text = "Well done!";
                }
                else
                {
                    WrongOptionSelected();
                    WronOptionPanel.GetComponentInChildren<Text>().text = "Wrong value submitted";
                    WronOptionPanel.GetComponentInChildren<Text>().color = new Color(0.9921f, 0.4431f, 0.08621f, 1);
                    chk = false;
                    break;
                }

            }
        }
               
        if (chk) {
            if (OptionUnlockedTill < 1)
            {
                AddXpEffect();
                Invoke("ResetAll", 4f);
            }
            else {
                Invoke("ResetAll", 3f);
            }

            PopUpMsg.SetActive(false);
            WrongOptionSelected();
            WronOptionPanel.GetComponentInChildren<Text>().text = "Well Done!";
            WronOptionPanel.GetComponentInChildren<Text>().color = new Color(0.7568f,0.8588f,0.075f,1);
        }
    }


    public void ResetAll() {

        ResetActs();

        for (int j = 0; j < nObjects.Length; j++)
        {
            for (int l = 0; l < nObjects[j].transform.childCount; l++)
            {
                nObjects[j].transform.GetChild(l).gameObject.transform.localPosition = new Vector3(0, 0, 0);
                nObjects[j].transform.GetChild(l).gameObject.SetActive(false);
            }
        }

        for (int j = 0; j < QuestionObjects.Length; j++)
        {
            QuestionObjects[j].GetComponentInChildren<TextMeshPro>().text = "?";
            QuestionObjects[j].GetComponentInChildren<TextMeshPro>().gameObject.transform.localScale = new Vector3(0, 0, 0);
            QuestionObjects[j].transform.GetChild(2).gameObject.SetActive(false);
            textValues[j].gameObject.GetComponent<TextMeshPro>().text = "0";
            DisplayFactors[j].text = "";
            Buttons[j].SetActive(false);
        }
       
        if(OptionUnlockedTill < 1) { 
            ChangeNumber(FirstFiveRandomNumbers[TopicToCheck - 1], FirstFivRandomDigit[TopicToCheck - 1]);
        }
        else {
           
            RandomNumQues = UnityEngine.Random.Range(1, 9999999);

            // print("Number" + RandomNumQues);

            RNQLength = (RandomNumQues.ToString()).Length;

            //  print("Length" + RNQLength);

            //.............................................Random digit position selection in selected number..............................

            findPlaceVal = UnityEngine.Random.Range(1, RNQLength); 

            //findPlaceVal = 3;

            //print("findPlace" + findPlaceVal);

            mulFa = 1;

            for (int P = 1; P < 9; P++)
            {
                if (P < findPlaceVal)
                {
                    mulFa = mulFa * 10;
                }
            }

            findingNum = GetPlace(RandomNumQues, mulFa);
           
            ChangeNumber(RandomNumQues, findingNum);

            ObjectiveInfoText.text = "Find the place value of all " + findingNum + "(s) in " + RandomNumQues;
        }
    }
}




public class Vid5005_PingPongPos : MonoBehaviour
{
    Vector3 intPos;

    private void Start()
    {
        intPos = transform.localPosition;
    }
    void Update()
    {
       transform.localPosition = new Vector3(transform.localPosition.x, Mathf.PingPong(Time.time, 0.5f) * 0.02f + intPos.y,  transform.localPosition.z);
    }
}

public class Vid5005_PingPongScale : MonoBehaviour
{
    Vector3 intPos;

    private void Start()
    {
        intPos = this.GetComponentInChildren<TextMeshPro>().gameObject.transform.localScale;
    }
    void Update()
    {
        this.GetComponentInChildren<TextMeshPro>().gameObject.transform.localScale = new Vector3(Mathf.PingPong(Time.time, 0.5f) * 1f + intPos.x, Mathf.PingPong(Time.time, 0.5f) * 1f + intPos.y, intPos.z);
    }
}
public class MathsPlaceMaindragAndDrop : MonoBehaviour
{
    public int id;
    private bool dragging = false;
    private float distance, intialGroundTouchvalue, RanSpeed;
    private Vector3 init_pos, initialRotation;
    RaycastHit hit;
    private Vector3 temp;
    private Vector3 screenSpace;
    private Vector3 offSet;
    private Camera cam;
    private GameObject Target_collder;
    private bool CorrectPlace;
    public orbit CamOrbit;
    public AROrbitControls ARorbit;
    public bool IsAR;
    public Vector3 relativePos, TargetPos, TargetPos_Init;
    private float Initial_Dist;
    public int num;

    public void Start()
    {
        CorrectPlace = false;
        cam = Camera.main.GetComponent<Camera>();
        dragging = false;
             
        gameObject.GetComponent<EventTrigger>().triggers.Clear();
        
        Target_collder = vid5005_placeandfacevalues.Instance.PoleColliders[num].gameObject;

        EventTrigger.Entry PointerDownentry = new EventTrigger.Entry();
        PointerDownentry.eventID = EventTriggerType.PointerDown;
        PointerDownentry.callback.AddListener((data) => { MouseDown(); });

        gameObject.GetComponent<EventTrigger>().triggers.Add(PointerDownentry);

        EventTrigger.Entry PointerUpentry = new EventTrigger.Entry();
        PointerUpentry.eventID = EventTriggerType.PointerUp;
        PointerUpentry.callback.AddListener((data) => { MouseUp(); });

        gameObject.GetComponent<EventTrigger>().triggers.Add(PointerUpentry);

        Target_collder.SetActive(false);
        //init_pos = transform.localPosition;                
    }

    void MouseDown()
    {
         //orbit.orbit_enabled = false;
            
         init_pos = transform.position;
         initialRotation = transform.eulerAngles;

            Target_collder.SetActive(true);
            this.GetComponent<Collider>().enabled = false;
          
            DisableOrbit();

            screenSpace = cam.WorldToScreenPoint(transform.position);
            offSet = transform.position - cam.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenSpace.z));

            Vector3 curScreenSpace = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenSpace.z);
            Vector3 curPosition = cam.ScreenToWorldPoint(curScreenSpace) + offSet;

            intialGroundTouchvalue = curPosition.y;
            dragging = true;
        
    }

    void MouseUp()
    {
            if (!CorrectPlace)
            {
                Ray ray = cam.ScreenPointToRay(Input.mousePosition);

                if (Physics.Raycast(ray, out hit, 100))
                {
                    if (hit.collider.name == Target_collder.name)
                    {
                        vid5005_placeandfacevalues.Instance.ChangeValue(1, vid5005_placeandfacevalues.Instance.textValues[num].GetComponent<TextMeshPro>(), num);
                    }
                    else
                    {
                  

                    this.GetComponent<Collider>().enabled = true;
                    }
                }
                else
                {
                    this.GetComponent<Collider>().enabled = true;
                }
              
                InvokeRepeating("PlacemnetAdjustment", 0.01f, 0.01f);
                this.GetComponent<Collider>().enabled = true;
                Target_collder.SetActive(false);
               
                EnableOrbit();
                dragging = false;
            }
       
    }
    void PlacemnetAdjustment()
    {
        //if (CorrectPlace)
        //{
        //    if (transform.position != Target_collder.transform.position)
        //    {
        //        transform.position = Vector3.Lerp(transform.position, Target_collder.transform.position, 0.1f);
        //        transform.eulerAngles = Vector3.Lerp(transform.eulerAngles, Target_collder.transform.eulerAngles, 0.1f);
        //    }
        //    else
        //    {
        //        CancelInvoke("PlacemnetAdjustment");
        //    }
        //}
        //else
        //{
        if (transform.position != init_pos)
        {
            transform.position = Vector3.Lerp(transform.position, init_pos, 1f);
            transform.eulerAngles = Vector3.Lerp(transform.eulerAngles, initialRotation, 1f);
        }
        else
        {
            CancelInvoke("PlacemnetAdjustment");
        }
    }

    void Update()
    {
        if (dragging)
        {
            Vector3 curScreenSpace = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenSpace.z);
            Vector3 curPosition = cam.ScreenToWorldPoint(curScreenSpace) + offSet;
            //curPosition = new Vector3 (curPosition.x, Mathf.Clamp (curPosition.y, intialGroundTouchvalue+GroundTouchValue, 100f), curPosition.z);
            transform.position = curPosition;
        }
    }

  
    public void EnableOrbit()
    {
        vid5005_placeandfacevalues.Instance.EnableOrbit();
    }

    public void DisableOrbit()
    {
        vid5005_placeandfacevalues.Instance.DisableOrbit();
    }
}

public class Maths2DPlaceMaindragAndDrop : MonoBehaviour
{
    public int id;
    private bool dragging = false;
    private float distance, intialGroundTouchvalue, RanSpeed;
    private Vector3 init_pos, initialRotation;
    RaycastHit hit;
    private Vector3 temp;
    private Vector3 screenSpace;
    private Vector3 offSet;
    private Camera cam;
    private GameObject Target_collder;
    private bool CorrectPlace;
    public orbit CamOrbit;
    public AROrbitControls ARorbit;
    public bool IsAR;
    public Vector3 relativePos, TargetPos, TargetPos_Init;
    private float Initial_Dist;
    public int num;


    public TextMeshPro txt;

    public void Start()
    {

        txt = GameObject.Find("txt").GetComponent<TextMeshPro>();

        CorrectPlace = false;
        cam = Camera.main.GetComponent<Camera>();
        dragging = false;

        gameObject.GetComponent<EventTrigger>().triggers.Clear();

       // Target_collder = vid5005_placeandfacevalues.Instance.PoleColliders[num].gameObject;

        EventTrigger.Entry PointerDownentry = new EventTrigger.Entry();
        PointerDownentry.eventID = EventTriggerType.PointerDown;
        PointerDownentry.callback.AddListener((data) => { MouseDown(); });

        gameObject.GetComponent<EventTrigger>().triggers.Add(PointerDownentry);

        EventTrigger.Entry PointerUpentry = new EventTrigger.Entry();
        PointerUpentry.eventID = EventTriggerType.PointerUp;
        PointerUpentry.callback.AddListener((data) => { MouseUp(); });

        gameObject.GetComponent<EventTrigger>().triggers.Add(PointerUpentry);

        //Target_collder.SetActive(false);
        //init_pos = transform.localPosition;                
    }

    void MouseDown()
    {
        //orbit.orbit_enabled = false;
        //txt.gameObject.transform.position = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenSpace.z);

        txt.text = num.ToString();

        ////Vector2 v = cam.ScreenToWorldPoint(transform.position);

        //Vector3 v = Camera.main.WorldToViewportPoint(transform.position);

        //txt.gameObject.transform.position = new Vector3(v.x, v.y, txt.gameObject.transform.position.z);

        ////var pos = gameObject.transform.position;
        ////var vec2 = RectTransformUtility.WorldToScreenPoint(Camera.main, pos);


        //init_pos = txt.gameObject.transform.position;
        //initialRotation = txt.gameObject.transform.eulerAngles;

        //Target_collder.SetActive(true);
        //this.GetComponent<Collider>().enabled = false;


        vid5005_placeandfacevalues.Instance.ActivateLerp(this.gameObject);

        DisableOrbit();

        screenSpace = cam.WorldToScreenPoint(txt.gameObject.transform.position);
        offSet = txt.gameObject.transform.position - cam.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenSpace.z));

        Vector3 curScreenSpace = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenSpace.z);
        Vector3 curPosition = cam.ScreenToWorldPoint(curScreenSpace) ;

        intialGroundTouchvalue = curPosition.y;
        dragging = true;

    }

    void MouseUp()
    {
        if (!CorrectPlace)
        {
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit, 100))
            {

                for (int i=  0; i < vid5005_placeandfacevalues.Instance.QuestionObjects.Length; i++){

                    if (hit.collider.name == vid5005_placeandfacevalues.Instance.QuestionObjects[i].name)
                    {
                        // vid5005_placeandfacevalues.Instance.ChangeValue(1, vid5005_placeandfacevalues.Instance.textValues[num].GetComponent<TextMeshPro>(), num);


                        //print("HI");


                        vid5005_placeandfacevalues.Instance.StoredValue = int.Parse(vid5005_placeandfacevalues.Instance.NumberObjects[num].GetComponentInChildren<Text>().text);

                        //obj.GetComponentInChildren<TextMeshPro>().text = StoredValue.ToString();

                        vid5005_placeandfacevalues.Instance.PlaceValue(vid5005_placeandfacevalues.Instance.QuestionObjects[i], i);



                    }

                    else
                    {
                        this.GetComponent<Collider>().enabled = true;
                    }
                }
            }
            else
            {
                this.GetComponent<Collider>().enabled = true;
            }

            //InvokeRepeating("PlacemnetAdjustment", 0.01f, 0.01f);
            this.GetComponent<Collider>().enabled = true;
            //Target_collder.SetActive(false);

            //txt.gameObject.transform.position = init_pos;
            //txt.gameObject.transform.eulerAngles = initialRotation;
            txt.text = "";
            EnableOrbit();
            dragging = false;
        }

    }
    void PlacemnetAdjustment()
    {
        //if (CorrectPlace)
        //{
        //    if (transform.position != Target_collder.transform.position)
        //    {
        //        transform.position = Vector3.Lerp(transform.position, Target_collder.transform.position, 0.1f);
        //        transform.eulerAngles = Vector3.Lerp(transform.eulerAngles, Target_collder.transform.eulerAngles, 0.1f);
        //    }
        //    else
        //    {
        //        CancelInvoke("PlacemnetAdjustment");
        //    }
        //}
        //else
        //{

        if (transform.position != init_pos)
        {
            txt.gameObject.transform.position = Vector3.Lerp(txt.gameObject.transform.position, init_pos, 1f);
            txt.gameObject.transform.eulerAngles = Vector3.Lerp(txt.gameObject.transform.eulerAngles, initialRotation, 1f);
        }
        else
        {
            CancelInvoke("PlacemnetAdjustment");
        }
    }

    void Update()
    {
        if (dragging)
        {
            Vector3 curScreenSpace = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenSpace.z);
            Vector3 curPosition = cam.ScreenToWorldPoint(curScreenSpace);
            //curPosition = new Vector3 (curPosition.x, Mathf.Clamp (curPosition.y, intialGroundTouchvalue+GroundTouchValue, 100f), curPosition.z);
            txt.gameObject.transform.position = new Vector3(curPosition.x, curPosition.y, curPosition.z) ;
        }
    }


    public void EnableOrbit()
    {
        vid5005_placeandfacevalues.Instance.EnableOrbit();
    }

    public void DisableOrbit()
    {
        vid5005_placeandfacevalues.Instance.DisableOrbit();
    }
}