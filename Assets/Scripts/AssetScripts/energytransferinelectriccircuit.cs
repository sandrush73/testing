﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;

public class energytransferinelectriccircuit : MonoBehaviour, IPointerDownHandler
{
    // Start is called before the first frame update

    public Canvas C_BG;

    public orbit CamOrbit;

    public AROrbitControls ArOrbit;

    public bool IsAR;

    public GameObject MainObj, Ground;

    public int OptionUnlockedTill, CurrentTopicSelected, TopicToCheck;

    public float Sub_XP_Earned, Total_XP;

    public Dropdown Options_DD, ToDoList_DD;

    public GameObject InstructionPanel_DD, ToDoList_Panel;

    public List<Dictionary<string, object>> CSV_data;

    public List<String> TaskList, XpList, InfoText;

    public List<GameObject> AllTaskData;

    public GameObject[] Labels;

    public Text XP_ToShow, XP_Effect, Main_XP_ToShow, localTotalXpValueText, localGainedXpValueText, ObjectiveInfoText, LGListText, IListText, AListText,
        InfoHeadingText, GF_ResultTxt, E_valueTxt, m_valtxt;

    public GameObject TaskObj, AllTaskListObj;

    public RectTransform rT;

    public int localTotalXpValue, localGaniedXpValue, MainXpValue;

    public GameObject ControlPanel, LocalXP, Main_Xp, ObjectivePanel, InformationPanel, InfoButton, MainSlider, InfoPanelBg, TasklistPanel, WronOptionPanel, Gained_XP_ArrowBut;

    public Button InfoCloseBtn, InfoContinueBtn, Gained_XP_Panel, CpCloseBtn, IncButton1, DecButton1, IncButton2, DecButton2;

    public bool sessionStart;

    public TextAsset CSVFile;

    public AssetBundle assetBundle;

    public string[] CorrectMsgs = new string[] { "Nice!", "WellDone!", "Excellent!", "Correct!" };

    public Color Highlight_Color, Normal_Color;

    public Text MessageT, DescText;

    //....................................................................................................................................
    public Slider Slider_Resistance;

    //public Button ShowGraphbtn;

    public int noOfVolts;

    public float V, R, I;

    public Text V_valTxt, RValue, VValue, IValue, result_val;


    public GameObject Rheostat, Batteries, VMeter_Arrow, AMeter_Arrow, On_Off_Switch, Switch, BulbLight, Bulb3, Wires_Anim,
        BulbShining, popup;
    //Graphpoint, Graph, GraphLine, graphOrigin;

    public bool swtch;

    //public GameObject[] grapLabels;
    //public Light BulbLight;
    public Image LabelImage;

    public bool Lab;

    void Start()
    {
        LoadFromAsssetBundle();

        MainObj = this.gameObject;

        C_BG = GameObject.Find("Canvas_BG").GetComponent<Canvas>();

        Ground = GameObject.Find("Ground");

        if (!IsAR)
        {
            CamOrbit = Camera.main.GetComponentInParent<orbit>();

            C_BG.transform.parent = Camera.main.transform;
            C_BG.GetComponentInChildren<RawImage>().enabled = true;
            C_BG.renderMode = RenderMode.ScreenSpaceCamera;

            C_BG.worldCamera = Camera.main;

            CamOrbit.target = Camera.main.transform.parent;

            CamOrbit.target.position = new Vector3(0, 0.01f, 0);
            CamOrbit.maxRotUp = 45;
            CamOrbit.minRotUp = 1;
            CamOrbit.zoomMin = 0.65f;
            CamOrbit.zoomMax = 2;
            CamOrbit.zoomStart = 1f;

            CamOrbit.cameraRotUpStart = 30f;

            CamOrbit.Start();
        }
        else
        {
            Ground.SetActive(false);
            C_BG.gameObject.SetActive(false);
            ArOrbit = MainObj.transform.parent.gameObject.GetComponent<AROrbitControls>();
        }

        sessionStart = false;

        // OptionUnlockedTill = 0;
        //TopicToCheck = 1;
        //MainXpValue = 0;

        ReadingDataFromCSVFile();
        ReadingXps();
        FindingObjects();
        TaskListCreation();
        AssigningClickEvents();
        LabelsAssignment();
        AssignInitialValues();
    }
    public int totXp, loclXp;

    public void ReadingXps()
    {

        
        totXp = PlayerPrefs.GetInt("TotalXP");
        loclXp = PlayerPrefs.GetInt("MaxEarnings");

        if (loclXp != 0)
        {
            OptionUnlockedTill = 0;

        }
        else
        {
            OptionUnlockedTill = 1;

        }

        TopicToCheck = 1;

        MainXpValue = totXp;

        int t = 0;
        int p = 0;

        for (int k = 0; k < TaskList.Count; k++)
        {

            p = loclXp / TaskList.Count;
            XpList.Add(p.ToString());
            t = t + p;
        }

        if (t != loclXp)
        {
            XpList[XpList.Count - 1] = (p + (loclXp - t)).ToString();
        }
    }

    public float result;

    public void GetResult()
    {
        result = ((float)(TopicToCheck - 1) / (float)(TaskList.Count)) * 100;
        Camera.main.SendMessage("UnloadActually", result);
    }
    public void LoadFromAsssetBundle()
    {
        //......................................................For Testing ..............................................................

      // CSVFile = Resources.Load("energytransferinelectriccircuitCSV") as TextAsset;

        //............................................................ For Assetbundle Creation ..........................................

       assetBundle = AssetBundle.LoadFromFile(PlayerPrefs.GetString("AssetBundleUrl"));

        CSVFile = assetBundle.LoadAsset(PlayerPrefs.GetString("LibraryName") + "CSV", typeof(TextAsset)) as TextAsset;        

        //.................................................................................................................................
        Camera.main.gameObject.AddComponent<PhysicsRaycaster>();

        Camera.main.backgroundColor = Color.black;

        GameObject TargetForCamera = GameObject.Find("Target");

        if (TargetForCamera != null)
        {
            TargetForCamera.AddComponent<orbit>();
            CamOrbit = TargetForCamera.GetComponent<orbit>();
            IsAR = false;
        }
        else
        {
            this.transform.parent.gameObject.AddComponent<AROrbitControls>();
            ArOrbit = this.transform.parent.gameObject.GetComponent<AROrbitControls>();
            IsAR = true;
        }
    }
    public void ReadingDataFromCSVFile()
    {
        TaskList = new List<string>();
        XpList = new List<string>();
        InfoText = new List<string>();

        AllTaskData = new List<GameObject>();

        CSV_data = CSVReader.Read(CSVFile);

        for (var i = 0; i < CSV_data.Count; i++)
        {
            if (CSV_data[i]["TaskList"].ToString() != "" && CSV_data[i]["TaskList"].ToString() != null)
            {
                TaskList.Add(CSV_data[i]["TaskList"].ToString());
            }

            //if (CSV_data[i]["XPList"].ToString() != "" && CSV_data[i]["XPList"].ToString() != null)
            //{
            //    XpList.Add(CSV_data[i]["XPList"].ToString());
            //}

            if (CSV_data[i]["InfoText"].ToString() != "" && CSV_data[i]["InfoText"].ToString() != null)
            {
                InfoText.Add(CSV_data[i]["InfoText"].ToString());
            }
        }
    }

    public void FindingObjects()
    {

        InformationPanel = GameObject.Find("InformationPanel");

        InfoCloseBtn = GameObject.Find("InfoCloseBtn").GetComponent<Button>();

        InfoContinueBtn = GameObject.Find("InfoContinueBtn").GetComponent<Button>();

        InfoPanelBg = GameObject.Find("InfoPanelBg");

        XP_ToShow = GameObject.Find("localGainedXpValueText").GetComponent<Text>();

        XP_Effect = GameObject.Find("Effect_Text").GetComponent<Text>();

        Main_XP_ToShow = GameObject.Find("Main_XP_Value").GetComponent<Text>();

        localGainedXpValueText = GameObject.Find("localGainedXpValueText").GetComponent<Text>();

        localTotalXpValueText = GameObject.Find("GainedXP_LocalTotalXp").GetComponent<Text>();

        ObjectiveInfoText = GameObject.Find("ObjectiveInfoText").GetComponent<Text>();

        TaskObj = GameObject.Find("TaskObj");

        AllTaskListObj = GameObject.Find("AllTaskList");

        ControlPanel = GameObject.Find("ControlPanel");
        LocalXP = GameObject.Find("LocalXP");
        Main_Xp = GameObject.Find("Main_Xp");
        ObjectivePanel = GameObject.Find("ObjectivePanel");
        InformationPanel = GameObject.Find("InformationPanel");
        InfoButton = GameObject.Find("InfoButton");
        MainSlider = GameObject.Find("MainSlider");
        WronOptionPanel = GameObject.Find("WrongPanel");

        Gained_XP_Panel = GameObject.Find("Gained_XP_Panel").GetComponent<Button>();
        TasklistPanel = GameObject.Find("TasklistPanel");
        CpCloseBtn = GameObject.Find("CpCloseBtn").GetComponent<Button>();

        InfoHeadingText = GameObject.Find("InfoHeadingText").GetComponent<Text>();
        LGListText = GameObject.Find("LGListText").GetComponent<Text>();
        IListText = GameObject.Find("IListText").GetComponent<Text>();
        AListText = GameObject.Find("AListText").GetComponent<Text>();

        Gained_XP_ArrowBut = GameObject.Find("Gained_XP_ArrowBut");

        //............................................................................................................

        IncButton1 = GameObject.Find("IncButton1").GetComponent<Button>();

        DecButton1 = GameObject.Find("DecButton1").GetComponent<Button>();

        //m_valtxt = GameObject.Find("m_valtxt").GetComponent<Text>();

        V_valTxt = GameObject.Find("V_valTxt").GetComponent<Text>();

        Slider_Resistance = GameObject.Find("Slider_Resistance").GetComponent<Slider>();

        Rheostat = GameObject.Find("Rheostat2");

        Batteries = GameObject.Find("Batteries");

        VMeter_Arrow = GameObject.Find("Meter_Arrow_Voltmeter");

        AMeter_Arrow = GameObject.Find("Meter_Arrow_Ammeter");

        Switch = GameObject.Find("Switch");

        On_Off_Switch = GameObject.Find("On_Off_Switch");

        RValue = GameObject.Find("RValue").GetComponent<Text>();
        VValue = GameObject.Find("VValue").GetComponent<Text>();
        IValue = GameObject.Find("IValue").GetComponent<Text>();
        result_val = GameObject.Find("result_val").GetComponent<Text>();

        BulbLight = GameObject.Find("BulbLight");

        Bulb3 = GameObject.Find("Bulb3");

        Wires_Anim = GameObject.Find("Wires_Anim");

        BulbShining = GameObject.Find("BulbShining");
        popup = GameObject.Find("popup");
        popup.transform.GetChild(2).GetComponent<TextMeshPro>().text = "P = 0 J";
        //  Graphpoint = GameObject.Find("Graphpoint");

        //  GraphLine = GameObject.Find("GraphLine");

        //Graph = GameObject.Find("GraphObj");

        // graphOrigin = GameObject.Find("graphOrigin");

        // ShowGraphbtn = GameObject.Find("ShowGraphbtn").GetComponent<Button>();

        //Bulb3.GetComponent<Renderer>().material.color = new Color(1, 1, 1, 0.2f);
        // BulbShining.GetComponent<Renderer>().material.color = new Color(1, 1, 0, 0);
        Bulb3.GetComponent<Renderer>().material.color = new Color(0, 0, 1, 1);
        BulbShining.GetComponent<Renderer>().material.color = new Color(0, 0, 1, 1);
        // BulbShining.GetComponent<Renderer>().material.color = new Color(1, 1, 0, 0);
        LabelImage = GameObject.Find("ActivateLabels").GetComponent<Image>();
    }

    public void AssigningClickEvents()
    {

        iTween.ScaleTo(InformationPanel.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

        InfoCloseBtn.onClick.AddListener(() => CloseInfoPanel());

        InfoContinueBtn.onClick.AddListener(() => ContinueInfoPanel());

        InfoButton.GetComponent<Button>().onClick.AddListener(() => OpenInfoPanel());

        Gained_XP_Panel.onClick.AddListener(() => OpenTaskList());

        CpCloseBtn.onClick.AddListener(() => OpenOrCloseControlPanel());

        IncButton1.GetComponent<Button>().onClick.AddListener(() => ChangeBallMass(1));
        DecButton1.GetComponent<Button>().onClick.AddListener(() => ChangeBallMass(-1));

        MainSlider.GetComponent<Slider>().onValueChanged.AddListener(delegate { ChangeDistance(MainSlider.GetComponent<Slider>().value, MainSlider.GetComponentInChildren<Text>()); });

        AddListener(EventTriggerType.PointerDown, DisableOrbit, MainSlider.gameObject);
        AddListener(EventTriggerType.PointerUp, EnableOrbit, MainSlider.gameObject);

        Slider_Resistance.onValueChanged.AddListener(delegate { ChangeDistance(Slider_Resistance.value, Slider_Resistance.GetComponentInChildren<Text>()); });

        AddListener(EventTriggerType.PointerDown, DisableOrbit, Slider_Resistance.gameObject);
        AddListener(EventTriggerType.PointerUp, EnableOrbit, Slider_Resistance.gameObject);

        AddListener(EventTriggerType.PointerDown, SwitchOn, Switch.gameObject);

        // ShowGraphbtn.onClick.AddListener(() => OnGraph());

        LabelImage.GetComponent<Button>().onClick.AddListener(() => ToggleActivateLabels());
    }

    public void AssignInitialValues()
    {
        //InfoHeadingText.text = InfoText[0];
        //LGListText.text = InfoText[1];
        //IListText.text = InfoText[2];
        //AListText.text = InfoText[3];
        // MainXpValue = 10;
        if (OptionUnlockedTill > 0)
        {
            LocalXP.transform.localScale = new Vector3(0, 0, 0);
            ObjectivePanel.transform.localScale = new Vector3(0, 0, 0);
        }
        InfoCloseBtn.gameObject.transform.localScale = new Vector3(0, 0, 0);
        localGainedXpValueText.text = "0";
        Main_XP_ToShow.text = MainXpValue + "";
        WronOptionPanel.transform.localScale = new Vector3(0, 0, 0);

        noOfVolts = 1;

        V = 1.5f;
        R = 1f;
        I = 1.5f;


        swtch = false;

        for (int i = 0; i < Wires_Anim.transform.childCount; i++) {

            Wires_Anim.transform.GetChild(i).gameObject.AddComponent<OhmsLawLinesRender1>();

        }

        Wires_Anim.SetActive(false);
        // Graph.SetActive(false);
    }

    public void TaskListCreation()
    {

        rT = AllTaskListObj.transform.parent.GetComponent<RectTransform>();

        localGaniedXpValue = 0;

        for (int i = 0; i < TaskList.Count; i++)
        {
            GameObject Tl = (GameObject)Instantiate(TaskObj);
            Tl.transform.SetParent(AllTaskListObj.transform);
            Tl.transform.localScale = new Vector3(1, 1, 1);
            Tl.name = i.ToString();

            AllTaskData.Add(Tl);
            AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(false);
            AllTaskData[i].gameObject.transform.GetChild(2).gameObject.SetActive(false);
            AllTaskData[i].gameObject.transform.GetChild(3).GetComponentInChildren<Text>().text = TaskList[i];
            AllTaskData[i].gameObject.transform.GetChild(4).GetComponentInChildren<Text>().text = "+" + XpList[i] + " XP";

            localTotalXpValue += int.Parse(XpList[i]);
        }

        rT.sizeDelta = new Vector2(rT.sizeDelta.x, TaskList.Count * 50);
        localTotalXpValueText.text = "/" + localTotalXpValue.ToString();

        AllTaskData[0].gameObject.transform.GetChild(2).gameObject.SetActive(true);
        ObjectiveInfoText.text = AllTaskData[0].gameObject.transform.GetChild(3).GetComponentInChildren<Text>().text;
    }

    public void LocalXpCalculation()
    {
        for (int i = 0; i < TaskList.Count; i++)
        {
            AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(false);
            AllTaskData[i].gameObject.transform.GetChild(2).gameObject.SetActive(false);
        }

        if (OptionUnlockedTill < 1)
        {
            if ((TopicToCheck) < (TaskList.Count + 1))
            {
                localGaniedXpValue = 0;

                for (int i = 0; i < TopicToCheck - 1; i++)
                {
                    localGaniedXpValue += int.Parse(XpList[i]);
                }

                for (int i = 0; i < TopicToCheck; i++)
                {
                    AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(true);
                }

                localGainedXpValueText.text = localGaniedXpValue.ToString();

                AllTaskData[TopicToCheck - 1].gameObject.transform.GetChild(2).gameObject.SetActive(true);

                ObjectiveInfoText.text = AllTaskData[TopicToCheck - 1].gameObject.transform.GetChild(3).GetComponentInChildren<Text>().text;
            }
            else
            {
                for (int i = 0; i < TaskList.Count; i++)
                {
                    AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(true);
                    AllTaskData[i].gameObject.transform.GetChild(2).gameObject.SetActive(false);
                }

                localGainedXpValueText.text = localTotalXpValue.ToString();

                XP_Effect.text = "+" + localTotalXpValue + " XP";

                XP_Effect.transform.position = (XP_ToShow.transform.position);

                iTween.Stop(XP_Effect.gameObject);

                iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeOutElastic));

                iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", XP_Effect.transform.position.x, "y", XP_Effect.transform.position.y + 50, "z", 0, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

                iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", Main_XP_ToShow.transform.position.x, "y", Main_XP_ToShow.transform.position.y, "z", 0, "delay", 1.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

                iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "delay", 2f, "time", 0.5f, "easetype", iTween.EaseType.linear, "oncomplete", "MainXpCalculation", "oncompletetarget", this.gameObject));
            }
        }
        else
        {
            for (int i = 0; i < TaskList.Count; i++)
            {
                AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(true);
                AllTaskData[i].gameObject.transform.GetChild(2).gameObject.SetActive(false);
            }

            localGainedXpValueText.text = localTotalXpValue.ToString();
        }
    }

    public void CloseAllpanels()
    {

        ControlPanel.GetComponent<Animator>().Play("CpanelFullClose");
        LocalXP.GetComponent<Animator>().Play("LocalXpPanelClose");
        Main_Xp.GetComponent<Animator>().Play("MainXpPanelClose");
        ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelClose");
        InfoButton.GetComponent<Animator>().Play("InfoButtonClose");
        MainSlider.GetComponent<Animator>().Play("MainSliderClose");

        if (TasklistPanel.gameObject.transform.localScale.y != 0)
            iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));
    }

    public void OpenAllpanels()
    {

        if (TopicToCheck <= (TaskList.Count))
        {
            LocalXP.GetComponent<Animator>().Play("LocalXpPanelOPen");
            ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen");
        }
        ControlPanel.GetComponent<Animator>().Play("CpanelOpen");

        Main_Xp.GetComponent<Animator>().Play("MainXpPanelOpen");

        InfoButton.GetComponent<Animator>().Play("InfoButtonOpen");
        MainSlider.GetComponent<Animator>().Play("MainSliderOpen");
        iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "time", 0.2f, "easetype", iTween.EaseType.linear));
        iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));
    }

    public void AddXpEffect()
    {

        if (TopicToCheck <= (TaskList.Count))
        {

            GameObject.Find("Canvas").GetComponent<CanvasGroup>().interactable = false;

            XP_Effect.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);

            XP_Effect.text = "+" + int.Parse(XpList[TopicToCheck - 1]) + " XP";

            XP_Effect.transform.localScale = new Vector3(1, 0, 1);

            iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeOutElastic));

            iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", XP_Effect.transform.position.x, "y", XP_Effect.transform.position.y + 50, "z", 0, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

            iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", XP_ToShow.transform.position.x, "y", XP_ToShow.transform.position.y, "z", 0, "delay", 1.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

            iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "delay", 2f, "time", 0.5f, "easetype", iTween.EaseType.linear, "oncomplete", "LocalXpCalculation", "oncompletetarget", this.gameObject));

            TopicToCheck++;

            ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelClose");

            Invoke("EnableRayCast", 3);


        }

    }

    public void EnableRayCast()
    {
        //Camera.main.GetComponent<PhysicsRaycaster>().enabled = true;
        GameObject.Find("Canvas").GetComponent<CanvasGroup>().interactable = true;
        if (TopicToCheck <= (TaskList.Count))
        {
            ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen");
        }
    }

    public void MainXpCalculation()
    {
        if (OptionUnlockedTill == 0)
        {
            OptionUnlockedTill = 1;

            MainXpValue += localTotalXpValue;

            Main_XP_ToShow.text = MainXpValue.ToString();

            LocalXP.GetComponent<Animator>().Play("LocalXpPanelClose");

            ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelClose");

            if ((ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelClose")) || (ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelFullClose")))
            {
                ControlPanel.GetComponent<Animator>().Play("CpanelOpen");
            }
        }
    }

    public void CloseInfoPanel()
    {

       // AddXpEffect();
        sessionStart = true;
        InfoPanelBg.SetActive(false);
        iTween.ScaleTo(InformationPanel.gameObject, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));
        OpenAllpanels();
    }

    public void ContinueInfoPanel()
    {
        sessionStart = true;
        InfoPanelBg.SetActive(false);
        iTween.ScaleTo(InformationPanel.gameObject, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));
        InfoCloseBtn.transform.localScale = new Vector3(0.27f, 0.27f, 0.27f);
        InfoContinueBtn.transform.localScale = new Vector3(0, 0, 0);
       OpenAllpanels();
    }

    public void OpenInfoPanel()
    {
        sessionStart = false;
        iTween.ScaleTo(InformationPanel.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));
        InfoPanelBg.SetActive(true);
        iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "time", 0.2f, "easetype", iTween.EaseType.linear));
        iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));
        CloseAllpanels();
    }

    public void OpenTaskList()
    {

        if (TasklistPanel.gameObject.transform.localScale.y != 1)
        {
            iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.01f, "time", 0.2f, "easetype", iTween.EaseType.easeInSine));

            ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelClose");

            iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 180, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));

            if ((ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelOpen")))
            {
                ControlPanel.GetComponent<Animator>().Play("CpanelClose");
            }
        }
        else
        {
            iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "delay", 0.01f, "time", 0.2f, "easetype", iTween.EaseType.easeInSine));
            iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));

            ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen");

            ControlPanel.GetComponent<Animator>().Play("CpanelOpen");
        }
    }

    public void OpenOrCloseControlPanel()
    {

        if ((ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelClose")) || (ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelFullClose")))
        {
            ControlPanel.GetComponent<Animator>().Play("CpanelOpen");
        }
        else
        {
            ControlPanel.GetComponent<Animator>().Play("CpanelClose");
        }
    }

    public void WrongOptionSelected()
    {
        WronOptionPanel.transform.localScale = new Vector3(0, 0, 0);

        iTween.Stop(WronOptionPanel.gameObject);

        iTween.ScaleTo(WronOptionPanel.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.01f, "time", 0.2f, "easetype", iTween.EaseType.easeInSine));

        iTween.ScaleTo(WronOptionPanel.gameObject, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 1f, "time", 0.2f, "easetype", iTween.EaseType.easeInSine));

        // InfoPanelBg.SetActive(true);

        Invoke("CloseBlurBg", 1f);

        GameObject.Find("Canvas").GetComponent<CanvasGroup>().interactable = false;

        ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelClose");


        var mouse = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);

        if (mouse.x < Screen.width / 2)
        {
            // WronOptionPanel
            //print("Mouse is on left side of screen.");
            WronOptionPanel.GetComponent<RectTransform>().pivot = new Vector2(0, 0);
        }

        if (mouse.x > Screen.width / 2)
        {
            //print("Mouse is on right side of screen.");
            WronOptionPanel.GetComponent<RectTransform>().pivot = new Vector2(1, 0);
        }


        WronOptionPanel.transform.position = Input.mousePosition;

    }
    public void CloseBlurBg()
    {
        InfoPanelBg.SetActive(false);
        GameObject.Find("Canvas").GetComponent<CanvasGroup>().interactable = true;

        if (OptionUnlockedTill < 1)
        {
            if (InformationPanel.gameObject.transform.localScale.x == 0f)
            {
                ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen", 0, 0f);
            }
        }
    }
    public void OnPointerDown(PointerEventData eventData)
    {
        if (InformationPanel.gameObject.transform.localScale.x != 1 && sessionStart)
        {
            if (OptionUnlockedTill < 1)
            {
                if (TopicToCheck <= (TaskList.Count))
                {
                    iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "time", 0.2f, "easetype", iTween.EaseType.linear));
                    iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));
                    ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen");
                    ControlPanel.GetComponent<Animator>().Play("CpanelOpen");
                }
            }
        }
    }


    public void LabelsAssignment()
    {
        GameObject Label = GameObject.Find("Labels");

        Labels = new GameObject[Label.transform.childCount];

        for (int j = 0; j < Labels.Length; j++)
        {
            Labels[j] = Label.transform.GetChild(j).gameObject;

            GameObject TempLabels = Labels[j];

            foreach (Transform ChildLabel in TempLabels.transform)
            {
                if (ChildLabel.name.Contains("SmoothLook"))
                {
                    ChildLabel.gameObject.AddComponent<SmoothLook>();
                    //ChildLabel.gameObject.AddComponent<ScaleRelativeToCamera>();
                }
            }
        }

        for (int k = 0; k < Labels.Length; k++)
        {
            string name = Labels[k].name.Remove(Labels[k].name.Length - 6);
            Labels[k].SetActive(false);
            if (GameObject.Find(name + "") != null)
            {
                GameObject dummy = GameObject.Find(name + "").gameObject;

                Labels[k].transform.parent = dummy.transform;
                Labels[k].transform.rotation = Quaternion.identity;
            }
        }


        GameObject graphLbl = GameObject.Find("GraphLabels");

        //grapLabels = new GameObject[graphLbl.transform.childCount];

        // for (int j = 0; j < grapLabels.Length; j++)
        {
            // grapLabels[j] = graphLbl.transform.GetChild(j).gameObject;

            //GameObject TempLabels = grapLabels[j];

            //foreach (Transform ChildLabel in TempLabels.transform)
            //{
            //if (ChildLabel.name.Contains("SmoothLook"))
            {
                //ChildLabel.gameObject.AddComponent<SmoothLook>();
                // ChildLabel.gameObject.AddComponent<ScaleRelativeToCamera>();
            }
        }
    }

    // for (int k = 0; k < grapLabels.Length; k++)
    // {
    //  string name = grapLabels[k].name.Remove(grapLabels[k].name.Length - 6);

    //  if (GameObject.Find(name + "") != null)
    //{
    // GameObject dummy = GameObject.Find(name + "").gameObject;
    //  grapLabels[k].transform.parent = dummy.transform;
    // grapLabels[k].transform.rotation = Quaternion.identity;
    //}
    // }
    // }

    public void ToggleActivateLabels()
    {
        if (!Lab)
        {
            Lab = true;
            for (int i = 0; i < Labels.Length; i++)
            {
                Labels[i].SetActive(true);
            }
        }
        else
        {
            Lab = false;
            for (int i = 0; i < Labels.Length; i++)
            {
                Labels[i].SetActive(false);
            }

        }
    }

    public void AddListener(EventTriggerType eventType, Action MethodToCall, GameObject TriggerObjToAdd)
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall());
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }

    public void AddListener(EventTriggerType eventType, Action<float> MethodToCall, GameObject TriggerObjToAdd)
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall(TriggerObjToAdd.GetComponent<Slider>().value));
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }

    public void EnableOrbit()
    {
        if (!IsAR)
        {
            CamOrbit.DisableInteration = false;
        }
        else
        {
            ArOrbit.DisableInteration = false;
        }

        if (OptionUnlockedTill < 1)
        {
            if (InformationPanel.gameObject.transform.localScale.x != 1)
            {
               if (TopicToCheck <= (TaskList.Count))
               {
                    iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "time", 0.2f, "easetype", iTween.EaseType.linear));
                   iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));
                    ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen");
                }
            }
        }
    


        if (OptionUnlockedTill < 1)
        {
           if (TopicToCheck == 3)
            {
                AddXpEffect();
            }
           else
            {
                //Slider_Resistance.value = 0.7f;
                WrongOptionSelected();
                WronOptionPanel.GetComponentInChildren<Text>().text = "Wrong parameter chosen";
            }
        }
   }

    public void DisableOrbit()
    {
        if (!IsAR)
        {
            CamOrbit.DisableInteration = true;
        }
        else
        {
            ArOrbit.DisableInteration = true;
        }

    }

    public Vector3 MidPoint(GameObject obj1, GameObject obj2)
    {
        Vector3 midp = (obj1.transform.localPosition + obj2.transform.localPosition) / 2;
        return midp;
    }

    public float Scale(GameObject obj1, GameObject obj2)
    {
        float scl = Vector3.Distance(obj1.transform.localPosition, obj2.transform.localPosition);
        return scl;
    }

    public void ChangeBallMass(int IncrVal)
    {
        if (OptionUnlockedTill < 1)
        {
            if (TopicToCheck == 2)
            {
                AddXpEffect();
                IncrVal += noOfVolts;

                IncrVal = Mathf.Clamp(IncrVal, 1, 3);

                noOfVolts = IncrVal;

                V_valTxt.text = (IncrVal).ToString();

                for (int i = 0; i < Batteries.transform.childCount; i++)
                {
                    Batteries.transform.GetChild(i).gameObject.SetActive(false);
                }

                for (int i = 0; i < IncrVal; i++)
                {
                    Batteries.transform.GetChild(i).gameObject.SetActive(true);
                }

                //VMeter_Arrow.transform.localEulerAngles = new Vector3(-45, 0, -60 + noOfVolts * 30);

                V = noOfVolts * 1.5f;

                VMeter_Arrow.transform.localEulerAngles = new Vector3(-45, 0, 12.666f * V - 38f);

                Currentcalc();
            }
            else
            {
                WrongOptionSelected();
                WronOptionPanel.GetComponentInChildren<Text>().text = "Wrong parameter chosen";
            }
        }
        else
        {

            IncrVal += noOfVolts;

            IncrVal = Mathf.Clamp(IncrVal, 1, 3);

            noOfVolts = IncrVal;

            V_valTxt.text = (IncrVal).ToString();

            for (int i = 0; i < Batteries.transform.childCount; i++)
            {
                Batteries.transform.GetChild(i).gameObject.SetActive(false);
            }

            for (int i = 0; i < IncrVal; i++)
            {
                Batteries.transform.GetChild(i).gameObject.SetActive(true);
            }

            //VMeter_Arrow.transform.localEulerAngles = new Vector3(-45, 0, -60 + noOfVolts * 30);

            V = noOfVolts * 1.5f;

            VMeter_Arrow.transform.localEulerAngles = new Vector3(-45, 0, 12.666f * V - 38f);

            Currentcalc();
        }

    }

    public void ChangeDistance(float val, Text txt)
    {

        txt.text = (val * 10).ToString("F1") + " Ω";

        R = val * 10;

        float pos = val * (-0.3833f) + 0.2183f;

        if (swtch)
        {
            Rheostat.transform.GetChild(1).gameObject.transform.localPosition = new Vector3(pos, Rheostat.transform.GetChild(1).gameObject.transform.localPosition.y, Rheostat.transform.GetChild(1).gameObject.transform.localPosition.z);
            Currentcalc();
        }
    }

    public void Currentcalc()
    {
        I = V / R;

        float P = V * R;
        print("P");
        result_val.text = P.ToString("F2") + " J";
        popup.transform.GetChild(2).GetComponent<TextMeshPro>().text="P ="+ P.ToString("F2") + " J";



        //Graphpoint.transform.localPosition = new Vector3(-I*0.1f, V*0.1f, 0);

        // Vector3 targetDir = Graph.transform.localPosition + Graphpoint.transform.localPosition;

        //float ang = Vector3.Angle(Graph.transform.localPosition, Graphpoint.transform.localPosition);

        // GraphLine.transform.localEulerAngles = new Vector3(0, 0, ang-90);

        //GraphLine.transform.localScale = new Vector3(Scale(graphOrigin, Graphpoint), GraphLine.transform.localScale.y, GraphLine.transform.localScale.z);

        for (int i = 0; i < Wires_Anim.transform.childCount; i++)
        {
            Wires_Anim.transform.GetChild(i).gameObject.GetComponent<OhmsLawLinesRender1>().scrollSpeed = I*2;
        }

        if (swtch)
        {
            AMeter_Arrow.transform.localEulerAngles = new Vector3(-45, 0, 12.666f * I - 38f);
            VMeter_Arrow.transform.localEulerAngles = new Vector3(-45, 0, 12.666f * V - 38f);
            RValue.text = R.ToString("F1") + " Ω";
            IValue.text = I.ToString("F1") + " A";
            VValue.text = V.ToString("F1") + " V";
            result_val.text = P.ToString("F2") + " J";
            popup.transform.GetChild(2).GetComponent<TextMeshPro>().text = "P =" + P.ToString("F2") + " J";
            BulbLight.GetComponent<Light>().intensity = 5+I * 5;

           // Bulb3.GetComponent<Renderer>().material.color = new Color(0.5f + 0.5f + I, 0, 0.5f + I, 0.2f);

           // BulbShining.GetComponent<Renderer>().material.color = new Color(1, 1, 1, I *0.2f);

            Bulb3.GetComponent<Renderer>().material.color = new Color(0, 0, 1, 1);

            BulbShining.GetComponent<Renderer>().material.color = new Color(0, 0, 1, 1);
        }
        else
        {
            AMeter_Arrow.transform.localEulerAngles = new Vector3(-45, 0, -38f);
            VMeter_Arrow.transform.localEulerAngles = new Vector3(-45, 0, -38f);

            RValue.text = "0 Ω";
            IValue.text = "0 A";
            VValue.text = "0 V";
            V_valTxt.text = "0";

        }
    }

    public void SwitchOn()
    {


        if (OptionUnlockedTill < 1)
        {

            if (TopicToCheck == 1)
            {
                AddXpEffect();

                Wires_Anim.SetActive(true);
                BulbLight.SetActive(true);
                AMeter_Arrow.transform.localEulerAngles = new Vector3(-45, 0, 12.666f * I - 38f);
                VMeter_Arrow.transform.localEulerAngles = new Vector3(-45, 0, 12.666f * V - 38f);

                BulbLight.GetComponent<Light>().intensity = 5 + I * 5;


                //Bulb3.GetComponent<Renderer>().material.color = new Color(0.5f + I, 0, 0.5f + I, 0.2f);

               // BulbShining.GetComponent<Renderer>().material.color = new Color(1, 1, 1, I * 0.2f);

                Bulb3.GetComponent<Renderer>().material.color = new Color(0, 0, 1, 1);

                BulbShining.GetComponent<Renderer>().material.color = new Color(0, 0, 1, 1);

                RValue.text = R.ToString("F1") + " Ω";
                IValue.text = I.ToString("F1") + " A";
                VValue.text = V.ToString("F1") + " V";
                
               Switch.GetComponent<Slider>().value = 1;
                swtch = true;
                iTween.RotateTo(On_Off_Switch, iTween.Hash("x", 0, "y", 90, "z", 0f, "delay", 0.01f, "time", 0.1f, "islocal", true, "easetype", iTween.EaseType.easeInSine));
                Currentcalc();
            }
            else {
                WrongOptionSelected();
                WronOptionPanel.GetComponentInChildren<Text>().text = "Wrong parameter chosen";
            }

        }

        else
        {
            if (swtch)
            {
                BulbLight.SetActive(false);
                Switch.GetComponent<Slider>().value = 0;
                Slider_Resistance.GetComponent<Slider>().value = 0;
                AMeter_Arrow.transform.localEulerAngles = new Vector3(-45, 0, -38f);
                VMeter_Arrow.transform.localEulerAngles = new Vector3(-45, 0, -38f);

                RValue.text = "0 Ω";
                IValue.text = "0 A";
                VValue.text = "0 V";
                result_val.text = "0 J";
                V_valTxt.text = "0";
               
                float P;
       
                popup.transform.GetChild(2).GetComponent<TextMeshPro>().text= "P = 0 J";

                // Bulb3.GetComponent<Renderer>().material.color = new Color(1, 1, 1, 0.2f);
                //BulbShining.GetComponent<Renderer>().material.color = new Color(1, 1, 1, 0);
                Bulb3.GetComponent<Renderer>().material.color = new Color(0, 0, 1, 1);
                BulbShining.GetComponent<Renderer>().material.color = new Color(0, 0, 1, 1);
                swtch = false;
                iTween.RotateTo(On_Off_Switch, iTween.Hash("x", 0, "y", 90, "z", -45f, "delay", 0.01f, "time", 0.1f, "islocal", true, "easetype", iTween.EaseType.easeInSine));
                Wires_Anim.SetActive(false);
                //Graph.SetActive(false);
                //ShowGraphbtn.GetComponentInChildren<Text>().text = "Show Graph";
            }
            else
            {
                Wires_Anim.SetActive(true);
                BulbLight.SetActive(true);
                AMeter_Arrow.transform.localEulerAngles = new Vector3(-45, 0, 12.666f * I - 38f);
                VMeter_Arrow.transform.localEulerAngles = new Vector3(-45, 0, 12.666f * V - 38f);

                BulbLight.GetComponent<Light>().intensity = 5 + I * 5;
               // Bulb3.GetComponent<Renderer>().material.color = new Color(0.5f + 0 + I, 0.5f + I, 0.2f);

                //BulbShining.GetComponent<Renderer>().material.color = new Color(1, 1, 1, I * 0.2f);

                Bulb3.GetComponent<Renderer>().material.color = new Color(0, 0, 1, 1);

                BulbShining.GetComponent<Renderer>().material.color = new Color(0, 0, 1, 1);

                RValue.text = R.ToString("F1") + " Ω";
                IValue.text = I.ToString("F1") + " A";
                VValue.text = V.ToString("F1") + " V";

                Switch.GetComponent<Slider>().value = 1;
                swtch = true;
                iTween.RotateTo(On_Off_Switch, iTween.Hash("x", 0, "y", 90, "z", 0f, "delay", 0.01f, "time", 0.1f, "islocal", true, "easetype", iTween.EaseType.easeInSine));
            }
        }
    }

   // public void OnGraph() {

       // if (OptionUnlockedTill < 1)
        //{
          //  if (TopicToCheck == 4)
            //{
               // AddXpEffect();
               // Graph.SetActive(true);
               // ShowGraphbtn.GetComponentInChildren<Text>().text = "Hide Graph";
            }
            //else
           // {
              //  Slider_Resistance.value = 0.1f;
               // WrongOptionSelected();
               // WronOptionPanel.GetComponentInChildren<Text>().text = "Wrong parameter chosen";
           // }
        //}
        //else
       // {
           // if (Graph.activeSelf)
           // {
             //   ShowGraphbtn.GetComponentInChildren<Text>().text = "Show Graph";
              //  Graph.SetActive(false);
           // }
            //else
            //{
                //if (swtch)
               // {
                   // Graph.SetActive(true);
                   // ShowGraphbtn.GetComponentInChildren<Text>().text = "Hide Graph";
                //}
               // else
                //{
                 ///   WrongOptionSelected();
                    //WronOptionPanel.GetComponentInChildren<Text>().text = "Tap on Switch";
               // }
          //  }
      //  }
    //}
//}

public class OhmsLawLinesRender1: MonoBehaviour
{
    Renderer rend;
    public float offset, scrollSpeed = 4f, dir = 1;
    void Start()
    {
        //dir = 1;
        rend = GetComponentInChildren<Renderer>();
        offset = 1f;
        rend.materials[0].SetTextureOffset("_MainTex", new Vector2(0, 1f));
    }

    void Update()
    {
        offset += Time.deltaTime * scrollSpeed;
        rend.materials[0].SetTextureOffset("_MainTex", new Vector2(0, -dir * offset));
        

        //rend.material.SetTextureScale("_MainTex", new Vector2(1, 500 * transform.localScale.x));
    }
}