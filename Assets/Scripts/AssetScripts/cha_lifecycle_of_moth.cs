﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;
using System.Text.RegularExpressions;

public class cha_lifecycle_of_moth : MonoBehaviour, IPointerDownHandler
{
    // Start is called before the first frame update
    public static cha_lifecycle_of_moth Instance;
    public TextAsset CSVFile;
    public AssetBundle assetBundle;
    public Canvas C_BG;

    public orbit CamOrbit;

    public AROrbitControls ArOrbit;

    public bool IsAR;

    public GameObject MainObj, Ground;

    public int OptionUnlockedTill, CurrentTopicSelected, TopicToCheck;
    public float Sub_XP_Earned, Total_XP;

    public List<Dictionary<string, object>> CSV_data;

    public List<String> TaskList,XpList,InfoText;

    public List<GameObject> AllTaskData;

    public GameObject[] Labels;

    public Text XP_ToShow, XP_Effect, Main_XP_ToShow, localTotalXpValueText, localGainedXpValueText, ObjectiveInfoText, LGListText, IListText, AListText, 
        InfoHeadingText, GF_ResultTxt, E_valueTxt, m_valtxt, NumOfSteps;

    public GameObject TaskObj, AllTaskListObj;
    public RectTransform rT;
    public int localTotalXpValue, localGaniedXpValue,MainXpValue;
    public GameObject ControlPanel, LocalXP, Main_Xp, ObjectivePanel, InformationPanel, InfoButton, MainSlider, InfoPanelBg, TasklistPanel, WronOptionPanel, Gained_XP_ArrowBut,PopUpPanel, InventoryScroll,InventryPanel;
    public Button InfoCloseBtn, InfoContinueBtn, Gained_XP_Panel, CpCloseBtn, IncButton, DecButton,RestartButton;
    public bool sessionStart, label_onbool;
    public Vector3  TargetPos;
    /// <summary>
    /// Topic variable
    /// </summary>
    public int CurrentStepCnt, DragDropSteps;
    public GameObject arroweffects, placeholders,moth1,info_panels,coco_obj;
    public Canvas UIcanvas;
    public float blend_value;
    public TextMeshPro info_panel_1_txt;
    
   
    //LerpTime = (Vector3.Distance(Parts[i].transform.position, CamOrbit.target.position)) / 0.05f;
    void Start()
    {
        CurrentStepCnt = 0;
        Instance = this;
        
       // start_bool = false;
        
        LoadFromAsssetBundle();
        
        sessionStart = false;
        //OptionUnlockedTill = 0;
        //TopicToCheck = 1;
        //MainXpValue = 10;
        label_onbool = false;

        MainObj = this.gameObject;

        C_BG = GameObject.Find("Canvas_BG").GetComponent<Canvas>();

       /// Ground = GameObject.Find("Ground");

        if (!IsAR)
        {
            CamOrbit = Camera.main.GetComponentInParent<orbit>();

            C_BG.transform.parent = Camera.main.transform;
            C_BG.GetComponentInChildren<RawImage>().enabled = true;
            C_BG.renderMode = RenderMode.ScreenSpaceCamera;
           // C_BG.gameObject.SetActive(false);
            C_BG.worldCamera = Camera.main;
          
            CamOrbit.target = Camera.main.transform.parent;
            CamOrbit.target.position = new Vector3(0, 0, 0);
            
           CamOrbit.maxRotUp = 45;
            CamOrbit.minRotUp = 1;
            CamOrbit.zoomMin = 0.3f;
            CamOrbit.zoomMax = .9f;
            CamOrbit.zoomStart = 0.58f;
            CamOrbit.cameraRotSideStart = 1.5f;
            CamOrbit.cameraRotUpStart = 45f;
            CamOrbit.Start();
        }
        else
        {
           /// Ground.SetActive(false);

            C_BG.gameObject.SetActive(false);

            ArOrbit = MainObj.transform.parent.gameObject.GetComponent<AROrbitControls>();
        }

        ReadingDataFromCSVFile();
        ReadingXps();
        FindingObjects();
        TaskListCreation();
        AssigningClickEvents();
       // LabelsAssignment();
        AssignInitialValues(); 

        LocalXpCalculation();

    }
    public void LoadFromAsssetBundle()
    {
        ///if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)
        // {
        assetBundle = AssetBundle.LoadFromFile(PlayerPrefs.GetString("AssetBundleUrl"));

        CSVFile = assetBundle.LoadAsset(PlayerPrefs.GetString("LibraryName") + "CSV", typeof(TextAsset)) as TextAsset;
        // }
        //else if (Application.platform == RuntimePlatform.Android)
        //{
        //    CSVFile = Resources.Load("lifecycle_of_mothCSV") as TextAsset;
        //}
        //else
        //{     

       // CSVFile = Resources.Load("cha_lifecycle_of_mothCSV") as TextAsset;
        //}

        Camera.main.gameObject.AddComponent<PhysicsRaycaster>();

        Camera.main.backgroundColor = Color.black;

        GameObject TargetForCamera = GameObject.Find("Target");

        if (TargetForCamera != null)
        {
            TargetForCamera.AddComponent<orbit>();
            CamOrbit = TargetForCamera.GetComponent<orbit>();
            IsAR = false;
        }
        else
        {
            this.transform.parent.gameObject.AddComponent<AROrbitControls>();
            ArOrbit = this.transform.parent.gameObject.GetComponent<AROrbitControls>();
            IsAR = true;
        }

    }

    public void ReadingDataFromCSVFile()
    {             
      
        TaskList = new List<string>();
        XpList = new List<string>();
        InfoText = new List<string>();

        AllTaskData = new List<GameObject>();

        CSV_data = CSVReader.Read(CSVFile);

          
        for (var i = 0; i < CSV_data.Count; i++)
        {            

            if (CSV_data[i]["TaskList"].ToString() != "" && CSV_data[i]["TaskList"].ToString() != null)
            {
                TaskList.Add(CSV_data[i]["TaskList"].ToString());
               
            }
            //if (CSV_data[i]["XPList"].ToString() != "" && CSV_data[i]["XPList"].ToString() != null)
            //{
            //    XpList.Add(CSV_data[i]["XPList"].ToString());

            //}

            //if (CSV_data[i]["InfoText"].ToString() != "" && CSV_data[i]["InfoText"].ToString() != null)
            //{
            //    InfoText.Add(CSV_data[i]["InfoText"].ToString());

            //}
        }
    }

    public int totXp, loclXp;

    public void ReadingXps()
    {

      //  PlayerPrefs.SetInt("TotalXP", 100);
      //  PlayerPrefs.SetInt("MaxEarnings", 20);

        totXp = PlayerPrefs.GetInt("TotalXP");
        loclXp = PlayerPrefs.GetInt("MaxEarnings");

        if (loclXp != 0)
        {
            OptionUnlockedTill = 0;

        }
        else
        {
            OptionUnlockedTill = 1;

        }

        TopicToCheck = 1;

        MainXpValue = totXp;

        int t = 0;
        int p = 0;

        for (int k = 0; k < TaskList.Count; k++)
        {

            p = loclXp / TaskList.Count;
            XpList.Add(p.ToString());
            t = t + p;
        }

        if (t != loclXp)
        {
            XpList[XpList.Count - 1] = (p + (loclXp - t)).ToString();
        }
    }

    public float result;

    public void GetResult()
    {
        result = ((float)(TopicToCheck - 1) / (float)(TaskList.Count)) * 100;
        Camera.main.SendMessage("UnloadActually", result);
    }

    public void FindingObjects() {

        InformationPanel = GameObject.Find("InformationPanel");

        InfoCloseBtn = GameObject.Find("InfoCloseBtn").GetComponent<Button>();

        InfoContinueBtn = GameObject.Find("InfoContinueBtn").GetComponent<Button>();

        InfoPanelBg = GameObject.Find("InfoPanelBg");
        
        XP_ToShow = GameObject.Find("localGainedXpValueText").GetComponent<Text>();

        XP_Effect = GameObject.Find("Effect_Text").GetComponent<Text>();

        Main_XP_ToShow = GameObject.Find("Main_XP_Value").GetComponent<Text>();

        localGainedXpValueText = GameObject.Find("localGainedXpValueText").GetComponent<Text>();

        localTotalXpValueText = GameObject.Find("GainedXP_LocalTotalXp").GetComponent<Text>();

        ObjectiveInfoText = GameObject.Find("ObjectiveInfoText").GetComponent<Text>();

        TaskObj = GameObject.Find("TaskObj");

        AllTaskListObj = GameObject.Find("AllTaskList");

        ControlPanel = GameObject.Find("ControlPanel");
        LocalXP = GameObject.Find("LocalXP");
        Main_Xp = GameObject.Find("Main_Xp");
        ObjectivePanel = GameObject.Find("ObjectivePanel");
        InformationPanel = GameObject.Find("InformationPanel");
        InfoButton = GameObject.Find("InfoButton");
        MainSlider = GameObject.Find("MainSlider");
        WronOptionPanel = GameObject.Find("WrongPanel");

        Gained_XP_Panel = GameObject.Find("Gained_XP_Panel").GetComponent<Button>();
        TasklistPanel = GameObject.Find("TasklistPanel");
        CpCloseBtn = GameObject.Find("CpCloseBtn").GetComponent<Button>();

        InfoHeadingText = GameObject.Find("InfoHeadingText").GetComponent<Text>();
        LGListText = GameObject.Find("LGListText").GetComponent<Text>();
        IListText = GameObject.Find("IListText").GetComponent<Text>();
        AListText = GameObject.Find("AListText").GetComponent<Text>();
        PopUpPanel = GameObject.Find("PopUpPanel");
        InventryPanel = GameObject.Find("RightScroll");
        NumOfSteps = GameObject.Find("steps").GetComponent<Text>();

         Gained_XP_ArrowBut = GameObject.Find("Gained_XP_ArrowBut");
        InventoryScroll = GameObject.Find("InventoryScrollView");
        UIcanvas = GameObject.Find("Canvas").GetComponent<Canvas>();
        DragDropSteps = UIcanvas.GetComponentsInChildren<UIDragAndDrop>().Length;

        NumOfSteps.text = DragDropSteps.ToString();
        arroweffects = GameObject.Find("ArrowEffects");
        RestartButton = GameObject.Find("ButtonRestart").GetComponent<Button>();

        placeholders = GameObject.Find("Placeholders");
       
        for(int i = 0; i < placeholders.transform.childCount; i++)
        {
            placeholders.transform.GetChild(i).gameObject.SetActive(false);
        }
        for (int i = 0; i < arroweffects.transform.childCount; i++)
        {
            arroweffects.transform.GetChild(i).gameObject.transform.GetChild(0).GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(0, 10);
            arroweffects.transform.GetChild(i).gameObject.GetComponent<Animator>().enabled = false;
        }

        moth1 = GameObject.Find("moth1");
        info_panel_1_txt = GameObject.Find("Label_Desc_Text_1").GetComponent<TextMeshPro>();
        info_panel_1_txt.text = "moth feeds on the mulberry leaf and produces eggs.";
        info_panels = GameObject.Find("Infopanel");
        for(int i = 0; i < info_panels.transform.childCount; i++)
        {
            info_panels.transform.GetChild(i).gameObject.SetActive(false);
            info_panels.transform.GetChild(i).transform.GetChild(0).gameObject.AddComponent<SmoothLook>();
        }
        info_panels.transform.GetChild(0).gameObject.SetActive(true);
        coco_obj = GameObject.Find("coco_Final");
        coco_obj.GetComponent<Animator>().Play("coco_normal_green");
        RestartButton.GetComponent<Animator>().Play("RestartClose");
        //iTween.MoveTo(RestartButton.gameObject, iTween.Hash("x", -200, "y", 230, "z", 0, "delay", 0.5f, "time", 0.5f, "islocal", true, "easetype", iTween.EaseType.easeInSine));
        //sugar_liquid.transform.localScale = new Vector3(0, 0, 0);
        //lim_liquid.transform.localScale = new Vector3(0, 0, 0);
        //but_start = GameObject.Find("Start");
        //effects = GameObject.Find("Effect");
        //sugar_liquid.GetComponent<Animator>().Play("sugar_start");
        //lim_liquid.GetComponent<Animator>().Play("lim_start");
        //effects.SetActive(false);
    }


    public void AssigningClickEvents() {

        iTween.ScaleTo(InformationPanel.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

        InfoCloseBtn.onClick.AddListener(() => CloseInfoPanel());

        InfoContinueBtn.onClick.AddListener(() => ContinueInfoPanel());

        InfoButton.GetComponent<Button>().onClick.AddListener(() => OpenInfoPanel());

        InfoButton.transform.GetChild(0).GetComponent<Button>().onClick.AddListener(() => Labels_on());
        Gained_XP_Panel.onClick.AddListener(() => OpenTaskList());

        CpCloseBtn.onClick.AddListener(() => OpenOrCloseControlPanel());

        //  MainSlider.GetComponent<Slider>().onValueChanged.AddListener(delegate { ChangeDistance(MainSlider.GetComponent<Slider>().value, MainSlider.GetComponentInChildren<Text>()); });

        AddListener(EventTriggerType.PointerDown, DisableOrbit, MainSlider.gameObject);

        AddListener(EventTriggerType.PointerUp, EnableOrbit, MainSlider.gameObject);

        //Slider_distance.onValueChanged.AddListener(delegate { ChangeDistance(Slider_distance.value, Slider_distance.GetComponentInChildren<Text>()); });

        AddListener(EventTriggerType.PointerDown, DisableOrbit, ControlPanel.transform.GetChild(0).gameObject);

        AddListener(EventTriggerType.PointerUp, EnableOrbit, ControlPanel.transform.GetChild(0).gameObject);

        AddListener(EventTriggerType.PointerUp, RadiusAddXp, MainSlider.gameObject);
        AddListener(EventTriggerType.PointerDown, EnableOrbit, InventoryScroll);
        AddListener(EventTriggerType.PointerUp, DisableOrbit, InventoryScroll);
        AddListener(EventTriggerType.PointerDown, EnableOrbit, InventoryScroll.transform.GetChild(2).gameObject);
        AddListener(EventTriggerType.PointerUp, DisableOrbit, InventoryScroll.transform.GetChild(2).gameObject);

        RestartButton.onClick.AddListener(() => ResetExperiment());
        // but_start.GetComponent<Button>().onClick.AddListener(() => startReaction());

        // EventTrigger.Entry PointerDownentrylabel = new EventTrigger.Entry();
        //  PointerDownentrylabel.eventID = EventTriggerType.PointerDown;
        // PointerDownentrylabel.callback.AddListener((data) => { Label_Decr_on(); });
        // for (int i = 0; i < orbit_obj.transform.childCount; i++)
        //  {
        //  PointerDownentrylabel.callback.AddListener((data) => { Label_Decr_on(orbit_obj.transform.GetChild(i).transform.GetChild(2).transform.GetChild(0).transform.GetChild(3).gameObject); });
        //  orbit_obj.transform.GetChild(i).transform.GetChild(2).gameObject.GetComponent<EventTrigger>().triggers.Add(PointerDownentrylabel);
        //  }

        //EventTrigger.Entry PointerDownenPlanet = new EventTrigger.Entry();
        //PointerDownenPlanet.eventID = EventTriggerType.PointerDown;
        //PointerDownenPlanet.callback.AddListener((data) => { onCl(); });

        //but_mass1.transform.GetChild(0).GetComponent<Button>().onClick.AddListener(() => MassIncre_m1());
        //but_mass1.transform.GetChild(2).GetComponent<Button>().onClick.AddListener(() => MassDecre_m1());
        //but_mass2.transform.GetChild(0).GetComponent<Button>().onClick.AddListener(() => MassIncre_m2());
        //but_mass2.transform.GetChild(2).GetComponent<Button>().onClick.AddListener(() => MassDecre_m2());
        //but_mass3.transform.GetChild(0).GetComponent<Button>().onClick.AddListener(() => MassIncre_m3());
        //but_mass3.transform.GetChild(2).GetComponent<Button>().onClick.AddListener(() => MassDecre_m3());
        // AddListener(EventTriggerType.PointerUp, DistanceAddXp, Slider_distance.gameObject);

        //  IncButton.GetComponent<Button>().onClick.AddListener(() => IncDecFunction(IncButton.name));

        //  DecButton.GetComponent<Button>().onClick.AddListener(() => IncDecFunction(DecButton.name));
        //AddListener(EventTriggerType.PointerDown, Switch_on, switch_1);
        //AddListener(EventTriggerType.PointerDown, Switch_on, switch_2);
        //but_group.transform.GetChild(0).GetComponent<Button>().onClick.AddListener(() => ElectricField());
        //but_group.transform.GetChild(1).GetComponent<Button>().onClick.AddListener(() => MagneticField());
        //but_group.transform.GetChild(2).GetComponent<Button>().onClick.AddListener(() => BothField());
        //but_reset.GetComponent<Button>().onClick.AddListener(() => ResetButton());
        //particleSystemForceField = particle_group.transform.GetChild(2).GetComponent<ParticleSystemForceField>();
        //magnet_obj.AddComponent<EventTrigger>();
        //magnet_obj.AddComponent<MaindragAndDrop>();
        //ControlPanel.GetComponent<Animator>().Play("CpanelClose");
        //EventTrigger.Entry PointerDownentryswitch = new EventTrigger.Entry();
        //PointerDownentryswitch.eventID = EventTriggerType.PointerDown;
        //PointerDownentryswitch.callback.AddListener((data) => { Switch_on(); });

        //switch_1.GetComponent<EventTrigger>().triggers.Add(PointerDownentryswitch);
        //switch_2.GetComponent<EventTrigger>().triggers.Add(PointerDownentryswitch);

    }
  

      





    public void AssignInitialValues()
    {

        //density = 1;

        //pi = 3.14f;

        //R = 1;

        //G = 6.67f;

        //r = 1f;


        // InfoHeadingText.text = InfoText[0];
        // LGListText.text = InfoText[1];
        // IListText.text = InfoText[2];
        // AListText.text = InfoText[3];
        if (OptionUnlockedTill > 0)
        {
            LocalXP.transform.localScale = new Vector3(0, 0, 0);
            ObjectivePanel.transform.localScale = new Vector3(0, 0, 0);
        }


        InfoCloseBtn.gameObject.transform.localScale = new Vector3(0, 0, 0);
        localGainedXpValueText.text = "0";
        Main_XP_ToShow.text = MainXpValue + "";
        WronOptionPanel.transform.localScale = new Vector3(0, 0, 0);

       // ChangeDistance(Slider_distance.value, Slider_distance.GetComponentInChildren<Text>());
        //ChangeRadius(MainSlider.GetComponent<Slider>().value, MainSlider.GetComponentInChildren<Text>());
      //  ScannerObject.AddComponent<RendererOffset>();
      //  ScannerObject.GetComponent<RendererOffset>().offset = -0.4f;
     //   ScannerObject.SetActive(false);

       // Labels[1].transform.localPosition = MidPoint(Dummy, Ball);
     //   Labels[0].transform.localPosition = MidPoint(Ball, MTape);
    }

   
    public void TaskListCreation() {

        rT = AllTaskListObj.transform.parent.GetComponent<RectTransform>();

        localGaniedXpValue = 0;

        for (int i = 0; i < TaskList.Count; i++)
        {

            GameObject Tl = (GameObject)Instantiate(TaskObj);
            Tl.transform.SetParent(AllTaskListObj.transform);
            Tl.transform.localScale = new Vector3(1, 1, 1);
            Tl.name = i.ToString();

            AllTaskData.Add(Tl);
            AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(false);
            AllTaskData[i].gameObject.transform.GetChild(2).gameObject.SetActive(false);
            AllTaskData[i].gameObject.transform.GetChild(3).GetComponentInChildren<Text>().text = TaskList[i];
            AllTaskData[i].gameObject.transform.GetChild(4).GetComponentInChildren<Text>().text = "+" + XpList[i] + " XP";

            localTotalXpValue += int.Parse(XpList[i]);
        }

        rT.sizeDelta = new Vector2(rT.sizeDelta.x, TaskList.Count * 50);
        localTotalXpValueText.text = "/" + localTotalXpValue.ToString();

        AllTaskData[0].gameObject.transform.GetChild(2).gameObject.SetActive(true);
        ObjectiveInfoText.text = AllTaskData[0].gameObject.transform.GetChild(3).GetComponentInChildren<Text>().text;
    }

    public void LocalXpCalculation() {

       
            for (int i = 0; i < TaskList.Count; i++)
            {
                AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(false);
                AllTaskData[i].gameObject.transform.GetChild(2).gameObject.SetActive(false);
            }

        if (OptionUnlockedTill < 1)
        {
            if ((TopicToCheck) < (TaskList.Count + 1))
            {
                localGaniedXpValue = 0;

                for (int i = 0; i < TopicToCheck - 1; i++)
                {
                    localGaniedXpValue += int.Parse(XpList[i]);
                }

                for (int i = 0; i < TopicToCheck; i++)
                {
                    AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(true);
                }

                localGainedXpValueText.text = localGaniedXpValue.ToString();

                AllTaskData[TopicToCheck - 1].gameObject.transform.GetChild(2).gameObject.SetActive(true);

                ObjectiveInfoText.text = AllTaskData[TopicToCheck - 1].gameObject.transform.GetChild(3).GetComponentInChildren<Text>().text;
            }
            else
            {
                for (int i = 0; i < TaskList.Count; i++)
                {
                    AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(true);
                    AllTaskData[i].gameObject.transform.GetChild(2).gameObject.SetActive(false);
                }

                localGainedXpValueText.text = localTotalXpValue.ToString();

                XP_Effect.text = "+" + localTotalXpValue + " XP";

                XP_Effect.transform.position = (XP_ToShow.transform.position);

                iTween.Stop(XP_Effect.gameObject);

                iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeOutElastic));

                iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", XP_Effect.transform.position.x, "y", XP_Effect.transform.position.y + 50, "z", 0, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

                iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", Main_XP_ToShow.transform.position.x, "y", Main_XP_ToShow.transform.position.y, "z", 0, "delay", 1.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

                iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "delay", 2f, "time", 0.5f, "easetype", iTween.EaseType.linear, "oncomplete", "MainXpCalculation", "oncompletetarget", this.gameObject));
            }
        }
        else
        {
            for (int i = 0; i < TaskList.Count; i++)
            {
                AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(true);
                AllTaskData[i].gameObject.transform.GetChild(2).gameObject.SetActive(false);
            }

            localGainedXpValueText.text = localTotalXpValue.ToString();   
        }
    }

    public void CloseAllpanels()
    {

        ControlPanel.GetComponent<Animator>().Play("CpanelFullClose");
        LocalXP.GetComponent<Animator>().Play("LocalXpPanelClose");
        Main_Xp.GetComponent<Animator>().Play("MainXpPanelClose");
        ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelClose");
        InfoButton.GetComponent<Animator>().Play("InfoButtonClose");
        MainSlider.GetComponent<Animator>().Play("MainSliderClose");
        InventryPanel.GetComponent<Animator>().Play("InventryClose");
        if (TasklistPanel.gameObject.transform.localScale.y!=0)
            iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));
    }

    public void OpenAllpanels()
    {
        if (OptionUnlockedTill < 1)
        {
            if (TopicToCheck <= (TaskList.Count))
            {
                LocalXP.GetComponent<Animator>().Play("LocalXpPanelOPen");
                ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen");
            }
        }

        ControlPanel.GetComponent<Animator>().Play("CpanelOpen");

        Main_Xp.GetComponent<Animator>().Play("MainXpPanelOpen");
        InventryPanel.GetComponent<Animator>().Play("InventryOpen");
        InfoButton.GetComponent<Animator>().Play("InfoButtonOpen");
        MainSlider.GetComponent<Animator>().Play("MainSliderOpen");
        iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "time", 0.2f, "easetype", iTween.EaseType.linear));
        iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));


    }


    public void AddXpEffect() {

        
        if (TopicToCheck <= (TaskList.Count))
        {
            // Camera.main.GetComponent<PhysicsRaycaster>().enabled = false;

            GameObject.Find("Canvas").GetComponent<CanvasGroup>().interactable = false;

            XP_Effect.GetComponent<RectTransform>().anchoredPosition = new Vector2(0,0);

            XP_Effect.text = "+"+int.Parse(XpList[TopicToCheck - 1]) + " XP";

            XP_Effect.transform.localScale = new Vector3(1, 0, 1);

           // iTween.Stop(XP_Effect.gameObject);


            iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeOutElastic));

            iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", XP_Effect.transform.position.x, "y", XP_Effect.transform.position.y + 50, "z", 0, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

            iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", XP_ToShow.transform.position.x, "y", XP_ToShow.transform.position.y, "z", 0, "delay", 1.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

            iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "delay", 2f, "time", 0.5f, "easetype", iTween.EaseType.linear, "oncomplete", "LocalXpCalculation", "oncompletetarget", this.gameObject));

            TopicToCheck++;

            Invoke("EnableRayCast", 3);
        }
        
    }

    public void EnableRayCast() {
        //Camera.main.GetComponent<PhysicsRaycaster>().enabled = true;
        GameObject.Find("Canvas").GetComponent<CanvasGroup>().interactable = true;
    }

    public IEnumerator MainXpCalculation()
    {
        if (OptionUnlockedTill == 0)
        {
            OptionUnlockedTill = 1;

            MainXpValue += localTotalXpValue;

            Main_XP_ToShow.text = MainXpValue.ToString();

            yield return new WaitForSeconds(1f);

            LocalXP.GetComponent<Animator>().Play("LocalXpPanelClose");

            ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelClose");

            if ((ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelClose")) || (ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelFullClose")))
            {
                ControlPanel.GetComponent<Animator>().Play("CpanelOpen");
            }
        }

    }

    public void CloseInfoPanel()
    {
        sessionStart = true;
        InfoPanelBg.SetActive(false);
        iTween.ScaleTo(InformationPanel.gameObject, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));
        OpenAllpanels();
        ControlPanel.GetComponent<Animator>().Play("CpanelOpen");
    }

    public void ContinueInfoPanel()
    {
        sessionStart = true;
        InfoPanelBg.SetActive(false);
        iTween.ScaleTo(InformationPanel.gameObject, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));
        InfoCloseBtn.transform.localScale = new Vector3(0.27f, 0.27f, 0.27f);
        InfoContinueBtn.transform.localScale = new Vector3(0, 0, 0);
        OpenAllpanels();
    }

    public void OpenInfoPanel()
    {
        sessionStart = false;
        iTween.ScaleTo(InformationPanel.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));
        InfoPanelBg.SetActive(true);
        iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "time", 0.2f, "easetype", iTween.EaseType.linear));
        iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));
        CloseAllpanels();
    }

    public void OpenTaskList() {


        if (TasklistPanel.gameObject.transform.localScale.y != 1)
        {
            iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.01f, "time", 0.2f, "easetype", iTween.EaseType.easeInSine));
            
            ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelClose");


            //Gained_XP_ArrowBut

            iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 180, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));

            if ((ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelOpen")))
            {
                ControlPanel.GetComponent<Animator>().Play("CpanelClose");
            }
        }
        else {
            iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "delay", 0.01f, "time", 0.2f, "easetype", iTween.EaseType.easeInSine));
            iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));

            ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen");

            ControlPanel.GetComponent<Animator>().Play("CpanelOpen");
        }
    }

    public void OpenOrCloseControlPanel() {

        if ((ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelClose")) || (ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelFullClose")))
        {
            ControlPanel.GetComponent<Animator>().Play("CpanelOpen");
        }
        else
        {
            ControlPanel.GetComponent<Animator>().Play("CpanelClose");
        }
    }

    public void WrongOptionSelected()
    {
        WronOptionPanel.transform.localScale = new Vector3(0, 0, 0);

        iTween.Stop(WronOptionPanel.gameObject);

        iTween.ScaleTo(WronOptionPanel.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.01f, "time", 0.2f, "easetype", iTween.EaseType.easeInSine));

        iTween.ScaleTo(WronOptionPanel.gameObject, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 1f, "time", 0.2f, "easetype", iTween.EaseType.easeInSine));

        // InfoPanelBg.SetActive(true);

        Invoke("CloseBlurBg", 1f);

        GameObject.Find("Canvas").GetComponent<CanvasGroup>().interactable = false;

        ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelClose");


        var mouse = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);

        if (mouse.x < Screen.width / 2)
        {
            // WronOptionPanel
            //print("Mouse is on left side of screen.");
            WronOptionPanel.GetComponent<RectTransform>().pivot = new Vector2(0, 0);
        }

        if (mouse.x > Screen.width / 2)
        {
            //print("Mouse is on right side of screen.");
            WronOptionPanel.GetComponent<RectTransform>().pivot = new Vector2(1, 0);
        }


        WronOptionPanel.transform.position = Input.mousePosition;

    }
    public void CloseBlurBg()
    {
        InfoPanelBg.SetActive(false);
        GameObject.Find("Canvas").GetComponent<CanvasGroup>().interactable = true;

        if (OptionUnlockedTill < 1)
        {
            if (InformationPanel.gameObject.transform.localScale.x == 0f)
            {
                ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen", 0, 0f);
            }
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (InformationPanel.gameObject.transform.localScale.x != 1 && sessionStart)
        {
            if (OptionUnlockedTill < 1)
            {
                if (TopicToCheck <= (TaskList.Count))
                {
                    iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "time", 0.2f, "easetype", iTween.EaseType.linear));
                    iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));
                    ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen");
                    ControlPanel.GetComponent<Animator>().Play("CpanelOpen");
                }
            }
        }
    }


    public void LabelsAssignment()
    {
        GameObject Label = GameObject.Find("Labels");

        Labels = new GameObject[Label.transform.childCount];

        for (int j = 0; j < Labels.Length; j++)
        {
            Labels[j] = Label.transform.GetChild(j).gameObject;

            GameObject TempLabels = Labels[j];

            foreach (Transform ChildLabel in TempLabels.transform)
            {
                if (ChildLabel.name.Contains("SmoothLook"))
                {
                    ChildLabel.gameObject.AddComponent<SmoothLook>();
                   // ChildLabel.gameObject.AddComponent<ScaleRelativeToCamera>();

                }
            }
        }
                     
        for (int k = 0; k < Labels.Length; k++)
        {
            string name = Labels[k].name.Remove(Labels[k].name.Length - 6);

            if (GameObject.Find(name + "") != null)
            {
                GameObject dummy = GameObject.Find(name + "").gameObject;

                Labels[k].transform.parent = dummy.transform;
                Labels[k].transform.rotation = Quaternion.identity;
                Labels[k].SetActive(false);
            }


            //TempTextMesh = Labels[k].GetComponentsInChildren<TextMeshPro>();

            //if (TempTextMesh.Length > 1)
            //{
            //    for (int p = 0; p < TempTextMesh.Length; p++)
            //    {
            //        if (DescriTextDoc.ContainsKey(name + "_" + p))
            //            TempTextMesh[p].text = DescriTextDoc[name + "_" + p];
            //    }
            //}
            //else
            //{
            //    if (DescriTextDoc.ContainsKey(name))
            //        Labels[k].GetComponentInChildren<TextMeshPro>().text = DescriTextDoc[name];
            //}
           
        }

      
    }

    public void AddListener(EventTriggerType eventType, Action MethodToCall, GameObject TriggerObjToAdd)
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall());
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }

    public void EnableOrbit()
    {
        if (!IsAR)
        {
            CamOrbit.DisableInteration = false;
        }
        else
        {
            ArOrbit.DisableInteration = false;
        }
        if (OptionUnlockedTill < 1)
        {

            if (InformationPanel.gameObject.transform.localScale.x != 1)
            {

                if (TopicToCheck <= (TaskList.Count))
                {
                    iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "time", 0.2f, "easetype", iTween.EaseType.linear));
                    iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));
                    ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen");
                }
            }
        }

    }

    public void RadiusAddXp() {
        if ((TopicToCheck <= (TaskList.Count)) && (InformationPanel.transform.localScale.y == 0))
        {
            if (TopicToCheck == 2)
            {
                AddXpEffect();
            }
            else
            {
                WrongOptionSelected();
            }
        }
    }

    public void DistanceAddXp()
    {
        if ((TopicToCheck <= (TaskList.Count)) && (InformationPanel.transform.localScale.y == 0))
        {
            if (TopicToCheck == 3)
            {
                AddXpEffect();
            }
            else
            {
                WrongOptionSelected();
            }
        }
    }

    public void DisableOrbit()
    {
        if (!IsAR)
        {

            CamOrbit.DisableInteration = true;


        }
        else
        {
            ArOrbit.DisableInteration = true;
        }

    }

    public void AddListener(EventTriggerType eventType, Action<float> MethodToCall, GameObject TriggerObjToAdd)
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall(TriggerObjToAdd.GetComponent<Slider>().value));
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }

    public Vector3 MidPoint(GameObject obj1, GameObject obj2)
    {
        Vector3 midp = (obj1.transform.localPosition + obj2.transform.localPosition) / 2;
        return midp;
    }

    public float Scale(GameObject obj1, GameObject obj2)
    {
        float scl = Vector3.Distance(obj1.transform.localPosition, obj2.transform.localPosition);
        return scl;
    }

    public void ChangeRadius(float val, Text txt)
    {
        
            //txt.text = val.ToString("F2")+" m";

            //Ball.transform.localScale = new Vector3(2*val, 2*val, 2*val);

            //VLine.transform.localScale = new Vector3(val*2, VLine.transform.localScale.y, VLine.transform.localScale.z);

            //tempPos = new Vector3(0, 0.5f+val*1f, 0);

            //Labels[1].transform.GetChild(0).GetComponentInChildren<TextMeshPro>().text = "a = " + txt.text;

            //Labels[1].transform.localPosition = MidPoint(Dummy, Ball);


       
    }

    //public void ChangeDistance(float val, Text txt)
    //{
    //    txt.text = val.ToString("F2") + " m";

    //    MTape.transform.localPosition = new Vector3(val, MTape.transform.localPosition.y, MTape.transform.localPosition.z);

    //    HLine.transform.localPosition = new Vector3(val, HLine.transform.localPosition.y, HLine.transform.localPosition.z);

    //    HLine.transform.localScale = new Vector3(Scale(HLine, Ball), HLine.transform.localScale.y, HLine.transform.localScale.z);

    //    //HVLine.transform.localPosition = new Vector3(val, HVLine.transform.localPosition.y, HVLine.transform.localPosition.z);

    //    Labels[0].transform.GetChild(0).GetComponentInChildren<TextMeshPro>().text = "A = " + txt.text;

    //    Labels[0].transform.localPosition = MidPoint(Ball, MTape);


    //    //a_label_txt.text = "r = " + txt.text;
    //}
    //public void IncDecFunction(string nam)
    //{
    //    ScannerObject.SetActive(false);
    //    if (nam == "IncButton")
    //    {
    //        if (density < 5f)
    //        {
    //            density = density + 1;

    //            ScannerObject.SetActive(true);

    //            if (TopicToCheck <= (TaskList.Count))
    //            {
    //                if (TopicToCheck == 1)
    //                {
    //                    AddXpEffect();
    //                }
    //                else
    //                {
    //                    WrongOptionSelected();
    //                }
    //            }

    //        }
    //    }
    //    else if (nam == "DecButton")
    //    {
    //        if (density > 1f)
    //        {
    //            density = density - 1;
    //            ScannerObject.SetActive(true);
    //        }

    //        if (TopicToCheck <= (TaskList.Count))
    //        {               
    //            WrongOptionSelected();
    //        }
    //    }

    //  //  mu_label_txt.text = density.ToString();
    //    m_valtxt.text = density.ToString();
    //    CancelInvoke("StopScanner");
    //    Invoke("StopScanner", 2f);

    //}
    //public void StopScanner() {
    //    ScannerObject.SetActive(false);
    //}

    //public void Update()
    //{
    //    float cnst = 0;

    //   // Dummy.transform.localPosition = tempPos;

    //   cnst = G * density * (4 * G * density * 1 / 4 * Mathf.Sqrt(MainSlider.GetComponent<Slider>().value)) - 1;

    //    E_valueTxt.text = "E = " + cnst.ToString("F2");
    //    Tape_E_value.text = "" + cnst.ToString("F2");
    //}
    //public void Switch_on()
    //{
    //    if (OptionUnlockedTill < 1)
    //    {
    //        if (TopicToCheck == 1)
    //        {// print(switch_1.transform.localEulerAngles.z);
    //            if (switch_1.transform.localEulerAngles.z == 0)
    //            {
    //                switch_1.transform.localEulerAngles = new Vector3(0, 0, -25);
    //                switch_2.transform.localEulerAngles = new Vector3(0, 0, -25);
    //                StartCamera(0);
    //                magnet_obj.transform.localScale = new Vector3(0, 0, 0);
    //                magnetic_bool = false;
    //                electric_bool = false;
    //                magnet_obj.transform.localPosition = magnetStartPos.transform.localPosition;
    //                magnet_obj.transform.localEulerAngles = magnetStartPos.transform.localEulerAngles;
    //                magnet_obj.GetComponent<BoxCollider>().enabled = false;
    //                //particleSystemForceField.directionY = 0.075f;
    //                both_bool = false;
    //                ray_bool = false;
    //                electricarrows.SetActive(false);
    //                magneticarrows.SetActive(false);
    //                particle_group.SetActive(true);
    //                arrow.SetActive(false);


    //                ControlPanel.GetComponent<Animator>().Play("CpanelOpen");
    //            }
    //            else
    //            {
    //                switch_1.transform.localEulerAngles = new Vector3(0, 0, 0);
    //                switch_2.transform.localEulerAngles = new Vector3(0, 0, 0);
    //                particle_group.SetActive(false);
    //                electric_bool = false;
    //                magnetic_bool = false;
    //                both_bool = false;

    //            }
    //            AddXpEffect();
    //        }
    //        else
    //        {
    //            WrongOptionSelected();
    //        }
    //    }
    //    else
    //    {
    //        if (switch_1.transform.localEulerAngles.z == 0)
    //        {
    //            switch_1.transform.localEulerAngles = new Vector3(0, 0, -25);
    //            switch_2.transform.localEulerAngles = new Vector3(0, 0, -25);
    //            StartCamera(0);
    //            magnet_obj.transform.localScale = new Vector3(0, 0, 0);
    //            magnetic_bool = false;
    //            electric_bool = false;
    //            magnet_obj.transform.localPosition = magnetStartPos.transform.localPosition;
    //            magnet_obj.transform.localEulerAngles = magnetStartPos.transform.localEulerAngles;
    //            magnet_obj.GetComponent<BoxCollider>().enabled = false;
    //            //particleSystemForceField.directionY = 0.075f;
    //            both_bool = false;
    //            ray_bool = false;
    //            electricarrows.SetActive(false);
    //            magneticarrows.SetActive(false);
    //            particle_group.SetActive(true);
    //            arrow.SetActive(false);


    //            ControlPanel.GetComponent<Animator>().Play("CpanelOpen");
    //        }
    //        else
    //        {
    //            switch_1.transform.localEulerAngles = new Vector3(0, 0, 0);
    //            switch_2.transform.localEulerAngles = new Vector3(0, 0, 0);
    //            particle_group.SetActive(false);
    //            electric_bool = false;
    //            magnetic_bool = false;
    //            both_bool = false;

    //        }
    //    }
      
    //}

    //public void StartCamera(float val)
    //{
    //    iTween.ValueTo(gameObject, iTween.Hash("from", CamOrbit.cameraRotSide, "to", val, "delay", 0.1f, "islocal", true, "time", 2f, "easetype", iTween.EaseType.easeInSine, "onupdate", "TweenedSomeValue"));
    //    //iTween.ValueTo(gameObject, iTween.Hash("from", orbit.distance, "to", 0.5f, "delay", 0.1f, "islocal", true, "time", 2f, "easetype", iTween.EaseType.easeInSine, "onupdate", "TweenedValue"));
    //    // orbit.distance = 0.5f;
    //}
    //public void TweenedSomeValue(float val)
    //{
    //    CamOrbit.cameraRotSide = val;
    //}

    public void AddXpCall()
    {
        AddXpEffect();
    }
    void Update()
    {
        //print("cam........."+CamOrbit.target.position+"...........tarpos......"+TargetPos);
        if (!IsAR)
        {
            if (CamOrbit.target.position != TargetPos)
            {

                /*if (TargetObj != null)
                {
                    TargetPos = TargetObj.transform.position;
                    //TargetObj = null;
                }*/

                //print("Invoke......" + LerpTime);


               // CamOrbit.target.position = Vector3.Lerp(CamOrbit.target.position, TargetPos, LerpTime * Time.deltaTime); // Vector3.Lerp(CamOrbit.transform.position,TargetPos,2.5f*Time.deltaTime);
            }
        }
      





    }
    public void Labels_on()
    {
        label_onbool = !label_onbool;
        if (!label_onbool)
        {
            for (int i = 0; i < Labels.Length; i++)
            {
                Labels[i].SetActive(true);
            }
        }
        else
        {
            for (int i = 0; i < Labels.Length; i++)
            {
                Labels[i].SetActive(false);
            }
        }
        
    }
    public void ResetExperiment()
    {
       CurrentStepCnt = 0;
        coco_obj.GetComponent<Animator>().Play("coco_normal_green");
        moth1.transform.localScale = new Vector3(0.003f, 0.003f, 0.003f);
        info_panel_1_txt.text = "moth feeds on the mulberry leaf and produces eggs.";
        if (!IsAR)
        {
            CamOrbit.maxRotUp = 45;
            CamOrbit.minRotUp = 1;
            CamOrbit.zoomMin = 0.3f;
            CamOrbit.zoomMax = .9f;
            CamOrbit.zoomStart = 0.58f;
            CamOrbit.cameraRotSideStart = 1.5f;
            CamOrbit.cameraRotUpStart = 45f;
            // CamOrbit.Start();
            CamOrbit.target.position = new Vector3(0, 0, 0);
        }
        UIDragAndDrop[] UIButtons = GameObject.FindObjectsOfType<UIDragAndDrop>();
        for (int i = 0; i < UIButtons.Length; i++)
        {
            UIButtons[i].Restart();
        }
        DragDropSteps = UIcanvas.GetComponentsInChildren<UIDragAndDrop>().Length;

        NumOfSteps.text = DragDropSteps.ToString();

        for (int i = 0; i < info_panels.transform.childCount; i++)
        {
            info_panels.transform.GetChild(i).gameObject.SetActive(false);
            
        }
        info_panels.transform.GetChild(0).gameObject.SetActive(true);
        RestartButton.GetComponent<Animator>().Play("RestartClose");
        for (int i = 0; i < arroweffects.transform.childCount; i++)
        {
            arroweffects.transform.GetChild(i).gameObject.transform.GetChild(0).GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(0, 10);
            arroweffects.transform.GetChild(i).gameObject.GetComponent<Animator>().enabled = false;
        }
        // but_reset.SetActive(false);
        //  iTween.MoveTo(RestartButton.gameObject, iTween.Hash("x", -200, "y", 230, "z", 0, "delay", 0.5f, "time", 0.5f, "islocal", true, "easetype", iTween.EaseType.easeInSine));
    }
    
    public void WrongDragDrop()
    {
       
        WronOptionPanel.transform.GetChild(0).GetComponent<Text>().text = "Wrong Option Selected";
        WronOptionPanel.transform.GetChild(0).GetComponent<Text>().color = new Color(0.9921f, 0.4431f, 0.08621f, 1);
        WrongOptionSelected();
       // ShowInstruction_Panel("Wrong Option Selected");
    }
    public void WrongPlacement()
    {
        WronOptionPanel.transform.GetChild(0).GetComponent<Text>().text = "Wrong Placement";
        WronOptionPanel.transform.GetChild(0).GetComponent<Text>().color = new Color(0.9921f, 0.4431f, 0.08621f, 1);
        WrongOptionSelected();
        //ShowInstruction_Panel("Wrong Placement");
    }
    
    /// <summary>
    /// Dummy
    /// </summary>
    public void PlaySequenceStep()
    {
        DragDropSteps--;
        if(CurrentStepCnt>=1&&CurrentStepCnt<=3)
        placeholders.transform.GetChild(CurrentStepCnt-1).gameObject.SetActive(false);
        if (CurrentStepCnt == 0)
        {
            blend_value = 10;
            iTween.ValueTo(arroweffects.transform.GetChild(0).gameObject, iTween.Hash("from", blend_value, "to", 100, "delay", 0.1f, "islocal", true, "time", 2f, "easetype", iTween.EaseType.easeInSine, "onupdate", "TweenedSomeValue", "onupdatetarget", this.gameObject, "oncomplete", "UpdateToNextStep", "oncompletetarget", this.gameObject));
            iTween.ScaleTo(moth1, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 2f, "easetype", iTween.EaseType.easeInSine));
            
        }
        else if (CurrentStepCnt == 1)
        {
            blend_value = 10;
            iTween.ValueTo(arroweffects.transform.GetChild(0).gameObject, iTween.Hash("from", blend_value, "to", 100, "delay", 0.1f, "islocal", true, "time", 2f, "easetype", iTween.EaseType.easeInSine, "onupdate", "TweenedSomeValue", "onupdatetarget", this.gameObject, "oncomplete", "UpdateToNextStep", "oncompletetarget", this.gameObject));

        }
        else if (CurrentStepCnt == 2)
        {
            blend_value = 10;
            iTween.ValueTo(arroweffects.transform.GetChild(0).gameObject, iTween.Hash("from", blend_value, "to", 100, "delay", 0.1f, "islocal", true, "time", 2f, "easetype", iTween.EaseType.easeInSine, "onupdate", "TweenedSomeValue", "onupdatetarget", this.gameObject, "oncomplete", "UpdateToNextStep", "oncompletetarget", this.gameObject));
           
            coco_obj.GetComponent<Animator>().Play("coco_white");
        }
        else if (CurrentStepCnt == 3)
        {
            blend_value = 10;
            iTween.ValueTo(arroweffects.transform.GetChild(0).gameObject, iTween.Hash("from", blend_value, "to", 100, "delay", 0.1f, "islocal", true, "time", 2f, "easetype", iTween.EaseType.easeInSine, "onupdate", "TweenedSomeValue", "onupdatetarget", this.gameObject, "oncomplete", "UpdateToNextStep", "oncompletetarget", this.gameObject));

        }
        else if (CurrentStepCnt == 4)
        {
            
           
        }


      //  UpdateToNextStep();
        


    }

   public void UpdateToNextStep()
    {
        //print("gggg");
       
        CurrentStepCnt++;
        if (CurrentStepCnt >= 1 && CurrentStepCnt <= 3)
            placeholders.transform.GetChild(CurrentStepCnt - 1).gameObject.SetActive(true);
        arroweffects.transform.GetChild(CurrentStepCnt-1).gameObject.GetComponent<Animator>().enabled = true;
        PlayerPrefs.SetInt("CurrentStepCount", PlayerPrefs.GetInt("CurrentStepCount") + 1);
        NumOfSteps.text = DragDropSteps.ToString();
        if (CurrentStepCnt == 1)
        {
            info_panel_1_txt.text = "eggs are ready to hatch.";
            if (TopicToCheck == 1 && OptionUnlockedTill < 1)
            {
                AddXpCall();
            }
        }
        else if (CurrentStepCnt == 2)
        {
            if (TopicToCheck == 2 && OptionUnlockedTill < 1)
            {
                AddXpCall();
            }
        }
        else if (CurrentStepCnt == 3)
        {
            if (TopicToCheck == 3 && OptionUnlockedTill < 1)
            {
                AddXpCall();
            }
        }
        else if (CurrentStepCnt == 4)
        {
            if (TopicToCheck == 4 && OptionUnlockedTill < 1)
            {
                AddXpCall();
            }
            // iTween.MoveTo(RestartButton.gameObject, iTween.Hash("x", 25, "y", 230, "z", 0, "delay", 0.5f, "time", 0.5f, "islocal", true, "easetype", iTween.EaseType.easeInSine));
            RestartButton.GetComponent<Animator>().Play("RestartOpen");
        }


        info_panels.transform.GetChild(CurrentStepCnt-1).gameObject.SetActive(true);

    }
    public void TweenedSomeValue(float value)
    {
        // print(value);
        if (CurrentStepCnt == 0)
        {
            arroweffects.transform.GetChild(0).transform.GetChild(0).GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(0, value);
            
        }
       else if (CurrentStepCnt == 1)
        {
            arroweffects.transform.GetChild(1).transform.GetChild(0).GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(0, value);
        }
        else if (CurrentStepCnt == 2)
        {
            arroweffects.transform.GetChild(2).transform.GetChild(0).GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(0, value);
        }
        else if (CurrentStepCnt == 3)
        {
            arroweffects.transform.GetChild(3).transform.GetChild(0).GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(0, value);
        }
    }
}
//public class MaindragAndDrop : MonoBehaviour
//{
//    public int id;
//    private bool dragging = false;
//    private float distance, intialGroundTouchvalue;
//    private Vector3 init_pos, initialRotation;
//    RaycastHit hit;
//    private Vector3 temp;
//    private Vector3 screenSpace;
//    private Vector3 offSet;
//    private Camera cam;
//    private GameObject Target_collder, Target_collder2, TagetPos, TagetPos2;
//    private bool CorrectPlace;
//    public orbit CamOrbit;
//    public bool IsAR;
//    public Vector3 relativePos, TargetPos, TargetPos_Init, Magnet_oldPos, Magnet_oldRot;
//    private float Initial_Dist;
//    public chargetomassratioelectron chargetomassratioelectron;
//    private int placePos;
//    public AROrbitControls ArOrbit;
//    public void Start()
//    {
//        placePos = 0;
//        CorrectPlace = false;
//        cam = Camera.main.GetComponent<Camera>();
//        IsAR = Camera.main.GetComponentInParent<orbit>() ? false : true;
//        chargetomassratioelectron = GameObject.Find("MainObject").GetComponent<chargetomassratioelectron>();

//        if (!IsAR)
//        {
//            CamOrbit = Camera.main.GetComponentInParent<orbit>();


//            TargetPos_Init = CamOrbit.target.transform.position;
//            //TargetPos = MidObj.transform.position;

//            Initial_Dist = orbit.distance;
//        }
//        else
//        {
//            //CamOrbit = chargetomassratioelectron.Instance.MainObj.transform.parent.gameObject.AddComponent<AROrbitControls>();
//            //  CamOrbit = chargetomassratioelectron.Instance.MainObj.transform.parent.gameObject.AddComponent<AROrbitControls>();
//            ArOrbit = chargetomassratioelectron.Instance.MainObj.transform.parent.gameObject.GetComponent<AROrbitControls>();
//            //ArOrbit = MainObj.transform.parent.gameObject.GetComponent<AROrbitControls>();
//        }

//        Target_collder = GameObject.Find(gameObject.name + "_Collider");
//        Target_collder2 = GameObject.Find(gameObject.name + "_Collider2");
//        TagetPos = GameObject.Find("MagnetPos");
//        TagetPos2 = GameObject.Find("MagnetStartPos");
//       // Target_collder.AddComponent<ColorLerp>();
//       // Target_collder2.AddComponent<ColorLerp>();
//        //dummyObject = GameObject.Find(gameObject.name + "_Dummy");

//        //initialRotation = transform.eulerAngles;

//        if (Target_collder)
//        {
//            Target_collder.SetActive(false);
//            Target_collder2.SetActive(false);
//        }

//        // dummyObject.SetActive(false);

//        EventTrigger.Entry PointerDownentry = new EventTrigger.Entry();
//        PointerDownentry.eventID = EventTriggerType.PointerDown;
//        PointerDownentry.callback.AddListener((data) => { MouseDown(); });

//        gameObject.GetComponent<EventTrigger>().triggers.Add(PointerDownentry);

//        EventTrigger.Entry PointerUpentry = new EventTrigger.Entry();
//        PointerUpentry.eventID = EventTriggerType.PointerUp;
//        PointerUpentry.callback.AddListener((data) => { MouseUp(); });

//        gameObject.GetComponent<EventTrigger>().triggers.Add(PointerUpentry);



//    }

//    void MouseDown()
//    {



//        if (!CorrectPlace)
//        {
//            //orbit.orbit_enabled = false;

//            init_pos = transform.position;

//            initialRotation = transform.eulerAngles;

//            if (placePos == 0)
//            {
//                Target_collder.SetActive(true);
//                Target_collder2.SetActive(false);
//            }
//            else
//            {
//                Target_collder.SetActive(false);
//                Target_collder2.SetActive(true);
//            }

//            this.GetComponent<Collider>().enabled = false;
//            //dummyObject.SetActive(true);
//            chargetomassratioelectron.arrow.transform.localPosition = chargetomassratioelectron.arrow_pos2.transform.localPosition;
//            DisableOrbit();

//            screenSpace = cam.WorldToScreenPoint(transform.position);
//            offSet = transform.position - cam.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenSpace.z));

//            Vector3 curScreenSpace = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenSpace.z);
//            Vector3 curPosition = cam.ScreenToWorldPoint(curScreenSpace) + offSet;

//            intialGroundTouchvalue = curPosition.y;
//            dragging = true;



//        }
//    }

//    public void DisableOrbit()
//    {
//        if (!IsAR)
//        {

//            CamOrbit.DisableInteration = true;


//        }
//        else
//        {
//            ArOrbit.DisableInteration = true;
//        }

//    }
//    void MouseUp()
//    {
//        //if (controllerLevel1.SeqCount == id) {
//        if (!CorrectPlace)
//        {

//            Ray ray = cam.ScreenPointToRay(Input.mousePosition);

//            if (Physics.Raycast(ray, out hit, 100))
//            {

//                if (hit.collider.name == Target_collder.name)
//                {

//                    // print(hit.collider.name);
//                    gameObject.transform.localPosition = TagetPos.transform.localPosition;
//                    gameObject.transform.localEulerAngles = TagetPos.transform.localEulerAngles;
//                    chargetomassratioelectron.magnetic_bool = true;
//                   // placePos = 1;
//                    chargetomassratioelectron.electricarrows.SetActive(false);
//                    chargetomassratioelectron.magneticarrows.SetActive(true);
//                    chargetomassratioelectron.arrow.SetActive(false);
//                    if (chargetomassratioelectron.OptionUnlockedTill < 1)
//                    {
//                        if (chargetomassratioelectron.TopicToCheck == 4)
//                        {
//                            chargetomassratioelectron.AddXpEffect();
//                        }
//                    }
//                }
//                else if (hit.collider.name == Target_collder2.name)
//                {
//                    gameObject.transform.localPosition = TagetPos2.transform.localPosition;
//                    gameObject.transform.localEulerAngles = TagetPos2.transform.localEulerAngles;
//                    chargetomassratioelectron.magnetic_bool = false;
//                    chargetomassratioelectron.electricarrows.SetActive(false);
//                    chargetomassratioelectron.magneticarrows.SetActive(false);
//                    placePos = 0;
//                }
//                else
//                {
//                    // InvokeRepeating("PlacemnetAdjustment", 0.1f, 0.01f);
//                    transform.position = init_pos;
//                    transform.eulerAngles = initialRotation;
//                    this.GetComponent<Collider>().enabled = true;
//                }

//            }
//            else
//            {
//                // InvokeRepeating("PlacemnetAdjustment", 0.1f, 0.01f);
//                transform.position = init_pos;
//                transform.eulerAngles = initialRotation;
//                this.GetComponent<Collider>().enabled = true;

//            }

//            // InvokeRepeating("PlacemnetAdjustment", 0.1f, 0.01f);
//            this.GetComponent<Collider>().enabled = true;
//            Target_collder.SetActive(false);
//            Target_collder2.SetActive(false);
//            //dummyObject.SetActive(false);
//            // orbit.orbit_enabled = true;
//            EnableOrbit();
//            dragging = false;
//        }
//    }

//    public void EnableOrbit()
//    {
//        if (!IsAR)
//        {
//            CamOrbit.DisableInteration = false;


//        }
//        else
//        {
//            ArOrbit.DisableInteration = false;
//        }

//        //if (InformationPanel.gameObject.transform.localScale.x != 1)
//        //{

//        //    if (TopicToCheck <= (TaskList.Count))
//        //    {
//        //        iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "time", 0.2f, "easetype", iTween.EaseType.linear));
//        //        iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));
//        //        ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen");
//        //    }
//        //}

//    }
//    //bool CheckCondition()
//    //{

//    //    bool b = false;
//    //    int i = 0;

//    //    while (i < chargetomassratioelectron.Instance.DragObjects.Length - 1)
//    //    {
//    //        //if (chargetomassratioelectron.Instance.DragObjects[i].transform.localPosition == chargetomassratioelectron.Instance.DummyObjects[i].transform.localPosition)

//    //        if (chargetomassratioelectron.Instance.DragObjects[i].transform.localPosition == chargetomassratioelectron.Instance.DummyObjects[i].transform.localPosition)
//    //        {
//    //            b = true;
//    //        }
//    //        else { b = false; }
//    //        i++;
//    //    }
//    //    return b;
//    //}


//    void PlacemnetAdjustment()
//    {
//        if (CorrectPlace)
//        {
//            if (transform.position != Target_collder.transform.position)
//            {
//                transform.position = Vector3.Lerp(transform.position, Target_collder.transform.position, 0.1f);
//                transform.eulerAngles = Vector3.Lerp(transform.eulerAngles, Target_collder.transform.eulerAngles, 0.1f);
//            }
//            else
//            {
//                CancelInvoke("PlacemnetAdjustment");
//            }
//        }
//        else
//        {
//            if (transform.position != init_pos)
//            {
//                transform.position = Vector3.Lerp(transform.position, init_pos, 0.1f);
//                transform.eulerAngles = Vector3.Lerp(transform.eulerAngles, initialRotation, 0.1f);
//            }
//            else
//            {
//                CancelInvoke("PlacemnetAdjustment");

//            }
//        }
//    }


//    void Update()
//    {

//        if (dragging)
//        {
//            Vector3 curScreenSpace = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenSpace.z);
//            Vector3 curPosition = cam.ScreenToWorldPoint(curScreenSpace) + offSet;
//            //curPosition = new Vector3 (curPosition.x, Mathf.Clamp (curPosition.y, intialGroundTouchvalue+GroundTouchValue, 100f), curPosition.z);
//            transform.position = curPosition;
//        }
//    }

//    //public void EnableOrbit()
//    //{
//    //    if (!IsAR)
//    //    {
//    //        CamOrbit.orbit_enabled = true;
//    //    }
//    //    else
//    //    {
//    //        ARorbit.orbit_enabled = true;
//    //    }
//    //}

//    //public void DisableOrbit()
//    //{

//    //    if (!IsAR)
//    //    {
//    //        CamOrbit.orbit_enabled = false;
//    //    }
//    //    else
//    //    {
//    //        ARorbit.orbit_enabled = false;
//    //    }
//    //}
//}






//public class Textur_animation : MonoBehaviour
//{
//    // Scroll main texture based on time

//    float scrollSpeed = 0.001f;
//    Renderer rend;

//    void Start()
//    {
//        rend = GetComponent<Renderer>();
//    }

//    void Update()
//    {
//        float offset = Time.time * scrollSpeed;
//        rend.material.SetTextureOffset("_MainTex", new Vector2(offset, 0));
//    }
//}


