﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;
using System.Linq;
using System;
using System.Text.RegularExpressions;
using UnityEngine.SceneManagement;

public class vid6068_writingachemicalformula : MonoBehaviour
{
    public AssetBundle assetBundle;
    public Material TransparentMaterial;
    public TextAsset CSVFile;
    public AROrbitControls ArOrbit;
    public orbit CamOrbit;
    public GameObject MainObject,FormulaPanel;
    public List<Button> AddAtom, RemoveAtom;
    public int Curr_Equation,NumObjToMove,CurrObjToMove,CurrElecToMove,ObjectivesCnt,Elec_Cnt;
    public List<GameObject> AtomToMove, AtomToMove_Target,ElecToMove, ElecToMove_Target,UIPanels,HeadingLabels;
    public List<Vector3> ElecInit_Pos, AtomInit_Pos;
    public List<LineRenderer> LineRendObj;
    public Dropdown Formulas_DD;
    public bool IsSubmitted;
    public List<String> Objectives_Individual;
    public Button ShowLabels_Btn;


    public bool IsAR, IsUIZooming;
    public int OptionUnlockedTill, CurrentTopicSelected, TopicToCheck;
    public float Sub_XP_Earned, Total_XP;
    public Dropdown ToDoList_DD;
    public Slider ValueDisplaySlider;
    public GameObject InstructionPanel_DD, ObjectivePanel, InstructionPanel, InfoPanel, GlassPanel, Canvas_BG, SidePannel;
    public Button Info_Close_Btn, Info_Cntue_Btn, Info_Open_Btn, SidePannelOpen, Reset_Btn;
    public List<Dictionary<string, object>> CSV_data;
    public List<String> Topics, Sub_Topics, Sub_Top_ToDoList, Sub_Top_Desc, Sub_Top_XP, LearningGoals;
    public Text XP_ToShow, XP_Effect, Main_XP_ToShow, Learninggoal_Txt, Instruction_Txt, Assumption_Txt, Heading_Txt, ElecCnt_Txt;
    public bool IsTopicUnlocked;
    public Canvas UIcanvas;

    // Start is called before the first frame update
    void Start()
    {
        //OptionUnlockedTill = 1;

        
        UIcanvas = GameObject.Find("Canvas_ChemEqu").GetComponent<Canvas>();

        LoadFromAsssetBundle();



        ReadingDataFromCSVFile();
        ReadingXps();

        FindGameObjects();

        AssignEventsToObjects();

        

    }

    public void Update()
    {
        if (LineRendObj[0].GetComponent<LineRenderer>().widthMultiplier != transform.parent.localScale.x)
        {
            for (int i = 0; i < LineRendObj.Count; i++)
            {
                LineRendObj[i].widthMultiplier = transform.parent.localScale.x;
            }
        }
    }

    public void LoadFromAsssetBundle()
    {
        //if (Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.Android)
        //{
        assetBundle = AssetBundle.LoadFromFile(PlayerPrefs.GetString("AssetBundleUrl"));

        CSVFile = assetBundle.LoadAsset(PlayerPrefs.GetString("LibraryName") + "CSV", typeof(TextAsset)) as TextAsset;

        TransparentMaterial = assetBundle.LoadAsset("TransparentMaterial", typeof(Material)) as Material;
        TransparentMaterial.shader = Shader.Find("Standard");



        //}
        //else
        //{

        //string CSVName = transform.parent.gameObject.name;



        //if (CSVName.Contains("Clone"))
        //{



        //    CSVName = CSVName.Substring(0, CSVName.Length - 7);
        //}



        //print("casvname........." + CSVName);



        //CSVFile = Resources.Load(CSVName + "CSV") as TextAsset;
        //}







        //Camera.main.gameObject.AddComponent<PhysicsRaycaster>();



        Camera.main.backgroundColor = Color.black;



        GameObject TargetForCamera = GameObject.Find("Target");
        Camera.main.gameObject.AddComponent<PhysicsRaycaster>();


        if (TargetForCamera != null)
        {


               TargetForCamera.AddComponent<orbit>();
                CamOrbit = TargetForCamera.GetComponent<orbit>();
                CamOrbit.zoomMax = 0.25f;
                CamOrbit.zoomMin = 0f;
                CamOrbit.zoomStart = 0.1f;
                CamOrbit.distance = CamOrbit.zoomStart;
                CamOrbit.transform.position = new Vector3(0, 0.2f, 0);

            CamOrbit.minSideRot = 20;
            CamOrbit.maxSideRot = 20;

            CamOrbit.maxRotUp = 20;
            CamOrbit.minRotUp = 0;

            

            IsAR = false;
        }
        else
        {
            this.transform.parent.gameObject.AddComponent<AROrbitControls>();
            ArOrbit = this.transform.parent.gameObject.GetComponent<AROrbitControls>();
            IsAR = true;
        }

        

    }

    

    public void ReadingDataFromCSVFile()
    {
        Topics = new List<string>();
        Sub_Topics = new List<string>();
        Sub_Top_Desc = new List<string>();
        Sub_Top_ToDoList = new List<string>();
        Sub_Top_XP = new List<string>();
        LearningGoals = new List<string>();

        CSV_data = CSVReader.Read(CSVFile);


        for (var i = 0; i < CSV_data.Count; i++)
        {
            //print("Topics " + CSV_data[i]["Learninggoals"] + ".......... " +"ToDo " + CSV_data[i]["Topic_TodoList"]);



            if (CSV_data[i]["Topic_ToDoList"].ToString() != "" && CSV_data[i]["Topic_ToDoList"].ToString() != null)
            {
                Sub_Top_ToDoList.Add(CSV_data[i]["Topic_ToDoList"].ToString());

            }



            //if (CSV_data[i]["Top_XP"].ToString() != "" && CSV_data[i]["Top_XP"].ToString() != null)
            //{
            //    Sub_Top_XP.Add(CSV_data[i]["Top_XP"].ToString());

            //}

            if (CSV_data[i]["Learninggoals"].ToString() != "" && CSV_data[i]["Learninggoals"].ToString() != null)
            {
                LearningGoals.Add(CSV_data[i]["Learninggoals"].ToString());

            }

            if (CSV_data[i]["ObjectivesIndi"].ToString() != "" && CSV_data[i]["ObjectivesIndi"].ToString() != null)
            {
                Objectives_Individual.Add(CSV_data[i]["ObjectivesIndi"].ToString());

            }
            

        }






    }


    public int totXp, loclXp, PerTopic_XP;

    public void ReadingXps()
    {

        //PlayerPrefs.SetInt("TotalXP", 34);
        //PlayerPrefs.SetInt("MaxEarnings", 22);

        totXp = PlayerPrefs.GetInt("TotalXP");
        loclXp = PlayerPrefs.GetInt("MaxEarnings");

        TopicToCheck = 0;

        if (loclXp != 0)
        {
            OptionUnlockedTill = 0;

        }
        else
        {
            OptionUnlockedTill = 1;

        }


        


        int t = 0;
        int p = 0;

        for (int k = 0; k < Sub_Top_ToDoList.Count; k++)
        {

            p = loclXp / Sub_Top_ToDoList.Count;
            Sub_Top_XP.Add(p.ToString());
            t = t + p;
        }

        if (t != loclXp)
        {
            Sub_Top_XP[Sub_Top_ToDoList.Count - 1] = (p + (loclXp - t)).ToString();
        }

        Total_XP = loclXp;
        

    }



    public float result;

    public void GetResult()
    {
        result = ((float)(TopicToCheck - 0) / (float)(Sub_Top_ToDoList.Count)) * 100;
        Camera.main.SendMessage("UnloadActually", result);
    }

    public void FindGameObjects()
    {
        IsSubmitted = false;
        MainObject = GameObject.Find("MainObject");
        //AddAtom = GameObject.Find("IncrementAtom").GetComponent<Button>();
        //RemoveAtom = GameObject.Find("DecrementAtom").GetComponent<Button>();
        UIPanels= GameObject.FindObjectsOfType(typeof(GameObject)).Select(g => g as GameObject).Where(g => g.name.Contains("Formula_")).ToList();
        UIPanels = UIPanels.OrderBy(x => x.name).ToList();
        LineRendObj = GameObject.FindObjectsOfType(typeof(LineRenderer)).Select(g => g as LineRenderer).ToList();
        for (int i = 0; i < LineRendObj.Count; i++)
        {
            for (int ij = 0; ij < LineRendObj[i].transform.childCount; ij++)
            {
                if(LineRendObj[i].gameObject.transform.GetChild(ij).childCount>0)
                LineRendObj[i].gameObject.transform.GetChild(ij).GetChild(0).gameObject.AddComponent<SmoothLook>();
            }
            
        }

        FormulaPanel = GameObject.Find("FormulaPanel");
        FormulaPanel.transform.localScale = Vector3.zero;

        HeadingLabels = GameObject.FindObjectsOfType(typeof(GameObject)).Select(g => g as GameObject).Where(g => g.name.Contains("_Heading")).ToList();
        for (int i = 0; i < HeadingLabels.Count; i++)
        {
            HeadingLabels[i].SetActive(false);
        }

        Curr_Equation = 0;
        ElecCnt_Txt = GameObject.Find("Electrons_Count").GetComponent<Text>();

        Formulas_DD = GameObject.Find("FormulaDropdown").GetComponent<Dropdown>();
        Canvas_BG = GameObject.Find("Canvas_BG");

        if (!IsAR)
        {
            Canvas_BG.transform.parent = Camera.main.transform;
            Canvas_BG.transform.GetComponentInChildren<RawImage>().enabled = true;
            Canvas_BG.GetComponent<Canvas>().worldCamera = Camera.main;
        }
        else
        {
            Canvas_BG.SetActive(false);
        }
        //GameObject content = GameObject.Find("Content");
        ShowLabels_Btn = GameObject.Find("LabelsButton").GetComponent<Button>();

        SidePannel = GameObject.Find("SidePanel");
        SidePannelOpen = GameObject.Find("PannelOpenButton").GetComponent<Button>();
        //ValueDisplaySlider = GameObject.Find("ValueDisplaySlider").GetComponent<Slider>();
        InstructionPanel_DD = GameObject.Find("DD_Instruction");
        InstructionPanel = GameObject.Find("InstructionPanel");
        ToDoList_DD = GameObject.Find("ToDoList_Dropdown").GetComponent<Dropdown>();
        XP_ToShow = GameObject.Find("Gained_XP_Value").GetComponent<Text>();
        XP_Effect = GameObject.Find("Effect_Text").GetComponent<Text>();
        Main_XP_ToShow = GameObject.Find("Main_XP_Value").GetComponent<Text>();
        ObjectivePanel = GameObject.Find("ObjectivePanel");
        InstructionPanel.transform.localScale = Vector3.zero;
        InfoPanel = GameObject.Find("InformationPanel");
        Info_Close_Btn = GameObject.Find("InfoCloseBtn").GetComponent<Button>();
        Info_Cntue_Btn = GameObject.Find("InfoContinueBtn").GetComponent<Button>();
        Info_Open_Btn = GameObject.Find("InfoButton").GetComponent<Button>();
        
        Info_Close_Btn.gameObject.SetActive(false);
        GlassPanel = GameObject.Find("GlassEffect");
        Instruction_Txt = GameObject.Find("IListText").GetComponent<Text>();
        Assumption_Txt = GameObject.Find("AListText").GetComponent<Text>();
        Learninggoal_Txt = GameObject.Find("LGListText").GetComponent<Text>();
        Heading_Txt = GameObject.Find("InfoHeadingText").GetComponent<Text>();
        //Reset_Btn = GameObject.Find("Reset_Exp").GetComponent<Button>();




        if (OptionUnlockedTill == 0)
        {
            /*ToDoList_DD.GetComponent<RectTransform>().anchoredPosition = new Vector3(150f, -150f, 0);
            XP_ToShow.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(150f, -150f, 0);
            SidePannel.GetComponent<RectTransform>().anchoredPosition = new Vector3(-600f, 0f, 0);*/
            ObjectivePanel.transform.localScale = new Vector3(1, 1, 1);
            IsTopicUnlocked = false;
        }
        else
        {
            ToDoList_DD.GetComponent<RectTransform>().anchoredPosition = new Vector3(-250f, -150f, 0);
            XP_ToShow.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(-250f, -150f, 0);
            ObjectivePanel.transform.localScale = new Vector3(1, 0, 1);
            IsTopicUnlocked = true;
        }

        Main_XP_ToShow.text = totXp.ToString();

        XP_ToShow.text = "0/" + Total_XP;


        for (int i = 0; i < Sub_Top_ToDoList.Count; i++)
        {

            ToDoList_DD.options.Add(new Dropdown.OptionData() { text = Sub_Top_ToDoList[i] });


            //List<float> TotalXP = Sub_Top_XP.Select(s => float.Parse(s)).ToList();

            //Total_XP += TotalXP[i];

            //XP_ToShow.text = "0/" + Total_XP;


            ObjectivePanel.transform.Find("ObjectiveText").GetComponent<Text>().text = Sub_Top_ToDoList[TopicToCheck];




        }

        OpenSidePannel();
        GlassPanel.transform.localScale = Vector3.zero;
        InfoPanel.transform.localScale = Vector3.zero;
        Invoke("OpenInfoPanel", 0.1f);

        Heading_Txt.text = LearningGoals[0];
        Learninggoal_Txt.text = LearningGoals[1];
        //Instruction_Txt.text = LearningGoals[2];
        Assumption_Txt.text = LearningGoals[3];

        SelectFormula(0);
    }


    public void AssignEventsToObjects()
    {





        SidePannelOpen.onClick.AddListener(OpenSidePannel);
        //TopSidePannelOpen.onClick.AddListener(OpenTopSidePannel);
        


        EventTrigger.Entry PointerDown = new EventTrigger.Entry();
        PointerDown.eventID = EventTriggerType.EndDrag;
        PointerDown.callback.AddListener((data) => { DisableOrbit(); });

        //ScrollBarViewPort.gameObject.GetComponent<EventTrigger>().triggers.Add(PointerDown);

        EventTrigger.Entry PointerUp = new EventTrigger.Entry();
        PointerUp.eventID = EventTriggerType.BeginDrag;
        PointerUp.callback.AddListener((data) => { EnableOrbit(); });

        //ScrollBarViewPort.gameObject.GetComponent<EventTrigger>().triggers.Add(PointerUp);

        /*for (int i = 0; i < SelectionButtons.Length; i++)
        {
            int j = i;
            SelectionButtons[i].onClick.AddListener(() => PropertyToDisplay(j));
            // print(SelectionButtons[i].name);
            old_color = SelectionButtons[0].transform.GetChild(1).GetComponent<Text>().color;
        }*/
        ToDoList_DD.GetComponent<EventTrigger>().triggers.Clear();

        AddListenerToEvents(EventTriggerType.PointerClick, ExploringToDOList_DD, ToDoList_DD.gameObject);
        AddListenerToEvents(EventTriggerType.Select, ToDoList_DDCancel, ToDoList_DD.gameObject);
        Info_Close_Btn.onClick.AddListener(() => CloseInfoPanel(Info_Close_Btn));
        Info_Cntue_Btn.onClick.AddListener(() => CloseInfoPanel(Info_Cntue_Btn));
        Info_Open_Btn.onClick.AddListener(OpenInfoPanel);
        ShowLabels_Btn.onClick.AddListener(ShowLabels);

        for (int i = 0; i < AddAtom.Count; i++)
        {
            int j = i;
            AddListenerToEvents(EventTriggerType.PointerClick, AddingAtom, AddAtom[j].gameObject,j);
            AddListenerToEvents(EventTriggerType.PointerClick, RemovingAtom, RemoveAtom[j].gameObject,j);
        }
        for (int i = 0; i < UIPanels.Count; i++)
        {
            AddListenerToEvents(EventTriggerType.PointerClick, OnSubmitClick, UIPanels[i].transform.Find("SubmitBtn").gameObject);
            
        }

        Formulas_DD.onValueChanged.AddListener(delegate { SelectFrom_DD(Formulas_DD.value); });
        AddListenerToEvents(EventTriggerType.PointerClick, ExploringFromulaList_DD, Formulas_DD.gameObject);
        AddListenerToEvents(EventTriggerType.Select, DDCancel, Formulas_DD.gameObject, Formulas_DD.gameObject);

        //Submit_Btn.onClick.AddListener(OnSubmitClick);
        //AddAtom.onClick.AddListener(AddingAtom);
        //RemoveAtom.onClick.AddListener(RemovingAtom);
        /*AddListenerToEvents(EventTriggerType.BeginDrag, OnElementBeginDrag, GameObject.Find("RawImage"));
        AddListenerToEvents(EventTriggerType.EndDrag, OnElementEndDrag, GameObject.Find("RawImage"));
        AddListenerToEvents(EventTriggerType.PointerClick, BackToPT, GameObject.Find("RawImage"));*/

    }

    public void ExploringFromulaList_DD()
    {
        
        Formulas_DD.transform.Find("Arrow").transform.eulerAngles = new Vector3(0, 0, 180);

        GameObject DDList = Formulas_DD.transform.Find("Dropdown List").gameObject;
        DDList.transform.localScale = new Vector3(1, 0, 1);
        iTween.ScaleTo(DDList, iTween.Hash("y", 1, "time", 0.5f, "easetype", iTween.EaseType.spring));


        for (int i = 0; i < Formulas_DD.options.Count; i++)
        {
            GameObject Child = GameObject.Find("Item " + i + ": " + Formulas_DD.options[i].text);

            GameObject Reminder = Child.transform.Find("ItemReminder").gameObject;

            /*if (i == OptionUnlockedTill && CurrentTopicSelected != i)
            {
                Reminder.GetComponent<Image>().enabled = true;
                if (!Reminder.GetComponent<iTween>())
                    iTween.ScaleTo(Reminder, iTween.Hash("x", 1, "y", 1, "time", 0.5f, "looptype", iTween.LoopType.pingPong, "easetype", iTween.EaseType.easeInOutSine));
                //Child.transform.Find("Item Label").GetComponent<Text>().color = Color.white;
            }
            else
            {
                Reminder.GetComponent<Image>().enabled = false;
                if (Reminder.GetComponent<iTween>())
                    Destroy(Reminder.GetComponent<iTween>());
            }*/


        }
    }

    public void DDCancel(GameObject DDToCheck)
    {
        DDToCheck.transform.Find("Arrow").transform.eulerAngles = new Vector3(0, 0, 0);
        if (DDToCheck.transform.Find("Dropdown List") != null)//&& Obj.transform.localScale.y!=1)
        {
            //print("cancelled........");
            //print("list.....cancelled........" + ToDoList_DD.transform.Find("Dropdown List").gameObject);
            iTween.ScaleTo(DDToCheck.transform.Find("Dropdown List").gameObject, iTween.Hash("y", 0, "time", 0.1f, "easetype", iTween.EaseType.spring));

        }

        if (!IsTopicUnlocked)
            iTween.ScaleTo(ObjectivePanel, iTween.Hash("y", 1, "time", 0.5f, "delay", 0.1f, "easetype", iTween.EaseType.easeInOutSine));
    }

    public void SelectFrom_DD(int CurrFormula)
    {
        if (!IsTopicUnlocked)
        {
            ShowInstructionPanel("Wrong parameter chosen");
            Formulas_DD.value = Curr_Equation;
            return;
        }
        SelectFormula(CurrFormula);
        SidePannel.GetComponent<Animator>().Play("Reset");
        Invoke("OpenSidePannel",0.1f);
    }

    public void SelectFormula(int CurrFormula)
    {
        

        if (Curr_Equation != CurrFormula)
        {
            for (int i = 0; i < AtomToMove.Count; i++)
            {
                AtomToMove[i].transform.localPosition = AtomInit_Pos[i];
            }

            for (int i = 0; i < ElecToMove.Count; i++)
            {
                ElecToMove[i].transform.localPosition = ElecInit_Pos[i];
            }

            
        }
        else
        {
            
        }

        for (int i = 0; i < UIPanels.Count; i++)
        {
            UIPanels[i].SetActive(false);
        }

        for (int i = 0; i < MainObject.transform.childCount; i++)
        {
            MainObject.transform.GetChild(i).gameObject.SetActive(false);
        }
        MainObject.transform.GetChild(CurrFormula).gameObject.SetActive(true);
        UIPanels[CurrFormula].SetActive(true);

        Curr_Equation = CurrFormula;

        if (CurrFormula == 0)
        {
            ObjectivesCnt = 0;
            Elec_Cnt =7;
        }
        else if (CurrFormula == 1)
        {
            ObjectivesCnt = 2;
            Elec_Cnt = 6;
        }
        else if (CurrFormula == 2)
        {
            ObjectivesCnt = 5;
            Elec_Cnt = 4;
        }
        else if (CurrFormula == 3)
        {
            ObjectivesCnt = 10;
            Elec_Cnt = 6;
        }

        ObjectivePanel.transform.Find("ObjectiveText").GetComponent<Text>().text = Objectives_Individual[ObjectivesCnt];
        ObjectivePanel.transform.localScale = new Vector3(1, 0, 1);
        iTween.ScaleTo(ObjectivePanel, iTween.Hash("y", 1, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine));

        LoadAtomPositions();
    }

    public void LoadAtomPositions()
    {

        AtomToMove = GameObject.FindObjectsOfType(typeof(GameObject)).Select(g => g as GameObject).Where(g => g.name.Contains("_Move_")).ToList();
        AtomToMove_Target = GameObject.FindObjectsOfType(typeof(GameObject)).Select(g => g as GameObject).Where(g => g.name.Contains("_MoveTo")).ToList();
        AddAtom = GameObject.FindObjectsOfType(typeof(Button)).Select(g => g as Button).Where(g => g.name.Contains("IncrementAtom")).ToList();
        RemoveAtom = GameObject.FindObjectsOfType(typeof(Button)).Select(g => g as Button).Where(g => g.name.Contains("DecrementAtom")).ToList();
        

        AtomToMove = AtomToMove.OrderBy(x => x.name).ToList();
        AtomToMove_Target = AtomToMove_Target.OrderBy(x => x.name).ToList();
        AddAtom = AddAtom.OrderBy(x => x.name).ToList();
        RemoveAtom = RemoveAtom.OrderBy(x => x.name).ToList();


        ElecToMove = GameObject.FindObjectsOfType(typeof(GameObject)).Select(g => g as GameObject).Where(g => g.name.Contains("AtomInCircle_Move")).ToList();
        ElecToMove_Target = GameObject.FindObjectsOfType(typeof(GameObject)).Select(g => g as GameObject).Where(g => g.name.Contains("AtomInCircle_Tar")).ToList();

        ElecToMove = ElecToMove.OrderBy(x => x.name).ToList();
        ElecToMove_Target = ElecToMove_Target.OrderBy(x => x.name).ToList();
        CurrElecToMove = 0;
        CurrObjToMove = 0;
        AtomInit_Pos.Clear();
        ElecInit_Pos.Clear();

        for (int i = 0; i < AtomToMove.Count; i++)
        {
            AtomInit_Pos.Add(AtomToMove[i].transform.localPosition);
        }

        for (int i = 0; i < ElecToMove.Count; i++)
        {
            ElecInit_Pos.Add(ElecToMove[i].transform.localPosition);
        }

        for (int i = 0; i < AddAtom.Count; i++)
        {
            int j = i;
            AddListenerToEvents(EventTriggerType.PointerClick, AddingAtom, AddAtom[j].gameObject, j);
            AddListenerToEvents(EventTriggerType.PointerClick, RemovingAtom, RemoveAtom[j].gameObject, j);
        }

        ElecCnt_Txt = GameObject.Find("Electrons_Count").GetComponent<Text>();
        ElecCnt_Txt.text =Elec_Cnt.ToString();

    }

    public void AddingAtom(int CurrAtom)
    {
        //if (CurrObjToMove < AtomToMove.Count)
        //{


        if (CurrObjToMove == CurrAtom)
        {
            CurrObjToMove++;

            CurrElecToMove = (CurrAtom * 2) + 2;
            if (!IsTopicUnlocked)
            {
                if (TopicToCheck == 0 || TopicToCheck == 1)
                {
                    if (CheckFor_XP_Condition(TopicToCheck))
                        return;
                }
            }
            else
            {
                ObjectivesCnt++;

            }
            

            ObjectivePanel.transform.Find("ObjectiveText").GetComponent<Text>().text = Objectives_Individual[ObjectivesCnt];
            ObjectivePanel.transform.localScale = new Vector3(1, 0, 1);
            iTween.ScaleTo(ObjectivePanel, iTween.Hash("y", 1, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine));

            UIcanvas.GetComponent<GraphicRaycaster>().enabled = false;
            iTween.MoveTo(AtomToMove[CurrAtom], iTween.Hash("x", AtomToMove_Target[CurrAtom].transform.localPosition.x, "y", AtomToMove_Target[CurrAtom].transform.localPosition.y,
                "z", AtomToMove_Target[CurrAtom].transform.localPosition.z, "time", 1f, "oncomplete", "ActionAfterAdding", "oncompletetarget", this.gameObject, "islocal", true, "easetype", iTween.EaseType.easeInOutSine));

            
        }
        else
        {
            ShowInstructionPanel("Wrong Option Selected");
        }
        //}
    }

    public void ActionAfterAdding()
    {
        //ElecInit_Pos.Clear();
        //CurrObjToMove++;
        //CurrElecToMove += 2;
        UIcanvas.GetComponent<GraphicRaycaster>().enabled=true;

        for (int i = CurrElecToMove-2; i < CurrElecToMove; i++)
        {
            ElecInit_Pos.Add(ElecToMove[i].transform.localPosition);
            iTween.MoveTo(ElecToMove[i], iTween.Hash("x", ElecToMove_Target[i].transform.localPosition.x, "y", ElecToMove_Target[i].transform.localPosition.y, "z", ElecToMove_Target[i].transform.localPosition.z , "time", 1f, "islocal", true, "easetype", iTween.EaseType.easeInOutSine));
            
        }

        

        if (Curr_Equation == 3)
        {
            if (CurrObjToMove == 2)
            {
                ElecCnt_Txt = GameObject.Find("Electrons_Count2").GetComponent<Text>();
                Elec_Cnt = 6;
                
            }
            else if (CurrObjToMove == 1)
            {
                ElecCnt_Txt = GameObject.Find("Electrons_Count").GetComponent<Text>();
                Elec_Cnt = 6;
                
            }
        }

        Elec_Cnt++;
        ElecCnt_Txt.text = Elec_Cnt.ToString();
    }


    public void RemovingAtom(int CurrAtom)
    {
        //if (CurrObjToMove == 0)
        //    return;
        //CurrObjToMove--;
        //CurrElecToMove -= 2;

        if (!IsTopicUnlocked)
        {
            if (TopicToCheck == 0 || TopicToCheck == 1)
            {
                ShowInstructionPanel("Wrong parameter chosen");
                return;
            }
        }
        if ((CurrObjToMove-1) == CurrAtom)
        {
            ObjectivesCnt--;
            CurrObjToMove--;
            

            CurrElecToMove = (CurrAtom * 2);

            ObjectivePanel.transform.Find("ObjectiveText").GetComponent<Text>().text = Objectives_Individual[ObjectivesCnt];
            ObjectivePanel.transform.localScale = new Vector3(1, 0, 1);
            iTween.ScaleTo(ObjectivePanel, iTween.Hash("y", 1, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine));

            for (int i = CurrElecToMove; i < CurrElecToMove + 2; i++)
            {
                iTween.MoveTo(ElecToMove[i], iTween.Hash("x", ElecInit_Pos[i].x, "y", ElecInit_Pos[i].y, "z", ElecInit_Pos[i].z, "time", 1f, "islocal", true, "easetype", iTween.EaseType.easeInOutSine));

            }

            iTween.MoveTo(AtomToMove[CurrAtom], iTween.Hash("x", AtomInit_Pos[CurrAtom].x, "y", AtomInit_Pos[CurrAtom].y,
                    "z", AtomInit_Pos[CurrAtom].z, "time", 1f, "islocal", true, "easetype", iTween.EaseType.easeInOutSine));

            if (Curr_Equation == 3)
            {
                if (CurrObjToMove == 1)
                {
                    ElecCnt_Txt = GameObject.Find("Electrons_Count2").GetComponent<Text>();
                    Elec_Cnt = 7;
                    
                }
                else if (CurrObjToMove == 0)
                {
                    ElecCnt_Txt = GameObject.Find("Electrons_Count").GetComponent<Text>();
                    Elec_Cnt = 7;
                    
                }
            }

            Elec_Cnt--;
            ElecCnt_Txt.text = Elec_Cnt.ToString();

        }
    }

    public void OnSubmitClick()
    {
        if (!IsTopicUnlocked)
        {
            if (TopicToCheck == 0)
            {
                ShowInstructionPanel("Wrong parameter chosen");
                return;
            }
            else
            {
                ShowFormulaPanel();
                IsSubmitted = true;
                CheckFor_XP_Condition(TopicToCheck);
            }
        }
        else
        {
            if(CurrObjToMove>=AtomToMove.Count)
            ShowFormulaPanel();

        }

        

    }

    public void ShowFormulaPanel()
    {
        UIcanvas.GetComponent<GraphicRaycaster>().enabled = false;
        for (int i = 0; i < FormulaPanel.transform.childCount; i++)
        {
            
            FormulaPanel.transform.GetChild(i).gameObject.SetActive(false);
        }
        FormulaPanel.transform.GetChild(Curr_Equation).gameObject.SetActive(true); ;

        iTween.ScaleTo(FormulaPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 1f, "easetype", iTween.EaseType.spring));
        FormulaPanel.transform.GetChild(Curr_Equation).gameObject.SetActive(true);
        iTween.ScaleTo(GlassPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 0.01f, "easetype", iTween.EaseType.spring));

        ObjectivePanel.transform.localScale = new Vector3(1, 0, 1);
        Invoke("CloseFormulaPanel",3f);
    }

    public void CloseFormulaPanel()
    {
        
        iTween.ScaleTo(FormulaPanel, iTween.Hash("x", 0, "y", 0, "z", 0, "time", 0.5f, "easetype", iTween.EaseType.linear));

        iTween.ScaleTo(GlassPanel, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.5f, "time", 0.01f, "easetype", iTween.EaseType.linear));

        UIcanvas.GetComponent<GraphicRaycaster>().enabled = true;
        //iTween.ScaleTo(ObjectivePanel, iTween.Hash("y", 1, "time", 0.5f, "delay", 1f, "easetype", iTween.EaseType.easeInOutSine));

    }

    public bool CheckFor_XP_Condition(int Current_Todo)
    {
        bool Condition = true;
        if (Current_Todo == 0)
        {
            if (CurrElecToMove == 2)
            {
                Collect_XP(AtomToMove[0]);
                Condition = false;
            }
            else
            {
                ShowInstructionPanel("Wrong parameter chosen");
                Condition = true;
            }
        }

        if (Current_Todo == 1)
        {
            if (IsSubmitted)
            {
                Collect_XP(AtomToMove[0]);
                Condition = false;
            }
            else
            {
                ShowInstructionPanel("Wrong parameter chosen");
                Condition = true;
            }
        }

        return Condition;
    }

    public void ShowLabels()
    {
        if (ShowLabels_Btn.GetComponent<Image>().sprite == ShowLabels_Btn.GetComponent<Button>().spriteState.pressedSprite)
        {
            ShowLabels_Btn.GetComponent<Image>().sprite = ShowLabels_Btn.GetComponent<Button>().spriteState.disabledSprite;
            for (int i = 0; i < HeadingLabels.Count; i++)
            {
                HeadingLabels[i].SetActive(false);
            }
        }
        else
        {
            ShowLabels_Btn.GetComponent<Image>().sprite = ShowLabels_Btn.GetComponent<Button>().spriteState.pressedSprite;
            for (int i = 0; i < HeadingLabels.Count; i++)
            {
                HeadingLabels[i].SetActive(true);
            }
        }
    }

    public void ShowInstructionPanel(string TextToShow)
    {
        var mouse = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);



        if (mouse.x < Screen.width / 2)
        {
            // WronOptionPanel
            //print("Mouse is on left side of screen.");
            InstructionPanel.GetComponent<RectTransform>().pivot = new Vector2(0, 0);
        }



        if (mouse.x > Screen.width / 2)
        {
            //print("Mouse is on right side of screen.");
            InstructionPanel.GetComponent<RectTransform>().pivot = new Vector2(1, 0);
        }

        InstructionPanel.transform.position = Input.mousePosition;
        UIcanvas.GetComponent<GraphicRaycaster>().enabled = false;
        Camera.main.GetComponent<PhysicsRaycaster>().enabled = false;
        iTween.Stop(InstructionPanel.gameObject);

        InstructionPanel.transform.Find("InstructionText").GetComponent<Text>().text = TextToShow;
        //iTween.ScaleTo(GlassPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 0.01f, "easetype", iTween.EaseType.spring));
        iTween.ScaleTo(InstructionPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 0.3f, "easetype", iTween.EaseType.spring, "oncomplete", "CloseInstructionPanel", "oncompletetarget", this.gameObject));
        ObjectivePanel.transform.localScale = new Vector3(1, 0, 1);
    }

    
    public void AddListenerToEvents(EventTriggerType eventType, Action MethodToCall, GameObject TriggerObjToAdd)
    {

        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall());
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }

    public void AddListenerToEvents(EventTriggerType eventType, Action<float> MethodToCall, GameObject TriggerObjToAdd, float p)
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall(p));
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }

    public void AddListenerToEvents(EventTriggerType eventType, Action<int> MethodToCall, GameObject TriggerObjToAdd, int p)
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall(p));
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Clear();
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }

    public void AddListenerToEvents(EventTriggerType eventType, Action<GameObject> MethodToCall, GameObject TriggerObjToAdd, GameObject p)
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall(p));
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }

    public void EnableOrbit()
    {

        if (!IsAR)
        {
            CamOrbit.DisableInteration = false;
            //print("EnableOrbit");
        }
        else
        {
            ArOrbit.DisableInteration = false;
        }
    }

    public void DisableOrbit()
    {

        if (!IsAR)
        {
            CamOrbit.DisableInteration = true;
            //print("DisableOrbit");
        }
        else
        {
            ArOrbit.DisableInteration = true;
        }
    }

    public void OpenInfoPanel()
    {
        UIZoomIn();
        iTween.ScaleTo(InfoPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 0.5f, "easetype", iTween.EaseType.spring));
        iTween.ScaleTo(GlassPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 0.01f, "easetype", iTween.EaseType.spring));

    }

    public void CloseInfoPanel(Button CloseBtn)
    {
        UIZoomOut();
        iTween.ScaleTo(InfoPanel, iTween.Hash("x", 0, "y", 0, "z", 0, "time", 0.2f, "easetype", iTween.EaseType.linear));
        iTween.ScaleTo(GlassPanel, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.2f, "time", 0.01f, "easetype", iTween.EaseType.linear));
        if (CloseBtn == Info_Cntue_Btn)
        {
            if (!IsAR)
                CamOrbit.DisableInteration = false;
            /*if (ActiveBtnCnt == 0)
            {
                for (int i = 0; i < Elements.Length; i++)
                {

                    Vector3 RandomPos = new Vector3(Elements[i].transform.localPosition.x, Elements[i].transform.localPosition.y, UnityEngine.Random.Range(-5f, 0f));
                    Vector3 CurrPos = Elements[i].transform.localPosition;

                    Elements[i].transform.localPosition = RandomPos;

                    iTween.MoveTo(Elements[i], iTween.Hash("position", CurrPos, "delay", 0.7f, "islocal", true, "time", UnityEngine.Random.Range(0.1f, 0.2f), "speed", 2.5f, "easetype", iTween.EaseType.easeInOutSine));


                }
            }*/
            Info_Close_Btn.gameObject.SetActive(true);
            Info_Cntue_Btn.gameObject.SetActive(false);
            
        }

       

    }

    public void CloseInstructionPanel()
    {
        UIcanvas.GetComponent<GraphicRaycaster>().enabled = true;
        Camera.main.GetComponent<PhysicsRaycaster>().enabled = true;
        //iTween.ScaleTo(GlassPanel, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 1f, "time", 0.01f, "easetype", iTween.EaseType.linear));
        iTween.ScaleTo(InstructionPanel, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.7f, "time", 0.3f, "easetype", iTween.EaseType.spring));
        if(!IsTopicUnlocked)
        iTween.ScaleTo(ObjectivePanel, iTween.Hash("y", 1, "time", 0.3f, "delay", 0.7f, "easetype", iTween.EaseType.easeInOutSine));
    }

    public void ToDoList_DDCancel()
    {
        if (ToDoList_DD.transform.Find("Dropdown List") != null)//&& Obj.transform.localScale.y!=1)
        {
            //print("cancelled........");
            //print("list.....cancelled........" + ToDoList_DD.transform.Find("Dropdown List").gameObject);
            ToDoList_DD.transform.Find("Arrow").transform.eulerAngles = new Vector3(0, 0, 0);
            iTween.ScaleTo(ToDoList_DD.transform.Find("Dropdown List").gameObject, iTween.Hash("y", 0, "time", 0.5f, "easetype", iTween.EaseType.spring));
            iTween.ScaleTo(ObjectivePanel, iTween.Hash("y", 1, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine));
            if (SidePannelOpen.transform.eulerAngles.z == 180 )
            {
                OpenSidePannel();
            }
        }
    }

    public void ExploringToDOList_DD()
    {
        //print("ShowDD.....");
        //ToDoList_DD.transform.Find("Topic_Objective").GetComponent<Text>().text = "";

        ToDoList_DD.transform.Find("Arrow").transform.eulerAngles = new Vector3(0, 0, 180);
        GameObject DDList = ToDoList_DD.transform.Find("Dropdown List").gameObject;
        DDList.transform.GetComponent<RectTransform>().sizeDelta = new Vector2(0, 250);
        DDList.transform.localScale = new Vector3(1, 0, 1);
        iTween.ScaleTo(DDList, iTween.Hash("y", 1, "time", 0.5f, "easetype", iTween.EaseType.spring));
        //iTween.ScaleTo(ToDoList_DD.transform.Find("Topic_Objective").gameObject, iTween.Hash("y", 0, "time", 0.5f, "easetype", iTween.EaseType.spring));
        if (!IsTopicUnlocked)
            iTween.ScaleTo(ObjectivePanel, iTween.Hash("y", 0, "time", 0.5f, "delay", 0.1f, "easetype", iTween.EaseType.easeInOutSine));

        if (SidePannelOpen.transform.eulerAngles.z == 0)
        {
            OpenSidePannel();
        }

        string TestStr = "";
        int CntToChangePos = 0;
        for (int i = 0; i < ToDoList_DD.options.Count; i++)
        {
            GameObject Child = GameObject.Find("Item " + i + ": " + ToDoList_DD.options[i].text);
            //Child.transform.parent.GetComponent<RectTransform>().sizeDelta = new Vector2(0,250);

            if (TestStr == ToDoList_DD.options[i].text)
            {
                Child.SetActive(false);
                Child.transform.localPosition = Child.transform.localPosition + new Vector3(0, 50 * CntToChangePos, 0);
                CntToChangePos++;
            }
            else
            {
                Child.transform.Find("XP_Text").GetComponent<Text>().text = "+" + Sub_Top_XP[i] + "XP";
                Child.transform.localPosition = Child.transform.localPosition + new Vector3(0, 50 * CntToChangePos, 0);

                if (i < TopicToCheck)
                {
                    Child.transform.Find("Item Checkmark").GetComponent<Image>().enabled = true;
                    //Child.transform.Find("XP_Text").GetComponent<Text>().color = Color.green / 2;
                    //Child.transform.Find("Item Checkmark").GetComponent<Image>().color = Color.green / 2;
                    Child.transform.Find("Item Background").GetComponent<Image>().enabled = false;
                }
                else if (i == TopicToCheck)
                {
                    Child.transform.Find("Item Background").GetComponent<Image>().enabled = true;// color = ReturnColorFromHex("#a01c65");
                                                                                                 //Child.transform.Find("Item Label").GetComponent<Text>().color = Color.white;
                }
            }
            TestStr = ToDoList_DD.options[i].text;
        }
    }


    public Color GetColorFromColorCode(string ColorCode)
    {
        Color ConvColor;
        bool Converted = ColorUtility.TryParseHtmlString(ColorCode, out ConvColor);
        return ConvColor;
    }



    public void ZoomCamera(float Value)
    {
        CamOrbit.distance = Value;
    }







    public void OpenSidePannel()
    {
        if (SidePannelOpen.transform.eulerAngles.z == 180)
        {
            //iTween.MoveAdd(SidePannel, iTween.Hash("x", -400f, "time",0.5f,"islocal",true, "easetype", iTween.EaseType.linear));
            //iTween.MoveAdd(SidePannelOpen.gameObject, iTween.Hash("x", -185f, "islocal", true, "time", 0.5f, "easetype", iTween.EaseType.linear));
            //SidePannelOpen.transform.eulerAngles = new Vector3(0,0,180);
            SidePannel.GetComponent<Animator>().Play("PannelForward");
        }
        else
        {
            //iTween.MoveAdd(SidePannel, iTween.Hash("x", 400f, "time", 0.5f, "islocal", true, "easetype", iTween.EaseType.linear));
            //iTween.MoveAdd(SidePannelOpen.gameObject, iTween.Hash("x", -185f, "islocal", true, "time", 0.5f, "easetype", iTween.EaseType.linear));
            //SidePannelOpen.transform.eulerAngles = new Vector3(0, 0, 0);
            SidePannel.GetComponent<Animator>().Play("PannelBackward");
        }
    }
    /*public void OpenTopSidePannel()
    {
        //print("Clicked......"+ PannelOpen.transform.eulerAngles.y);
        if (TopSidePannelOpen.gameObject.transform.localEulerAngles.z == 0)
        {
            TopSidePannel.GetComponent<Animator>().Play("TopButtonOpen");
            BtnselName.gameObject.SetActive(false);
            // TopSidePannelOpen.gameObject.transform.localEulerAngles = new Vector3(0, 0, 180);
        }
        else
        {
            TopSidePannel.GetComponent<Animator>().Play("TopButtonClose");
            // TopSidePannelOpen.gameObject.transform.localEulerAngles = new Vector3(0, 0, 0);
        }
    }*/

    // Update is called once per frame
    void FixedUpdate()
    {
        
        
        //if (!IsAR)
        //{

        //    if (CamOrbit.distance < CamOrbit.zoomStart && !IsUIZooming)
        //    {
        //        UIZoomIn();
        //        IsUIZooming = true;
        //    }
        //    else if (CamOrbit.distance > CamOrbit.zoomStart && IsUIZooming)
        //    {
        //        UIZoomOut();
        //        IsUIZooming = false;
        //    }
        //}

        /*if (CamOrbit.Zoom_bool && ActiveBtnCnt<8)
        {
            CamOrbit.zoomMin = CamOrbit.cameraRotUp * -0.01125f + 1.15f;
        }*/
    }



    public void UIReset()
    {
        ObjectivePanel.GetComponent<RectTransform>().anchoredPosition = new Vector3(0f, 5f, 0);

        //ValueDisplaySlider.transform.GetComponent<RectTransform>().anchoredPosition = new Vector3(-20f, 0f, 0);
        Info_Open_Btn.transform.GetComponent<RectTransform>().anchoredPosition = new Vector3(-37f, -150f, 0);
        ShowLabels_Btn.transform.GetComponent<RectTransform>().anchoredPosition = new Vector3(-90f, -150f, 0);
        Formulas_DD.transform.GetComponent<RectTransform>().anchoredPosition = new Vector3(-135f, -210f, 0);

        SidePannel.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(0f, 0f, 0);

        //print("side......" + SidePannel.transform.position+"....local....."+ SidePannel.transform.localPosition);
        if (!IsTopicUnlocked)
        {
            ToDoList_DD.transform.GetComponent<RectTransform>().anchoredPosition = new Vector3(135f, -150f, 0);
            XP_ToShow.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(135f, -150f, 0);

        }
        else
        {
            ToDoList_DD.transform.GetComponent<RectTransform>().anchoredPosition = new Vector3(-250f, -150f, 0);
            XP_ToShow.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(-250f, -150f, 0);

        }

        //Main_XP_ToShow.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(-135f, -60f, 0);

    }

    public void UIZoomOut()
    {
        if (ObjectivePanel.GetComponent<RectTransform>().anchoredPosition.y == 5)
            return;

        UIReset();
        iTween.MoveFrom(ShowLabels_Btn.gameObject, iTween.Hash("x", ShowLabels_Btn.transform.localPosition.x + 250, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveFrom(Info_Open_Btn.gameObject, iTween.Hash("x", Info_Open_Btn.transform.localPosition.x + 250, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveFrom(SidePannel.transform.parent.gameObject, iTween.Hash("x", SidePannel.transform.parent.localPosition.x - 1000f, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        //iTween.MoveFrom(ValueDisplaySlider.gameObject, iTween.Hash("x", ValueDisplaySlider.transform.localPosition.x + 250, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveFrom(ObjectivePanel, iTween.Hash("y", ObjectivePanel.transform.localPosition.y - 1000, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));

        iTween.MoveFrom(Formulas_DD.gameObject, iTween.Hash("x", Formulas_DD.transform.localPosition.x + 400, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveFrom(ToDoList_DD.gameObject, iTween.Hash("x", ToDoList_DD.transform.localPosition.x - 400, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveFrom(XP_ToShow.transform.parent.gameObject, iTween.Hash("x", XP_ToShow.transform.parent.localPosition.x - 400, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));

        //iTween.MoveFrom(Main_XP_ToShow.transform.parent.gameObject, iTween.Hash("x", Main_XP_ToShow.transform.parent.localPosition.x + 550, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
    }



    public void UIZoomIn()
    {
        if (ObjectivePanel.GetComponent<RectTransform>().anchoredPosition.y != 5)
            return;

        UIReset();
        //print("side......" + SidePannel.transform.position);
        iTween.MoveAdd(ObjectivePanel, iTween.Hash("y", ObjectivePanel.transform.localPosition.y - 1000, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveAdd(Info_Open_Btn.gameObject, iTween.Hash("x", Info_Open_Btn.transform.localPosition.x + 250, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveAdd(SidePannel.transform.parent.gameObject, iTween.Hash("x", SidePannel.transform.parent.localPosition.x - 1000f, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));

        //iTween.MoveAdd(ValueDisplaySlider.gameObject, iTween.Hash("x", ValueDisplaySlider.transform.localPosition.x + 250, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveAdd(ShowLabels_Btn.gameObject, iTween.Hash("x", ShowLabels_Btn.transform.localPosition.x + 250, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveAdd(XP_ToShow.transform.parent.gameObject, iTween.Hash("x", XP_ToShow.transform.parent.localPosition.x - 400, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveAdd(ToDoList_DD.gameObject, iTween.Hash("x", ToDoList_DD.transform.localPosition.x - 400, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveAdd(Formulas_DD.gameObject, iTween.Hash("x", Formulas_DD.transform.localPosition.x + 400, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        //iTween.MoveAdd(Main_XP_ToShow.transform.parent.gameObject, iTween.Hash("x", Main_XP_ToShow.transform.parent.localPosition.x + 550, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
    }

    public float ChangeToLocal_X(float Val)
    {
        float X_ToRet = 0;
        //print("wid........"+ UIcanvas.GetComponent<RectTransform>().rect.width);
        X_ToRet = ((UIcanvas.GetComponent<RectTransform>().rect.x) + Val);

        return X_ToRet;
    }

    public float ChangeToLocal_Y(float Val)
    {
        float Y_ToRet = 0;
        //print("wid........"+ UIcanvas.GetComponent<RectTransform>().rect.width);
        Y_ToRet = ((UIcanvas.GetComponent<RectTransform>().rect.y) + Val);

        return Y_ToRet;
    }


    public void Collect_XP(GameObject TargetPos)
    {

        UIcanvas.GetComponent<GraphicRaycaster>().enabled = false;
        Camera.main.GetComponent<PhysicsRaycaster>().enabled = false;


        XP_Effect.transform.position = Camera.main.WorldToScreenPoint(TargetPos.transform.position);

        XP_Effect.text = Sub_Top_XP[TopicToCheck] + "XP";

        XP_Effect.transform.localScale = new Vector3(2, 0, 2);

        iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 2, "y", 2, "z", 2, "time", 0.5f, "easetype", iTween.EaseType.easeOutElastic));

        iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", XP_Effect.transform.position.x, "y", XP_Effect.transform.position.y + 150, "z", 0, "delay", 0.3f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

        iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", 150, "y", XP_ToShow.transform.position.y, "z", 0, "delay", 1f, "time", 1f, "easetype", iTween.EaseType.easeInSine, "oncomplete", "Add_To_XP", "oncompletetarget", this.gameObject));

    }

    public void Add_To_XP()
    {

        //print("TopicToCheck......."+ TopicToCheck+"....sub......"+Sub_Topics.Count);
        if (TopicToCheck < (Sub_Top_ToDoList.Count))
        {
            Sub_XP_Earned += float.Parse(Sub_Top_XP[TopicToCheck]);

            XP_Effect.text = "";
            XP_ToShow.text = Sub_XP_Earned + "/" + Total_XP;

            TopicToCheck++;
            if (TopicToCheck <= (Sub_Top_ToDoList.Count - 1))
            {
                ObjectivePanel.transform.Find("ObjectiveText").GetComponent<Text>().text = Sub_Top_ToDoList[TopicToCheck];
                ObjectivePanel.transform.localScale = new Vector3(1, 0, 1);
                iTween.ScaleTo(ObjectivePanel, iTween.Hash("y", 1, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine));
                UIcanvas.GetComponent<GraphicRaycaster>().enabled = true;
                Camera.main.GetComponent<PhysicsRaycaster>().enabled = true;
                //ToDoList_DD.transform.Find("Topic_Objective").GetComponent<Text>().text = "Objective: " + Sub_Top_ToDoList[TopicToCheck];
            }
            else
            {
                XP_Effect.transform.position = (XP_ToShow.transform.position);

                XP_Effect.text = Total_XP + "XP";

                XP_Effect.transform.localScale = new Vector3(1, 0, 1);

                iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 2, "y", 2, "z", 2, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeOutElastic));

                iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", XP_Effect.transform.position.x, "y", XP_Effect.transform.position.y + 50, "z", 0, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

                iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", Main_XP_ToShow.transform.position.x, "y", Main_XP_ToShow.transform.position.y, "z", 0, "delay", 1.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine, "oncomplete", "AddTotalToMainXP", "oncompletetarget", this.gameObject));

                iTween.MoveTo(ToDoList_DD.gameObject, iTween.Hash("x", ChangeToLocal_X(-400), "delay", 3.5f, "time", 0.5f, "islocal", true, "easetype", iTween.EaseType.easeInOutSine));

                iTween.MoveTo(XP_ToShow.transform.parent.gameObject, iTween.Hash("x", ChangeToLocal_X(-400), "delay", 3.5f, "time", 0.5f, "islocal", true, "easetype", iTween.EaseType.easeInOutSine));
                //print("label......" + LabelImage.GetComponent<RectTransform>().anchoredPosition+".......explode...."+ ExplodeSlider.GetComponent<RectTransform>().anchoredPosition);


                //iTween.MoveTo(ValueDisplaySlider.gameObject, iTween.Hash("x", ChangeToLocal_X ( 150), "delay", 0.7f, "time", 0.5f, "islocal", true, "easetype", iTween.EaseType.easeInOutSine));
            }
        }

    }

    public void AddTotalToMainXP()
    {
        if (OptionUnlockedTill < 1)
        {
            OptionUnlockedTill++;
            UIcanvas.GetComponent<GraphicRaycaster>().enabled = true;
            Camera.main.GetComponent<PhysicsRaycaster>().enabled = true;
            float MainXp = float.Parse(Main_XP_ToShow.text);
            MainXp += Total_XP;
            Main_XP_ToShow.text = MainXp.ToString();
            //ObjectivePanel.transform.Find("ObjectiveText").GetComponent<Text>().text = "YaY!......Next topic unlocked";
            XP_Effect.text = "";
            IsTopicUnlocked = true;
            iTween.ScaleTo(ObjectivePanel, iTween.Hash("y", 0, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine));



        }
    }
}
