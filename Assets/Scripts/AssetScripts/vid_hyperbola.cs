﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;

public class vid_hyperbola : MonoBehaviour, IPointerDownHandler
{
    // Start is called before the first frame update

    public Canvas C_BG;

    public orbit CamOrbit;

    public AROrbitControls ArOrbit;

    public bool IsAR;

    public GameObject MainObj, Ground;

    public int OptionUnlockedTill, CurrentTopicSelected, TopicToCheck;

    public float Sub_XP_Earned, Total_XP;

    public Dropdown Options_DD, ToDoList_DD;

    public GameObject InstructionPanel_DD, ToDoList_Panel;

    public List<Dictionary<string, object>> CSV_data;

    public List<String> TaskList, XpList, InfoText;

    public List<GameObject> AllTaskData;

    public GameObject[] Labels;

    public Text XP_ToShow, XP_Effect, Main_XP_ToShow, localTotalXpValueText, localGainedXpValueText, ObjectiveInfoText, LGListText, IListText, AListText,
        InfoHeadingText, GF_ResultTxt, E_valueTxt, m_valtxt;

    public GameObject TaskObj, AllTaskListObj;

    public RectTransform rT;

    public int localTotalXpValue, localGaniedXpValue, MainXpValue;

    public GameObject ControlPanel, LocalXP, Main_Xp, ObjectivePanel, InformationPanel, InfoButton, MainSlider, InfoPanelBg, TasklistPanel, WronOptionPanel, Gained_XP_ArrowBut;

    public Button InfoCloseBtn, InfoContinueBtn, Gained_XP_Panel, CpCloseBtn, IncButton1, DecButton1, IncButton2, DecButton2, DecButton3, IncButton3;

    public bool sessionStart;

    public TextAsset CSVFile;

    public AssetBundle assetBundle;

    public string[] CorrectMsgs = new string[] { "Nice!", "WellDone!", "Excellent!", "Correct!" };

    public Color Highlight_Color, Normal_Color;

    public Text MessageT, DescText;

    //......................

    public GameObject hyp1, hyp2, Point, middleRod, Ring1, Ring2, PointMovebale, Focus1, Focus2, assymptos1, 
        assymptos2, aLine, bLine, xaxis1, xaxis2, yaxis1, yaxis2, FociRelated, asemptos1, asemptos2, AsymRelated, Water1, Water2;

    public float a, b,x,y,c, pf1,pf2;

    public Slider Slider_a,Slider_b, ASlider_pointx;

    public List<Vector3> AllPos, AllPosD;

    public TextMeshProUGUI a2, b2, PfvalTxt, Pf2valTxt, pfConstant;

    public Button EquilateralBtn, FociBtn, AssympBtn;

    public bool equi, loclChk;

    public GameObject PFDisVal_label, PF2DisVal_label, pToF, pToF2;

    void Start()
    {

        LoadFromAsssetBundle();

        MainObj = this.gameObject;

        C_BG = GameObject.Find("Canvas_BG").GetComponent<Canvas>();

        Ground = GameObject.Find("Ground");

        if (!IsAR)
        {
            CamOrbit = Camera.main.GetComponentInParent<orbit>();

            C_BG.transform.parent = Camera.main.transform;
            C_BG.GetComponentInChildren<RawImage>().enabled = true;
            C_BG.renderMode = RenderMode.ScreenSpaceCamera;
            C_BG.worldCamera = Camera.main;
            CamOrbit.target = Camera.main.transform.parent;

            CamOrbit.target.position = new Vector3(0, 0.63f, 0);
            CamOrbit.maxRotUp = 45;
            CamOrbit.minRotUp = 2;

            CamOrbit.maxSideRot = 180;
            CamOrbit.minSideRot = 180;

            CamOrbit.cameraRotUpStart = 0;
            CamOrbit.zoomMin = 1f;
            CamOrbit.zoomMax = 3f;
            CamOrbit.zoomStart = 1.5f;
            CamOrbit.Start();
        }
        else
        {
            Ground.SetActive(false);
            C_BG.gameObject.SetActive(false);
            ArOrbit = MainObj.transform.parent.gameObject.GetComponent<AROrbitControls>();
        }
        
        sessionStart = false;
        ReadingDataFromCSVFile();
        ReadingXps();
        FindingObjects();
        TaskListCreation();
        AssigningClickEvents();
        LabelsAssignment();
        AssignInitialValues();
    }

    public int totXp, loclXp;
    public void ReadingXps()
    {
        totXp = PlayerPrefs.GetInt("TotalXP");
        loclXp = PlayerPrefs.GetInt("MaxEarnings");

        if (loclXp != 0)
        {
            OptionUnlockedTill = 0;
        }
        else
        {
            OptionUnlockedTill = 1;
        }

        TopicToCheck = 1;
        MainXpValue = totXp;

        int t = 0;
        int p = 0;

        for (int k = 0; k < TaskList.Count; k++)
        {
            p = loclXp / TaskList.Count;
            XpList.Add(p.ToString());
            t = t + p;
        }

        if (t != loclXp)
        {
            XpList[XpList.Count - 1] = (p + (loclXp - t)).ToString();
        }
    }
    public float result;
    public void GetResult()
    {
        result = ((float)(TopicToCheck - 1) / (float)(TaskList.Count)) * 100;
        Camera.main.SendMessage("UnloadActually", result);
    }
    public void LoadFromAsssetBundle()
    {
        //......................................................For Testing ..............................................................

       // CSVFile = Resources.Load("vid_hyperbolaCSV") as TextAsset;

        //............................................................ For Assetbundle Creation ..........................................

        assetBundle = AssetBundle.LoadFromFile(PlayerPrefs.GetString("AssetBundleUrl"));

        CSVFile = assetBundle.LoadAsset(PlayerPrefs.GetString("LibraryName") + "CSV", typeof(TextAsset)) as TextAsset;       

        //...............................................................................................................................

        Camera.main.gameObject.AddComponent<PhysicsRaycaster>();

        //Camera.main.backgroundColor = Color.black;

        GameObject TargetForCamera = GameObject.Find("Target");

        if (TargetForCamera != null)
        {
            TargetForCamera.AddComponent<orbit>();
            CamOrbit = TargetForCamera.GetComponent<orbit>();
            IsAR = false;
        }
        else
        {
            this.transform.parent.gameObject.AddComponent<AROrbitControls>();
            ArOrbit = this.transform.parent.gameObject.GetComponent<AROrbitControls>();
            IsAR = true;
        }
    }
    public void ReadingDataFromCSVFile()
    {

        TaskList = new List<string>();
        XpList = new List<string>();
        InfoText = new List<string>();

        AllTaskData = new List<GameObject>();

        CSV_data = CSVReader.Read(CSVFile);

        for (var i = 0; i < CSV_data.Count; i++)
        {
            if (CSV_data[i]["TaskList"].ToString() != "" && CSV_data[i]["TaskList"].ToString() != null)
            {
                TaskList.Add(CSV_data[i]["TaskList"].ToString());
            }

            //if (CSV_data[i]["XPList"].ToString() != "" && CSV_data[i]["XPList"].ToString() != null)
            //{
            //    XpList.Add(CSV_data[i]["XPList"].ToString());
            //}

            if (CSV_data[i]["InfoText"].ToString() != "" && CSV_data[i]["InfoText"].ToString() != null)
            {
                InfoText.Add(CSV_data[i]["InfoText"].ToString());
            }
        }
    }
    public void FindingObjects()
    {
        InformationPanel = GameObject.Find("InformationPanel");

        InfoCloseBtn = GameObject.Find("InfoCloseBtn").GetComponent<Button>();

        InfoContinueBtn = GameObject.Find("InfoContinueBtn").GetComponent<Button>();

        InfoPanelBg = GameObject.Find("InfoPanelBg");

        XP_ToShow = GameObject.Find("localGainedXpValueText").GetComponent<Text>();

        XP_Effect = GameObject.Find("Effect_Text").GetComponent<Text>();

        Main_XP_ToShow = GameObject.Find("Main_XP_Value").GetComponent<Text>();

        localGainedXpValueText = GameObject.Find("localGainedXpValueText").GetComponent<Text>();

        localTotalXpValueText = GameObject.Find("GainedXP_LocalTotalXp").GetComponent<Text>();

        ObjectiveInfoText = GameObject.Find("ObjectiveInfoText").GetComponent<Text>();

        TaskObj = GameObject.Find("TaskObj");

        AllTaskListObj = GameObject.Find("AllTaskList");

        ControlPanel = GameObject.Find("ControlPanel");
        LocalXP = GameObject.Find("LocalXP");
        Main_Xp = GameObject.Find("Main_Xp");
        ObjectivePanel = GameObject.Find("ObjectivePanel");
        InformationPanel = GameObject.Find("InformationPanel");
        InfoButton = GameObject.Find("InfoButton");
        MainSlider = GameObject.Find("MainSlider");
        WronOptionPanel = GameObject.Find("WrongPanel");

        Gained_XP_Panel = GameObject.Find("Gained_XP_Panel").GetComponent<Button>();
        TasklistPanel = GameObject.Find("TasklistPanel");
        CpCloseBtn = GameObject.Find("CpCloseBtn").GetComponent<Button>();

        InfoHeadingText = GameObject.Find("InfoHeadingText").GetComponent<Text>();
        LGListText = GameObject.Find("LGListText").GetComponent<Text>();
        IListText = GameObject.Find("IListText").GetComponent<Text>();
        AListText = GameObject.Find("AListText").GetComponent<Text>();

        Gained_XP_ArrowBut = GameObject.Find("Gained_XP_ArrowBut");

        //......................................................     

        hyp1 = GameObject.Find("hyp1");

        hyp2 = GameObject.Find("hyp2");

        Point = GameObject.Find("Point");

        Slider_a = GameObject.Find("ASlider_X").GetComponent<Slider>();
        Slider_b = GameObject.Find("ASlider_Y").GetComponent<Slider>();

        //ASlider_pointx = GameObject.Find("ASlider_pointx").GetComponent<Slider>();

        middleRod = GameObject.Find("middleRod");
        Ring1 = GameObject.Find("Ring1");
        Ring2 = GameObject.Find("Ring2");

        PointMovebale = GameObject.Find("PointMovebale");

        Focus1 = GameObject.Find("Focus1");
        Focus2 = GameObject.Find("Focus2");

        assymptos1 = GameObject.Find("assymptos1");
        assymptos2 = GameObject.Find("assymptos2");

        aLine = GameObject.Find("aLine");
        bLine = GameObject.Find("bLine");

        a2 = GameObject.Find("a2").GetComponent<TextMeshProUGUI>();
        b2 = GameObject.Find("b2").GetComponent<TextMeshProUGUI>();

        PfvalTxt = GameObject.Find("PfvalTxt").GetComponent<TextMeshProUGUI>();
        Pf2valTxt = GameObject.Find("Pf2valTxt").GetComponent<TextMeshProUGUI>();

        pfConstant = GameObject.Find("pfConstant").GetComponent<TextMeshProUGUI>();


        EquilateralBtn = GameObject.Find("EquilateralBtn").GetComponent<Button>();

        xaxis1 = GameObject.Find("xaxis1");

        yaxis1 = GameObject.Find("yaxis1");

        xaxis2 = GameObject.Find("xaxis2");

        yaxis2 = GameObject.Find("yaxis2");

        FociBtn = GameObject.Find("FociBtn").GetComponent<Button>();

        FociRelated = GameObject.Find("FociRelated");

        asemptos1 = GameObject.Find("asemptos1");
        asemptos2 = GameObject.Find("asemptos2");

        AssympBtn = GameObject.Find("AssympBtn").GetComponent<Button>();

        AsymRelated = GameObject.Find("AsymRelated");

        Water1 = GameObject.Find("Water1");

        Water2 = GameObject.Find("Water2");

        PFDisVal_label = GameObject.Find("PFDisVal_label");
        PF2DisVal_label = GameObject.Find("PF2DisVal_label");

        pToF = GameObject.Find("pToF");
        pToF2 = GameObject.Find("pToF2");
    }
    public void AssigningClickEvents()
    {
        iTween.ScaleTo(InformationPanel.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

        InfoCloseBtn.onClick.AddListener(() => CloseInfoPanel());

        InfoContinueBtn.onClick.AddListener(() => ContinueInfoPanel());

        InfoButton.GetComponent<Button>().onClick.AddListener(() => OpenInfoPanel());

        Gained_XP_Panel.onClick.AddListener(() => OpenTaskList());

        CpCloseBtn.onClick.AddListener(() => OpenOrCloseControlPanel());

        Slider_a.onValueChanged.AddListener(delegate { Slider_aVal(Slider_a.value, Slider_a.gameObject.GetComponentInChildren<Text>()); });        

        AddListener(EventTriggerType.PointerDown, DisableOrbit, Slider_a.gameObject);
        AddListener(EventTriggerType.PointerUp, EnableOrbit, Slider_a.gameObject);

        Slider_b.onValueChanged.AddListener(delegate { Slider_bVal(Slider_b.value, Slider_b.gameObject.GetComponentInChildren<Text>()); });

        AddListener(EventTriggerType.PointerDown, DisableOrbit, Slider_b.gameObject);
        AddListener(EventTriggerType.PointerUp, EnableOrbit, Slider_b.gameObject);

        MainSlider.GetComponent<Slider>().onValueChanged.AddListener(delegate { SliderVal(MainSlider.GetComponent<Slider>().value, MainSlider.gameObject.GetComponentInChildren<Text>()); });
        AddListener(EventTriggerType.PointerDown, DisableOrbit, MainSlider.gameObject);
        AddListener(EventTriggerType.PointerUp, EnableOrbit, MainSlider.gameObject);


        EquilateralBtn.onClick.AddListener(() => EquilateralFun());

        FociBtn.onClick.AddListener(() => FociFun());

        AssympBtn.onClick.AddListener(() => AsymptFun());

        AsymRelated.SetActive(false);

    }
    public void AssignInitialValues()
    {
        if (OptionUnlockedTill > 0)
        {
            LocalXP.transform.localScale = new Vector3(0, 0, 0);
            ObjectivePanel.transform.localScale = new Vector3(0, 0, 0);
        }

        InfoCloseBtn.gameObject.transform.localScale = new Vector3(0, 0, 0);
        localGainedXpValueText.text = "0";
        Main_XP_ToShow.text = MainXpValue + "";
        WronOptionPanel.transform.localScale = new Vector3(0, 0, 0);
        InfoPanelBg.transform.localScale = new Vector3(1, 1, 1);

        equi = false;
        loclChk = false;
        Calculation();

        FociRelated.SetActive(false);
    }
    public void TaskListCreation()
    {
        rT = AllTaskListObj.transform.parent.GetComponent<RectTransform>();

        localGaniedXpValue = 0;

        for (int i = 0; i < TaskList.Count; i++)
        {

            GameObject Tl = (GameObject)Instantiate(TaskObj);
            Tl.transform.SetParent(AllTaskListObj.transform);
            Tl.transform.localScale = new Vector3(1, 1, 1);
            Tl.name = i.ToString();

            AllTaskData.Add(Tl);
            AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(false);
            AllTaskData[i].gameObject.transform.GetChild(2).gameObject.SetActive(false);
            AllTaskData[i].gameObject.transform.GetChild(3).GetComponentInChildren<Text>().text = TaskList[i];
            AllTaskData[i].gameObject.transform.GetChild(4).GetComponentInChildren<Text>().text = "+" + XpList[i] + " XP";

            localTotalXpValue += int.Parse(XpList[i]);
        }

        rT.sizeDelta = new Vector2(rT.sizeDelta.x, TaskList.Count * 50);
        localTotalXpValueText.text = "/" + localTotalXpValue.ToString();

        AllTaskData[0].gameObject.transform.GetChild(2).gameObject.SetActive(true);
        ObjectiveInfoText.text = AllTaskData[0].gameObject.transform.GetChild(3).GetComponentInChildren<Text>().text;
    }
    public void LocalXpCalculation()
    {
        for (int i = 0; i < TaskList.Count; i++)
        {
            AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(false);
            AllTaskData[i].gameObject.transform.GetChild(2).gameObject.SetActive(false);
        }

        if (OptionUnlockedTill < 1)
        {
            if ((TopicToCheck) < (TaskList.Count + 1))
            {
                localGaniedXpValue = 0;

                for (int i = 0; i < TopicToCheck - 1; i++)
                {
                    localGaniedXpValue += int.Parse(XpList[i]);
                }

                for (int i = 0; i < TopicToCheck; i++)
                {
                    AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(true);
                }

                localGainedXpValueText.text = localGaniedXpValue.ToString();

                AllTaskData[TopicToCheck - 1].gameObject.transform.GetChild(2).gameObject.SetActive(true);

                ObjectiveInfoText.text = AllTaskData[TopicToCheck - 1].gameObject.transform.GetChild(3).GetComponentInChildren<Text>().text;
            }
            else
            {
                for (int i = 0; i < TaskList.Count; i++)
                {
                    AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(true);
                    AllTaskData[i].gameObject.transform.GetChild(2).gameObject.SetActive(false);
                }

                localGainedXpValueText.text = localTotalXpValue.ToString();

                XP_Effect.text = "+" + localTotalXpValue + " XP";

                XP_Effect.transform.position = (XP_ToShow.transform.position);

                iTween.Stop(XP_Effect.gameObject);

                iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeOutElastic));

                iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", XP_Effect.transform.position.x, "y", XP_Effect.transform.position.y + 50, "z", 0, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

                iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", Main_XP_ToShow.transform.position.x, "y", Main_XP_ToShow.transform.position.y, "z", 0, "delay", 1f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

                iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "delay", 1.5f, "time", 0.5f, "easetype", iTween.EaseType.linear, "oncomplete", "MainXpCalculation", "oncompletetarget", this.gameObject));
            }
        }
        else
        {
            for (int i = 0; i < TaskList.Count; i++)
            {
                AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(true);
                AllTaskData[i].gameObject.transform.GetChild(2).gameObject.SetActive(false);
            }

            localGainedXpValueText.text = localTotalXpValue.ToString();
        }
    }
    public void CloseAllpanels()
    {
        ControlPanel.GetComponent<Animator>().Play("CpanelFullClose");
        LocalXP.GetComponent<Animator>().Play("LocalXpPanelClose");
        Main_Xp.GetComponent<Animator>().Play("MainXpPanelClose");
        ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelClose");
        InfoButton.GetComponent<Animator>().Play("InfoButtonClose");
        MainSlider.GetComponent<Animator>().Play("MainSliderClose");       

        if (TasklistPanel.gameObject.transform.localScale.y != 0)
            iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));
    }
    public void OpenAllpanels()
    {
        if (OptionUnlockedTill < 1)
        {
            if (TopicToCheck <= (TaskList.Count))
            {
                LocalXP.GetComponent<Animator>().Play("LocalXpPanelOPen");
                ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen");
            }
        }

        ControlPanel.GetComponent<Animator>().Play("CpanelOpen");

        Main_Xp.GetComponent<Animator>().Play("MainXpPanelOpen");

        InfoButton.GetComponent<Animator>().Play("InfoButtonOpen");
        MainSlider.GetComponent<Animator>().Play("MainSliderOpen");
        iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "time", 0.2f, "easetype", iTween.EaseType.linear));
        iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));

        
    }
    public void AddXpEffect()
    {
        if (TopicToCheck <= (TaskList.Count))
        {

            ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelClose");

            GameObject.Find("Canvas").GetComponent<CanvasGroup>().interactable = false;

            XP_Effect.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);

            XP_Effect.text = "+" + int.Parse(XpList[TopicToCheck - 1]) + " XP";

            XP_Effect.transform.localScale = new Vector3(1, 0, 1);

            iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeOutElastic));

            iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", XP_Effect.transform.position.x, "y", XP_Effect.transform.position.y + 50, "z", 0, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

            iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", XP_ToShow.transform.position.x, "y", XP_ToShow.transform.position.y, "z", 0, "delay", 1.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

            iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "delay", 2f, "time", 0.5f, "easetype", iTween.EaseType.linear, "oncomplete", "LocalXpCalculation", "oncompletetarget", this.gameObject));

            TopicToCheck++;

            Invoke("EnableRayCast", 2.1f);
        }

    }
    public void EnableRayCast()
    {
        //Camera.main.GetComponent<PhysicsRaycaster>().enabled = true;
        GameObject.Find("Canvas").GetComponent<CanvasGroup>().interactable = true;
        if (OptionUnlockedTill < 1)
        {
            if (InformationPanel.gameObject.transform.localScale.x == 0f)
            {
                ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen", 0, 0f);
            }
        }
    }
    public void MainXpCalculation()
    {
        if (OptionUnlockedTill == 0)
        {
            OptionUnlockedTill = 1;

            MainXpValue += localTotalXpValue;

            Main_XP_ToShow.text = MainXpValue.ToString();

            LocalXP.GetComponent<Animator>().Play("LocalXpPanelClose");

            ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelClose");

            if ((ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelClose")) || (ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelFullClose")))
            {
                ControlPanel.GetComponent<Animator>().Play("CpanelOpen");
            }
        }
    }
    public void CloseInfoPanel()
    {
        sessionStart = true;
        InfoPanelBg.SetActive(false);
        iTween.ScaleTo(InformationPanel.gameObject, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));
        OpenAllpanels();
    }
    public void ContinueInfoPanel()
    {
        sessionStart = true;
        InfoPanelBg.SetActive(false);
        iTween.ScaleTo(InformationPanel.gameObject, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));
        InfoCloseBtn.transform.localScale = new Vector3(0.27f, 0.27f, 0.27f);
        InfoContinueBtn.transform.localScale = new Vector3(0, 0, 0);
        OpenAllpanels();
    }
    public void OpenInfoPanel()
    {
        sessionStart = false;
        iTween.ScaleTo(InformationPanel.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));
        InfoPanelBg.SetActive(true);
        iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "time", 0.2f, "easetype", iTween.EaseType.linear));
        iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));
        CloseAllpanels();
    }
    public void OpenTaskList()
    {
        if (TasklistPanel.gameObject.transform.localScale.y != 1)
        {
            iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.01f, "time", 0.2f, "easetype", iTween.EaseType.easeInSine));

            ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelClose");

            iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 180, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));

            if ((ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelOpen")))
            {
                ControlPanel.GetComponent<Animator>().Play("CpanelClose");
            }
        }
        else
        {
            iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "delay", 0.01f, "time", 0.2f, "easetype", iTween.EaseType.easeInSine));
            iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));

            ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen");

            ControlPanel.GetComponent<Animator>().Play("CpanelOpen");
        }
    }
    public void OpenOrCloseControlPanel()
    {
        if ((ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelClose")) || (ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelFullClose")))
        {
            ControlPanel.GetComponent<Animator>().Play("CpanelOpen");
        }
        else
        {
            ControlPanel.GetComponent<Animator>().Play("CpanelClose");
        }
    }
    public void WrongOptionSelected()
    {
        WronOptionPanel.transform.localScale = new Vector3(0, 0, 0);

        iTween.Stop(WronOptionPanel.gameObject);

        iTween.ScaleTo(WronOptionPanel.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.01f, "time", 0.2f, "easetype", iTween.EaseType.easeInSine));

        iTween.ScaleTo(WronOptionPanel.gameObject, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 1f, "time", 0.2f, "easetype", iTween.EaseType.easeInSine));

        // InfoPanelBg.SetActive(true);

        Invoke("CloseBlurBg", 1f);

        GameObject.Find("Canvas").GetComponent<CanvasGroup>().interactable = false;

        ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelClose");

        var mouse = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);

        if (mouse.x < Screen.width / 2)
        {
            WronOptionPanel.GetComponent<RectTransform>().pivot = new Vector2(0, 0);
        }
       else
        {
            WronOptionPanel.GetComponent<RectTransform>().pivot = new Vector2(1, 0);
        }

        WronOptionPanel.transform.position = Input.mousePosition;
    }
    public void CloseBlurBg()
    {
        InfoPanelBg.SetActive(false);
        GameObject.Find("Canvas").GetComponent<CanvasGroup>().interactable = true;

        if (OptionUnlockedTill < 1)
        {
            if (InformationPanel.gameObject.transform.localScale.x == 0f)
            {
                ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen", 0, 0f);
            }
        }
    }
    public void OnPointerDown(PointerEventData eventData)
    {
        if (OptionUnlockedTill < 1)
        {
            if (InformationPanel.gameObject.transform.localScale.x != 1 && sessionStart)
            {           
                if (TopicToCheck <= (TaskList.Count))
                {
                    iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "time", 0.2f, "easetype", iTween.EaseType.linear));
                    iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));
                    ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen");
                    ControlPanel.GetComponent<Animator>().Play("CpanelOpen");
                }
            }
        }
    }
    public void AlertPanel(string s, bool y)
    {
        WrongOptionSelected();
        WronOptionPanel.GetComponentInChildren<Text>().text = s;

        if (y)
        {
            WronOptionPanel.GetComponentInChildren<Text>().color = new Color(0.7568f, 0.8588f, 0.075f, 1);
        }
        else
        {
            WronOptionPanel.GetComponentInChildren<Text>().color = new Color(0.9921f, 0.4431f, 0.08621f, 1);
        }
    }
    public void LabelsAssignment()
    {
        GameObject Label = GameObject.Find("Labels");

        Labels = new GameObject[Label.transform.childCount];

        for (int j = 0; j < Labels.Length; j++)
        {
            Labels[j] = Label.transform.GetChild(j).gameObject;

            GameObject TempLabels = Labels[j];

            foreach (Transform ChildLabel in TempLabels.transform)
            {
                if (ChildLabel.name.Contains("SmoothLook"))
                {
                    ChildLabel.gameObject.AddComponent<SmoothLook>();
                    //ChildLabel.gameObject.AddComponent<ScaleRelativeToCamera>();

                }
            }
        }

        for (int k = 0; k < Labels.Length; k++)
        {
            string name = Labels[k].name.Remove(Labels[k].name.Length - 6);

            if (GameObject.Find(name + "") != null)
            {
                GameObject dummy = GameObject.Find(name + "").gameObject;

                Labels[k].transform.parent = dummy.transform;
                Labels[k].transform.rotation = Quaternion.identity;
            }

        }


    }
    public void AddListener(EventTriggerType eventType, Action MethodToCall, GameObject TriggerObjToAdd)
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall());
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }
    public void AddListener(EventTriggerType eventType, Action<float> MethodToCall, GameObject TriggerObjToAdd)
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall(TriggerObjToAdd.GetComponent<Slider>().value));
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }
    public void AddListener(EventTriggerType eventType, Action<GameObject, int> MethodToCall, GameObject TriggerObjToAdd, GameObject obj, int t)
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall(obj, t));
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }
    public void AddListener(EventTriggerType eventType, Action<TextMeshPro> MethodToCall, GameObject TriggerObjToAdd, TextMeshPro t)
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall(t));
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }

    public Text ttt;
    public void EnableOrbit()
    {
        if (!IsAR)
        {
            CamOrbit.DisableInteration = false;
        }
        else
        {
            ArOrbit.DisableInteration = false;
        }

        if (OptionUnlockedTill < 1)
        {
            if (InformationPanel.gameObject.transform.localScale.x != 1)
            {
                if (TopicToCheck <= (TaskList.Count))
                {
                    iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "time", 0.2f, "easetype", iTween.EaseType.linear));
                    iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));
                    ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen");
                }
            }
        }
        loclChk = false;
        CancelInvoke("EqualSlidersHigh");
        CancelInvoke("EqualSlidersLow");

        if (OptionUnlockedTill < 1) { 
            if(TopicToCheck == 1)
            {
                if (ttt.gameObject.name == "SliderA") {
                    AddXpEffect();

                }
                else
                {
                    AlertPanel("Wrong parameter chosen", false);
                }
            }
            else if (TopicToCheck == 2)
            {
                if (ttt.gameObject.name == "SliderB")
                {
                    AddXpEffect();
                }
                else
                {
                    AlertPanel("Wrong parameter chosen", false);
                }
            }
        }

        ttt = null;
    }
    public void DisableOrbit()
    {
        if (!IsAR)
        {
            CamOrbit.DisableInteration = true;
        }
        else
        {
            ArOrbit.DisableInteration = true;
        }
    }
    public Vector3 MidPoint(GameObject obj1, GameObject obj2)
    {
        Vector3 midp = (obj1.transform.localPosition + obj2.transform.localPosition) / 2;
        return midp;
    }
    public float Scale(GameObject obj1, GameObject obj2)
    {
        float scl = Vector3.Distance(obj1.transform.localPosition, obj2.transform.localPosition);
        return scl;
    }
    public void QueueChange(Renderer r, int num)
    {
        r.material.renderQueue = num;
    }
    public void SliderVal(float val, Text t)
    {
        ttt = t;

        t.text = val.ToString("F2");
        Calculation();



       // equi = false;
    }
    public void Slider_aVal(float val, Text t) {

        ttt = t;

        if (equi && !loclChk)
        {
         Slider_b.value = val;           
        }
        t.text = val.ToString("F2");
        Calculation();

    }
    public void Slider_bVal(float val, Text t)
    {

        ttt = t;

        if (equi && !loclChk)
        {
            Slider_a.value = val;
        }
        t.text = val.ToString("F2");
        Calculation();

    }
    public void Calculation()
    {



        a = Slider_a.value;
        b = Slider_b.value;

        y = MainSlider.GetComponent<Slider>().value;
        x = Mathf.Sqrt((b * b) * (1 + ((y * y) / (a * a))));

        //assymptos1.GetComponent<LineRenderer>().SetPosition(0, new Vector3( (-(b / a) * (Point.transform.localPosition.y - 3) * 0.1f), -3 * 0.1f, 0));
        //assymptos1.GetComponent<LineRenderer>().SetPosition(1, assymptos1.transform.localPosition);

        //assymptos1.GetComponent<LineRenderer>().SetPosition(2, new Vector3(-3 * 0.1f, Point.transform.localPosition.y + ((b / a) * (Point.transform.localPosition.y -3) * 0.1f), 0));
        //assymptos1.GetComponent<LineRenderer>().SetPosition(3, assymptos1.transform.localPosition);

        PointMovebale.transform.localPosition = new Vector3(x * 0.1f, Point.transform.localPosition.y + y * 0.1f, 0);

        AllPos.Clear();
        AllPosD.Clear();

        for (float i = -3f; i < 3; i = i + 0.05f)
        {
            x = i;
            y = Mathf.Sqrt((b * b) * (1 + ((x * x) / (a * a))));
            AllPos.Add(new Vector3(0 + y * 0.1f, Point.transform.localPosition.y + x * 0.1f, 0));
            AllPosD.Add(new Vector3(0 - y * 0.1f, Point.transform.localPosition.y - x * 0.1f, 0));
        }

        //hyp1.GetComponent<LineRenderer>().positionCount = AllPos.Count;
        //hyp2.GetComponent<LineRenderer>().positionCount = AllPos.Count;

        for (int i = 0; i < AllPos.Count; i++)
        {
            hyp1.GetComponent<LineRenderer>().SetPosition(i, AllPos[i]);
        }

        for (int i = 0; i < AllPosD.Count; i++)
        {
            hyp2.GetComponent<LineRenderer>().SetPosition(i, AllPosD[i]);
        }

        float p = Mathf.Sqrt((b * b) * (1 + ((0 * 0) / (a * a))));

        middleRod.transform.localScale = new Vector3(p, 1, 1);

        Ring1.transform.localPosition = new Vector3(p * 0.1f, Ring1.transform.localPosition.y, Ring1.transform.localPosition.z);
        Ring2.transform.localPosition = new Vector3(-p * 0.1f, Ring2.transform.localPosition.y, Ring2.transform.localPosition.z);

        c = Mathf.Sqrt(a * a + b * b);

        Focus1.transform.localPosition = new Vector3(c * 0.1f, Point.transform.localPosition.y, 0);
        Focus2.transform.localPosition = new Vector3(-c * 0.1f, Point.transform.localPosition.y, 0);

        //PointMovebale.GetComponent<LineRenderer>().SetPosition(0, 100*PointMovebale.transform.position);
        //PointMovebale.GetComponent<LineRenderer>().SetPosition(1, 100 * Focus1.transform.position);

        //PointMovebale.GetComponent<LineRenderer>().SetPosition(2, 100 * PointMovebale.transform.position);
        //PointMovebale.GetComponent<LineRenderer>().SetPosition(3, 100 * Focus2.transform.position);




       pf1 = Vector3.Distance(Focus1.transform.localPosition, PointMovebale.transform.localPosition);
        pf2 = Vector3.Distance(Focus2.transform.localPosition, PointMovebale.transform.localPosition);


        pToF.transform.localPosition = MidPoint(PointMovebale, Focus1);

        pToF.transform.localScale = new Vector3(pf1, 1, 1);

        float s1 = (PointMovebale.transform.localPosition.y - Focus1.transform.localPosition.y) / (PointMovebale.transform.localPosition.x - Focus1.transform.localPosition.x);


        pToF.transform.localEulerAngles = new Vector3(0, 0, Mathf.Atan(s1) * Mathf.Rad2Deg);


        pToF2.transform.localPosition = MidPoint(PointMovebale, Focus2);

        pToF2.transform.localScale = new Vector3(pf2, 1, 1);

        float s2 = (PointMovebale.transform.localPosition.y - Focus2.transform.localPosition.y) / (PointMovebale.transform.localPosition.x - Focus2.transform.localPosition.x);


        pToF2.transform.localEulerAngles = new Vector3(0, 0, Mathf.Atan(s2) * Mathf.Rad2Deg);



        //  print(pf2 - pf1);      

        aLine.transform.localScale = new Vector3(b, 1, 1);
        bLine.transform.localScale = new Vector3(a, 1, 1);

        a2.text = b.ToString("f1") + "<sup>2</sup>";
        b2.text = a.ToString("f1") + "<sup>2</sup>";

        PfvalTxt.text = (pf1*10).ToString("F2");
        Pf2valTxt.text = (pf2 * 10).ToString("F2");
        pfConstant.text = ((pf2 * 10) - (pf1 * 10)).ToString("F2");

        if (FociBtn.gameObject.transform.GetChild(0).gameObject.activeSelf)
        {
            for (int i = 0; i < 360; i++)
            {
                Point.GetComponent<LineRenderer>().SetPosition(i, new Vector3(100*c * Mathf.Cos(i * Mathf.Deg2Rad) * 0.1f, Point.transform.localPosition.y + 100*c * Mathf.Sin(i * Mathf.Deg2Rad) * 0.1f, 0));
            }

            xaxis1.transform.localScale = new Vector3(b, 1, 1);
            xaxis2.transform.localScale = new Vector3(b, 1, 1);

            yaxis1.transform.localScale = new Vector3(a, 1, 1);
            yaxis2.transform.localScale = new Vector3(a, 1, 1);

            yaxis1.transform.localPosition = new Vector3(-b * 0.1f, yaxis1.transform.localPosition.y, yaxis1.transform.localPosition.z);
            yaxis2.transform.localPosition = new Vector3(b * 0.1f, yaxis2.transform.localPosition.y, yaxis2.transform.localPosition.z);

            xaxis1.transform.localPosition = new Vector3(xaxis1.transform.localPosition.x, Point.transform.localPosition.y - a * 0.1f, xaxis1.transform.localPosition.z);
            xaxis2.transform.localPosition = new Vector3(xaxis2.transform.localPosition.x, Point.transform.localPosition.y + a * 0.1f, xaxis2.transform.localPosition.z);

        }


        asemptos1.GetComponent<LineRenderer>().SetPosition(0, new Vector3(-(b / a) * 3 * 0.1f*100, Point.transform.localPosition.y - 3 * 0.1f * 100, 0));
        asemptos1.GetComponent<LineRenderer>().SetPosition(1, new Vector3((b / a) * 3 * 0.1f * 100, Point.transform.localPosition.y+3 * 0.1f * 100,  0));

        asemptos2.GetComponent<LineRenderer>().SetPosition(0, new Vector3(-(b / a) * 3 * 0.1f * 100, Point.transform.localPosition.y+ 3 * 0.1f * 100, 0));
        asemptos2.GetComponent<LineRenderer>().SetPosition(1, new Vector3((b / a) * 3 * 0.1f * 100, Point.transform.localPosition.y - 3 * 0.1f * 100, 0));

        float wy = 3;
        float wx = Mathf.Sqrt((b * b) * (1 + ((wy * wy) / (a * a))));


        Water1.transform.localPosition = new Vector3(wx * 0.1f, Point.transform.localPosition.y + wy * 0.1f, 0);

        wy = 3;
        wx = Mathf.Sqrt((b * b) * (1 + ((wy * wy) / (a * a))));


        Water2.transform.localPosition = new Vector3(-wx * 0.1f, Point.transform.localPosition.y + wy * 0.1f, 0);

        PFDisVal_label.GetComponentInChildren<TextMeshPro>().text = (pf1*10).ToString("F2");
        PF2DisVal_label.GetComponentInChildren<TextMeshPro>().text = (pf2*10).ToString("F2");

        PFDisVal_label.transform.localPosition = MidPoint(Focus1, PointMovebale);
        PF2DisVal_label.transform.localPosition = MidPoint(Focus2, PointMovebale);
    }
    public void EquilateralFun() {

        if (OptionUnlockedTill < 1)
        {
            if (TopicToCheck >= 3)
            {
                if (!equi)
                {
                    EquilateralBtn.gameObject.transform.GetChild(0).gameObject.SetActive(true);
                    equi = true;
                    CancelInvoke("EqualSlidersHigh");
                    CancelInvoke("EqualSlidersLow");
                    if (Slider_b.value > Slider_a.value)
                    {
                        InvokeRepeating("EqualSlidersHigh", 0.01f, 0.0001f);
                    }
                    else
                    {
                        InvokeRepeating("EqualSlidersLow", 0.01f, 0.0001f);
                    }
                    loclChk = true;
                }
                else
                {
                    CancelInvoke("EqualSlidersHigh");
                    CancelInvoke("EqualSlidersLow");
                    equi = false;
                    EquilateralBtn.gameObject.transform.GetChild(0).gameObject.SetActive(false);
                }
            }
            else
            {
                AlertPanel("Wrong parameter chosen", false);
            }
        }
        else
        {
            if (!equi)
            {
                EquilateralBtn.gameObject.transform.GetChild(0).gameObject.SetActive(true);
                equi = true;
                CancelInvoke("EqualSlidersHigh");
                CancelInvoke("EqualSlidersLow");
                if (Slider_b.value > Slider_a.value)
                {
                    InvokeRepeating("EqualSlidersHigh", 0.01f, 0.0001f);
                }
                else
                {
                    InvokeRepeating("EqualSlidersLow", 0.01f, 0.0001f);
                }
                loclChk = true;
            }
            else
            {
                CancelInvoke("EqualSlidersHigh");
                CancelInvoke("EqualSlidersLow");
                equi = false;
                EquilateralBtn.gameObject.transform.GetChild(0).gameObject.SetActive(false);
            }
        }

    }
    public void FociFun()
    {
        if (OptionUnlockedTill < 1)
        {
            if (TopicToCheck >= 3)
            {
                if (FociBtn.gameObject.transform.GetChild(0).gameObject.activeSelf == false)
                {
                    FociBtn.gameObject.transform.GetChild(0).gameObject.SetActive(true);
                    FociRelated.SetActive(true);
                    Calculation();
                }
                else
                {
                    FociBtn.gameObject.transform.GetChild(0).gameObject.SetActive(false);
                    FociRelated.SetActive(false);
                }
            }
            else
            {
                AlertPanel("Wrong parameter chosen", false);

            }
        }
        else
        {
            if (FociBtn.gameObject.transform.GetChild(0).gameObject.activeSelf == false)
            {
                FociBtn.gameObject.transform.GetChild(0).gameObject.SetActive(true);
                FociRelated.SetActive(true);
                Calculation();
            }
            else
            {
                FociBtn.gameObject.transform.GetChild(0).gameObject.SetActive(false);
                FociRelated.SetActive(false);
            }

        }
    }
    public void AsymptFun() {
        if (OptionUnlockedTill <1) {
            if (TopicToCheck == 3)
            {
                AddXpEffect();

                if (AssympBtn.gameObject.transform.GetChild(0).gameObject.activeSelf == false)
                {
                    AssympBtn.gameObject.transform.GetChild(0).gameObject.SetActive(true);
                    AsymRelated.SetActive(true);
                    Calculation();
                }
                else
                {
                    AssympBtn.gameObject.transform.GetChild(0).gameObject.SetActive(false);
                    AsymRelated.SetActive(false);
                }
            }
            else {
                AlertPanel("Wrong parameter chosen", false);
            }
        }
        else {
            if (AssympBtn.gameObject.transform.GetChild(0).gameObject.activeSelf == false)
            {
                AssympBtn.gameObject.transform.GetChild(0).gameObject.SetActive(true);
                AsymRelated.SetActive(true);
                Calculation();
            }
            else
            {
                AssympBtn.gameObject.transform.GetChild(0).gameObject.SetActive(false);
                AsymRelated.SetActive(false);
            }
        }
    }
    public void EqualSlidersHigh() {
        if (equi)
        {
            if (Slider_b.GetComponentInChildren<Text>().text != Slider_a.GetComponentInChildren<Text>().text)
            {
                Slider_b.value = Slider_b.value - 0.01f;
            }
            else
            {
                loclChk = false;
                CancelInvoke("EqualSliders");
            }
           
        }
    }
    public void EqualSlidersLow() {
         if (equi)
         {

            if (Slider_b.GetComponentInChildren<Text>().text != Slider_a.GetComponentInChildren<Text>().text)
            {
                Slider_b.value = Slider_b.value + 0.01f;
            }
            else
            {
                loclChk = false;
                CancelInvoke("EqualSliders");
            }
         }
    }
}
