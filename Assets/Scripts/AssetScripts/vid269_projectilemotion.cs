﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;
public class vid269_projectilemotion : MonoBehaviour
{
    public TextAsset CSVFile;
    // TrajectoryPoint and Ball will be instantiated
    public GameObject Range_Final, Ground, Shadow;
    public GameObject Projectile_Barrel, Projectile_Pivot, BallPrefb, Effects, Wheels, Pannel;

    public GameObject ball, ball1;
    public Vector3 BallInitPos, screenPoint, OffSet;
    private bool isPressed, isBallThrown, IsAR;
    public float Init_Velocity = 10, RotationAngle, Time, SliderTime, Gravity, dx, dy, Tx, Range, Distance, TimeOfFlight, Range_Display, CanMovSliderVal, CanRotSliderVal, CanVelSliderVal;
    public Slider BarrelSlider, VelocitySlider, CannonMovmentSlider;
    public Button BallThrow;
    public Text Velocity_Val, Angle_Val, Range_Val, TimeOfFlight_Val;
    public orbit CamOrbit;
    public AROrbitControls ArOrbit;
    public LineRenderer TR_CanonRange;
    public ParticleSystem ParticleHit;
    public Dictionary<string, string> DescriTextDoc;
    public Material TransparentMaterial;
    public Transform CamTarget;
    public static vid269_projectilemotion Instance;
    public AssetBundle assetBundle;

    public int OptionUnlockedTill, CurrentTopicSelected, TopicToCheck;
    public float Sub_XP_Earned, Total_XP;
    public Dropdown ToDoList_DD;
    public GameObject InstructionPanel_DD, ObjectivePanel, InstructionPanel, InfoPanel, GlassPanel, Canvas_BG, SidePannel, Togg_Bubble;
    public Button Info_Close_Btn, Info_Cntue_Btn, Info_Open_Btn;
    public List<Dictionary<string, object>> CSV_data;
    public List<String> Sub_Top_ToDoList, Sub_Top_Desc, Sub_Top_XP, LearningGoals;
    public Text XP_ToShow, XP_Effect, Main_XP_ToShow, Learninggoal_Txt, Instruction_Txt, Assumption_Txt, Heading_Txt, IntensityValue_Txt;
    public bool IsTopicUnlocked;
    public Canvas UIcanvas;
    public Button SidePannelOpen, LeaveZekki_But;
    public bool IsClicked,IsUIZooming;
    
    //---------------------------------------    
    void Start()
    {
        //OptionUnlockedTill = 1;

        Instance = this;


        LoadFromAsssetBundle();

        ReadingDataFromCSVFile();

        ReadingXps();

        FindingGameObjects();

        AssignClickEventsToObjects();

        
    }

    public void FindingGameObjects()
    {
        Projectile_Barrel = GameObject.Find("phy_projectile_canon_V03");
        //BallPrefb = Resources.Load("Ball") as GameObject;

        Wheels = GameObject.Find("Cylin1_lp");

        Projectile_Pivot = GameObject.Find("ProjectilePivot");

        Range_Final = GameObject.Find("RangeLine");
        //Range_Final.SetActive(false);

        BarrelSlider = GameObject.Find("Barrel_Rot_Slider").GetComponent<Slider>();
        VelocitySlider = GameObject.Find("Velocity_Slider").GetComponent<Slider>();
        CannonMovmentSlider = GameObject.Find("CanonPos_Slider").GetComponent<Slider>();

        BallThrow = GameObject.Find("BallThrow").GetComponent<Button>();
        
        Effects = Projectile_Barrel.transform.GetChild(0).Find("Effects").gameObject;
        Pannel = GameObject.Find("SidePanel");

        Velocity_Val = GameObject.Find("Velocity_DisplayText").GetComponent<Text>();
        Angle_Val = GameObject.Find("Angle_DisplayText").GetComponent<Text>();
        Range_Val = GameObject.Find("Range_DisplayText").GetComponent<Text>();
        TimeOfFlight_Val = GameObject.Find("TimeOfFlight_DisplayText").GetComponent<Text>();
        TR_CanonRange = GameObject.Find("TR_Range").GetComponent<LineRenderer>();
        ParticleHit = GameObject.Find("WFXMR_BImpact Concrete").GetComponent<ParticleSystem>();

        Shadow = GameObject.Find("Shadow");
        Ground = GameObject.Find("Ground");


        GameObject SmoothLook = GameObject.Find("SmoothLook");

        if (SmoothLook != null)
            SmoothLook.AddComponent<SmoothLook>();

        isPressed = isBallThrown = false;

        Gravity = 9.81f;
        Init_Velocity = 1.25f;
        RotationAngle = 0.1f;

        

        if (IsAR)
            Ground.SetActive(false);


        SidePannelOpen = GameObject.Find("PannelOpenButton").GetComponent<Button>();
        SidePannelOpen.gameObject.SetActive(false);
        SidePannel = GameObject.Find("SidePanel");

        InstructionPanel_DD = GameObject.Find("DD_Instruction");
        InstructionPanel = GameObject.Find("InstructionPanel");
        ToDoList_DD = GameObject.Find("ToDoList_Dropdown").GetComponent<Dropdown>();
        XP_ToShow = GameObject.Find("Gained_XP_Value").GetComponent<Text>();
        XP_Effect = GameObject.Find("Effect_Text").GetComponent<Text>();
        Main_XP_ToShow = GameObject.Find("Main_XP_Value").GetComponent<Text>();
        ObjectivePanel = GameObject.Find("ObjectivePanel");
        InstructionPanel.transform.localScale = Vector3.zero;
        InfoPanel = GameObject.Find("InformationPanel");
        Info_Close_Btn = GameObject.Find("InfoCloseBtn").GetComponent<Button>();
        Info_Cntue_Btn = GameObject.Find("InfoContinueBtn").GetComponent<Button>();
        Info_Open_Btn = GameObject.Find("InfoButton").GetComponent<Button>();
        Info_Close_Btn.gameObject.SetActive(false);
        GlassPanel = GameObject.Find("GlassEffect");
        Learninggoal_Txt = GameObject.Find("LGListText").GetComponent<Text>();
        Instruction_Txt = GameObject.Find("IListText").GetComponent<Text>();
        Assumption_Txt = GameObject.Find("AListText").GetComponent<Text>();
        Heading_Txt = GameObject.Find("InfoHeadingText").GetComponent<Text>();
        UIcanvas = GameObject.Find("Canvas_Projectile").GetComponent<Canvas>();
        

        if (OptionUnlockedTill == 0)
        {
            /*ToDoList_DD.GetComponent<RectTransform>().anchoredPosition = new Vector3(150f, -150f, 0);
            XP_ToShow.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(150f, -150f, 0);
            SidePannel.GetComponent<RectTransform>().anchoredPosition = new Vector3(-600f, 0f, 0);*/
            ObjectivePanel.transform.localScale = new Vector3(1, 1, 1);
            IsTopicUnlocked = false;
        }
        else
        {
            ToDoList_DD.GetComponent<RectTransform>().anchoredPosition = new Vector3(-250f, -150f, 0);
            XP_ToShow.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(-250f, -150f, 0);
            ObjectivePanel.transform.localScale = new Vector3(1, 0, 1);
            IsTopicUnlocked = true;
        }
        Main_XP_ToShow.text = totXp.ToString();

        XP_ToShow.text = "0/" + Total_XP;



        for (int i = 0; i < Sub_Top_ToDoList.Count; i++)
        {

            ToDoList_DD.options.Add(new Dropdown.OptionData() { text = Sub_Top_ToDoList[i] });


            //List<float> TotalXP = Sub_Top_XP.Select(s => float.Parse(s)).ToList();

            //Total_XP += TotalXP[i];

            //XP_ToShow.text = "0/" + Total_XP;


            ObjectivePanel.transform.Find("ObjectiveText").GetComponent<Text>().text = Sub_Top_ToDoList[TopicToCheck];




        }


        GlassPanel.transform.localScale = Vector3.zero;
        InfoPanel.transform.localScale = Vector3.zero;
        Invoke("OpenInfoPanel", 1f);

        Heading_Txt.text = LearningGoals[0];
        Learninggoal_Txt.text = LearningGoals[1];
        //Instruction_Txt.text = LearningGoals[2];
        Assumption_Txt.text = LearningGoals[3];



        OpenSidePannel();
        //ChangeRotationOfBarrel(RotationAngle);
    }


    public void AssignClickEventsToObjects()
    {
        BarrelSlider.onValueChanged.AddListener(delegate { ChangeRotationOfBarrel(BarrelSlider.value); });
        VelocitySlider.onValueChanged.AddListener(delegate { ChangeVelocity(VelocitySlider.value); });
        CannonMovmentSlider.onValueChanged.AddListener(delegate { ChangeCannonPosition(CannonMovmentSlider.value); });

        BallThrow.onClick.AddListener(() => ThrowBall());
        
        AddListenerToEvents(EventTriggerType.PointerDown, DisableOrbit, BarrelSlider.gameObject);
        AddListenerToEvents(EventTriggerType.PointerDown, DisableOrbit, VelocitySlider.gameObject);
        AddListenerToEvents(EventTriggerType.PointerDown, DisableOrbit, CannonMovmentSlider.gameObject);
        AddListenerToEvents(EventTriggerType.PointerDown, DisableOrbit, Range_Final.gameObject);

        AddListenerToEvents(EventTriggerType.PointerUp, EnableOrbit, BarrelSlider.gameObject);
        AddListenerToEvents(EventTriggerType.PointerUp, EnableOrbit, VelocitySlider.gameObject);
        AddListenerToEvents(EventTriggerType.PointerUp, EnableOrbit, CannonMovmentSlider.gameObject);
        AddListenerToEvents(EventTriggerType.PointerUp, EnableOrbit, Range_Final.gameObject);


        AddListenerToEvents(EventTriggerType.PointerDown, DestroyBall, BarrelSlider.gameObject);
        AddListenerToEvents(EventTriggerType.PointerDown, DestroyBall, VelocitySlider.gameObject);

        AddListenerToEvents(EventTriggerType.PointerDown, OnDragObjClick, Range_Final.gameObject);


        AddListenerToEvents(EventTriggerType.Drag, DragTargetObj, Range_Final.gameObject);


        //BarrelSlider.gameObject.GetComponent<EventTrigger>().triggers.Add(BallPointerUp);

        SidePannelOpen.onClick.AddListener(OpenSidePannel);

        ToDoList_DD.GetComponent<EventTrigger>().triggers.Clear();

        AddListenerToEvents(EventTriggerType.PointerClick, ExploringToDOList_DD, ToDoList_DD.gameObject);
        AddListenerToEvents(EventTriggerType.Select, ToDoList_DDCancel, ToDoList_DD.gameObject);


        Info_Close_Btn.onClick.AddListener(() => CloseInfoPanel(Info_Close_Btn));
        Info_Cntue_Btn.onClick.AddListener(() => CloseInfoPanel(Info_Cntue_Btn));
        Info_Open_Btn.onClick.AddListener(OpenInfoPanel);

        AddListenerToEvents(EventTriggerType.PointerDown, SliderCondMouseDown, CannonMovmentSlider.gameObject, CannonMovmentSlider.gameObject);
        AddListenerToEvents(EventTriggerType.PointerDown, SliderCondMouseDown, VelocitySlider.gameObject, VelocitySlider.gameObject);
        AddListenerToEvents(EventTriggerType.PointerDown, SliderCondMouseDown, BarrelSlider.gameObject, BarrelSlider.gameObject);

        AddListenerToEvents(EventTriggerType.PointerUp, SliderConditionCheck, CannonMovmentSlider.gameObject);
        AddListenerToEvents(EventTriggerType.PointerUp, SliderConditionCheck, VelocitySlider.gameObject);
        AddListenerToEvents(EventTriggerType.PointerUp, SliderConditionCheck, BarrelSlider.gameObject);
    }

    public void AddListenerToEvents(EventTriggerType eventType, Action MethodToCall, GameObject TriggerObjToAdd)
    {

        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall());
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }

    public void AddListenerToEvents(EventTriggerType eventType, Action<float> MethodToCall, GameObject TriggerObjToAdd, float p)
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall(p));
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }

    public void AddListenerToEvents(EventTriggerType eventType, Action<GameObject> MethodToCall, GameObject TriggerObjToAdd, GameObject p)
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall(p));
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }


    public void ReadingDataFromCSVFile()
    {
        Sub_Top_Desc = new List<string>();
        Sub_Top_ToDoList = new List<string>();
        Sub_Top_XP = new List<string>();
        LearningGoals = new List<string>();

        CSV_data = CSVReader.Read(CSVFile);


        for (var i = 0; i < CSV_data.Count; i++)
        {
            //print("Topics " + CSV_data[i]["Learninggoals"] + ".......... " +"ToDo " + CSV_data[i]["Topic_TodoList"]);




            if (CSV_data[i]["Topic_ToDoList"].ToString() != "" && CSV_data[i]["Topic_ToDoList"].ToString() != null)
            {
                Sub_Top_ToDoList.Add(CSV_data[i]["Topic_ToDoList"].ToString());

            }



            //if (CSV_data[i]["Top_XP"].ToString() != "" && CSV_data[i]["Top_XP"].ToString() != null)
            //{
            //    Sub_Top_XP.Add(CSV_data[i]["Top_XP"].ToString());

            //}

            if (CSV_data[i]["Learninggoals"].ToString() != "" && CSV_data[i]["Learninggoals"].ToString() != null)
            {
                LearningGoals.Add(CSV_data[i]["Learninggoals"].ToString());

            }

        }






    }

    public int totXp, loclXp, PerTopic_XP;

    public void ReadingXps()
    {

        //PlayerPrefs.SetInt("TotalXP", 34);
        //PlayerPrefs.SetInt("MaxEarnings", 39);

        totXp = PlayerPrefs.GetInt("TotalXP");
        loclXp = 0;// PlayerPrefs.GetInt("MaxEarnings");

        TopicToCheck = 0;

        if (loclXp != 0)
        {
            OptionUnlockedTill = 0;

        }
        else
        {
            OptionUnlockedTill = 1;

        }


       

        int t = 0;
        int p = 0;

        for (int k = 0; k < Sub_Top_ToDoList.Count; k++)
        {

            p = loclXp / Sub_Top_ToDoList.Count;
            Sub_Top_XP.Add(p.ToString());
            t = t + p;
        }

        if (t != loclXp)
        {
            Sub_Top_XP[Sub_Top_ToDoList.Count - 1] = (p + (loclXp - t)).ToString();
        }

        Total_XP = loclXp;
        

    }



    public float result;

    public void GetResult()
    {
        result = ((float)(TopicToCheck - 0) / (float)(Sub_Top_ToDoList.Count)) * 100;
        Camera.main.SendMessage("UnloadActually", result);
    }

    public void LoadFromAsssetBundle()
    {
        //if (Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.Android)
        //{
        assetBundle = AssetBundle.LoadFromFile(PlayerPrefs.GetString("AssetBundleUrl"));

        CSVFile = assetBundle.LoadAsset(PlayerPrefs.GetString("LibraryName") + "CSV", typeof(TextAsset)) as TextAsset;

        TransparentMaterial = assetBundle.LoadAsset("TransparentMaterial", typeof(Material)) as Material;
        TransparentMaterial.shader = Shader.Find("Standard");




        //}
        //else
        //{

        //string CSVName = transform.parent.gameObject.name;



        //if (CSVName.Contains("Clone"))
        //{



        //    CSVName = CSVName.Substring(0, CSVName.Length - 7);
        //}



        //print("casvname........." + CSVName);



        //CSVFile = Resources.Load(CSVName + "CSV") as TextAsset;
        //}







        Camera.main.gameObject.AddComponent<PhysicsRaycaster>();



        Camera.main.backgroundColor = Color.white;



        GameObject TargetForCamera = GameObject.Find("Target");



        if (TargetForCamera != null)
        {
            TargetForCamera.AddComponent<orbit>();
            CamOrbit = TargetForCamera.GetComponent<orbit>();
            CamTarget = TargetForCamera.transform;

            CamOrbit.zoomMax = 3f;
            CamOrbit.zoomMin = 1f;
            CamOrbit.zoomStart = 2f;
            CamOrbit.distance = CamOrbit.zoomStart;
            CamOrbit.transform.position = new Vector3(0, 0.25f, 0);

            CamOrbit.maxRotUp = 45;
            CamOrbit.minRotUp = 1;

            CamOrbit.cameraRotUpStart = 22;

            CamOrbit.maxSideRot = 90;
            CamOrbit.minSideRot = 90;


            IsAR = false;
        }
        else
        {
            this.transform.parent.gameObject.AddComponent<AROrbitControls>();
            ArOrbit = this.transform.parent.gameObject.GetComponent<AROrbitControls>();
            IsAR = true;
        }



    }

    
    public void OnDragObjClick()
    {
        screenPoint = Camera.main.WorldToScreenPoint(Range_Final.transform.localPosition);
        OffSet = Range_Final.transform.localPosition - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Range_Final.transform.localPosition.y, screenPoint.z));

    }

    public void DragTargetObj()
    {
        //print("indrag......."+ Range_Final.transform.position+"............local..........."+ Range_Final.transform.localPosition);
        Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Range_Final.transform.localPosition.y, screenPoint.z);
        Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint) + OffSet;
        float x = curPosition.x;
        x = Mathf.Clamp(x, Projectile_Pivot.transform.localPosition.x, 100);
        Range_Final.transform.localPosition = new Vector3(x, curPosition.y, Range_Final.transform.localPosition.z);




        if (ball)
        {
            Range_Final.transform.GetComponentInChildren<TextMeshPro>().text = ((x - Projectile_Pivot.transform.localPosition.x) * Distance / (Range - Projectile_Pivot.transform.localPosition.x)).ToString("0.00") + " m";
            //TR_CanonRange.SetPosition(0,new Vector3(BarrelSlider.transform.localPosition.x, 0, -0.05F));
            TR_CanonRange.SetPosition(1, new Vector3(Range_Final.transform.localPosition.x - TR_CanonRange.transform.localPosition.x, 0, 0));
        }

        //float distance_to_screen = Camera.main.WorldToScreenPoint(Range_Final.transform.position).z;
        //Range_Final.transform.position = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, 0.0088f, distance_to_screen));

    }

    public void ChangeRotationOfBarrel(float SliderVal)
    {
        
        Projectile_Pivot.transform.parent = Projectile_Barrel.transform.GetChild(0);

        if (IsInvoking("BallMovement"))
        {
            CancelInvoke("BallMovement");
            Time = 0;

        }
        //CanRotSliderVal = SliderVal;

        RotationAngle = 80f * SliderVal;
        RotationAngle = Mathf.Round(RotationAngle);
        Projectile_Barrel.transform.GetChild(0).transform.localEulerAngles = new Vector3(0, 0, RotationAngle);



        float Horz_Distance = Init_Velocity * Init_Velocity * Mathf.Sin(2 * RotationAngle * Mathf.Deg2Rad) / Gravity;
        //TrajectoryPointPrefeb.transform.localPosition = new Vector3(Horz_Distance + Projectile_Pivot.transform.position.x, 0.028f, 0);
        //TrajectoryPointPrefeb.SetActive(true);

        //BarrelSlider.GetComponentInChildren<Text>().text = (-RotationAngle).ToString("00");
        Angle_Val.text = (RotationAngle).ToString("00");


        float DottedLine_Scale = RotationAngle * 0.003625f + 0.1f;
        float DottedLine_Pos = RotationAngle * 0.0018f + 0.054f;

        TR_CanonRange.transform.GetChild(0).localPosition = new Vector3(TR_CanonRange.transform.GetChild(0).localPosition.x, DottedLine_Pos, TR_CanonRange.transform.GetChild(0).localPosition.z);
        TR_CanonRange.transform.GetChild(0).localScale = new Vector3(TR_CanonRange.transform.GetChild(0).localScale.x, DottedLine_Scale, TR_CanonRange.transform.GetChild(0).localScale.z);

        float ShadowScale = RotationAngle * -0.007f + 1.056f;
        Shadow.transform.localScale = new Vector3(ShadowScale, Shadow.transform.localScale.y, Shadow.transform.localScale.z);

        if (ball)
            DestroyBall();

        //BallFinalPos();
    }

    public void ChangeVelocity(float SliderVal)
    {
       

        if (IsInvoking("BallMovement"))
        {
            CancelInvoke("BallMovement");
            Time = 0;

        }
        

        Init_Velocity = SliderVal;
        //CanVelSliderVal = SliderVal;
        Init_Velocity = Init_Velocity / 4;
        //VelocitySlider.GetComponentInChildren<Text>().text = SliderVal.ToString("0.0");
        Velocity_Val.text = SliderVal.ToString("0.0");



        if (ball)
            DestroyBall();
    }

    public void ChangeCannonPosition(float SliderVal)
    {
        
        Projectile_Pivot.transform.parent = Projectile_Barrel.transform.GetChild(0);

        if (IsInvoking("BallMovement"))
        {
            CancelInvoke("BallMovement");
            Time = 0;

        }
        //CanMovSliderVal = SliderVal;
        float x_pos = 1.25f * SliderVal - 0.75f;
        

        Projectile_Barrel.transform.localPosition = new Vector3(x_pos, Projectile_Barrel.transform.localPosition.y, Projectile_Barrel.transform.localPosition.z);

        Wheels.transform.eulerAngles = new Vector3(Wheels.transform.eulerAngles.x, Wheels.transform.eulerAngles.y, -718 * SliderVal + 1);



        if (ball)
            DestroyBall();

    }

    public void SliderCondMouseDown(GameObject Slider)
    {
        if (!IsTopicUnlocked)
        {
            if (Slider == CannonMovmentSlider.gameObject)
            {
                CanMovSliderVal = CannonMovmentSlider.value;
            }
            if (Slider == VelocitySlider.gameObject)
            {
                CanVelSliderVal = VelocitySlider.value;
            }
            if (Slider == BarrelSlider.gameObject)
            {
                CanRotSliderVal = BarrelSlider.value;
            }
        }
    }

    public void SliderConditionCheck()
    {
        if (!IsTopicUnlocked)
        {
            
                if (TopicToCheck == 0 )
                {
                    if (CheckFor_XP_Condition(TopicToCheck))
                    {
                        VelocitySlider.value = CanVelSliderVal;
                        BarrelSlider.value = CanRotSliderVal;
                        return;
                    }
                }
            if (TopicToCheck == 1)
            {
                if (CheckFor_XP_Condition(TopicToCheck))
                {
                    CannonMovmentSlider.value = CanMovSliderVal;
                    VelocitySlider.value = CanVelSliderVal;
                    return;
                }
            }
            if (TopicToCheck == 2)
            {
                if (CheckFor_XP_Condition(TopicToCheck))
                {
                    CannonMovmentSlider.value = CanMovSliderVal;
                    
                    BarrelSlider.value = CanRotSliderVal;
                    return;
                }
            }
            if (TopicToCheck == 3)
            {
                if (CheckFor_XP_Condition(TopicToCheck))
                {
                    CannonMovmentSlider.value = CanMovSliderVal;
                    VelocitySlider.value = CanVelSliderVal;
                    BarrelSlider.value = CanRotSliderVal;
                    return;
                }
            }
            CanRotSliderVal = BarrelSlider.value;
            CanVelSliderVal = VelocitySlider.value;
            CanMovSliderVal = CannonMovmentSlider.value;
        }

        

    }

    public void ThrowBall()
    {
        if (!IsTopicUnlocked)
        {
            if (TopicToCheck == 0 || TopicToCheck == 1 || TopicToCheck == 2)
            {
                if (CheckFor_XP_Condition(TopicToCheck))
                    return;
            }
            else
            {
                Effects.SetActive(true);
                CheckFor_XP_Condition(TopicToCheck);
            }
        }

        if (ball)
        {
            DestroyBall();
            CancelInvoke("BallMovement");
        }

        createBall();

        Effects.SetActive(true);
        //ball1.SetActive(true);

        Projectile_Pivot.transform.parent = Projectile_Barrel.transform.parent;


        ParticleSystem ps = ball.transform.GetChild(0).GetComponent<ParticleSystem>();
        var main = ps.main;
        //print("ps........"+ps.name);
        main.customSimulationSpace = this.transform;

        TR_CanonRange.transform.GetChild(0).gameObject.SetActive(true);

        BallHitGround();
        ValuesToDisplay();

        /*Vector3 velocity= Projectile_Pivot.transform.forward * Init_Velocity;
       
       ball.GetComponent<Rigidbody>().useGravity = true;
       ball.GetComponent<Rigidbody>().AddForce(velocity, ForceMode.Impulse);*/

        InvokeRepeating("BallMovement", 0.1f, 0.005f);
    }

    public void BallFinalPos()
    {




        for (float i = 0.04f; i < 1; i += 0.001f)
        {
            dx = Init_Velocity * Mathf.Cos(RotationAngle * Mathf.Deg2Rad) * i;
            dy = Init_Velocity * Mathf.Sin(RotationAngle * Mathf.Deg2Rad) * i - 0.5f * Gravity * i * i;
            Tx = i;
            if ((Projectile_Pivot.transform.position.y + (dy / 4)) < (0.028f / 4))
                break;


        }

        dx = dx / 4;
        dy = dy / 4;

        //print("Tx......" + Tx + ".....dist...." + Tx * Init_Velocity);

        //Range_Final.transform.localPosition = new Vector3(Projectile_Pivot.transform.position.x + dx, Projectile_Pivot.transform.position.y + dy, 0);

    }

    public void BallMovement()
    {
        Time += 0.001f;



        dx = Init_Velocity * Mathf.Cos(RotationAngle * Mathf.Deg2Rad) * Time;
        dy = (Init_Velocity * Mathf.Sin(RotationAngle * Mathf.Deg2Rad) * Time) - (0.5f * Gravity * Time * Time);

        //dx = dx;
        //dy = dy;

        float Diff_Y = Vector3.Distance(ball.transform.localPosition, Range_Final.transform.localPosition);// (dy) - (BarrelSlider.value * 0.015f)-((Init_Velocity*0.00093f)-0.014f);

        //print("dist.........."+Diff_Y);
        if (Diff_Y < 0.01f)
        {
            CancelInvoke("BallMovement");

            ParticleHit.transform.position = Range_Final.transform.position;
            if (Distance < 2)
                ParticleHit.gameObject.transform.localScale = new Vector3(0.15f, 0.15f, 0.15f);
            else
                ParticleHit.gameObject.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);

            ParticleHit.Play();

            Invoke("ResetCamPosition", 2f);

            //ball.transform.localPosition = new Vector3(BallInitPos.x + dx,0.025f, ball.transform.localPosition.z);

            //return;
        }

        //print("dx......"+dx+"........dy......"+dy+"....bally........."+ Diff_Y);
        ball.transform.localPosition = new Vector3(Projectile_Pivot.transform.localPosition.x + dx, Projectile_Pivot.transform.localPosition.y + dy, ball.transform.localPosition.z);



    }

    public void ResetCamPosition()
    {
        if (!IsAR && ball)
        {
            iTween.MoveTo(CamTarget.gameObject,iTween.Hash("x", (Projectile_Pivot.transform.localPosition.x + ball.transform.localPosition.x) / 2,"y",0.25f,"time",0.5f,"easetype",iTween.EaseType.linear));
            //CamTarget.transform.localPosition = new Vector3((Projectile_Pivot.transform.localPosition.x + ball.transform.localPosition.x) / 2, CamTarget.transform.localPosition.y, CamTarget.transform.localPosition.z);
            CamOrbit.target = CamTarget;
        }
    }

    public void BallHitGround()
    {
        Tx = (((Mathf.Sqrt((Init_Velocity * Init_Velocity * Mathf.Sin(RotationAngle * Mathf.Deg2Rad) * Mathf.Sin(RotationAngle * Mathf.Deg2Rad)) + 2 * Gravity * Projectile_Pivot.transform.localPosition.y)) - (Init_Velocity * Mathf.Sin(RotationAngle * Mathf.Deg2Rad))) / Gravity);

        Range = (((Init_Velocity * Init_Velocity * Mathf.Sin(2 * RotationAngle * Mathf.Deg2Rad)) / Gravity) + (Init_Velocity * Mathf.Cos(RotationAngle * Mathf.Deg2Rad) * Tx));

        //print("sin......"+ ((Init_Velocity * Init_Velocity * Mathf.Sin(2 * -RotationAngle * Mathf.Deg2Rad)) / Gravity) + ".......cos......" + (Init_Velocity * Mathf.Cos(-RotationAngle * Mathf.Deg2Rad) * Tx));
        //Range = (Init_Velocity * Init_Velocity * Mathf.Sin(2 * -RotationAngle * Mathf.Deg2Rad) / Gravity);

        Range = (Range + Projectile_Pivot.transform.localPosition.x);

        //Tx_Val.text=(Tx).ToString("0.00");


        Range_Final.transform.localPosition = new Vector3(Range, Range_Final.transform.localPosition.y, Projectile_Pivot.transform.localPosition.z);

        TR_CanonRange.transform.localPosition = new Vector3(Projectile_Pivot.transform.localPosition.x, -0.025f, Projectile_Pivot.transform.localPosition.z);
        TR_CanonRange.enabled = true;
        TR_CanonRange.SetPositions(new Vector3[2] { Vector3.zero, Vector3.zero });
        TR_CanonRange.SetPosition(1, new Vector3(Range_Final.transform.localPosition.x - TR_CanonRange.transform.localPosition.x, 0, 0));

        //print("InvokeCancelled" +Tx+"..........distance........"+(Range_Final.transform.position.x-ball.transform.position.x));
        //Distance = Vector3.Distance(new Vector3(Projectile_Pivot.transform.position.x, 0, 0), new Vector3(Range_Final.transform.position.x, 0, 0));

        //Range_Final.transform.GetComponentInChildren<TextMeshPro>().text=Distance.ToString("0.00");

        //Range_Val.text = Distance.ToString("0.00");

        //TimeOfFlight = (2*Init_Velocity* Mathf.Sin(-RotationAngle * Mathf.Deg2Rad)/Gravity);


        //TimeOfFlight_Val.text = (Tx+TimeOfFlight).ToString("0.00");
    }

    public void ValuesToDisplay()
    {
        float Velocity_Display = Init_Velocity * 4;


        Range_Display = ((Velocity_Display * Velocity_Display * Mathf.Sin(2 * RotationAngle * Mathf.Deg2Rad)) / Gravity);

        Distance = Vector3.Distance(new Vector3(Projectile_Pivot.transform.localPosition.x, 0, 0), new Vector3(Range_Display, 0, 0));


        Distance = (float)((int)(Range_Display * 100)) / 100;

        Range_Final.transform.GetComponentInChildren<TextMeshPro>().text = Distance.ToString() + " m";

        Range_Val.text = Distance.ToString();

        TimeOfFlight = (2 * Velocity_Display * Mathf.Sin(RotationAngle * Mathf.Deg2Rad) / Gravity);

        TimeOfFlight = (float)((int)(TimeOfFlight * 1000)) / 1000;

        TimeOfFlight_Val.text = (TimeOfFlight).ToString();
    }


    public void EnableOrbit()
    {

        if (!IsAR)
        {
            CamOrbit.DisableInteration = false;
            //print("EnableOrbit");
        }
        else
        {
            ArOrbit.DisableInteration = false;
        }
    }

    public void DisableOrbit()
    {

        if (!IsAR)
        {
            CamOrbit.DisableInteration = true;
            //print("DisableOrbit");
        }
        else
        {
            ArOrbit.DisableInteration = true;
        }
    }

    //---------------------------------------    
    void Update()
    {

        // when mouse button is pressed, cannon is rotated as per mouse movement and projectile trajectory path is displayed.

    }
    //---------------------------------------    
    // Following method creates new ball
    //---------------------------------------    
    private void createBall()
    {
        ball = (GameObject)Instantiate(BallPrefb, Projectile_Pivot.transform.position, Projectile_Pivot.transform.rotation, this.transform);
        BallInitPos = ball.transform.localPosition;

        if (!IsAR)
            CamOrbit.target = ball.transform;
    }

    public void DestroyBall()
    {
        if (ball)
        {
            if (!IsAR)
                CamOrbit.target = Projectile_Pivot.transform;
            //iTween.MoveTo(CamOrbit.target.gameObject,iTween.Hash("x",Projectile_Pivot.transform.localPosition.x,"y", Projectile_Pivot.transform.localPosition.y,"time",0.2f,"easetype",iTween.EaseType.easeInOutSine));

            CancelInvoke("BallMovement");

            Destroy(ball);
            Effects.SetActive(false);

            TR_CanonRange.enabled = false;
            TR_CanonRange.SetPositions(new Vector3[2] { Vector3.zero, Vector3.zero });

            TR_CanonRange.transform.GetChild(0).gameObject.SetActive(false);
        }

        Time = 0;
        dx = 0;
        dy = 0;
    }

    public bool CheckFor_XP_Condition(int Current_Todo)
    {
        bool Condition = true;
        if (Current_Todo == 0)
        {
            if (CannonMovmentSlider.value != 0.35f)
            {
                Collect_XP(Projectile_Barrel);
                Condition = false;
            }
            else
            {
                ShowInstructionPanel("Wrong parameter chosen");
                //iTween.ScaleTo(GlassPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 0.01f, "easetype", iTween.EaseType.spring));
                //InstructionPanel.transform.GetChild(0).GetComponent<Text>().text = "Wrong parameter choosen";
                //iTween.ScaleTo(InstructionPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 1f, "easetype", iTween.EaseType.spring, "oncomplete", "CloseInstructionPanel", "oncompletetarget", this.gameObject));
                //ObjectivePanel.transform.localScale = new Vector3(1, 0, 1);
                Condition = true;
            }
        }
        if (Current_Todo == 1)
        {
            if (BarrelSlider.value > 0.63f)
            {
                Collect_XP(Projectile_Barrel);
                Condition = false;
            }
            else
            {
                ShowInstructionPanel("Wrong parameter chosen");
                //iTween.ScaleTo(GlassPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 0.01f, "easetype", iTween.EaseType.spring));
                //InstructionPanel.transform.GetChild(0).GetComponent<Text>().text = "Wrong parameter choosen";
                //iTween.ScaleTo(InstructionPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 1f, "easetype", iTween.EaseType.spring, "oncomplete", "CloseInstructionPanel", "oncompletetarget", this.gameObject));
                //ObjectivePanel.transform.localScale = new Vector3(1, 0, 1);
                Condition = true;
            }
        }
        if (Current_Todo == 2)
        {
            if (VelocitySlider.value > 10f)
            {
                Collect_XP(Projectile_Barrel);
                Condition = false;
            }
            else
            {
                ShowInstructionPanel("Wrong parameter chosen");
                //iTween.ScaleTo(GlassPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 0.01f, "easetype", iTween.EaseType.spring));
                //InstructionPanel.transform.GetChild(0).GetComponent<Text>().text = "Wrong parameter choosen";
                //iTween.ScaleTo(InstructionPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 1f, "easetype", iTween.EaseType.spring, "oncomplete", "CloseInstructionPanel", "oncompletetarget", this.gameObject));
                //ObjectivePanel.transform.localScale = new Vector3(1, 0, 1);
                Condition = true;
            }
        }
        if (Current_Todo == 3)
        {
            if (Effects.activeInHierarchy)
            {
                Collect_XP(Effects);
                Condition = false;
            }
            else
            {
                ShowInstructionPanel("Wrong parameter chosen");
                //iTween.ScaleTo(GlassPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 0.01f, "easetype", iTween.EaseType.spring));
                //InstructionPanel.transform.GetChild(0).GetComponent<Text>().text = "Wrong parameter choosen";
                //iTween.ScaleTo(InstructionPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 1f, "easetype", iTween.EaseType.spring, "oncomplete", "CloseInstructionPanel", "oncompletetarget", this.gameObject));
                //ObjectivePanel.transform.localScale = new Vector3(1, 0, 1);
                Condition = true;
            }
        }
        return Condition;
    }

    public void OpenInfoPanel()
    {
        UIZoomIn();
        iTween.ScaleTo(InfoPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 0.5f, "easetype", iTween.EaseType.spring));
        iTween.ScaleTo(GlassPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 0.01f, "easetype", iTween.EaseType.spring));

    }

    public void CloseInfoPanel(Button CloseBtn)
    {
        UIZoomOut();
        iTween.ScaleTo(InfoPanel, iTween.Hash("x", 0, "y", 0, "z", 0, "time", 0.2f, "easetype", iTween.EaseType.linear));
        iTween.ScaleTo(GlassPanel, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.2f, "time", 0.01f, "easetype", iTween.EaseType.linear));
        if (CloseBtn == Info_Cntue_Btn)
        {
            if (!IsAR)
                CamOrbit.DisableInteration = false;
            /*if (ActiveBtnCnt == 0)
            {
                for (int i = 0; i < Elements.Length; i++)
                {

                    Vector3 RandomPos = new Vector3(Elements[i].transform.localPosition.x, Elements[i].transform.localPosition.y, UnityEngine.Random.Range(-5f, 0f));
                    Vector3 CurrPos = Elements[i].transform.localPosition;

                    Elements[i].transform.localPosition = RandomPos;

                    iTween.MoveTo(Elements[i], iTween.Hash("position", CurrPos, "delay", 0.7f, "islocal", true, "time", UnityEngine.Random.Range(0.1f, 0.2f), "speed", 2.5f, "easetype", iTween.EaseType.easeInOutSine));


                }
            }*/
            Info_Close_Btn.gameObject.SetActive(true);
            Info_Cntue_Btn.gameObject.SetActive(false);
        }
        IsClicked = false;
    }


    public void ShowInstructionPanel(string TextToDisplay)
    {
        var mouse = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);



        if (mouse.x < Screen.width / 2)
        {
            // WronOptionPanel
            //print("Mouse is on left side of screen.");
            InstructionPanel.GetComponent<RectTransform>().pivot = new Vector2(0, 0);
        }



        if (mouse.x > Screen.width / 2)
        {
            //print("Mouse is on right side of screen.");
            InstructionPanel.GetComponent<RectTransform>().pivot = new Vector2(1, 0);
        }

        InstructionPanel.transform.position = Input.mousePosition;
        UIcanvas.GetComponent<GraphicRaycaster>().enabled = false;
        Camera.main.GetComponent<PhysicsRaycaster>().enabled = false;
        iTween.Stop(InstructionPanel.gameObject);

        InstructionPanel.transform.Find("InstructionText").GetComponent<Text>().text = TextToDisplay;
        InstructionPanel.transform.GetChild(0).GetComponent<Text>().color = GetColorFromColorCode("#FF7500");
        //iTween.ScaleTo(GlassPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 0.01f, "easetype", iTween.EaseType.spring));
        iTween.ScaleTo(InstructionPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 0.3f, "easetype", iTween.EaseType.spring, "oncomplete", "CloseInstructionPanel", "oncompletetarget", this.gameObject));
        ObjectivePanel.transform.localScale = new Vector3(1, 0, 1);
    }
    
    public void CloseInstructionPanel()
    {
        UIcanvas.GetComponent<GraphicRaycaster>().enabled = true;
        Camera.main.GetComponent<PhysicsRaycaster>().enabled = true;
        //iTween.ScaleTo(GlassPanel, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 1f, "time", 0.01f, "easetype", iTween.EaseType.linear));
        iTween.ScaleTo(InstructionPanel, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.5f, "time", 0.3f, "easetype", iTween.EaseType.spring));
        if (!IsTopicUnlocked)
            iTween.ScaleTo(ObjectivePanel, iTween.Hash("y", 1, "time", 0.3f, "delay", 0.5f, "easetype", iTween.EaseType.easeInOutSine));
    }

    public Color GetColorFromColorCode(string ColorCode)
    {
        Color ConvColor;
        bool Converted = ColorUtility.TryParseHtmlString(ColorCode, out ConvColor);
        return ConvColor;
    }

    public void ToDoList_DDCancel()
    {
        if (ToDoList_DD.transform.Find("Dropdown List") != null)//&& Obj.transform.localScale.y!=1)
        {
            //print("cancelled........");
            //print("list.....cancelled........" + ToDoList_DD.transform.Find("Dropdown List").gameObject);
            ToDoList_DD.transform.Find("Arrow").transform.eulerAngles = new Vector3(0, 0, 0);
            iTween.ScaleTo(ToDoList_DD.transform.Find("Dropdown List").gameObject, iTween.Hash("y", 0, "time", 0.5f, "easetype", iTween.EaseType.spring));
            iTween.ScaleTo(ObjectivePanel, iTween.Hash("y", 1, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine));
            if (SidePannelOpen.transform.eulerAngles.z == 180)
            {
                OpenSidePannel();
            }
        }
    }

    public void ExploringToDOList_DD()
    {
        //print("ShowDD.....");
        //ToDoList_DD.transform.Find("Topic_Objective").GetComponent<Text>().text = "";
        if (SidePannelOpen.transform.eulerAngles.z == 0)
        {
            OpenSidePannel();
        }

        ToDoList_DD.transform.Find("Arrow").transform.eulerAngles = new Vector3(0, 0, 180);
        GameObject DDList = ToDoList_DD.transform.Find("Dropdown List").gameObject;
        DDList.transform.GetComponent<RectTransform>().sizeDelta = new Vector2(0, 250);
        DDList.transform.localScale = new Vector3(1, 0, 1);
        iTween.ScaleTo(DDList, iTween.Hash("y", 1, "time", 0.5f, "easetype", iTween.EaseType.spring));
        //iTween.ScaleTo(ToDoList_DD.transform.Find("Topic_Objective").gameObject, iTween.Hash("y", 0, "time", 0.5f, "easetype", iTween.EaseType.spring));
        if (!IsTopicUnlocked)
            iTween.ScaleTo(ObjectivePanel, iTween.Hash("y", 0, "time", 0.5f, "delay", 0.1f, "easetype", iTween.EaseType.easeInOutSine));

        string TestStr = "";
        int CntToChangePos = 0;
        for (int i = 0; i < ToDoList_DD.options.Count; i++)
        {
            GameObject Child = GameObject.Find("Item " + i + ": " + ToDoList_DD.options[i].text);
            //Child.transform.parent.GetComponent<RectTransform>().sizeDelta = new Vector2(0,250);

            if (TestStr == ToDoList_DD.options[i].text)
            {
                Child.SetActive(false);
                Child.transform.localPosition = Child.transform.localPosition + new Vector3(0, 50 * CntToChangePos, 0);
                CntToChangePos++;
            }
            else
            {
                Child.transform.Find("XP_Text").GetComponent<Text>().text = "+" + Sub_Top_XP[i] + "XP";
                Child.transform.localPosition = Child.transform.localPosition + new Vector3(0, 50 * CntToChangePos, 0);

                if (i < TopicToCheck)
                {
                    Child.transform.Find("Item Checkmark").GetComponent<Image>().enabled = true;
                    //Child.transform.Find("XP_Text").GetComponent<Text>().color = Color.green / 2;
                    //Child.transform.Find("Item Checkmark").GetComponent<Image>().color = Color.green / 2;
                    Child.transform.Find("Item Background").GetComponent<Image>().enabled = false;
                }
                else if (i == TopicToCheck)
                {
                    Child.transform.Find("Item Background").GetComponent<Image>().enabled = true;// color = ReturnColorFromHex("#a01c65");
                                                                                                 //Child.transform.Find("Item Label").GetComponent<Text>().color = Color.white;
                }
            }
            TestStr = ToDoList_DD.options[i].text;
        }
    }


    public void OpenSidePannel()
    {
        if (SidePannelOpen.transform.eulerAngles.z == 180)
        {
            //iTween.MoveAdd(SidePannel, iTween.Hash("x", -400f, "time",0.5f,"islocal",true, "easetype", iTween.EaseType.linear));
            //iTween.MoveAdd(SidePannelOpen.gameObject, iTween.Hash("x", -185f, "islocal", true, "time", 0.5f, "easetype", iTween.EaseType.linear));
            //SidePannelOpen.transform.eulerAngles = new Vector3(0,0,180);
            SidePannel.GetComponent<Animator>().Play("PannelForward");
        }
        else
        {
            //iTween.MoveAdd(SidePannel, iTween.Hash("x", 400f, "time", 0.5f, "islocal", true, "easetype", iTween.EaseType.linear));
            //iTween.MoveAdd(SidePannelOpen.gameObject, iTween.Hash("x", -185f, "islocal", true, "time", 0.5f, "easetype", iTween.EaseType.linear));
            //SidePannelOpen.transform.eulerAngles = new Vector3(0, 0, 0);
            SidePannel.GetComponent<Animator>().Play("PannelBackward");
        }
    }


    public void UIReset()
    {
        ObjectivePanel.GetComponent<RectTransform>().anchoredPosition = new Vector3(0f, 5f, 0);

        BarrelSlider.transform.GetComponent<RectTransform>().anchoredPosition = new Vector3(-20f, 0f, 0);
        Info_Open_Btn.transform.GetComponent<RectTransform>().anchoredPosition = new Vector3(-37f, -150f, 0);


        SidePannel.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(0f, 0f, 0);

        //print("side......" + SidePannel.transform.position+"....local....."+ SidePannel.transform.localPosition);
        if (!IsTopicUnlocked)
        {
            ToDoList_DD.transform.GetComponent<RectTransform>().anchoredPosition = new Vector3(135f, -150f, 0);
            XP_ToShow.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(135f, -150f, 0);

        }
        else
        {
            ToDoList_DD.transform.GetComponent<RectTransform>().anchoredPosition = new Vector3(-250f, -150f, 0);
            XP_ToShow.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(-250f, -150f, 0);

        }

        //Main_XP_ToShow.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(-135f, -60f, 0);

    }

    public void UIZoomOut()
    {
        if (ObjectivePanel.GetComponent<RectTransform>().anchoredPosition.y == 5)
            return;

        UIReset();

        iTween.MoveFrom(Info_Open_Btn.gameObject, iTween.Hash("x", Info_Open_Btn.transform.localPosition.x + 250, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveFrom(SidePannel.transform.parent.gameObject, iTween.Hash("x", SidePannel.transform.parent.localPosition.x - 1000f, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveFrom(BarrelSlider.gameObject, iTween.Hash("x", BarrelSlider.transform.localPosition.x + 250, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveFrom(ObjectivePanel, iTween.Hash("y", ObjectivePanel.transform.localPosition.y - 1000, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));


        iTween.MoveFrom(ToDoList_DD.gameObject, iTween.Hash("x", ToDoList_DD.transform.localPosition.x - 400, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveFrom(XP_ToShow.transform.parent.gameObject, iTween.Hash("x", XP_ToShow.transform.parent.localPosition.x - 400, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));

        //iTween.MoveFrom(Main_XP_ToShow.transform.parent.gameObject, iTween.Hash("x", Main_XP_ToShow.transform.parent.localPosition.x + 550, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
    }



    public void UIZoomIn()
    {
        if (ObjectivePanel.GetComponent<RectTransform>().anchoredPosition.y != 5)
            return;

        UIReset();
        //print("side......" + SidePannel.transform.position);
        iTween.MoveAdd(ObjectivePanel, iTween.Hash("y", ObjectivePanel.transform.localPosition.y - 1000, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveAdd(Info_Open_Btn.gameObject, iTween.Hash("x", Info_Open_Btn.transform.localPosition.x + 250, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveAdd(SidePannel.transform.parent.gameObject, iTween.Hash("x", SidePannel.transform.parent.localPosition.x - 1000f, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));

        iTween.MoveAdd(BarrelSlider.gameObject, iTween.Hash("x", BarrelSlider.transform.localPosition.x + 250, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));

        iTween.MoveAdd(XP_ToShow.transform.parent.gameObject, iTween.Hash("x", XP_ToShow.transform.parent.localPosition.x - 400, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveAdd(ToDoList_DD.gameObject, iTween.Hash("x", ToDoList_DD.transform.localPosition.x - 400, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));

        //iTween.MoveAdd(Main_XP_ToShow.transform.parent.gameObject, iTween.Hash("x", Main_XP_ToShow.transform.parent.localPosition.x + 550, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
    }

    public float ChangeToLocal_X(float Val)
    {
        float X_ToRet = 0;
        //print("wid........"+ UIcanvas.GetComponent<RectTransform>().rect.width);
        X_ToRet = ((UIcanvas.GetComponent<RectTransform>().rect.x) + Val);

        return X_ToRet;
    }

    public float ChangeToLocal_Y(float Val)
    {
        float Y_ToRet = 0;
        //print("wid........"+ UIcanvas.GetComponent<RectTransform>().rect.width);
        Y_ToRet = ((UIcanvas.GetComponent<RectTransform>().rect.y) + Val);

        return Y_ToRet;
    }





    public void Collect_XP(GameObject TargetPos)
    {
        UIcanvas.GetComponent<GraphicRaycaster>().enabled = false;
        Camera.main.GetComponent<PhysicsRaycaster>().enabled = false;

        XP_Effect.transform.position = Camera.main.WorldToScreenPoint(TargetPos.transform.position);

        XP_Effect.text = "+" + Sub_Top_XP[TopicToCheck] + "XP";

        XP_Effect.transform.localScale = new Vector3(2, 0, 2);

        iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 2, "y", 2, "z", 2, "time", 0.5f, "easetype", iTween.EaseType.easeOutElastic));

        iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", XP_Effect.transform.position.x, "y", XP_Effect.transform.position.y + 150, "z", 0, "delay", 0.3f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

        iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", 150, "y", XP_ToShow.transform.position.y, "z", 0, "delay", 1f, "time", 1f, "easetype", iTween.EaseType.easeInSine, "oncomplete", "Add_To_XP", "oncompletetarget", this.gameObject));

    }

    public void Add_To_XP()
    {

        //print("TopicToCheck......."+ TopicToCheck+"....sub......"+Sub_Topics.Count);
        if (TopicToCheck < (Sub_Top_ToDoList.Count))
        {
            Sub_XP_Earned += float.Parse(Sub_Top_XP[TopicToCheck]);

            XP_Effect.text = "";
            XP_ToShow.text = Sub_XP_Earned + "/" + Total_XP;

            TopicToCheck++;
            if (TopicToCheck <= (Sub_Top_ToDoList.Count - 1))
            {
                ObjectivePanel.transform.Find("ObjectiveText").GetComponent<Text>().text = Sub_Top_ToDoList[TopicToCheck];
                ObjectivePanel.transform.localScale = new Vector3(1, 0, 1);
                iTween.ScaleTo(ObjectivePanel, iTween.Hash("y", 1, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine));
                //ToDoList_DD.transform.Find("Topic_Objective").GetComponent<Text>().text = "Objective: " + Sub_Top_ToDoList[TopicToCheck];
                UIcanvas.GetComponent<GraphicRaycaster>().enabled = true;
                Camera.main.GetComponent<PhysicsRaycaster>().enabled = true;
            }
            else
            {
                XP_Effect.transform.position = (XP_ToShow.transform.position);

                XP_Effect.text = Total_XP + "XP";

                XP_Effect.transform.localScale = new Vector3(1, 0, 1);

                iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 2, "y", 2, "z", 2, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeOutElastic));

                iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", XP_Effect.transform.position.x, "y", XP_Effect.transform.position.y + 50, "z", 0, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

                iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", Main_XP_ToShow.transform.position.x, "y", Main_XP_ToShow.transform.position.y, "z", 0, "delay", 1.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine, "oncomplete", "AddTotalToMainXP", "oncompletetarget", this.gameObject));

                iTween.MoveTo(ToDoList_DD.gameObject, iTween.Hash("x", ChangeToLocal_X(-400), "delay", 3.5f, "time", 0.5f, "islocal", true, "easetype", iTween.EaseType.easeInOutSine));

                iTween.MoveTo(XP_ToShow.transform.parent.gameObject, iTween.Hash("x", ChangeToLocal_X(-400), "delay", 3.5f, "time", 0.5f, "islocal", true, "easetype", iTween.EaseType.easeInOutSine));
                //print("label......" + LabelImage.GetComponent<RectTransform>().anchoredPosition+".......explode...."+ ExplodeSlider.GetComponent<RectTransform>().anchoredPosition);


                //iTween.MoveTo(ValueDisplaySlider.gameObject, iTween.Hash("x", ChangeToLocal_X ( 150), "delay", 0.7f, "time", 0.5f, "islocal", true, "easetype", iTween.EaseType.easeInOutSine));
            }
        }

    }

    public void AddTotalToMainXP()
    {
        if (OptionUnlockedTill < 1)
        {
            OptionUnlockedTill++;
            UIcanvas.GetComponent<GraphicRaycaster>().enabled = true;
            Camera.main.GetComponent<PhysicsRaycaster>().enabled = true;
            float MainXp = float.Parse(Main_XP_ToShow.text);
            MainXp += Total_XP;
            Main_XP_ToShow.text = MainXp.ToString();
            //ObjectivePanel.transform.Find("ObjectiveText").GetComponent<Text>().text = "YaY!......Next topic unlocked";
            XP_Effect.text = "";
            IsTopicUnlocked = true;
            iTween.ScaleTo(ObjectivePanel, iTween.Hash("y", 0, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine));
            //IsInteraction = true;


        }
    }

}