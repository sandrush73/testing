﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;
using System.Linq;
using System;
using System.Text.RegularExpressions;
using UnityEngine.SceneManagement;

public class PeriodicTable : MonoBehaviour
{
    public TextAsset JsonTxtData;
    public TextAsset CSVFile;
    public List<JsonElements> ElecConfigData_Json;
    public GameObject PerodicTableSet, PerodicTableSet2, ElecConfigShell, SelectedElement, EleConfigPanel, AtomInCircle, AtomDisplayObj, SidePannel,
        Proton, Neutron, ProtonsAndNeutrons, OrbitNames, AtomSize, ElementForTrans, Target, ScrollBarViewPort, IonicRadiusSphere, IonizationPotential, OxidationStates;
    public GameObject[] Elements, Elements_Sphere, ShellNames, UIPanels;
    public string TestString, JsonString, AtomsCount, ElecConfig;
    public Dictionary<string, string> DescriTextDoc;
    public Text ElementSymbol_UI, ElementName_UI, AtomicNumber_UI, NumOfElectrons_UI, ElecConfig_UI, MassNumber_UI, NumOfNeutrons_UI, IonicRadius_UI, BoilingPoint_UI,
        MeltingPoint_UI, Density_UI, ElectroNegativity_UI,SliderValue_Text,SliderValue_Units;
    public Button SidePannelOpen, BackToPT_But;// TopSidePannelOpen, TopSidePannel, BtnselName;
    public Toggle ShellInfo_Toggle, SpinElec_Toggle, Transparency_Toggle;
    public Material TransparentMaterial;
    public Renderer Shadow;
    public int ElectronsCount, NeutronsCount, ActiveBtnCnt;
    public TextMeshPro AtomicRadius_Text;
    public Material[] ElementsMaterials;
    public string[] ArrayOfStrings;
    public int[] AtomsNumber;
    public bool IsClicked, CanPool, IsShellInfo, IsSpinElec, IsTransparency, IsAR, IsUIZooming;
    public Color ShellColor;
    public List<GameObject> ProtonsPooled, NeutronsPooled, Atoms;
    int DummyCount_P, DummyCount_N;
    //public Button[] SelectionButtons;
    public Color[] OriginalColors, ColorsBetween, ReffColor;
    public List<float> BoilingPoint, MeltingPoint;
    public Slider ValueDisplaySlider;
    public AssetBundle assetBundle;
    
    public AROrbitControls ArOrbit;



    public ObjectOrbitChem CamOrbit;
    public float scale_value;
    private int old_sel_btn;
    private Color old_color;

    public int OptionUnlockedTill, CurrentTopicSelected, TopicToCheck;
    public float Sub_XP_Earned, Total_XP;
    public Dropdown ToDoList_DD;
    public GameObject InstructionPanel_DD, ObjectivePanel, InstructionPanel, InfoPanel,GlassPanel,Canvas_BG;
    public Button Info_Close_Btn, Info_Cntue_Btn,Info_Open_Btn;
    public List<Dictionary<string, object>> CSV_data;
    public List<String> Topics, Sub_Topics, Sub_Top_ToDoList, Sub_Top_Desc, Sub_Top_XP,LearningGoals;
    public Text XP_ToShow, XP_Effect, Main_XP_ToShow,Learninggoal_Txt,Instruction_Txt,Assumption_Txt,Heading_Txt,IP1_Text,IP2_Text,IP3_Text;
    public bool IsTopicUnlocked;
    public Canvas UIcanvas;

    public enum PeriodicTableTopic { vid473_ec, vid465_am, vid472_ar, vid214_ir, vid591_bp, vid591_mp, den, vid291_en, vid98_os, vid217_ip };
    public PeriodicTableTopic PT_CurrentTopic;
    // Start is called before the first frame update
    void Start()
    {
        //PT_CurrentTopic = (PeriodicTableTopic)PlayerPrefs.GetInt("PT_Name");// PeriodicTable.EN;
        //transform.parent.name = PT_CurrentTopic.ToString();

        string TopicName = transform.parent.name;



        if (TopicName.Contains("Clone"))
        {

            TopicName = TopicName.Substring(0, TopicName.Length - 7);
        }

        Enum.TryParse(TopicName, out PT_CurrentTopic);//PeriodicTable.sap;//change this for PT Asset bundles

        //print("......." + PT_CurrentTopic.ToString());



        UIcanvas = GameObject.Find("Canvas_EC").GetComponent<Canvas>();// FindObjectOfType<Canvas>().GetComponent<RectTransform>().rect;

        LoadFromAsssetBundle();

        string[] JsonLines = Regex.Split(JsonTxtData.text, "\n");

        
        ElecConfigData_Json = new List<JsonElements>();
        //ScrollBarViewPort = GameObject.Find("Scroll View (1)");
        for (int i = 0; i < JsonLines.Length; i++)
        {
            int j = i;
            JsonString = JsonLines[j];
            JSONObject jsonObject;
            //print("i......" + i);
            if (j == 0)
            {
                //ElecConfigData_Json = JsonArray.FromJson<JsonElements>(JsonString);
                jsonObject = JSONObject.Parse(JsonString);
                //print("ElecConfigData_Json........" + ElecConfigData_Json.Count);

            }
            else
            {


                //ElecConfigData_Json.Concat(JsonArray.FromJson<JsonElements>(JsonString));
                jsonObject = JSONObject.Parse(JsonString);
                //print("ElecConfigData_Json222222........" + ElecConfigData_Json.Count);
            }

            for (int JsArray = 0; JsArray < jsonObject.GetArray("JsonElements").Length; JsArray++)
            {
                JsonElements ElementsData = new JsonElements();

                JSONObject ArrayData = jsonObject.GetArray("JsonElements")[JsArray].Obj;

                ElementsData.SNO = ArrayData.GetValue("SNO").Str;
                ElementsData.AtomicRadius = ArrayData.GetValue("AR").Str;
                ElementsData.COLOR = ArrayData.GetValue("COL").Str;
                ElementsData.ELECTRONICCONFIGURATION = ArrayData.GetValue("EC").Str;
                ElementsData.ELECTRONICCONFIGURATIONATOMS = ArrayData.GetValue("ECA").Str;
                ElementsData.ELECTRONS = ArrayData.GetValue("ELEC").Str;
                ElementsData.ELEMENT = ArrayData.GetValue("ELE").Str;
                ElementsData.NEUTRONS = ArrayData.GetValue("NEU").Str;
                ElementsData.SYMBOL = ArrayData.GetValue("SYM").Str;
                ElementsData.IonicRadius = ArrayData.GetValue("IR").Str;
                ElementsData.BoilingPoint = ArrayData.GetValue("BP").Str;
                ElementsData.MeltingPoint = ArrayData.GetValue("MP").Str;
                ElementsData.Density = ArrayData.GetValue("DEN").Str;
                ElementsData.ElectroNegativity = ArrayData.GetValue("EN").Str;
                ElementsData.IP1 = ArrayData.GetValue("IP1").Str;
                ElementsData.IP2 = ArrayData.GetValue("IP2").Str;
                ElementsData.IP3 = ArrayData.GetValue("IP3").Str;
                //print("ElecConfigData_Json222222........" + ArrayData.GetValue("COLOR").Str);

                ElecConfigData_Json.Add(ElementsData);

                //print("ElecConfigData_Json222222........" + ElecConfigData_Json.Count);

            }
        }

        print("data loaded from json.......");

        IsClicked = true;
        IsShellInfo = true;
        IsSpinElec = true;
        IsTransparency = false;
        ActiveBtnCnt = 0;
        
        

        ReadingDataFromCSVFile();

        ReadingXps();
        //print(TableData.dataArray[1].Elements);
        FindGameObjects();

        AssignEventsToObjects();


        


        //print("enum......"+ (int)PT_CurrentTopic);
        PropertyToDisplay((int)PT_CurrentTopic);
        //TestString= @"{"ElementsList":[{"SNO":"1","ELEMENT":"Hydrogen","ELECTRONS":"1","NEUTRONS":"0","SYMBOL":"H","COLOR":"#f6f6f6","ELECTRONICCONFIGURATIONATOMS":"1s1","ELECTRONICCONFIGURATION":"1s1"}]}";
        //ElecConfigData_Json = JsonUtility.FromJson<JsonObject>(TestString);

        //if((int)PT_CurrentTopic<8)

        

        if(ActiveBtnCnt <= 7)
        InvokeRepeating("IncrShadowAlpha",2f,0.01f);

        
    }

    public void LoadFromAsssetBundle()
    {
        //if (Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.Android)
        //{
        assetBundle = AssetBundle.LoadFromFile(PlayerPrefs.GetString("AssetBundleUrl"));

        CSVFile = assetBundle.LoadAsset(PlayerPrefs.GetString("LibraryName") + "CSV", typeof(TextAsset)) as TextAsset;

        TransparentMaterial = assetBundle.LoadAsset("TransparentMaterial", typeof(Material)) as Material;
        TransparentMaterial.shader = Shader.Find("Standard");

        AtomInCircle = assetBundle.LoadAsset("AtomInCircle", typeof(GameObject)) as GameObject;
        Proton = assetBundle.LoadAsset("Proton", typeof(GameObject)) as GameObject;
        Neutron = assetBundle.LoadAsset("Neutron", typeof(GameObject)) as GameObject;


        TextAsset JsonText = assetBundle.LoadAsset("ElementsDataJson.txt", typeof(TextAsset)) as TextAsset;
        JsonTxtData = JsonText;



        //}
        //else
        //{

        //string CSVName = PT_CurrentTopic.ToString();



        //    if (CSVName.Contains("Clone"))
        //    {



        //        CSVName = CSVName.Substring(0, CSVName.Length - 7);
        //    }



        //    print("casvname........." + CSVName);



        //    CSVFile = Resources.Load(CSVName+"CSV") as TextAsset;
        //}







        Camera.main.gameObject.AddComponent<PhysicsRaycaster>();



        Camera.main.backgroundColor = Color.black;



        GameObject TargetForCamera = GameObject.Find("Target");



        if (TargetForCamera != null)
        {
            TargetForCamera.AddComponent<ObjectOrbitChem>();
            CamOrbit = TargetForCamera.GetComponent<ObjectOrbitChem>();

            
            

            
            CamOrbit.Pan_X_Min = -2f;
            CamOrbit.Pan_X_Max = 2f;
            CamOrbit.Pan_Y_Min = -0.75f;
            CamOrbit.Pan_Y_Max = 0.75f;
            CamOrbit.CamRot_Up_Min = -60f;
            CamOrbit.CamRot_Up_Max = -20f;
            CamOrbit.CamRot_Side_Min = -20f;
            CamOrbit.CamRot_Side_Max = 20f;

            if ((int)PT_CurrentTopic >= 8)
            {

                CamOrbit.zoomMax = 3f;
                CamOrbit.zoomMin = 0.5f;
                CamOrbit.zoomStart = 1.5f;
                CamOrbit.distance = CamOrbit.zoomStart;
                CamOrbit.Target_Y_Value = 0.4f;
                CamOrbit.ZoomMax_Y_Value = 0.4f;
                CamOrbit.transform.position = new Vector3(0, 0.4f, 0);
            }
            else
            {
                CamOrbit.distance = CamOrbit.zoomStart;
                CamOrbit.zoomStart = 3f;
                CamOrbit.zoomMin = 1.5f;
                CamOrbit.zoomMax = 5.5f;
                CamOrbit.Target_Y_Value = 0.7f;
                CamOrbit.ZoomMax_Y_Value = 0.7f;
                CamOrbit.transform.position = new Vector3(0, 0.7f, 0);
            }

            IsAR = false;
        }
        else
        {
            this.transform.parent.gameObject.AddComponent<AROrbitControls>();
            ArOrbit = this.transform.parent.gameObject.GetComponent<AROrbitControls>();
            IsAR = true;
        }



    }

    public void ReadingDataFromCSVFile()
    {
        Topics = new List<string>();
        Sub_Topics = new List<string>();
        Sub_Top_Desc = new List<string>();
        Sub_Top_ToDoList = new List<string>();
        Sub_Top_XP = new List<string>();
        LearningGoals = new List<string>();

        CSV_data = CSVReader.Read(CSVFile);


        for (var i = 0; i < CSV_data.Count; i++)
        {
            //print("Topics " + CSV_data[i]["Learninggoals"] + ".......... " +"ToDo " + CSV_data[i]["Topic_TodoList"]);

           

            if (CSV_data[i]["Topic_SubTopics"].ToString() != "" && CSV_data[i]["Topic_SubTopics"].ToString() != null)
            {
                Sub_Topics.Add(CSV_data[i]["Topic_SubTopics"].ToString());

            }

            if (CSV_data[i]["Topic_ToDoList"].ToString() != "" && CSV_data[i]["Topic_ToDoList"].ToString() != null)
            {
                Sub_Top_ToDoList.Add(CSV_data[i]["Topic_ToDoList"].ToString());

            }

            

            //if (CSV_data[i]["Top_XP"].ToString() != "" && CSV_data[i]["Top_XP"].ToString() != null)
            //{
            //    Sub_Top_XP.Add(CSV_data[i]["Top_XP"].ToString());

            //}

            if (CSV_data[i]["Learninggoals"].ToString() != "" && CSV_data[i]["Learninggoals"].ToString() != null)
            {
                LearningGoals.Add(CSV_data[i]["Learninggoals"].ToString());

            }

        }






    }

    public int totXp, loclXp, PerTopic_XP;

    public void ReadingXps()
    {

        //PlayerPrefs.SetInt("TotalXP", 34);
        //PlayerPrefs.SetInt("MaxEarnings", 39);

        totXp = PlayerPrefs.GetInt("TotalXP");
        loclXp = PlayerPrefs.GetInt("MaxEarnings");

        TopicToCheck = 0;

        if (loclXp != 0)
        {
            OptionUnlockedTill = 0;

        }
        else
        {
            OptionUnlockedTill = 1;

        }

        



        int t = 0;
        int p = 0;

        for (int k = 0; k < Sub_Top_ToDoList.Count; k++)
        {

            p = loclXp / Sub_Top_ToDoList.Count;
            Sub_Top_XP.Add(p.ToString());
            t = t + p;
        }

        if (t != loclXp)
        {
            Sub_Top_XP[Sub_Top_ToDoList.Count - 1] = (p + (loclXp - t)).ToString();
        }

        Total_XP = loclXp;
        

    }



    public float result;

    public void GetResult()
    {
        result = ((float)(TopicToCheck - 0) / (float)(Sub_Top_ToDoList.Count)) * 100;
        Camera.main.SendMessage("UnloadActually", result);
    }

    public void CalculateColors(Color StartColor, Color EndColor, int Steps)
    {
        ColorsBetween = new Color[Steps];
        ReffColor = new Color[Steps];
        float[] step = new float[3];

        var steps = Steps - 1;
        step[0] = (EndColor.r - StartColor.r) / steps;
        step[1] = (EndColor.g - StartColor.g) / steps;
        step[2] = (EndColor.b - StartColor.b) / steps;

        var a = 0.58f;

        for (int i = 1; i < Steps; i++)
        {
            var r = (StartColor.r + (step[0] * i));
            var g = (StartColor.g + (step[1] * i));
            var b = (StartColor.b + (step[2] * i));

            ColorsBetween[i] = new Color(r, g, b, a);
        }

        ColorsBetween[0] = new Color(StartColor.r, StartColor.g, StartColor.b, a);
        ColorsBetween[Steps - 1] = new Color(EndColor.r, EndColor.g, EndColor.b, a);
    }




    public void FindGameObjects()
    {
        print("FindGameObjects.......");
        PerodicTableSet = GameObject.Find("periodic_table_LP");
        PerodicTableSet2 = GameObject.Find("SphericalPeriodicTable");
        PerodicTableSet2.SetActive(false);
        //GameObject Canvas = GameObject.Find("Canvas_EC");
        Shadow = GameObject.Find("Shadow").transform.GetChild(0).GetComponent<MeshRenderer>();
        Canvas_BG = GameObject.Find("Canvas_BG");
        if (!IsAR)
        {
            Canvas_BG.transform.parent = Camera.main.transform;
            Canvas_BG.GetComponent<Canvas>().worldCamera=Camera.main;
        }

        else
        {
            Canvas_BG.SetActive(false);
        }
        //GameObject content = GameObject.Find("Content");

        UIPanels = new GameObject[UIcanvas.transform.childCount - 1];
        //SelectionButtons = content.GetComponentsInChildren<Button>();

        for (int i = 0; i < UIPanels.Length; i++)
        {
            UIPanels[i] = UIcanvas.transform.GetChild(i).gameObject;
            if (i != 0)
                UIPanels[i].SetActive(false);
        }

        Elements = new GameObject[118];
        Elements_Sphere = new GameObject[118];
        ElementsMaterials = new Material[118];
        OriginalColors = new Color[118];
        BoilingPoint = new List<float>();
        MeltingPoint = new List<float>();
        AtomsNumber = new int[0];

        for (int i = 0; i < Elements.Length; i++)
        {
            Elements[i] = PerodicTableSet.transform.Find("Element_" + (i + 1)).gameObject;
            Elements_Sphere[i] = PerodicTableSet2.transform.GetChild(0).Find("Element_" + (i + 1)).gameObject;
            /*Elements[i].transform.Find("ElementSymbol_Text").GetComponent<TextMeshPro>().text = TableData.dataArray[i].Symbol;
            Elements[i].transform.Find("ElementName_Text").GetComponent<TextMeshPro>().text = TableData.dataArray[i].Name;
            Elements[i].transform.Find("ElementNumber_Text").GetComponent<TextMeshPro>().text = TableData.dataArray[i].Atomic_Number.ToString();*/

            Elements[i].transform.Find("ElementSymbol_Text").GetComponent<TextMeshPro>().text = ElecConfigData_Json[i].SYMBOL;
            Elements[i].transform.Find("ElementName_Text").GetComponent<TextMeshPro>().text = ElecConfigData_Json[i].ELEMENT;
            Elements[i].transform.Find("ElementNumber_Text").GetComponent<TextMeshPro>().text = ElecConfigData_Json[i].SNO;

            Elements_Sphere[i].transform.Find("ElementSymbol_Text").GetComponent<TextMeshPro>().text = ElecConfigData_Json[i].SYMBOL;
            Elements_Sphere[i].transform.Find("ElementName_Text").GetComponent<TextMeshPro>().text = ElecConfigData_Json[i].ELEMENT;
            Elements_Sphere[i].transform.Find("ElementNumber_Text").GetComponent<TextMeshPro>().text = ElecConfigData_Json[i].SNO;

            /*Vector3 RandomPos = new Vector3(Elements[i].transform.localPosition.x, Elements[i].transform.localPosition.y, UnityEngine.Random.Range(-5f, 0f));
            Vector3 CurrPos = Elements[i].transform.localPosition;

            Elements[i].transform.localPosition = RandomPos;

            iTween.MoveTo(Elements[i], iTween.Hash("position", CurrPos, "delay", 0.7f, "islocal", true, "time", UnityEngine.Random.Range(0.1f, 0.2f), "speed", 2.5f, "easetype", iTween.EaseType.easeInOutSine));
            */
            ElementsMaterials[i] = Elements[i].GetComponentInChildren<MeshRenderer>().material;

            OriginalColors[i] = Elements[i].GetComponentInChildren<MeshRenderer>().material.color;



            if (ElecConfigData_Json[i].BoilingPoint != "" && ElecConfigData_Json[i].BoilingPoint != "0" && ElecConfigData_Json[i].BoilingPoint != "-")
                BoilingPoint.Add(float.Parse(ElecConfigData_Json[i].BoilingPoint));

            if (ElecConfigData_Json[i].MeltingPoint != "" && ElecConfigData_Json[i].MeltingPoint != "0" && ElecConfigData_Json[i].MeltingPoint != "-")
                MeltingPoint.Add(float.Parse(ElecConfigData_Json[i].MeltingPoint));



        }



        //AtomInCircle =Resources.Load("AtomInCircle") as GameObject;

        //Neutron = Resources.Load("Neutron") as GameObject;
        //Proton = Resources.Load("Proton") as GameObject;

        AtomDisplayObj = GameObject.Find("AtomDisplay");
        EleConfigPanel = GameObject.Find("ConfigurationPanel");
        ElecConfigShell = GameObject.Find("electronic_config");
        ProtonsAndNeutrons = GameObject.Find("ProtonsAndNeutrons");
        OrbitNames = GameObject.Find("Names");
        AtomSize = GameObject.Find("AtomSize");
        Target = GameObject.Find("Target");
        IonicRadiusSphere = GameObject.Find("IonicRadiusSphere");
        IonicRadiusSphere.SetActive(false);
        IonizationPotential = PerodicTableSet2.transform.GetChild(2).gameObject;
        IonizationPotential.SetActive(false);
        OxidationStates = PerodicTableSet2.transform.GetChild(1).gameObject;
        OxidationStates.SetActive(false);
        

        ValueDisplaySlider = GameObject.Find("ValueDisplaySlider").GetComponent<Slider>();
        SliderValue_Text = GameObject.Find("Slider_Value_Text").GetComponent<Text>();
        SliderValue_Units = GameObject.Find("Slider_Units_Text").GetComponent<Text>();
        SliderValue_Units.color =Color.black;
        ValueDisplaySlider.gameObject.SetActive(false);

        for (int i = 0; i < OrbitNames.transform.childCount; i++)
        {
            OrbitNames.transform.GetChild(i).GetChild(0).gameObject.AddComponent<SmoothLook>();
        }

        ShellNames = new GameObject[AtomDisplayObj.transform.childCount];
        for (int i = 0; i < AtomDisplayObj.transform.childCount; i++)
        {
            ShellNames[i] = AtomDisplayObj.transform.GetChild(i).gameObject;
        }

        /*ElementSymbol_UI = GameObject.Find("ElementSymbol_Value").GetComponent<Text>();
        ElementName_UI = GameObject.Find("ElementName_Value").GetComponent<Text>();
        AtomicNumber_UI = GameObject.Find("AtomicNumber_Value").GetComponent<Text>();
        NumOfElectrons_UI = GameObject.Find("NumOfElectrons_Value").GetComponent<Text>();
        ElecConfig_UI = GameObject.Find("ElectronicConfig_Value").GetComponent<Text>();*/

        AtomicRadius_Text = GameObject.Find("AtomRadius_Value").GetComponent<TextMeshPro>();

       
        //TopSidePannel = GameObject.Find("pivot");
        SidePannelOpen = GameObject.Find("PannelOpenButton").GetComponent<Button>();
        
        //TopSidePannelOpen = GameObject.Find("ArrowImage").GetComponent<Button>();
        BackToPT_But = GameObject.Find("GoToPT").GetComponent<Button>();

        ShellInfo_Toggle = GameObject.Find("ShowShell_Toggle").GetComponent<Toggle>();
        SpinElec_Toggle = GameObject.Find("SpinElec_Toggle").GetComponent<Toggle>();
        Transparency_Toggle = GameObject.Find("Transparency_Toggle").GetComponent<Toggle>();
        //TransparentMaterial = Resources.Load("TransparentMaterial")as Material;
        //ElectronicConfiguration =ConvertToNormal(TestString);
        //BtnselName = GameObject.Find("SelName");
       

        ProtonsPooled = new List<GameObject>();
        NeutronsPooled = new List<GameObject>();
        Atoms = new List<GameObject>();
        CanPool = true;

        for (int Pro = 0; Pro < 118; Pro++)
        {
            GameObject proton = Instantiate(Proton) as GameObject;
            ProtonsPooled.Add(proton);
            proton.SetActive(false);

            proton.transform.parent = ProtonsAndNeutrons.transform;
        }

        for (int Neu = 0; Neu < 175; Neu++)
        {
            GameObject neutron = Instantiate(Neutron) as GameObject;
            NeutronsPooled.Add(neutron);
            neutron.SetActive(false);

            neutron.transform.parent = ProtonsAndNeutrons.transform;
        }

        ElecConfigShell.transform.localScale = Vector3.zero;

        AtomicRadius_Text.gameObject.transform.parent.gameObject.AddComponent<SmoothLook>();
        AtomSize.SetActive(false);
        AtomicRadius_Text.gameObject.transform.parent.gameObject.SetActive(false);


        InstructionPanel_DD = GameObject.Find("DD_Instruction");
        InstructionPanel = GameObject.Find("InstructionPanel");
        ToDoList_DD = GameObject.Find("ToDoList_Dropdown").GetComponent<Dropdown>();
        XP_ToShow = GameObject.Find("Gained_XP_Value").GetComponent<Text>();
        XP_Effect = GameObject.Find("Effect_Text").GetComponent<Text>();
        Main_XP_ToShow = GameObject.Find("Main_XP_Value").GetComponent<Text>();
        ObjectivePanel = GameObject.Find("ObjectivePanel");
        InstructionPanel.transform.localScale = Vector3.zero;
        InfoPanel= GameObject.Find("InformationPanel");
        Info_Close_Btn= GameObject.Find("InfoCloseBtn").GetComponent<Button>();
        Info_Cntue_Btn = GameObject.Find("InfoContinueBtn").GetComponent<Button>();
        Info_Open_Btn = GameObject.Find("InfoButton").GetComponent<Button>();
        Info_Close_Btn.gameObject.SetActive( false);
        GlassPanel = GameObject.Find("GlassEffect");



        if (OptionUnlockedTill == 0)
        {
            /*ToDoList_DD.GetComponent<RectTransform>().anchoredPosition = new Vector3(150f, -150f, 0);
            XP_ToShow.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(150f, -150f, 0);
            SidePannel.GetComponent<RectTransform>().anchoredPosition = new Vector3(-600f, 0f, 0);*/
            ObjectivePanel.transform.localScale = new Vector3(1, 1, 1);
            IsTopicUnlocked = false;
        }
        else
        {
            ToDoList_DD.GetComponent<RectTransform>().anchoredPosition = new Vector3(-250f, -150f, 0);
            XP_ToShow.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(-250f, -150f, 0);
            ObjectivePanel.transform.localScale = new Vector3(1, 0, 1);
            IsTopicUnlocked = true;
        }

        Main_XP_ToShow.text = totXp.ToString();

        XP_ToShow.text = "0/" + Total_XP;




        for (int i = 0; i < Sub_Top_ToDoList.Count; i++)
        {

            ToDoList_DD.options.Add(new Dropdown.OptionData() { text = Sub_Top_ToDoList[i] });


            //List<float> TotalXP = Sub_Top_XP.Select(s => float.Parse(s)).ToList();

            //Total_XP += TotalXP[i];

            //XP_ToShow.text = "0/" + Total_XP;

            
            ObjectivePanel.transform.Find("ObjectiveText").GetComponent<Text>().text = Sub_Top_ToDoList[TopicToCheck];

            


        }


        GlassPanel.transform.localScale = Vector3.zero;
        InfoPanel.transform.localScale =Vector3.zero;
       
        Invoke("OpenInfoPanel", 3f);
        

    }

    public void IncrShadowAlpha()
    {
        if (Shadow.material.color.a < 0.3f)
        {
            Shadow.material.color += new Color(0, 0, 0, 0.010f);
        }
        else
        {
            CancelInvoke("IncrShadowAlpha");
        }
    }

    public void DecrShadowAlpha()
    {
        if (Shadow.material.color.a > 0)
        {
            Shadow.material.color -= new Color(0, 0, 0, 0.08f);
        }
        else
        {
            CancelInvoke("DecrShadowAlpha");
        }
    }

    public void ChangeClicked()
    {
        IsClicked = false;
    }

    public void ToggleShellInfo(bool ison)
    {
        IsShellInfo = ison;
        if (!IsShellInfo)
        {
            for (int Child = 0; Child < ShellNames.Length; Child++)
            {
                OrbitNames.transform.GetChild(Child).gameObject.SetActive(false);

            }
        }
        else
        {

            for (int j = 0; j < AtomsNumber.Length; j++)
            {
                if (AtomsNumber[j] != 0)
                    OrbitNames.transform.GetChild(j).gameObject.SetActive(true);
                else
                    OrbitNames.transform.GetChild(j).gameObject.SetActive(false);
            }
        }
        ShellInfo_Toggle.isOn = IsShellInfo;
    }

    public void ToggleSpinElec(bool ison)
    {
        IsSpinElec = ison;
        if (IsSpinElec)
        {



            for (int l = 0; l < AtomsNumber.Length; l++)
            {
                float ToRot = ArrayOfStrings.Length * 1.2f - l;// (10f/AtomDisplayObj.transform.GetChild(l).childCount)+5;
                ToRot = Mathf.Clamp(ToRot, 3, 20);
                iTween.RotateBy(ShellNames[l].gameObject, iTween.Hash("x", 0, "y", 1, "z", 0, "Time", ToRot, "looptype", iTween.LoopType.loop, "easetype", iTween.EaseType.linear));
            }
        }
        else
        {


            for (int l = 0; l < AtomsNumber.Length; l++)
            {
                Destroy(ShellNames[l].GetComponent<iTween>());
            }
        }

        SpinElec_Toggle.isOn = IsSpinElec;
    }

    public void ToggleTransparency(bool ison)
    {
        IsTransparency = ison;
        if (IsTransparency)
        {

            for (int k = 0; k < Elements.Length; k++)
            {
                if (Elements[k].transform.GetChild(0).GetComponent<iTween>())
                {
                    Destroy(Elements[k].transform.GetChild(0).GetComponent<iTween>());
                }

                if (Elements[k] != ElementForTrans)
                    Elements[k].GetComponentInChildren<MeshRenderer>().material = TransparentMaterial;


            }


        }
        else
        {
            for (int k = 0; k < Elements.Length; k++)
            {
                if (Elements[k].transform.GetChild(0).GetComponent<iTween>())
                {
                    Destroy(Elements[k].transform.GetChild(0).GetComponent<iTween>());
                }


                Elements[k].GetComponentInChildren<MeshRenderer>().material = ElementsMaterials[k];


            }

        }
        //print("Toggles");
        Transparency_Toggle.isOn = IsTransparency;
    }

    public string ConverToSuperScript(string ConfigString)
    {
        ArrayOfStrings = ConfigString.Split(' ');
        string ReturnStr = "";
        for (int i = 0; i < ArrayOfStrings.Length; i++)
        {

            string SubString = "";

            if (ArrayOfStrings[i].Substring(ArrayOfStrings[i].Length - 2).Contains("s") || ArrayOfStrings[i].Substring(ArrayOfStrings[i].Length - 2).Contains("p")
                || ArrayOfStrings[i].Substring(ArrayOfStrings[i].Length - 2).Contains("d") || ArrayOfStrings[i].Substring(ArrayOfStrings[i].Length - 2).Contains("f"))
            {
                SubString = ArrayOfStrings[i].Substring(ArrayOfStrings[i].Length - 1);
            }
            else
                SubString = ArrayOfStrings[i].Substring(ArrayOfStrings[i].Length - 2);


            if (i == 0)
                AtomsCount = string.Concat(AtomsCount, SubString);
            else
                AtomsCount = string.Concat(AtomsCount, "," + SubString);

            switch (SubString)
            {
                case "1":
                    SubString = "¹";
                    break;

                case "2":
                    SubString = "²";
                    break;

                case "3":
                    SubString = "³";
                    break;

                case "4":
                    SubString = "⁴";
                    break;

                case "5":
                    SubString = "⁵";
                    break;

                case "6":
                    SubString = "⁶";
                    break;

                case "7":
                    SubString = "⁷";
                    break;

                case "8":
                    SubString = "⁸";
                    break;

                case "9":
                    SubString = "⁹";
                    break;

                case "10":
                    SubString = "¹⁰";
                    break;

                case "11":
                    SubString = "¹¹";
                    break;

                case "12":
                    SubString = "¹²";
                    break;

                case "13":
                    SubString = "¹³";
                    break;

                case "14":
                    SubString = "¹⁴";
                    break;
            }



            if (ArrayOfStrings[i].Substring(ArrayOfStrings[i].Length - 2).Contains("s") || ArrayOfStrings[i].Substring(ArrayOfStrings[i].Length - 2).Contains("p")
               || ArrayOfStrings[i].Substring(ArrayOfStrings[i].Length - 2).Contains("d") || ArrayOfStrings[i].Substring(ArrayOfStrings[i].Length - 2).Contains("f"))
            {
                ArrayOfStrings[i] = ArrayOfStrings[i].Substring(0, ArrayOfStrings[i].Length - 1) + SubString;
            }
            else
                ArrayOfStrings[i] = ArrayOfStrings[i].Substring(0, ArrayOfStrings[i].Length - 2) + SubString;




        }




        for (int j = 0; j < ArrayOfStrings.Length; j++)
        {
            if (j == 0)
                ReturnStr = string.Concat(ReturnStr, ArrayOfStrings[j]);
            else
                ReturnStr = string.Concat(ReturnStr, "," + ArrayOfStrings[j]);
        }

        return ReturnStr;
    }


    public string ConvertToNormal(string ConfigString)
    {
        ArrayOfStrings = ConfigString.Split(' ');
        string ReturnStr = "";
        for (int i = 0; i < ArrayOfStrings.Length; i++)
        {

            string SubString = "";

            if (ArrayOfStrings[i].Substring(ArrayOfStrings[i].Length - 2).Contains("s") || ArrayOfStrings[i].Substring(ArrayOfStrings[i].Length - 2).Contains("p")
                || ArrayOfStrings[i].Substring(ArrayOfStrings[i].Length - 2).Contains("d") || ArrayOfStrings[i].Substring(ArrayOfStrings[i].Length - 2).Contains("f"))
            {
                SubString = ArrayOfStrings[i].Substring(ArrayOfStrings[i].Length - 1);
            }
            else
                SubString = ArrayOfStrings[i].Substring(ArrayOfStrings[i].Length - 2);


            if (i == 0)
                AtomsCount = string.Concat(AtomsCount, SubString);
            else
                AtomsCount = string.Concat(AtomsCount, "," + SubString);

            switch (SubString)
            {
                case "¹":
                    SubString = "1";
                    break;

                case "²":
                    SubString = "2";
                    break;

                case "³":
                    SubString = "3";
                    break;

                case "⁴":
                    SubString = "4";
                    break;

                case "⁵":
                    SubString = "5";
                    break;

                case "⁶":
                    SubString = "6";
                    break;

                case "⁷":
                    SubString = "7";
                    break;

                case "⁸":
                    SubString = "8";
                    break;

                case "⁹":
                    SubString = "9";
                    break;

                case "¹⁰":
                    SubString = "10";
                    break;

                case "¹¹":
                    SubString = "11";
                    break;

                case "¹²":
                    SubString = "12";
                    break;

                case "¹³":
                    SubString = "13";
                    break;

                case "¹⁴":
                    SubString = "14";
                    break;
            }



            if (ArrayOfStrings[i].Substring(ArrayOfStrings[i].Length - 2).Contains("s") || ArrayOfStrings[i].Substring(ArrayOfStrings[i].Length - 2).Contains("p")
               || ArrayOfStrings[i].Substring(ArrayOfStrings[i].Length - 2).Contains("d") || ArrayOfStrings[i].Substring(ArrayOfStrings[i].Length - 2).Contains("f"))
            {
                ArrayOfStrings[i] = ArrayOfStrings[i].Substring(0, ArrayOfStrings[i].Length - 1) + SubString;
            }
            else
                ArrayOfStrings[i] = ArrayOfStrings[i].Substring(0, ArrayOfStrings[i].Length - 2) + SubString;




        }




        for (int j = 0; j < ArrayOfStrings.Length; j++)
        {
            if (j == 0)
                ReturnStr = string.Concat(ReturnStr, ArrayOfStrings[j]);
            else
                ReturnStr = string.Concat(ReturnStr, "," + ArrayOfStrings[j]);
        }

        return ReturnStr;
    }


    public void AssignEventsToObjects()
    {


        for (int i = 0; i < Elements.Length; i++)
        {
            
            int j = i;

            AddListenerToEvents(EventTriggerType.BeginDrag, OnElementBeginDrag, Elements[i]);
            AddListenerToEvents(EventTriggerType.EndDrag, OnElementEndDrag, Elements[i]);

            AddListenerToEvents(EventTriggerType.PointerClick,ShowDetailsOfElement,Elements[i],Elements[j]);

            

            AddListenerToEvents(EventTriggerType.PointerClick, ShowDetailsOfElement, Elements_Sphere[i], Elements_Sphere[j]);
        }




        SidePannelOpen.onClick.AddListener(OpenSidePannel);
        //TopSidePannelOpen.onClick.AddListener(OpenTopSidePannel);
        BackToPT_But.onClick.AddListener(BackToPT);

        ShellInfo_Toggle.onValueChanged.AddListener(ToggleShellInfo);
        SpinElec_Toggle.onValueChanged.AddListener(ToggleSpinElec);
        Transparency_Toggle.onValueChanged.AddListener(ToggleTransparency);

        EventTrigger.Entry PointerDown = new EventTrigger.Entry();
        PointerDown.eventID = EventTriggerType.EndDrag;
        PointerDown.callback.AddListener((data) => { DisableOrbit(); });

        //ScrollBarViewPort.gameObject.GetComponent<EventTrigger>().triggers.Add(PointerDown);

        EventTrigger.Entry PointerUp = new EventTrigger.Entry();
        PointerUp.eventID = EventTriggerType.BeginDrag;
        PointerUp.callback.AddListener((data) => { EnableOrbit(); });

        //ScrollBarViewPort.gameObject.GetComponent<EventTrigger>().triggers.Add(PointerUp);

        /*for (int i = 0; i < SelectionButtons.Length; i++)
        {
            int j = i;
            SelectionButtons[i].onClick.AddListener(() => PropertyToDisplay(j));
            // print(SelectionButtons[i].name);
            old_color = SelectionButtons[0].transform.GetChild(1).GetComponent<Text>().color;
        }*/
        ToDoList_DD.GetComponent<EventTrigger>().triggers.Clear();

        AddListenerToEvents(EventTriggerType.PointerClick, ExploringToDOList_DD, ToDoList_DD.gameObject);
        AddListenerToEvents(EventTriggerType.Select, ToDoList_DDCancel, ToDoList_DD.gameObject);
        Info_Close_Btn.onClick.AddListener(() => CloseInfoPanel(Info_Close_Btn));
        Info_Cntue_Btn.onClick.AddListener(() => CloseInfoPanel(Info_Cntue_Btn));
        Info_Open_Btn.onClick.AddListener(OpenInfoPanel);

        /*AddListenerToEvents(EventTriggerType.BeginDrag, OnElementBeginDrag, GameObject.Find("RawImage"));
        AddListenerToEvents(EventTriggerType.EndDrag, OnElementEndDrag, GameObject.Find("RawImage"));
        AddListenerToEvents(EventTriggerType.PointerClick, BackToPT, GameObject.Find("RawImage"));*/

    }


    public void AddListenerToEvents(EventTriggerType eventType, Action MethodToCall, GameObject TriggerObjToAdd)
    {

        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall());
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }

    public void AddListenerToEvents(EventTriggerType eventType, Action<float> MethodToCall, GameObject TriggerObjToAdd, float p)
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall(p));
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }

    public void AddListenerToEvents(EventTriggerType eventType, Action<GameObject> MethodToCall, GameObject TriggerObjToAdd, GameObject p)
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall(p));
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }
    public void OnElementBeginDrag()
    {
        IsClicked = true;
    }

    public void OnElementEndDrag()
    {
        //print("drag ended");
        IsClicked = false;
    }

    public void EnableOrbit()
    {
        //if (Camera.main.GetComponent<OrbitControls>() != null)
        //{
        //    Camera.main.GetComponent<OrbitControls>().enabled = true;
        //}
        if (!IsAR)
        {
            CamOrbit.DisableInteraction = false;

        }
        else
        {
            ArOrbit.DisableInteration = false;
        }
    }

    public void DisableOrbit()
    {
        //if (Camera.main.GetComponent<OrbitControls>())
        //{
        //    Camera.main.GetComponent<OrbitControls>().enabled = false;
        //}
        if (!IsAR)
        {
            CamOrbit.DisableInteraction = true;

        }
        else
        {
            ArOrbit.DisableInteration = true;
        }
    }

    public void OpenInfoPanel()
    {
        UIZoomIn();
        iTween.ScaleTo(InfoPanel,iTween.Hash("x",1,"y",1,"z",1,"time",0.5f,"easetype",iTween.EaseType.spring));
        iTween.ScaleTo(GlassPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 0.01f, "easetype", iTween.EaseType.spring));

    }

    public void CloseInfoPanel(Button CloseBtn)
    {
        UIZoomOut();
        iTween.ScaleTo(InfoPanel, iTween.Hash("x", 0, "y", 0, "z", 0, "time", 0.2f, "easetype", iTween.EaseType.linear));
        iTween.ScaleTo(GlassPanel, iTween.Hash("x", 0, "y", 0, "z", 0,"delay",0.2f, "time", 0.01f, "easetype", iTween.EaseType.linear));
        if (CloseBtn == Info_Cntue_Btn)
        {
            if (!IsAR)
                CamOrbit.DisableInteraction = false;
            /*if (ActiveBtnCnt == 0)
            {
                for (int i = 0; i < Elements.Length; i++)
                {

                    Vector3 RandomPos = new Vector3(Elements[i].transform.localPosition.x, Elements[i].transform.localPosition.y, UnityEngine.Random.Range(-5f, 0f));
                    Vector3 CurrPos = Elements[i].transform.localPosition;

                    Elements[i].transform.localPosition = RandomPos;

                    iTween.MoveTo(Elements[i], iTween.Hash("position", CurrPos, "delay", 0.7f, "islocal", true, "time", UnityEngine.Random.Range(0.1f, 0.2f), "speed", 2.5f, "easetype", iTween.EaseType.easeInOutSine));


                }
            }*/
            Info_Close_Btn.gameObject.SetActive(true);
            Info_Cntue_Btn.gameObject.SetActive(false);
        }
        IsClicked = false;
    }

    
    public void ToDoList_DDCancel()
    {
        if (ToDoList_DD.transform.Find("Dropdown List") != null)//&& Obj.transform.localScale.y!=1)
        {
            //print("cancelled........");
            //print("list.....cancelled........" + ToDoList_DD.transform.Find("Dropdown List").gameObject);
            ToDoList_DD.transform.Find("Arrow").transform.eulerAngles = new Vector3(0, 0, 0);
            iTween.ScaleTo(ToDoList_DD.transform.Find("Dropdown List").gameObject, iTween.Hash("y", 0, "time", 0.5f, "easetype", iTween.EaseType.spring));
            iTween.ScaleTo(ObjectivePanel, iTween.Hash("y", 1, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine));
            if (SidePannelOpen.transform.eulerAngles.z == 180 && SelectedElement!=null)
            {
                OpenSidePannel();
            }
        }
    }

    public void ExploringToDOList_DD()
    {
        //print("ShowDD.....");
        //ToDoList_DD.transform.Find("Topic_Objective").GetComponent<Text>().text = "";

        GameObject DDList = ToDoList_DD.transform.Find("Dropdown List").gameObject;
        DDList.transform.localScale = new Vector3(1, 0, 1);
        iTween.ScaleTo(DDList, iTween.Hash("y", 1, "time", 0.5f, "easetype", iTween.EaseType.spring));
        ToDoList_DD.transform.Find("Arrow").transform.eulerAngles = new Vector3(0,0,180);
        ObjectivePanel.transform.localScale = new Vector3(1, 0, 1);
        
        //iTween.ScaleTo(ToDoList_DD.transform.Find("Topic_Objective").gameObject, iTween.Hash("y", 0, "time", 0.5f, "easetype", iTween.EaseType.spring));
        if (SidePannelOpen.transform.eulerAngles.z == 0)
        {
            OpenSidePannel();
        }
        
        for (int i = 0; i < ToDoList_DD.options.Count; i++)
        {
            GameObject Child = GameObject.Find("Item " + i + ": " + ToDoList_DD.options[i].text);

            Child.transform.Find("XP_Text").GetComponent<Text>().text = "+" + Sub_Top_XP[i] + "XP";

            if (i < TopicToCheck)
            {
                Child.transform.Find("Item Checkmark").GetComponent<Image>().enabled = true;
                //Child.transform.Find("XP_Text").GetComponent<Text>().color = Color.green / 2;
                //Child.transform.Find("Item Checkmark").GetComponent<Image>().color = Color.green / 2;
                Child.transform.Find("Item Background").GetComponent<Image>().enabled = false;
            }
            else if (i == TopicToCheck)
            {
                Child.transform.Find("Item Background").GetComponent<Image>().enabled = true;// color = ReturnColorFromHex("#a01c65");
                //Child.transform.Find("Item Label").GetComponent<Text>().color = Color.white;
            }


        }
    }


    public Color GetColorFromColorCode(string ColorCode)
    {
        Color ConvColor;
        bool Converted = ColorUtility.TryParseHtmlString(ColorCode, out ConvColor);
        return ConvColor;
    }

    public void ShowDetailsOfElement(GameObject CurrObject)
    {
        
        if (IsClicked)
            return;

        IsUIZooming = true;
        UIZoomOut();
        // PerodicTableSet.transform.localScale = Vector3.Lerp(PerodicTableSet.transform.localScale, new Vector3(0, 0, 0), 0.1f);
        // PerodicTableSet.transform.localScale = new Vector3(0, 0, 0);
        if (ActiveBtnCnt == 0 || ActiveBtnCnt == 1 || ActiveBtnCnt == 2)
        {
            scale_value = 0;
            //InvokeRepeating("PeriodicTableScaleDown", 0.1f, 0.01f);
            Shadow.transform.parent.parent = PerodicTableSet.transform;
            iTween.ScaleTo(PerodicTableSet, iTween.Hash("x", 0, "y", 0, "z", 0,"delay",0.01f, "time", 0.4f, "easetype", iTween.EaseType.linear));
           
        }
        // Debug.Log("Table btn click");
        ElecConfigShell.transform.GetChild(0).gameObject.SetActive(true);
        IonicRadiusSphere.SetActive(false);

        AtomSize.transform.localScale = new Vector3(AtomSize.transform.localScale.x, 0, AtomSize.transform.localScale.z);

        if (SidePannelOpen.transform.eulerAngles.z == 180)
            OpenSidePannel();

        SelectedElement = CurrObject;

        ElecConfigShell.transform.localPosition = CurrObject.transform.position;

        if (ActiveBtnCnt != 4 && ActiveBtnCnt != 5 && ActiveBtnCnt != 6 && ActiveBtnCnt != 7 && ActiveBtnCnt != 8 && ActiveBtnCnt != 9)
        {
            if (ActiveBtnCnt != 3)
                IsClicked = true;
            ElecConfigShell.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);

            iTween.MoveTo(ElecConfigShell, iTween.Hash("x", 0, "y", 1, "z", 0, "islocal", true, "time", 0.5f, "delay", 0f, "easetype", iTween.EaseType.easeInOutSine));

            iTween.ScaleTo(ElecConfigShell, iTween.Hash("x", 2.5f, "y", 2.5f, "z", 2.5f, "islocal", true, "time", 0.5f, "delay", 0.1f, "easetype", iTween.EaseType.linear));
        }
        int ECCnt = 0;

        if (ActiveBtnCnt != 8 && ActiveBtnCnt != 9)
        {
            for (int i = 0; i < Elements_Sphere.Length; i++)
            {
                bool NorCol = ColorUtility.TryParseHtmlString(ElecConfigData_Json[i].COLOR, out ShellColor);
                //print("i........."+i+"......element......."+Elements[i]+"..........currobj........."+CurrObject);
                if (Elements[i] == CurrObject)
                {

                    /*ElementSymbol_UI.text = TableData.dataArray[i].Symbol;
                    ElementName_UI.text = TableData.dataArray[i].Name;
                    AtomicNumber_UI.text = TableData.dataArray[i].Atomic_Number.ToString();
                    AtomicWeight_UI.text = TableData.dataArray[i].Atomic_Weight;*/

                    if (ActiveBtnCnt == 0)//ec
                    {
                        ElementSymbol_UI.text = ElecConfigData_Json[i].SYMBOL;
                        ElementName_UI.text = ElecConfigData_Json[i].ELEMENT;
                        AtomicNumber_UI.text = ElecConfigData_Json[i].SNO;
                        NumOfElectrons_UI.text = ElecConfigData_Json[i].ELECTRONS;
                        ElecConfig_UI.text = ElecConfigData_Json[i].ELECTRONICCONFIGURATION;

                        Material Mat = ElecConfigShell.transform.GetChild(0).GetComponent<MeshRenderer>().material;
                        Mat.color = ShellColor;
                        /*ElementForTrans = Elements[i];
                        if (Mat.GetFloat("_Mode") == 3)
                        {
                            Mat.shader = Shader.Find("Standard");
                            SetupMaterialWithBlendMode(Mat, BlendMode.Opaque);

                        }*/

                        if (!IsTopicUnlocked)
                        {
                            if (ElecConfigData_Json[i].ELEMENT == Sub_Topics[TopicToCheck])
                            {
                                Collect_XP(Elements[i]);
                            }
                            else
                            {

                                ShowInstruction_Panel("This is not " + Sub_Topics[TopicToCheck]);
                                //InstructionPanel.transform.Find("InstructionText").GetComponent<Text>().text = "This is not " + Sub_Topics[TopicToCheck];
                                //iTween.ScaleTo(GlassPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 0.01f, "easetype", iTween.EaseType.spring));
                                //iTween.ScaleTo(InstructionPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 1f, "easetype", iTween.EaseType.spring, "oncomplete", "CloseInstructionPanel", "oncompletetarget", this.gameObject));
                                //ObjectivePanel.transform.localScale = new Vector3(1, 0, 1);
                            }
                        }
                    }

                    if (ActiveBtnCnt == 1)//sap
                    {
                        ElementSymbol_UI.text = ElecConfigData_Json[i].SYMBOL;
                        ElementName_UI.text = ElecConfigData_Json[i].ELEMENT;
                        //AtomicNumber_UI.text = ElecConfigData_Json[i].SNO;
                        NumOfElectrons_UI.text = ElecConfigData_Json[i].ELECTRONS;

                        ElectronsCount = int.Parse(ElecConfigData_Json[i].ELECTRONS);
                        NeutronsCount = int.Parse(ElecConfigData_Json[i].NEUTRONS);

                        NumOfNeutrons_UI.text = (NeutronsCount).ToString();
                        MassNumber_UI.text = ElectronsCount+ "+"+NeutronsCount+" = "+(ElectronsCount + NeutronsCount).ToString();

                        Material Mat = ElecConfigShell.transform.GetChild(0).GetComponent<MeshRenderer>().material;
                        Mat.color = ShellColor;

                        if (!IsTopicUnlocked)
                        {
                            if (ElecConfigData_Json[i].ELEMENT == Sub_Topics[TopicToCheck])
                            {
                                Collect_XP(Elements[i]);
                            }
                            else
                            {
                                ShowInstruction_Panel("This is not " + Sub_Topics[TopicToCheck]);
                                //InstructionPanel.transform.Find("InstructionText").GetComponent<Text>().text = "This is not " + Sub_Topics[TopicToCheck];
                                //iTween.ScaleTo(GlassPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 0.01f, "easetype", iTween.EaseType.spring));
                                //iTween.ScaleTo(InstructionPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 1f, "easetype", iTween.EaseType.spring, "oncomplete", "CloseInstructionPanel", "oncompletetarget", this.gameObject));
                                //ObjectivePanel.transform.localScale = new Vector3(1, 0, 1);
                            }
                        }

                        /*ElementForTrans = Elements[i];
                        if (Mat.GetFloat("_Mode") == 3)
                        {
                            Mat.shader = Shader.Find("Standard");
                            SetupMaterialWithBlendMode(Mat, BlendMode.Opaque);

                        }*/
                    }

                    if (ActiveBtnCnt == 2)//ar
                    {
                        ElementSymbol_UI.text = ElecConfigData_Json[i].SYMBOL;
                        ElementName_UI.text = ElecConfigData_Json[i].ELEMENT;
                        AtomicNumber_UI.text = ElecConfigData_Json[i].SNO;
                        NumOfElectrons_UI.text = ElecConfigData_Json[i].ELECTRONS;
                        ElecConfig_UI.text = ElecConfigData_Json[i].ELECTRONICCONFIGURATION;
                        AtomicRadius_Text.text = ElecConfigData_Json[i].AtomicRadius;
                        AtomSize.SetActive(true);
                        AtomicRadius_Text.gameObject.transform.parent.gameObject.SetActive(true);

                        //ElecConfigShell.transform.GetChild(0).GetComponent<MeshRenderer>().material.color = ShellColor / 2;
                        ElementForTrans = Elements[i];
                        Material Mat = ElecConfigShell.transform.GetChild(0).GetComponent<MeshRenderer>().material;
                        Mat.color = ShellColor / 2;

                        if (!IsTopicUnlocked)
                        {
                            if (ElecConfigData_Json[i].ELEMENT == Sub_Topics[TopicToCheck])
                            {
                                Collect_XP(Elements[i]);
                            }
                            else
                            {
                                ShowInstruction_Panel("This is not " + Sub_Topics[TopicToCheck]);
                                //InstructionPanel.transform.Find("InstructionText").GetComponent<Text>().text = "This is not " + Sub_Topics[TopicToCheck];
                                //iTween.ScaleTo(GlassPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 0.01f, "easetype", iTween.EaseType.spring));
                                //iTween.ScaleTo(InstructionPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 1f, "easetype", iTween.EaseType.spring, "oncomplete", "CloseInstructionPanel", "oncompletetarget", this.gameObject));
                                //ObjectivePanel.transform.localScale = new Vector3(1, 0, 1);
                            }
                        }

                        /*if (Mat.GetFloat("_Mode") != 3)
                        {
                            Mat.shader = Shader.Find("Standard");
                            SetupMaterialWithBlendMode(Mat, BlendMode.Transparent);
                            Mat.color = ShellColor / 2;
                        }*/
                    }
                    if (ActiveBtnCnt == 3)//ir
                    {
                        ElecConfigShell.transform.GetChild(0).gameObject.SetActive(false);
                        IonicRadiusSphere.SetActive(true);
                        if (!ElementForTrans || ElementForTrans != Elements[i])
                        {
                            ElementName_UI.text = ElecConfigData_Json[i].ELEMENT;
                            ElementSymbol_UI.text = ElecConfigData_Json[i].SYMBOL;
                            AtomicNumber_UI.text = ElecConfigData_Json[i].SNO;
                            IonicRadius_UI.text = ElecConfigData_Json[i].IonicRadius;

                            //ValueDisplaySlider.value = 0;
                            //iTween.ValueTo(ValueDisplaySlider.gameObject, iTween.Hash("name", ValueDisplaySlider.name, "from", -1, "to", , "time", 1f, "onupdate", "ChangingSliderVal", "easetype", iTween.EaseType.linear, "onupdatetarget", this.gameObject));

                            string[] IR_Val = ElecConfigData_Json[i].IonicRadius.Split('(');
                            float ToScale = 0;
                            float Scale = 0;

                            if (IR_Val.Length > 1)
                            {
                                String[] IR_Scale = IR_Val[1].Split(')');



                                if (IR_Scale[0] != "" && IR_Scale[0] != "0" && IR_Scale[0] != "-")
                                    Scale = float.Parse(IR_Scale[0]);

                                if (IR_Val[0] != "" && IR_Val[0] != "0" && IR_Val[0] != "-")
                                    ToScale = (Scale + float.Parse(IR_Val[0]));

                                //print("scale......" + Scale);
                                if (Scale >= 0)
                                {
                                    IonicRadiusSphere.transform.GetChild(0).GetComponent<MeshRenderer>().material.color = Color.red;
                                    IonicRadiusSphere.transform.GetComponent<MeshRenderer>().material.color = Color.white;

                                }
                                else
                                {
                                    IonicRadiusSphere.transform.GetChild(0).GetComponent<MeshRenderer>().material.color = Color.white;
                                    IonicRadiusSphere.transform.GetComponent<MeshRenderer>().material.color = Color.blue;
                                }

                                IonicRadiusSphere.transform.localScale = Vector3.one * (15 + (Scale * 3));

                            }
                            else
                            {
                                IonicRadiusSphere.transform.localScale = Vector3.one * (15 + (Scale * 3));
                                IonicRadiusSphere.transform.GetChild(0).GetComponent<MeshRenderer>().material.color = Color.red;
                                IonicRadiusSphere.transform.GetComponent<MeshRenderer>().material.color = Color.white;
                            }

                            AtomSize.SetActive(false);
                            AtomicRadius_Text.gameObject.transform.parent.gameObject.SetActive(false);

                            Elements[i].GetComponentInChildren<MeshRenderer>().material = ElementsMaterials[i];
                            ElementForTrans = Elements[i];

                            if (!IsTopicUnlocked)
                            {
                                if (ElecConfigData_Json[i].ELEMENT == Sub_Topics[TopicToCheck])
                                {
                                    Collect_XP(Elements[i]);
                                }
                                else
                                {
                                    ShowInstruction_Panel("This is not " + Sub_Topics[TopicToCheck]);
                                    //InstructionPanel.transform.Find("InstructionText").GetComponent<Text>().text = "This is not " + Sub_Topics[TopicToCheck];
                                    //iTween.ScaleTo(GlassPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 0.01f, "easetype", iTween.EaseType.spring));
                                    //iTween.ScaleTo(InstructionPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 1f, "easetype", iTween.EaseType.spring, "oncomplete", "CloseInstructionPanel", "oncompletetarget", this.gameObject));
                                    //ObjectivePanel.transform.localScale = new Vector3(1, 0, 1);
                                }
                            }

                        }
                        else
                        {
                            ElementForTrans = null;
                            BackToPT();
                            OpenSidePannel();

                            return;
                        }
                    }

                    if (ActiveBtnCnt == 4)//bp
                    {
                        //(SidePannelOpen.transform.eulerAngles.z == 0 && !Elements[i].GetComponentInChildren<MeshRenderer>().material.name.Contains(TransparentMaterial.name)) ||
                        //(SidePannelOpen.transform.eulerAngles.z != 0 && Elements[i].GetComponentInChildren<MeshRenderer>().material.name.Contains(TransparentMaterial.name))
                        if (!ElementForTrans || ElementForTrans != Elements[i])
                        {
                            ElementSymbol_UI.text = ElecConfigData_Json[i].SYMBOL;
                            ElementName_UI.text = ElecConfigData_Json[i].ELEMENT;
                            AtomicNumber_UI.text = ElecConfigData_Json[i].SNO;
                            BoilingPoint_UI.text = ElecConfigData_Json[i].BoilingPoint;

                            float BP = 0;
                            float.TryParse(ElecConfigData_Json[i].BoilingPoint, out BP);
                            //print("......."+this.gameObject);
                            //ValueDisplaySlider.value =0;
                            iTween.ValueTo(ValueDisplaySlider.gameObject, iTween.Hash("name", ValueDisplaySlider.name, "from", ValueDisplaySlider.value, "to", BP / 6000f, "time", 1f, "onupdate", "ChangingSliderVal", "easetype", iTween.EaseType.linear, "onupdatetarget", this.gameObject));
                            //ValueDisplaySlider.value =BP / 6000;
                            // SliderValue_Text.text = BP.ToString();

                            if (!IsTopicUnlocked)
                            {
                                if (ElecConfigData_Json[i].ELEMENT == Sub_Topics[TopicToCheck])
                                {
                                    Collect_XP(Elements[i]);
                                }
                                else
                                {
                                    ShowInstruction_Panel("This is not " + Sub_Topics[TopicToCheck]);
                                    //InstructionPanel.transform.Find("InstructionText").GetComponent<Text>().text = "This is not " + Sub_Topics[TopicToCheck];
                                    //iTween.ScaleTo(GlassPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 0.01f, "easetype", iTween.EaseType.spring));
                                    //iTween.ScaleTo(InstructionPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 1f, "easetype", iTween.EaseType.spring, "oncomplete", "CloseInstructionPanel", "oncompletetarget", this.gameObject));
                                    //ObjectivePanel.transform.localScale = new Vector3(1, 0, 1);
                                }
                            }

                            AtomSize.SetActive(false);
                            AtomicRadius_Text.gameObject.transform.parent.gameObject.SetActive(false);

                            Elements[i].GetComponentInChildren<MeshRenderer>().material = ElementsMaterials[i];
                            ElementForTrans = Elements[i];
                        }
                        else //if (SidePannelOpen.transform.eulerAngles.z != 0 && !Elements[i].GetComponentInChildren<MeshRenderer>().material.name.Contains(TransparentMaterial.name))
                        {
                            BackToPT();
                            OpenSidePannel();
                            ElementForTrans = null;
                            return;
                        }
                    }

                    if (ActiveBtnCnt == 5)//mp
                    {
                        if (!ElementForTrans || ElementForTrans != Elements[i])
                        {
                            ElementSymbol_UI.text = ElecConfigData_Json[i].SYMBOL;
                            ElementName_UI.text = ElecConfigData_Json[i].ELEMENT;
                            AtomicNumber_UI.text = ElecConfigData_Json[i].SNO;
                            MeltingPoint_UI.text = ElecConfigData_Json[i].MeltingPoint;

                            float MP = 0;
                            float.TryParse(ElecConfigData_Json[i].MeltingPoint, out MP);

                            //ValueDisplaySlider.value = 0;
                            iTween.ValueTo(ValueDisplaySlider.gameObject, iTween.Hash("name", ValueDisplaySlider.name, "from", ValueDisplaySlider.value, "to", MP / 6000f, "time", 1f, "onupdate", "ChangingSliderVal", "easetype", iTween.EaseType.linear, "onupdatetarget", this.gameObject));
                            // SliderValue_Text.text = MP.ToString();


                            if (!IsTopicUnlocked)
                            {
                                if (ElecConfigData_Json[i].ELEMENT == Sub_Topics[TopicToCheck])
                                {
                                    Collect_XP(Elements[i]);
                                }
                                else
                                {
                                    ShowInstruction_Panel("This is not " + Sub_Topics[TopicToCheck]);
                                    //InstructionPanel.transform.Find("InstructionText").GetComponent<Text>().text = "This is not " + Sub_Topics[TopicToCheck];
                                    //iTween.ScaleTo(GlassPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 0.01f, "easetype", iTween.EaseType.spring));
                                    //iTween.ScaleTo(InstructionPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 1f, "easetype", iTween.EaseType.spring, "oncomplete", "CloseInstructionPanel", "oncompletetarget", this.gameObject));
                                    //ObjectivePanel.transform.localScale = new Vector3(1, 0, 1);
                                }
                            }

                            AtomSize.SetActive(false);
                            AtomicRadius_Text.gameObject.transform.parent.gameObject.SetActive(false);

                            Elements[i].GetComponentInChildren<MeshRenderer>().material = ElementsMaterials[i];
                            ElementForTrans = Elements[i];
                        }
                        else
                        {
                            BackToPT();
                            OpenSidePannel();
                            ElementForTrans = null;
                            return;
                        }
                    }

                    if (ActiveBtnCnt == 6)//density
                    {
                        if (!ElementForTrans || ElementForTrans != Elements[i])
                        {
                            ElementSymbol_UI.text = ElecConfigData_Json[i].SYMBOL;
                            ElementName_UI.text = ElecConfigData_Json[i].ELEMENT;
                            AtomicNumber_UI.text = ElecConfigData_Json[i].SNO;
                            Density_UI.text = ElecConfigData_Json[i].Density;
                            float Density = 0;
                            float.TryParse(ElecConfigData_Json[i].Density, out Density);
                            iTween.ValueTo(ValueDisplaySlider.gameObject, iTween.Hash("name", ValueDisplaySlider.name, "from", ValueDisplaySlider.value, "to", Density, "time", 1f, "onupdate", "ChangingSliderVal", "easetype", iTween.EaseType.linear, "onupdatetarget", this.gameObject));
                            AtomSize.SetActive(false);
                            AtomicRadius_Text.gameObject.transform.parent.gameObject.SetActive(false);

                            if (!IsTopicUnlocked)
                            {
                                if (ElecConfigData_Json[i].ELEMENT == Sub_Topics[TopicToCheck])
                                {
                                    Collect_XP(Elements[i]);
                                }
                                else
                                {
                                    ShowInstruction_Panel("This is not " + Sub_Topics[TopicToCheck]);
                                    //InstructionPanel.transform.Find("InstructionText").GetComponent<Text>().text = "This is not " + Sub_Topics[TopicToCheck];
                                    //iTween.ScaleTo(GlassPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 0.01f, "easetype", iTween.EaseType.spring));
                                    //iTween.ScaleTo(InstructionPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 1f, "easetype", iTween.EaseType.spring, "oncomplete", "CloseInstructionPanel", "oncompletetarget", this.gameObject));
                                    //ObjectivePanel.transform.localScale = new Vector3(1, 0, 1);
                                }
                            }

                            Elements[i].GetComponentInChildren<MeshRenderer>().material = ElementsMaterials[i];
                            ElementForTrans = Elements[i];
                        }
                        else
                        {
                            BackToPT();
                            OpenSidePannel();
                            ElementForTrans = null;
                            return;
                        }
                    }

                    if (ActiveBtnCnt == 7)//Electro-Negativity
                    {
                        if (!ElementForTrans || ElementForTrans != Elements[i])
                        {
                            ElementSymbol_UI.text = ElecConfigData_Json[i].SYMBOL;
                            ElementName_UI.text = ElecConfigData_Json[i].ELEMENT;
                            AtomicNumber_UI.text = ElecConfigData_Json[i].SNO;
                            ElectroNegativity_UI.text = ElecConfigData_Json[i].ElectroNegativity;

                            float ENeg = 0;
                            float.TryParse(ElecConfigData_Json[i].ElectroNegativity, out ENeg);
                            iTween.ValueTo(ValueDisplaySlider.gameObject, iTween.Hash("name", ValueDisplaySlider.name, "from", ValueDisplaySlider.value, "to", ENeg, "time", 1f, "onupdate", "ChangingSliderVal", "easetype", iTween.EaseType.linear, "onupdatetarget", this.gameObject));

                            if (!IsTopicUnlocked)
                            {
                                if (ElecConfigData_Json[i].ELEMENT == Sub_Topics[TopicToCheck])
                                {
                                    Collect_XP(Elements[i]);
                                }
                                else
                                {
                                    ShowInstruction_Panel("This is not " + Sub_Topics[TopicToCheck]);
                                    //InstructionPanel.transform.Find("InstructionText").GetComponent<Text>().text = "This is not " + Sub_Topics[TopicToCheck];
                                    //iTween.ScaleTo(GlassPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 0.01f, "easetype", iTween.EaseType.spring));
                                    //iTween.ScaleTo(InstructionPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 1f, "easetype", iTween.EaseType.spring, "oncomplete", "CloseInstructionPanel", "oncompletetarget", this.gameObject));
                                    //ObjectivePanel.transform.localScale = new Vector3(1, 0, 1);
                                }
                            }

                            AtomSize.SetActive(false);
                            AtomicRadius_Text.gameObject.transform.parent.gameObject.SetActive(false);

                            Elements[i].GetComponentInChildren<MeshRenderer>().material = ElementsMaterials[i];
                            ElementForTrans = Elements[i];
                        }
                        else
                        {
                            BackToPT();
                            OpenSidePannel();
                            ElementForTrans = null;
                            return;
                        }
                    }

                    ElecConfig = ElecConfigData_Json[i].ELECTRONICCONFIGURATIONATOMS;

                    ElectronsCount = int.Parse(ElecConfigData_Json[i].ELECTRONS);
                    NeutronsCount = int.Parse(ElecConfigData_Json[i].NEUTRONS);

                    if (!IsAR)
                    {

                        CamOrbit.cameraRotUp = -20;
                        CamOrbit.cameraRotSide = 0;


                        /*if (ActiveBtnCnt < 3)
                        {
                            if (i <= 9)
                            {
                                CamOrbit.distance = 2.5f;
                            }
                            else if (i > 9 && i <= 17)
                            {
                                CamOrbit.distance = 2.7f;
                            }
                            else if (i > 17 && i <= 54)
                            {
                                CamOrbit.distance = 3.5f;
                            }
                            else if (i > 54 && i <= 86)
                            {
                                CamOrbit.distance = 4f;
                            }
                            else if (i > 86 && i <= 118)
                            {
                                CamOrbit.distance = 5f;
                            }
                        }
                        else if (ActiveBtnCnt == 3)
                        {
                            CamOrbit.distance = 3f;
                        }*/
                    }

                }
                else
                {
                    //if (ActiveBtnCnt != 3 && ActiveBtnCnt != 4)
                    if (IsTransparency)
                    {
                        //print("hi..."+Elements[i].transform.GetChild(0).GetComponent<iTween>());
                        if (Elements[i].transform.GetChild(0).GetComponent<iTween>())
                        {
                            Destroy(Elements[i].transform.GetChild(0).GetComponent<iTween>());
                        }

                        if (ActiveBtnCnt == 4 || ActiveBtnCnt == 5)
                            ElementsMaterials[i].color = ReffColor[i];

                        Elements[i].GetComponentInChildren<MeshRenderer>().material = TransparentMaterial;
                        //print("InTransparency");
                    }
                }
            }
        }
        else
        {


            for (int i = 0; i < Elements_Sphere.Length; i++)
            {
                //print("inelse......." + CurrObject + "............" + Elements_Sphere[i]);
                if (Elements_Sphere[i] == CurrObject)
                {
                    ElecConfig = ElecConfigData_Json[i].ELECTRONICCONFIGURATIONATOMS;
                    if (ActiveBtnCnt == 8)//OxidationStates
                    {
                        if (!ElementForTrans || ElementForTrans != Elements_Sphere[i])
                        {
                            ElementName_UI.text = ElecConfigData_Json[i].ELEMENT;
                            AtomicNumber_UI.text = ElecConfigData_Json[i].SNO;
                            ElementSymbol_UI.text = ElecConfigData_Json[i].SYMBOL;

                            AtomSize.SetActive(false);
                            AtomicRadius_Text.gameObject.transform.parent.gameObject.SetActive(false);

                            GameObject ElementOxi = OxidationStates.transform.Find("OS_" + (i + 1)).gameObject;

                            ElementOxi.SetActive(true);
                            //print("oxi......."+ ElementOxi);
                            for (int child = 0; child < ElementOxi.transform.childCount; child++)
                            {
                                string Name = ElementOxi.transform.GetChild(child).name;
                                string Num = Name.Substring(Name.Length - 1);
                                float Multiple = 0;
                                float.TryParse(Num, out Multiple);
                                if (Multiple == 8)
                                    Multiple = 7.99f;

                                if (Name.Contains("P"))
                                {

                                    ElementOxi.transform.GetChild(child).localEulerAngles = new Vector3(0, -22.5f * Multiple, 0);
                                }
                                else if (Name.Contains("N"))
                                    ElementOxi.transform.GetChild(child).localEulerAngles = new Vector3(0, 22.5f * Multiple, 0);

                                if (ElementOxi.transform.GetChild(child).eulerAngles.y != 0)
                                {
                                    iTween.RotateTo(ElementOxi.transform.GetChild(child).gameObject, iTween.Hash("y", 0, "delay", 0.3f * child, "islocal", true, "time", 1f, "easetype", iTween.EaseType.easeInOutBack, "oncomplete", "DisplayText", "oncompleteparams", ElementOxi.transform.GetChild(child).gameObject, "oncompletetarget", this.gameObject));
                                }
                            }

                            if (!IsTopicUnlocked)
                            {
                                if (ElecConfigData_Json[i].ELEMENT == Sub_Topics[TopicToCheck])
                                {
                                    Collect_XP(Elements[i]);
                                }
                                else
                                {
                                    ShowInstruction_Panel("This is not " + Sub_Topics[TopicToCheck]);
                                    //InstructionPanel.transform.Find("InstructionText").GetComponent<Text>().text = "This is not " + Sub_Topics[TopicToCheck];
                                    //iTween.ScaleTo(GlassPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 0.01f, "easetype", iTween.EaseType.spring));
                                    //iTween.ScaleTo(InstructionPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 1f, "easetype", iTween.EaseType.spring, "oncomplete", "CloseInstructionPanel", "oncompletetarget", this.gameObject));
                                    //ObjectivePanel.transform.localScale = new Vector3(1, 0, 1);
                                }
                            }

                            ElementForTrans = Elements_Sphere[i];
                            //CamOrbit.target.transform.localPosition = ElementForTrans.transform.position;

                            if (!IsAR)
                            {
                                CamOrbit.distance = CamOrbit.zoomMin;
                                //CamOrbit.Target_X_Value = ElementForTrans.transform.position.x;
                                //CamOrbit.Target_Y_Value = ElementForTrans.transform.position.y;
                                //CamOrbit.Target_Z_Value = ElementForTrans.transform.position.z;
                                CamOrbit.ChangeTarget(ElementForTrans.transform);
                                CamOrbit.Zoom_X_Speed = 3f;
                            }
                            //iTween.MoveTo(CamOrbit.target.gameObject, iTween.Hash("x", ElementForTrans.transform.position.x, "y", ElementForTrans.transform.position.y, "z", ElementForTrans.transform.position.z, "islocal", true, "time", 1f, "delay", 0f, "easetype", iTween.EaseType.linear));//, "oncomplete", "ZoomCamera", "oncompleteparams",1f, "oncompletetarget", this.gameObject));
                        }
                        else
                        {
                            if (!IsAR)
                            {
                                CamOrbit.Target_Y_Value = 0.4f;
                                CamOrbit.ZoomMax_Y_Value = 0.4f;
                            }
                            //iTween.MoveTo(CamOrbit.target.gameObject, iTween.Hash("x", 0, "y", 0.4f, "z", 0, "islocal", true, "time", 0.5f, "delay", 0f, "easetype", iTween.EaseType.linear));
                            BackToPT();
                            OpenSidePannel();

                            ElementForTrans = null;
                            return;
                        }
                    }

                    if (ActiveBtnCnt == 9)//IonizationPotential
                    {
                        if (!ElementForTrans || ElementForTrans != Elements_Sphere[i])
                        {
                            ElementName_UI.text = ElecConfigData_Json[i].ELEMENT;
                            AtomicNumber_UI.text = ElecConfigData_Json[i].SNO;
                            ElementSymbol_UI.text = ElecConfigData_Json[i].SYMBOL;

                            AtomSize.SetActive(false);
                            AtomicRadius_Text.gameObject.transform.parent.gameObject.SetActive(false);

                            GameObject IonizationPot = IonizationPotential.transform.Find("IP_" + (i + 1)).gameObject;

                            IonizationPot.SetActive(true);

                            float IP1 = 0.01f, IP2 = 0.01f, IP3 = 0.01f;

                            
                            float.TryParse(ElecConfigData_Json[i].IP1,out IP1);
                            float.TryParse(ElecConfigData_Json[i].IP2, out IP2);
                            float.TryParse(ElecConfigData_Json[i].IP3, out IP3);

                            IP1_Text.text =IP1.ToString("0.00");
                            IP2_Text.text = IP2.ToString("0.00");
                            IP3_Text.text = IP3.ToString("0.00");

                            if (IP1 == 0)
                                IP1 = 0.01f;
                            if (IP2 == 0)
                                IP2 = 0.01f;
                            if (IP3 == 0)
                                IP3 = 0.01f;

                            iTween.ScaleTo(IonizationPot.transform.GetChild(0).gameObject, iTween.Hash("y", IP1/30f, "delay", 1.1f, "islocal", true, "time", UnityEngine.Random.Range(0.1f, 0.2f), "speed", 2.5f, "easetype", iTween.EaseType.easeInOutSine));
                            iTween.ScaleTo(IonizationPot.transform.GetChild(1).gameObject, iTween.Hash("y", IP2/30f, "delay", 1.3f, "islocal", true, "time", UnityEngine.Random.Range(0.1f, 0.2f), "speed", 2.5f, "easetype", iTween.EaseType.easeInOutSine));
                            iTween.ScaleTo(IonizationPot.transform.GetChild(2).gameObject, iTween.Hash("y", IP3/30f, "delay", 1.7f, "islocal", true, "time", UnityEngine.Random.Range(0.1f, 0.2f), "speed", 2.5f, "easetype", iTween.EaseType.easeInOutSine));


                            if (!IsTopicUnlocked)
                            {
                                if (ElecConfigData_Json[i].ELEMENT == Sub_Topics[TopicToCheck])
                                {
                                    Collect_XP(Elements[i]);
                                }
                                else
                                {
                                    ShowInstruction_Panel("This is not " + Sub_Topics[TopicToCheck]);
                                    //InstructionPanel.transform.Find("InstructionText").GetComponent<Text>().text = "This is not " + Sub_Topics[TopicToCheck];
                                    //iTween.ScaleTo(GlassPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 0.01f, "easetype", iTween.EaseType.spring));
                                    //iTween.ScaleTo(InstructionPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 1f, "easetype", iTween.EaseType.spring, "oncomplete", "CloseInstructionPanel", "oncompletetarget", this.gameObject));
                                    //ObjectivePanel.transform.localScale = new Vector3(1, 0, 1);
                                }
                            }

                            ElementForTrans = Elements_Sphere[i];
                            if (!IsAR)
                            {
                                CamOrbit.distance = CamOrbit.zoomMin;
                                //CamOrbit.Target_X_Value = ElementForTrans.transform.position.x;
                                //CamOrbit.Target_Y_Value = ElementForTrans.transform.position.y;
                                //CamOrbit.Target_Z_Value = ElementForTrans.transform.position.z;
                                CamOrbit.ChangeTarget(ElementForTrans.transform);
                                CamOrbit.Zoom_X_Speed = 3f;
                            }
                            //iTween.MoveTo(CamOrbit.target.gameObject, iTween.Hash("x", ElementForTrans.transform.position.x, "y", ElementForTrans.transform.position.y, "z", ElementForTrans.transform.position.z, "islocal", true, "time", 1f, "delay", 0f, "easetype", iTween.EaseType.linear));//, "oncomplete", "ZoomCamera", "oncompleteparams",1f, "oncompletetarget", this.gameObject));
                        }
                        else
                        {
                            if (!IsAR)
                            {
                                CamOrbit.Target_Y_Value = 0.4f;
                                CamOrbit.ZoomMax_Y_Value = 0.4f;
                            }
                            //iTween.MoveTo(CamOrbit.target.gameObject, iTween.Hash("x", 0, "y", 0.5f, "z", 0, "islocal", true, "time", 0.5f, "delay", 0f, "easetype", iTween.EaseType.linear));
                            BackToPT();
                            OpenSidePannel();
                            ElementForTrans = null;
                            return;
                        }
                    }

                }

            }
        }
        //ElecConfigShell.transform.GetChild(0).GetComponent<MeshRenderer>().material.color = ShellColor;

        //print("config....."+ElecConfig);
        ArrayOfStrings = ElecConfig.Split(' ').ToArray();




        for (int i = ShellNames.Length - 1; i > 0; i--)
        {
            //print("i......" +i+".........."+ ArrayOfStrings[ArrayOfStrings.Length - 1].Substring(0,2));
            if (ArrayOfStrings[ArrayOfStrings.Length - 1].Substring(0, 2) == ShellNames[i].name)
            {
                ECCnt = i + 1;

                break;
            }
        }

        float scale = (4.33f * ECCnt) + 10.33f;
        scale = Mathf.Clamp(scale, 14, 92);


        ElecConfigShell.transform.GetChild(0).localScale = Vector3.one * scale;

        AtomSize.transform.GetChild(0).localScale = new Vector3(scale / 10000, AtomSize.transform.GetChild(0).localScale.y, scale / 10000);

        if (ActiveBtnCnt != 3 && ActiveBtnCnt != 4 && ActiveBtnCnt != 5 && ActiveBtnCnt != 6 && ActiveBtnCnt != 7 && ActiveBtnCnt != 8 && ActiveBtnCnt != 9)
        {

            Invoke("ArrangeObjectsInCircle", 0.65f);
        }
    }


    public void ZoomCamera(float Value)
    {
        CamOrbit.distance = Value;
    }

    public void DisplayText(GameObject Obj)
    {
        //print("Obj..."+ Obj.GetComponentInChildren<TextMeshPro>());
        Obj.GetComponentInChildren<TextMeshPro>().enabled = true;
    }


    void ChangingSliderVal(float ValueToApply)
    {
        ValueDisplaySlider.value = ValueToApply;

        if (ActiveBtnCnt == 4 || ActiveBtnCnt == 5)
             SliderValue_Text.text = (ValueToApply * 6000).ToString("F2");
        else
             SliderValue_Text.text = (ValueToApply).ToString("F2");
        //print("Slider....."+ Val);
    }

    public void BackToPT()
    {
        //print("sel....."+SelectedElement+".....click........"+IsClicked);
        if (!SelectedElement)
            return;

        Atoms = new List<GameObject>();
        // PerodicTableSet.transform.localScale = new Vector3(1, 1, 1);
        scale_value = 0;
        Shadow.transform.parent.parent = PerodicTableSet.transform.parent.parent;
        Shadow.transform.parent.localPosition = new Vector3(0,-0.6f,0f);  

        iTween.ScaleTo(PerodicTableSet,iTween.Hash("x",1,"y",1,"z",1,"time",1f,"easetype",iTween.EaseType.linear));

        if (PT_CurrentTopic== PeriodicTableTopic.vid473_ec || PT_CurrentTopic == PeriodicTableTopic.vid472_ar)
        {
            ElementSymbol_UI.text = "-";
            ElementName_UI.text = "Element Name";
            AtomicNumber_UI.text = "-";
            NumOfElectrons_UI.text = "-";
            ElecConfig_UI.text = "-";
            
        }

        if (PT_CurrentTopic == PeriodicTableTopic.vid465_am)
        {
            ElementSymbol_UI.text = "-";
            ElementName_UI.text = "Element Name";
            MassNumber_UI.text = "-";
            NumOfElectrons_UI.text = "-";
            NumOfNeutrons_UI.text = "-";
        }

        
        if (PT_CurrentTopic == PeriodicTableTopic.vid214_ir)
        {
            ElementSymbol_UI.text = "-";
            ElementName_UI.text = "Element Name";
            IonicRadius_UI.text = "-";
            ValueDisplaySlider.value = -1;
             SliderValue_Text.text = "0";
        }

        if (PT_CurrentTopic == PeriodicTableTopic.vid591_bp)
        {
            ElementSymbol_UI.text = "-";
            ElementName_UI.text = "Element Name";
            BoilingPoint_UI.text = "-";
            ValueDisplaySlider.value = -1;
             SliderValue_Text.text = "0";
        }

        if (PT_CurrentTopic == PeriodicTableTopic.vid591_mp)
        {
            ElementSymbol_UI.text = "-";
            ElementName_UI.text = "Element Name";
            MeltingPoint_UI.text = "-";
            ValueDisplaySlider.value = -1;
             SliderValue_Text.text = "0";
        }

        if (PT_CurrentTopic == PeriodicTableTopic.den)
        {
            ElementSymbol_UI.text = "-";
            ElementName_UI.text = "Element Name";
            Density_UI.text = "-";
            ValueDisplaySlider.value = -1;
             SliderValue_Text.text = "0";
        }

        if (PT_CurrentTopic == PeriodicTableTopic.vid217_ip)
        {
            IP1_Text.text = "";
            IP2_Text.text = "";
            IP3_Text.text = "";
        }
        //InvokeRepeating("PeriodicTableScaleUp", 0.1f, 0.01f);
        print("backtopt..."+ActiveBtnCnt);
        if (!IsAR)
        {

            

            if (ActiveBtnCnt >= 8)
            {
                CamOrbit.cameraRotUp = -50;
                CamOrbit.cameraRotSide = 0;

                CamOrbit.zoomMax = 3f;
                CamOrbit.zoomMin = 0.5f;
                CamOrbit.zoomStart = 1.5f;
                CamOrbit.distance = CamOrbit.zoomStart;
                CamOrbit.Target_Y_Value = 0.4f;
                CamOrbit.Target_X_Value = 0;
                CamOrbit.Target_Z_Value = 0;
                CamOrbit.ChangeTarget(null);
                iTween.MoveTo(CamOrbit.target.gameObject, iTween.Hash("x", 0, "y", 0.4f, "z", 0, "islocal", true, "time", 0.5f, "delay", 0f, "easetype", iTween.EaseType.linear));
            }
            else
            {
                CamOrbit.distance = CamOrbit.zoomStart;
                CamOrbit.zoomStart = 3f;
                CamOrbit.zoomMin = 1.5f;
                CamOrbit.zoomMax = 5.5f;
                CamOrbit.Target_Y_Value = 0.7f;
                CamOrbit.Target_X_Value = 0;
                CamOrbit.Target_Z_Value = 0;
                CamOrbit.ChangeTarget(null);

                CamOrbit.cameraRotUp = -20;
                CamOrbit.cameraRotSide = 0;
            }

        }

        for (int i = 0; i < Elements.Length; i++)
        {
            Elements[i].GetComponentInChildren<MeshRenderer>().material = ElementsMaterials[i];
        }



        for (int Child = 0; Child < ShellNames.Length; Child++)
        {
            for (int i = 0; i < ShellNames[Child].transform.childCount; i++)
            {
                Destroy(ShellNames[Child].transform.GetChild(i).gameObject);
            }
            Destroy(ShellNames[Child].GetComponent<iTween>());
            OrbitNames.transform.GetChild(Child).gameObject.SetActive(false);
        }

        DestroyProtonsAndNeutrons();

        for (int i = 0; i < ShellNames.Length; i++)
        {
            ShellNames[i].GetComponent<LineRenderer>().positionCount = 0;
        }

        if (SelectedElement)
            iTween.MoveTo(ElecConfigShell, iTween.Hash("x", SelectedElement.transform.position.x, "y", SelectedElement.transform.position.y, "z", SelectedElement.transform.position.z, "islocal", true, "time", 0.5f, "delay", 0f, "easetype", iTween.EaseType.easeInOutSine));

        iTween.ScaleTo(ElecConfigShell, iTween.Hash("x", 0, "y", 0, "z", 0, "islocal", true, "time", 0.5f, "delay", 0.1f, "easetype", iTween.EaseType.linear));


        OpenSidePannel();

        SelectedElement = null;

        Invoke("ChangeClicked", 1f);
    }

    

    public void OpenSidePannel()
    {
        if (SidePannelOpen.transform.eulerAngles.z == 180)
        {
            //iTween.MoveAdd(SidePannel, iTween.Hash("x", -400f, "time",0.5f,"islocal",true, "easetype", iTween.EaseType.linear));
            //iTween.MoveAdd(SidePannelOpen.gameObject, iTween.Hash("x", -185f, "islocal", true, "time", 0.5f, "easetype", iTween.EaseType.linear));
            //SidePannelOpen.transform.eulerAngles = new Vector3(0,0,180);
            SidePannel.GetComponent<Animator>().Play("PannelForward");
        }
        else
        {
            //iTween.MoveAdd(SidePannel, iTween.Hash("x", 400f, "time", 0.5f, "islocal", true, "easetype", iTween.EaseType.linear));
            //iTween.MoveAdd(SidePannelOpen.gameObject, iTween.Hash("x", -185f, "islocal", true, "time", 0.5f, "easetype", iTween.EaseType.linear));
            //SidePannelOpen.transform.eulerAngles = new Vector3(0, 0, 0);
            SidePannel.GetComponent<Animator>().Play("PannelBackward");
        }
    }
    /*public void OpenTopSidePannel()
    {
        //print("Clicked......"+ PannelOpen.transform.eulerAngles.y);
        if (TopSidePannelOpen.gameObject.transform.localEulerAngles.z == 0)
        {
            TopSidePannel.GetComponent<Animator>().Play("TopButtonOpen");
            BtnselName.gameObject.SetActive(false);
            // TopSidePannelOpen.gameObject.transform.localEulerAngles = new Vector3(0, 0, 180);
        }
        else
        {
            TopSidePannel.GetComponent<Animator>().Play("TopButtonClose");
            // TopSidePannelOpen.gameObject.transform.localEulerAngles = new Vector3(0, 0, 0);
        }
    }*/

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Escape))
            SceneManager.LoadScene(0);

        if (ShellNames[0].GetComponent<LineRenderer>().widthMultiplier != transform.parent.localScale.x)
        {
            for (int i = 0; i < ShellNames.Length; i++)
            {
                ShellNames[i].GetComponent<LineRenderer>().widthMultiplier = transform.parent.localScale.x;
            }
        }
        if (!IsAR)
        {

            if (CamOrbit.distance < CamOrbit.zoomStart && !IsUIZooming)
            {
                UIZoomIn();
                IsUIZooming = true;
            }
            else if (CamOrbit.distance > CamOrbit.zoomStart && IsUIZooming)
            {
                UIZoomOut();
                IsUIZooming = false;
            }
        }

        /*if (CamOrbit.Zoom_bool && ActiveBtnCnt<8)
        {
            CamOrbit.zoomMin = CamOrbit.cameraRotUp * -0.01125f + 1.15f;
        }*/
    }

    public int SuperScriptToInt(string SubString)
    {
        switch (SubString)
        {
            case "¹":
                SubString = "1";
                break;

            case "²":
                SubString = "2";
                break;

            case "³":
                SubString = "3";
                break;

            case "⁴":
                SubString = "4";
                break;

            case "⁵":
                SubString = "5";
                break;

            case "⁶":
                SubString = "6";
                break;

            case "⁷":
                SubString = "7";
                break;

            case "⁸":
                SubString = "8";
                break;

            case "⁹":
                SubString = "9";
                break;

            case "¹⁰":
                SubString = "10";
                break;

            case "¹¹":
                SubString = "11";
                break;

            case "¹²":
                SubString = "12";
                break;

            case "¹³":
                SubString = "13";
                break;

            case "¹⁴":
                SubString = "14";
                break;
        }

        return int.Parse(SubString);
    }

    void ArrangeObjectsInCircle()
    {
        ArrayOfStrings = ElecConfig.Split(' ').ToArray();

        AtomsNumber = new int[ShellNames.Length];



        //for (int shell = 0; shell< ShellNames.Length; shell++)
        int shell = 0, StringArray = 0;
        while (shell < ShellNames.Length)
        {
            //print("while......" + shell+".....length......"+ StringArray);
            if (ArrayOfStrings[StringArray].Contains(ShellNames[shell].transform.name.Substring(1)))
            {
                string SuperScript = ArrayOfStrings[StringArray].Substring(2);
                //print("SuperScript......"+ SuperScript);
                AtomsNumber[shell] = int.Parse(SuperScript);


                shell++;
                if (StringArray < ArrayOfStrings.Length - 1)
                {
                    StringArray++;

                }
                else
                {
                    break;
                }
            }
            else
            {
                AtomsNumber[shell] = 0;

                shell++;
            }
        }

        float CircleRadius = 0;

        for (int j = 0; j < AtomsNumber.Length; j++)
        {
            List<GameObject> AtomsArray = new List<GameObject>();
            GameObject Atom;

            if (IsShellInfo)
            {
                if (AtomsNumber[j] != 0)
                    OrbitNames.transform.GetChild(j).gameObject.SetActive(true);
                else
                    OrbitNames.transform.GetChild(j).gameObject.SetActive(false);
            }

            float AtomSize = 0.045f;


            for (int k = 0; k < AtomsNumber[j]; k++)
            {
                Atom = Instantiate(AtomInCircle, Vector3.zero, Quaternion.identity, ShellNames[j].transform);
                //Atom.transform.localScale =Vector3.one*AtomSize;
                //Atom.transform.parent = ShellNames[j].transform;
                Atom.transform.GetChild(0).gameObject.AddComponent<SmoothLook>();
                MeshRenderer Halo = Atom.transform.GetChild(0).GetChild(0).gameObject.GetComponent<MeshRenderer>();


                AtomsArray.Add(Atom);
                Atoms.Add(Atom);


                if (ActiveBtnCnt == 2)
                {
                    Material Mat = Atom.GetComponent<MeshRenderer>().material;
                    if (Mat.GetFloat("_Mode") != 3)
                    {
                        Mat.shader = Shader.Find("Standard");
                        SetupMaterialWithBlendMode(Mat, BlendMode.Transparent);

                        Mat.color = Mat.color / 2;
                        Halo.material.color = Halo.material.color / 2;
                    }
                }
                else
                {
                    Material Mat = Atom.GetComponent<MeshRenderer>().material;
                    if (Mat.GetFloat("_Mode") == 3)
                    {
                        Mat.shader = Shader.Find("Standard");
                        SetupMaterialWithBlendMode(Mat, BlendMode.Cutout);
                        Mat.color = Mat.color * 2;
                        Halo.material.color = Halo.material.color * 2;
                    }
                }

            }


            for (int i = 0; i < AtomsArray.Count; i++)
            {
                int a = (i) * 360 / AtomsArray.Count;
                int Cnt = 0;

                Cnt = j;

                if (Cnt >= 1 && Cnt < 3)
                    Cnt++;
                else if (Cnt >= 3 && Cnt < 6)
                    Cnt += 2;
                else if (Cnt >= 6 && Cnt < 10)
                    Cnt += 3;
                else if (Cnt >= 10 && Cnt < 14)
                    Cnt += 4;
                else if (Cnt >= 14 && Cnt < 18)
                    Cnt += 5;
                else if (Cnt >= 18)
                    Cnt += 6;

                CircleRadius = ((Cnt * 2f / (AtomsNumber.Length + 7)) + 0.24f);

                Vector3 pos = RandomCircle(Vector3.zero, CircleRadius, a);

                AtomsArray[i].transform.localPosition = pos;

                DrawLineRenderCircle(ShellNames[j], CircleRadius, AtomsArray.Count);

                //print("cirrad...."+CircleRadius);
                //float TRTime = ArrayOfStrings.Length * 1.2f - j;
                //TRTime = Mathf.Clamp(TRTime, 3, 20);

                //AtomsArray[i].transform.GetComponentInChildren<TrailRenderer>().time = (TRTime) / AtomDisplayObj.transform.GetChild(j).childCount;
            }


        }

        AtomSize.transform.localScale = new Vector3(AtomSize.transform.localScale.x, CircleRadius, AtomSize.transform.localScale.z);

        if (IsSpinElec)
        {
            for (int l = 0; l < AtomsNumber.Length; l++)
            {
                float ToRot = ArrayOfStrings.Length * 1.2f - l;// (10f/AtomDisplayObj.transform.GetChild(l).childCount)+5;
                ToRot = Mathf.Clamp(ToRot, 3, 20);
                iTween.RotateBy(ShellNames[l].gameObject, iTween.Hash("x", 0, "y", 1, "z", 0, "Time", ToRot, "looptype", iTween.LoopType.loop, "easetype", iTween.EaseType.linear));
            }

        }

        //Invoke("CreateProtonsAndNeutrons",0.1f);
        CreateProtonsAndNeutrons();


    }

    public void PropertyToDisplay(int SelectedBtn)
    {
        //if(old_sel_btn != 0)
        /*SelectionButtons[old_sel_btn].transform.GetChild(1).GetComponent<Text>().color = old_color;

        for (int i = 0; i < SelectionButtons.Length; i++)
        {
            SelectionButtons[i].transform.GetChild(0).GetComponent<Image>().color = Color.white;
        }
        old_color = SelectionButtons[SelectedBtn].transform.GetChild(1).GetComponent<Text>().color;
        old_sel_btn = SelectedBtn;
        BtnselName.transform.GetChild(0).GetComponent<Image>().color = SelectionButtons[SelectedBtn].transform.GetChild(1).GetComponent<Text>().color;
        BtnselName.transform.GetChild(1).GetComponent<Text>().text = SelectionButtons[SelectedBtn].transform.GetChild(1).GetComponent<Text>().text;
        SelectionButtons[SelectedBtn].transform.GetChild(0).GetComponent<Image>().color = SelectionButtons[SelectedBtn].transform.GetChild(1).GetComponent<Text>().color;
        SelectionButtons[SelectedBtn].transform.GetChild(1).GetComponent<Text>().color = Color.white;*/




        if (SelectedBtn == 0)
        {
            //Target.transform.position = new Vector3(0, 0.7f, 0);
            ShowEC(SelectedBtn);
            // CamOrbit.zoomMin = 3;

        }

        if (SelectedBtn == 1)
        {
            //Target.transform.position = new Vector3(0, 0.7f, 0);
            ShowAtomicNumber(SelectedBtn);
            //  CamOrbit.zoomMin = 3;
        }

        if (SelectedBtn == 2)
        {
            //Target.transform.position = new Vector3(0, 0.7f, 0);
            ShowAtomicRadius(SelectedBtn);
            //  CamOrbit.zoomMin = 3;
        }

        if (SelectedBtn == 3)
        {
            //Target.transform.position = new Vector3(0, 0.7f, 0);
            ShowIonicRadius(SelectedBtn);
            IsTransparency = true;
            //   CamOrbit.zoomMin = 2.3f;
        }

        if (SelectedBtn == 4)
        {
            //Target.transform.position = new Vector3(0, 0.7f, 0);
            ShowBoilingPoint(SelectedBtn);
            IsTransparency = true;
            //  CamOrbit.zoomMin = 2.3f;
        }

        if (SelectedBtn == 5)
        {
            //Target.transform.position = new Vector3(0, 0.7f, 0);
            ShowMeltingPoint(SelectedBtn);
            IsTransparency = true;
            // CamOrbit.zoomMin = 2.3f;
        }

        if (SelectedBtn == 6)
        {
            //Target.transform.position = new Vector3(0, 0.7f, 0);
            ShowDensity(SelectedBtn);
            IsTransparency = true;
            //  CamOrbit.zoomMin = 2.3f;
        }

        if (SelectedBtn == 7)
        {
            //Target.transform.position = new Vector3(0, 0.7f, 0);
            ShowElectroNegativity(SelectedBtn);
            IsTransparency = true;
            //  CamOrbit.zoomMin = 2.3f;
        }

        if (SelectedBtn == 8)
        {
            //Target.transform.position = new Vector3(0, 0.4f, 0);
            ShowOxidationStates(SelectedBtn);
            IsTransparency = true;
            //  CamOrbit.zoomMin = 3f;
        }

        if (SelectedBtn == 9)
        {
            //Target.transform.position = new Vector3(0, 0.4f, 0);
            ShowIonizationPotential(SelectedBtn);
            IsTransparency = true;
            //  CamOrbit.zoomMin = 3;
        }
        if (!IsAR)
        {
            if (SelectedBtn >= 8)
            {
                CamOrbit.cameraRotUpStart = -50;
                CamOrbit.cameraRotSideStart = 0;

                CamOrbit.cameraRotUp = -50;
                CamOrbit.cameraRotSide = 0;



                CamOrbit.zoomMax = 3f;
                CamOrbit.zoomMin = 0.5f;
                CamOrbit.zoomStart = 1.5f;
                CamOrbit.distance = CamOrbit.zoomStart;
                CamOrbit.Target_Y_Value = 0.4f;
                CamOrbit.Target_X_Value = 0;
                CamOrbit.Target_Z_Value = 0;
            }
            else
            {
                CamOrbit.cameraRotUp = -20;
                CamOrbit.cameraRotSide = 0;

                CamOrbit.distance = CamOrbit.zoomStart;
                CamOrbit.Target_Y_Value = 0.7f;
                CamOrbit.Target_X_Value = 0;
                CamOrbit.Target_Z_Value = 0;
            }
        }
        ActiveBtnCnt = SelectedBtn;

        //ToggleShellInfo(false);
        //ToggleSpinElec(false);
        //ToggleTransparency(false);
        ShellInfo_Toggle.isOn = IsShellInfo;
        SpinElec_Toggle.isOn = IsSpinElec;
        //Transparency_Toggle.isOn = true;
        
        ElementForTrans = null;
        IsClicked = true;
        IsUIZooming = true;
        //SidePannelOpen.gameObject.SetActive(false);
        //TopSidePannel.GetComponent<Animator>().Play("TopButtonClose");
        if (SidePannelOpen.transform.eulerAngles.z != 0)
            SidePannel.GetComponent<Animator>().Play("PannelBackward");

        /*if (SidePannelOpen.transform.eulerAngles.z != 180)
        {
            iTween.MoveTo(SidePannel,iTween.Hash("x",-400f,"time",1,"easetype",iTween.EaseType.linear));
        }*/
    }

    


    public void CreateProtonsAndNeutrons()
    {
        var r = 0.1f;
        Vector3 ReffPos = ProtonsAndNeutrons.transform.localPosition;
        for (int i = 0; i < ElectronsCount; i++)
        {
            var spawned = GetProtonPooled() as GameObject;

            var x = UnityEngine.Random.Range(ReffPos.x - 0.03f, ReffPos.x + 0.03f);
            var y = UnityEngine.Random.Range(ReffPos.y - 0.03f, ReffPos.y + 0.03f);
            var z = UnityEngine.Random.Range(ReffPos.z - 0.03f, ReffPos.z + 0.03f);

            var vec = new Vector3(x, y, z).normalized * r / 10;
            //spawned.transform.parent = ProtonsAndNeutrons.transform;
            spawned.SetActive(true);
            spawned.transform.localPosition = vec;


        }

        for (int i = 0; i < NeutronsCount; i++)
        {
            var spawned = GetNeutronPooled() as GameObject;

            var x = UnityEngine.Random.Range(ReffPos.x - 0.03f, ReffPos.x + 0.03f);
            var y = UnityEngine.Random.Range(ReffPos.y - 0.03f, ReffPos.y + 0.03f);
            var z = UnityEngine.Random.Range(ReffPos.z - 0.03f, ReffPos.z + 0.03f);

            var vec = new Vector3(x, y, z).normalized * r / 10;
            //spawned.transform.parent = ProtonsAndNeutrons.transform;

            spawned.SetActive(true);
            spawned.transform.localPosition = vec;

        }

        Invoke("RigidbodySleep", 0.1f);
    }

    public void RigidbodySleep()
    {
        for (int i = 0; i < NeutronsPooled.Count; i++)
        {
            NeutronsPooled[i].GetComponent<SphereCollider>().enabled = false;
            NeutronsPooled[i].GetComponent<Rigidbody>().Sleep();

        }

        for (int i = 0; i < ProtonsPooled.Count; i++)
        {
            ProtonsPooled[i].GetComponent<SphereCollider>().enabled = false;
            ProtonsPooled[i].GetComponent<Rigidbody>().Sleep();
        }
    }

    public GameObject GetProtonPooled()
    {
        for (int i = DummyCount_P; i < ProtonsPooled.Count; i++)
        {
            if (!ProtonsPooled[i].gameObject.activeInHierarchy)
            {
                if (ActiveBtnCnt == 2)
                {
                    Material Mat = ProtonsPooled[i].GetComponent<MeshRenderer>().material;
                    if (Mat.GetFloat("_Mode") != 3)
                    {
                        Mat.shader = Shader.Find("Standard");
                        SetupMaterialWithBlendMode(Mat, BlendMode.Transparent);
                        Mat.color = Mat.color / 2;
                    }
                }
                else
                {
                    Material Mat = ProtonsPooled[i].GetComponent<MeshRenderer>().material;
                    if (Mat.GetFloat("_Mode") == 3)
                    {
                        Mat.shader = Shader.Find("Standard");
                        SetupMaterialWithBlendMode(Mat, BlendMode.Opaque);
                        Mat.color = Mat.color * 2;
                    }
                }

                DummyCount_P++;
                return ProtonsPooled[i];
            }
        }

        if (CanPool)
        {
            GameObject proton = Instantiate(Proton) as GameObject;
            ProtonsPooled.Add(proton);
            proton.transform.parent = ProtonsAndNeutrons.transform;
            proton.SetActive(false);

            return proton;
        }

        return null;
    }

    public GameObject GetNeutronPooled()
    {

        for (int i = DummyCount_N; i < NeutronsPooled.Count; i++)
        {
            if (!NeutronsPooled[i].gameObject.activeInHierarchy)
            {
                if (ActiveBtnCnt == 2)
                {
                    Material Mat = NeutronsPooled[i].GetComponent<MeshRenderer>().material;
                    if (Mat.GetFloat("_Mode") != 3)
                    {
                        Mat.shader = Shader.Find("Standard");
                        SetupMaterialWithBlendMode(Mat, BlendMode.Transparent);
                        Mat.color = Mat.color / 2;
                    }
                }
                else
                {
                    Material Mat = NeutronsPooled[i].GetComponent<MeshRenderer>().material;
                    if (Mat.GetFloat("_Mode") == 3)
                    {
                        Mat.shader = Shader.Find("Standard");
                        SetupMaterialWithBlendMode(Mat, BlendMode.Opaque);
                        Mat.color = Mat.color * 2;
                    }
                }

                DummyCount_N++;
                return NeutronsPooled[i];
            }
        }

        if (CanPool)
        {
            GameObject Netr = Instantiate(Neutron) as GameObject;
            NeutronsPooled.Add(Netr);
            Netr.transform.parent = ProtonsAndNeutrons.transform;
            Netr.SetActive(false);

            return Netr;
        }

        return null;
    }

    public void DestroyProtonsAndNeutrons()
    {
        for (int i = 0; i < NeutronsPooled.Count; i++)
        {
            NeutronsPooled[i].SetActive(false);
            NeutronsPooled[i].GetComponent<Rigidbody>().WakeUp();
            NeutronsPooled[i].GetComponent<SphereCollider>().enabled = true;
        }

        for (int i = 0; i < ProtonsPooled.Count; i++)
        {
            ProtonsPooled[i].SetActive(false);
            ProtonsPooled[i].GetComponent<Rigidbody>().WakeUp();
            ProtonsPooled[i].GetComponent<SphereCollider>().enabled = true;
        }
        DummyCount_N = 0;
        DummyCount_P = 0;
    }

    public enum BlendMode
    {
        Opaque,
        Cutout,
        Fade,   // Old school alpha-blending mode, fresnel does not affect amount of transparency
        Transparent // Physically plausible transparency mode, implemented as alpha pre-multiply
    }

    public static void SetupMaterialWithBlendMode(Material material, BlendMode blendMode)
    {
        switch (blendMode)
        {
            case BlendMode.Opaque:
                //material.SetOverrideTag("RenderType", "");
                material.SetFloat("_Mode", 0);
                material.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
                material.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.Zero);
                material.SetInt("_ZWrite", 1);
                material.DisableKeyword("_ALPHATEST_ON");
                material.DisableKeyword("_ALPHABLEND_ON");
                material.DisableKeyword("_ALPHAPREMULTIPLY_ON");
                material.renderQueue = -1;
                break;
            case BlendMode.Cutout:
                //material.SetOverrideTag("RenderType", "TransparentCutout");
                material.SetFloat("_Mode", 1);
                material.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
                material.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.Zero);
                material.SetInt("_ZWrite", 1);
                material.EnableKeyword("_ALPHATEST_ON");
                material.DisableKeyword("_ALPHABLEND_ON");
                material.DisableKeyword("_ALPHAPREMULTIPLY_ON");
                material.renderQueue = (int)UnityEngine.Rendering.RenderQueue.AlphaTest;
                break;
            case BlendMode.Fade:
                //material.SetOverrideTag("RenderType", "Transparent");
                material.SetFloat("_Mode", 2);
                material.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
                material.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
                material.SetInt("_ZWrite", 0);
                material.DisableKeyword("_ALPHATEST_ON");
                material.EnableKeyword("_ALPHABLEND_ON");
                material.DisableKeyword("_ALPHAPREMULTIPLY_ON");
                material.renderQueue = (int)UnityEngine.Rendering.RenderQueue.Transparent;
                break;
            case BlendMode.Transparent:
                //material.SetOverrideTag("RenderType", "Transparent");
                material.SetFloat("_Mode", 3);
                material.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
                material.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
                material.SetInt("_ZWrite", 0);
                material.DisableKeyword("_ALPHATEST_ON");
                material.DisableKeyword("_ALPHABLEND_ON");
                material.EnableKeyword("_ALPHAPREMULTIPLY_ON");
                material.renderQueue = (int)UnityEngine.Rendering.RenderQueue.Transparent;
                break;
        }
    }

    Vector3 RandomCircle(Vector3 center, float radius, int a)
    {

        float ang = a;
        Vector3 pos;
        pos.x = center.x + radius * Mathf.Sin(ang * Mathf.Deg2Rad);
        pos.y = center.y;
        pos.z = center.z + radius * Mathf.Cos(ang * Mathf.Deg2Rad);
        return pos;
    }

    public void DrawLineRenderCircle(GameObject LineRend, float CirRadius, int NumOfAtoms, bool Ref)
    {
        int segments = 50 / NumOfAtoms;
        var line = LineRend.GetComponent<LineRenderer>();
        float CirLength = 360 / NumOfAtoms;




        var pointCount = (segments * NumOfAtoms) + 1; // add extra point to make startpoint and endpoint the same to close the circle

        line.positionCount = pointCount;

        print("segments......" + segments + "......atoms..........." + NumOfAtoms);

        var points = new Vector3[pointCount];
        int AtomMultiplier = 1;

        for (int i = 0; i < pointCount; i++)
        {
            var rad = Mathf.Deg2Rad * (i * CirLength / segments);
            points[i] = new Vector3(Mathf.Sin(rad) * CirRadius, 0, Mathf.Cos(rad) * CirRadius);

            if (i % segments == 1)
            {
                if (AtomMultiplier < NumOfAtoms)
                    AtomMultiplier++;
            }

        }

        line.SetPositions(points);
    }


    public void DrawLineRenderCircle(GameObject LineRend, float CirRadius, int NumOfAtoms)
    {
        int segments = 360;
        var line = LineRend.GetComponent<LineRenderer>();
        float CirLength = 360;




        var pointCount = (segments) + 1; // add extra point to make startpoint and endpoint the same to close the circle

        line.positionCount = pointCount;

        //print("segments......" + segments + "......atoms..........." + NumOfAtoms);

        var points = new Vector3[pointCount];

        for (int i = 0; i < pointCount; i++)
        {
            var rad = Mathf.Deg2Rad * (i * CirLength / segments);
            points[i] = new Vector3(Mathf.Sin(rad) * CirRadius, 0, Mathf.Cos(rad) * CirRadius);

        }

        line.SetPositions(points);

        AnimationCurve WidthCurve = new AnimationCurve();

        for (int j = 0; j < NumOfAtoms; j++)
        {
            Keyframe KeyRef = new Keyframe();

            float KeyTime = (j) / (float)NumOfAtoms;

            //print("key0........"+Key+".....j........."+j);
            KeyRef.time = KeyTime;
            KeyRef.value = 0.007f;

            KeyRef.inTangent = 0f;
            KeyRef.outTangent = 0f;

            WidthCurve.AddKey(KeyRef);// Key, 0.007f);

            float Key2 = (j + 1) / (float)NumOfAtoms;

            KeyRef.time = Key2 - 0.01f;
            KeyRef.value = 0.001f;

            KeyRef.inTangent = 0f;
            KeyRef.outTangent = 0f;

            //print("key2........" + Key2 + ".....j........." + j);
            WidthCurve.AddKey(KeyRef);//(Key2-0.01f, 0.001f);
        }




        line.widthCurve = WidthCurve;
    }


    public void ShowEC(int Cnt)
    {
        for (int i = 0; i < UIPanels.Length; i++)
        {
            UIPanels[i].SetActive(false);
        }

        UIPanels[Cnt].SetActive(true);
        AtomSize.SetActive(false);
        AtomicRadius_Text.gameObject.transform.parent.gameObject.SetActive(false);
        ValueDisplaySlider.gameObject.SetActive(false);

        ElementSymbol_UI = GameObject.Find("ElementSymbol_Value").GetComponent<Text>();
        ElementName_UI = GameObject.Find("ElementName_Value").GetComponent<Text>();
        AtomicNumber_UI = GameObject.Find("AtomicNumber_Value").GetComponent<Text>();
        NumOfElectrons_UI = GameObject.Find("NumOfElectrons_Value").GetComponent<Text>();
        ElecConfig_UI = GameObject.Find("ElectronicConfig_Value").GetComponent<Text>();

        Learninggoal_Txt = GameObject.Find("LGListText").GetComponent<Text>();
        Instruction_Txt = GameObject.Find("IListText").GetComponent<Text>();
        Assumption_Txt = GameObject.Find("AListText").GetComponent<Text>();
        Heading_Txt = GameObject.Find("InfoHeadingText").GetComponent<Text>();

        SidePannel = GameObject.Find("SidePanel");
        SidePannelOpen = GameObject.Find("PannelOpenButton").GetComponent<Button>();
        BackToPT_But = GameObject.Find("GoToPT").GetComponent<Button>();

        ShellInfo_Toggle = GameObject.Find("ShowShell_Toggle").GetComponent<Toggle>();
        SpinElec_Toggle = GameObject.Find("SpinElec_Toggle").GetComponent<Toggle>();
        Transparency_Toggle = GameObject.Find("Transparency_Toggle").GetComponent<Toggle>();

        SidePannelOpen.onClick.RemoveAllListeners();
        BackToPT_But.onClick.RemoveAllListeners();
        ShellInfo_Toggle.onValueChanged.RemoveAllListeners();
        SpinElec_Toggle.onValueChanged.RemoveAllListeners();
        Transparency_Toggle.onValueChanged.RemoveAllListeners();
        Transparency_Toggle.isOn = true;

        SidePannelOpen.onClick.AddListener(OpenSidePannel);

        BackToPT_But.onClick.AddListener(BackToPT);

        ShellInfo_Toggle.onValueChanged.AddListener(ToggleShellInfo);
        SpinElec_Toggle.onValueChanged.AddListener(ToggleSpinElec);
        Transparency_Toggle.onValueChanged.AddListener(ToggleTransparency);

        ToggleShellInfo(true);
        BackToPT();
        PerodicTableSet.SetActive(true);
        PerodicTableSet2.SetActive(false);

        for (int i = 0; i < Elements.Length; i++)
        {
            /*if (Elements[i].GetComponent<iTween>())
                Destroy(Elements[i].GetComponent<iTween>());

            if (Elements[i].transform.GetChild(0).GetComponent<iTween>())
                Destroy(Elements[i].transform.GetChild(0).GetComponent<iTween>());
                
             if (Elements[i].transform.localScale.z <= 0)
            {
                TextMeshPro[] TextChilds = Elements[i].GetComponentsInChildren<TextMeshPro>();
                for (int child = 0; child < TextChilds.Length; child++)
                {
                    TextChilds[child].gameObject.transform.localPosition = new Vector3(TextChilds[child].gameObject.transform.localPosition.x, TextChilds[child].gameObject.transform.localPosition.y, -0.098f);
                }
            }*/

            Vector3 RandomPos = new Vector3(Elements[i].transform.localPosition.x, Elements[i].transform.localPosition.y, UnityEngine.Random.Range(-5f, 0f));
            Vector3 CurrPos = new Vector3(Elements[i].transform.localPosition.x, Elements[i].transform.localPosition.y, 0.14f);

            

            Elements[i].transform.localPosition = RandomPos;
            Elements[i].transform.localScale = Vector3.one * 2;

            iTween.MoveTo(Elements[i], iTween.Hash("position", CurrPos, "delay", 0.7f, "islocal", true, "time", UnityEngine.Random.Range(0.1f, 0.2f), "speed", 2.5f, "easetype", iTween.EaseType.easeInOutSine));
            
            ElementsMaterials[i].color = OriginalColors[i];
            //Elements[i].GetComponent<EventTrigger>().enabled = true;
        }

        ElementSymbol_UI.text = "-";
        ElementName_UI.text = "Element Name";
        AtomicNumber_UI.text = "-";
        NumOfElectrons_UI.text = "-";
        ElecConfig_UI.text = "-";
        Heading_Txt.text = LearningGoals[0];
        Learninggoal_Txt.text =LearningGoals[1];
        Instruction_Txt.text = LearningGoals[2];
        Assumption_Txt.text = LearningGoals[3];
    }


    public void ShowAtomicNumber(int Cnt)
    {
        for (int i = 0; i < UIPanels.Length; i++)
        {
            UIPanels[i].SetActive(false);
        }

        UIPanels[Cnt].SetActive(true);
        AtomSize.SetActive(false);
        AtomicRadius_Text.gameObject.transform.parent.gameObject.SetActive(false);
        ValueDisplaySlider.gameObject.SetActive(false);

        ElementSymbol_UI = GameObject.Find("ElementSymbol_Value").GetComponent<Text>();
        ElementName_UI = GameObject.Find("ElementName_Value").GetComponent<Text>();
        //AtomicNumber_UI = GameObject.Find("AtomicNumber_Value").GetComponent<Text>();
        NumOfElectrons_UI = GameObject.Find("NumOfProtons_Value").GetComponent<Text>();
        //ElecConfig_UI = GameObject.Find("ElectronicConfig_Value").GetComponent<Text>();

        MassNumber_UI = GameObject.Find("MassNumber_Value").GetComponent<Text>();
        NumOfNeutrons_UI = GameObject.Find("NumOfNeutrons_Value").GetComponent<Text>();

        Learninggoal_Txt = GameObject.Find("LGListText").GetComponent<Text>();
        Instruction_Txt = GameObject.Find("IListText").GetComponent<Text>();
        Assumption_Txt = GameObject.Find("AListText").GetComponent<Text>();
        Heading_Txt = GameObject.Find("InfoHeadingText").GetComponent<Text>();

        SidePannel = GameObject.Find("SidePanel");
        SidePannelOpen = GameObject.Find("PannelOpenButton").GetComponent<Button>();
        BackToPT_But = GameObject.Find("GoToPT").GetComponent<Button>();

        ShellInfo_Toggle = GameObject.Find("ShowShell_Toggle").GetComponent<Toggle>();
        SpinElec_Toggle = GameObject.Find("SpinElec_Toggle").GetComponent<Toggle>();
        Transparency_Toggle = GameObject.Find("Transparency_Toggle").GetComponent<Toggle>();

        SidePannelOpen.onClick.RemoveAllListeners();
        BackToPT_But.onClick.RemoveAllListeners();
        ShellInfo_Toggle.onValueChanged.RemoveAllListeners();
        SpinElec_Toggle.onValueChanged.RemoveAllListeners();
        Transparency_Toggle.onValueChanged.RemoveAllListeners();
        Transparency_Toggle.isOn = true;

        SidePannelOpen.onClick.AddListener(OpenSidePannel);

        BackToPT_But.onClick.AddListener(BackToPT);

        ShellInfo_Toggle.onValueChanged.AddListener(ToggleShellInfo);
        SpinElec_Toggle.onValueChanged.AddListener(ToggleSpinElec);
        Transparency_Toggle.onValueChanged.AddListener(ToggleTransparency);

        ToggleShellInfo(true);
        BackToPT();
        PerodicTableSet.SetActive(true);
        PerodicTableSet2.SetActive(false);

        for (int i = 0; i < Elements.Length; i++)
        {
            /*if (Elements[i].GetComponent<iTween>())
                Destroy(Elements[i].GetComponent<iTween>());

            if (Elements[i].transform.GetChild(0).GetComponent<iTween>())
                Destroy(Elements[i].transform.GetChild(0).GetComponent<iTween>());
                
            if (Elements[i].transform.localScale.z <= 0)
            {
                TextMeshPro[] TextChilds = Elements[i].GetComponentsInChildren<TextMeshPro>();
                for (int child = 0; child < TextChilds.Length; child++)
                {
                    TextChilds[child].gameObject.transform.localPosition = new Vector3(TextChilds[child].gameObject.transform.localPosition.x, TextChilds[child].gameObject.transform.localPosition.y, -0.098f);
                }
            }
             */

            Vector3 RandomPos = new Vector3(Elements[i].transform.localPosition.x, Elements[i].transform.localPosition.y, UnityEngine.Random.Range(-5f, 0f));
            Vector3 CurrPos = new Vector3(Elements[i].transform.localPosition.x, Elements[i].transform.localPosition.y, 0.14f);

            

            Elements[i].transform.localPosition = RandomPos;
            Elements[i].transform.localScale = Vector3.one * 2;
            iTween.MoveTo(Elements[i], iTween.Hash("position", CurrPos, "delay", 0.7f, "islocal", true, "time", UnityEngine.Random.Range(0.1f, 0.2f), "speed", 2.5f, "easetype", iTween.EaseType.easeInOutSine));
            //Elements[i].GetComponent<EventTrigger>().enabled = true;
            ElementsMaterials[i].color = OriginalColors[i];
        }

        ElementSymbol_UI.text = "-";
        ElementName_UI.text = "Element Name";
        NumOfElectrons_UI.text = "-";
        NumOfNeutrons_UI.text = "-";
        MassNumber_UI.text = "-";
        Heading_Txt.text = LearningGoals[0];
        Learninggoal_Txt.text = LearningGoals[1];
        Instruction_Txt.text = LearningGoals[2];
        Assumption_Txt.text = LearningGoals[3];
    }

    public void ShowAtomicRadius(int Cnt)
    {
        for (int i = 0; i < UIPanels.Length; i++)
        {
            UIPanels[i].SetActive(false);
        }

        UIPanels[Cnt].SetActive(true);
        ValueDisplaySlider.gameObject.SetActive(false);


        ElementSymbol_UI = GameObject.Find("ElementSymbol_Value").GetComponent<Text>();
        ElementName_UI = GameObject.Find("ElementName_Value").GetComponent<Text>();
        AtomicNumber_UI = GameObject.Find("AtomicNumber_Value").GetComponent<Text>();
        NumOfElectrons_UI = GameObject.Find("NumOfElectrons_Value").GetComponent<Text>();
        ElecConfig_UI = GameObject.Find("ElectronicConfig_Value").GetComponent<Text>();

        Learninggoal_Txt = GameObject.Find("LGListText").GetComponent<Text>();
        Instruction_Txt = GameObject.Find("IListText").GetComponent<Text>();
        Assumption_Txt = GameObject.Find("AListText").GetComponent<Text>();
        Heading_Txt = GameObject.Find("InfoHeadingText").GetComponent<Text>();

        SidePannel = GameObject.Find("SidePanel");
        SidePannelOpen = GameObject.Find("PannelOpenButton").GetComponent<Button>();
        BackToPT_But = GameObject.Find("GoToPT").GetComponent<Button>();

        ShellInfo_Toggle = GameObject.Find("ShowShell_Toggle").GetComponent<Toggle>();
        SpinElec_Toggle = GameObject.Find("SpinElec_Toggle").GetComponent<Toggle>();
        Transparency_Toggle = GameObject.Find("Transparency_Toggle").GetComponent<Toggle>();

        SidePannelOpen.onClick.RemoveAllListeners();
        BackToPT_But.onClick.RemoveAllListeners();
        ShellInfo_Toggle.onValueChanged.RemoveAllListeners();
        SpinElec_Toggle.onValueChanged.RemoveAllListeners();
        Transparency_Toggle.onValueChanged.RemoveAllListeners();
        Transparency_Toggle.isOn = true;

        SidePannelOpen.onClick.AddListener(OpenSidePannel);

        BackToPT_But.onClick.AddListener(BackToPT);

        ShellInfo_Toggle.onValueChanged.AddListener(ToggleShellInfo);
        SpinElec_Toggle.onValueChanged.AddListener(ToggleSpinElec);
        Transparency_Toggle.onValueChanged.AddListener(ToggleTransparency);

        ToggleShellInfo(true);
        BackToPT();
        PerodicTableSet.SetActive(true);
        PerodicTableSet2.SetActive(false);

        for (int i = 0; i < Elements.Length; i++)
        {

            float ToScale = 0;

            /*if (Elements[i].GetComponent<iTween>())
                Destroy(Elements[i].GetComponent<iTween>());

            if (Elements[i].transform.GetChild(0).GetComponent<iTween>())
                Destroy(Elements[i].transform.GetChild(0).GetComponent<iTween>());

                if (Elements[i].transform.localScale.z <= 0)
            {
                TextMeshPro[] TextChilds = Elements[i].GetComponentsInChildren<TextMeshPro>();
                for (int child = 0; child < TextChilds.Length; child++)
                {
                    TextChilds[child].gameObject.transform.localPosition = new Vector3(TextChilds[child].gameObject.transform.localPosition.x, TextChilds[child].gameObject.transform.localPosition.y, -0.098f);
                }
            }*/

            if (ElecConfigData_Json[i].AtomicRadius != "" && ElecConfigData_Json[i].AtomicRadius != "0" && ElecConfigData_Json[i].AtomicRadius != "-")
                ToScale = float.Parse(ElecConfigData_Json[i].AtomicRadius);

            Vector3 RandomScale = new Vector3(Elements[i].transform.localScale.x, Elements[i].transform.localScale.y, (0.1f) + ToScale);
            Vector3 CurrPos = new Vector3(Elements[i].transform.localPosition.x, Elements[i].transform.localPosition.y, 0.14f);

            

            Elements[i].transform.localPosition = CurrPos;

            Elements[i].transform.localScale = new Vector3(Elements[i].transform.localScale.x, Elements[i].transform.localScale.y, 0.1f);

            iTween.ScaleTo(Elements[i], iTween.Hash("scale", RandomScale, "delay", 0.7f, "islocal", true, "time", UnityEngine.Random.Range(0.1f, 0.2f), "speed", 2.5f, "easetype", iTween.EaseType.easeInOutSine));

            ElementsMaterials[i].color = OriginalColors[i];
            //Elements[i].GetComponent<EventTrigger>().enabled = true;
        }

        ElementSymbol_UI.text = "-";
        ElementName_UI.text = "Element Name";
        AtomicNumber_UI.text = "-";
        NumOfElectrons_UI.text = "-";
        ElecConfig_UI.text = "-";
        Heading_Txt.text = LearningGoals[0];
        Learninggoal_Txt.text = LearningGoals[1];
        Instruction_Txt.text = LearningGoals[2];
        Assumption_Txt.text = LearningGoals[3];

    }

    public void ShowIonicRadius(int Cnt)
    {
        // InvokeRepeating("PeriodicTableScaleUp", 0.1f, 0.01f);
        for (int i = 0; i < UIPanels.Length; i++)
        {
            UIPanels[i].SetActive(false);
        }

        UIPanels[Cnt].SetActive(true);
        ValueDisplaySlider.gameObject.SetActive(false);



        ElementSymbol_UI = GameObject.Find("ElementSymbol_Value").GetComponent<Text>();
        ElementName_UI = GameObject.Find("ElementName_Value").GetComponent<Text>();
        AtomicNumber_UI = GameObject.Find("AtomicNumber_Value").GetComponent<Text>();
        IonicRadius_UI = GameObject.Find("IonicRadius_Value").GetComponent<Text>();

        Learninggoal_Txt = GameObject.Find("LGListText").GetComponent<Text>();
        Instruction_Txt = GameObject.Find("IListText").GetComponent<Text>();
        Assumption_Txt = GameObject.Find("AListText").GetComponent<Text>();
        Heading_Txt = GameObject.Find("InfoHeadingText").GetComponent<Text>();

        SidePannel = GameObject.Find("SidePanel");
        SidePannelOpen = GameObject.Find("PannelOpenButton").GetComponent<Button>();
        BackToPT_But = GameObject.Find("GoToPT").GetComponent<Button>();

        //ShellInfo_Toggle = GameObject.Find("ShowShell_Toggle").GetComponent<Toggle>();
        Transparency_Toggle = GameObject.Find("Transparency_Toggle").GetComponent<Toggle>();

        SidePannelOpen.onClick.RemoveAllListeners();
        BackToPT_But.onClick.RemoveAllListeners();
        //ShellInfo_Toggle.onValueChanged.RemoveAllListeners();
        Transparency_Toggle.onValueChanged.RemoveAllListeners();
        Transparency_Toggle.isOn = true;

        SidePannelOpen.onClick.AddListener(OpenSidePannel);

        BackToPT_But.onClick.AddListener(BackToPT);

        //ShellInfo_Toggle.onValueChanged.AddListener(ToggleShellInfo);
        Transparency_Toggle.onValueChanged.AddListener(ToggleTransparency);

        ToggleShellInfo(false);
        BackToPT();
        PerodicTableSet.SetActive(true);
        PerodicTableSet2.SetActive(false);

        for (int i = 0; i < Elements.Length; i++)
        {
            string[] IR_Val = ElecConfigData_Json[i].IonicRadius.Split('(');
            float ToScale = 0;

            /*if (Elements[i].GetComponent<iTween>())
                Destroy(Elements[i].GetComponent<iTween>());

            if (Elements[i].transform.GetChild(0).GetComponent<iTween>())
                Destroy(Elements[i].transform.GetChild(0).GetComponent<iTween>());
                
             if (ToScale < 0)
            {
                TextMeshPro[] TextChilds = Elements[i].GetComponentsInChildren<TextMeshPro>();
                for (int child = 0; child < TextChilds.Length; child++)
                {
                    TextChilds[child].gameObject.transform.localPosition = new Vector3(TextChilds[child].gameObject.transform.localPosition.x, TextChilds[child].gameObject.transform.localPosition.y, 0.001f);
                }
            }
            */

            if (IR_Val.Length > 1)
            {
                String[] IR_Scale = IR_Val[1].Split(')');

                float Scale = 0;

                if (IR_Scale[0] != "" && IR_Scale[0] != "0" && IR_Scale[0] != "-")
                    Scale = float.Parse(IR_Scale[0]);

                if (IR_Val[0] != "" && IR_Val[0] != "0" && IR_Val[0] != "-")
                    ToScale = (0.05f) + (Scale * float.Parse(IR_Val[0]));
                else
                    ToScale = (0.1f);
                //print("scale......" + ToScale);

            }
            else
                ToScale = (0.1f);

            

            Vector3 RandomScale = new Vector3(Elements[i].transform.localScale.x, Elements[i].transform.localScale.y, ToScale);
            Vector3 CurrPos = new Vector3(Elements[i].transform.localPosition.x, Elements[i].transform.localPosition.y, 0.14f);

            Elements[i].transform.localPosition = CurrPos;

            Elements[i].transform.localScale = new Vector3(Elements[i].transform.localScale.x, Elements[i].transform.localScale.y, 0.1f);

            iTween.ScaleTo(Elements[i], iTween.Hash("scale", RandomScale, "delay", 0.7f, "islocal", true, "time", UnityEngine.Random.Range(0.1f, 0.2f), "speed", 2.5f, "easetype", iTween.EaseType.easeInOutSine));
            ElementsMaterials[i].color = OriginalColors[i];
            //Elements[i].GetComponent<EventTrigger>().enabled = false;
        }

        ElementSymbol_UI.text = "-";
        ElementName_UI.text = "Element Name";
        AtomicNumber_UI.text = "-";
        IonicRadius_UI.text = "-";
        Heading_Txt.text = LearningGoals[0];
        Learninggoal_Txt.text = LearningGoals[1];
        Instruction_Txt.text = LearningGoals[2];
        Assumption_Txt.text = LearningGoals[3];


    }

    public void ShowBoilingPoint(int Cnt)
    {
        // InvokeRepeating("PeriodicTableScaleUp", 0.1f, 0.01f);
        for (int i = 0; i < UIPanels.Length; i++)
        {
            UIPanels[i].SetActive(false);
        }

        UIPanels[Cnt].SetActive(true);



        CalculateColors(new Color(204, 204, 102, 255) / 255, Color.red, Elements.Length);
        BoilingPoint.Sort();
        //Array.Sort(BoilingPoint);
        //BoilingPoint.OrderBy(x => x);

        ElementSymbol_UI = GameObject.Find("ElementSymbol_Value").GetComponent<Text>();
        ElementName_UI = GameObject.Find("ElementName_Value").GetComponent<Text>();
        AtomicNumber_UI = GameObject.Find("AtomicNumber_Value").GetComponent<Text>();
        BoilingPoint_UI = GameObject.Find("BoilingPoint_Value").GetComponent<Text>();

        Learninggoal_Txt = GameObject.Find("LGListText").GetComponent<Text>();
        Instruction_Txt = GameObject.Find("IListText").GetComponent<Text>();
        Assumption_Txt = GameObject.Find("AListText").GetComponent<Text>();
        Heading_Txt = GameObject.Find("InfoHeadingText").GetComponent<Text>();


        SidePannel = GameObject.Find("SidePanel");
        SidePannelOpen = GameObject.Find("PannelOpenButton").GetComponent<Button>();
        //BackToPT_But = GameObject.Find("GoToPT").GetComponent<Button>();

        //ShellInfo_Toggle = GameObject.Find("ShowShell_Toggle").GetComponent<Toggle>();
        Transparency_Toggle = GameObject.Find("Transparency_Toggle").GetComponent<Toggle>();


        SidePannelOpen.onClick.RemoveAllListeners();
        BackToPT_But.onClick.RemoveAllListeners();
        //ShellInfo_Toggle.onValueChanged.RemoveAllListeners();
        Transparency_Toggle.onValueChanged.RemoveAllListeners();
        Transparency_Toggle.isOn = true;
        ValueDisplaySlider.gameObject.SetActive(true);

        ValueDisplaySlider.minValue = -1;
        ValueDisplaySlider.maxValue = 1;


        SidePannelOpen.onClick.AddListener(OpenSidePannel);

        //BackToPT_But.onClick.AddListener(BackToPT);

        //ShellInfo_Toggle.onValueChanged.AddListener(ToggleShellInfo);
        Transparency_Toggle.onValueChanged.AddListener(ToggleTransparency);

        BackToPT();
        PerodicTableSet.SetActive(true);
        PerodicTableSet2.SetActive(false);

        for (int i = 0; i < Elements.Length; i++)
        {

            float ToScale = 0;

            /*if (Elements[i].GetComponent<iTween>())
                Destroy(Elements[i].GetComponent<iTween>());

            if (Elements[i].transform.GetChild(0).GetComponent<iTween>())
                Destroy(Elements[i].transform.GetChild(0).GetComponent<iTween>());
                
              if (Elements[i].transform.localScale.z <= 0)
            {
                TextMeshPro[] TextChilds = Elements[i].GetComponentsInChildren<TextMeshPro>();
                for (int child = 0; child < TextChilds.Length; child++)
                {
                    TextChilds[child].gameObject.transform.localPosition = new Vector3(TextChilds[child].gameObject.transform.localPosition.x, TextChilds[child].gameObject.transform.localPosition.y, -0.098f);
                }
            }*/

            ElementsMaterials[i].color = ColorsBetween[0];

            if (ElecConfigData_Json[i].BoilingPoint != "" && ElecConfigData_Json[i].BoilingPoint != "-")
                ToScale = float.Parse(ElecConfigData_Json[i].BoilingPoint);

            int Index = 0;

            if (BoilingPoint.Contains(ToScale))
                Index = BoilingPoint.IndexOf(ToScale);

            ToScale = ToScale / 800;

            Vector3 RandomScale = new Vector3(Elements[i].transform.localScale.x, Elements[i].transform.localScale.y, (0.1f) + ToScale);

            Vector3 CurrPos = new Vector3(Elements[i].transform.localPosition.x, Elements[i].transform.localPosition.y, 0.14f);

           

            Elements[i].transform.localPosition = CurrPos;

            Elements[i].transform.localScale = new Vector3(Elements[i].transform.localScale.x, Elements[i].transform.localScale.y, 0.1f);

            iTween.ScaleTo(Elements[i], iTween.Hash("scale", RandomScale, "delay", 0.7f, "islocal", true, "time", UnityEngine.Random.Range(0.1f, 0.2f), "speed", 2.5f, "easetype", iTween.EaseType.easeInOutSine));

            /*for (int colr = 0; colr < BoilingPoint.Length; colr++)
            {
                print(BoilingPoint[colr] +"...............bp......"+(ToScale*1000));
                if(BoilingPoint[colr] == (ToScale * 1000))
                    ElementsMaterials[i].color = ColorsBetween[colr];
            }*/


            //print("index........."+Index+".....iii....."+i);
            ReffColor[i] = ColorsBetween[Index];

            iTween.ColorTo(Elements[i].transform.GetComponentInChildren<MeshRenderer>().gameObject, iTween.Hash("color", ColorsBetween[Index], "delay", 0.5f, "time", ToScale / 2, "easetype", iTween.EaseType.easeInOutSine));
            //ElementsMaterials[i].color = ColorsBetween[Index];

            //Elements[i].GetComponent<EventTrigger>().enabled = false;
        }

        ElementSymbol_UI.text = "-";
        ElementName_UI.text = "Element Name";
        AtomicNumber_UI.text = "-";
        BoilingPoint_UI.text = "-";
        Heading_Txt.text = LearningGoals[0];
        Learninggoal_Txt.text = LearningGoals[1];
        Instruction_Txt.text = LearningGoals[2];
        Assumption_Txt.text = LearningGoals[3];

         SliderValue_Text.text = "0";
        SliderValue_Units.text = "°C";
        ValueDisplaySlider.value = -1;
    }

    public void ShowMeltingPoint(int Cnt)
    {
        for (int i = 0; i < UIPanels.Length; i++)
        {
            UIPanels[i].SetActive(false);
        }

        UIPanels[Cnt].SetActive(true);
        ValueDisplaySlider.gameObject.SetActive(true);
        ValueDisplaySlider.minValue = -1;
        ValueDisplaySlider.maxValue = 1;


        CalculateColors(new Color(0, 0, 255, 255) / 255, new Color(0, 255, 0, 255) / 255, Elements.Length);
        MeltingPoint.Sort();

        ElementSymbol_UI = GameObject.Find("ElementSymbol_Value").GetComponent<Text>();
        ElementName_UI = GameObject.Find("ElementName_Value").GetComponent<Text>();
        AtomicNumber_UI = GameObject.Find("AtomicNumber_Value").GetComponent<Text>();
        MeltingPoint_UI = GameObject.Find("MeltingPoint_Value").GetComponent<Text>();

        Learninggoal_Txt = GameObject.Find("LGListText").GetComponent<Text>();
        Instruction_Txt = GameObject.Find("IListText").GetComponent<Text>();
        Assumption_Txt = GameObject.Find("AListText").GetComponent<Text>();
        Heading_Txt = GameObject.Find("InfoHeadingText").GetComponent<Text>();

        SidePannel = GameObject.Find("SidePanel");
        SidePannelOpen = GameObject.Find("PannelOpenButton").GetComponent<Button>();
        //BackToPT_But = GameObject.Find("GoToPT").GetComponent<Button>();

        //ShellInfo_Toggle = GameObject.Find("ShowShell_Toggle").GetComponent<Toggle>();
        Transparency_Toggle = GameObject.Find("Transparency_Toggle").GetComponent<Toggle>();

        SidePannelOpen.onClick.RemoveAllListeners();
        BackToPT_But.onClick.RemoveAllListeners();
        //ShellInfo_Toggle.onValueChanged.RemoveAllListeners();
        Transparency_Toggle.onValueChanged.RemoveAllListeners();
        Transparency_Toggle.isOn = true;

        SidePannelOpen.onClick.AddListener(OpenSidePannel);

        //BackToPT_But.onClick.AddListener(BackToPT);

        //ShellInfo_Toggle.onValueChanged.AddListener(ToggleShellInfo);
        Transparency_Toggle.onValueChanged.AddListener(ToggleTransparency);

        BackToPT();
        PerodicTableSet.SetActive(true);
        PerodicTableSet2.SetActive(false);

        for (int i = 0; i < Elements.Length; i++)
        {
            float ToScale = 0;

            /*if (Elements[i].GetComponent<iTween>())
                Destroy(Elements[i].GetComponent<iTween>());

            if (Elements[i].transform.GetChild(0).GetComponent<iTween>())
                Destroy(Elements[i].transform.GetChild(0).GetComponent<iTween>());.

                if (Elements[i].transform.localScale.z <= 0)
            {
                TextMeshPro[] TextChilds = Elements[i].GetComponentsInChildren<TextMeshPro>();
                for (int child = 0; child < TextChilds.Length; child++)
                {
                    TextChilds[child].gameObject.transform.localPosition = new Vector3(TextChilds[child].gameObject.transform.localPosition.x, TextChilds[child].gameObject.transform.localPosition.y, -0.098f);
                }
            }*/


            ElementsMaterials[i].color = ColorsBetween[0];

            if (ElecConfigData_Json[i].MeltingPoint != "" && ElecConfigData_Json[i].MeltingPoint != "-")
                ToScale = float.Parse(ElecConfigData_Json[i].MeltingPoint);

            int Index = 0;

            if (MeltingPoint.Contains(ToScale))
                Index = MeltingPoint.IndexOf(ToScale);

            ToScale = ToScale / 550;

            Vector3 RandomScale = new Vector3(Elements[i].transform.localScale.x, Elements[i].transform.localScale.y, (0.1f) + ToScale);

            Vector3 CurrPos = new Vector3(Elements[i].transform.localPosition.x, Elements[i].transform.localPosition.y, 0.14f);

            

            Elements[i].transform.localPosition = CurrPos;

            Elements[i].transform.localScale = new Vector3(Elements[i].transform.localScale.x, Elements[i].transform.localScale.y, 0.1f);

            iTween.ScaleTo(Elements[i], iTween.Hash("scale", RandomScale, "delay", 0.7f, "islocal", true, "time", UnityEngine.Random.Range(0.1f, 0.2f), "speed", 2.5f, "easetype", iTween.EaseType.easeInOutSine));

            ReffColor[i] = ColorsBetween[Index];

            iTween.ColorTo(Elements[i].transform.GetComponentInChildren<MeshRenderer>().gameObject, iTween.Hash("color", ColorsBetween[Index], "delay", 0.5f, "time", ToScale / 2, "easetype", iTween.EaseType.easeInOutSine));
            
            //ElementsMaterials[i].color = ColorsBetween[Index];
            //Elements[i].GetComponent<EventTrigger>().enabled = false;
        }

        ElementSymbol_UI.text = "-";
        ElementName_UI.text = "Element Name";
        AtomicNumber_UI.text = "-";
        MeltingPoint_UI.text = "-";
        Heading_Txt.text = LearningGoals[0];
        Learninggoal_Txt.text = LearningGoals[1];
        Instruction_Txt.text = LearningGoals[2];
        Assumption_Txt.text = LearningGoals[3];

         SliderValue_Text.text = "0";
        SliderValue_Units.text = "°C";
        ValueDisplaySlider.value = -1;
    }

    public void ShowDensity(int Cnt)
    {
        for (int i = 0; i < UIPanels.Length; i++)
        {
            UIPanels[i].SetActive(false);
        }

        UIPanels[Cnt].SetActive(true);
        ValueDisplaySlider.gameObject.SetActive(true);

        ValueDisplaySlider.minValue = 0;
        ValueDisplaySlider.maxValue = 25;


        ElementSymbol_UI = GameObject.Find("ElementSymbol_Value").GetComponent<Text>();
        ElementName_UI = GameObject.Find("ElementName_Value").GetComponent<Text>();
        AtomicNumber_UI = GameObject.Find("AtomicNumber_Value").GetComponent<Text>();
        Density_UI = GameObject.Find("Density_Value").GetComponent<Text>();

        Learninggoal_Txt = GameObject.Find("LGListText").GetComponent<Text>();
        Instruction_Txt = GameObject.Find("IListText").GetComponent<Text>();
        Assumption_Txt = GameObject.Find("AListText").GetComponent<Text>();
        Heading_Txt = GameObject.Find("InfoHeadingText").GetComponent<Text>();

        SidePannel = GameObject.Find("SidePanel");
        SidePannelOpen = GameObject.Find("PannelOpenButton").GetComponent<Button>();
        //BackToPT_But = GameObject.Find("GoToPT").GetComponent<Button>();

        //ShellInfo_Toggle = GameObject.Find("ShowShell_Toggle").GetComponent<Toggle>();
        Transparency_Toggle = GameObject.Find("Transparency_Toggle").GetComponent<Toggle>();

        SidePannelOpen.onClick.RemoveAllListeners();
        BackToPT_But.onClick.RemoveAllListeners();
        //ShellInfo_Toggle.onValueChanged.RemoveAllListeners();
        Transparency_Toggle.onValueChanged.RemoveAllListeners();

        SidePannelOpen.onClick.AddListener(OpenSidePannel);

        //BackToPT_But.onClick.AddListener(BackToPT);

        //ShellInfo_Toggle.onValueChanged.AddListener(ToggleShellInfo);
        Transparency_Toggle.onValueChanged.AddListener(ToggleTransparency);
        Transparency_Toggle.isOn = true;

        BackToPT();
        PerodicTableSet.SetActive(true);
        PerodicTableSet2.SetActive(false);

        for (int i = 0; i < Elements.Length; i++)
        {
            float ToScale = 0;

            /*if (Elements[i].GetComponent<iTween>())
                Destroy(Elements[i].GetComponent<iTween>());

            if (Elements[i].transform.GetChild(0).GetComponent<iTween>())
                Destroy(Elements[i].transform.GetChild(0).GetComponent<iTween>());
                
             if (Elements[i].transform.localScale.z <= 0)
            {
                TextMeshPro[] TextChilds = Elements[i].GetComponentsInChildren<TextMeshPro>();
                for (int child = 0; child < TextChilds.Length; child++)
                {
                    TextChilds[child].gameObject.transform.localPosition = new Vector3(TextChilds[child].gameObject.transform.localPosition.x, TextChilds[child].gameObject.transform.localPosition.y, -0.098f);
                }
            }*/

            if (ElecConfigData_Json[i].Density != "0" && ElecConfigData_Json[i].Density != "" && ElecConfigData_Json[i].Density != "-")
                ToScale = float.Parse(ElecConfigData_Json[i].Density);

            ToScale = ToScale / 4;

            Vector3 RandomScale = new Vector3(Elements[i].transform.localScale.x, Elements[i].transform.localScale.y, (0.1f) + ToScale);

            Vector3 CurrPos = new Vector3(Elements[i].transform.localPosition.x, Elements[i].transform.localPosition.y, 0.14f);

            

            Elements[i].transform.localPosition = CurrPos;

            Elements[i].transform.localScale = new Vector3(Elements[i].transform.localScale.x, Elements[i].transform.localScale.y, 0.1f);

            iTween.ScaleTo(Elements[i], iTween.Hash("scale", RandomScale, "delay", 0.7f, "islocal", true, "time", UnityEngine.Random.Range(0.1f, 0.2f), "speed", 2.5f, "easetype", iTween.EaseType.easeInOutSine));
            ElementsMaterials[i].color = OriginalColors[i];
            //Elements[i].GetComponent<EventTrigger>().enabled = false;
        }

        ElementSymbol_UI.text = "-";
        ElementName_UI.text = "Element Name";
        AtomicNumber_UI.text = "-";
        Density_UI.text = "-";
        Heading_Txt.text = LearningGoals[0];
        Learninggoal_Txt.text = LearningGoals[1];
        Instruction_Txt.text = LearningGoals[2];
        Assumption_Txt.text = LearningGoals[3];

         SliderValue_Text.text = "0";
        SliderValue_Units.text = "cm3";
        ValueDisplaySlider.value = 0;
    }

    public void ShowElectroNegativity(int Cnt)
    {
        for (int i = 0; i < UIPanels.Length; i++)
        {
            UIPanels[i].SetActive(false);
        }

        UIPanels[Cnt].SetActive(true);
        ValueDisplaySlider.gameObject.SetActive(true);

        ValueDisplaySlider.minValue = 0;
        ValueDisplaySlider.maxValue = 4;

        ElementSymbol_UI = GameObject.Find("ElementSymbol_Value").GetComponent<Text>();
        ElementName_UI = GameObject.Find("ElementName_Value").GetComponent<Text>();
        AtomicNumber_UI = GameObject.Find("AtomicNumber_Value").GetComponent<Text>();
        ElectroNegativity_UI = GameObject.Find("ElectroNegativity_Value").GetComponent<Text>();

        Learninggoal_Txt = GameObject.Find("LGListText").GetComponent<Text>();
        Instruction_Txt = GameObject.Find("IListText").GetComponent<Text>();
        Assumption_Txt = GameObject.Find("AListText").GetComponent<Text>();
        Heading_Txt = GameObject.Find("InfoHeadingText").GetComponent<Text>();

        SidePannel = GameObject.Find("SidePanel");
        SidePannelOpen = GameObject.Find("PannelOpenButton").GetComponent<Button>();
        //BackToPT_But = GameObject.Find("GoToPT").GetComponent<Button>();

        //ShellInfo_Toggle = GameObject.Find("ShowShell_Toggle").GetComponent<Toggle>();
        Transparency_Toggle = GameObject.Find("Transparency_Toggle").GetComponent<Toggle>();

        SidePannelOpen.onClick.RemoveAllListeners();
        BackToPT_But.onClick.RemoveAllListeners();
        //ShellInfo_Toggle.onValueChanged.RemoveAllListeners();
        Transparency_Toggle.onValueChanged.RemoveAllListeners();

        SidePannelOpen.onClick.AddListener(OpenSidePannel);

        //BackToPT_But.onClick.AddListener(BackToPT);

        //ShellInfo_Toggle.onValueChanged.AddListener(ToggleShellInfo);
        Transparency_Toggle.onValueChanged.AddListener(ToggleTransparency);
        Transparency_Toggle.isOn = true;

        BackToPT();
        PerodicTableSet.SetActive(true);
        PerodicTableSet2.SetActive(false);

        for (int i = 0; i < Elements.Length; i++)
        {
            float ToScale = 0;

            /*if (Elements[i].GetComponent<iTween>())
                Destroy(Elements[i].GetComponent<iTween>());

            if (Elements[i].transform.GetChild(0).GetComponent<iTween>())
                Destroy(Elements[i].transform.GetChild(0).GetComponent<iTween>());
                
             if (Elements[i].transform.localScale.z <= 0)
            {
                TextMeshPro[] TextChilds = Elements[i].GetComponentsInChildren<TextMeshPro>();
                for (int child = 0; child < TextChilds.Length; child++)
                {
                    TextChilds[child].gameObject.transform.localPosition = new Vector3(TextChilds[child].gameObject.transform.localPosition.x, TextChilds[child].gameObject.transform.localPosition.y, -0.098f);
                }
            }*/

            if (ElecConfigData_Json[i].ElectroNegativity != "0" && ElecConfigData_Json[i].ElectroNegativity != "" && ElecConfigData_Json[i].ElectroNegativity != "-")
                ToScale = float.Parse(ElecConfigData_Json[i].ElectroNegativity);

            
            Vector3 RandomScale = new Vector3(Elements[i].transform.localScale.x, Elements[i].transform.localScale.y, (0.1f) + ToScale);

            Vector3 CurrPos = new Vector3(Elements[i].transform.localPosition.x, Elements[i].transform.localPosition.y, 0.14f);

            

            Elements[i].transform.localPosition = CurrPos;

            Elements[i].transform.localScale = new Vector3(Elements[i].transform.localScale.x, Elements[i].transform.localScale.y, 0.1f);

            iTween.ScaleTo(Elements[i], iTween.Hash("scale", RandomScale, "delay", 0.7f, "islocal", true, "time", UnityEngine.Random.Range(0.1f, 0.2f), "speed", 2.5f, "easetype", iTween.EaseType.easeInOutSine));
            ElementsMaterials[i].color = OriginalColors[i];
            //Elements[i].GetComponent<EventTrigger>().enabled = false;
        }

        ElementSymbol_UI.text = "-";
        ElementName_UI.text = "Element Name";
        AtomicNumber_UI.text = "-";
        ElectroNegativity_UI.text = "-";
        Heading_Txt.text = LearningGoals[0];
        Learninggoal_Txt.text = LearningGoals[1];
        Instruction_Txt.text = LearningGoals[2];
        Assumption_Txt.text = LearningGoals[3];

         SliderValue_Text.text = "0";
        SliderValue_Units.text = "EN";
        ValueDisplaySlider.value = 0;
    }

    public void ShowOxidationStates(int Cnt)
    {
        for (int i = 0; i < UIPanels.Length; i++)
        {
            UIPanels[i].SetActive(false);
        }

        UIPanels[Cnt].SetActive(true);
        ValueDisplaySlider.gameObject.SetActive(false);

        SidePannel = GameObject.Find("SidePanel");
        SidePannelOpen = GameObject.Find("PannelOpenButton").GetComponent<Button>();

        ElementName_UI = GameObject.Find("ElementName_Value").GetComponent<Text>();
        AtomicNumber_UI = GameObject.Find("AtomicNumber_Value").GetComponent<Text>();
        ElementSymbol_UI = GameObject.Find("ElementSymbol_Value").GetComponent<Text>();

        Learninggoal_Txt = GameObject.Find("LGListText").GetComponent<Text>();
        Instruction_Txt = GameObject.Find("IListText").GetComponent<Text>();
        Assumption_Txt = GameObject.Find("AListText").GetComponent<Text>();
        Heading_Txt = GameObject.Find("InfoHeadingText").GetComponent<Text>();

        SidePannelOpen.onClick.RemoveAllListeners();
        BackToPT_But.onClick.RemoveAllListeners();



        SidePannelOpen.onClick.AddListener(OpenSidePannel);



        BackToPT();

        PerodicTableSet.SetActive(false);
        PerodicTableSet2.SetActive(true);
        IonizationPotential.SetActive(false);
        OxidationStates.SetActive(true);

        for (int j = 0; j < Elements_Sphere.Length; j++)
        {
            Elements_Sphere[j].transform.localPosition = new Vector3(Elements_Sphere[j].transform.localPosition.x, 0, Elements_Sphere[j].transform.localPosition.z);

            /*if (Elements_Sphere[j].GetComponent<iTween>())
                Destroy(Elements_Sphere[j].GetComponent<iTween>());

            if (Elements_Sphere[j].transform.GetChild(0).GetComponent<iTween>())
                Destroy(Elements_Sphere[j].transform.GetChild(0).GetComponent<iTween>());*/

            Vector3 RandomPos = new Vector3(Elements_Sphere[j].transform.localPosition.x, UnityEngine.Random.Range(0f, 5f), Elements_Sphere[j].transform.localPosition.z);
            Vector3 CurrPos = new Vector3(Elements_Sphere[j].transform.localPosition.x, Elements_Sphere[j].transform.localPosition.y, Elements_Sphere[j].transform.localPosition.z);



            Elements_Sphere[j].transform.localPosition = RandomPos;


            iTween.MoveTo(Elements_Sphere[j], iTween.Hash("position", CurrPos, "delay", 0.7f, "islocal", true, "time", UnityEngine.Random.Range(0.1f, 0.2f), "speed", 2.5f, "easetype", iTween.EaseType.easeInOutSine));


            for (int child = 0; child < OxidationStates.transform.GetChild(j).childCount; child++)
            {
                string Name = OxidationStates.transform.GetChild(j).GetChild(child).name;
                string Num = Name.Substring(Name.Length - 1);
                float Multiple = 0;
                float.TryParse(Num, out Multiple);
                if (Multiple == 8)
                    Multiple = 7.99f;

                if (Name.Contains("P"))
                {

                    OxidationStates.transform.GetChild(j).GetChild(child).localEulerAngles = new Vector3(0, -22.5f * Multiple, 0);
                }
                else if (Name.Contains("M"))
                    OxidationStates.transform.GetChild(j).GetChild(child).localEulerAngles = new Vector3(0, 22.5f * Multiple, 0);

                OxidationStates.transform.GetChild(j).GetChild(child).GetComponentInChildren<TextMeshPro>().enabled = false;
                OxidationStates.transform.GetChild(j).gameObject.SetActive(false);

            }

        }


        ElementSymbol_UI.text = "-";
        ElementName_UI.text = "Element Name";
        AtomicNumber_UI.text = "-";
        Heading_Txt.text = LearningGoals[0];
        Learninggoal_Txt.text = LearningGoals[1];
        Instruction_Txt.text = LearningGoals[2];
        Assumption_Txt.text = LearningGoals[3];

    }

    public void ShowIonizationPotential(int Cnt)
    {
        for (int i = 0; i < UIPanels.Length; i++)
        {
            UIPanels[i].SetActive(false);
        }

        UIPanels[Cnt].SetActive(true);
        ValueDisplaySlider.gameObject.SetActive(false);

        SidePannel = GameObject.Find("SidePanel");
        SidePannelOpen = GameObject.Find("PannelOpenButton").GetComponent<Button>();
        //BackToPT_But = GameObject.Find("GoToPT").GetComponent<Button>();


        SidePannelOpen.onClick.RemoveAllListeners();
        BackToPT_But.onClick.RemoveAllListeners();
        //ShellInfo_Toggle.onValueChanged.RemoveAllListeners();

        SidePannelOpen.onClick.AddListener(OpenSidePannel);

        //BackToPT_But.onClick.AddListener(BackToPT);

        //ShellInfo_Toggle.onValueChanged.AddListener(ToggleShellInfo);
        ElementName_UI = GameObject.Find("ElementName_Value").GetComponent<Text>();
        AtomicNumber_UI = GameObject.Find("AtomicNumber_Value").GetComponent<Text>();
        ElementSymbol_UI = GameObject.Find("ElementSymbol_Value").GetComponent<Text>();

        Learninggoal_Txt = GameObject.Find("LGListText").GetComponent<Text>();
        Instruction_Txt = GameObject.Find("IListText").GetComponent<Text>();
        Assumption_Txt = GameObject.Find("AListText").GetComponent<Text>();
        Heading_Txt = GameObject.Find("InfoHeadingText").GetComponent<Text>();

        IP1_Text = GameObject.Find("IP1_Val_Text").GetComponent<Text>();
        IP2_Text = GameObject.Find("IP2_Val_Text").GetComponent<Text>();
        IP3_Text = GameObject.Find("IP3_Val_Text").GetComponent<Text>();

        BackToPT();

        PerodicTableSet.SetActive(false);
        PerodicTableSet2.SetActive(true);
        OxidationStates.SetActive(false);
        IonizationPotential.SetActive(true);

        for (int j = 0; j < Elements_Sphere.Length; j++)
        {
            float IP1 = 0.5f, IP2 = 0.2f, IP3 = 0.1f;

            //float.TryParse(ElecConfigData_Json[j].IP1,out IP1);
            //float.TryParse(ElecConfigData_Json[j].IP2, out IP2);
            //float.TryParse(ElecConfigData_Json[j].IP3, out IP3);
            IonizationPotential.transform.GetChild(j).GetChild(0).localScale = new Vector3(1, 0.001f, 1);
            IonizationPotential.transform.GetChild(j).GetChild(1).localScale = new Vector3(1, 0.001f, 1);
            IonizationPotential.transform.GetChild(j).GetChild(2).localScale = new Vector3(1, 0.001f, 1);
            IonizationPotential.transform.GetChild(j).gameObject.SetActive(false);


            Elements_Sphere[j].transform.localPosition = new Vector3(Elements_Sphere[j].transform.localPosition.x, 0, Elements_Sphere[j].transform.localPosition.z);

            /*if (Elements_Sphere[j].GetComponent<iTween>())
                Destroy(Elements_Sphere[j].GetComponent<iTween>());

            if (Elements_Sphere[j].transform.GetChild(0).GetComponent<iTween>())
                Destroy(Elements_Sphere[j].transform.GetChild(0).GetComponent<iTween>());*/

            Vector3 RandomPos = new Vector3(Elements_Sphere[j].transform.localPosition.x, UnityEngine.Random.Range(0f, 5f), Elements_Sphere[j].transform.localPosition.z);
            Vector3 CurrPos = new Vector3(Elements_Sphere[j].transform.localPosition.x, Elements_Sphere[j].transform.localPosition.y, Elements_Sphere[j].transform.localPosition.z);



            Elements_Sphere[j].transform.localPosition = RandomPos;


            iTween.MoveTo(Elements_Sphere[j], iTween.Hash("position", CurrPos, "delay", 0.7f, "islocal", true, "time", UnityEngine.Random.Range(0.1f, 0.2f), "speed", 2.5f, "easetype", iTween.EaseType.easeInOutSine));

        }

        ElementSymbol_UI.text = "-";
        ElementName_UI.text = "Element Name";
        AtomicNumber_UI.text = "-";
        Heading_Txt.text = LearningGoals[0];
        Learninggoal_Txt.text = LearningGoals[1];
        Instruction_Txt.text = LearningGoals[2];
        Assumption_Txt.text = LearningGoals[3];
    }

    public void ShowInstruction_Panel(string TextToDisplay)
    {
        var mouse = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);



        if (mouse.x < Screen.width / 2)
        {
            // WronOptionPanel
            //print("Mouse is on left side of screen.");
            InstructionPanel.GetComponent<RectTransform>().pivot = new Vector2(0, 0);
        }



        if (mouse.x > Screen.width / 2)
        {
            //print("Mouse is on right side of screen.");
            InstructionPanel.GetComponent<RectTransform>().pivot = new Vector2(1, 0);
        }

        InstructionPanel.transform.position = Input.mousePosition;
        UIcanvas.GetComponent<GraphicRaycaster>().enabled = false;
        Camera.main.GetComponent<PhysicsRaycaster>().enabled = false;
        iTween.Stop(InstructionPanel.gameObject);

        InstructionPanel.transform.Find("InstructionText").GetComponent<Text>().text = TextToDisplay;
        //iTween.ScaleTo(GlassPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 0.01f, "easetype", iTween.EaseType.spring));
        iTween.ScaleTo(InstructionPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 0.3f, "easetype", iTween.EaseType.spring, "oncomplete", "CloseInstructionPanel", "oncompletetarget", this.gameObject));
        ObjectivePanel.transform.localScale = new Vector3(1, 0, 1);
    }

    public void CloseInstructionPanel()
    {
        UIcanvas.GetComponent<GraphicRaycaster>().enabled = true;
        Camera.main.GetComponent<PhysicsRaycaster>().enabled = true;
        //iTween.ScaleTo(GlassPanel, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 1f, "time", 0.01f, "easetype", iTween.EaseType.linear));
        iTween.ScaleTo(InstructionPanel, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 1f, "time", 0.3f, "easetype", iTween.EaseType.spring));
        if(!IsTopicUnlocked)
        iTween.ScaleTo(ObjectivePanel, iTween.Hash("y", 1, "time", 0.3f, "delay", 1f, "easetype", iTween.EaseType.easeInOutSine));
    }

    public void UIReset()
    {
        ObjectivePanel.GetComponent<RectTransform>().anchoredPosition = new Vector3(0f, 5f, 0);
        
        ValueDisplaySlider.transform.GetComponent<RectTransform>().anchoredPosition = new Vector3(-20f, 0f, 0);
        Info_Open_Btn.transform.GetComponent<RectTransform>().anchoredPosition = new Vector3(-37f, -150f, 0);

        
        SidePannel.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(0f, 0f, 0);
            
        //print("side......" + SidePannel.transform.position+"....local....."+ SidePannel.transform.localPosition);
        if (!IsTopicUnlocked)
        {
            ToDoList_DD.transform.GetComponent<RectTransform>().anchoredPosition = new Vector3(135f, -150f, 0);
            XP_ToShow.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(135f, -150f, 0);
            
        }
        else
        {
            ToDoList_DD.transform.GetComponent<RectTransform>().anchoredPosition = new Vector3(-250f, -150f, 0);
            XP_ToShow.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(-250f, -150f, 0);
            
        }

        //Main_XP_ToShow.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(-135f, -60f, 0);

    }

    public void UIZoomOut()
    {
        if (ObjectivePanel.GetComponent<RectTransform>().anchoredPosition.y == 5)
            return;

        UIReset();
        
        iTween.MoveFrom(Info_Open_Btn.gameObject, iTween.Hash("x", Info_Open_Btn.transform.localPosition.x +250, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveFrom(SidePannel.transform.parent.gameObject, iTween.Hash("x", SidePannel.transform.parent.localPosition.x - 1000f, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveFrom(ValueDisplaySlider.gameObject, iTween.Hash("x", ValueDisplaySlider.transform.localPosition.x + 250, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveFrom(ObjectivePanel, iTween.Hash("y", ObjectivePanel.transform.localPosition.y - 1000, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        
        
        iTween.MoveFrom(ToDoList_DD.gameObject, iTween.Hash("x", ToDoList_DD.transform.localPosition.x - 400, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveFrom(XP_ToShow.transform.parent.gameObject, iTween.Hash("x", XP_ToShow.transform.parent.localPosition.x - 400, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        
        //iTween.MoveFrom(Main_XP_ToShow.transform.parent.gameObject, iTween.Hash("x", Main_XP_ToShow.transform.parent.localPosition.x + 550, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
    }



    public void UIZoomIn()
    {
        if (ObjectivePanel.GetComponent<RectTransform>().anchoredPosition.y != 5)
            return;

        UIReset();
        //print("side......" + SidePannel.transform.position);
        iTween.MoveAdd(ObjectivePanel, iTween.Hash("y", ObjectivePanel.transform.localPosition.y - 1000, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveAdd(Info_Open_Btn.gameObject, iTween.Hash("x", Info_Open_Btn.transform.localPosition.x + 250, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveAdd(SidePannel.transform.parent.gameObject, iTween.Hash("x", SidePannel.transform.parent.localPosition.x - 1000f, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));

        iTween.MoveAdd(ValueDisplaySlider.gameObject, iTween.Hash("x", ValueDisplaySlider.transform.localPosition.x + 250, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));

        iTween.MoveAdd(XP_ToShow.transform.parent.gameObject, iTween.Hash("x", XP_ToShow.transform.parent.localPosition.x - 400, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveAdd(ToDoList_DD.gameObject, iTween.Hash("x", ToDoList_DD.transform.localPosition.x - 400, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        
        //iTween.MoveAdd(Main_XP_ToShow.transform.parent.gameObject, iTween.Hash("x", Main_XP_ToShow.transform.parent.localPosition.x + 550, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
    }

    public float ChangeToLocal_X(float Val)
    {
        float X_ToRet = 0;
        //print("wid........"+ UIcanvas.GetComponent<RectTransform>().rect.width);
        X_ToRet = ((UIcanvas.GetComponent<RectTransform>().rect.x) + Val);

        return X_ToRet;
    }

    public float ChangeToLocal_Y(float Val)
    {
        float Y_ToRet = 0;
        //print("wid........"+ UIcanvas.GetComponent<RectTransform>().rect.width);
        Y_ToRet = ((UIcanvas.GetComponent<RectTransform>().rect.y) + Val);

        return Y_ToRet;
    }


    public void Collect_XP(GameObject TargetPos)
    {
        XP_Effect.transform.position = Camera.main.WorldToScreenPoint(TargetPos.transform.position);

        XP_Effect.text = "+"+Sub_Top_XP[TopicToCheck] + "XP";

        XP_Effect.transform.localScale = new Vector3(2, 0, 2);

        iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 2, "y", 2, "z", 2, "time", 0.5f, "easetype", iTween.EaseType.easeOutElastic));

        iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", XP_Effect.transform.position.x, "y", XP_Effect.transform.position.y + 150, "z", 0, "delay", 0.3f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

        iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", 150, "y", XP_ToShow.transform.position.y, "z", 0, "delay", 1f, "time", 1f, "easetype", iTween.EaseType.easeInSine, "oncomplete", "Add_To_XP", "oncompletetarget", this.gameObject));

    }

    public void Add_To_XP()
    {
        
        //print("TopicToCheck......."+ TopicToCheck+"....sub......"+Sub_Topics.Count);
        if (TopicToCheck < (Sub_Topics.Count))
        {
            Sub_XP_Earned += float.Parse(Sub_Top_XP[TopicToCheck]);

            XP_Effect.text = "";
            XP_ToShow.text = Sub_XP_Earned + "/" + Total_XP;

            TopicToCheck++;
            if (TopicToCheck <= (Sub_Topics.Count - 1))
            {
                ObjectivePanel.transform.Find("ObjectiveText").GetComponent<Text>().text = Sub_Top_ToDoList[TopicToCheck];
                ObjectivePanel.transform.localScale = new Vector3(1,0,1);
                iTween.ScaleTo(ObjectivePanel, iTween.Hash("y",1, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine));
                //ToDoList_DD.transform.Find("Topic_Objective").GetComponent<Text>().text = "Objective: " + Sub_Top_ToDoList[TopicToCheck];
            }
            else
            {
                XP_Effect.transform.position = (XP_ToShow.transform.position);

                XP_Effect.text = Total_XP + "XP";

                XP_Effect.transform.localScale = new Vector3(1, 0, 1);

                iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 2, "y", 2, "z", 2, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeOutElastic));

                iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", XP_Effect.transform.position.x, "y", XP_Effect.transform.position.y + 50, "z", 0, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

                iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", Main_XP_ToShow.transform.position.x, "y", Main_XP_ToShow.transform.position.y, "z", 0, "delay", 1.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine, "oncomplete", "AddTotalToMainXP", "oncompletetarget", this.gameObject));

                iTween.MoveTo(ToDoList_DD.gameObject, iTween.Hash("x", ChangeToLocal_X (- 400), "delay", 3.5f, "time", 0.5f, "islocal", true, "easetype", iTween.EaseType.easeInOutSine));

                iTween.MoveTo(XP_ToShow.transform.parent.gameObject, iTween.Hash("x", ChangeToLocal_X(-400), "delay", 3.5f, "time", 0.5f, "islocal", true, "easetype", iTween.EaseType.easeInOutSine));
                //print("label......" + LabelImage.GetComponent<RectTransform>().anchoredPosition+".......explode...."+ ExplodeSlider.GetComponent<RectTransform>().anchoredPosition);


                //iTween.MoveTo(ValueDisplaySlider.gameObject, iTween.Hash("x", ChangeToLocal_X ( 150), "delay", 0.7f, "time", 0.5f, "islocal", true, "easetype", iTween.EaseType.easeInOutSine));
            }
        }

    }

    public void AddTotalToMainXP()
    {
        if (OptionUnlockedTill < 1)
        {
            OptionUnlockedTill++;

            float MainXp = float.Parse(Main_XP_ToShow.text);
            MainXp += Total_XP;
            Main_XP_ToShow.text = MainXp.ToString();
            //ObjectivePanel.transform.Find("ObjectiveText").GetComponent<Text>().text = "YaY!......Next topic unlocked";
            XP_Effect.text = "";
            IsTopicUnlocked = true;
            iTween.ScaleTo(ObjectivePanel, iTween.Hash("y", 0, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine));

           
           
        }
    }
}
