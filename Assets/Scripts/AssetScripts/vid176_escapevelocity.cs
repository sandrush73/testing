﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;

public class vid176_escapevelocity : MonoBehaviour, IPointerDownHandler
{
    // Start is called before the first frame update

    public Canvas C_BG;

    public orbit CamOrbit;

    public AROrbitControls ArOrbit;

    public bool IsAR;

    public GameObject MainObj, Ground;

    public int OptionUnlockedTill, CurrentTopicSelected, TopicToCheck;

    public float Sub_XP_Earned, Total_XP;

    public Dropdown Options_DD, ToDoList_DD;

    public GameObject InstructionPanel_DD, ToDoList_Panel;

    public List<Dictionary<string, object>> CSV_data;

    public List<String> TaskList, XpList, InfoText;

    public List<GameObject> AllTaskData;

    public GameObject[] Labels;

    public Text XP_ToShow, XP_Effect, Main_XP_ToShow, localTotalXpValueText, localGainedXpValueText, ObjectiveInfoText, LGListText, IListText, AListText,
        InfoHeadingText, GF_ResultTxt, E_valueTxt, m_valtxt;

    public GameObject TaskObj, AllTaskListObj;

    public RectTransform rT;

    public int localTotalXpValue, localGaniedXpValue, MainXpValue;

    public GameObject ControlPanel, LocalXP, Main_Xp, ObjectivePanel, InformationPanel, InfoButton, MainSlider, InfoPanelBg, TasklistPanel, WronOptionPanel, Gained_XP_ArrowBut;

    public Button InfoCloseBtn, InfoContinueBtn, Gained_XP_Panel, CpCloseBtn, IncButton1, DecButton1, IncButton2, DecButton2;

    public bool sessionStart;

    public TextAsset CSVFile;

    public AssetBundle assetBundle;

    public string[] CorrectMsgs = new string[] { "Nice!", "WellDone!", "Excellent!", "Correct!" };

    public Color Highlight_Color, Normal_Color;

    public Text MessageT, DescText;

    //...................................................................................

    public GameObject SmoothLookPlane, PullObj, obj, BallObj;

    public float t, R, r, v, ini, d, init, tttt, rVal, mVal, escV, orbV;

    public InputField InputMass, InputRad;

    public GameObject CreatePlanetPanel, CreatePlanetWM, RefImg;

    public Button CreateBut, ResetPlanet;

    public Text CpMassText, CpRadText, CpEscpVText, CpOrbVText;

    public int NoOfTime;


    public float[] sliderConds = new float[] { 50, 50, 50, 60, 60, 60 };
   
    

    public static vid176_escapevelocity Instance;

    //public TextAsset CSVFile, DescriTex;
    //public AssetBundle assetBundle;


    void Start()
    {


        Instance = this;
        LoadFromAsssetBundle();

        MainObj = this.gameObject;

        C_BG = GameObject.Find("Canvas_BG").GetComponent<Canvas>();

        Ground = GameObject.Find("Ground");

        if (!IsAR)
        {
            CamOrbit = Camera.main.GetComponentInParent<orbit>();

            C_BG.transform.parent = Camera.main.transform;
            C_BG.GetComponentInChildren<RawImage>().enabled = true;
            C_BG.renderMode = RenderMode.ScreenSpaceCamera;

            C_BG.worldCamera = Camera.main;

            CamOrbit.target = Camera.main.transform.parent;

            CamOrbit.target.position = new Vector3(0, 0.7f, 0);
            CamOrbit.maxRotUp = 45;
            CamOrbit.minRotUp = 1;
            CamOrbit.zoomMin = 1f;
            CamOrbit.zoomMax = 4;
            CamOrbit.zoomStart = 2f;

            CamOrbit.cameraRotUpStart = 0;

            CamOrbit.Start();
        }
        else
        {
            Ground.SetActive(false);
            C_BG.gameObject.SetActive(false);
            ArOrbit = MainObj.transform.parent.gameObject.GetComponent<AROrbitControls>();
        }

        sessionStart = false;

        //OptionUnlockedTill = 0;
        //TopicToCheck = 1;
        //MainXpValue = 10;

        ReadingDataFromCSVFile();
        ReadingXps();
        FindingObjects();
        TaskListCreation();
        AssigningClickEvents();
        LabelsAssignment();
        AssignInitialValues();
    }
    public void LoadFromAsssetBundle()
    {
        //......................................................For Testing ..............................................................

        //CSVFile = Resources.Load("vid176_escapevelocityCSV") as TextAsset;

        //............................................................ For Assetbundle Creation ..........................................

        assetBundle = AssetBundle.LoadFromFile(PlayerPrefs.GetString("AssetBundleUrl"));

        CSVFile = assetBundle.LoadAsset(PlayerPrefs.GetString("LibraryName") + "CSV", typeof(TextAsset)) as TextAsset;        

        //.................................................................................................................................
        Camera.main.gameObject.AddComponent<PhysicsRaycaster>();

        Camera.main.backgroundColor = Color.black;

        GameObject TargetForCamera = GameObject.Find("Target");

        if (TargetForCamera != null)
        {
            TargetForCamera.AddComponent<orbit>();
            CamOrbit = TargetForCamera.GetComponent<orbit>();
            IsAR = false;
        }
        else
        {
            this.transform.parent.gameObject.AddComponent<AROrbitControls>();
            ArOrbit = this.transform.parent.gameObject.GetComponent<AROrbitControls>();
            IsAR = true;
        }
    }

    public int totXp, loclXp;

    public void ReadingXps()
    {
        totXp = PlayerPrefs.GetInt("TotalXP");
        loclXp = PlayerPrefs.GetInt("MaxEarnings");

        if (loclXp != 0)
        {
            OptionUnlockedTill = 0;
        }
        else
        {
            OptionUnlockedTill = 1;
        }

        TopicToCheck = 1;

        MainXpValue = totXp;

        int t = 0;
        int p = 0;

        for (int k = 0; k < TaskList.Count; k++)
        {
            p = loclXp / TaskList.Count;
            XpList.Add(p.ToString());
            t = t + p;
        }

        if (t != loclXp)
        {
            XpList[XpList.Count - 1] = (p + (loclXp - t)).ToString();
        }
    }

    public float result;
    public void GetResult()
    {
        result = ((float)(TopicToCheck - 1) / (float)(TaskList.Count)) * 100;
        Camera.main.SendMessage("UnloadActually", result);
    }

    public void ReadingDataFromCSVFile()
    {
        TaskList = new List<string>();
        XpList = new List<string>();
        InfoText = new List<string>();

        AllTaskData = new List<GameObject>();

        CSV_data = CSVReader.Read(CSVFile);

        for (var i = 0; i < CSV_data.Count; i++)
        {
            if (CSV_data[i]["TaskList"].ToString() != "" && CSV_data[i]["TaskList"].ToString() != null)
            {
                TaskList.Add(CSV_data[i]["TaskList"].ToString());
            }

            //if (CSV_data[i]["XPList"].ToString() != "" && CSV_data[i]["XPList"].ToString() != null)
            //{
            //    XpList.Add(CSV_data[i]["XPList"].ToString());
            //}

            //if (CSV_data[i]["InfoText"].ToString() != "" && CSV_data[i]["InfoText"].ToString() != null)
            //{
            //    InfoText.Add(CSV_data[i]["InfoText"].ToString());
            //}
        }
    }
   
    public void FindingObjects()
    {

        InformationPanel = GameObject.Find("InformationPanel");

        InfoCloseBtn = GameObject.Find("InfoCloseBtn").GetComponent<Button>();

        InfoContinueBtn = GameObject.Find("InfoContinueBtn").GetComponent<Button>();

        InfoPanelBg = GameObject.Find("InfoPanelBg");

        XP_ToShow = GameObject.Find("localGainedXpValueText").GetComponent<Text>();

        XP_Effect = GameObject.Find("Effect_Text").GetComponent<Text>();

        Main_XP_ToShow = GameObject.Find("Main_XP_Value").GetComponent<Text>();

        localGainedXpValueText = GameObject.Find("localGainedXpValueText").GetComponent<Text>();

        localTotalXpValueText = GameObject.Find("GainedXP_LocalTotalXp").GetComponent<Text>();

        ObjectiveInfoText = GameObject.Find("ObjectiveInfoText").GetComponent<Text>();

        TaskObj = GameObject.Find("TaskObj");

        AllTaskListObj = GameObject.Find("AllTaskList");

        ControlPanel = GameObject.Find("ControlPanel");
        LocalXP = GameObject.Find("LocalXP");
        Main_Xp = GameObject.Find("Main_Xp");
        ObjectivePanel = GameObject.Find("ObjectivePanel");
        InformationPanel = GameObject.Find("InformationPanel");
        InfoButton = GameObject.Find("InfoButton");
        MainSlider = GameObject.Find("MainSlider");
        WronOptionPanel = GameObject.Find("WrongPanel");

        Gained_XP_Panel = GameObject.Find("Gained_XP_Panel").GetComponent<Button>();
        TasklistPanel = GameObject.Find("TasklistPanel");
        CpCloseBtn = GameObject.Find("CpCloseBtn").GetComponent<Button>();

        InfoHeadingText = GameObject.Find("InfoHeadingText").GetComponent<Text>();
        LGListText = GameObject.Find("LGListText").GetComponent<Text>();
        IListText = GameObject.Find("IListText").GetComponent<Text>();
        AListText = GameObject.Find("AListText").GetComponent<Text>();
      
        Gained_XP_ArrowBut = GameObject.Find("Gained_XP_ArrowBut");

        SmoothLookPlane = GameObject.Find("SmoothLookPlane");

        PullObj = GameObject.Find("PullObject");

        InputMass = GameObject.Find("InputField_Mass").GetComponent<InputField>();

        InputRad = GameObject.Find("InputField_Radius").GetComponent<InputField>();

        InputMass.onValueChanged.AddListener(checkButStatus);

        InputRad.onValueChanged.AddListener(checkButStatus);

        CreatePlanetPanel = GameObject.Find("CreatePlanetPanel");

        CreateBut = GameObject.Find("CreateBut").GetComponent<Button>();

        CreateBut.GetComponent<Button>().onClick.AddListener(() => PlanetCreation());

        CpMassText = GameObject.Find("CpMassText").GetComponent<Text>();

        CpRadText = GameObject.Find("CpRadText").GetComponent<Text>();

        CpEscpVText = GameObject.Find("CpEscpVText").GetComponent<Text>();

        CpOrbVText = GameObject.Find("CpOrbVText").GetComponent<Text>();

        BallObj = GameObject.Find("soccerball");

        BallObj.gameObject.GetComponent<TrailRenderer>().enabled = false;

        ResetPlanet = GameObject.Find("restplnet").GetComponent<Button>();

        ResetPlanet.onClick.AddListener(() => ResetPlanetFun());

        CreatePlanetWM = GameObject.Find("CreatePlanetWM");

        CreatePlanetWM.transform.localScale = new Vector3(0, 0, 0);

        CreatePlanetPanel.transform.localScale = new Vector3(0, 0, 0);

        RefImg = GameObject.Find("RefImg");

    }
    public void AssigningClickEvents()
    {

        iTween.ScaleTo(InformationPanel.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

        InfoCloseBtn.onClick.AddListener(() => CloseInfoPanel());

        InfoContinueBtn.onClick.AddListener(() => ContinueInfoPanel());

        InfoButton.GetComponent<Button>().onClick.AddListener(() => OpenInfoPanel());

        Gained_XP_Panel.onClick.AddListener(() => OpenTaskList());

        CpCloseBtn.onClick.AddListener(() => OpenOrCloseControlPanel());

        MainSlider.GetComponent<Slider>().onValueChanged.AddListener(delegate { ChangeValue(MainSlider.GetComponent<Slider>().value, MainSlider.GetComponentInChildren<Text>()); });
       // MainSlider.GetComponent<Slider>().onValueChanged.AddListener(delegate { ChangeDistance(MainSlider.GetComponent<Slider>().value, MainSlider.GetComponentInChildren<Text>()); });

        AddListener(EventTriggerType.PointerDown, DisableOrbit, MainSlider.gameObject);
        AddListener(EventTriggerType.PointerUp, EnableOrbit, MainSlider.gameObject);

      
    }

    public void AssignInitialValues()
    {
        if (OptionUnlockedTill > 0)
        {
            LocalXP.transform.localScale = new Vector3(0, 0, 0);
            ObjectivePanel.transform.localScale = new Vector3(0, 0, 0);
        }

        InfoCloseBtn.gameObject.transform.localScale = new Vector3(0, 0, 0);
        localGainedXpValueText.text = "0";
        Main_XP_ToShow.text = MainXpValue + "";
        WronOptionPanel.transform.localScale = new Vector3(0, 0, 0);

        InfoPanelBg.SetActive(true);
    }


    public void TaskListCreation()
    {

        rT = AllTaskListObj.transform.parent.GetComponent<RectTransform>();

        localGaniedXpValue = 0;

        for (int i = 0; i < TaskList.Count; i++)
        {
            GameObject Tl = (GameObject)Instantiate(TaskObj);
            Tl.transform.SetParent(AllTaskListObj.transform);
            Tl.transform.localScale = new Vector3(1, 1, 1);
            Tl.name = i.ToString();

            AllTaskData.Add(Tl);
            AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(false);
            AllTaskData[i].gameObject.transform.GetChild(2).gameObject.SetActive(false);
            AllTaskData[i].gameObject.transform.GetChild(3).GetComponentInChildren<Text>().text = TaskList[i];
            AllTaskData[i].gameObject.transform.GetChild(4).GetComponentInChildren<Text>().text = "+" + XpList[i] + " XP";

            localTotalXpValue += int.Parse(XpList[i]);
        }

        rT.sizeDelta = new Vector2(rT.sizeDelta.x, TaskList.Count * 50);
        localTotalXpValueText.text = "/" + localTotalXpValue.ToString();

        AllTaskData[0].gameObject.transform.GetChild(2).gameObject.SetActive(true);
        ObjectiveInfoText.text = AllTaskData[0].gameObject.transform.GetChild(3).GetComponentInChildren<Text>().text;
    }

    public void LocalXpCalculation()
    {
        for (int i = 0; i < TaskList.Count; i++)
        {
            AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(false);
            AllTaskData[i].gameObject.transform.GetChild(2).gameObject.SetActive(false);
        }

        if (OptionUnlockedTill < 1)
        {
            if ((TopicToCheck) < (TaskList.Count + 1))
            {
                localGaniedXpValue = 0;

                for (int i = 0; i < TopicToCheck - 1; i++)
                {
                    localGaniedXpValue += int.Parse(XpList[i]);
                }

                for (int i = 0; i < TopicToCheck; i++)
                {
                    AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(true);
                }

                localGainedXpValueText.text = localGaniedXpValue.ToString();

                AllTaskData[TopicToCheck - 1].gameObject.transform.GetChild(2).gameObject.SetActive(true);

                ObjectiveInfoText.text = AllTaskData[TopicToCheck - 1].gameObject.transform.GetChild(3).GetComponentInChildren<Text>().text;
            }
            else
            {
                for (int i = 0; i < TaskList.Count; i++)
                {
                    AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(true);
                    AllTaskData[i].gameObject.transform.GetChild(2).gameObject.SetActive(false);
                }

                localGainedXpValueText.text = localTotalXpValue.ToString();

                XP_Effect.text = "+" + localTotalXpValue + " XP";

                XP_Effect.transform.position = (XP_ToShow.transform.position);

                iTween.Stop(XP_Effect.gameObject);

                iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeOutElastic));

                iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", XP_Effect.transform.position.x, "y", XP_Effect.transform.position.y + 50, "z", 0, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

                iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", Main_XP_ToShow.transform.position.x, "y", Main_XP_ToShow.transform.position.y, "z", 0, "delay", 1.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

                iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "delay", 2f, "time", 0.5f, "easetype", iTween.EaseType.linear, "oncomplete", "MainXpCalculation", "oncompletetarget", this.gameObject));
            }
        }
        else
        {
            for (int i = 0; i < TaskList.Count; i++)
            {
                AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(true);
                AllTaskData[i].gameObject.transform.GetChild(2).gameObject.SetActive(false);
            }

            localGainedXpValueText.text = localTotalXpValue.ToString();
        }
    }
    public void CloseAllpanels()
    {

        ControlPanel.GetComponent<Animator>().Play("CpanelFullClose");
        LocalXP.GetComponent<Animator>().Play("LocalXpPanelClose");
        Main_Xp.GetComponent<Animator>().Play("MainXpPanelClose");
        ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelClose");
        InfoButton.GetComponent<Animator>().Play("InfoButtonClose");
        MainSlider.GetComponent<Animator>().Play("MainSliderClose");

        if (TasklistPanel.gameObject.transform.localScale.y != 0)
            iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));
    }

    public void OpenAllpanels()
    {

        if (OptionUnlockedTill < 1) { 
            LocalXP.GetComponent<Animator>().Play("LocalXpPanelOPen");
            ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen");
        }

        ControlPanel.GetComponent<Animator>().Play("CpanelOpen");

        Main_Xp.GetComponent<Animator>().Play("MainXpPanelOpen");

        InfoButton.GetComponent<Animator>().Play("InfoButtonOpen");
        MainSlider.GetComponent<Animator>().Play("MainSliderOpen");
        iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "time", 0.2f, "easetype", iTween.EaseType.linear));
        iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));
    }

    public void AddXpEffect()
    {
        if (OptionUnlockedTill < 1)
        {
            if (TopicToCheck <= (TaskList.Count))
            {
                GameObject.Find("Canvas").GetComponent<CanvasGroup>().interactable = false;

                XP_Effect.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);

                XP_Effect.text = "+" + int.Parse(XpList[TopicToCheck - 1]) + " XP";

                XP_Effect.transform.localScale = new Vector3(1, 0, 1);

                iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeOutElastic));

                iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", XP_Effect.transform.position.x, "y", XP_Effect.transform.position.y + 50, "z", 0, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

                iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", RefImg.transform.position.x, "y", RefImg.transform.position.y, "z", 0, "delay", 1.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

                iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "delay", 2f, "time", 0.5f, "easetype", iTween.EaseType.linear, "oncomplete", "LocalXpCalculation", "oncompletetarget", this.gameObject));

                TopicToCheck++;

                ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelClose");

                Invoke("EnableRayCast", 3);


            }
        }

    }

    public void EnableRayCast()
    {
        //Camera.main.GetComponent<PhysicsRaycaster>().enabled = true;
        GameObject.Find("Canvas").GetComponent<CanvasGroup>().interactable = true;
        if (TopicToCheck <= (TaskList.Count))
        {
            ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen");
        }
    }

    public void MainXpCalculation()
    {
        if (OptionUnlockedTill == 0)
        {
            OptionUnlockedTill = 1;

            MainXpValue += localTotalXpValue;

            Main_XP_ToShow.text = MainXpValue.ToString();

            LocalXP.GetComponent<Animator>().Play("LocalXpPanelClose");

            ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelClose");

            if ((ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelClose")) || (ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelFullClose")))
            {
                ControlPanel.GetComponent<Animator>().Play("CpanelOpen");
            }
        }
    }


    public void CloseInfoPanel()
    {
        sessionStart = true;
        InfoPanelBg.SetActive(false);
        iTween.ScaleTo(InformationPanel.gameObject, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));
        OpenAllpanels();
    }

    public void ContinueInfoPanel()
    {
        sessionStart = true;
       // InfoPanelBg.SetActive(false);
        iTween.ScaleTo(InformationPanel.gameObject, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));
        InfoCloseBtn.transform.localScale = new Vector3(0.27f, 0.27f, 0.27f);
        InfoContinueBtn.transform.localScale = new Vector3(0, 0, 0);
        // OpenAllpanels();
        InfoPanelBg.SetActive(true);
        iTween.ScaleTo(CreatePlanetPanel, iTween.Hash("x", 1, "y", 1, "z",1, "time", 0.5f, "easetype", iTween.EaseType.spring));
    }

    public void OpenInfoPanel()
    {
        sessionStart = false;
        iTween.ScaleTo(InformationPanel.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));
        InfoPanelBg.SetActive(true);
        iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "time", 0.2f, "easetype", iTween.EaseType.linear));
        iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));
        CloseAllpanels();
    }

    public void OpenTaskList()
    {

        if (TasklistPanel.gameObject.transform.localScale.y != 1)
        {
            iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.01f, "time", 0.2f, "easetype", iTween.EaseType.easeInSine));

            ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelClose");

            iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 180, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));

            if ((ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelOpen")))
            {
                ControlPanel.GetComponent<Animator>().Play("CpanelClose");
            }
        }
        else
        {
            iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "delay", 0.01f, "time", 0.2f, "easetype", iTween.EaseType.easeInSine));
            iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));

            ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen");

            ControlPanel.GetComponent<Animator>().Play("CpanelOpen");
        }
    }

    public void OpenOrCloseControlPanel()
    {

        if ((ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelClose")) || (ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelFullClose")))
        {
            ControlPanel.GetComponent<Animator>().Play("CpanelOpen");
        }
        else
        {
            ControlPanel.GetComponent<Animator>().Play("CpanelClose");
        }
    }

    public void WrongOptionSelected()
    {
        WronOptionPanel.transform.localScale = new Vector3(0, 0, 0);

        iTween.Stop(WronOptionPanel.gameObject);

        iTween.ScaleTo(WronOptionPanel.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.01f, "time", 0.2f, "easetype", iTween.EaseType.easeInSine));

        iTween.ScaleTo(WronOptionPanel.gameObject, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 1f, "time", 0.2f, "easetype", iTween.EaseType.easeInSine));

        // InfoPanelBg.SetActive(true);

        Invoke("CloseBlurBg", 1f);

        GameObject.Find("Canvas").GetComponent<CanvasGroup>().interactable = false;

        ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelClose");


        var mouse = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);

        if (mouse.x < Screen.width / 2)
        {
            // WronOptionPanel
            //print("Mouse is on left side of screen.");
            WronOptionPanel.GetComponent<RectTransform>().pivot = new Vector2(0, 0);
        }

        if (mouse.x > Screen.width / 2)
        {
            //print("Mouse is on right side of screen.");
            WronOptionPanel.GetComponent<RectTransform>().pivot = new Vector2(1, 0);
        }


        WronOptionPanel.transform.position = Input.mousePosition;

    }

    public void CloseBlurBg()
    {
        InfoPanelBg.SetActive(false);
        GameObject.Find("Canvas").GetComponent<CanvasGroup>().interactable = true;

        if (OptionUnlockedTill < 1)
        {
            if (InformationPanel.gameObject.transform.localScale.x == 0f)
            {
                //ObjectivePanel.GetComponent<Animator>().Play("");
                ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen", 0, 0f);
            }
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (InformationPanel.gameObject.transform.localScale.x != 1 && CreatePlanetPanel.gameObject.transform.localScale.x == 0 && sessionStart)
        {
            if (OptionUnlockedTill < 1)
            {
                if (TopicToCheck <= (TaskList.Count))
                {
                    iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "time", 0.2f, "easetype", iTween.EaseType.linear));
                    iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));
                    ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen");
                    ControlPanel.GetComponent<Animator>().Play("CpanelOpen");
                }
            }
        }
    }
    public void LabelsAssignment()
    {
        GameObject Label = GameObject.Find("Labels");

        Labels = new GameObject[Label.transform.childCount];

        for (int j = 0; j < Labels.Length; j++)
        {
            Labels[j] = Label.transform.GetChild(j).gameObject;

            GameObject TempLabels = Labels[j];

            foreach (Transform ChildLabel in TempLabels.transform)
            {
                if (ChildLabel.name.Contains("SmoothLook"))
                {
                    ChildLabel.gameObject.AddComponent<SmoothLook>();
                    //ChildLabel.gameObject.AddComponent<ScaleRelativeToCamera>();
                }
            }
        }

        for (int k = 0; k < Labels.Length; k++)
        {
            string name = Labels[k].name.Remove(Labels[k].name.Length - 6);
            //Labels[k].SetActive(false);
            if (GameObject.Find(name + "") != null)
            {
                GameObject dummy = GameObject.Find(name + "").gameObject;

                Labels[k].transform.parent = dummy.transform;
                Labels[k].transform.rotation = Quaternion.identity;
            }
        }
                     
       
   
       SmoothLookPlane.AddComponent<SmoothLook>();
        iTween.ScaleTo(CreatePlanetPanel, iTween.Hash("x", 0, "y", 0, "z", 0, "time", 0.5f, "easetype", iTween.EaseType.spring));
        InfoPanelBg.SetActive(false);
        // CreatePlanetPanel.SetActive(false);
    }




    public void AddListener(EventTriggerType eventType, Action MethodToCall, GameObject TriggerObjToAdd)
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall());
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }

    public void AddListener(EventTriggerType eventType, Action<float> MethodToCall, GameObject TriggerObjToAdd)
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall(TriggerObjToAdd.GetComponent<Slider>().value));
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }

    public void EnableOrbit()
    {
        if (!IsAR)
        {
            CamOrbit.DisableInteration = false;
        }
        else
        {
            ArOrbit.DisableInteration = false;
        }
        if (OptionUnlockedTill < 1)
        {
            if (InformationPanel.gameObject.transform.localScale.x != 1)
            {
                if (TopicToCheck <= (TaskList.Count))
                {
                    iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "time", 0.2f, "easetype", iTween.EaseType.linear));
                    iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));
                    ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen");
                }
            }


            if (TopicToCheck == 2)
            {
                if (MainSlider.GetComponent<Slider>().value < sliderConds[1])
                {
                    Invoke("CallLocalXp", 4.5f);
                    InvokeRepeating("Timer", 0.01f, 0.01f);
                }
                else
                {
                    Invoke("DestroyObj", 0.1f);
                    WrongOptionSelected();
                }
            }
            else if (TopicToCheck == 3)
            {
                if (sliderConds[2] == MainSlider.GetComponent<Slider>().value)
                {
                    Invoke("CallLocalXp", 4.5f);
                    InvokeRepeating("Timer", 0.01f, 0.01f);
                }
                else
                {
                    Invoke("DestroyObj", 0.1f);
                    WrongOptionSelected();
                }
            }
            else if (TopicToCheck == 4)
            {
                if (MainSlider.GetComponent<Slider>().value > sliderConds[2] && MainSlider.GetComponent<Slider>().value < sliderConds[3])
                {
                    Invoke("CallLocalXp", 4.5f);
                    InvokeRepeating("Timer", 0.01f, 0.01f);
                }
                else
                {
                    Invoke("DestroyObj", 0.1f);
                    WrongOptionSelected();
                }
            }
            else if (TopicToCheck == 5)
            {
                if (MainSlider.GetComponent<Slider>().value > sliderConds[4])
                {
                    Invoke("CallLocalXp", 4.5f);
                    InvokeRepeating("Timer", 0.01f, 0.01f);
                }
                else
                {
                    Invoke("DestroyObj", 0.05f);
                    WrongOptionSelected();
                }
            }
        }
        else {

            InvokeRepeating("Timer", 0.01f, 0.01f);
        }
    }

    public void DisableOrbit()
    {
        if (!IsAR)
        {
            CamOrbit.DisableInteration = true;
        }
        else
        {
            ArOrbit.DisableInteration = true;
        }

        Labels[0].gameObject.gameObject.transform.localScale = new Vector3(0, 0, 0);
        
        CancelInvoke("Timer");

        if (obj != null) {
            Destroy(obj);
        }

        obj = Instantiate(BallObj, transform.position, transform.rotation);

        obj.transform.GetChild(0).gameObject.SetActive(false);

        

        obj.GetComponent<Renderer>().enabled = true;

        obj.transform.parent = PullObj.transform;

        obj.transform.localScale = BallObj.transform.localScale;

        obj.transform.localPosition = new Vector3(0.113f, 0.01f, 0);

       // DescriptionText_Parent.transform.localScale = new Vector3(0, 0, 0);

    }

    public Vector3 MidPoint(GameObject obj1, GameObject obj2)
    {
        Vector3 midp = (obj1.transform.localPosition + obj2.transform.localPosition) / 2;
        return midp;
    }

    public float Scale(GameObject obj1, GameObject obj2)
    {
        float scl = Vector3.Distance(obj1.transform.localPosition, obj2.transform.localPosition);
        return scl;
    }

    public void ChangeValue(float val, Text txt)
    {
        Reset();

        PullObj.transform.localEulerAngles = new Vector3(0, 0, -val * 0.79f + 80f);

        txt.text = val.ToString("F2");

        float v = (val) * ((escV - orbV) / 10) + (6 * orbV) - (5 * escV);

        txt.text = v.ToString("F2");

    }

    void Timer()
    {
        if (PullObj.transform.localEulerAngles.z < 80.9f)
        {
            PullObj.transform.localEulerAngles = Vector3.Lerp(PullObj.transform.localEulerAngles, new Vector3(0, 0, 81), Time.deltaTime * 25f);
        }
        else
        {
            CancelInvoke("Timer");
            if (!obj.GetComponent<BallThrow>())
            {
                obj.AddComponent<BallThrow>();
            }
        }
    }
       
    public void ResetAllPanels() {

        //iTween.ScaleTo(PanelLocalXp, iTween.Hash("x", 1, "y", 0, "z", 1, "time", 0.5f, "easetype", iTween.EaseType.linear));

        //ControlPanel.Play("PanelCP");

        //LocalXpPanel.GetComponent<Animator>().Play("localXpPanel");

        //TotalXpPanel.GetComponent<Animator>().Play("TotalXpPointsPanle");

        //infopanelOpen.GetComponent<Animator>().Play("InfoBut_close");
    }

    public void checkButStatus(string arg0)
    {
        if (InputMass.text != "" && InputRad.text != "")
        {
            //CreateBut.GetComponent<Button>().interactable = true;
        }
        else
        {
            //CreateBut.GetComponent<Button>().interactable = false;
        }
    }

    public void PlanetCreation()
    {
        if (InputMass.text != "" && InputRad.text != "")
        {
            if (InputMass.text != "0" && InputRad.text != "0")
            {
                // CreateBut.GetComponent<Button>().interactable = true;
                CreatePlanetWM.transform.localScale = new Vector3(0, 0, 0);

                CpMassText.text = "= " + InputMass.text + " × 10 ²³ Kg";

                CpRadText.text = "= " + InputRad.text + " × 10 ³ Km";

                //CreatePlanetPanel.SetActive(false);
                iTween.ScaleTo(CreatePlanetPanel, iTween.Hash("x", 0, "y", 0, "z", 0, "time", 0.5f, "easetype", iTween.EaseType.spring));
                InfoPanelBg.SetActive(false);
                mVal = float.Parse(InputMass.text);

                rVal = float.Parse(InputRad.text);

                orbV = (Mathf.Sqrt((66.7f * mVal) / rVal));

                CpOrbVText.text = "= " + orbV.ToString("F2") + " Km/s";

                escV = orbV * 1.414f;

                CpEscpVText.text = "= " + escV.ToString("F2") + " Km/s";

                if (!IsAR)
                {
                    CamOrbit.Invoke("StartCamEffect", 0.2f);
                }

                if (OptionUnlockedTill < 1)
                {
                    if (TopicToCheck == 1)
                    {
                        Invoke("CallLocalXp", 0.2f);
                        //AddXpEffect();
                    }
                }

                OpenAllpanels();
            }
            else
            {

                CreatePlanetWM.transform.localScale = new Vector3(0, 0, 0);
                //iTween.ScaleTo(PanelLocalXp, iTween.Hash("x", 1, "y", 0, "z", 1, "time", 0.5f, "easetype", iTween.EaseType.linear));
                iTween.ScaleTo(CreatePlanetWM, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 0.5f, "easetype", iTween.EaseType.spring));
               
            }
        }
        else {
            CreatePlanetWM.transform.localScale = new Vector3(0, 0, 0);
            //iTween.ScaleTo(PanelLocalXp, iTween.Hash("x", 1, "y", 0, "z", 1, "time", 0.5f, "easetype", iTween.EaseType.linear));
            iTween.ScaleTo(CreatePlanetWM, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 0.5f, "easetype", iTween.EaseType.spring));


            //CreateBut.GetComponent<Button>().interactable = false;
        }
    }
    public void CallLocalXp() {
        AddXpEffect();
    }

    public void PlaneCreationClose()
    {
        if (InputMass.text != "" && InputRad.text != "")
        {

            CreatePlanetWM.transform.localScale = new Vector3(0, 0, 0);

            CpMassText.text = "= " + InputMass.text + " × 10 ²³ Kg";

            CpRadText.text = "= " + InputRad.text + " × 10 ³ Km";

            // CreatePlanetPanel.SetActive(false);
            InfoPanelBg.SetActive(false);
            iTween.ScaleTo(CreatePlanetPanel, iTween.Hash("x", 0, "y", 0, "z", 0, "time", 0.5f, "easetype", iTween.EaseType.spring));
            mVal = float.Parse(InputMass.text);

            rVal = float.Parse(InputRad.text);

            orbV = (Mathf.Sqrt((66.7f * mVal) / rVal));

            CpOrbVText.text = "= " + orbV.ToString("F2") + " Km/s";

            escV = orbV * 1.414f;

            CpEscpVText.text = "= " + escV.ToString("F2") + " Km/s";

            if (!IsAR)
            {
                CamOrbit.Invoke("StartCamEffect", 0.2f);
            }
            OpenAllpanels();


        }        
        else
        {
            //CreatePlanetPanel.SetActive(false);
            iTween.ScaleTo(CreatePlanetPanel.gameObject, iTween.Hash("x", 0, "y", 0, "z", 0, "time", 0.5f, "easetype", iTween.EaseType.spring));
            //CreatePlanetPanel.SetActive(false);
            InfoPanelBg.SetActive(false);
            //  iTween()
            OpenInfoPanel();

        }

    }


    public void ResetPlanetFun() {

        CloseAllpanels();
        //CreatePlanetPanel.SetActive(true);
        InfoPanelBg.SetActive(true);
        iTween.ScaleTo(CreatePlanetPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 0.5f, "easetype", iTween.EaseType.spring));

        CamOrbit.distance = 10;
        CamOrbit.cameraRotSide = 180;
    }

    public void ToggleActivateLabels(GameObject but)
    {
        Image I_Image = but.GetComponent<Image>();

        if (I_Image.color == Normal_Color)
        {
            I_Image.color = Highlight_Color;

        }
        else
        {
            I_Image.color = Normal_Color;

        }
    }

    public void Reset()
    {
       

        CancelInvoke("Timer");

    }

    public void OnInputvalueEnd(int a)
    {

    }


    public void DestroyObj()
    {
        Destroy(obj);
       
        OpenAllpanels();
    }    
}

public class BallThrow : MonoBehaviour
{
    public GameObject obj, PlanetAsset,MainObj;

    public float d, v, ini, t, r, tttt, R, Valu;

    public Slider MainSlider;

    public TextMeshPro DescText;

    GameObject Lab;
    public void Start()
    {

        MainObj = GameObject.Find("MainObject").transform.parent.gameObject;

        PlanetAsset = GameObject.Find("PlanetAsset");

        obj = this.gameObject;
        
        R = GameObject.Find("planet_X").transform.localScale.x;

        obj.transform.parent = MainObj.transform;

        obj.GetComponent<TrailRenderer>().enabled = true;
        obj.GetComponent<TrailRenderer>().time = 2f;

        obj.GetComponent<TrailRenderer>().startWidth = 0.5f * (obj.transform.localScale.x);
        //obj.transform.parent = null;

        d = Vector3.Distance(obj.transform.localPosition, PlanetAsset.transform.localPosition);

        v = -0.1f;

        ini = Mathf.Atan(obj.transform.localPosition.y / obj.transform.localPosition.x);

        t = ini * Mathf.Rad2Deg;      

        r = d - R;

        tttt = 0;

        MainSlider = GameObject.Find("MainSlider").GetComponent<Slider>();

        InvokeRepeating("Timer2", 0.001f, 0.01f);

        Valu = MainSlider.value;

        Lab = GameObject.Find("Labels");

        DescText =GameObject.Find("PopUpTxt2").GetComponentInChildren<TextMeshPro>();

        vid176_escapevelocity.Instance.CloseAllpanels();

       
    }

    public void Timer2()
    {
        Invoke("DestroyObj", 5);
        t = t + 1f;

        if (Valu < 50f)
        {
            if (r + R > (0.53f))
            {
                r = r - (0.0025f * (10f / (MainSlider.value * 1)));
                float x = PlanetAsset.transform.localPosition.x + (R + r) * Mathf.Cos((t * Mathf.Deg2Rad));
                float y = PlanetAsset.transform.localPosition.y + (R + r) * Mathf.Sin((t * Mathf.Deg2Rad));
                obj.transform.localPosition = new Vector3(x, y, 0);
            }
            else
            {
                CancelInvoke("Timer2");
                obj.GetComponent<Renderer>().enabled = false;
                obj.transform.GetChild(0).gameObject.SetActive(true);

                Lab.transform.GetChild(0).gameObject.transform.localScale = new Vector3(0, 0, 0);

                iTween.ScaleTo(Lab.transform.GetChild(0).gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 0.5f, "easetype", iTween.EaseType.spring));

                DescText.text = "Velocity is less than orbital velocity, Ball falls on the planet";
            }           
        }
        else if (Valu == 50f)
        {
            if (t < (360f + 90f))
            {
                float x = PlanetAsset.transform.localPosition.x + (R + r) * Mathf.Cos((t * Mathf.Deg2Rad));
                float y = PlanetAsset.transform.localPosition.y + (R + r) * Mathf.Sin((t * Mathf.Deg2Rad));
                obj.transform.localPosition = new Vector3(x, y, 0);
            }
            else
            {
                CancelInvoke("Timer2");
                obj.GetComponent<Renderer>().enabled = false;
                obj.transform.GetChild(0).gameObject.SetActive(true);
              
                Lab.transform.GetChild(0).gameObject.transform.localScale = new Vector3(0, 0, 0);

                iTween.ScaleTo(Lab.transform.GetChild(0).gameObject.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 0.5f, "easetype", iTween.EaseType.spring));
                
                DescText.text = "Velocity is equal to orbital velocity, Ball moves in planet's orbit";
            }            
        }
        else if (Valu > 50f && Valu < 60f)
        {

            if (t < (360f + 90f))
            {
                //r = r + (0.0005f * ( MainSlider.value*0.5f));
                float x = PlanetAsset.transform.localPosition.x + (R + r + 0.25f) * Mathf.Cos((t * Mathf.Deg2Rad));
                float y = PlanetAsset.transform.localPosition.y + (R + r) * Mathf.Sin((t * Mathf.Deg2Rad));
                obj.transform.localPosition = new Vector3(x, y, 0);
            }
            else
            {
                CancelInvoke("Timer2");
                obj.GetComponent<Renderer>().enabled = false;
                obj.transform.GetChild(0).gameObject.SetActive(true);            

                Lab.transform.GetChild(0).gameObject.gameObject.transform.localScale = new Vector3(0, 0, 0);

                iTween.ScaleTo(Lab.transform.GetChild(0).gameObject.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 0.5f, "easetype", iTween.EaseType.spring));

                DescText.text = "Velocity is less than escape velocity, Ball moves in planet's orbit";
            }            
        }

        else
        {
            tttt = tttt + 0.1f;
            
            float Vx = PlanetAsset.transform.localPosition.x + Mathf.Sqrt(Valu * 0.01f) * Mathf.Sin(tttt * Mathf.Deg2Rad);
            float Vy = PlanetAsset.transform.localPosition.y + Mathf.Sqrt(Valu * 0.01f) * Mathf.Cos(tttt * Mathf.Deg2Rad);
            obj.transform.localEulerAngles = new Vector3(0, 0, 80);
            obj.transform.Translate(-Vx * Time.deltaTime * 5, ((Vy - (9.8f * Valu * 0.0005f)) / 3) * Time.deltaTime * 5, 0);

            if (tttt > 50)
            {
                CancelInvoke("Timer2");

                Lab.transform.GetChild(0).gameObject.gameObject.transform.localScale = new Vector3(0, 0, 0);

                iTween.ScaleTo(Lab.transform.GetChild(0).gameObject.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 0.5f, "easetype", iTween.EaseType.spring));

                DescText.text = "Velocity is greater than escape velocity, Ball escapes the planet orbit";
            }

           
        }
    }
    public void DestroyObj()
    {
        Destroy(obj);
        MainSlider.gameObject.GetComponent<Animator>().Play("SliderIn");
        vid176_escapevelocity.Instance.OpenAllpanels();
       
    }
}



