﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;
using System.Linq;
using System;
using System.Text.RegularExpressions;
using UnityEngine.SceneManagement;

public class vid402_findingareaundercurve : MonoBehaviour
{
    public AssetBundle assetBundle;
    public LayerMask layer ;
    public TextAsset CSVFile;
    public bool InEquation1,IsLockClicked;
    public AROrbitControls ArOrbit;
    public float a_Value, b_Value,Delta_X,n_Value=1,InvokeTimer,EstimatedArea,FinalArea, a_Value_Ref, b_Value_Ref;
    public GameObject LineToDisplay1, LineToDisplay2, LinePositions;
    public List<GameObject> LinesToDisplay_Eq1, LinesToDisplay_Eq2;
    RaycastHit Hit;
    public orbit CamOrbit;
    public float scale_value;
    private int old_sel_btn;
    private Color old_color;
    public Material TransparentMaterial;

    public Text a_Val_Text, b_Val_Text, n_Val_Text, Delta_X_Text;
    public Button a_Increment, a_Decrement, b_Increment, b_Decrement,LockValues_Btn,Equation1,Equation2;
    public bool IsAR, IsUIZooming;
    public int OptionUnlockedTill, CurrentTopicSelected, TopicToCheck;
    public float Sub_XP_Earned, Total_XP;
    public Dropdown ToDoList_DD;
    public Slider ValueDisplaySlider;
    public GameObject InstructionPanel_DD, ObjectivePanel, InstructionPanel, InfoPanel, GlassPanel, Canvas_BG, SidePannel;
    public Button Info_Close_Btn, Info_Cntue_Btn, Info_Open_Btn, SidePannelOpen, Reset_Btn;
    public List<Dictionary<string, object>> CSV_data;
    public List<String> Topics, Sub_Topics, Sub_Top_ToDoList, Sub_Top_Desc, Sub_Top_XP, LearningGoals;
    public Text XP_ToShow, XP_Effect, Main_XP_ToShow, Learninggoal_Txt, Instruction_Txt, Assumption_Txt, Heading_Txt,ActualArea_Txt,EstimatedArea_Txt;
    public bool IsTopicUnlocked;
    public Canvas UIcanvas;

    // Start is called before the first frame update
    void Start()
    {
        //OptionUnlockedTill = 1;

        layer = 2;

        UIcanvas = GameObject.Find("Canvas_Integration").GetComponent<Canvas>();

        LoadFromAsssetBundle();



        ReadingDataFromCSVFile();
        ReadingXps();

        FindGameObjects();

        AssignEventsToObjects();

        

    }

    public void LoadFromAsssetBundle()
    {
        //if (Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.Android)
        //{
        assetBundle = AssetBundle.LoadFromFile(PlayerPrefs.GetString("AssetBundleUrl"));

        CSVFile = assetBundle.LoadAsset(PlayerPrefs.GetString("LibraryName") + "CSV", typeof(TextAsset)) as TextAsset;

        TransparentMaterial = assetBundle.LoadAsset("TransparentMaterial", typeof(Material)) as Material;
        TransparentMaterial.shader = Shader.Find("Standard");




        //}
        //else
        //{

        //string CSVName = transform.parent.gameObject.name;



        //if (CSVName.Contains("Clone"))
        //{



        //    CSVName = CSVName.Substring(0, CSVName.Length - 7);
        //}



        //print("casvname........." + CSVName);



        //CSVFile = Resources.Load(CSVName + "CSV") as TextAsset;
        //}







        Camera.main.gameObject.AddComponent<PhysicsRaycaster>();



        Camera.main.backgroundColor = Color.black;



        GameObject TargetForCamera = GameObject.Find("Target");



        if (TargetForCamera != null)
        {


               TargetForCamera.AddComponent<orbit>();
                CamOrbit = TargetForCamera.GetComponent<orbit>();
                CamOrbit.zoomMax = 3f;
                CamOrbit.zoomMin = 0.7f;
                CamOrbit.zoomStart = 3f;
                CamOrbit.distance = CamOrbit.zoomStart;
                CamOrbit.transform.position = new Vector3(0, 0.4f, 0);

            CamOrbit.minSideRot = 45;
            CamOrbit.maxSideRot = 45;

            CamOrbit.maxRotUp = 25;
            CamOrbit.minRotUp = 0;

            

            IsAR = false;
        }
        else
        {
            this.transform.parent.gameObject.AddComponent<AROrbitControls>();
            ArOrbit = this.transform.parent.gameObject.GetComponent<AROrbitControls>();
            IsAR = true;
        }

        

    }

    public void ZoomCamera()
    {

        //Camera.main.gameObject.AddComponent<PhysicsRaycaster>();
        UIcanvas.GetComponent<GraphicRaycaster>().enabled = true;
        if (!IsAR)
        {
           CamOrbit.zoomMax = 3f;
            if(InEquation1)
            CamOrbit.zoomMin = 0.7f;
            else
                CamOrbit.zoomMin = 0.9f;
            CamOrbit.zoomStart = 1.5f;
            CamOrbit.distance = CamOrbit.zoomStart;
        }
    }

    public void ReadingDataFromCSVFile()
    {
        Topics = new List<string>();
        Sub_Topics = new List<string>();
        Sub_Top_Desc = new List<string>();
        Sub_Top_ToDoList = new List<string>();
        Sub_Top_XP = new List<string>();
        LearningGoals = new List<string>();

        CSV_data = CSVReader.Read(CSVFile);


        for (var i = 0; i < CSV_data.Count; i++)
        {
            //print("Topics " + CSV_data[i]["Learninggoals"] + ".......... " +"ToDo " + CSV_data[i]["Topic_TodoList"]);



            if (CSV_data[i]["Topic_ToDoList"].ToString() != "" && CSV_data[i]["Topic_ToDoList"].ToString() != null)
            {
                Sub_Top_ToDoList.Add(CSV_data[i]["Topic_ToDoList"].ToString());

            }



            //if (CSV_data[i]["Top_XP"].ToString() != "" && CSV_data[i]["Top_XP"].ToString() != null)
            //{
            //    Sub_Top_XP.Add(CSV_data[i]["Top_XP"].ToString());

            //}

            if (CSV_data[i]["Learninggoals"].ToString() != "" && CSV_data[i]["Learninggoals"].ToString() != null)
            {
                LearningGoals.Add(CSV_data[i]["Learninggoals"].ToString());

            }

        }






    }


    public int totXp, loclXp, PerTopic_XP;

    public void ReadingXps()
    {

        //PlayerPrefs.SetInt("TotalXP", 34);
        //PlayerPrefs.SetInt("MaxEarnings", 39);

        totXp = PlayerPrefs.GetInt("TotalXP");
        loclXp = PlayerPrefs.GetInt("MaxEarnings");

        TopicToCheck = 0;

        if (loclXp != 0)
        {
            OptionUnlockedTill = 0;

        }
        else
        {
            OptionUnlockedTill = 1;

        }


        



        int t = 0;
        int p = 0;

        for (int k = 0; k < Sub_Top_ToDoList.Count; k++)
        {

            p = loclXp / Sub_Top_ToDoList.Count;
            Sub_Top_XP.Add(p.ToString());
            t = t + p;
        }

        if (t != loclXp)
        {
            Sub_Top_XP[Sub_Top_ToDoList.Count - 1] = (p + (loclXp - t)).ToString();
        }

        Total_XP = loclXp;
        

    }



    public float result;

    public void GetResult()
    {
        result = ((float)(TopicToCheck - 0) / (float)(Sub_Top_ToDoList.Count)) * 100;
        Camera.main.SendMessage("UnloadActually", result);
    }

    public void FindGameObjects()
    {
        LineToDisplay1 = GameObject.Find("LineToDisplay1");
        LineToDisplay2 = GameObject.Find("LineToDisplay2");
        //LinePositions = GameObject.Find("LinesPos");

        LinesToDisplay_Eq1 = new List<GameObject>();

        for (int i = 0; i <= 10; i++)
        {
            GameObject Line =Instantiate(LineToDisplay1, LineToDisplay1.transform.localPosition,Quaternion.identity, LineToDisplay1.transform.parent);
            LinesToDisplay_Eq1.Add(Line);
            Line.SetActive(false);

            GameObject Line1 = Instantiate(LineToDisplay2, LineToDisplay2.transform.localPosition, Quaternion.identity, LineToDisplay2.transform.parent);
            LinesToDisplay_Eq2.Add(Line1);
            Line1.SetActive(false);
        }

        LineToDisplay1.SetActive(false);
        LineToDisplay2.SetActive(false);


        a_Val_Text = GameObject.Find("a_Value").GetComponent<Text>();
        b_Val_Text = GameObject.Find("b_Value").GetComponent<Text>();
        n_Val_Text = GameObject.Find("n_Value_Text").GetComponent<Text>();
        Delta_X_Text = GameObject.Find("Delta_X_Value").GetComponent<Text>();

        a_Increment = GameObject.Find("a_Increment").GetComponent<Button>();
        a_Decrement = GameObject.Find("a_Decrement").GetComponent<Button>();
        b_Increment = GameObject.Find("b_Increment").GetComponent<Button>();
        b_Decrement = GameObject.Find("b_Decrement").GetComponent<Button>();
        LockValues_Btn = GameObject.Find("LockValues").GetComponent<Button>();
        Equation1 = GameObject.Find("Equation1_Button").GetComponent<Button>();
        Equation2 = GameObject.Find("Equation2_Button").GetComponent<Button>();

        Canvas_BG = GameObject.Find("Canvas_BG");

        if (!IsAR)
        {
            Canvas_BG.transform.parent = Camera.main.transform;
            Canvas_BG.transform.GetComponentInChildren<RawImage>().enabled = true;
            Canvas_BG.GetComponent<Canvas>().worldCamera = Camera.main;
        }

        else
        {
            Canvas_BG.SetActive(false);
        }
        //GameObject content = GameObject.Find("Content");


        SidePannel = GameObject.Find("SidePanel");
        SidePannelOpen = GameObject.Find("PannelOpenButton").GetComponent<Button>();
        SidePannelOpen.gameObject.SetActive(false);
        ValueDisplaySlider = GameObject.Find("ValueDisplaySlider").GetComponent<Slider>();
        InstructionPanel_DD = GameObject.Find("DD_Instruction");
        InstructionPanel = GameObject.Find("InstructionPanel");
        ToDoList_DD = GameObject.Find("ToDoList_Dropdown").GetComponent<Dropdown>();
        XP_ToShow = GameObject.Find("Gained_XP_Value").GetComponent<Text>();
        XP_Effect = GameObject.Find("Effect_Text").GetComponent<Text>();
        Main_XP_ToShow = GameObject.Find("Main_XP_Value").GetComponent<Text>();
        ObjectivePanel = GameObject.Find("ObjectivePanel");
        InstructionPanel.transform.localScale = Vector3.zero;
        InfoPanel = GameObject.Find("InformationPanel");
        Info_Close_Btn = GameObject.Find("InfoCloseBtn").GetComponent<Button>();
        Info_Cntue_Btn = GameObject.Find("InfoContinueBtn").GetComponent<Button>();
        Info_Open_Btn = GameObject.Find("InfoButton").GetComponent<Button>();
        Info_Close_Btn.gameObject.SetActive(false);
        GlassPanel = GameObject.Find("GlassEffect");
        Instruction_Txt = GameObject.Find("IListText").GetComponent<Text>();
        Assumption_Txt = GameObject.Find("AListText").GetComponent<Text>();
        Learninggoal_Txt = GameObject.Find("LGListText").GetComponent<Text>();
        Heading_Txt = GameObject.Find("InfoHeadingText").GetComponent<Text>();
        Reset_Btn = GameObject.Find("Reset_Exp").GetComponent<Button>();


        ActualArea_Txt = GameObject.Find("ActualArea_Value").GetComponent<Text>();
        EstimatedArea_Txt = GameObject.Find("EstimatedArea_Value").GetComponent<Text>();

        if (OptionUnlockedTill == 0)
        {
            /*ToDoList_DD.GetComponent<RectTransform>().anchoredPosition = new Vector3(150f, -150f, 0);
            XP_ToShow.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(150f, -150f, 0);
            SidePannel.GetComponent<RectTransform>().anchoredPosition = new Vector3(-600f, 0f, 0);*/
            ObjectivePanel.transform.localScale = new Vector3(1, 1, 1);
            IsTopicUnlocked = false;
        }
        else
        {
            ToDoList_DD.GetComponent<RectTransform>().anchoredPosition = new Vector3(-250f, -150f, 0);
            XP_ToShow.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(-250f, -150f, 0);
            ObjectivePanel.transform.localScale = new Vector3(1, 0, 1);
            IsTopicUnlocked = true;
        }

        Main_XP_ToShow.text = totXp.ToString();

        XP_ToShow.text = "0/" + Total_XP;


        for (int i = 0; i < Sub_Top_ToDoList.Count; i++)
        {

            ToDoList_DD.options.Add(new Dropdown.OptionData() { text = Sub_Top_ToDoList[i] });


            //List<float> TotalXP = Sub_Top_XP.Select(s => float.Parse(s)).ToList();

            //Total_XP += TotalXP[i];

            //XP_ToShow.text = "0/" + Total_XP;


            ObjectivePanel.transform.Find("ObjectiveText").GetComponent<Text>().text = Sub_Top_ToDoList[TopicToCheck];




        }


        GlassPanel.transform.localScale = Vector3.zero;
        InfoPanel.transform.localScale = Vector3.zero;
        Invoke("OpenInfoPanel", 0.1f);

        Heading_Txt.text = LearningGoals[0];
        Learninggoal_Txt.text = LearningGoals[1];
        //Instruction_Txt.text = LearningGoals[2];
        //Assumption_Txt.text = LearningGoals[3];

        SwitchEquation(-1);
        
    }


    public void AssignEventsToObjects()
    {





        SidePannelOpen.onClick.AddListener(OpenSidePannel);
        //TopSidePannelOpen.onClick.AddListener(OpenTopSidePannel);
        


        EventTrigger.Entry PointerDown = new EventTrigger.Entry();
        PointerDown.eventID = EventTriggerType.EndDrag;
        PointerDown.callback.AddListener((data) => { DisableOrbit(); });

        //ScrollBarViewPort.gameObject.GetComponent<EventTrigger>().triggers.Add(PointerDown);

        EventTrigger.Entry PointerUp = new EventTrigger.Entry();
        PointerUp.eventID = EventTriggerType.BeginDrag;
        PointerUp.callback.AddListener((data) => { EnableOrbit(); });

        //ScrollBarViewPort.gameObject.GetComponent<EventTrigger>().triggers.Add(PointerUp);

        /*for (int i = 0; i < SelectionButtons.Length; i++)
        {
            int j = i;
            SelectionButtons[i].onClick.AddListener(() => PropertyToDisplay(j));
            // print(SelectionButtons[i].name);
            old_color = SelectionButtons[0].transform.GetChild(1).GetComponent<Text>().color;
        }*/
        ToDoList_DD.GetComponent<EventTrigger>().triggers.Clear();

        AddListenerToEvents(EventTriggerType.PointerClick, ExploringToDOList_DD, ToDoList_DD.gameObject);
        AddListenerToEvents(EventTriggerType.Select, ToDoList_DDCancel, ToDoList_DD.gameObject);
        Info_Close_Btn.onClick.AddListener(() => CloseInfoPanel(Info_Close_Btn));
        Info_Cntue_Btn.onClick.AddListener(() => CloseInfoPanel(Info_Cntue_Btn));
        Info_Open_Btn.onClick.AddListener(OpenInfoPanel);

        a_Increment.onClick.AddListener(() => IncrementValue(a_Increment));
        b_Increment.onClick.AddListener(() => IncrementValue(b_Increment));
        a_Decrement.onClick.AddListener(() => DecrementValue(a_Decrement));
        b_Decrement.onClick.AddListener(() => DecrementValue(b_Decrement));
        LockValues_Btn.onClick.AddListener(() => LockValues(false));
        ValueDisplaySlider.onValueChanged.AddListener(delegate { Change_SliderValue(ValueDisplaySlider.value); });
        AddListenerToEvents(EventTriggerType.PointerUp, EnableOrbit, ValueDisplaySlider.gameObject);
        AddListenerToEvents(EventTriggerType.PointerDown, DisableOrbit, ValueDisplaySlider.gameObject);
        AddListenerToEvents(EventTriggerType.PointerUp, SliderConditionCheck, ValueDisplaySlider.gameObject);


        Equation1.onClick.AddListener(() => SwitchEquation(0));
        Equation2.onClick.AddListener(() => SwitchEquation(1));
        /*AddListenerToEvents(EventTriggerType.BeginDrag, OnElementBeginDrag, GameObject.Find("RawImage"));
        AddListenerToEvents(EventTriggerType.EndDrag, OnElementEndDrag, GameObject.Find("RawImage"));
        AddListenerToEvents(EventTriggerType.PointerClick, BackToPT, GameObject.Find("RawImage"));*/

    }

    public void IncrementValue(Button Btn)
    {
        if (Btn.name == "a_Increment")
        {
            if (!IsTopicUnlocked)
            {
                if (TopicToCheck == 1 || TopicToCheck == 2 || TopicToCheck == 3 || TopicToCheck == 4)
                {
                    if (CheckFor_XP_Condition(TopicToCheck))
                        return;
                }

            }
           
            if (InEquation1)
            {
                if (a_Value < 2)
                    a_Value++;
            }
            else
            {
                if (a_Value < 5)
                    a_Value++;
            }
            a_Val_Text.text = a_Value.ToString();

           
        }
        else
        {
            if (!IsTopicUnlocked)
            {
                if (TopicToCheck == 0 || TopicToCheck == 2 || TopicToCheck == 3 || TopicToCheck == 4)
                {
                    if (CheckFor_XP_Condition(TopicToCheck))
                        return;
                }

            }


            if (InEquation1)
            {
                if (b_Value < 2)
                    b_Value++;


            }
            else
            {

                if (b_Value < 5)
                    b_Value++;
            }
           
            b_Val_Text.text = b_Value.ToString();

        }
        a_Value_Ref = a_Value;
        b_Value_Ref = b_Value;

    }

    public void DecrementValue(Button Btn)
    {
        if (Btn.name == "a_Decrement")
        {
            
            if (InEquation1)
            {
                if (a_Value > -2)
                    a_Value--;

            }
            else
            {

                if (a_Value > 0)
                    a_Value--;
            }

            if (!IsTopicUnlocked)
            {
                if (TopicToCheck == 1 || TopicToCheck == 2 || TopicToCheck == 3 || TopicToCheck == 4)
                {
                    if (CheckFor_XP_Condition(TopicToCheck))
                    {
                         a_Value++;
                        return;
                    }
                }
                else
                {
                    if (a_Value == -2)
                        CheckFor_XP_Condition(TopicToCheck);
                }

            }

            a_Val_Text.text = a_Value.ToString();

        }
        else
        {
            

            if (InEquation1)
            {
                if (b_Value > -2)
                    b_Value--;

            }
            else
            {
                if (b_Value > 0)
                    b_Value--;
            }


            if (!IsTopicUnlocked)
            {
                if (TopicToCheck == 0 || TopicToCheck == 2 || TopicToCheck == 3 || TopicToCheck == 4)
                {
                    if (CheckFor_XP_Condition(TopicToCheck))
                    {
                        b_Value++;
                        return;
                    }
                }
                else
                {
                    if (b_Value == -1)
                        CheckFor_XP_Condition(TopicToCheck);
                }

            }

            b_Val_Text.text = b_Value.ToString();

        }
        a_Value_Ref = a_Value;
        b_Value_Ref = b_Value;
    }


    public void LockValues(bool ToApply)
    {
        if (!IsTopicUnlocked)
        {
            if (TopicToCheck == 0 || TopicToCheck == 1 || TopicToCheck == 3 || TopicToCheck == 4)
            {
                if (CheckFor_XP_Condition(TopicToCheck))
                    return;
            }
            else
            {
                if (a_Value == -2 && b_Value == -1)
                    IsLockClicked = true;

                CheckFor_XP_Condition(TopicToCheck);
            }

        }

        if (InEquation1)
        {
            if (a_Value == -2 && b_Value == -1)
            {
                a_Increment.interactable = ToApply;
                b_Increment.interactable = ToApply;
                a_Decrement.interactable = ToApply;
                b_Decrement.interactable = ToApply;

                a_Increment.GetComponent<Image>().color = new Color(1,1,1,0.5f);
                b_Increment.GetComponent<Image>().color = new Color(1, 1, 1, 0.5f);
                a_Decrement.GetComponent<Image>().color = new Color(1, 1, 1, 0.5f);
                b_Decrement.GetComponent<Image>().color = new Color(1, 1, 1, 0.5f);

                LockValues_Btn.interactable = false;
                LockValues_Btn.GetComponent<Image>().color = new Color(1, 1, 1, 0.5f);

                ValueDisplaySlider.interactable = true;
                ShowCalculation();
            }
            else
            {
                if(IsTopicUnlocked)
                ShowInstructionPanel("Wrong values chosen");
            }
        }
        else
        {
            if (a_Value == 4 && b_Value == 5)
            {
                a_Increment.interactable = ToApply;
                b_Increment.interactable = ToApply;
                a_Decrement.interactable = ToApply;
                b_Decrement.interactable = ToApply;

                a_Increment.GetComponent<Image>().color = new Color(1, 1, 1, 0.5f);
                b_Increment.GetComponent<Image>().color = new Color(1, 1, 1, 0.5f);
                a_Decrement.GetComponent<Image>().color = new Color(1, 1, 1, 0.5f);
                b_Decrement.GetComponent<Image>().color = new Color(1, 1, 1, 0.5f);

                LockValues_Btn.interactable = false;
                LockValues_Btn.GetComponent<Image>().color = new Color(1, 1, 1, 0.5f);

                ValueDisplaySlider.interactable = true;
                ShowCalculation();
            }
            else
            {
                if (IsTopicUnlocked)
                    ShowInstructionPanel("Wrong values chosen");
            }
        }

    }

    public void ShowCalculation()
    {
        Delta_X = (b_Value - a_Value) /n_Value;

        Delta_X_Text.text =Delta_X.ToString();
        EstimatedArea = 0;

        if (InEquation1)
        {
            
            for (int i = 0; i < n_Value; i++)
            {
                float x_Square = 1 / n_Value;
                x_Square = -2+(x_Square*i);
                //print("i....."+i+"...X_S....."+x_Square);
                float Area =4/(x_Square*x_Square);
                EstimatedArea += Area;
            }
            EstimatedArea = EstimatedArea * Delta_X;
            //print("Area......" + EstimatedArea);

            ActualArea_Txt.text = "2";
            EstimatedArea_Txt.text = EstimatedArea.ToString("0.00");
        }
        else
        {

            for (int i = 0; i < n_Value; i++)
            {
                float x_Square = 1 / n_Value;
                x_Square = (x_Square * i) + 4;
                //print("i....." + i + "...X_S....." + x_Square);
                float Area = Mathf.Sqrt(x_Square);
                EstimatedArea += Area;
            }
            EstimatedArea = EstimatedArea * Delta_X;
            //print("Area......" + EstimatedArea);

            ActualArea_Txt.text = "2.12";
            EstimatedArea_Txt.text = EstimatedArea.ToString("0.00");
        }

        
    }

    public void Change_SliderValue(float SliderVal)
    {
        if(!IsTopicUnlocked)
        {
            if (TopicToCheck != 3 && ValueDisplaySlider.interactable)
            {
                if(CheckFor_XP_Condition(TopicToCheck))
                {
                    ValueDisplaySlider.value = n_Value;
                    return;

                }
                
            }
                

        }
            
        n_Value = SliderVal;
        n_Val_Text.text = "n = "+SliderVal.ToString();
        //DisplayLines = true;
        InvokeTimer = 0;
        if (!LockValues_Btn.interactable)
        {
            CancelInvoke("ChangeDisplayLines");
            InvokeRepeating("ChangeDisplayLines", 0f, 0.02f);
        }
        ShowCalculation();

        //ShowLines((int)SliderVal);
    }

    public void SliderConditionCheck()
    {
        if (!IsTopicUnlocked)
        {
            if (TopicToCheck == 3)
            {
                CheckFor_XP_Condition(TopicToCheck);

            }

        }
    }

    public void ChangeDisplayLines()
    {
        if (InvokeTimer < 3)
        {
            InvokeTimer++;
            ShowLines((int)ValueDisplaySlider.value);
        }
        else
        {
            CancelInvoke("ChangeDisplayLines");

        }
         
    }

    public void ShowLines(int NumOfLines)
    {
        if (InEquation1)
        {
            for (int i = 0; i < LinesToDisplay_Eq1.Count; i++)
            {
                LinesToDisplay_Eq1[i].SetActive(false);
                //LinesToDisplay[i].transform.localScale = new Vector3((0.144f / NumOfLines), 0.2f, LinesToDisplay[i].transform.localScale.z);

            }
        }
        else
        {
            for (int i = 0; i < LinesToDisplay_Eq2.Count; i++)
            {
                LinesToDisplay_Eq2[i].SetActive(false);
                //LinesToDisplay[i].transform.localScale = new Vector3((0.144f / NumOfLines), 0.2f, LinesToDisplay[i].transform.localScale.z);

            }
        }


        for (int i = 0; i < NumOfLines; i++)
        {


            if (InEquation1)
            {


                LinesToDisplay_Eq1[i].SetActive(true);

                float Scale = 0;
                LinesToDisplay_Eq1[i].transform.localPosition = new Vector3(LineToDisplay1.transform.localPosition.x - (-0.01383f * i * 10 / NumOfLines), LineToDisplay1.transform.localPosition.y, LineToDisplay1.transform.localPosition.z);

                //Debug.DrawRay(LinesToDisplay_Eq1[i].transform.localPosition, LinesToDisplay_Eq1[i].transform.up, Color.red);
                if (Physics.Raycast(LinesToDisplay_Eq1[i].transform.position, LinesToDisplay_Eq1[i].transform.up, out Hit, 1000f, layer))
                {


                    Scale = (transform.InverseTransformPoint(Hit.point).y - LinesToDisplay_Eq1[i].transform.localPosition.y);
                    //print("i........." +i + "...obj...." + Hit.collider + "...point...." + Hit.point.y);
                }


                //LinesToDisplay[i].transform.localScale = new Vector3((0.144f / NumOfLines), Scale, LinesToDisplay[i].transform.localScale.z);
                LinesToDisplay_Eq1[i].GetComponent<SpriteRenderer>().size = new Vector2(0.96f / NumOfLines, Scale / LineToDisplay1.transform.localPosition.y);
                LinesToDisplay_Eq1[i].transform.GetChild(0).localPosition = new Vector3(-0.1f, (Scale / LineToDisplay1.transform.localPosition.y) + 0.18f, 0);
                LinesToDisplay_Eq1[i].GetComponentInChildren<TextMeshPro>().text = "X<sub>" + i + "<sub>";
            }
            else
            {

                LinesToDisplay_Eq2[i].SetActive(true);

                float Scale = 0f;

                LinesToDisplay_Eq2[i].transform.localPosition = new Vector3(LineToDisplay2.transform.localPosition.x - (-0.0124f * i * 10 / NumOfLines), LineToDisplay2.transform.localPosition.y, LineToDisplay2.transform.localPosition.z);

                if (Physics.Raycast(LinesToDisplay_Eq2[i].transform.position, LinesToDisplay_Eq2[i].transform.up, out Hit, 1000f, layer))
                {

                    //Debug.DrawRay(transform.localPosition, Vector3.up, Color.red);
                    Scale = (transform.InverseTransformPoint(Hit.point).y - LinesToDisplay_Eq2[i].transform.localPosition.y);
                    //print("i........." +i + "...point...." + Hit.point.y);
                }


                //LinesToDisplay[i].transform.localScale = new Vector3((0.144f / NumOfLines), Scale, LinesToDisplay[i].transform.localScale.z);
                LinesToDisplay_Eq2[i].GetComponent<SpriteRenderer>().size = new Vector2(0.87f / NumOfLines, Scale / LineToDisplay2.transform.localPosition.y);
                LinesToDisplay_Eq2[i].transform.GetChild(0).localPosition = new Vector3(0f, (Scale / LineToDisplay2.transform.localPosition.y) + 0.15f, 0);
                LinesToDisplay_Eq2[i].GetComponentInChildren<TextMeshPro>().text = "X<sub>" + i + "<sub>";
            }
        }

        //if (NumOfLines % 2 == 0)
        //{

        //    for (int i = 0; i < NumOfLines; i++)
        //    {
        //        int Cnt = 10/NumOfLines;
        //        LinesToDisplay[i].SetActive(true);
        //        LinesToDisplay[i].transform.localPosition = LinePositions.transform.GetChild(Cnt).transform.localPosition;
        //        LinesToDisplay[i].transform.localScale = new Vector3(0.016f * Cnt, LinePositions.transform.GetChild(i).transform.localPosition.y - (Cnt*0.115f), 0.00026148f);
        //    }
        //}

    }

    public void SwitchEquation(int Equation)
    {


        if (!IsTopicUnlocked)
        {
            if (TopicToCheck == 0 || TopicToCheck == 1 || TopicToCheck == 2 || TopicToCheck == 3)
            {
                if (CheckFor_XP_Condition(TopicToCheck) && Equation != -1)
                    return;
            }


        }

        a_Value = 0;
        b_Value = 0;
        a_Val_Text.text = a_Value.ToString();
        b_Val_Text.text = b_Value.ToString();
        a_Increment.interactable = true;
        b_Increment.interactable = true;
        a_Decrement.interactable = true;
        b_Decrement.interactable = true;

        a_Increment.GetComponent<Image>().color = new Color(1, 1, 1, 1);
        b_Increment.GetComponent<Image>().color = new Color(1, 1, 1, 1);
        a_Decrement.GetComponent<Image>().color = new Color(1, 1, 1, 1);
        b_Decrement.GetComponent<Image>().color = new Color(1, 1, 1, 1);

        LockValues_Btn.interactable = true;
        LockValues_Btn.GetComponent<Image>().color = new Color(1, 1, 1, 1);

        ValueDisplaySlider.interactable = false;
        ValueDisplaySlider.value = 1;
        Change_SliderValue(1);
        ActualArea_Txt.text = "";
        EstimatedArea_Txt.text = "";


        if (SidePannelOpen.transform.eulerAngles.z == 180)
            OpenSidePannel();
        if (Camera.main.GetComponent<PhysicsRaycaster>() && Equation != -1)
            Invoke("ZoomCamera", 1.5f);



        if (Equation == -1)
        {
            InEquation1 = true;
            Equation1.GetComponent<Image>().sprite = Equation1.spriteState.pressedSprite;
            Equation2.GetComponent<Image>().sprite = Equation2.spriteState.disabledSprite;
            transform.GetChild(0).gameObject.SetActive(true);
            transform.GetChild(1).gameObject.SetActive(false);
            if (!IsAR)
            {
                CamOrbit.zoomStart = 3;
                CamOrbit.distance = CamOrbit.zoomStart;
                CamOrbit.transform.position = new Vector3(0, 0.4f, 0);

            }


            return;
        }


        

        if (Equation == 0)
        {
            InEquation1 = true;
            Equation1.GetComponent<Image>().sprite = Equation1.spriteState.pressedSprite;
            Equation2.GetComponent<Image>().sprite = Equation2.spriteState.disabledSprite;
            transform.GetChild(0).gameObject.SetActive(true);
            transform.GetChild(1).gameObject.SetActive(false);
            if (!IsAR)
            {
                CamOrbit.zoomStart =3;
                CamOrbit.distance = CamOrbit.zoomStart;
                CamOrbit.transform.position = new Vector3(0, 0.4f, 0);
                
            }
        }
        else
        {
            InEquation1 = false;
            Equation2.GetComponent<Image>().sprite = Equation2.spriteState.pressedSprite;
            Equation1.GetComponent<Image>().sprite = Equation1.spriteState.disabledSprite;
            transform.GetChild(1).gameObject.SetActive(true);
            transform.GetChild(0).gameObject.SetActive(false);
            if (!IsAR)
            {
                CamOrbit.zoomStart = 3;
                CamOrbit.distance = CamOrbit.zoomStart;
                CamOrbit.transform.position = new Vector3(0, 0.6f, 0);
                
            }
        }


        if (!IsTopicUnlocked)
        {
            if (TopicToCheck == 4)
            {
                CheckFor_XP_Condition(TopicToCheck);
            }


        }

    }

    public bool CheckFor_XP_Condition(int Current_Todo)
    {
        bool Condition = true;
        if (Current_Todo == 1)
        {
            if (b_Value == -1)
            {
                Collect_XP(LineToDisplay1);
                Condition = false;
            }
            else
            {
                a_Value = a_Value_Ref;
                b_Value = b_Value_Ref;
                a_Val_Text.text = a_Value.ToString();
                b_Val_Text.text = b_Value.ToString();
                ShowInstructionPanel("Wrong parameter chosen");
                Condition = true;
            }
        }
        if (Current_Todo == 0)
        {
            if (a_Value == -2)
            {
                Collect_XP(LineToDisplay1);
                Condition = false;
            }
            else
            {
                ShowInstructionPanel("Wrong parameter chosen");
                a_Value = a_Value_Ref;
                b_Value = b_Value_Ref;
                a_Val_Text.text = a_Value.ToString();
                b_Val_Text.text = b_Value.ToString();
                Condition = true;
            }
        }
        if (Current_Todo == 2)
        {
            
            if (IsLockClicked) //b_Value == -1 && a_Value == -2)
            {
                Collect_XP(LineToDisplay1);
                Condition = false;
            }
            else
            {
                a_Value = a_Value_Ref;
                b_Value = b_Value_Ref;
                a_Val_Text.text = a_Value.ToString();
                b_Val_Text.text = b_Value.ToString();
                ShowInstructionPanel("Wrong parameter chosen");
                Condition = true;
            }
        }

        if (Current_Todo == 3)
        {

            if (ValueDisplaySlider.value!=1)
            {
                Collect_XP(LineToDisplay1);
                Condition = false;
            }
            else
            {
                ShowInstructionPanel("Wrong parameter chosen");
                Condition = true;
            }
        }
        if (Current_Todo == 4)
        {

            if (!InEquation1)
            {
                Collect_XP(LineToDisplay2);
                Condition = false;
            }
            else
            {
                ShowInstructionPanel("Wrong parameter chosen");
                Condition = true;
            }
        }

        return Condition;
    }


    public void ShowInstructionPanel(string TextToShow)
    {
        var mouse = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);



        if (mouse.x < Screen.width / 2)
        {
            // WronOptionPanel
            //print("Mouse is on left side of screen.");
            InstructionPanel.GetComponent<RectTransform>().pivot = new Vector2(0, 0);
        }



        if (mouse.x > Screen.width / 2)
        {
            //print("Mouse is on right side of screen.");
            InstructionPanel.GetComponent<RectTransform>().pivot = new Vector2(1, 0);
        }

        InstructionPanel.transform.position = Input.mousePosition; 
        UIcanvas.GetComponent<GraphicRaycaster>().enabled = false;
        Camera.main.GetComponent<PhysicsRaycaster>().enabled = false;
        iTween.Stop(InstructionPanel.gameObject);

        //iTween.ScaleTo(GlassPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 0.01f, "easetype", iTween.EaseType.spring));
        InstructionPanel.transform.GetChild(0).GetComponent<Text>().text = TextToShow;// "Wrong parameter choosen";
        iTween.ScaleTo(InstructionPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 0.3f, "easetype", iTween.EaseType.spring, "oncomplete", "CloseInstructionPanel", "oncompletetarget", this.gameObject));
        ObjectivePanel.transform.localScale = new Vector3(1, 0, 1);
    }

    
    public void AddListenerToEvents(EventTriggerType eventType, Action MethodToCall, GameObject TriggerObjToAdd)
    {

        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall());
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }

    public void AddListenerToEvents(EventTriggerType eventType, Action<float> MethodToCall, GameObject TriggerObjToAdd, float p)
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall(p));
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }

    public void AddListenerToEvents(EventTriggerType eventType, Action<GameObject> MethodToCall, GameObject TriggerObjToAdd, GameObject p)
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall(p));
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }

    public void EnableOrbit()
    {

        if (!IsAR)
        {
            CamOrbit.DisableInteration = false;
            //print("EnableOrbit");
        }
        else
        {
            ArOrbit.DisableInteration = false;
        }
    }

    public void DisableOrbit()
    {

        if (!IsAR)
        {
            CamOrbit.DisableInteration = true;
            //print("DisableOrbit");
        }
        else
        {
            ArOrbit.DisableInteration = true;
        }
    }

    public void OpenInfoPanel()
    {
        UIZoomIn();
        iTween.ScaleTo(InfoPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 0.5f, "easetype", iTween.EaseType.spring));
        iTween.ScaleTo(GlassPanel, iTween.Hash("x", 1, "y", 1, "z", 1, "time", 0.01f, "easetype", iTween.EaseType.spring));

    }

    public void CloseInfoPanel(Button CloseBtn)
    {
        UIZoomOut();
        iTween.ScaleTo(InfoPanel, iTween.Hash("x", 0, "y", 0, "z", 0, "time", 0.2f, "easetype", iTween.EaseType.linear));
        iTween.ScaleTo(GlassPanel, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.2f, "time", 0.01f, "easetype", iTween.EaseType.linear));
        if (CloseBtn == Info_Cntue_Btn)
        {
            if (!IsAR)
                CamOrbit.DisableInteration = false;
            /*if (ActiveBtnCnt == 0)
            {
                for (int i = 0; i < Elements.Length; i++)
                {

                    Vector3 RandomPos = new Vector3(Elements[i].transform.localPosition.x, Elements[i].transform.localPosition.y, UnityEngine.Random.Range(-5f, 0f));
                    Vector3 CurrPos = Elements[i].transform.localPosition;

                    Elements[i].transform.localPosition = RandomPos;

                    iTween.MoveTo(Elements[i], iTween.Hash("position", CurrPos, "delay", 0.7f, "islocal", true, "time", UnityEngine.Random.Range(0.1f, 0.2f), "speed", 2.5f, "easetype", iTween.EaseType.easeInOutSine));


                }
            }*/
            Info_Close_Btn.gameObject.SetActive(true);
            Info_Cntue_Btn.gameObject.SetActive(false);
            UIcanvas.GetComponent<GraphicRaycaster>().enabled = false;
            Invoke("ZoomCamera", 1.5f);
        }

    }

    public void CloseInstructionPanel()
    {
        UIcanvas.GetComponent<GraphicRaycaster>().enabled = true;
        Camera.main.GetComponent<PhysicsRaycaster>().enabled = true;

        //iTween.ScaleTo(GlassPanel, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 1f, "time", 0.01f, "easetype", iTween.EaseType.linear));
        iTween.ScaleTo(InstructionPanel, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.5f, "time", 0.3f, "easetype", iTween.EaseType.spring));
        if(!IsTopicUnlocked)
        iTween.ScaleTo(ObjectivePanel, iTween.Hash("y", 1, "time", 0.3f, "delay", 0.5f, "easetype", iTween.EaseType.easeInOutSine));
    }

    public void ToDoList_DDCancel()
    {
        if (ToDoList_DD.transform.Find("Dropdown List") != null)//&& Obj.transform.localScale.y!=1)
        {
            //print("cancelled........");
            //print("list.....cancelled........" + ToDoList_DD.transform.Find("Dropdown List").gameObject);
            ToDoList_DD.transform.Find("Arrow").transform.eulerAngles = new Vector3(0, 0, 0);
            iTween.ScaleTo(ToDoList_DD.transform.Find("Dropdown List").gameObject, iTween.Hash("y", 0, "time", 0.5f, "easetype", iTween.EaseType.spring));
            iTween.ScaleTo(ObjectivePanel, iTween.Hash("y", 1, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine));
            if (SidePannelOpen.transform.eulerAngles.z == 180 )
            {
                OpenSidePannel();
            }
        }
    }

    public void ExploringToDOList_DD()
    {
        //print("ShowDD.....");
        //ToDoList_DD.transform.Find("Topic_Objective").GetComponent<Text>().text = "";

        GameObject DDList = ToDoList_DD.transform.Find("Dropdown List").gameObject;
        DDList.transform.localScale = new Vector3(1, 0, 1);
        iTween.ScaleTo(DDList, iTween.Hash("y", 1, "time", 0.5f, "easetype", iTween.EaseType.spring));
        ToDoList_DD.transform.Find("Arrow").transform.eulerAngles = new Vector3(0, 0, 180);
        ObjectivePanel.transform.localScale = new Vector3(1, 0, 1);

        //iTween.ScaleTo(ToDoList_DD.transform.Find("Topic_Objective").gameObject, iTween.Hash("y", 0, "time", 0.5f, "easetype", iTween.EaseType.spring));
        if (SidePannelOpen.transform.eulerAngles.z == 0)
        {
            OpenSidePannel();
        }

        for (int i = 0; i < ToDoList_DD.options.Count; i++)
        {
            GameObject Child = GameObject.Find("Item " + i + ": " + ToDoList_DD.options[i].text);

            Child.transform.Find("XP_Text").GetComponent<Text>().text = "+" + Sub_Top_XP[i] + "XP";

            if (i < TopicToCheck)
            {
                Child.transform.Find("Item Checkmark").GetComponent<Image>().enabled = true;
                //Child.transform.Find("XP_Text").GetComponent<Text>().color = Color.green / 2;
                //Child.transform.Find("Item Checkmark").GetComponent<Image>().color = Color.green / 2;
                Child.transform.Find("Item Background").GetComponent<Image>().enabled = false;
            }
            else if (i == TopicToCheck)
            {
                Child.transform.Find("Item Background").GetComponent<Image>().enabled = true;// color = ReturnColorFromHex("#a01c65");
                //Child.transform.Find("Item Label").GetComponent<Text>().color = Color.white;
            }


        }
    }


    public Color GetColorFromColorCode(string ColorCode)
    {
        Color ConvColor;
        bool Converted = ColorUtility.TryParseHtmlString(ColorCode, out ConvColor);
        return ConvColor;
    }



    public void ZoomCamera(float Value)
    {
        CamOrbit.distance = Value;
    }







    public void OpenSidePannel()
    {
        if (SidePannelOpen.transform.eulerAngles.z == 180)
        {
            //iTween.MoveAdd(SidePannel, iTween.Hash("x", -400f, "time",0.5f,"islocal",true, "easetype", iTween.EaseType.linear));
            //iTween.MoveAdd(SidePannelOpen.gameObject, iTween.Hash("x", -185f, "islocal", true, "time", 0.5f, "easetype", iTween.EaseType.linear));
            //SidePannelOpen.transform.eulerAngles = new Vector3(0,0,180);
            SidePannel.GetComponent<Animator>().Play("PannelForward");
        }
        else
        {
            //iTween.MoveAdd(SidePannel, iTween.Hash("x", 400f, "time", 0.5f, "islocal", true, "easetype", iTween.EaseType.linear));
            //iTween.MoveAdd(SidePannelOpen.gameObject, iTween.Hash("x", -185f, "islocal", true, "time", 0.5f, "easetype", iTween.EaseType.linear));
            //SidePannelOpen.transform.eulerAngles = new Vector3(0, 0, 0);
            SidePannel.GetComponent<Animator>().Play("PannelBackward");
        }
    }
    /*public void OpenTopSidePannel()
    {
        //print("Clicked......"+ PannelOpen.transform.eulerAngles.y);
        if (TopSidePannelOpen.gameObject.transform.localEulerAngles.z == 0)
        {
            TopSidePannel.GetComponent<Animator>().Play("TopButtonOpen");
            BtnselName.gameObject.SetActive(false);
            // TopSidePannelOpen.gameObject.transform.localEulerAngles = new Vector3(0, 0, 180);
        }
        else
        {
            TopSidePannel.GetComponent<Animator>().Play("TopButtonClose");
            // TopSidePannelOpen.gameObject.transform.localEulerAngles = new Vector3(0, 0, 0);
        }
    }*/

    // Update is called once per frame
    void FixedUpdate()
    {
        
        
        //if (!IsAR)
        //{

        //    if (CamOrbit.distance < CamOrbit.zoomStart && !IsUIZooming)
        //    {
        //        UIZoomIn();
        //        IsUIZooming = true;
        //    }
        //    else if (CamOrbit.distance > CamOrbit.zoomStart && IsUIZooming)
        //    {
        //        UIZoomOut();
        //        IsUIZooming = false;
        //    }
        //}

        /*if (CamOrbit.Zoom_bool && ActiveBtnCnt<8)
        {
            CamOrbit.zoomMin = CamOrbit.cameraRotUp * -0.01125f + 1.15f;
        }*/
    }



    public void UIReset()
    {
        ObjectivePanel.GetComponent<RectTransform>().anchoredPosition = new Vector3(0f, 5f, 0);

        //ValueDisplaySlider.transform.GetComponent<RectTransform>().anchoredPosition = new Vector3(-20f, 0f, 0);
        Info_Open_Btn.transform.GetComponent<RectTransform>().anchoredPosition = new Vector3(-37f, -150f, 0);


        SidePannel.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(0f, 0f, 0);

        //print("side......" + SidePannel.transform.position+"....local....."+ SidePannel.transform.localPosition);
        if (!IsTopicUnlocked)
        {
            ToDoList_DD.transform.GetComponent<RectTransform>().anchoredPosition = new Vector3(135f, -150f, 0);
            XP_ToShow.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(135f, -150f, 0);

        }
        else
        {
            ToDoList_DD.transform.GetComponent<RectTransform>().anchoredPosition = new Vector3(-250f, -150f, 0);
            XP_ToShow.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(-250f, -150f, 0);

        }

        //Main_XP_ToShow.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector3(-135f, -60f, 0);

    }

    public void UIZoomOut()
    {
        if (ObjectivePanel.GetComponent<RectTransform>().anchoredPosition.y == 5)
            return;

        UIReset();

        iTween.MoveFrom(Info_Open_Btn.gameObject, iTween.Hash("x", Info_Open_Btn.transform.localPosition.x + 250, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveFrom(SidePannel.transform.parent.gameObject, iTween.Hash("x", SidePannel.transform.parent.localPosition.x - 1000f, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        //iTween.MoveFrom(ValueDisplaySlider.gameObject, iTween.Hash("x", ValueDisplaySlider.transform.localPosition.x + 250, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveFrom(ObjectivePanel, iTween.Hash("y", ObjectivePanel.transform.localPosition.y - 1000, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));


        iTween.MoveFrom(ToDoList_DD.gameObject, iTween.Hash("x", ToDoList_DD.transform.localPosition.x - 400, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveFrom(XP_ToShow.transform.parent.gameObject, iTween.Hash("x", XP_ToShow.transform.parent.localPosition.x - 400, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));

        //iTween.MoveFrom(Main_XP_ToShow.transform.parent.gameObject, iTween.Hash("x", Main_XP_ToShow.transform.parent.localPosition.x + 550, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
    }



    public void UIZoomIn()
    {
        if (ObjectivePanel.GetComponent<RectTransform>().anchoredPosition.y != 5)
            return;

        UIReset();
        //print("side......" + SidePannel.transform.position);
        iTween.MoveAdd(ObjectivePanel, iTween.Hash("y", ObjectivePanel.transform.localPosition.y - 1000, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveAdd(Info_Open_Btn.gameObject, iTween.Hash("x", Info_Open_Btn.transform.localPosition.x + 250, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveAdd(SidePannel.transform.parent.gameObject, iTween.Hash("x", SidePannel.transform.parent.localPosition.x - 1000f, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));

        //iTween.MoveAdd(ValueDisplaySlider.gameObject, iTween.Hash("x", ValueDisplaySlider.transform.localPosition.x + 250, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));

        iTween.MoveAdd(XP_ToShow.transform.parent.gameObject, iTween.Hash("x", XP_ToShow.transform.parent.localPosition.x - 400, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
        iTween.MoveAdd(ToDoList_DD.gameObject, iTween.Hash("x", ToDoList_DD.transform.localPosition.x - 400, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));

        //iTween.MoveAdd(Main_XP_ToShow.transform.parent.gameObject, iTween.Hash("x", Main_XP_ToShow.transform.parent.localPosition.x + 550, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine, "islocal", true));
    }

    public float ChangeToLocal_X(float Val)
    {
        float X_ToRet = 0;
        //print("wid........"+ UIcanvas.GetComponent<RectTransform>().rect.width);
        X_ToRet = ((UIcanvas.GetComponent<RectTransform>().rect.x) + Val);

        return X_ToRet;
    }

    public float ChangeToLocal_Y(float Val)
    {
        float Y_ToRet = 0;
        //print("wid........"+ UIcanvas.GetComponent<RectTransform>().rect.width);
        Y_ToRet = ((UIcanvas.GetComponent<RectTransform>().rect.y) + Val);

        return Y_ToRet;
    }


    public void Collect_XP(GameObject TargetPos)
    {

        UIcanvas.GetComponent<GraphicRaycaster>().enabled = false;
        Camera.main.GetComponent<PhysicsRaycaster>().enabled = false;


        XP_Effect.transform.position = Camera.main.WorldToScreenPoint(TargetPos.transform.position);

        XP_Effect.text = "+"+Sub_Top_XP[TopicToCheck] + "XP";

        XP_Effect.transform.localScale = new Vector3(2, 0, 2);

        iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 2, "y", 2, "z", 2, "time", 0.5f, "easetype", iTween.EaseType.easeOutElastic));

        iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", XP_Effect.transform.position.x, "y", XP_Effect.transform.position.y + 150, "z", 0, "delay", 0.3f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

        iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", 150, "y", XP_ToShow.transform.position.y, "z", 0, "delay", 1f, "time", 1f, "easetype", iTween.EaseType.easeInSine, "oncomplete", "Add_To_XP", "oncompletetarget", this.gameObject));

    }

    public void Add_To_XP()
    {

        //print("TopicToCheck......."+ TopicToCheck+"....sub......"+Sub_Topics.Count);
        if (TopicToCheck < (Sub_Top_ToDoList.Count))
        {
            Sub_XP_Earned += float.Parse(Sub_Top_XP[TopicToCheck]);

            XP_Effect.text = "";
            XP_ToShow.text = Sub_XP_Earned + "/" + Total_XP;

            TopicToCheck++;
            if (TopicToCheck <= (Sub_Top_ToDoList.Count - 1))
            {
                ObjectivePanel.transform.Find("ObjectiveText").GetComponent<Text>().text = Sub_Top_ToDoList[TopicToCheck];
                ObjectivePanel.transform.localScale = new Vector3(1, 0, 1);
                iTween.ScaleTo(ObjectivePanel, iTween.Hash("y", 1, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine));
                UIcanvas.GetComponent<GraphicRaycaster>().enabled = true;
                Camera.main.GetComponent<PhysicsRaycaster>().enabled = true;
                //ToDoList_DD.transform.Find("Topic_Objective").GetComponent<Text>().text = "Objective: " + Sub_Top_ToDoList[TopicToCheck];
            }
            else
            {
                XP_Effect.transform.position = (XP_ToShow.transform.position);

                XP_Effect.text = Total_XP + "XP";

                XP_Effect.transform.localScale = new Vector3(1, 0, 1);

                iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 2, "y", 2, "z", 2, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeOutElastic));

                iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", XP_Effect.transform.position.x, "y", XP_Effect.transform.position.y + 50, "z", 0, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

                iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", Main_XP_ToShow.transform.position.x, "y", Main_XP_ToShow.transform.position.y, "z", 0, "delay", 1.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine, "oncomplete", "AddTotalToMainXP", "oncompletetarget", this.gameObject));

                iTween.MoveTo(ToDoList_DD.gameObject, iTween.Hash("x", ChangeToLocal_X(-400), "delay", 3.5f, "time", 0.5f, "islocal", true, "easetype", iTween.EaseType.easeInOutSine));

                iTween.MoveTo(XP_ToShow.transform.parent.gameObject, iTween.Hash("x", ChangeToLocal_X(-400), "delay", 3.5f, "time", 0.5f, "islocal", true, "easetype", iTween.EaseType.easeInOutSine));
                //print("label......" + LabelImage.GetComponent<RectTransform>().anchoredPosition+".......explode...."+ ExplodeSlider.GetComponent<RectTransform>().anchoredPosition);


                //iTween.MoveTo(ValueDisplaySlider.gameObject, iTween.Hash("x", ChangeToLocal_X ( 150), "delay", 0.7f, "time", 0.5f, "islocal", true, "easetype", iTween.EaseType.easeInOutSine));
            }
        }

    }

    public void AddTotalToMainXP()
    {
        if (OptionUnlockedTill < 1)
        {
            OptionUnlockedTill++;
            UIcanvas.GetComponent<GraphicRaycaster>().enabled = true;
            Camera.main.GetComponent<PhysicsRaycaster>().enabled = true;
            float MainXp = float.Parse(Main_XP_ToShow.text);
            MainXp += Total_XP;
            Main_XP_ToShow.text = MainXp.ToString();
            //ObjectivePanel.transform.Find("ObjectiveText").GetComponent<Text>().text = "YaY!......Next topic unlocked";
            XP_Effect.text = "";
            IsTopicUnlocked = true;
            iTween.ScaleTo(ObjectivePanel, iTween.Hash("y", 0, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine));



        }
    }
}
