﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;

public class vid426_friction : MonoBehaviour, IPointerDownHandler
{
    // Start is called before the first frame update

    public Canvas C_BG;

    public orbit CamOrbit;

    public AROrbitControls ArOrbit;

    public bool IsAR;

    public GameObject MainObj, Ground;

    public int OptionUnlockedTill, CurrentTopicSelected, TopicToCheck;

    public float Sub_XP_Earned, Total_XP;

    public Dropdown Options_DD, ToDoList_DD;

    public GameObject InstructionPanel_DD, ToDoList_Panel;

    public List<Dictionary<string, object>> CSV_data;

    public List<String> TaskList,XpList,InfoText;

    public List<GameObject> AllTaskData;

    public GameObject[] Labels;

    public Text XP_ToShow, XP_Effect, Main_XP_ToShow, localTotalXpValueText, localGainedXpValueText, ObjectiveInfoText, LGListText, IListText, AListText,
        InfoHeadingText;

    public GameObject TaskObj, AllTaskListObj;

    public RectTransform rT;

    public int localTotalXpValue, localGaniedXpValue,MainXpValue;

    public GameObject ControlPanel, LocalXP, Main_Xp, ObjectivePanel, InformationPanel, InfoButton, MainSlider, InfoPanelBg, TasklistPanel, WronOptionPanel, Gained_XP_ArrowBut;

    public Button InfoCloseBtn, InfoContinueBtn, Gained_XP_Panel, CpCloseBtn;

    public bool sessionStart;

    public TextAsset CSVFile;

    public AssetBundle assetBundle;

    public Slider ExplodeSlider;

    public float ExplodeValue;

    public PhysicMaterial ObjectMat;

    public Button SubmitBtn_theta, PanelBut, Reset, IncButton, DecButton;

    public InputField inputValu1;

    public GameObject SlopeObject, DragObject, AngleBlend, arrow_Friction, arrow_Force, HelpBut, HelpPanel, 
        SliderBlock, BlockToSlide, LightObj, TalkBubble_Label;

    public TextMeshPro AngText, FCTextDisplay, LengthTextDisplay, mgSinTextDisplay, mgCosTextDisplay;

    public string[] CorrectMsgs = new string[] { "Nice!", "WellDone!", "Excellent!", "Correct!" };

    public int MultFactor;

    public float angle, FC, initRot, a, L;

    public Text DeleteText, FrictionValue, MessageT, angleValue, aValue, tValue, MgSinThetaValue, MgCosThetaValue, SliderValueText;

    public Color Highlight_Color, Normal_Color;

    public Vector3 BlockInitPos, BlockInitRot;

    public bool LocalTest;

    public GameObject Linefs, Linefk;


    void Start()
    {

        LoadFromAsssetBundle();

        sessionStart = false;

        //OptionUnlockedTill = 0;

        //TopicToCheck = 1;

        //MainXpValue = 10;
      
        MainObj = this.gameObject;

        C_BG = GameObject.Find("Canvas_BG").GetComponent<Canvas>();

        Ground = GameObject.Find("Ground");

        if (!IsAR)
        {
            CamOrbit = Camera.main.GetComponentInParent<orbit>();

            C_BG.transform.parent = Camera.main.transform;
            C_BG.GetComponentInChildren<RawImage>().enabled = true;
            C_BG.renderMode = RenderMode.ScreenSpaceCamera;
            C_BG.worldCamera = Camera.main;

            CamOrbit.target = Camera.main.transform.parent;
            CamOrbit.target.position = new Vector3(0, 0.2f, 0);
            CamOrbit.maxRotUp = 15;
            CamOrbit.minRotUp = 1;
            CamOrbit.zoomMin = 0.5f;
            CamOrbit.zoomMax = 2;
            CamOrbit.zoomStart = 1f;
            CamOrbit.cameraRotUpStart = 5f;

            CamOrbit.Start();
        }
        else
        {
            Ground.SetActive(false);

            C_BG.gameObject.SetActive(false);

            ArOrbit = MainObj.transform.parent.gameObject.GetComponent<AROrbitControls>();
        }

        ReadingDataFromCSVFile();
        ReadingXps();
        FindingObjects();
        TaskListCreation();
        AssigningClickEvents();
        LabelsAssignment();
        AssignInitialValues();
        LocalXpCalculation();

        MultFactor = UnityEngine.Random.Range(1, 5);

        initRot = UnityEngine.Random.Range(0, 1);

        ExplodeValue = 0;

        LocalTest = false;

        InvokeRepeating("StartInitRotation", 0.5f, 0.01f);
    }


    public void StartInitRotation()
    {

        if ((ExplodeSlider.value).ToString("F2") != initRot.ToString("F2"))
        {
            ExplodeSlider.value = Mathf.Lerp(ExplodeSlider.value, initRot, Time.deltaTime * 2f);
        }
        else
        {
            CancelInvoke("StartInitRotation");
        }

    }


    public void LoadFromAsssetBundle()
    {
      
            assetBundle = AssetBundle.LoadFromFile(PlayerPrefs.GetString("AssetBundleUrl"));

            CSVFile = assetBundle.LoadAsset(PlayerPrefs.GetString("LibraryName") + "CSV", typeof(TextAsset)) as TextAsset;
        
           // CSVFile = Resources.Load("vid426_frictionCSV") as TextAsset;
        

        Camera.main.gameObject.AddComponent<PhysicsRaycaster>();

        Camera.main.backgroundColor = Color.black;

        GameObject TargetForCamera = GameObject.Find("Target");

        if (TargetForCamera != null)
        {
            TargetForCamera.AddComponent<orbit>();
            CamOrbit = TargetForCamera.GetComponent<orbit>();
            IsAR = false;
        }
        else
        {
            this.transform.parent.gameObject.AddComponent<AROrbitControls>();
            ArOrbit = this.transform.parent.gameObject.GetComponent<AROrbitControls>();
            IsAR = true;
        }

    }
    public void ReadingDataFromCSVFile()
    {             
      
        TaskList = new List<string>();
        XpList = new List<string>();
        InfoText = new List<string>();

        AllTaskData = new List<GameObject>();

        CSV_data = CSVReader.Read(CSVFile);
                  
        for (var i = 0; i < CSV_data.Count; i++)
        {
            if (CSV_data[i]["TaskList"].ToString() != "" && CSV_data[i]["TaskList"].ToString() != null)
            {
                TaskList.Add(CSV_data[i]["TaskList"].ToString());          
            }
            //if (CSV_data[i]["XPList"].ToString() != "" && CSV_data[i]["XPList"].ToString() != null)
            //{
            //    XpList.Add(CSV_data[i]["XPList"].ToString());
            //}
        }


        TaskList[1] = "Change the friction coefficient(µ) and increase the height of the plank.";
    }

    public int totXp, loclXp;

    public void ReadingXps()
    {

        totXp = PlayerPrefs.GetInt("TotalXP");
        loclXp = PlayerPrefs.GetInt("MaxEarnings");

        if (loclXp != 0)
        {
            OptionUnlockedTill = 0;

        }
        else
        {
            OptionUnlockedTill = 1;

        }

        TopicToCheck = 1;

        MainXpValue = totXp;

        int t = 0;
        int p = 0;

        for (int k = 0; k < TaskList.Count; k++)
        {
            p = loclXp / TaskList.Count;
            XpList.Add(p.ToString());
            t = t + p;
        }

        if (t != loclXp)
        {
            XpList[XpList.Count - 1] = (p + (loclXp - t)).ToString();
        }
    }
    public float result;

    public void GetResult()
    {
        result = ((float)(TopicToCheck - 1) / (float)(TaskList.Count)) * 100;
        Camera.main.SendMessage("UnloadActually", result);
    }
    public void FindingObjects() {

        InformationPanel = GameObject.Find("InformationPanel");

        InfoCloseBtn = GameObject.Find("InfoCloseBtn").GetComponent<Button>();

        InfoContinueBtn = GameObject.Find("InfoContinueBtn").GetComponent<Button>();

        InfoPanelBg = GameObject.Find("InfoPanelBg");
        
        XP_ToShow = GameObject.Find("localGainedXpValueText").GetComponent<Text>();

        XP_Effect = GameObject.Find("Effect_Text").GetComponent<Text>();

        Main_XP_ToShow = GameObject.Find("Main_XP_Value").GetComponent<Text>();

        localGainedXpValueText = GameObject.Find("localGainedXpValueText").GetComponent<Text>();

        localTotalXpValueText = GameObject.Find("GainedXP_LocalTotalXp").GetComponent<Text>();

        ObjectiveInfoText = GameObject.Find("ObjectiveInfoText").GetComponent<Text>();

        TaskObj = GameObject.Find("TaskObj");

        AllTaskListObj = GameObject.Find("AllTaskList");

        ControlPanel = GameObject.Find("ControlPanel");
        LocalXP = GameObject.Find("LocalXP");
        Main_Xp = GameObject.Find("Main_Xp");
        ObjectivePanel = GameObject.Find("ObjectivePanel");
        InformationPanel = GameObject.Find("InformationPanel");
        InfoButton = GameObject.Find("InfoButton");
        MainSlider = GameObject.Find("MainSlider");
        WronOptionPanel = GameObject.Find("WrongPanel");

        Gained_XP_Panel = GameObject.Find("Gained_XP_Panel").GetComponent<Button>();
        TasklistPanel = GameObject.Find("TasklistPanel");
        CpCloseBtn = GameObject.Find("CpCloseBtn").GetComponent<Button>();

        InfoHeadingText = GameObject.Find("InfoHeadingText").GetComponent<Text>();
        LGListText = GameObject.Find("LGListText").GetComponent<Text>();
        IListText = GameObject.Find("IListText").GetComponent<Text>();
        AListText = GameObject.Find("AListText").GetComponent<Text>();

       
        //IncButton = GameObject.Find("IncButton").GetComponent<Button>();

        //DecButton = GameObject.Find("DecButton").GetComponent<Button>();
       
        Gained_XP_ArrowBut = GameObject.Find("Gained_XP_ArrowBut");

      

        //......................................................


        // mu_label_txt = GameObject.Find("mu_label_txt").GetComponent<TextMeshPro>();


        ExplodeSlider = GameObject.Find("MainSlider").GetComponent<Slider>();

       

        //............. Finding Objects...............................................

        SlopeObject = GameObject.Find("wedge");

        DragObject = GameObject.Find("plank");

        AngleBlend = GameObject.Find("AngleBlend");

        arrow_Force = GameObject.Find("arrow_Force");

        arrow_Friction = GameObject.Find("arrow_Friction");



        ObjectMat = DragObject.GetComponent<BoxCollider>().material;

        //InputField_FC = GameObject.Find("InputField_FC").GetComponent<InputField>();

        ObjectMat.staticFriction = float.Parse(UnityEngine.Random.Range(0.1f, 1f).ToString("F1"));

        FC = ObjectMat.staticFriction;

        // InputField_FC.text = FC.ToString("");

        AngText = GameObject.Find("AngleTextDisplay").GetComponent<TextMeshPro>();

        FCTextDisplay = GameObject.Find("FCTextDisplay").GetComponent<TextMeshPro>();

        LengthTextDisplay = GameObject.Find("LengthTextDisplay").GetComponent<TextMeshPro>();

        mgSinTextDisplay = GameObject.Find("mgSinTextDisplay").GetComponent<TextMeshPro>();

        mgCosTextDisplay = GameObject.Find("mgCosTextDisplay").GetComponent<TextMeshPro>();

        //Rad_UIText = GameObject.Find("R").GetComponent<Text>();

        //inputValu1.onEndEdit.AddListener(delegate { OnInputvalueEnd(1); });

     

        ExplodeSlider.onValueChanged.AddListener(delegate { ShadowValue(ExplodeSlider.value, ExplodeSlider.gameObject.GetComponentInChildren<Text>()); });

        EventTrigger.Entry pointerEntry = new EventTrigger.Entry();
        pointerEntry.eventID = EventTriggerType.PointerUp;
        pointerEntry.callback.AddListener((data) => { EnableOrbit(); });

        //ExplodeSlider.OnPointerUp.PointerUpentry

        EventTrigger.Entry PointerDownentry = new EventTrigger.Entry();
        PointerDownentry.eventID = EventTriggerType.PointerDown;
        PointerDownentry.callback.AddListener((data) => { DisableOrbit(); });

        ExplodeSlider.gameObject.GetComponent<EventTrigger>().triggers.Add(PointerDownentry);

        EventTrigger.Entry PointerUpentry = new EventTrigger.Entry();
        PointerUpentry.eventID = EventTriggerType.PointerUp;
        PointerUpentry.callback.AddListener((data) => { EnableOrbit(); });

        ExplodeSlider.gameObject.GetComponent<EventTrigger>().triggers.Add(PointerUpentry);

        BlockToSlide = GameObject.Find("plank");

        BlockInitPos = BlockToSlide.transform.localPosition;

        BlockInitRot = BlockToSlide.transform.localEulerAngles;

        Reset = GameObject.Find("Reset").GetComponent<Button>();

        Reset.GetComponent<Button>().onClick.AddListener(() => ResetFun());

        angle = (Mathf.Atan(FC));

        angleValue = GameObject.Find("AngleValue").GetComponent<Text>();

        angleValue.text = (Mathf.CeilToInt(angle * Mathf.Rad2Deg)).ToString();

        a = Mathf.Abs((9.8f * (Mathf.Sin(angle * Mathf.Deg2Rad) - FC * Mathf.Cos(angle * Mathf.Deg2Rad))));

        aValue = GameObject.Find("A_value").GetComponent<Text>();

        aValue.text = (a).ToString("F2");

        tValue = GameObject.Find("tValue").GetComponent<Text>();

        FrictionValue = GameObject.Find("FrictionValue").GetComponent<Text>();

        MgSinThetaValue = GameObject.Find("MgSinThetaValue").GetComponent<Text>();

        MgCosThetaValue = GameObject.Find("MgCosThetaValue").GetComponent<Text>();

        // SliderValueText = GameObject.Find("SliderValueText").GetComponent<Text>();

        IncButton = GameObject.Find("IncButton").GetComponent<Button>();

        DecButton = GameObject.Find("DecButton").GetComponent<Button>();

        IncButton.GetComponent<Button>().onClick.AddListener(() => IncDecFunction(IncButton.name));

        DecButton.GetComponent<Button>().onClick.AddListener(() => IncDecFunction(DecButton.name));


        TalkBubble_Label = GameObject.Find("TalkBubble_Label");

        Linefs = GameObject.Find("Linefs");
        Linefk = GameObject.Find("Linefk");

        Linefs.SetActive(true);
        Linefk.SetActive(false);
    }

     
    public void AssigningClickEvents() {
                
        iTween.ScaleTo(InformationPanel.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

        InfoCloseBtn.onClick.AddListener(() => CloseInfoPanel());

        InfoContinueBtn.onClick.AddListener(() => ContinueInfoPanel());

        InfoButton.GetComponent<Button>().onClick.AddListener(() => OpenInfoPanel());

        Gained_XP_Panel.onClick.AddListener(() => OpenTaskList());

        CpCloseBtn.onClick.AddListener(() => OpenOrCloseControlPanel());

       // MainSlider.GetComponent<Slider>().onValueChanged.AddListener(delegate { ChangeRadius(MainSlider.GetComponent<Slider>().value, MainSlider.GetComponentInChildren<Text>()); });

        AddListener(EventTriggerType.PointerDown, DisableOrbit, MainSlider.gameObject);

        AddListener(EventTriggerType.PointerUp, EnableOrbit, MainSlider.gameObject);
        
        //IncButton.GetComponent<Button>().onClick.AddListener(() => IncDecFunction(IncButton.name));

        //DecButton.GetComponent<Button>().onClick.AddListener(() => IncDecFunction(DecButton.name));

    }

    public void AssignInitialValues()
    {
        if (OptionUnlockedTill > 0)
        {
            LocalXP.transform.localScale = new Vector3(0, 0, 0);
            ObjectivePanel.transform.localScale = new Vector3(0, 0, 0);
        }

        InfoCloseBtn.gameObject.transform.localScale = new Vector3(0, 0, 0);
        localGainedXpValueText.text = "0";
        Main_XP_ToShow.text = MainXpValue + "";
        WronOptionPanel.transform.localScale = new Vector3(0, 0, 0);

        TalkBubble_Label.SetActive(false);
    }


    public void TaskListCreation() {

        rT = AllTaskListObj.transform.parent.GetComponent<RectTransform>();

        localGaniedXpValue = 0;

        for (int i = 0; i < TaskList.Count; i++)
        {

            GameObject Tl = (GameObject)Instantiate(TaskObj);
            Tl.transform.SetParent(AllTaskListObj.transform);
            Tl.transform.localScale = new Vector3(1, 1, 1);
            Tl.name = i.ToString();

            AllTaskData.Add(Tl);
            AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(false);
            AllTaskData[i].gameObject.transform.GetChild(2).gameObject.SetActive(false);
            AllTaskData[i].gameObject.transform.GetChild(3).GetComponentInChildren<Text>().text = TaskList[i];
            AllTaskData[i].gameObject.transform.GetChild(4).GetComponentInChildren<Text>().text = "+" + XpList[i] + " XP";

            localTotalXpValue += int.Parse(XpList[i]);
        }

        rT.sizeDelta = new Vector2(rT.sizeDelta.x, TaskList.Count * 50);
        localTotalXpValueText.text = "/" + localTotalXpValue.ToString();

        AllTaskData[0].gameObject.transform.GetChild(2).gameObject.SetActive(true);
        ObjectiveInfoText.text = AllTaskData[0].gameObject.transform.GetChild(3).GetComponentInChildren<Text>().text;
    }

    public void LocalXpCalculation()
    {


        for (int i = 0; i < TaskList.Count; i++)
        {
            AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(false);
            AllTaskData[i].gameObject.transform.GetChild(2).gameObject.SetActive(false);
        }

        if (OptionUnlockedTill < 1)
        {
            if ((TopicToCheck) < (TaskList.Count + 1))
            {
                localGaniedXpValue = 0;

                for (int i = 0; i < TopicToCheck - 1; i++)
                {
                    localGaniedXpValue += int.Parse(XpList[i]);
                }

                for (int i = 0; i < TopicToCheck; i++)
                {
                    AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(true);
                }

                localGainedXpValueText.text = localGaniedXpValue.ToString();

                AllTaskData[TopicToCheck - 1].gameObject.transform.GetChild(2).gameObject.SetActive(true);

                ObjectiveInfoText.text = AllTaskData[TopicToCheck - 1].gameObject.transform.GetChild(3).GetComponentInChildren<Text>().text;
            }
            else
            {
                for (int i = 0; i < TaskList.Count; i++)
                {
                    AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(true);
                    AllTaskData[i].gameObject.transform.GetChild(2).gameObject.SetActive(false);
                }

                localGainedXpValueText.text = localTotalXpValue.ToString();

                XP_Effect.text = "+" + localTotalXpValue + " XP";

                XP_Effect.transform.position = (XP_ToShow.transform.position);

                iTween.Stop(XP_Effect.gameObject);

                iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeOutElastic));

                iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", XP_Effect.transform.position.x, "y", XP_Effect.transform.position.y + 50, "z", 0, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

                iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", Main_XP_ToShow.transform.position.x, "y", Main_XP_ToShow.transform.position.y, "z", 0, "delay", 1.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

                iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "delay", 2f, "time", 0.5f, "easetype", iTween.EaseType.linear, "oncomplete", "MainXpCalculation", "oncompletetarget", this.gameObject));
            }
        }
        else
        {
            for (int i = 0; i < TaskList.Count; i++)
            {
                AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(true);
                AllTaskData[i].gameObject.transform.GetChild(2).gameObject.SetActive(false);
            }

            localGainedXpValueText.text = localTotalXpValue.ToString();
        }
    }

    public void CloseAllpanels()
    {

        ControlPanel.GetComponent<Animator>().Play("CpanelFullClose");
        LocalXP.GetComponent<Animator>().Play("LocalXpPanelClose");
        Main_Xp.GetComponent<Animator>().Play("MainXpPanelClose");
        ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelClose");
        InfoButton.GetComponent<Animator>().Play("InfoButtonClose");
        MainSlider.GetComponent<Animator>().Play("MainSliderClose");

        if (TasklistPanel.gameObject.transform.localScale.y != 0)
            iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));
    }

    public void OpenAllpanels()
    {
        if (OptionUnlockedTill < 1)
        {
            if (TopicToCheck <= (TaskList.Count))
            {
                LocalXP.GetComponent<Animator>().Play("LocalXpPanelOPen");
                ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen");
            }
        }


        ControlPanel.GetComponent<Animator>().Play("CpanelOpen");

        Main_Xp.GetComponent<Animator>().Play("MainXpPanelOpen");

        InfoButton.GetComponent<Animator>().Play("InfoButtonOpen");
        MainSlider.GetComponent<Animator>().Play("MainSliderOpen");
        iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "time", 0.2f, "easetype", iTween.EaseType.linear));
        iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));
    }

    public void AddXpEffect()
    {
        if (TopicToCheck <= (TaskList.Count))
        {
            // Camera.main.GetComponent<PhysicsRaycaster>().enabled = false;

            GameObject.Find("Canvas").GetComponent<CanvasGroup>().interactable = false;

            XP_Effect.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);

            XP_Effect.text = "+" + int.Parse(XpList[TopicToCheck - 1]) + " XP";

            XP_Effect.transform.localScale = new Vector3(1, 0, 1);

            // iTween.Stop(XP_Effect.gameObject);


            iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeOutElastic));

            iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", XP_Effect.transform.position.x, "y", XP_Effect.transform.position.y + 50, "z", 0, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

            iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", XP_ToShow.transform.position.x, "y", XP_ToShow.transform.position.y, "z", 0, "delay", 1.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

            iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "delay", 2f, "time", 0.5f, "easetype", iTween.EaseType.linear, "oncomplete", "LocalXpCalculation", "oncompletetarget", this.gameObject));

            TopicToCheck++;

            Invoke("EnableRayCast", 3);
        }

    }

    public void EnableRayCast()
    {
        //Camera.main.GetComponent<PhysicsRaycaster>().enabled = true;
        GameObject.Find("Canvas").GetComponent<CanvasGroup>().interactable = true;
    }

    public IEnumerator MainXpCalculation()
    {
        if (OptionUnlockedTill == 0)
        {
            OptionUnlockedTill = 1;

            MainXpValue += localTotalXpValue;

            Main_XP_ToShow.text = MainXpValue.ToString();

            yield return new WaitForSeconds(1f);

            LocalXP.GetComponent<Animator>().Play("LocalXpPanelClose");

            ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelClose");

            if ((ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelClose")) || (ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelFullClose")))
            {
                ControlPanel.GetComponent<Animator>().Play("CpanelOpen");
            }
        }

    }

    public void CloseInfoPanel()
    {
        sessionStart = true;
        InfoPanelBg.SetActive(false);
        iTween.ScaleTo(InformationPanel.gameObject, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));
        OpenAllpanels();
    }

    public void ContinueInfoPanel()
    {
        sessionStart = true;
        InfoPanelBg.SetActive(false);
        iTween.ScaleTo(InformationPanel.gameObject, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));
        InfoCloseBtn.transform.localScale = new Vector3(0.27f, 0.27f, 0.27f);
        InfoContinueBtn.transform.localScale = new Vector3(0, 0, 0);
        OpenAllpanels();
    }

    public void OpenInfoPanel()
    {
        sessionStart = false;
        iTween.ScaleTo(InformationPanel.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));
        InfoPanelBg.SetActive(true);
        iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "time", 0.2f, "easetype", iTween.EaseType.linear));
        iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));
        CloseAllpanels();
    }

    public void OpenTaskList()
    {


        if (TasklistPanel.gameObject.transform.localScale.y != 1)
        {
            iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.01f, "time", 0.2f, "easetype", iTween.EaseType.easeInSine));

            ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelClose");


            //Gained_XP_ArrowBut

            iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 180, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));

            if ((ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelOpen")))
            {
                ControlPanel.GetComponent<Animator>().Play("CpanelClose");
            }
        }
        else
        {
            iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "delay", 0.01f, "time", 0.2f, "easetype", iTween.EaseType.easeInSine));
            iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));

            ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen");

            ControlPanel.GetComponent<Animator>().Play("CpanelOpen");
        }
    }

    public void OpenOrCloseControlPanel()
    {

        if ((ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelClose")) || (ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelFullClose")))
        {
            ControlPanel.GetComponent<Animator>().Play("CpanelOpen");
        }
        else
        {
            ControlPanel.GetComponent<Animator>().Play("CpanelClose");
        }
    }

    public void WrongOptionSelected()
    {
        WronOptionPanel.transform.localScale = new Vector3(0, 0, 0);

        iTween.Stop(WronOptionPanel.gameObject);

        iTween.ScaleTo(WronOptionPanel.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.01f, "time", 0.2f, "easetype", iTween.EaseType.easeInSine));

        iTween.ScaleTo(WronOptionPanel.gameObject, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 1f, "time", 0.2f, "easetype", iTween.EaseType.easeInSine));

        // InfoPanelBg.SetActive(true);

        Invoke("CloseBlurBg", 1f);

        GameObject.Find("Canvas").GetComponent<CanvasGroup>().interactable = false;

        ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelClose");


        var mouse = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);

        if (mouse.x < Screen.width / 2)
        {
            // WronOptionPanel
            //print("Mouse is on left side of screen.");
            WronOptionPanel.GetComponent<RectTransform>().pivot = new Vector2(0, 0);
        }

        if (mouse.x > Screen.width / 2)
        {
            //print("Mouse is on right side of screen.");
            WronOptionPanel.GetComponent<RectTransform>().pivot = new Vector2(1, 0);
        }


        WronOptionPanel.transform.position = Input.mousePosition;

    }

    public void CloseBlurBg()
    {
        InfoPanelBg.SetActive(false);
        GameObject.Find("Canvas").GetComponent<CanvasGroup>().interactable = true;


        if (OptionUnlockedTill <0)
        {
            if (TopicToCheck <= (TaskList.Count))
            {
                if (InformationPanel.gameObject.transform.localScale.x == 0f)
                {
                    //ObjectivePanel.GetComponent<Animator>().Play("");
                    ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen", 0, 0f);
                }
            }
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
      
        if (OptionUnlockedTill < 1)
        {
            if (InformationPanel.gameObject.transform.localScale.x != 1 && sessionStart)
            {
                if (TopicToCheck <= (TaskList.Count))
                {
                    iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "time", 0.2f, "easetype", iTween.EaseType.linear));
                    ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen");
                    iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));
                    ControlPanel.GetComponent<Animator>().Play("CpanelOpen");
                }
            }
        }

    }

    public void LabelsAssignment()
    {
        GameObject Label = GameObject.Find("Labels");

        Labels = new GameObject[Label.transform.childCount];

        for (int j = 0; j < 6; j++)
        {
            Labels[j] = Label.transform.GetChild(j).gameObject;

            GameObject TempLabels = Labels[j];

            foreach (Transform ChildLabel in TempLabels.transform)
            {
                if (ChildLabel.name.Contains("SmoothLook"))
                    ChildLabel.gameObject.AddComponent<SmoothLook>();
            }
        }


        for (int k = 0; k < 5; k++)
        {

            string name = Labels[k].name.Remove(Labels[k].name.Length - 6);

            GameObject dummy = GameObject.Find(name + "").gameObject;

            Labels[k].transform.parent = dummy.transform;
            Labels[k].transform.rotation = Quaternion.identity;

        }

        ResetFun();


    }

    public void AddListener(EventTriggerType eventType, Action MethodToCall, GameObject TriggerObjToAdd)
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall());
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }


    public void EnableOrbit()
    {
        if (!IsAR)
        {
            CamOrbit.DisableInteration = false;
        }
        else
        {
            ArOrbit.DisableInteration = false;
        }

        if (OptionUnlockedTill < 1)
        {
            if (InformationPanel.gameObject.transform.localScale.x != 1)
            {
                if (TopicToCheck <= (TaskList.Count))
                {
                    iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "time", 0.2f, "easetype", iTween.EaseType.linear));
                    iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));
                    ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen");
                }
            }
        }


        if (OptionUnlockedTill < 1)
        {
            if (TopicToCheck == 2 )
            {
                if (LocalTest != false)
                {
                    WrongOptionSelected();
                    WronOptionPanel.GetComponentInChildren<Text>().text = "Tap on Reset";
                }
                else if (temp == FC) {
                    WrongOptionSelected();
                    WronOptionPanel.GetComponentInChildren<Text>().text = "Change the Friction coefficient";
                }
               // ResetFun();
            }
        }

    }

    public void DisableOrbit()
    {
        if (!IsAR)
        {
            CamOrbit.DisableInteration = true;
        }
        else
        {
            ArOrbit.DisableInteration = true;
        }

    }

    public void AddListener(EventTriggerType eventType, Action<float> MethodToCall, GameObject TriggerObjToAdd)
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall(TriggerObjToAdd.GetComponent<Slider>().value));
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }

    public Vector3 MidPoint(GameObject obj1, GameObject obj2)
    {
        Vector3 midp = (obj1.transform.localPosition + obj2.transform.localPosition) / 2;
        return midp;
    }

    public float Scale(GameObject obj1, GameObject obj2)
    {
        float scl = Vector3.Distance(obj1.transform.localPosition, obj2.transform.localPosition);
        return scl;
    }
    public float temp;

    public void IncDecFunction(string nam)
    {
       
        if (nam == "IncButton")
        {
            if (FC < 0.9f)
                FC = FC + 0.1f;
        }
        else
        {
            if (FC > 0.1f)
                FC = FC - 0.1f;
        }

        LocalTest = false;
        ObjectMat.staticFriction = FC;

        FC = ObjectMat.staticFriction;

        angle = (Mathf.Atan(FC));

        FrictionValue.text = FC.ToString("F1");

        angle = (Mathf.Atan(FC));

        ExplodeSlider.value = 0;

        BlockToSlide.transform.parent = SlopeObject.transform;

        BlockToSlide.transform.localPosition = BlockInitPos;

        BlockToSlide.transform.localEulerAngles = BlockInitRot;

        angleValue.text = (Mathf.CeilToInt(angle * Mathf.Rad2Deg)).ToString() + " °";

        AngleBlend.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(0, 100 - ExplodeSlider.value * 0.277f);
        SlopeObject.transform.localEulerAngles = new Vector3(0, 0, -ExplodeSlider.value);

        TalkBubble_Label.SetActive(false);

        Linefs.SetActive(true);
        Linefk.SetActive(false);

        if (OptionUnlockedTill < 1)
        {
            if (TopicToCheck == 1)
            {
                WrongOptionSelected();
                WronOptionPanel.GetComponentInChildren<Text>().text = "Wrong parameter chosen";
            }
        }
    }

    public void Update()
    {
        angle = (Mathf.Atan(FC));

        //print(Mathf.CeilToInt(angle*Mathf.Rad2Deg) + "...... Angle...");               

        float MgSinTheta = (1 * 9.8f * Mathf.Sin(angle));
        float FCMgCosTheta = (FC * 0.92f * 9.8f * Mathf.Cos(angle));

        //print("MgSinTheta   = " + MgSinTheta.ToString("F3") + "..... MgCos  = "+ FCMgCosTheta.ToString("F3"));

        a = Mathf.Abs((9.8f * (Mathf.Sin(angle * Mathf.Deg2Rad) - FC * Mathf.Cos(angle * Mathf.Deg2Rad))));

        L = 5;

        AngText.text = "" + (Mathf.CeilToInt(ExplodeSlider.value * 1.045f)).ToString() + " °";

        FCTextDisplay.text = "" + FC.ToString("F1");

        LengthTextDisplay.text = L.ToString() + " m";

        float MgSinTheta1 = (1 * 9.8f * Mathf.Sin(ExplodeSlider.value * Mathf.Deg2Rad));

        float FCMgCosTheta1 = (FC * 0.92f * 9.8f * Mathf.Cos(ExplodeSlider.value * Mathf.Deg2Rad));

        arrow_Force.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(0, MgSinTheta1 * 10);

        arrow_Friction.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(0, FCMgCosTheta1 * 10);
               
        mgSinTextDisplay.text = "F = " + MgSinTheta1.ToString("F2") + " N";

        if (Mathf.CeilToInt(ExplodeSlider.value * 1.05f) < Mathf.CeilToInt(angle * Mathf.Rad2Deg))
            mgCosTextDisplay.text = "f = " + MgSinTheta1.ToString("F2") + " N";

        tValue.text = Mathf.Sqrt((2 * L) / a).ToString("f2") + " sec";

        aValue.text = (a).ToString("F2") + " m/sec²";

        MgSinThetaValue.text = MgSinTheta1.ToString("F2") + " N";

        if (BlockToSlide.transform.localPosition.x > 0.001) {
            BlockToSlide.transform.parent = null;
            Linefs.SetActive(false);
            Linefk.SetActive(true);

            TalkBubble_Label.GetComponentInChildren<TextMeshPro>().text = "The static friction (f<sub>s</sub>) had reached maximum at angle of repose " + (Mathf.CeilToInt(angle * Mathf.Rad2Deg)) + "°, and as the result the block slides down.";

            TalkBubble_Label.SetActive(true);

            if (!LocalTest) {

                LocalTest = true;

                if (OptionUnlockedTill < 1)
                {
                    if (TopicToCheck == 1)
                    {
                        Invoke("AddXpEffect", 0);
                    }
                    if (TopicToCheck == 2 && temp != FC)
                    {
                        Invoke("AddXpEffect", 0);
                    }
                }
            }
        }
        
        if (Mathf.CeilToInt(ExplodeSlider.value * 1.05f) < Mathf.CeilToInt(angle * Mathf.Rad2Deg))
            MgCosThetaValue.text = MgSinTheta1.ToString("F2") + " N";

    }
    public void ResetFun()
    {
        LocalTest = false;
       
        ExplodeSlider.value = 0;

        //ObjectMat.staticFriction = float.Parse(UnityEngine.Random.Range(0.1f, 0.9f).ToString("F1"));

        ObjectMat.staticFriction = 0.1f;

        FC = ObjectMat.staticFriction;

        temp = FC;

        angle = (Mathf.Atan(FC));

        FrictionValue.text = FC.ToString("F1");

        BlockToSlide.transform.parent = SlopeObject.transform;

        BlockToSlide.transform.localPosition = BlockInitPos;

        BlockToSlide.transform.localEulerAngles = BlockInitRot;

        angleValue.text = (Mathf.CeilToInt(angle * Mathf.Rad2Deg)).ToString() + " °";

        AngleBlend.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(0, 100 - ExplodeSlider.value * 0.277f);
        SlopeObject.transform.localEulerAngles = new Vector3(0, 0, -ExplodeSlider.value);
        //InputField_FC.text = FC.ToString();

        TalkBubble_Label.SetActive(false);
        Linefs.SetActive(true);
        Linefk.SetActive(false);

    }

    public void SubmitFun()
    {

        float res;

        if (inputValu1.text != "")
        {

            if (MainObj.name == "MainObject_0")
            {
                res = Mathf.CeilToInt(angle * Mathf.Rad2Deg);
                Debug.Log(res);
                if (res.ToString() == inputValu1.text)
                {
                    MessageT.text = CorrectMsgs[UnityEngine.Random.Range(0, CorrectMsgs.Length)];
                    MessageT.color = Color.green;
                    //ChairHeightText.text = ResultT.text + " m";
                    inputValu1.interactable = false;
                    SliderBlock.SetActive(false);
                    // Invoke("ClosePanel", 5);

                }
                else
                {
                    MessageT.text = "Wrong\nTry again";
                    MessageT.color = Color.red;

                
                }

            }
            else if (MainObj.name == "MainObject_1")
            {
                res = Mathf.Sqrt((2 * L) / a);

                Debug.Log(res);
                if ((res >= float.Parse(inputValu1.text) - 0.02f) && (res <= float.Parse(inputValu1.text) + 0.02))
                {
                    MessageT.text = CorrectMsgs[UnityEngine.Random.Range(0, CorrectMsgs.Length)];
                    MessageT.color = Color.green;
                    //ChairHeightText.text = ResultT.text + " m";
                    inputValu1.interactable = false;
                    SliderBlock.SetActive(false);
                    // Invoke("ClosePanel", 5);

                }
                else
                {
                    MessageT.text = "Wrong\nTry again";
                    MessageT.color = Color.red;

                  
                }
            }
            else if (MainObj.name == "MainObject_2")
            {

                float MgSinTheta = (1 * 9.8f * Mathf.Sin(angle));

                res = MgSinTheta;
                Debug.Log(res);

                if ((res >= float.Parse(inputValu1.text) - 0.02f) && (res <= float.Parse(inputValu1.text) + 0.02))
                {
                    MessageT.text = CorrectMsgs[UnityEngine.Random.Range(0, CorrectMsgs.Length)];
                    MessageT.color = Color.green;
                    //ChairHeightText.text = ResultT.text + " m";
                    inputValu1.interactable = false;
                    SliderBlock.SetActive(false);
                    // Invoke("ClosePanel", 5);

                }
                else
                {
                    MessageT.text = "Wrong\nTry again";
                    MessageT.color = Color.red;

                  
                }


            }


            // Camera.main.SendMessage("OnLevelSubmit", "Done");



        }
        else
        {
            MessageT.text = "Enter a value...";
            MessageT.color = Color.red;

           
        }



    }

    public void ShadowValue(float value,Text txt)
    {
        //if ((Mathf.CeilToInt(ExplodeSlider.value * 1.045f)) >= (Mathf.CeilToInt(angle * Mathf.Rad2Deg)))
        //{
        // 
        // }
        // print(Mathf.CeilToInt(angle * Mathf.Rad2Deg) + "  angle  " + (Mathf.CeilToInt(ExplodeSlider.value * 1.045f)));
        if (OptionUnlockedTill < 1)
        {
            if (TopicToCheck == 2)
            {
                if (FC != temp)
                {
                    SlopeObject.transform.localEulerAngles = new Vector3(0, 0, -value);

                    txt.text = (Mathf.CeilToInt(ExplodeSlider.value * 1.045f)).ToString() + " °";
                    AngleBlend.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(0, 100 - ExplodeSlider.value * 0.277f);
                }
            }
            else {
                SlopeObject.transform.localEulerAngles = new Vector3(0, 0, -value);
                AngleBlend.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(0, 100 - ExplodeSlider.value * 0.277f);
                txt.text = (Mathf.CeilToInt(ExplodeSlider.value * 1.045f)).ToString() + " °";
            }
        }
        else {
            SlopeObject.transform.localEulerAngles = new Vector3(0, 0, -value);
            AngleBlend.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(0, 100 - ExplodeSlider.value * 0.277f);
            txt.text = (Mathf.CeilToInt(ExplodeSlider.value * 1.045f)).ToString() + " °";
        }

    }
}
