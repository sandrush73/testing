﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;
using System.Text.RegularExpressions;

public class vid126_trigonometry_balloon : MonoBehaviour, IPointerDownHandler
{
    // Start is called before the first frame update
    public static vid126_trigonometry_balloon Instance;

    public TextAsset CSVFile;
    public AssetBundle assetBundle;
    public Canvas C_BG;

    public orbit CamOrbit;

    public AROrbitControls ArOrbit;

    public bool IsAR;

    public GameObject MainObj, Ground;

    public int OptionUnlockedTill, CurrentTopicSelected, TopicToCheck;
    public float Sub_XP_Earned, Total_XP;

    public List<Dictionary<string, object>> CSV_data;

    public List<String> TaskList, XpList, InfoText;

    public List<GameObject> AllTaskData;

    public GameObject[] Labels;

    public Text XP_ToShow, XP_Effect, Main_XP_ToShow, localTotalXpValueText, localGainedXpValueText, ObjectiveInfoText, LGListText, IListText, AListText,
        InfoHeadingText, GF_ResultTxt, E_valueTxt, m_valtxt;

    public GameObject TaskObj, AllTaskListObj;
    public RectTransform rT;
    public int localTotalXpValue, localGaniedXpValue, MainXpValue;
    public GameObject ControlPanel, LocalXP, Main_Xp, ObjectivePanel, InformationPanel, InfoButton, MainSlider, InfoPanelBg, TasklistPanel, WronOptionPanel, Gained_XP_ArrowBut, PopUpPanel;
    public Button InfoCloseBtn, InfoContinueBtn, Gained_XP_Panel, CpCloseBtn, IncButton, DecButton;
    public bool sessionStart, label_onbool;
    //public Slider Slider;
    /// <summary>
    /// Topic variable
    /// </summary>
    /// 
    public GameObject infoPanel,betaLine,alphaLine,moveObj,assetgroup,topline,bottomline,rightline,dis_cal_group, dis_cal_2,ref_sphere,line_6,angleblend_1, angleblend_2,beta_txtvalue,alpha_txtvalue,talkbubble,point_a, point_b,
        point_c,point_d,point_e,dis_txt;
    public Text velocity_txt, time_txt, beta_txt, alpha_txt, dis_ans, cal2ans;
    public int beta_value, alpha_value,round_num;
    public int[] beta_alpha = new int[] { 15, 30, 45, 60, 75 };
    public int r_pickvalue, V, T;
    public int[] velocity_values = new int[] { 1,2,3, };
    public int[] time_values = new int[] { 2, 3, 4 };
    public Vector3 finalPos, toplineStartPos;
    public float t, slope, x1, x2, y1, y2, theta, D;
    public GameObject inputField0, inputField1;
    public Button button_submit, but_submit_1;
    public GameObject inputField21, inputField22, inputField23, inputField24, inputField25, inputField26;
    public bool update_bool;
    private float alpha_scle, topline_scale, bottomline_scale, r_l_scale;
    private Vector3 bottomTargetPos, right_line_start_pos;
    private Camera cam;

    //public GameObject Parentcube1, Parentcube2, clone_obj1, clone_obj2, Pos_button, Neg_button, total_label, flylabel, bot_obj, submit_button, buttongroup, but_add_a, but_add_b, but_pick_a, but_pick_b, but_clear, but_dummy,addend_a,addend_b,but_droptoken;
    //public int pos_value, neg_value, res_value;
    //public List<GameObject> green_obj = new List<GameObject>();
    //public List<GameObject> red_obj = new List<GameObject>();
    //public bool collision_one;
    //public float y_pos, sel_addend_side;
    //public int n_value, ran_x, ran_y, ran_res,dummy_res,dummy_x,dummy_y,ran_que,pick_cnt;
    //public Rect UIcanvas;
    //public GameObject obj1, obj2;


    void Start()
    {
        
        Instance = this;
       
        LoadFromAsssetBundle();

        sessionStart = false;
        //OptionUnlockedTill = 0;
        //TopicToCheck = 1;
        //MainXpValue = 10;
        label_onbool = false;
       
        MainObj = this.gameObject;

        C_BG = GameObject.Find("Canvas_BG").GetComponent<Canvas>();

        Ground = GameObject.Find("Ground");

        if (!IsAR)
        {
            CamOrbit = Camera.main.GetComponentInParent<orbit>();

            C_BG.transform.parent = Camera.main.transform;
            C_BG.GetComponentInChildren<RawImage>().enabled = true;
            C_BG.renderMode = RenderMode.ScreenSpaceCamera;
            // C_BG.gameObject.SetActive(false);
            C_BG.worldCamera = Camera.main;

            CamOrbit.target = Camera.main.transform.parent;
            CamOrbit.target.position = new Vector3(0, 3f, 0);

            CamOrbit.maxRotUp = 45;
            CamOrbit.minRotUp = 1;
            CamOrbit.zoomMin = 15f;
            CamOrbit.zoomMax = 25f;
            CamOrbit.zoomStart = 45f;
            CamOrbit.cameraRotSideStart = 0;
            CamOrbit.cameraRotUpStart = 10;
            CamOrbit.Start();
        }
        else
        {
            Ground.SetActive(false);

            C_BG.gameObject.SetActive(false);

            ArOrbit = MainObj.transform.parent.gameObject.GetComponent<AROrbitControls>();
        }

        ReadingDataFromCSVFile();
        ReadingXps();

        FindingObjects();
        TaskListCreation();
        AssigningClickEvents();
        //  LabelsAssignment();
        AssignInitialValues();


        LocalXpCalculation();


    }

    public void LoadFromAsssetBundle()
    {
        //#if UNITY_EDITOR

        //string CSVName = transform.parent.name;

        //if (CSVName.Contains("Clone"))
        //{

        //    CSVName = CSVName.Substring(0, CSVName.Length - 7);
        //}

        //print("casvname........." + CSVName);

        //CSVFile = Resources.Load(CSVName + "CSV") as TextAsset;
        //#elif UNITY_ANDROID


        assetBundle = AssetBundle.LoadFromFile(PlayerPrefs.GetString("AssetBundleUrl"));

        CSVFile = assetBundle.LoadAsset(PlayerPrefs.GetString("LibraryName") + "CSV", typeof(TextAsset)) as TextAsset;

       // TransparentMaterial = assetBundle.LoadAsset("TransparentMaterial", typeof(Material)) as Material;
       // TransparentMaterial.shader = Shader.Find("Standard");

        //#elif UNITY_IPHONE
        //        assetBundle = AssetBundle.LoadFromFile(PlayerPrefs.GetString("AssetBundleUrl"));

        //        CSVFile = assetBundle.LoadAsset(PlayerPrefs.GetString("LibraryName") + "CSV", typeof(TextAsset)) as TextAsset;

        //        TransparentMaterial = assetBundle.LoadAsset("TransparentMaterial", typeof(Material)) as Material;
        //        TransparentMaterial.shader = Shader.Find("Standard");
        //#endif

        Camera.main.gameObject.AddComponent<PhysicsRaycaster>();

        Camera.main.backgroundColor = Color.black;

        GameObject TargetForCamera = GameObject.Find("Target");

        if (TargetForCamera != null)
        {
            TargetForCamera.AddComponent<orbit>();
            CamOrbit = TargetForCamera.GetComponent<orbit>();
            IsAR = false;
        }
        else
        {
            this.transform.parent.gameObject.AddComponent<AROrbitControls>();
            ArOrbit = this.transform.parent.gameObject.GetComponent<AROrbitControls>();
            IsAR = true;
        }

    }

    public void ReadingDataFromCSVFile()
    {

        TaskList = new List<string>();
        XpList = new List<string>();
        InfoText = new List<string>();

        AllTaskData = new List<GameObject>();

        CSV_data = CSVReader.Read(CSVFile);


        for (var i = 0; i < CSV_data.Count; i++)
        {

            if (CSV_data[i]["TaskList"].ToString() != "" && CSV_data[i]["TaskList"].ToString() != null)
            {
                TaskList.Add(CSV_data[i]["TaskList"].ToString());

            }
            //if (CSV_data[i]["XPList"].ToString() != "" && CSV_data[i]["XPList"].ToString() != null)
            //{
            //    XpList.Add(CSV_data[i]["XPList"].ToString());

            //}

            //if (CSV_data[i]["InfoText"].ToString() != "" && CSV_data[i]["InfoText"].ToString() != null)
            //{
            //    InfoText.Add(CSV_data[i]["InfoText"].ToString());

            //}
        }
    }
    public int totXp, loclXp;

    public void ReadingXps()
    {

        //PlayerPrefs.SetInt("TotalXP", 100);
       // PlayerPrefs.SetInt("MaxEarnings", 20);

        totXp = PlayerPrefs.GetInt("TotalXP");
        loclXp = PlayerPrefs.GetInt("MaxEarnings");

        if (loclXp != 0)
        {
            OptionUnlockedTill = 0;

        }
        else
        {
            OptionUnlockedTill = 1;

        }

        TopicToCheck = 1;

        MainXpValue = totXp;

        int t = 0;
        int p = 0;

        for (int k = 0; k < TaskList.Count; k++)
        {

            p = loclXp / TaskList.Count;
            XpList.Add(p.ToString());
            t = t + p;
        }

        if (t != loclXp)
        {
            XpList[XpList.Count - 1] = (p + (loclXp - t)).ToString();
        }
    }

    public float result;

    public void GetResult()
    {
        result = ((float)(TopicToCheck - 1) / (float)(TaskList.Count)) * 100;
        Camera.main.SendMessage("UnloadActually", result);
    }


    public void FindingObjects() {

        InformationPanel = GameObject.Find("InformationPanel");

        InfoCloseBtn = GameObject.Find("InfoCloseBtn").GetComponent<Button>();

        InfoContinueBtn = GameObject.Find("InfoContinueBtn").GetComponent<Button>();

        InfoPanelBg = GameObject.Find("InfoPanelBg");

        XP_ToShow = GameObject.Find("localGainedXpValueText").GetComponent<Text>();

        XP_Effect = GameObject.Find("Effect_Text").GetComponent<Text>();

        Main_XP_ToShow = GameObject.Find("Main_XP_Value").GetComponent<Text>();

        localGainedXpValueText = GameObject.Find("localGainedXpValueText").GetComponent<Text>();

        localTotalXpValueText = GameObject.Find("GainedXP_LocalTotalXp").GetComponent<Text>();

        ObjectiveInfoText = GameObject.Find("ObjectiveInfoText").GetComponent<Text>();

        TaskObj = GameObject.Find("TaskObj");

        AllTaskListObj = GameObject.Find("AllTaskList");

        ControlPanel = GameObject.Find("ControlPanel");
        LocalXP = GameObject.Find("LocalXP");
        Main_Xp = GameObject.Find("Main_Xp");
        ObjectivePanel = GameObject.Find("ObjectivePanel");
        InformationPanel = GameObject.Find("InformationPanel");
        InfoButton = GameObject.Find("InfoButton");
        MainSlider = GameObject.Find("MainSlider");
        WronOptionPanel = GameObject.Find("WrongPanel");

        Gained_XP_Panel = GameObject.Find("Gained_XP_Panel").GetComponent<Button>();
        TasklistPanel = GameObject.Find("TasklistPanel");
        CpCloseBtn = GameObject.Find("CpCloseBtn").GetComponent<Button>();

        InfoHeadingText = GameObject.Find("InfoHeadingText").GetComponent<Text>();
        LGListText = GameObject.Find("LGListText").GetComponent<Text>();
        IListText = GameObject.Find("IListText").GetComponent<Text>();
        AListText = GameObject.Find("AListText").GetComponent<Text>();
        PopUpPanel = GameObject.Find("PopUpPanel");


        Gained_XP_ArrowBut = GameObject.Find("Gained_XP_ArrowBut");


        ////
        ///

        betaLine = GameObject.Find("Line_1");
        alphaLine = GameObject.Find("Line_2");
        moveObj = GameObject.Find("TravelObj");
        assetgroup = GameObject.Find("Assets");
        topline = GameObject.Find("Line_3");
        bottomline = GameObject.Find("Line_4");
        rightline = GameObject.Find("Line_5");
        line_6 = GameObject.Find("Line_6");
        velocity_txt = GameObject.Find("velocity").GetComponent<Text>();
        time_txt = GameObject.Find("timeValue").GetComponent<Text>();
        beta_txt = GameObject.Find("betaValue").GetComponent<Text>();
        dis_ans = GameObject.Find("Ans").GetComponent<Text>();
        alpha_txt = GameObject.Find("alphaValue").GetComponent<Text>();
        inputField0 = GameObject.Find("InputField 0");
        inputField1 = GameObject.Find("InputField 1");
        button_submit = GameObject.Find("Start1").GetComponent<Button>();
        button_submit.onClick.AddListener(() => DistanceCalculation());
        button_submit.gameObject.GetComponentInChildren<Text>().text = "Submit";
        dis_cal_group = GameObject.Find("DisCal1");
        dis_cal_group.SetActive(true);
        dis_cal_2 = GameObject.Find("DisCal2");
        ref_sphere = GameObject.Find("Refpoint");
        but_submit_1 = GameObject.Find("Start2").GetComponent<Button>();
        but_submit_1.gameObject.GetComponentInChildren<Text>().text = "Submit";
        but_submit_1.onClick.AddListener(() => HightCalculation());

        inputField21 = GameObject.Find("InputField 20");
        inputField22 = GameObject.Find("InputField 21");
        inputField23 = GameObject.Find("InputField 22");
        inputField24 = GameObject.Find("InputField 23");
        inputField25 = GameObject.Find("InputField 24");
        inputField26 = GameObject.Find("InputField 25");
        cal2ans = GameObject.Find("cal2Ans").GetComponent<Text>();
        ref_sphere.AddComponent<Trigno_ColorLerp>();
        dis_cal_2.SetActive(false);
        angleblend_1 = GameObject.Find("Angle_blend");
        angleblend_2 = GameObject.Find("Angle_blend2");
        beta_txtvalue = GameObject.Find("betavalue");
        alpha_txtvalue = GameObject.Find("alphavalue");
        talkbubble = GameObject.Find("Talkbubble");
        point_a = GameObject.Find("points 1");
        point_b = GameObject.Find("points 2");
        point_c = GameObject.Find("points 3");
        point_d = GameObject.Find("points 4");
        point_e = GameObject.Find("points 5");
        dis_txt = GameObject.Find("distance");

        // r_pickvalue = UnityEngine.Random.Range(0, 4);
        //r_pickvalue = UnityEngine.Random.Range(0, 4);
        // StartCoroutine("RandomMethod");
        V = VelocityPick();
        T = TimePick();
        talkbubble.transform.GetChild(2).GetComponent<TextMeshPro>().text = "Velocity V = " + V + "\n" + "Time T = " + T;
        alpha_value = alpha_pick();
        // yield return new WaitForSeconds(.1f);
        beta_value = beta_pick();
        InvokeRepeating("RandomMethod", 0.1f, 0.001f);
        update_bool = false;



    }


    public void AssigningClickEvents() {

        iTween.ScaleTo(InformationPanel.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

        InfoCloseBtn.onClick.AddListener(() => CloseInfoPanel());

        InfoContinueBtn.onClick.AddListener(() => ContinueInfoPanel());

        InfoButton.GetComponent<Button>().onClick.AddListener(() => OpenInfoPanel());

        InfoButton.transform.GetChild(0).GetComponent<Button>().onClick.AddListener(() => Labels_on());
        Gained_XP_Panel.onClick.AddListener(() => OpenTaskList());

        CpCloseBtn.onClick.AddListener(() => OpenOrCloseControlPanel());

        //  MainSlider.GetComponent<Slider>().onValueChanged.AddListener(delegate { ChangeDistance(MainSlider.GetComponent<Slider>().value, MainSlider.GetComponentInChildren<Text>()); });

        AddListener(EventTriggerType.PointerDown, DisableOrbit, MainSlider.gameObject);

        AddListener(EventTriggerType.PointerUp, EnableOrbit, MainSlider.gameObject);

        //Slider_distance.onValueChanged.AddListener(delegate { ChangeDistance(Slider_distance.value, Slider_distance.GetComponentInChildren<Text>()); });

        AddListener(EventTriggerType.PointerDown, DisableOrbit, ControlPanel.transform.GetChild(0).gameObject);

        AddListener(EventTriggerType.PointerUp, EnableOrbit, ControlPanel.transform.GetChild(0).gameObject);

        AddListener(EventTriggerType.PointerUp, RadiusAddXp, MainSlider.gameObject);

        inputField0.GetComponent<Button>().onClick.AddListener(() => ReadValueFromClick(1,talkbubble.transform.GetChild(2).gameObject, inputField0,V));
        inputField1.GetComponent<Button>().onClick.AddListener(() => ReadValueFromClick(2, talkbubble.transform.GetChild(2).gameObject, inputField1, T));

        inputField21.GetComponent<Button>().onClick.AddListener(() => ReadValueFromClick(4, talkbubble.transform.GetChild(2).gameObject, inputField21, V));
        inputField22.GetComponent<Button>().onClick.AddListener(() => ReadValueFromClick(4, talkbubble.transform.GetChild(2).gameObject, inputField22, T));
        inputField23.GetComponent<Button>().onClick.AddListener(() => ReadValueFromClick(4, alpha_txtvalue.gameObject, inputField23, alpha_value));
        inputField24.GetComponent<Button>().onClick.AddListener(() => ReadValueFromClick(4, beta_txtvalue.gameObject, inputField24, beta_value));
        inputField25.GetComponent<Button>().onClick.AddListener(() => ReadValueFromClick(4, alpha_txtvalue.gameObject, inputField25, alpha_value));
        inputField26.GetComponent<Button>().onClick.AddListener(() => ReadValueFromClick(4, beta_txtvalue.gameObject, inputField26, beta_value));

        //addend_a.GetComponent<SpriteRenderer>().sprite = but_dummy.GetComponent<Button>().spriteState.pressedSprite;
        //addend_b.GetComponent<SpriteRenderer>().sprite = but_dummy.GetComponent<Button>().spriteState.disabledSprite;
    }









    public void AssignInitialValues()
    {


        //InfoHeadingText.text = InfoText[0];
        //LGListText.text = InfoText[1];
        //IListText.text = InfoText[2];
        //AListText.text = InfoText[3];


        InfoCloseBtn.gameObject.transform.localScale = new Vector3(0, 0, 0);
        localGainedXpValueText.text = "0";
        Main_XP_ToShow.text = MainXpValue + "";
        WronOptionPanel.transform.localScale = new Vector3(0, 0, 0);

        if (OptionUnlockedTill > 0)
        {
            LocalXP.transform.localScale = new Vector3(0, 0, 0);
            ObjectivePanel.transform.localScale = new Vector3(0, 0, 0);
        }

        // ChangeDistance(Slider_distance.value, Slider_distance.GetComponentInChildren<Text>());
        //ChangeRadius(MainSlider.GetComponent<Slider>().value, MainSlider.GetComponentInChildren<Text>());
        //  ScannerObject.AddComponent<RendererOffset>();
        //  ScannerObject.GetComponent<RendererOffset>().offset = -0.4f;
        //   ScannerObject.SetActive(false);

        // Labels[1].transform.localPosition = MidPoint(Dummy, Ball);
        //   Labels[0].transform.localPosition = MidPoint(Ball, MTape);
    }



    public void Update()
    {
        if (update_bool)
        {
            x1 = betaLine.transform.localPosition.x;
            y1 = betaLine.transform.localPosition.y;
            x2 = moveObj.transform.localPosition.x;
            y2 = moveObj.transform.localPosition.y;

            slope = (y2 - y1) / (x2 - x1);
            theta = Mathf.Atan(slope);
            theta = theta * Mathf.Rad2Deg;
            alphaLine.transform.localEulerAngles = new Vector3(0, 0, theta);
            alpha_scle = Vector3.Distance(betaLine.transform.localPosition, moveObj.transform.localPosition);
            alphaLine.transform.localScale = new Vector3(alpha_scle, alphaLine.transform.localScale.y, alphaLine.transform.localScale.z);
            topline_scale = Vector3.Distance(toplineStartPos, moveObj.transform.localPosition);
            topline.transform.localScale = new Vector3(topline_scale, topline.transform.localScale.y, topline.transform.localScale.z);
            bottomTargetPos = new Vector3(moveObj.transform.localPosition.x, betaLine.transform.localPosition.y, betaLine.transform.localPosition.z);
            bottomline_scale = Vector3.Distance(betaLine.transform.localPosition, bottomTargetPos);
            bottomline.transform.localScale = new Vector3(bottomline_scale, bottomline.transform.localScale.y, bottomline.transform.localScale.z);
            right_line_start_pos = new Vector3(moveObj.transform.localPosition.x, betaLine.transform.localPosition.y, betaLine.transform.localPosition.z);
            rightline.transform.localPosition = right_line_start_pos;
            r_l_scale = Vector3.Distance(right_line_start_pos, moveObj.transform.localPosition);
            rightline.transform.localScale = new Vector3(r_l_scale, rightline.transform.localScale.y, rightline.transform.localScale.z);
            line_6.transform.localScale = rightline.transform.localScale;
            angleblend_1.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(0, 100-(0.27777f * betaLine.transform.localEulerAngles.z));
            angleblend_2.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(0, 100 - (0.27777f * alphaLine.transform.localEulerAngles.z));
            beta_txtvalue.GetComponent<TextMeshPro>().text = betaLine.transform.localEulerAngles.z.ToString()+ "°";
           // alpha_txtvalue.GetComponent<TextMeshPro>().text = alphaLine.transform.localEulerAngles.z.ToString("F2");
            alpha_txtvalue.GetComponent<TextMeshPro>().text =alpha_value.ToString()+ "°";
            point_b.transform.localPosition = new Vector3(line_6.transform.localPosition.x+0.2f, point_b.transform.localPosition.y, point_b.transform.localPosition.z);
            point_c.transform.localPosition = new Vector3(rightline.transform.localPosition.x+0.2f, point_b.transform.localPosition.y, point_b.transform.localPosition.z);
            point_d.transform.localPosition = new Vector3(rightline.transform.localPosition.x + 0.8f, topline.transform.localPosition.y - 0.3f, point_b.transform.localPosition.z);
            point_e.transform.localPosition = new Vector3(line_6.transform.localPosition.x + 0.8f, topline.transform.localPosition.y - 0.3f, point_b.transform.localPosition.z);
            dis_txt.transform.localPosition = new Vector3(line_6.transform.localPosition.x + (topline.transform.localScale.x/2)-1.2f, topline.transform.localPosition.y + 0.3f, topline.transform.localPosition.z);
            dis_txt.GetComponent<TextMeshPro>().text ="Distance D = " + (D*2).ToString()+" m";
            if (beta_value == 15)
            {
                beta_txtvalue.transform.localPosition = new Vector3(1.35f,10.75f,0.071f);

            }
            else if (beta_value == 30)
            {
                beta_txtvalue.transform.localPosition = new Vector3(2.53f, 11.83f, 0.071f);

            }
            else if (beta_value == 45)
            {
                beta_txtvalue.transform.localPosition = new Vector3(1.35f, 11.95f, 0.071f);

            }
            else if (beta_value == 60)
            {
                beta_txtvalue.transform.localPosition = new Vector3(0.992f, 12.423f, 0.071f);

            }
            else if (beta_value == 75)
            {
                beta_txtvalue.transform.localPosition = new Vector3(0.34f, 12.28f, 0.071f);

            }
        }


    }

    public void TaskListCreation() {

        rT = AllTaskListObj.transform.parent.GetComponent<RectTransform>();

        localGaniedXpValue = 0;

        for (int i = 0; i < TaskList.Count; i++)
        {

            GameObject Tl = (GameObject)Instantiate(TaskObj);
            Tl.transform.SetParent(AllTaskListObj.transform);
            Tl.transform.localScale = new Vector3(1, 1, 1);
            Tl.name = i.ToString();

            AllTaskData.Add(Tl);
            AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(false);
            AllTaskData[i].gameObject.transform.GetChild(2).gameObject.SetActive(false);
            AllTaskData[i].gameObject.transform.GetChild(3).GetComponentInChildren<Text>().text = TaskList[i];
            AllTaskData[i].gameObject.transform.GetChild(4).GetComponentInChildren<Text>().text = "+" + XpList[i] + " XP";

            localTotalXpValue += int.Parse(XpList[i]);
        }

        rT.sizeDelta = new Vector2(rT.sizeDelta.x, TaskList.Count * 50);
        localTotalXpValueText.text = "/" + localTotalXpValue.ToString();

        AllTaskData[0].gameObject.transform.GetChild(2).gameObject.SetActive(true);
        ObjectiveInfoText.text = AllTaskData[0].gameObject.transform.GetChild(3).GetComponentInChildren<Text>().text;
    }

    public void LocalXpCalculation() {

       
            for (int i = 0; i < TaskList.Count; i++)
            {
                AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(false);
                AllTaskData[i].gameObject.transform.GetChild(2).gameObject.SetActive(false);
            }

        if (OptionUnlockedTill < 1)
        {
            if ((TopicToCheck) < (TaskList.Count + 1))
            {
                localGaniedXpValue = 0;

                for (int i = 0; i < TopicToCheck - 1; i++)
                {
                    localGaniedXpValue += int.Parse(XpList[i]);
                }

                for (int i = 0; i < TopicToCheck; i++)
                {
                    AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(true);
                }

                localGainedXpValueText.text = localGaniedXpValue.ToString();

                AllTaskData[TopicToCheck - 1].gameObject.transform.GetChild(2).gameObject.SetActive(true);

                ObjectiveInfoText.text = AllTaskData[TopicToCheck - 1].gameObject.transform.GetChild(3).GetComponentInChildren<Text>().text;
            }
            else
            {
                for (int i = 0; i < TaskList.Count; i++)
                {
                    AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(true);
                    AllTaskData[i].gameObject.transform.GetChild(2).gameObject.SetActive(false);
                }

                localGainedXpValueText.text = localTotalXpValue.ToString();

                XP_Effect.text = "+" + localTotalXpValue + " XP";

                XP_Effect.transform.position = (XP_ToShow.transform.position);

                iTween.Stop(XP_Effect.gameObject);

                iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeOutElastic));

                iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", XP_Effect.transform.position.x, "y", XP_Effect.transform.position.y + 50, "z", 0, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

                iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", Main_XP_ToShow.transform.position.x, "y", Main_XP_ToShow.transform.position.y, "z", 0, "delay", 1.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

                iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "delay", 2f, "time", 0.5f, "easetype", iTween.EaseType.linear, "oncomplete", "MainXpCalculation", "oncompletetarget", this.gameObject));
            }
        }
        else
        {
            for (int i = 0; i < TaskList.Count; i++)
            {
                AllTaskData[i].gameObject.transform.GetChild(1).gameObject.SetActive(true);
                AllTaskData[i].gameObject.transform.GetChild(2).gameObject.SetActive(false);
            }

            localGainedXpValueText.text = localTotalXpValue.ToString();   
        }
    }

    public void CloseAllpanels()
    {

        ControlPanel.GetComponent<Animator>().Play("CpanelFullClose");
        LocalXP.GetComponent<Animator>().Play("LocalXpPanelClose");
        Main_Xp.GetComponent<Animator>().Play("MainXpPanelClose");
       // ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelClose");
        InfoButton.GetComponent<Animator>().Play("InfoButtonClose");
        MainSlider.GetComponent<Animator>().Play("MainSliderClose");
        
        if(TasklistPanel.gameObject.transform.localScale.y!=0)
            iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));
    }

    public void OpenAllpanels()
    {
        if (OptionUnlockedTill < 1)
        {
            if (TopicToCheck <= (TaskList.Count))
            {
                LocalXP.GetComponent<Animator>().Play("LocalXpPanelOPen");
                ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen");
            }
        }

        ControlPanel.GetComponent<Animator>().Play("CpanelOpen");

        Main_Xp.GetComponent<Animator>().Play("MainXpPanelOpen");

        InfoButton.GetComponent<Animator>().Play("InfoButtonOpen");
        MainSlider.GetComponent<Animator>().Play("MainSliderOpen");
        iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "time", 0.2f, "easetype", iTween.EaseType.linear));
        iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));


    }


    public void AddXpEffect() {

        
        if (TopicToCheck <= (TaskList.Count))
        {
            // Camera.main.GetComponent<PhysicsRaycaster>().enabled = false;
            ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelClose");
            GameObject.Find("Canvas").GetComponent<CanvasGroup>().interactable = false;

            XP_Effect.GetComponent<RectTransform>().anchoredPosition = new Vector2(0,0);

            XP_Effect.text = "+"+int.Parse(XpList[TopicToCheck - 1]) + " XP";

            XP_Effect.transform.localScale = new Vector3(1, 0, 1);

           // iTween.Stop(XP_Effect.gameObject);

            iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeOutElastic));

            iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", XP_Effect.transform.position.x, "y", XP_Effect.transform.position.y + 50, "z", 0, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

            iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", XP_ToShow.transform.position.x, "y", XP_ToShow.transform.position.y, "z", 0, "delay", 1.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

            iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "delay", 2f, "time", 0.5f, "easetype", iTween.EaseType.linear, "oncomplete", "LocalXpCalculation", "oncompletetarget", this.gameObject));

            TopicToCheck++;

            Invoke("EnableRayCast", 3);
        }
        
    }

    public void EnableRayCast() {
        //Camera.main.GetComponent<PhysicsRaycaster>().enabled = true;
        GameObject.Find("Canvas").GetComponent<CanvasGroup>().interactable = true;
       // if (TopicToCheck <= (TaskList.Count))
        {
            ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen");
          //  Invoke("ResetToNext", 3.3f);
        }
       
    }

    public IEnumerator MainXpCalculation()
    {
        if (OptionUnlockedTill == 0)
        {
            OptionUnlockedTill = 1;

            MainXpValue += localTotalXpValue;

            Main_XP_ToShow.text = MainXpValue.ToString();

            yield return new WaitForSeconds(1f);

            LocalXP.GetComponent<Animator>().Play("LocalXpPanelClose");

            ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelClose");

            if ((ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelClose")) || (ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelFullClose")))
            {
                ControlPanel.GetComponent<Animator>().Play("CpanelOpen");
            }
        }

    }

    public void CloseInfoPanel()
    {
        sessionStart = true;
        InfoPanelBg.SetActive(false);
        iTween.ScaleTo(InformationPanel.gameObject, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));
        OpenAllpanels();
        ControlPanel.GetComponent<Animator>().Play("CpanelOpen");
    }

    public void ContinueInfoPanel()
    {
       // iTween.MoveTo(bot_obj, iTween.Hash("x", 0, "y", 0.6f, "z", 0, "delay", 0.5f, "time", 0.5f,"islocal",true, "easetype", iTween.EaseType.easeInSine));
        sessionStart = true;
        InfoPanelBg.SetActive(false);
        iTween.ScaleTo(InformationPanel.gameObject, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));
        InfoCloseBtn.transform.localScale = new Vector3(0.27f, 0.27f, 0.27f);
        InfoContinueBtn.transform.localScale = new Vector3(0, 0, 0);
        OpenAllpanels();
        update_bool = true;
    }

    public void OpenInfoPanel()
    {
        sessionStart = false;
        iTween.ScaleTo(InformationPanel.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.01f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));
        InfoPanelBg.SetActive(true);
        iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "time", 0.2f, "easetype", iTween.EaseType.linear));
        iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));
        CloseAllpanels();
    }

    public void OpenTaskList() {


        if (TasklistPanel.gameObject.transform.localScale.y != 1)
        {
            iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.01f, "time", 0.2f, "easetype", iTween.EaseType.easeInSine));
            
            ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelClose");


            //Gained_XP_ArrowBut

            iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 180, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));

            if ((ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelOpen")))
            {
                ControlPanel.GetComponent<Animator>().Play("CpanelClose");
            }
        }
        else {
            iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "delay", 0.01f, "time", 0.2f, "easetype", iTween.EaseType.easeInSine));
            iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));

            ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen");

            ControlPanel.GetComponent<Animator>().Play("CpanelOpen");
        }
    }

    public void OpenOrCloseControlPanel()
    {

        if ((ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelClose")) || (ControlPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CpanelFullClose")))
        {
              ControlPanel.GetComponent<Animator>().Play("CpanelOpen");
            update_bool = true;
        }
        else
        {
            ControlPanel.GetComponent<Animator>().Play("CpanelClose");
        }
    }

    public void WrongOptionSelected()
    {
        WronOptionPanel.transform.localScale = new Vector3(0, 0, 0);

        iTween.Stop(WronOptionPanel.gameObject);

        iTween.ScaleTo(WronOptionPanel.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.01f, "time", 0.2f, "easetype", iTween.EaseType.easeInSine));

        iTween.ScaleTo(WronOptionPanel.gameObject, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 1f, "time", 0.2f, "easetype", iTween.EaseType.easeInSine));

        // InfoPanelBg.SetActive(true);

        Invoke("CloseBlurBg", 1f);

        GameObject.Find("Canvas").GetComponent<CanvasGroup>().interactable = false;

        ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelClose");


        var mouse = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);

        if (mouse.x < Screen.width / 2)
        {
            // WronOptionPanel
            //print("Mouse is on left side of screen.");
            WronOptionPanel.GetComponent<RectTransform>().pivot = new Vector2(0, 0);
        }

        if (mouse.x > Screen.width / 2)
        {
            //print("Mouse is on right side of screen.");
            WronOptionPanel.GetComponent<RectTransform>().pivot = new Vector2(1, 0);
        }


        WronOptionPanel.transform.position = Input.mousePosition;

    }


    public void CloseBlurBg()
    {
        InfoPanelBg.SetActive(false);
        GameObject.Find("Canvas").GetComponent<CanvasGroup>().interactable = true;

        if (OptionUnlockedTill < 1)
        {
            if (InformationPanel.gameObject.transform.localScale.x == 0f)
            {
                ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen", 0, 0f);
            }
        }
    }


    public void OnPointerDown(PointerEventData eventData)
    {
        if (InformationPanel.gameObject.transform.localScale.x != 1 && sessionStart)
        {
            if (OptionUnlockedTill < 1)
            {
                if (TopicToCheck <= (TaskList.Count))
                {
                    iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "time", 0.2f, "easetype", iTween.EaseType.linear));
                    iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));
                    ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen");
                    ControlPanel.GetComponent<Animator>().Play("CpanelOpen");
                }
            }
        }
    }


    public void LabelsAssignment()
    {
        GameObject Label = GameObject.Find("Labels");

        Labels = new GameObject[Label.transform.childCount];

        for (int j = 0; j < Labels.Length; j++)
        {
            Labels[j] = Label.transform.GetChild(j).gameObject;

            GameObject TempLabels = Labels[j];

            foreach (Transform ChildLabel in TempLabels.transform)
            {
                if (ChildLabel.name.Contains("SmoothLook"))
                {
                    ChildLabel.gameObject.AddComponent<SmoothLook>();
                   // ChildLabel.gameObject.AddComponent<ScaleRelativeToCamera>();

                }
            }
        }
                     
        for (int k = 0; k < Labels.Length; k++)
        {
            string name = Labels[k].name.Remove(Labels[k].name.Length - 6);

            if (GameObject.Find(name + "") != null)
            {
                GameObject dummy = GameObject.Find(name + "").gameObject;

                Labels[k].transform.parent = dummy.transform;
                Labels[k].transform.rotation = Quaternion.identity;
                Labels[k].SetActive(false);
            }


            //TempTextMesh = Labels[k].GetComponentsInChildren<TextMeshPro>();

            //if (TempTextMesh.Length > 1)
            //{
            //    for (int p = 0; p < TempTextMesh.Length; p++)
            //    {
            //        if (DescriTextDoc.ContainsKey(name + "_" + p))
            //            TempTextMesh[p].text = DescriTextDoc[name + "_" + p];
            //    }
            //}
            //else
            //{
            //    if (DescriTextDoc.ContainsKey(name))
            //        Labels[k].GetComponentInChildren<TextMeshPro>().text = DescriTextDoc[name];
            //}
           
        }

      
    }
    //public void AddListener(EventTriggerType eventType, Action MethodToCall2(float v), GameObject TriggerObjToAdd)
    //{
    //    EventTrigger.Entry entry = new EventTrigger.Entry();
    //    entry.eventID = eventType;
    //    entry.callback.AddListener((data) => MethodToCall2(float v ));
    //    TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    //}
    public void AddListenerToEvents(EventTriggerType eventType, Action<float> MethodToCall, GameObject TriggerObjToAdd, float p)
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall(p));
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }

    public void AddListener(EventTriggerType eventType, Action MethodToCall, GameObject TriggerObjToAdd)
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall());
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }

    public void EnableOrbit()
    {
        if (!IsAR)
        {
            CamOrbit.DisableInteration = false;


        }
        else
        {
            ArOrbit.DisableInteration = false;
        }
        if (OptionUnlockedTill < 1)
        {

            if (InformationPanel.gameObject.transform.localScale.x != 1)
            {

                if (TopicToCheck <= (TaskList.Count))
                {
                    iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "time", 0.2f, "easetype", iTween.EaseType.linear));
                    iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));
                    ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen");
                }
            }
        }
    }

    public void RadiusAddXp() {
        if ((TopicToCheck <= (TaskList.Count)) && (InformationPanel.transform.localScale.y == 0))
        {
            if (TopicToCheck == 2)
            {
                AddXpEffect();
            }
            else
            {
                WrongOptionSelected();
            }
        }
    }

    public void DistanceAddXp()
    {
        if ((TopicToCheck <= (TaskList.Count)) && (InformationPanel.transform.localScale.y == 0))
        {
            if (TopicToCheck == 3)
            {
                AddXpEffect();
            }
            else
            {
                WrongOptionSelected();
            }
        }
    }

    public void DisableOrbit()
    {
        if (!IsAR)
        {
           CamOrbit.DisableInteration = true;
        }
        else
        {
            ArOrbit.DisableInteration = true;
        }

    }

    public void AddListener(EventTriggerType eventType, Action<float> MethodToCall, GameObject TriggerObjToAdd)
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => MethodToCall(TriggerObjToAdd.GetComponent<Slider>().value));
        TriggerObjToAdd.GetComponent<EventTrigger>().triggers.Add(entry);
    }

    public Vector3 MidPoint(GameObject obj1, GameObject obj2)
    {
        Vector3 midp = (obj1.transform.localPosition + obj2.transform.localPosition) / 2;
        return midp;
    }

    public float Scale(GameObject obj1, GameObject obj2)
    {
        float scl = Vector3.Distance(obj1.transform.localPosition, obj2.transform.localPosition);
        return scl;
    }





    public void StartCamera(float val)
    {
      //  iTween.ValueTo(gameObject, iTween.Hash("from", orbit.cameraRotSide, "to", val, "delay", 0.1f, "islocal", true, "time", 2f, "easetype", iTween.EaseType.easeInSine, "onupdate", "TweenedSomeValue"));
        //iTween.ValueTo(gameObject, iTween.Hash("from", orbit.distance, "to", 0.5f, "delay", 0.1f, "islocal", true, "time", 2f, "easetype", iTween.EaseType.easeInSine, "onupdate", "TweenedValue"));
        // orbit.distance = 0.5f;
    }
    public void TweenedSomeValue(float val)
    {
      //  orbit.cameraRotSide = val;
    }

    public void AddXpCall()
    {
        AddXpEffect();
    }
  
    public void Labels_on()
    {
        label_onbool = !label_onbool;
        if (!label_onbool)
        {
            for (int i = 0; i < Labels.Length; i++)
            {
                Labels[i].SetActive(true);
            }
        }
        else
        {
            for (int i = 0; i < Labels.Length; i++)
            {
                Labels[i].SetActive(false);
            }
        }
        
    }
    public void ResetButton()
    {
       
    }

    public void RandomMethod()
    {

        // yield return new WaitForSeconds(.1f);
        D = V * T;
        D = D / 2;

        if (beta_value < alpha_value)
        {
            beta_pick();
        }
        else if (beta_value == alpha_value)
        {
            beta_pick();
            alpha_pick();
        }
        else
        {
            // Debug.Log("BETA =" + beta_value + ", ALPHA =" + alpha_value);
            betaLine.transform.localEulerAngles = new Vector3(0, 0, beta_value);
            alphaLine.transform.localEulerAngles = new Vector3(0, 0, alpha_value);
            // moveObj.transform.localPosition = betaLine.transform.GetChild(1).transform.localPosition;
            //moveObj.transform.localEulerAngles = betaLine.transform.localEulerAngles;
            // moveObj.transform.localEulerAngles = new Vector3(0, 0, -beta_value);
            moveObj.transform.parent = assetgroup.transform;
            line_6.transform.localPosition = new Vector3(moveObj.transform.localPosition.x, line_6.transform.localPosition.y, line_6.transform.localPosition.z);
            topline.transform.localPosition = moveObj.transform.localPosition;
            toplineStartPos = topline.transform.localPosition;
            moveObj.transform.localEulerAngles = new Vector3(0, 0, 0);
            finalPos = new Vector3(moveObj.transform.localPosition.x + D, moveObj.transform.localPosition.y, moveObj.transform.localPosition.z);
            t = 0;
            InvokeRepeating("ObjectMovement", 2f, 0.01f);
            CancelInvoke("RandomMethod");


        }
        velocity_txt.text = V.ToString() + " m/s";
        time_txt.text = T.ToString() + " mins";
        beta_txt.text = beta_value.ToString()+ "°";
        alpha_txt.text = alpha_value.ToString()+ "°";
        if (round_num==1)
        {
            Invoke("AfterReset",.3f);
        }

    }
    public void AfterReset()
    {
         update_bool = true;
        round_num = 0;
    }
    public int beta_pick()
    {
        r_pickvalue = UnityEngine.Random.Range(0, 4);
        beta_value = beta_alpha[r_pickvalue];
        return beta_value;
    }
    public int alpha_pick()
    {
        r_pickvalue = UnityEngine.Random.Range(0, 4);
        alpha_value = beta_alpha[r_pickvalue];
        return alpha_value;
    }
    public int VelocityPick()
    {
        int index = UnityEngine.Random.Range(0, 2);
        V = velocity_values[index];
        return V;

    }
    public int TimePick()
    {
        int index = UnityEngine.Random.Range(0, 2);
        T = time_values[index];
        return T;

    }

    public void DistanceCalculation()
    {
       
       if (button_submit.gameObject.GetComponentInChildren<Text>().text == "Submit")
       {
            if (TopicToCheck == 3 || OptionUnlockedTill == 1)
            {
                float dist = V * T;
                dis_ans.text = dist.ToString() + " m";
                button_submit.gameObject.GetComponentInChildren<Text>().text = "Next";
                ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelClose");
                if (OptionUnlockedTill < 1)
                {
                    if(TopicToCheck == 3)
                    AddXpEffect();
                }
                   

            }
            else
            {
                WrongOptionSelected();
                WronOptionPanel.transform.GetChild(0).GetComponent<Text>().text = "Wrong parameter chosen";
                WronOptionPanel.transform.GetChild(0).GetComponent<Text>().color = new Color(0.9921f, 0.4431f, 0.08621f, 1);
            }

        }
            else if (button_submit.gameObject.GetComponentInChildren<Text>().text == "Next")
            {
                dis_cal_group.SetActive(false);
                dis_cal_2.SetActive(true);
            ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen");
        }
          

    }

    public void HightCalculation()
    {
        if (but_submit_1.gameObject.GetComponentInChildren<Text>().text == "Submit")
        {
            if ((inputField21.GetComponentInChildren<Text>().text!=""&& inputField22.GetComponentInChildren<Text>().text !=""&&
                inputField23.GetComponentInChildren<Text>().text != ""&& inputField24.GetComponentInChildren<Text>().text != ""&&
                inputField25.GetComponentInChildren<Text>().text != ""&& inputField26.GetComponentInChildren<Text>().text != "") || OptionUnlockedTill == 1)
            {
                float c2_v = float.Parse(inputField21.GetComponentInChildren<Text>().text);
                float c2_t = float.Parse(inputField22.GetComponentInChildren<Text>().text);
                float c2_tan_alpha1 = float.Parse(inputField23.GetComponentInChildren<Text>().text);
                float c2_tanbeta1 = float.Parse(inputField24.GetComponentInChildren<Text>().text);
                float c2_tan_alpha2 = float.Parse(inputField25.GetComponentInChildren<Text>().text);
                float c2_tanbeta2 = float.Parse(inputField26.GetComponentInChildren<Text>().text);

                float neumanetor = (c2_v * c2_t) * (Mathf.Tan(c2_tan_alpha1) * Mathf.Tan(c2_tanbeta1));

                float denaminator = (Mathf.Tan(c2_tan_alpha2) - Mathf.Tan(c2_tanbeta2));
                float H = neumanetor / denaminator;
                cal2ans.text = H.ToString("F2") + " m";

                float original_ans = ((V * T) * (Mathf.Tan(alpha_value) * Mathf.Tan(beta_value))) / (Mathf.Tan(alpha_value) - Mathf.Tan(beta_value));


                //Debug.Log("Height = "+ H+" OeriginalAns = " +original_ans);
                //if (original_ans == H)
                //{
                    if (OptionUnlockedTill < 1)
                    {
                        if (TopicToCheck == 4)
                            AddXpEffect();
                    }
                       
                    cal2ans.color = Color.green;
                    but_submit_1.gameObject.GetComponentInChildren<Text>().text = "Reset";
                //}
                //else
                //{
                //    cal2ans.color = Color.red;
                //    but_submit_1.gameObject.GetComponentInChildren<Text>().text = "Retry";
                //}
               
            }
            else
            {
                WrongOptionSelected();
                WronOptionPanel.transform.GetChild(0).GetComponent<Text>().text = "value cannot be emmpty.";
                WronOptionPanel.transform.GetChild(0).GetComponent<Text>().color = new Color(0.9921f, 0.4431f, 0.08621f, 1);
            }
        }
        else if (but_submit_1.gameObject.GetComponentInChildren<Text>().text == "Retry")
        {
            inputField21.GetComponentInChildren<Text>().text = "";
            inputField22.GetComponentInChildren<Text>().text = "";
            inputField23.GetComponentInChildren<Text>().text = "";
            inputField24.GetComponentInChildren<Text>().text = "";
            inputField25.GetComponentInChildren<Text>().text = "";
            inputField26.GetComponentInChildren<Text>().text = "";
            cal2ans.text = "";
            cal2ans.color = Color.white;
            but_submit_1.gameObject.GetComponentInChildren<Text>().text = "Submit";
        }
        else if (but_submit_1.gameObject.GetComponentInChildren<Text>().text == "Reset")
        {
            //gameObject.GetComponent<Controller>().
            // FindingGameObjects();
            update_bool = false;
            x1 = 0;
            y1 = 0;
            x2 = 0;
            y2 = 0;
            slope = 0;
            theta = 0;
            D = 0;
            T = 0;
           
            alpha_value = 0;
            beta_value = 0;
            alpha_scle = 0; topline_scale = 0; bottomline_scale = 0; r_l_scale = 0;
            bottomTargetPos = new Vector3(0, 0, 0); right_line_start_pos = new Vector3(0, 0, 0);
            finalPos = new Vector3(0, 0, 0);
            moveObj.transform.parent = betaLine.transform;
            moveObj.transform.localPosition = new Vector3(6, 0, 0);
            inputField0.GetComponentInChildren<Text>().text = "";
            inputField1.GetComponentInChildren<Text>().text = "";
            button_submit.gameObject.GetComponentInChildren<Text>().text = "Submit";
            inputField21.GetComponentInChildren<Text>().text = "";
            inputField22.GetComponentInChildren<Text>().text = "";
            inputField23.GetComponentInChildren<Text>().text = "";
            inputField24.GetComponentInChildren<Text>().text = "";
            inputField25.GetComponentInChildren<Text>().text = "";
            inputField26.GetComponentInChildren<Text>().text = "";
            cal2ans.text = "";
            dis_ans.text = "";
            cal2ans.color = Color.white;
            but_submit_1.gameObject.GetComponentInChildren<Text>().text = "Submit";
            //  betaLine.transform.localScale = new Vector3(0, betaLine.transform.localScale.y, betaLine.transform.localScale.z);
            alphaLine.transform.localScale = new Vector3(0, alphaLine.transform.localScale.y, alphaLine.transform.localScale.z);
            topline.transform.localScale = new Vector3(0, topline.transform.localScale.y, topline.transform.localScale.z);
            bottomline.transform.localScale = new Vector3(0, bottomline.transform.localScale.y, bottomline.transform.localScale.z);
            rightline.transform.localScale = new Vector3(0, rightline.transform.localScale.y, rightline.transform.localScale.z);

            dis_cal_group.SetActive(true);
            dis_cal_2.SetActive(false);
            V = VelocityPick();
            T = TimePick();
            alpha_value = alpha_pick();
            beta_value = beta_pick();

            InvokeRepeating("RandomMethod", 1f, 0.001f);
            round_num = 1;
        }


    }
    public void ObjectMovement()
    {
        if (update_bool)
        {

            //  print(finalPos.x+"   :  "+ moveObj.transform.localPosition.x);

            if (float.Parse(moveObj.transform.localPosition.x.ToString("f2")) < float.Parse((finalPos.x).ToString("f2")))
            {
                t = t + 0.0001f;
                moveObj.transform.localPosition = Vector3.Lerp(moveObj.transform.localPosition, finalPos, t * 2);
               // if(!IsAR)
               // CamOrbit.target.transform.localPosition = new Vector3(moveObj.transform.localPosition.x, 3, 0);
            }
            else
            {
                iTween.MoveTo(CamOrbit.target.gameObject, iTween.Hash("x", (finalPos.x/2)-3, "y", 3, "z", 0, "delay", 0.01f, "time", 1.5f, "islocal", true, "easetype", iTween.EaseType.easeInSine));
                
                ControlPanel.GetComponent<Animator>().Play("CpanelOpen");
                CancelInvoke("ObjectMovement");
            }

           
        }
    }

    GameObject obj;


    public void ReadValueFromClick(int id,GameObject from, GameObject to, float value)
    {
        // print("pppppppp");
        if (TopicToCheck == id||OptionUnlockedTill==1)
        {

            obj = to;
            iTween.ScaleTo(from.gameObject, iTween.Hash("x", 0.08f, "y", 0.08f, "z", 0.08f, "delay", 0.2f, "time", 0.5f, "easetype", iTween.EaseType.easeOutElastic));
            iTween.ScaleTo(from.gameObject, iTween.Hash("x", 0.055f, "y", 0.055f, "z", 0.055f, "delay", 0.7f, "time", 0.5f, "easetype", iTween.EaseType.easeOutElastic));

            // XP_Effect.GetComponent<RectTransform>().position = from.GetComponent<RectTransform>().position;

            // XP_Effect.text = "+" + int.Parse(XpList[TopicToCheck - 1]) + " XP";
            //Vector3 screenPos = Camera.main.WorldToScreenPoint(talkbubble.transform.localPosition);
            // Vector3 screenPos = Camera.main.WorldToViewportPoint(talkbubble.transform.localPosition);

            var pos = from.transform.position;
            var vec2 = RectTransformUtility.WorldToScreenPoint(Camera.main, pos);

            XP_Effect.transform.position = vec2;

            XP_Effect.text = value.ToString();

            XP_Effect.transform.localScale = new Vector3(1, 0, 1);

            // iTween.Stop(XP_Effect.gameObject);

            iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 1, "y", 1, "z", 1, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeOutElastic));

            // iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", XP_Effect.transform.position.x, "y", XP_Effect.transform.position.y + 50, "z", 0, "delay", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

            iTween.MoveTo(XP_Effect.gameObject, iTween.Hash("x", to.transform.position.x, "y", to.transform.position.y, "z", 0, "delay", 1.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInSine));

            iTween.ScaleTo(XP_Effect.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "delay", 2f, "time", 0.5f, "easetype", iTween.EaseType.linear, "oncomplete", "SetvalueAfterAnimation", "oncompleteparams", value, "oncompletetarget", this.gameObject));
            GameObject.Find("Canvas").GetComponent<CanvasGroup>().interactable = false;
            
        }
        else
        {
            WrongOptionSelected();
            WronOptionPanel.transform.GetChild(0).GetComponent<Text>().text = "Wrong parameter chosen";
            WronOptionPanel.transform.GetChild(0).GetComponent<Text>().color = new Color(0.9921f, 0.4431f, 0.08621f, 1);
        }
    }

    public void SetvalueAfterAnimation(float v2)
    {
        obj.transform.GetChild(0).GetComponent<Text>().text = v2.ToString();
        obj = null;
        if (OptionUnlockedTill < 1)
        {
            if (TopicToCheck <= 2)
                AddXpEffect();
        }
        Invoke("EnableRayCast", 2);
    }
}

public class Trigno_ColorLerp : MonoBehaviour
{
    Color lerpeColor = Color.red;

    void Update()
    {
        lerpeColor = Color.Lerp(new Color(1, 0, 0, 0.5f), new Color(0, 1, 0, 0.5f), Mathf.PingPong(Time.time, 0.75f));
        this.GetComponent<Renderer>().material.color = lerpeColor;
    }
}


//public class MaindragAndDrop : MonoBehaviour
//{
//    public int id;
//    private bool dragging = false;
//    private float distance, intialGroundTouchvalue;
//    private Vector3 init_pos, initialRotation;
//    RaycastHit hit;
//    private Vector3 temp;
//    private Vector3 screenSpace;
//    private Vector3 offSet;
//    private Camera cam;
//    private GameObject Target_collder, Target_collder2, TagetPos, TagetPos2;
//    private bool CorrectPlace;
//    public orbit CamOrbit;
//    public bool IsAR;
//    public Vector3 relativePos, TargetPos, TargetPos_Init, Magnet_oldPos, Magnet_oldRot;
//    private float Initial_Dist;
//    public chargetomassratioelectron chargetomassratioelectron;
//    private int placePos;
//    public AROrbitControls ArOrbit;
//    public void Start()
//    {
//        placePos = 0;
//        CorrectPlace = false;
//        cam = Camera.main.GetComponent<Camera>();
//        IsAR = Camera.main.GetComponentInParent<orbit>() ? false : true;
//        chargetomassratioelectron = GameObject.Find("MainObject").GetComponent<chargetomassratioelectron>();

//        if (!IsAR)
//        {
//            CamOrbit = Camera.main.GetComponentInParent<orbit>();


//            TargetPos_Init = CamOrbit.target.transform.position;
//            //TargetPos = MidObj.transform.position;

//            Initial_Dist = orbit.distance;
//        }
//        else
//        {
//            //CamOrbit = chargetomassratioelectron.Instance.MainObj.transform.parent.gameObject.AddComponent<AROrbitControls>();
//            //  CamOrbit = chargetomassratioelectron.Instance.MainObj.transform.parent.gameObject.AddComponent<AROrbitControls>();
//            ArOrbit = chargetomassratioelectron.Instance.MainObj.transform.parent.gameObject.GetComponent<AROrbitControls>();
//            //ArOrbit = MainObj.transform.parent.gameObject.GetComponent<AROrbitControls>();
//        }

//        Target_collder = GameObject.Find(gameObject.name + "_Collider");
//        Target_collder2 = GameObject.Find(gameObject.name + "_Collider2");
//        TagetPos = GameObject.Find("MagnetPos");
//        TagetPos2 = GameObject.Find("MagnetStartPos");
//       // Target_collder.AddComponent<ColorLerp>();
//       // Target_collder2.AddComponent<ColorLerp>();
//        //dummyObject = GameObject.Find(gameObject.name + "_Dummy");

//        //initialRotation = transform.eulerAngles;

//        if (Target_collder)
//        {
//            Target_collder.SetActive(false);
//            Target_collder2.SetActive(false);
//        }

//        // dummyObject.SetActive(false);






//        EventTrigger.Entry PointerDownentry = new EventTrigger.Entry();
//        PointerDownentry.eventID = EventTriggerType.PointerDown;
//        PointerDownentry.callback.AddListener((data) => { MouseDown(); });

//        gameObject.GetComponent<EventTrigger>().triggers.Add(PointerDownentry);

//        EventTrigger.Entry PointerUpentry = new EventTrigger.Entry();
//        PointerUpentry.eventID = EventTriggerType.PointerUp;
//        PointerUpentry.callback.AddListener((data) => { MouseUp(); });

//        gameObject.GetComponent<EventTrigger>().triggers.Add(PointerUpentry);



//    }

//    void MouseDown()
//    {



//        if (!CorrectPlace)
//        {
//            //orbit.orbit_enabled = false;

//            init_pos = transform.position;

//            initialRotation = transform.eulerAngles;

//            if (placePos == 0)
//            {
//                Target_collder.SetActive(true);
//                Target_collder2.SetActive(false);
//            }
//            else
//            {
//                Target_collder.SetActive(false);
//                Target_collder2.SetActive(true);
//            }

//            this.GetComponent<Collider>().enabled = false;
//            //dummyObject.SetActive(true);
//            chargetomassratioelectron.arrow.transform.localPosition = chargetomassratioelectron.arrow_pos2.transform.localPosition;
//            DisableOrbit();

//            screenSpace = cam.WorldToScreenPoint(transform.position);
//            offSet = transform.position - cam.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenSpace.z));

//            Vector3 curScreenSpace = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenSpace.z);
//            Vector3 curPosition = cam.ScreenToWorldPoint(curScreenSpace) + offSet;

//            intialGroundTouchvalue = curPosition.y;
//            dragging = true;



//        }
//    }

//    public void DisableOrbit()
//    {
//        if (!IsAR)
//        {

//            CamOrbit.DisableInteration = true;


//        }
//        else
//        {
//            ArOrbit.DisableInteration = true;
//        }

//    }
//    void MouseUp()
//    {
//        //if (controllerLevel1.SeqCount == id) {
//        if (!CorrectPlace)
//        {

//            Ray ray = cam.ScreenPointToRay(Input.mousePosition);

//            if (Physics.Raycast(ray, out hit, 100))
//            {

//                if (hit.collider.name == Target_collder.name)
//                {

//                    // print(hit.collider.name);
//                    gameObject.transform.localPosition = TagetPos.transform.localPosition;
//                    gameObject.transform.localEulerAngles = TagetPos.transform.localEulerAngles;
//                    chargetomassratioelectron.magnetic_bool = true;
//                   // placePos = 1;
//                    chargetomassratioelectron.electricarrows.SetActive(false);
//                    chargetomassratioelectron.magneticarrows.SetActive(true);
//                    chargetomassratioelectron.arrow.SetActive(false);
//                    if (chargetomassratioelectron.OptionUnlockedTill < 1)
//                    {
//                        if (chargetomassratioelectron.TopicToCheck == 4)
//                        {
//                            chargetomassratioelectron.AddXpEffect();
//                        }
//                    }
//                }
//                else if (hit.collider.name == Target_collder2.name)
//                {
//                    gameObject.transform.localPosition = TagetPos2.transform.localPosition;
//                    gameObject.transform.localEulerAngles = TagetPos2.transform.localEulerAngles;
//                    chargetomassratioelectron.magnetic_bool = false;
//                    chargetomassratioelectron.electricarrows.SetActive(false);
//                    chargetomassratioelectron.magneticarrows.SetActive(false);
//                    placePos = 0;
//                }
//                else
//                {
//                    // InvokeRepeating("PlacemnetAdjustment", 0.1f, 0.01f);
//                    transform.position = init_pos;
//                    transform.eulerAngles = initialRotation;
//                    this.GetComponent<Collider>().enabled = true;
//                }

//            }
//            else
//            {
//                // InvokeRepeating("PlacemnetAdjustment", 0.1f, 0.01f);
//                transform.position = init_pos;
//                transform.eulerAngles = initialRotation;
//                this.GetComponent<Collider>().enabled = true;

//            }

//            // InvokeRepeating("PlacemnetAdjustment", 0.1f, 0.01f);
//            this.GetComponent<Collider>().enabled = true;
//            Target_collder.SetActive(false);
//            Target_collder2.SetActive(false);
//            //dummyObject.SetActive(false);
//            // orbit.orbit_enabled = true;
//            EnableOrbit();
//            dragging = false;
//        }
//    }

//    public void EnableOrbit()
//    {
//        if (!IsAR)
//        {
//            CamOrbit.DisableInteration = false;


//        }
//        else
//        {
//            ArOrbit.DisableInteration = false;
//        }

//        //if (InformationPanel.gameObject.transform.localScale.x != 1)
//        //{

//        //    if (TopicToCheck <= (TaskList.Count))
//        //    {
//        //        iTween.ScaleTo(TasklistPanel.gameObject, iTween.Hash("x", 1, "y", 0, "z", 1, "time", 0.2f, "easetype", iTween.EaseType.linear));
//        //        iTween.RotateTo(Gained_XP_ArrowBut, iTween.Hash("x", 0, "y", 0, "z", 0, "delay", 0.01f, "time", 0.02f, "easetype", iTween.EaseType.easeInSine));
//        //        ObjectivePanel.GetComponent<Animator>().Play("ObjectivePanelOpen");
//        //    }
//        //}

//    }
//    //bool CheckCondition()
//    //{

//    //    bool b = false;
//    //    int i = 0;

//    //    while (i < chargetomassratioelectron.Instance.DragObjects.Length - 1)
//    //    {
//    //        //if (chargetomassratioelectron.Instance.DragObjects[i].transform.localPosition == chargetomassratioelectron.Instance.DummyObjects[i].transform.localPosition)

//    //        if (chargetomassratioelectron.Instance.DragObjects[i].transform.localPosition == chargetomassratioelectron.Instance.DummyObjects[i].transform.localPosition)
//    //        {
//    //            b = true;
//    //        }
//    //        else { b = false; }
//    //        i++;
//    //    }
//    //    return b;
//    //}


//    void PlacemnetAdjustment()
//    {
//        if (CorrectPlace)
//        {
//            if (transform.position != Target_collder.transform.position)
//            {
//                transform.position = Vector3.Lerp(transform.position, Target_collder.transform.position, 0.1f);
//                transform.eulerAngles = Vector3.Lerp(transform.eulerAngles, Target_collder.transform.eulerAngles, 0.1f);
//            }
//            else
//            {
//                CancelInvoke("PlacemnetAdjustment");
//            }
//        }
//        else
//        {
//            if (transform.position != init_pos)
//            {
//                transform.position = Vector3.Lerp(transform.position, init_pos, 0.1f);
//                transform.eulerAngles = Vector3.Lerp(transform.eulerAngles, initialRotation, 0.1f);
//            }
//            else
//            {
//                CancelInvoke("PlacemnetAdjustment");

//            }
//        }
//    }


//    void Update()
//    {

//        if (dragging)
//        {
//            Vector3 curScreenSpace = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenSpace.z);
//            Vector3 curPosition = cam.ScreenToWorldPoint(curScreenSpace) + offSet;
//            //curPosition = new Vector3 (curPosition.x, Mathf.Clamp (curPosition.y, intialGroundTouchvalue+GroundTouchValue, 100f), curPosition.z);
//            transform.position = curPosition;
//        }
//    }

//    //public void EnableOrbit()
//    //{
//    //    if (!IsAR)
//    //    {
//    //        CamOrbit.orbit_enabled = true;
//    //    }
//    //    else
//    //    {
//    //        ARorbit.orbit_enabled = true;
//    //    }
//    //}

//    //public void DisableOrbit()
//    //{

//    //    if (!IsAR)
//    //    {
//    //        CamOrbit.orbit_enabled = false;
//    //    }
//    //    else
//    //    {
//    //        ARorbit.orbit_enabled = false;
//    //    }
//    //}
//}
