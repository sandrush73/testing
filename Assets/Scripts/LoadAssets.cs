﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Better.StreamingAssets;
public class LoadAssets : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject panel;
    public Button But;
    public List<Button> Butns;
    //public InputField InputField_;
    public string[] m_allStreamingAssets = new string[0];
    public int sceneNumber;
    public TMP_InputField totalXP_field;
    public TMP_InputField maxEarning_field;

    private void Initialize()
    {

        m_allStreamingAssets = BetterStreamingAssets.GetFiles("/", "*", SearchOption.AllDirectories);


    }

    public void UpdateSceneNumber(int num) {

        sceneNumber = num;
        Debug.Log("scene number: " + num.ToString());
    }


    void Start()
    {


        But = GameObject.Find("But").GetComponent<Button>();
        panel = GameObject.Find("Panel");
        //InputField_ = GameObject.Find("InputField_").GetComponent<InputField>();
        //InputField_.onValueChanged.AddListener(delegate { ShadowValue(ExplodeSlider.value); });

        // InputField_.onValueChanged.AddListener(delegate { ShadowValue(InputField_.text); });
        //  PlayerPrefs.SetString("All", Path.Combine(Application.streamingAssetsPath, assetBundleName));
        BetterStreamingAssets.Initialize();

        Initialize();

        for (int i = 0; i < m_allStreamingAssets.Length; i++) {
            Button butt= Instantiate(But);
            butt.transform.parent = panel.transform;
            butt.transform.localScale = new Vector3(1, 1, 1);
            butt.GetComponentInChildren<Text>().text = m_allStreamingAssets[i];
            butt.onClick.AddListener(() => LoadBundle(butt.GetComponentInChildren<Text>().text));
            butt.name = m_allStreamingAssets[i];
            Butns.Add(butt);
        }
    }

    public void ShadowValue(string str) {

        if (str != "")
        {
            for (int i = 0; i < Butns.Count; i++)
            {
                if (Butns[i].name.Contains(str))
                {
                    Butns[i].gameObject.SetActive(true);
                }
                else
                {
                    Butns[i].gameObject.SetActive(false);
                }
            }
        }
        else {
            for (int i = 0; i < Butns.Count; i++)
            {
               
                    Butns[i].gameObject.SetActive(true);
                
                
            }
        }
    }
    public void LoadBundle(string path) {

        bool sendChallenge = false;
        PlayerPrefs.SetString(Constants.LOGIN_ID, "demojeemainbeta1@3rdflix.com");
        PlayerPrefs.SetString(Constants.PASSWORD_HASH, "ceb6c970658f31504a901b89dcd3e461");
        PlayerPrefs.SetString(Constants.CLIENT_ACCESSKEY, "ceb6c970658f31504a901b89dcd3e461");
        PlayerPrefs.SetString(Constants.AUTH_TOKEN, "da2aa9a0add9deb2df215acdc5b7e75b875860c6");
        PlayerPrefs.SetString(Constants.ID_USER, "72878");
        PlayerPrefs.SetString(Constants.ID_ENTITY, "88");
        PlayerPrefs.SetString(Constants.ID_COURSE, "189");
        PlayerPrefs.SetString(Constants.ID_STUDENT, "71474");
        string aburl = Application.streamingAssetsPath + "/" + path;
        string _toSend = "";
        string totalXP = totalXP_field.text;
        string maxEarnings = maxEarning_field.text;

        Debug.Log("scene number: " + sceneNumber.ToString());
        PlayerPrefs.SetString("NetworkConnected", "F");
        if (!sendChallenge)
        {
            _toSend = sceneNumber + "|" + aburl + "|" + path + "|" + path + "|Lorem Ipsum|9481|" + "landscape" + "|524|9820|23763|" + totalXP + "|" + maxEarnings + "|";
            SendMessage("GetValuesFromNative", _toSend);
        }
        else
        {
            _toSend = sceneNumber + "|" + aburl + "|" + path + "|" + path + "|Friction Force Challenge|9481|" + "landscape" + "|";
            SendMessage("GetValuesForChallenge", _toSend);
        }
    }

  
}
